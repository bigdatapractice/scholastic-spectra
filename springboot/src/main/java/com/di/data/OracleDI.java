package com.di.data;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URISyntaxException;
import java.security.NoSuchAlgorithmException;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.logging.Logger;
import java.util.zip.ZipInputStream;

import com.google.gson.Gson;

import oracle.cloud.storage.CloudStorage;
import oracle.cloud.storage.CloudStorageConfig;
import oracle.cloud.storage.CloudStorageFactory;
import oracle.cloud.storage.model.Key;
import oracle.cloud.storage.model.StorageInputStream;



public class OracleDI {
	Properties prop = null;
	String propFileName = "conf/oracle.properties";
	static Logger log = Logger.getLogger(OracleDI.class.getName());
	Map<String, FileConfig> fConfigMap  = null;
	String fconfFile = "conf/fileconfig.json";
	
	public static void main(String[] args) throws Exception {
			
		log.info("Program start time: " + new Date());
		OracleDI odi = new OracleDI();
		odi.readProperties();
		odi.readFileConfig("json");
		CloudStorage conn = odi.getConnection();
		odi.processData(conn);
		log.info("Program End Date: " + new Date());
		
	}
	
	
	public void startload(String json) throws Exception {
		log.info("Program start time: " + new Date());
		this.readProperties();
		this.readFileConfig(json);
		CloudStorage conn = this.getConnection();
		this.processData(conn);
		log.info("Program End Date: " + new Date());
		
		
		
	}
	

	private void readProperties() {
		
		log.info("Reading Properties File ...............");
		try (InputStream input = new FileInputStream(propFileName)) {
			prop = new Properties();
			prop.load(input);
			log.info("Reading Property File Complted");
		}catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	
	
	private void readFileConfig(String json) throws IOException {
		log.info("Reading Json Config File............");
		fConfigMap = new HashMap<String, FileConfig>();
        //String content = new String(Files.readAllBytes(Paths.get(fconfFile)));
		String content = json;
		Gson gson = new Gson();
		FileConfig[] fileConfigArray = gson.fromJson(content, FileConfig[].class);  

		for (int i = 0;i<fileConfigArray.length;i++ ) {
			fConfigMap.put(fileConfigArray[i].getInputfile().toLowerCase(), fileConfigArray[i]);
			
		}
	}
	
	private CloudStorage getConnection() throws Exception {
		CloudStorageConfig myConfig = new CloudStorageConfig();
		CloudStorage myConnection = null;
		try {
	        myConfig.setServiceName(prop.getProperty("servicename")).setUsername(prop.getProperty("username"))
	                        .setPassword(prop.get("password").toString().toCharArray()).setServiceUrl(prop.getProperty("serviceurl"));
	         myConnection = CloudStorageFactory.getStorage(myConfig);
		}catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
		
		return myConnection;
		
	}
	
	private void processData(CloudStorage conn) throws IOException, URISyntaxException, NoSuchAlgorithmException {
		List<Key> fList = this.getListofFiles(conn);
		HDFS hdfs = new HDFS(prop.getProperty("hdfsmaster"));	
		//LocalFileSystem local = new LocalFileSystem();
		
		for (Key k:fList) {
			if (k.getKey().endsWith(".zip")) {
				String keyStr = null;
				try {
				keyStr = k.getKey().split("-")[0];
				}
				catch (ArrayIndexOutOfBoundsException e) {
					e.printStackTrace();
					log.warning("File : " + k.getKey() + " Has different File name Format , will not be processed");
					continue;
				}
				if (fConfigMap.containsKey(keyStr.toLowerCase())) {
					FileConfig fileConfig = fConfigMap.get(keyStr.toLowerCase());
					log.info("Processing For File " + k.getKey());
					StorageInputStream st = null;
					ZipInputStream zis = null;
					try {
					st = conn.retrieveObject(prop.getProperty("container"), k.getKey());
					//String md5 = org.apache.commons.codec.digest.DigestUtils.md5Hex(st);
					//System.out.println(md5);
				
					//local.writetoLocal(st, k.getKey());
					
					//***********************
					zis = new ZipInputStream(st);
					zis.getNextEntry();
					hdfs.hdfsProcess(fileConfig, zis, k.getKey().replace(".zip", ""));
					
					//****************
					
					}
					catch (Exception e) {
						e.printStackTrace();
					}finally {
						if (st != null) st.close();
						if (zis != null) zis.close();					
					}	
					}			
				}
		}
		
	}
	private List<Key> getListofFiles(CloudStorage conn) {
		log.info("Getting List of Files in Container" + prop.getProperty("container"));
		List<Key> fList = conn.listObjects(prop.getProperty("container"),null);
		return fList;
		
	}
}
