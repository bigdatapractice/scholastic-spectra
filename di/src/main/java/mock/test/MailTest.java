package mock.test;

import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

public class MailTest {

	public static void main(String[] args) throws MessagingException {  
		  
		  String host="localhost";  
		  final String user="oracle_extract@localhost";//change accordingly  
		  final String password="";//change accordingly  
		    
		  String to="barun-consultant@scholastic.com;kyaswanth-consultant@scholastic.com";//change accordingly  
		  
		  
		  String[] recipientList = to.split("\\;");
		  
		  InternetAddress[] recipientAddress = new InternetAddress[recipientList.length];
		  int counter = 0;
		  for (String recipient : recipientList) {
		      recipientAddress[counter] = new InternetAddress(recipient.trim());
		      counter++;
		  }
		  
		   //Get the session object  
		   Properties props = new Properties();  
		   props.put("mail.smtp.host",host);  
		   props.put("mail.smtp.auth", "false");  
		     
		   Session session = Session.getDefaultInstance(props,  
		    new javax.mail.Authenticator() {  
		      protected PasswordAuthentication getPasswordAuthentication() {  
		    return new PasswordAuthentication(user,password);  
		      }  
		    });  
		  
		 /*  Session session = Session.getDefaultInstance(props,  
				    new javax.mail.Authenticator() {  
				      protected PasswordAuthentication getPasswordAuthentication() {  
				    return new PasswordAuthentication(user,password);  
				      }  
				    });  
				  
		   */
		   //Compose the message  
		    try {  
		     MimeMessage message = new MimeMessage(session);  
		     message.setFrom(new InternetAddress(user));  
		     message.addRecipients(Message.RecipientType.TO,recipientAddress);  
		     message.setSubject("sample-93898321");  
		     message.setText("This is simple program of sending email using JavaMail API");  
		       
		    //send the message  
		     Transport.send(message);  
		  
		     System.out.println("message sent successfully...");  
		   
		     } catch (MessagingException e) {
		    	 e.printStackTrace();
		    	 throw e;
		     }  
		 }  
}

