package mock.test;

import java.io.IOException;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;


public class HDFSTest {

	public static void main(String[] args) throws IOException {

		
		
		Configuration conf = new Configuration();
		conf.set("fs.hdfs.impl", org.apache.hadoop.hdfs.DistributedFileSystem.class.getName());
		conf.set("fs.file.impl", org.apache.hadoop.fs.LocalFileSystem.class.getName());
		conf.set("fs.defaultFS", "hdfs://nameservice1");
		conf.set("dfs.nameservices", "nameservice1");
		conf.set("dfs.ha.namenodes.nameservice1", "namenode108,namenode156");
		conf.set("dfs.namenode.rpc-address.nameservice1.namenode108", "ip-10-130-11-238.ec2.internal:8020");
		conf.set("dfs.namenode.rpc-address.nameservice1.namenode156", "ip-10-130-13-254.ec2.internal:8020");
		conf.set("dfs.client.failover.proxy.provider.nameservice1","org.apache.hadoop.hdfs.server.namenode.ha.ConfiguredFailoverProxyProvider");
		
		FileSystem hdfs = FileSystem.get(conf);
		
		boolean t = hdfs.mkdirs(new Path("/tmp/testdir"));
		
		System.out.println(t);
		

		
		
	}

}
