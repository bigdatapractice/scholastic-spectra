package com.rl.email;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Date;
import java.util.Properties;
import java.util.Scanner;
import java.util.logging.Logger;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;


public class ReportEmail {

	Properties prop = null;
	static String propFileName = "conf/oracle.properties";
	static Logger log = Logger.getLogger(ReportEmail.class.getName());
	int processCnt = 0;
	int unprocessCnt = 0;
	
	
	public static void main(String[] args) throws SQLException {
	
		ReportEmail reportEmail = new ReportEmail();
		reportEmail.readProperties();
		Connection mySqlCon = null;
		
		try {
			mySqlCon = reportEmail.getMysqlConnection();
			String mailContenet = reportEmail.generateMailContent(mySqlCon);
			reportEmail.sendEmail(mailContenet);
			
		
		}catch (Exception e) {
			e.printStackTrace();
		}finally {
			if (mySqlCon != null) mySqlCon.close();
		}
	}

	
	//<p style="color:white;">
private String generateMailContent(Connection mySqlCon) throws SQLException {
	
	int maxId = 0;
	
	String query = "select * from di.di_files where f_id > " + this.getlastRequest() + ";";
	Statement st = mySqlCon.createStatement();
	ResultSet rs = null;
	StringBuilder mailContent = new StringBuilder();
	
	mailContent.append("<html><body>");
	mailContent.append("<br>");
	mailContent.append("<p style=\"font-family:verdana;\">" + "Report Generated on : " + new Date() +"</p>");
	mailContent.append("<br>");

	
	mailContent.append("<hr>");
	mailContent.append("<br>");
	mailContent.append("<br>");
	
	mailContent.append("<table border=\"0.1\" bordercolor=\"#000000\"");
		
	mailContent.append("<tr bgcolor = \"#b3b3b3\">");
	mailContent.append("<td>");
	mailContent.append("<b><p style=\"color:black;\">File Name</p></b>");
	mailContent.append("</td>");
	
	mailContent.append("<td>");
	mailContent.append("<b><p style=\"color:black;\">Size &nbsp;&nbsp;&nbsp;&nbsp;</p></b>");
	mailContent.append("</td>");
	
	mailContent.append("<td>");
	mailContent.append("<b><p style=\"color:black;\">Processed By&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</p></b>");
	mailContent.append("</td>");
	
	
	mailContent.append("<td>");
	mailContent.append("<b><p style=\"color:black;\">Received  Date</p></b>");
	mailContent.append("</td>");
	
	mailContent.append("<td>");
	mailContent.append("<b><p style=\"color:black;\">Record Count</p></b>");
	mailContent.append("</td>");
	
	mailContent.append("<td>");
	mailContent.append("<b><p style=\"color:black;\">Status&nbsp;&nbsp;&nbsp;&nbsp;s&nbsp;&nbsp;&nbsp;&nbsp;s&nbsp;&nbsp;&nbsp;&nbsp;</p></b>");
	mailContent.append("</td>");
	
	mailContent.append("<td>");
	mailContent.append("<b><p style=\"color:black;\">Remarks&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</p></b>");
	mailContent.append("</td>");
	mailContent.append("<tr>");
	
	
	try {
		rs = st.executeQuery(query);
		while(rs.next()) {
			while (rs.next()) {
				
				if (rs.getInt("f_id") > maxId) {
					maxId = rs.getInt("f_id");
				}
				
				mailContent.append("<tr bgcolor=\"#EDE5E5\">");
	            mailContent.append("<td>");
	            String  fname = rs.getString("file_name");
	            mailContent.append(this.createFont(fname));
	            mailContent.append("</td>");

	            mailContent.append("<td>");
	            String  size = this.createFont(this.checknull(this.getMb(rs.getLong("src_file_size"))));
	            mailContent.append(size);
	            mailContent.append("</td>");
	            
	            mailContent.append("<td>");
	            String  processedBy = this.createFont(this.checknull(rs.getString("processed_by")));
	            mailContent.append(processedBy);
	            mailContent.append("</td>");
	            
	            mailContent.append("<td>");
	            String  processedOn = rs.getString("add_date").substring(0,10);
	            mailContent.append(this.createFont(processedOn));
	            mailContent.append("</td>");
	            
	            mailContent.append("<td>");
	            mailContent.append(this.createFont(this.checknull(rs.getString("row_count"))));
	            mailContent.append("</td>");
	            
	            this.updateStatusAndRemarks(mailContent, rs);

	         
	            
		        mailContent.append("<tr>");
	        }

			
			mailContent.append("</table>");
			mailContent.append("</p>");
			mailContent.append("</p>");
			mailContent.append("</p>");
			
			mailContent.append("<p style=\"font-family:verdana;\">" + "Total Number of Files : " + (processCnt + unprocessCnt) +"</p>");
			mailContent.append("<p style=\"font-family:verdana;\">" + "Number of Files Processed : " + processCnt +"</p>");
			mailContent.append("<p style=\"font-family:verdana;\">" + "Number of Files Unprocessed : " + unprocessCnt + "</p>");
			
			mailContent.append("<p></p>");
			mailContent.append("<hr>");
			mailContent.append("<p></p>");
			
			mailContent.append("<p style=\"font-family:calibri;\">" + "This is Auto Generated Email, please Do Not Respond </p>");
			
			mailContent.append("</body></html>");
			this.writeLastRequest(maxId);
		}
	}catch (Exception e) {
		e.printStackTrace();
	}finally {
		if (rs != null) rs.close();
		if (st != null) st.close();
	}
	
	return mailContent.toString();
}

private String createFont(String s) {
	
	return "<p style=\"font-family:calibri;\">" + s + "</p>" ;
	
}

private void updateStatusAndRemarks(StringBuilder mailContent,ResultSet rs) throws SQLException {
	String processed = "Processed";
	String comment = "NA";
	//String cellColor = null;
	String celltag = "<td bgcolor = \"#5cd65c\">";
	
	if (rs.getString("processed_by") == null) {
		unprocessCnt ++;
		processed = "Not Processed";
		celltag = "<td bgcolor = \"#ffad33\">";
		if (rs.getString("target_dir") == null ) {
			comment = "Target Dir Not Provided";
			
		}else {
			celltag = "<td bgcolor = \"#ff471a\"";
			comment = "File Not Processed . Dev Team will look into it";
		}
	}else {
		processCnt ++;
	}
	
	   mailContent.append(celltag);
       mailContent.append(this.createFont(processed));
       mailContent.append("</td>");
       
       mailContent.append(celltag);
       mailContent.append(this.createFont(comment));
       mailContent.append("</td>");
    
}

private String checknull(String in) {
	
	String rt = in;
	if (in == null) {
		 rt = "NA";
	}
	return rt;
}



private void sendEmail(String mailContent) throws AddressException {

	
	//this.sendGmailMessage("bhuvan.arun@relevancelab.com","","bhuvan.arun@relevancelab.com","hello javatpoint",mailContent);  
	this.sendSmtpMessgae(mailContent);
	
}


private void sendSmtpMessgae(String msg) throws AddressException {
	
	  Properties props = new Properties();
  	  props.put("mail.smtp.host","localhost");  
  	  props.put("mail.smtp.auth", "false");  
  	
  	  String user="extraction_report@localhost";	
  	  String password = "";
  	
	  String to = prop.getProperty("email");	  
	  String[] recipientList = to.split("\\;");
	  InternetAddress[] recipientAddress = new InternetAddress[recipientList.length];
	  int counter = 0;
	  for (String recipient : recipientList) {
	      recipientAddress[counter] = new InternetAddress(recipient.trim());
	      counter++;
	  }
	  
	  
	  Session session = Session.getDefaultInstance(props,  
			    new javax.mail.Authenticator() {  
			      protected PasswordAuthentication getPasswordAuthentication() {  
			    return new PasswordAuthentication(user,password);  
			      }  
			    }); 
	  
	  try {  
		     MimeMessage message = new MimeMessage(session);  
		     message.setFrom(new InternetAddress(user));  
		     message.addRecipients(Message.RecipientType.TO,recipientAddress);  
		     message.setSubject( "Oracle Extraction Report - " + prop.getProperty("env") + " " + new Date() ); 
		     message.setContent(msg,"text/html");
		       
		     Transport.send(message);  
		  
		    
		     } catch (MessagingException e) {e.printStackTrace();}  
		 
	
	
	
	
	
	
}
@SuppressWarnings("unused")
private void sendGmailMessage(String from,String password,String to,String sub,String msg) {
	 Properties props = new Properties();    
     props.put("mail.smtp.host", "smtp.gmail.com");    
     props.put("mail.smtp.socketFactory.port", "465");    
     props.put("mail.smtp.socketFactory.class",    
               "javax.net.ssl.SSLSocketFactory");    
     props.put("mail.smtp.auth", "true");    
     props.put("mail.smtp.port", "465");    
     //get Session   
     Session session = Session.getDefaultInstance(props,    
      new javax.mail.Authenticator() {    
      protected PasswordAuthentication getPasswordAuthentication() {    
      return new PasswordAuthentication(from,password);  
      }    
     });    
     //compose message    
     try {    
      MimeMessage message = new MimeMessage(session);    
      message.addRecipient(Message.RecipientType.TO,new InternetAddress(to));    
      message.setSubject(sub);    
      //message.setText(msg);    
      message.setContent(msg,"text/html");
      //send message  
      Transport.send(message);    
      System.out.println("message sent successfully");    
     } catch (MessagingException e) {throw new RuntimeException(e);}    
        
}  


private Connection getMysqlConnection() throws SQLException, ClassNotFoundException {
	String mysql = prop.getProperty("mysql");
	String mysqlUsername = prop.getProperty("mysql_username");
	String mysqlPassword = prop.getProperty("mysql_password");
	Class.forName("com.mysql.jdbc.Driver");  
	Connection conn = DriverManager.getConnection(  
			mysql,mysqlUsername,mysqlPassword);  
	return conn;
}

private void readProperties() {
	
	log.info("Reading Properties File ...............");
	try (InputStream input = new FileInputStream(propFileName)) {
		prop = new Properties();
		prop.load(input);
		log.info("Reading Property File Complted");
	}catch (Exception e) {
		e.printStackTrace();
	}
	
}


private String getMb(long size) {
	
	String mbsize = Math.round(size / (1024*1024)) + " MB";
	return mbsize;
}

private int getlastRequest() {
	String rt = "";
	String lreqFile = "conf/lr.log";
	Scanner sc = null;
	int num = 0;
	try {
	sc = new Scanner(new File(lreqFile));
	rt = sc.useDelimiter("\\Z").next();
	int tnum = Integer.valueOf(rt.trim());

	log.info("Previously reported ID: " + tnum);
	
	num = tnum;
	}
	catch (Exception e) {
		e.printStackTrace();
	}finally {
		if (sc != null) sc.close();
	}
	return num;
	
}

private void writeLastRequest(int content) {
	String lreqFile = "conf/lr.log";
	
	Path path = Paths.get(lreqFile);
	try (BufferedWriter writer = Files.newBufferedWriter(path)) 
	{
	    writer.write(String.valueOf(content));
	}catch (Exception e) {
		e.printStackTrace();
	}	
}
}

