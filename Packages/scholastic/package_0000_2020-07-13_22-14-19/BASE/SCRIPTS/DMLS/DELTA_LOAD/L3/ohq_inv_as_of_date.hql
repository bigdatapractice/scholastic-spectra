insert into table rlsf_prod.ohq_inv_as_of_date
select
oracle_item ,
isbn13 ,
border_item ,
opus_id ,
org_id ,
cost_center ,
trans_eo_inventory_item_id  as inventory_item_id,
sum(totalsum) as total_qty ,
run_date 
from rlsf_prod.ohq_inv_as_of_date_by_cost_rec cost
join rlsf_prod.cost_trans_rank_assign_delta assign on assign.transactionseoinventoryitemid = cost.trans_eo_inventory_item_id and 
assign.ranknumber >=${var:fromranknumber} and assign.ranknumber <= ${var:toranknumber}

group by 
oracle_item ,
isbn13 ,
border_item ,
opus_id ,
org_id ,
cost_center ,
trans_eo_inventory_item_id ,
run_date;
