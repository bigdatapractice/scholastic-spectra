INVALIDATE METADATA rlsf_prod.s3_inv_master_distinct;

INSERT OVERWRITE TABLE rlsf_prod.inv_master_distinct_parquet
select 
oracle_item_id 
,inventory_item_id 
,DESCRIPTION 
,ITEM_TYPE 
 from rlsf_prod.s3_inv_master_distinct;