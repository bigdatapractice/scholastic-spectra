#!/bin/bash

export PYTHON_EGG_CACHE=./myeggs

LOG=ArchiveData-$(date +%s%N).log
connect_impala=$1	#ip-10-130-13-108.ec2.internal:21000
QUERY_FILE=$2		# query file name 
TABLENAME=$3
flag=$4

if [ ! -f $QUERY_FILE ];
then
  ERROR="Unable to access [ query-file: $TRUNCATE_TABLE ], check existence and permissions"
  echo "$ERROR" >> ${LOG}
  hadoop fs -put ${LOG} /sch/logs
  exit 1
fi

if hdfs dfs -test -e /sch/trigger/${flag} ; 
then
	echo " table already appended to hist table"
	impala-shell -i $connect_impala -q "truncate table ${TABLENAME}" >>${LOG} 2>&1
	if [ $RC -eq 0 ];
	then
		echo "delta table data has been truncated" 
		hadoop fs -rmr /sch/trigger/${flag}
	else
		echo " truncate command failed."
		hadoop fs -put ${LOG} /sch/logs
		exit 2
	fi
else 
	impala-shell -i $connect_impala -f $QUERY_FILE >>${LOG} 2>&1
	RC=$?
	if [ $RC -eq 0 ];
	then
			echo "data has been appended succefully to hist table" $QUERY_FILE
			hadoop fs -touchz /sch/trigger/${flag}
			if hdfs dfs -test -e /sch/trigger/${flag} ; 
			then
				impala-shell -i $connect_impala -q "truncate table ${TABLENAME}" >>${LOG} 2>&1
				if [ $RC -eq 0 ];
				then
					echo "delta table data has been truncated" 
					hadoop fs -rmr /sch/trigger/${flag}
				else
					echo " truncate command failed."
					hadoop fs -put ${LOG} /sch/logs
					exit 2
				fi
			fi
	else
			echo " table has been failed . please check the logs  " $QUERY_FILE
			hadoop fs -put ${LOG} /sch/logs
			exit 1
	fi
fi	

hadoop fs -rmr /sch/trigger/${flag}
rm ${LOG} 



