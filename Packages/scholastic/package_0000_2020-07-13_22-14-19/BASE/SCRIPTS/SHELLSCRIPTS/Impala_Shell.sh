#!/bin/bash

export PYTHON_EGG_CACHE=./myeggs

LOG=ArchiveData-$(date +%s%N).log
connect_impala=$1	#ip-10-130-13-108.ec2.internal:21000
QUERY_FILE=$2		# query file name 

if [ ! -f $QUERY_FILE ];
then
  ERROR="Unable to access [ query-file: $TRUNCATE_TABLE ], check existence and permissions"
  echo "$ERROR" >> ${LOG}
  hadoop fs -put ${LOG} /sch/logs
  exit 1
fi

impala-shell -i $connect_impala -f $QUERY_FILE >>${LOG} 2>&1
RC=$?
if [ $RC -eq 0 ];
		then
        echo "table has been executed " $QUERY_FILE 
else
        echo " table has been failed . please check the logs  " $QUERY_FILE
		hadoop fs -put ${LOG} /sch/logs
		exit 1
fi 

rm ${LOG} 



