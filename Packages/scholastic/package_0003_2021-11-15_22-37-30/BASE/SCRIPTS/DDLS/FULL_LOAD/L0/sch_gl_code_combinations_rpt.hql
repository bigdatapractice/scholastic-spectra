DROP TABLE

IF EXISTS rlsf_prod.sch_gl_code_combinations_rpt;
	CREATE EXTERNAL TABLE rlsf_prod.sch_gl_code_combinations_rpt (
		code_combination_id STRING
		,last_update_date STRING
		,chart_of_accounts_id STRING
		,detail_posting_allowed_flag STRING
		,detail_budgeting_allowed_flag STRING
		,account_type STRING
		,enabled_flag STRING
		,summary_flag STRING
		,segment1 STRING
		,segment2 STRING
		,segment3 STRING
		,segment4 STRING
		,segment5 STRING
		,segment6 STRING
		,segment7 STRING
		,segment8 STRING
		,segment9 STRING
		,description STRING
		,creation_date STRING
		) ROW FORMAT DELIMITED FIELDS TERMINATED BY '|' STORED AS TEXTFILE LOCATION 'hdfs://nameservice1/sch/l0/SCH_GL_CODE_COMBINATIONS_RPT';