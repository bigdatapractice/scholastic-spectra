DROP TABLE

IF EXISTS rlsf_prod.oracle_items_parquet;
	CREATE EXTERNAL TABLE rlsf_prod.oracle_items_parquet (
		oracle_id STRING
		,inventory_item_id STRING
		,title STRING
		,isbn13 STRING
		,inventory_item_flag STRING
		,item_type STRING
		,inventory_asset_flag STRING
		,costing_enabled_flag STRING
		,opus_num STRING
		,bf_num STRING
		,short_id STRING
		,mdm_id STRING
		,creation_dt STRING
		,category_code STRING
		,category_name STRING
		,cat_description STRING
		,catalog_code STRING
		,version_type STRING
		) STORED AS PARQUET LOCATION 'hdfs://nameservice1/sch/l0/oracle_items_parquet';