DROP TABLE

IF EXISTS rlsf_prod.material_transactions_dist;
	CREATE EXTERNAL TABLE rlsf_prod.material_transactions_dist (
		txn_id STRING
		,txn_qty DOUBLE
		,organization_id STRING
		,org_code STRING
		,org_category STRING
		,item_id STRING
		,oracle_item STRING
		,isbn13 STRING
		,title STRING
		,transaction_date STRING
		,imt_creation_date STRING
		,creation_date STRING
		,last_update_date STRING
		,source STRING
		,legacy_transaction_type STRING
		,legacy_reason STRING
		,reference STRING
		,source_line STRING
		,transfer_organization_id STRING
		,transfer_organization_code STRING
		,transaction_type_id STRING
		,transaction_type STRING
		,subinventory_code STRING
		,trans_date STRING
		,account_string STRING
		,run_date STRING
		,as_of_date STRING
		) STORED AS PARQUET LOCATION 'hdfs://nameservice1/sch/l0/material_transactions_dist';