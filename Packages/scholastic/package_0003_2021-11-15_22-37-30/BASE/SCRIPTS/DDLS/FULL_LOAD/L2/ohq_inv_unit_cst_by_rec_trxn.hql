DROP TABLE

IF EXISTS rlsf_prod.ohq_inv_unit_cst_by_rec_trxn;
	CREATE EXTERNAL TABLE rlsf_prod.ohq_inv_unit_cst_by_rec_trxn (
		transaction_inventory_item_id STRING
		,oracle_id STRING
		,transaction_inventory_org_id STRING
		,org_code STRING
		,cstlayer_cost_rec_trxnid STRING
		,cost_unit DECIMAL(38, 6)
		,transactions_creation_date STRING
		,cost_dist_line_cost_element_id STRING
		,cost_element_code STRING
		,dist_line_id STRING
		,cstlayer_cost_transaction_type STRING
		,po_number STRING
		) STORED AS PARQUET LOCATION 'hdfs://nameservice1/sch/l2/ohq_inv_unit_cst_by_rec_trxn';