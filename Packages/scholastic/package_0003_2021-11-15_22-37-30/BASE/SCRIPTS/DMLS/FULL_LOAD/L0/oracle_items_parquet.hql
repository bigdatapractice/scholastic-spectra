INVALIDATE METADATA rlsf_prod.oracle_items;

INSERT OVERWRITE TABLE rlsf_prod.oracle_items_parquet
SELECT *
FROM rlsf_prod.oracle_items;

TRUNCATE rlsf_prod.oracle_items;