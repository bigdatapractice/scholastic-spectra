invalidate metadata rlsf_prod.material_transactions;

INSERT INTO TABLE rlsf_prod.material_transactions_parquet
SELECT *
	,cast(from_utc_timestamp(now(),'America/New_York') AS string) AS run_date
FROM rlsf_prod.material_transactions;

TRUNCATE rlsf_prod.material_transactions;