refresh rlsf_prod.material_transactions_parquet;

INSERT overwrite TABLE rlsf_prod.material_transactions_dist
WITH lite AS (

WITH m_date as (select txn_id,max(run_date) as run_date from rlsf_prod.material_transactions_parquet group by txn_id)
		SELECT DISTINCT mm.txn_id
			,txn_qty
			,organization_id
			,item_id
			,transaction_date
			,imt_creation_date
			,last_update_date
			,source
			,legacy_transaction_type
			,legacy_reason
			,reference
			,source_line
			,transfer_organization_id
			,transaction_type_id
			,subinventory_code
			,distribution_account_id
			,m_date.run_date as run_date
		FROM rlsf_prod.material_transactions_parquet mm
		left join m_date on m_date.txn_id=mm.txn_id
		)
SELECT distinct txn_id
	,txn_qty
	,organization_id
	,org.oicode
	,org.oitype
	,item_id
	,itm.oracle_id
	,itm.isbn13
	,replace(itm.title, ',', ' ')
	,substr(transaction_date,1,10)
	,substr(imt_creation_date,1,10)
	,substr(imt_creation_date, 1, 10)
	,lite.last_update_date
	,source
	,legacy_transaction_type
	,legacy_reason
	,replace(reference, ',', ' ')
	,source_line
	,transfer_organization_id
	,CASE 
		WHEN transfer_organization_id = org1.oiorgid
			THEN org1.oicode
		END AS transfer_organization_code
	,lite.transaction_type_id
	,typ.transaction_type_name
	,CASE 
		WHEN lite.subinventory_code = 'MAIN'
			THEN 'Main'
		WHEN (
				lite.subinventory_code = ''
				OR lite.subinventory_code IS NULL
				)
			AND txn_id = sub.external_system_ref_id
			THEN sub.subinventory_code
		WHEN (
				lite.subinventory_code = ''
				OR lite.subinventory_code IS NULL
				)
			THEN 'Main'
		ELSE lite.subinventory_code
		END AS subinventory_code
	,substr(transaction_date, 1, 10) AS trans_date
	,CONCAT (
		segment1
		,'.'
		,segment2
		,'.'
		,segment3
		,'.'
		,segment4
		,'.'
		,segment5
		,'.'
		,segment6
		,'.'
		,segment7
		,'.'
		,segment8
		) AS Account_String
	,substr(run_date,1,10),substr(cast(from_utc_timestamp(now(),'America/New_York') AS string),1,16) as as_of_date
FROM lite
LEFT JOIN rlsf_prod.oracle_items_parquet itm ON lite.item_id = itm.inventory_item_id
LEFT JOIN rlsf_prod.org_master_parquet org ON lite.organization_id = org.oiorgid
LEFT JOIN rlsf_prod.org_master_parquet org1 ON lite.transfer_organization_id = org1.oiorgid
LEFT JOIN rlsf_prod.transaction_type typ ON lite.transaction_type_id = typ.transaction_type_id
LEFT JOIN rlsf_prod.cost_sub_inventory sub ON txn_id = sub.external_system_ref_id
LEFT JOIN (
	SELECT distinct *
	FROM rlsf_prod.sch_gl_code_combinations_rpt
	--WHERE creation_date <> ''
	) ccd ON trim(ccd.code_combination_id) = trim(distribution_account_id);

COMPUTE stats rlsf_prod.material_transactions_dist;