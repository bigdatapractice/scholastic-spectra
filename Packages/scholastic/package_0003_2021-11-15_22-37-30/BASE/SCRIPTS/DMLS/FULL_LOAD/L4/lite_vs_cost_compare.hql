refresh rlsf_prod.material_transactions_parquet;

WITH lite
AS (
	SELECT *
	FROM material_transactions_parquet
	WHERE TXN_ID <> 'TXN_ID'
	)
	,cost
AS (
	SELECT DISTINCT txn_external_system_ref_id
		,org_id
		,inventory_item_id
	FROM rlsf_prod.cost_trans_prod_allorgs_hist
	)
INSERT overwrite rlsf_prod.lite_vs_cost_compare
SELECT DISTINCT om.oicode
	,oi.oracle_id
	,CASE 
		WHEN oi.inventory_item_id = lite.item_id
			AND oi.org_id = lite.organization_id
			THEN oi.inventory_item_flag
		ELSE 'Y'
		END inventory_item_flag
	,CASE 
		WHEN oi.inventory_item_id = lite.item_id
			AND oi.org_id = lite.organization_id
			THEN oi.inventory_asset_flag
		ELSE 'Y'
		END inventory_asset_flag
	,CASE 
		WHEN oi.inventory_item_id = lite.item_id
			AND oi.org_id = lite.organization_id
			THEN oi.costing_enabled_flag
		ELSE 'Y'
		END costing_enabled_flag
	,cost.txn_external_system_ref_id
	,lite.txn_id
	,CASE 
		WHEN cost.txn_external_system_ref_id = lite.txn_id
			THEN 'Matched'
		ELSE 'Not Matched'
		END External_sys_ref_id_Status
	,lite.txn_qty
	,lite.organization_id
	,lite.item_id
	,lite.transaction_date
	,lite.imt_creation_date
	,lite.last_update_date
	,lite.source
	,lite.legacy_transaction_type
	,lite.legacy_reason
	,lite.source_line
	,lite.costed_flag
	,substr(cast(now() AS string), 1, 10) AS run_date
	,DATEDIFF(TO_DATE(substr(cast(now() AS string), 1, 10)), TO_DATE(lite.transaction_date)) AS days_diff
FROM lite
LEFT JOIN cost ON cost.txn_external_system_ref_id = lite.txn_id
	AND lite.organization_id = cost.org_id
	AND lite.item_id = cost.inventory_item_id
LEFT JOIN oracle_items_n oi ON oi.inventory_item_id = lite.item_id
	AND oi.org_id = lite.organization_id
LEFT JOIN org_master_parquet om ON om.oiorgid = lite.organization_id;