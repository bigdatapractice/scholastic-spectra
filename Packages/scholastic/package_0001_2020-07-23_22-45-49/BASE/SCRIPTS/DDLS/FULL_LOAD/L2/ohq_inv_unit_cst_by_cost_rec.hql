DROP TABLE IF EXISTS rlsf_prod.ohq_inv_unit_cst_by_cost_rec;
CREATE EXTERNAL TABLE rlsf_prod.ohq_inv_unit_cst_by_cost_rec (   transactionseoinventoryitemid STRING,
   transactionseoinventoryorgid STRING,
   cstlayercostspeorectrxnid STRING,
   cost_unit DECIMAL(38,6),  
 transactionseocreationdate STRING,
   costdistributionlineseocostelementid STRING,
   distributionlineid STRING,
   cstlayercostspeocosttransactiontype string ) STORED AS PARQUET LOCATION '/sch/l2/ohq_inv_unit_cst_by_cost_rec';
