INSERT INTO TABLE rlsf_prod.ohq_inv_qty_by_cost_rec
SELECT DISTINCT deb.transactionseoinventoryitemid
	,CASE 
		WHEN deb.cstlayercostspeorectrxnid = deb.cstlayercosttransid
			AND deb.cstlayercostspeocosttransactiontype = "RECEIPT"
			THEN cast(deb.costdistributionseolayerquantity AS DECIMAL(38, 6))
		WHEN deb.cstlayercostspeorectrxnid = deb.cstlayercosttransid
			AND deb.cstlayercostspeocosttransactiontype = "ISSUE"
			THEN 0
		ELSE cast(deb.costdistributionseolayerquantity AS DECIMAL(38, 6))
		END AS costdistributionseolayerquantity
	,deb.transactionseoinventoryorgid
	,deb.transactionseocreationdate AS transactionseocreationdate
	,deb.transactionseocstinvtransactionid
	,deb.cstlayercostspeorectrxnid
	,deb.txn_external_system_ref_id
	,deb.cstlayercosttransid
FROM rlsf_prod.cost_trans_debit deb
JOIN rlsf_prod.cost_trans_rank_assign assign ON assign.transactionseoinventoryitemid = deb.transactionseoinventoryitemid
	AND assign.ranknumber >= ${var:fromranknumber}
	AND assign.ranknumber <= ${var:toranknumber}
WHERE deb.costdistributionlineseocostelementid = '300000073384740'
	AND deb.cstlayercostspeocosttransactiontype <> 'ADJUST';

INSERT INTO TABLE rlsf_prod.ohq_inv_qty_by_cost_rec
SELECT DISTINCT deb.transactionseoinventoryitemid
	,cast(deb.costdistributionseolayerquantity AS DECIMAL(38, 6)) AS costdistributionseolayerquantity
	,deb.transactionseoinventoryorgid
	,deb.transactionseocreationdate AS transactionseocreationdate
	,deb.transactionseocstinvtransactionid
	,deb.cstlayercostspeorectrxnid
	,deb.txn_external_system_ref_id
	,deb.cstlayercosttransid
FROM rlsf_prod.cost_trans_debit deb left anti
JOIN rlsf_prod.ohq_inv_qty_by_cost_rec rcpt ON rcpt.cstlayercosttransid = deb.cstlayercosttransid
	AND rcpt.cstlayercostspeorectrxnid = deb.cstlayercostspeorectrxnid
JOIN rlsf_prod.cost_trans_rank_assign assign ON assign.transactionseoinventoryitemid = deb.transactionseoinventoryitemid
	AND assign.ranknumber >= ${var:fromranknumber}
	AND assign.ranknumber <= ${var:toranknumber}
WHERE deb.costdistributionlineseocostelementid = '300000073384740'
	AND deb.cstlayercostspeocosttransactiontype = 'ADJUST';