DROP TABLE

IF EXISTS rlsf_prod.oracle_items;
	CREATE EXTERNAL TABLE rlsf_prod.oracle_items (
		oracle_id String
		,title String
		,isbn13 String
		,inventory_item_flag String
		,item_type String
		,inventory_asset_flag String
		,costing_enabled_flag String
		,short_id String
		,mdm_id String
		,creation_dt String
		,inventory_item_id String
		,opus_num String
		,bf_num String
		,category_code String
		,cat_description String
		,catalog_code String
		) ROW FORMAT DELIMITED FIELDS TERMINATED BY '|' STORED AS TEXTFILE LOCATION 'hdfs://nameservice1/sch/l0/oracle_items';