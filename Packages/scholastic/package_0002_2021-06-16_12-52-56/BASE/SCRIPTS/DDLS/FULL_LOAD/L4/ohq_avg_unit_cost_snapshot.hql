DROP TABLE

IF EXISTS rlsf_prod.ohq_avg_unit_cost_snapshot;
	CREATE EXTERNAL TABLE rlsf_prod.ohq_avg_unit_cost_snapshot (
		cost_center STRING
		,oracle_item STRING
		,isbn13 STRING
		,title STRING
		,opus_id STRING
		,border_item STRING
		,inventory_item_id STRING
		,org_id STRING
		,total_amount DECIMAL(38,2)
		,total_qty bigint
		,avg_unit_cost DECIMAL(38,2)
		,as_of_date STRING
		,mdm_id STRING
		,short_id STRING
		,category STRING
		) STORED AS PARQUET LOCATION 'hdfs://nameservice1/sch/l4/ohq_avg_unit_cost_snapshot';