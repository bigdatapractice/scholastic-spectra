DROP TABLE

IF EXISTS rlsf_prod.ohq_po;
	CREATE EXTERNAL TABLE rlsf_prod.ohq_po (
		oracle_item STRING
		,isbn13 STRING
		,border_item STRING
		,opus_id STRING
		,cost_center STRING
		,org_id STRING
		,organization_name STRING
		,rec_trans_id STRING
		,inventory_item_id STRING
		,total_quantity bigint
		,unit_cost DECIMAL(38,2)
		,total_amount DECIMAL(38,2)
		,po_number STRING
		,transaction_type_name STRING
		,layer_date STRING
		,year_ STRING
		,month_ STRING
		,date_ STRING
		,run_date STRING
		,as_of_date STRING
		,category STRING
		) STORED AS PARQUET LOCATION 'hdfs://nameservice1/sch/l4/ohq_po';