DROP TABLE

IF EXISTS rlsf_prod.ohq_inv_as_of_date_with_layer_cost;
	CREATE EXTERNAL TABLE rlsf_prod.ohq_inv_as_of_date_with_layer_cost (
		oracle_item STRING
		,isbn13 STRING
		,border_item STRING
		,opus_id STRING
		,cost_center STRING
		,org_id STRING
		,organization_name STRING
		,lc_p_eo_rec_trans_id STRING
		,inventory_item_id STRING
		,total_qty DECIMAL(38, 6)
		,unit_cost DECIMAL(38, 6)
		,total_amount DECIMAL(38, 6)
		,layer_date STRING
		,year_ STRING
		,month_ STRING
		,date_ STRING
		,run_date STRING
		,as_of_date STRING
		) STORED AS PARQUET LOCATION 'hdfs://nameservice1/sch/l3/ohq_inv_as_of_date_with_layer_cost';