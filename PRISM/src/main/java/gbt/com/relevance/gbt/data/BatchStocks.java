package com.relevance.gbt.data;

public class BatchStocks {
	String client;
	String material;
	String plant;
	String storageLocation;
	String batch;
	String stockDeletionFlag;
	String createdOn;
	String createdBy;
	String lastChange;
	String changedBy;
	String yearCurrentPeriod;
	String currentPeriod;
	String physicalInventaryBlock;
	String quantity;
	String qualifier;
	String countryOfOrigin;
	String dateofLastCount;
	String chjin;
	String chrue;
	String warehouseStockCY;	
	String qualityInspectionStockCY;	
	String restrictedUseStock;	
	String blockedStock;
	String warehouseStockPY;	
	String qualInspStockPrvPD;
	String restrictedUsePP;
	String blckedStockPrevPD;
	String key;
	
	public String getKey() {
		return key;
	}
	public void setKey(String key) {
		this.key = key;
	}
	public String getClient() {
		return client;
	}
	public void setClient(String client) {
		this.client = client;
	}
	public String getMaterial() {
		return material;
	}
	public void setMaterial(String material) {
		this.material = material;
	}
	public String getPlant() {
		return plant;
	}
	public void setPlant(String plant) {
		this.plant = plant;
	}
	public String getStorageLocation() {
		return storageLocation;
	}
	public void setStorageLocation(String storageLocation) {
		this.storageLocation = storageLocation;
	}
	public String getBatch() {
		return batch;
	}
	public void setBatch(String batch) {
		this.batch = batch;
	}
	public String getStockDeletionFlag() {
		return stockDeletionFlag;
	}
	public void setStockDeletionFlag(String stockDeletionFlag) {
		this.stockDeletionFlag = stockDeletionFlag;
	}
	public String getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(String createdOn) {
		this.createdOn = createdOn;
	}
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	public String getLastChange() {
		return lastChange;
	}
	public void setLastChange(String lastChange) {
		this.lastChange = lastChange;
	}
	public String getChangedBy() {
		return changedBy;
	}
	public void setChangedBy(String changedBy) {
		this.changedBy = changedBy;
	}
	public String getYearCurrentPeriod() {
		return yearCurrentPeriod;
	}
	public void setYearCurrentPeriod(String yearCurrentPeriod) {
		this.yearCurrentPeriod = yearCurrentPeriod;
	}
	public String getCurrentPeriod() {
		return currentPeriod;
	}
	public void setCurrentPeriod(String currentPeriod) {
		this.currentPeriod = currentPeriod;
	}
	public String getPhysicalInventaryBlock() {
		return physicalInventaryBlock;
	}
	public void setPhysicalInventaryBlock(String physicalInventaryBlock) {
		this.physicalInventaryBlock = physicalInventaryBlock;
	}
	public String getQuantity() {
		return quantity;
	}
	public void setQuantity(String quantity) {
		this.quantity = quantity;
	}
	public String getQualifier() {
		return qualifier;
	}
	public void setQualifier(String qualifier) {
		this.qualifier = qualifier;
	}
	public String getCountryOfOrigin() {
		return countryOfOrigin;
	}
	public void setCountryOfOrigin(String countryOfOrigin) {
		this.countryOfOrigin = countryOfOrigin;
	}
	public String getDateofLastCount() {
		return dateofLastCount;
	}
	public void setDateofLastCount(String dateofLastCount) {
		this.dateofLastCount = dateofLastCount;
	}
	public String getChjin() {
		return chjin;
	}
	public void setChjin(String chjin) {
		this.chjin = chjin;
	}
	public String getChrue() {
		return chrue;
	}
	public void setChrue(String chrue) {
		this.chrue = chrue;
	}
	public String getWarehouseStockCY() {
		return warehouseStockCY;
	}
	public void setWarehouseStockCY(String warehouseStockCY) {
		this.warehouseStockCY = warehouseStockCY;
	}
	public String getQualityInspectionStockCY() {
		return qualityInspectionStockCY;
	}
	public void setQualityInspectionStockCY(String qualityInspectionStockCY) {
		this.qualityInspectionStockCY = qualityInspectionStockCY;
	}
	public String getRestrictedUseStock() {
		return restrictedUseStock;
	}
	public void setRestrictedUseStock(String restrictedUseStock) {
		this.restrictedUseStock = restrictedUseStock;
	}
	public String getBlockedStock() {
		return blockedStock;
	}
	public void setBlockedStock(String blockedStock) {
		this.blockedStock = blockedStock;
	}
	public String getWarehouseStockPY() {
		return warehouseStockPY;
	}
	public void setWarehouseStockPY(String warehouseStockPY) {
		this.warehouseStockPY = warehouseStockPY;
	}
	public String getQualInspStockPrvPD() {
		return qualInspStockPrvPD;
	}
	public void setQualInspStockPrvPD(String qualInspStockPrvPD) {
		this.qualInspStockPrvPD = qualInspStockPrvPD;
	}
	public String getRestrictedUsePP() {
		return restrictedUsePP;
	}
	public void setRestrictedUsePP(String restrictedUsePP) {
		this.restrictedUsePP = restrictedUsePP;
	}
	public String getBlckedStockPrevPD() {
		return blckedStockPrevPD;
	}
	public void setBlckedStockPrevPD(String blckedStockPrevPD) {
		this.blckedStockPrevPD = blckedStockPrevPD;
	}
	
}
