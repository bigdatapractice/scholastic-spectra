package com.relevance.gbt.data;

/**
 * 
 * E2emf GBT Pojo  
 *
 */
public class Makt {

	private String id;
	
	private String materialNum;
	
	private String materialDesc;

	public String getId() {
		return id;
	}

	public String getMaterialNum() {
		return materialNum;
	}

	public String getMaterialDesc() {
		return materialDesc;
	}

	public void setId(String id) {
		this.id = id;
	}

	public void setMaterialNum(String materialNum) {
		this.materialNum = materialNum;
	}

	public void setMaterialDesc(String materialDesc) {
		this.materialDesc = materialDesc;
	}
}
