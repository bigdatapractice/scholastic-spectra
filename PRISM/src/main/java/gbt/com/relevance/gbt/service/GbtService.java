package com.relevance.gbt.service;

import org.apache.solr.client.solrj.SolrServerException;
import org.apache.solr.client.solrj.impl.HttpSolrServer;
import org.apache.solr.client.solrj.response.FacetField;
import org.apache.solr.client.solrj.response.FacetField.Count;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.apache.solr.common.params.ModifiableSolrParams;
import org.json.JSONException;
import org.json.JSONObject;

import com.google.gson.Gson;
import com.relevance.gbt.util.GbtConstants;
import com.relevance.gbt.util.GbtLog;
import com.relevance.gbt.util.GbtSolrUtil;
import com.relevance.prism.data.SearchParam;
import com.relevance.prism.util.AppException;
import com.relevance.prism.util.E2emfConstants;
import com.relevance.prism.util.Log;
import com.relevance.prism.util.PrismDbUtil;

/**
 * @author 
 * GBT Search Service 
 *
 */
public class GbtService implements BatchGeneologyService {
	
	GbtSolrUtil appUtil;
	PrismDbUtil dbUtil;
	
	static String solarUrl = "SOLR_SERVER_URL";
	static String solarInstanceName = "SOLR_SERVER_INSTANCE_NAME";
	
	static String solarServerInsanceUrl = null;
	private String searchType;

	public GbtService() {

		appUtil = new GbtSolrUtil();
		dbUtil = new PrismDbUtil();
	}

	
	public String searchSolar(Object... obj) throws AppException{
		String searchResultJson = null;
		HttpSolrServer solarServer = null;
		QueryResponse queryResponse = null;
		
		SearchParam searchParam = null;
		String searchCriteria = null;
		searchType = appUtil.getAppProperty(E2emfConstants.gbtSearchType);
		boolean searchOutputColumns = true;
		try
		{
			if(obj != null && obj.length >1)
			{
				searchParam = (SearchParam)obj[0];
				searchCriteria = ((String) obj[1]).trim();
				
				if (obj.length >= 3 && obj[2] != null)
				{
					searchOutputColumns = Boolean.parseBoolean((String)obj[2]);
				}
				Log.Info("Searching initiated for " + searchCriteria);
			}
			
			StringBuilder searchStr = new StringBuilder();
			  
			 
			 if ("1".equalsIgnoreCase(searchType))
			 {
				 searchStr.append("rawdata:*");
				 searchStr.append(searchParam.getKey().replaceAll(" ", "*"));
				 searchStr.append("*");
			 }
			 else if (searchOutputColumns)
			  {
				 //Replace Special character like - with "+" with AND condition 
				 String searchWords = searchParam.getKey();
				 searchWords = searchWords.replaceAll("-", "+");
				 
				  //parameters.set("q", "output:*" + searchParam.getKey().replaceAll(" ", "* OR output:*") + "*");
				  searchStr.append("output:");
				  searchStr.append("*");
				  searchStr.append(searchWords.replaceAll(" ", "* OR output:*"));
				  searchStr.append("*");
				  searchStr = new StringBuilder(searchStr.toString().replaceAll("\\+", "* AND output:*"));
			  }
			  else
			  {
				  //parameters.set("q", "rawdata:*" + searchParam.getKey().replaceAll(" ", "* OR rawdata:*") + "*");//Search only in all data column
				  searchStr.append("rawdata:");
				  searchStr.append("\"");
				  searchStr.append(searchParam.getKey().replaceAll(" ", "\" OR rawdata:\""));
				  searchStr.append("\"");
				  searchStr = new StringBuilder(searchStr.toString().replaceAll("\\+", "\" AND rawdata:\""));
			  }
			 
			if(searchCriteria != null && searchCriteria.equalsIgnoreCase("searchService") && searchParam.getFacet() == null) {
				
				 solarServer = getSolarServerConnection(searchParam.getSource());
				 
				  ModifiableSolrParams parameters = new ModifiableSolrParams();	 
				  //parameters.set("q", "rawdata:\"" + searchParam.getKey() + "\"");
				  parameters.set("q", searchStr.toString());
				  parameters.set("defType", "edismax");	
				  //parameters.set("wt", "xml");//Not required since converting from the object directly
				  parameters.set("start", "0");
				  parameters.set("rows", "0");
				  parameters.set("qf", "rawdata rank^2 keywords");
				  parameters.set("facet", "true");
				  parameters.set("facet.field", "facet");
				  parameters.set("facet.sort", "rank+asc");
				  //parameters.set("fq", "facet:"+queryFilterVal);	 
	
				  try {
					  if(solarServer != null){
						  queryResponse = solarServer.query(parameters);  
						  Log.Info("Recieved response from solar successfully "  );
					  }else{
						  Log.Error("Unable to connect to solarServer...");
					  }
					  			
					  Gson gson = new Gson();
					  java.util.LinkedHashMap<String, String> facetMap = new java.util.LinkedHashMap<String, String>();
					  
					  //First for loop is for fieldname as "facet"
					  if (queryResponse != null && queryResponse.getFacetFields() != null)
					  {
						  for(FacetField facetField : queryResponse.getFacetFields())
						  {
								  for(Count facetFieldCount : facetField.getValues())
								  {
									  facetMap.put(facetFieldCount.getName(), "" + facetFieldCount.getCount());
								  }
						  }
					  }
					  String json = gson.toJson(facetMap);
					  searchResultJson = json;
				  }
				  catch (SolrServerException e) {					  
					  	Log.Error("Exception in getting Solar Server Connection." + e.getStackTrace());
						Log.Error(e);
						e.printStackTrace();
						throw new AppException(e.getMessage(),"Error while processing the request.", 2 ,e.getCause(), true);
	
				  }
				  catch(Exception e){
						Log.Error("Exception Fetching JSON Data." + e);
						if(e instanceof NullPointerException){
							throw new AppException("NullPointerException","No Indexed Data Available in Collection.", 3 ,e.getCause(), true);
						}
						throw new AppException(e.getMessage(),"Error while processing the request.", 2 ,e.getCause(), true);					 
				}
	
				
			}
			//To implement detailed search	
			  else if(searchCriteria != null && searchCriteria.equalsIgnoreCase("searchService")  && searchParam.getFacet() != null) {
					
					 
					 solarServer = getSolarServerConnection(searchParam.getSource());
					   
				
					  ModifiableSolrParams parameters = new ModifiableSolrParams();	 
					  //parameters.set("q", "rawdata:\"" + searchParam.getKey().replaceAll(" ", "*") + "*");
					  //parameters.set("q", "rawdata:\"" + searchParam.getKey() + "\"");
					  parameters.set("q", searchStr.toString());
					  parameters.set("defType", "edismax");
					  //parameters.set("wt", "xml");//Not required since converting from the object directly
					  parameters.set("start", "0");
					  parameters.set("rows", "50");
					  parameters.set("fl", "output,id");
					  parameters.set("qf", "rawdata rank^2 keywords");
					  parameters.set("fq", "facet:" + searchParam.getFacet());	
					   
					  //new collection type settings
					  //parameters.set("group","true")
					  //parameters.set("group.field", "key");
					  //parameters.set("group.main", "true");

					  try {
						  if(solarServer != null){
							  queryResponse = solarServer.query(parameters);  
							  Log.Info("Recieved response from solar successfully "  );
						  }else{
							  Log.Error("Unable to connect to solarServer...");
						  }
						  			

						  Gson gson = new Gson();
						  String json = gson.toJson(queryResponse.getResults());
						 						  
						  searchResultJson = parseJSONStr(json);
						  //System.out.println("parsedJSONOutPut:" + searchResultJson);
						  //Log.Info("parsedJSONOutPut:" +searchResultJson);
						  
					  }
					  catch (SolrServerException e) {					  
						  	Log.Error("Exception in getting Solar Server Connection." + e.getStackTrace());
							Log.Error(e);
							e.printStackTrace();
							throw new AppException(e.getMessage(),"Error while processing the request.", 2 ,e.getCause(), true);

					  }
					  catch(Exception e){
							Log.Error("Exception Fetching Search Details." + e);
							if(e instanceof NullPointerException){
								throw new AppException("NullPointerException","No Indexed Data Available in Collection.", 3 ,e.getCause(), true);
							}
							throw new AppException(e.getMessage(),"Error while processing the request.", 2 ,e.getCause(), true);					 
					}
				}		
		
			}
		  catch(Exception e){
				Log.Error("Exception Fetching GBT JSON Data." + e);
				if(e instanceof NullPointerException){
					throw new AppException("NullPointerException","No Indexed Data Available in Collection.", 3 ,e.getCause(), true);
				}
				throw new AppException(e.getMessage(),"Error while processing the request.", 2 ,e.getCause(), true);					 
		}
		return searchResultJson;

	}
	private String parseJSONStr(String jsonStr) throws AppException{

		org.json.JSONArray jsonArr = null;
		String outputJSONStr = "";
		//List<String> outputList =  new ArrayList<String>();
		
		try {
			
			jsonArr = new org.json.JSONArray(jsonStr);
			int length = jsonArr.length();
			//System.out.println("length:" + length);
			outputJSONStr = "[";

			for(int i=0 ;i<length ;i++){			
				JSONObject jsonObj = (JSONObject)jsonArr.get(i);	
				outputJSONStr = outputJSONStr.concat("{\"key\":\"").concat(jsonObj.getString("id").concat("\","));
				outputJSONStr = outputJSONStr.concat(jsonObj.getString("output").substring(1));
				if(i != length-1){
					outputJSONStr = outputJSONStr.concat(",");
				}				
			}
			outputJSONStr = outputJSONStr.concat("]");
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			GbtLog.Error(e);		
		}
		catch(Exception e){
			Log.Error("Exception parsing the JSON String." + e);
			if(e instanceof NullPointerException){
				throw new AppException("NullPointerException","No Indexed Data Available in Collection.", 3 ,e.getCause(), true);
			}
			throw new AppException(e.getMessage(),"Error while processing the request.", 2 ,e.getCause(), true);					 
		}
		return outputJSONStr;
	}
	
	
	private HttpSolrServer getSolarServerConnection(String Source) throws AppException{
		HttpSolrServer serverCon = null;
		String solarInstanceName= null;
		GbtSolrUtil gbtSolrUtil = new GbtSolrUtil();
		if (Source.equalsIgnoreCase("slr"))
			solarInstanceName = GbtConstants.gbtSLRSolarInstance;
		else if (Source.equalsIgnoreCase("alt"))				
			solarInstanceName = GbtConstants.gbtALTSolarInstance;
				if(solarInstanceName!=null)
					solarInstanceName = gbtSolrUtil.getAppProperty(solarInstanceName);
		try {		  
			serverCon = new HttpSolrServer(solarInstanceName);
		  } catch(Exception e){
				Log.Error("Exception connecting to solr server." + e);
				if(e instanceof NullPointerException){
					throw new AppException("NullPointerException","No Indexed Data Available in Collection.", 3 ,e.getCause(), true);
				}
				throw new AppException(e.getMessage(),"Error while processing the request.", 2 ,e.getCause(), true);					 
			}
		
		return serverCon;
	}

	@Override
	public Object fetchBatchGeneology(Object... obj) {
		
		return null;
	}
	
}
