package com.relevance.search.data;

public class SolrMaster {
	public String key;
	public String components;
	public String docnum;
	public String manufacturer;
	public String relateditems;
	public String source;
	public String title;
	public String userupdated;
	public String smallimagepath;
	public String imagepath;
	public String filepath;
	public String cspeciality;
	public String categoryl0;
	public String lables;
	public String rank;
	public String date_1;
	public String imageset;
	public String partnumber;
	public String finished_goods_number;
	public String component_number;
	public String deleted;
	public String notes;
	public String revision;
	public String clicked;
	public String downloaded;
	public String emailed;
	
	public String getKey() {
		return key;
	}
	public void setKey(String key) {
		this.key = key;
	}
	public String getComponents() {
		return components;
	}
	public void setComponents(String components) {
		this.components = components;
	}
	public String getDocnum() {
		return docnum;
	}
	public void setDocnum(String docnum) {
		this.docnum = docnum;
	}
	public String getManufacturer() {
		return manufacturer;
	}
	public void setManufacturer(String manufacturer) {
		this.manufacturer = manufacturer;
	}
	public String getRelateditems() {
		return relateditems;
	}
	public void setRelateditems(String relateditems) {
		this.relateditems = relateditems;
	}
	public String getSource() {
		return source;
	}
	public void setSource(String source) {
		this.source = source;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getUserupdated() {
		return userupdated;
	}
	public void setUserupdated(String userupdated) {
		this.userupdated = userupdated;
	}
	public String getSmallimagepath() {
		return smallimagepath;
	}
	public void setSmallimagepath(String smallimagepath) {
		this.smallimagepath = smallimagepath;
	}
	public String getImagepath() {
		return imagepath;
	}
	public void setImagepath(String imagepath) {
		this.imagepath = imagepath;
	}
	public String getFilepath() {
		return filepath;
	}
	public void setFilepath(String filepath) {
		this.filepath = filepath;
	}
	public String getCspeciality() {
		return cspeciality;
	}
	public void setCspeciality(String cspeciality) {
		this.cspeciality = cspeciality;
	}
	public String getCategoryl0() {
		return categoryl0;
	}
	public void setCategoryl0(String categoryl0) {
		this.categoryl0 = categoryl0;
	}
	public String getLables() {
		return lables;
	}
	public void setLables(String lables) {
		this.lables = lables;
	}
	public String getRank() {
		return rank;
	}
	public void setRank(String rank) {
		this.rank = rank;
	}
	public String getDate_1() {
		return date_1;
	}
	public void setDate_1(String date_1) {
		this.date_1 = date_1;
	}
	public String getImageset() {
		return imageset;
	}
	public void setImageset(String imageset) {
		this.imageset = imageset;
	}
	public String getPartnumber() {
		return partnumber;
	}
	public void setPartnumber(String partnumber) {
		this.partnumber = partnumber;
	}
	public String getFinished_goods_number() {
		return finished_goods_number;
	}
	public void setFinished_goods_number(String finished_goods_number) {
		this.finished_goods_number = finished_goods_number;
	}
	public String getComponent_number() {
		return component_number;
	}
	public void setComponent_number(String component_number) {
		this.component_number = component_number;
	}
	public String getDeleted() {
		return deleted;
	}
	public void setDeleted(String deleted) {
		this.deleted = deleted;
	}
	public String getNotes() {
		return notes;
	}
	public void setNotes(String notes) {
		this.notes = notes;
	}
	public String getRevision() {
		return revision;
	}
	public void setRevision(String revision) {
		this.revision = revision;
	}
	public String getClicked() {
		return clicked;
	}
	public void setClicked(String clicked) {
		this.clicked = clicked;
	}
	public String getDownloaded() {
		return downloaded;
	}
	public void setDownloaded(String downloaded) {
		this.downloaded = downloaded;
	}
	public String getEmailed() {
		return emailed;
	}
	public void setEmailed(String emailed) {
		this.emailed = emailed;
	}
	
	public String toString()
	{
		return   "Key:"+key+"Components:"+components+"DocNum:"+docnum+"Manufacture:"+manufacturer+
				  "Related Items :"+relateditems+"Source :"+source+"Tittle :"+title+"User Updated :"+userupdated+
				  "SmallImage Path :"+smallimagepath+"Image Path :"+imagepath+"File Path :"+filepath+
				  "Cspeciality :"+cspeciality+"Catagory10 :"+categoryl0+"Lables :"+lables+"Rank :"+rank+
				  "Date_1 :"+date_1+"ImageSet :"+imageset+"PartNumber :"+partnumber+"Finished_Good :"+finished_goods_number+
				  "Component_number :"+component_number+"Deleted :"+deleted+"Notes :"+notes+
				  "Revision :"+revision+"Clicked :"+clicked+"Downloaded :"+downloaded+"Emailed :"+emailed;
	}

}
