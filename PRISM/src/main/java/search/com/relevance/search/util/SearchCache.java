package com.relevance.search.util;

import java.util.HashMap;

public class SearchCache {
	
	/**
	 * A Singleton used for Caching.
	 * Created on :  05-Oct-2016
	 */	
	
	private SearchCache() {
		//Avoiding Initialization.
	}
	
	public static HashMap<String, String> QueriesCache;
	public static HashMap<String, String> JsonResultsCache;
	
	static {
		QueriesCache = new HashMap<>();
		JsonResultsCache = new HashMap<>();
	}
}
