package com.relevance.search.data;

import java.util.ArrayList;

import com.relevance.prism.data.E2emfBusinessObject;

/**
 * 
 * E2emf Search Business Object  
 *
 */
public class QueryParam extends E2emfBusinessObject
{

	private String facet;
	private String start;
	private String rows;
	private String source;
	private ArrayList<QueryItem> queryItems;
	private String condition;
	private String outputField;
	
	public String getOutputField() {
		return outputField;
	}
	public void setOutputField(String outputField) {
		this.outputField = outputField;
	}
	public String getCondition() {
		return condition;
	}
	public void setCondition(String condition) {
		this.condition = condition;
	}
	public ArrayList<QueryItem> getQueryItems() {
		return queryItems;
	}
	public void setQueryItems(ArrayList<QueryItem> queryItems) {
		this.queryItems = queryItems;
	}
	public String getSource() {
		return source;
	}
	public void setSource(String source) {
		this.source = source;
	}
	public String getFacet() {
		return facet;
	}
	public void setFacet(String facet) {
		this.facet = facet;
	}
	public String getStart() {
		return start;
	}
	public void setStart(String start) {
		this.start = start;
	}
	public String getRows() {
		return rows;
	}
	public void setRows(String rows) {
		this.rows = rows;
	}
}
