package com.relevance.search.rest;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import org.apache.commons.lang3.StringUtils;
import org.json.JSONObject;

import com.google.gson.Gson;
import com.relevance.prism.data.SearchLog;
import com.relevance.prism.data.SearchParam;
import com.relevance.prism.data.SecondaryRole;
import com.relevance.prism.data.Shortlist;
import com.relevance.prism.rest.BaseResource;
import com.relevance.prism.service.UserService;
import com.relevance.prism.util.AppException;
import com.relevance.prism.util.DataObfuscator;
import com.relevance.prism.util.E2emfAppUtil;
import com.relevance.prism.util.E2emfConstants;
import com.relevance.prism.util.Log;
import com.relevance.prism.util.PrismHandler;
import com.relevance.search.data.SolrConf;
import com.relevance.search.data.SolrField;
import com.relevance.search.service.InstrumentCatalogServiceLocator;
import com.relevance.search.service.InstrumentSearchService;
import com.relevance.search.service.SearchService;


@Path("/search")
@Consumes("application/javascript")
public class SearchResource extends BaseResource {
	
	@Context
	ServletContext context;

	public SearchResource(@Context ServletContext value) {
		this.context = value;
	}
	
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Path("/searchservice/")
	public String searchData(@FormParam("key") String key,
			@FormParam("facet") String facet,
			@FormParam("start") String start,
			@FormParam("rows") String rows,
			@FormParam("type") String type,
			@FormParam("searchInDisplaycolumns") String searchInDisplaycolumns,
			@FormParam("solrcollectionname") String solrcollectionname) throws AppException{
		long startTime = System.currentTimeMillis();
		long endTime = 0L;

		Log.Info("Recieved Request searchservice with param : " + key);

		String searchResults = null;

		try {

			SearchService service = new SearchService();

			Log.Info("Search Param recieved is " + key);
			SearchParam searchParam = new  SearchParam();
			searchParam.setKey(DataObfuscator.deObfuscateToken(key));
			searchParam.setFacet(facet);
			searchParam.setStart(start);
			searchParam.setRows(rows);
			searchParam.setType(type);
			searchParam.setSolrCollectionName(solrcollectionname);
			String obfuscateFlag = E2emfAppUtil.getAppProperty(E2emfConstants.DATA_OBFUSCATION);
			searchResults = (String) service.getJsonObject(searchParam,
					"searchService",searchInDisplaycolumns, obfuscateFlag);
			
			searchResults = DataObfuscator.obfuscate(searchResults);
			endTime = System.currentTimeMillis();
		} catch(AppException appe){
			Log.Error(appe);
			return appe.toString();
		} catch (Exception e) {
			Log.Error("Exception in searchService " + e);
		}
		Log.Info("Returning Search Results to Presentation, total Time taken is "
				+ E2emfAppUtil.getTotalElapsedTime(startTime, endTime)
				+ " seconds");
		return searchResults;
	}
	
	
	@SuppressWarnings("unchecked")
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Path("/solrSearch/")
	public String searchData(@FormParam("query") String query,
			@FormParam("solrcollectionname") String solrcollectionname,
			@FormParam("profileId") String profileId,
			@FormParam("storeHistory") boolean storeHistory,
			@FormParam("row") String row,
			@FormParam("noRow") String noRows,
			@FormParam("overRideParams") String overRideParams,
			@FormParam("enableSC") String enableSC,
			@FormParam("moduleName") String moduleName,
			@FormParam("profileName") String profileName,
			@FormParam("secondaryTabs") String secondaryTabs,
			@FormParam("overRideMap") String overRideMap,
			@Context HttpServletRequest req) throws AppException{
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());		
		String  response = null;
		String searchResults;
		String userEmail;
		List<SecondaryRole> secondaryRoleList = null;
		Boolean scEnabled;
		try {
			HttpSession session = req.getSession(true);
			if (session.getAttribute("secondaryRoleList") == null){
				userEmail = session.getAttribute("UserEmail").toString();
				UserService userService = new UserService();
				secondaryRoleList = userService.getSecondaryRoles(userEmail);
				session.setAttribute("secondaryRoleList",secondaryRoleList);
			}else{
				secondaryRoleList = (List<SecondaryRole>) session.getAttribute("secondaryRoleList");
			}
			SearchService service = new SearchService();
			query = query == null ? "" : query;
			solrcollectionname = solrcollectionname == null ? "" : solrcollectionname;
			profileId = profileId == null ? "" : profileId;
			profileName = profileName == null ? "" : profileName;
			row = row == null ? "" : row;
			overRideParams = (overRideParams == null) ? "" : overRideParams;			
			if(enableSC != null)
				scEnabled = "true".equalsIgnoreCase(enableSC) ? true : false;
			else
				scEnabled = false;
			
			Gson gson = new Gson();	
			
			HashMap<String, String> hmap = gson.fromJson(overRideMap, HashMap.class);
			
			searchResults = service.getSearchResult(query, solrcollectionname, profileId, row, overRideParams,secondaryRoleList,scEnabled,profileName,secondaryTabs, session, noRows, hmap);
			SearchLog searchLog = new SearchLog();
			if(storeHistory) {
				searchLog.setUserID(session.getAttribute("userName").toString());
				searchLog.setUserName(session.getAttribute("userDisplayName").toString());
				searchLog.setJsonQuery(query);
				searchLog.setType("History");
				searchLog.setDisplayQuery(query);
				searchLog.setModule(moduleName);
				searchLog.setBookmarkName(query);
				E2emfAppUtil.logSearchQuery(searchLog);	
			}			
			response = gson.toJson(searchResults);
			response = DataObfuscator.obfuscate(response);
		} catch(AppException appe){
			Log.Error(appe);
			return appe.toString();
		} catch (Exception e) {
			Log.Error("Exception in searchService " + e);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return response;
	}
	
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Path("/shortlist/")
	public String shortlist(@FormParam("solrId") String id,
			@FormParam("shortlistall") String shortlistall,
			@FormParam("moduleName") String appName,
			@Context HttpServletRequest req) throws AppException {
		long startTime = System.currentTimeMillis();
		long endTime = 0L;

		Log.Info("Recieved Request searchservice with param : " + id);

		String searchResults = null;
		String userName = null;
		String userId = null;

		try {
			Log.Info("Search Param recieved is " + id);
			HttpSession session = req.getSession(true);
			userId = session.getAttribute("userName").toString();
			userName = session.getAttribute("userDisplayName").toString();
			if(appName == null || StringUtils.isBlank(appName)) {
				throw new Exception("No App Name present");
			}
			if(id == null || StringUtils.isBlank(id)) {
				throw new Exception("No Id present");
			}
			String[] ids = id.split(",");
			for(String no : ids) {
				E2emfAppUtil.shortlist(userId, userName, no, appName, shortlistall);
			}
			
			endTime = System.currentTimeMillis();			
			searchResults = "{\"message\" : true}";
		} catch (AppException appe) {
			Log.Error(appe);
			return appe.toString();
		} catch (Exception e) {
			Log.Error("Exception in searchService " + e);
		}
		Log.Info("Returning Search Results to Presentation, total Time taken is "
				+ E2emfAppUtil.getTotalElapsedTime(startTime, endTime)
				+ " seconds");
		return searchResults;
	}
	
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Path("/getshortlist/")
	public String getshortlist(@FormParam("module") String module,
			@Context HttpServletRequest req) throws AppException {
		long startTime = System.currentTimeMillis();
		long endTime = 0L;

		Log.Info("Recieved Request searchservice with param : " + module);

		String searchResults = null;
		String userName = null;
		String userId = null;

		try {
			Log.Info("Search Param recieved is " + module);
			HttpSession session = req.getSession(true);
			userId = session.getAttribute("userName").toString();
			userName = session.getAttribute("userDisplayName").toString();
			if(StringUtils.isBlank(module)) {
				throw new Exception("No Module found."); 
			}
			List<Shortlist> shortlistsArray = E2emfAppUtil.getShortlist(userId,
					userName, module);
			Gson gson = new Gson();
			searchResults = gson.toJson(shortlistsArray);
			endTime = System.currentTimeMillis();
		} catch (AppException appe) {
			Log.Error(appe);
			return appe.toString();
		} catch (Exception e) {
			Log.Error("Exception in searchService " + e);
		}
		Log.Info("Returning Search Results to Presentation, total Time taken is "
				+ E2emfAppUtil.getTotalElapsedTime(startTime, endTime)
				+ " seconds");
		return searchResults;
	}
	
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Path("/updateQueryCache/")
	public String updateCache(@FormParam("id") String id,
			@FormParam("name") String name,
			@FormParam("query") String query) throws AppException{
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		String  response = null;
		try{
			SearchService service = new SearchService();
			if(id == null) {
				throw new Exception("No Query Id Specified");
			}
			if(query == null) {
				throw new Exception("No Query Specified");
			}
			boolean message = service.updateCache(id, name, query);
			response = "{ \"result\" : "+message+"}";			
		}catch(AppException appe){
			Log.Error(appe);
			return appe.toString();
		} catch (Exception e) {
			Log.Error("Exception in searchService " + e);
		}
		
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return response;
	}
	
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Path("/markSearch/")
	public String bookmarksearch(@FormParam("key") String key,			
			@FormParam("bookmarkname") String bookmarkName,
			@FormParam("bookmarkDesc") String bookmarkDesc,
			@FormParam("moduleName") String moduleName,
			@FormParam("type") String type,
			@Context HttpServletRequest req) throws Exception {
		Log.Info("Recieved Request searchservice with param : " + key);

		String searchResults;


		SearchLog searchLog = new SearchLog();
		HttpSession session = req.getSession(true);
		searchLog.setUserID(session.getAttribute("userName").toString());
		searchLog.setUserName(session.getAttribute("userDisplayName").toString());
		searchLog.setJsonQuery(bookmarkDesc);
		searchLog.setDisplayQuery(key);
		searchLog.setType(type);
		searchLog.setModule(moduleName);
		searchLog.setBookmarkName(bookmarkName);
		searchResults = E2emfAppUtil.logSearchQuery(searchLog);

		return searchResults;
	}
	
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Path("/getSearchLogs")
	public String getSearchHisory(@FormParam("type") String type,
			@FormParam("moduleName") String moduleName,
			@FormParam("orderBy") String orderBy,
			@FormParam("orderStyle") String orderStyle,
			@Context HttpServletRequest req) {
		String json = null;
		SearchLog searchQuery = new SearchLog();
		List<SearchLog> searchHistory = new ArrayList<SearchLog>();
		try {
			HttpSession session = req.getSession(true);
			searchQuery.setUserID(session.getAttribute("userName").toString());
			searchQuery.setType(type);
			searchQuery.setModule(moduleName);
			SearchService service = new SearchService();
			searchHistory = service.getSearchHistory(searchQuery, orderBy, orderStyle);
			Gson gson = new Gson();
			json = gson.toJson(searchHistory);
		} catch (AppException appe) {
			Log.Error(appe);
			return appe.toString();
		} catch (Exception ex) {
			Log.Error("Exception while retrieving the user list "
					+ ex);
			String failureMessage = "{\"message\":\"failure\"}";
			return failureMessage;
		}
		return json;
	}
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/searchSuggestions")
	public String getSearchSuggestions(@javax.ws.rs.QueryParam("type") String varX,@javax.ws.rs.QueryParam("query") String query, @javax.ws.rs.QueryParam("solrcollectionname") String solrcollectionname) {

		long startTime = System.currentTimeMillis();
		long endTime = 0L;

		Log.Info("Recieved Slob searchMasterDatanew Request with param " + varX	+ " searchParam " + query);

		String masterDataJson = null;

		try {
			SearchParam searchParam = new  SearchParam();
			searchParam.setKey(query);
			searchParam.setSolrCollectionName(solrcollectionname);
			SearchService searchService = new SearchService();
			masterDataJson = searchService.getSearchSuggestion(searchParam);

			endTime = System.currentTimeMillis();

		} catch (AppException appe) {
			Log.Error(appe);
			return appe.toString();
		} catch (Exception e) {
			Log.Error("Exception getSlobMasterData() of the MasterData with queryParams "
					+ e);
		}

		Log.Info("Returning Slob MasterData Json to presentation, total Time taken is "	+ E2emfAppUtil.getTotalElapsedTime(startTime, endTime));

		return masterDataJson;
	}
	
	
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Path("/getSolrSchema")
	public String getSolrSchema(@FormParam("solrCollectionName") String solrCollectionName) {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());		
		String response = null;
		SearchService service;
		ArrayList<SolrField> serviceResponse;
		try {
			service = new SearchService();	
			serviceResponse = service.getSolrSchema(solrCollectionName);
			Gson gson = new Gson();			
			response = gson.toJson(serviceResponse.toArray()); 
		} catch (Exception ex) {
			response = PrismHandler.handleException(ex);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return response;
	}	
	
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Path("/indexSolr")
	public String indexSolr(@FormParam("config") String json, @FormParam("filterColumn") String filterJson) {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		String response = null;
		SolrConf solrConf;
		String filterColName = null;
		String filterColType = null;
		String filterColValue = null;
		try {
			Gson gson = new Gson();
			solrConf = gson.fromJson(json, SolrConf.class);
			if(filterJson != null && !filterJson.isEmpty()) {
				JSONObject jsonObj = new JSONObject(filterJson);
				filterColName = jsonObj.getString("columnName");
				filterColType = jsonObj.getString("columnType");
				filterColValue = jsonObj.getString("columnValue");
			}
			 
			SearchService searchService = new SearchService();
			response = searchService.indexSolr(solrConf, filterColName, filterColType, filterColValue);	
		} catch (Exception ex) {
			response = PrismHandler.handleException(ex);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return response;
	}
	
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Path("/deleteSearchLog")
	public String indexSolr(@FormParam("id") String id) {
		String responseMessage = null;
		int logId;
		boolean isDeleted = false;
		try{
			logId = Integer.parseInt(id);
			SearchService service = new SearchService();
			isDeleted = service.deleteSearchLog(logId);
			responseMessage = "{ \"result\": "+isDeleted+" }";			
		} catch (Exception ex) {
			responseMessage = PrismHandler.handleException(ex);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return responseMessage;
	}
}