package com.relevance.search.data;

public class SmartFindDocument {

	private String key;

	private String id;

	private String originalTitle;

	private String correctedTitle;

	private String components;

	private String docnum;

	private String manufacturer;

	private String relateditems;

	private String source;

	private String title;

	private String userupdated;

	private String smallimagepath;

	private String imagepath;

	private String filepath;

	private String cspeciality;

	private String categoryl0;

	private String lables;

	private String rank;

	private String date_1;

	private String deleted;

	private String partnumber;

	private String finishedGoodsNumber;

	private String componentNumber;

	private String notes;

	private String imageset;

	private Long clicked;

	private Long downloaded;

	private Long emailed;

	private String userLastUpdated;

	private String revision;

	private String mLocation;

	private String datetimestamp;
	private String pngimage;
	private String jpgimage;
	private String userwhoupdated;
	private String pages;
	private String docnumOnly;

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getOriginalTitle() {
		return originalTitle;
	}

	public void setOriginalTitle(String originalTitle) {
		this.originalTitle = originalTitle;
	}

	public String getCorrectedTitle() {
		return correctedTitle;
	}

	public void setCorrectedTitle(String correctedTitle) {
		this.correctedTitle = correctedTitle;
	}

	public String getComponents() {
		return components;
	}

	public void setComponents(String components) {
		this.components = components;
	}

	public String getDocnum() {
		return docnum;
	}

	public void setDocnum(String docnum) {
		this.docnum = docnum;
	}

	public String getManufacturer() {
		return manufacturer;
	}

	public void setManufacturer(String manufacturer) {
		this.manufacturer = manufacturer;
	}

	public String getRelateditems() {
		return relateditems;
	}

	public void setRelateditems(String relateditems) {
		this.relateditems = relateditems;
	}

	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getUserupdated() {
		return userupdated;
	}

	public void setUserupdated(String userupdated) {
		this.userupdated = userupdated;
	}

	public String getSmallimagepath() {
		return smallimagepath;
	}

	public void setSmallimagepath(String smallimagepath) {
		this.smallimagepath = smallimagepath;
	}

	public String getImagepath() {
		return imagepath;
	}

	public void setImagepath(String imagepath) {
		this.imagepath = imagepath;
	}

	public String getFilepath() {
		return filepath;
	}

	public void setFilepath(String filepath) {
		this.filepath = filepath;
	}

	public String getCspeciality() {
		return cspeciality;
	}

	public void setCspeciality(String cspeciality) {
		this.cspeciality = cspeciality;
	}

	public String getCategoryl0() {
		return categoryl0;
	}

	public void setCategoryl0(String categoryl0) {
		this.categoryl0 = categoryl0;
	}

	public String getLables() {
		return lables;
	}

	public void setLables(String lables) {
		this.lables = lables;
	}

	public String getRank() {
		return rank;
	}

	public void setRank(String rank) {
		this.rank = rank;
	}

	public String getDate_1() {
		return date_1;
	}

	public void setDate_1(String date_1) {
		this.date_1 = date_1;
	}

	public String getDeleted() {
		return deleted;
	}

	public void setDeleted(String deleted) {
		this.deleted = deleted;
	}

	public String getPartnumber() {
		return partnumber;
	}

	public void setPartnumber(String partnumber) {
		this.partnumber = partnumber;
	}

	public String getFinishedGoodsNumber() {
		return finishedGoodsNumber;
	}

	public void setFinishedGoodsNumber(String finishedGoodsNumber) {
		this.finishedGoodsNumber = finishedGoodsNumber;
	}

	public String getComponentNumber() {
		return componentNumber;
	}

	public void setComponentNumber(String componentNumber) {
		this.componentNumber = componentNumber;
	}

	public String getNotes() {
		return notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}

	public String getImageset() {
		return imageset;
	}

	public void setImageset(String imageset) {
		this.imageset = imageset;
	}

	public Long getClicked() {
		return clicked;
	}

	public void setClicked(Long clicked) {
		this.clicked = clicked;
	}

	public Long getDownloaded() {
		return downloaded;
	}

	public void setDownloaded(Long downloaded) {
		this.downloaded = downloaded;
	}

	public Long getEmailed() {
		return emailed;
	}

	public void setEmailed(Long emailed) {
		this.emailed = emailed;
	}

	public String getUserLastUpdated() {
		return userLastUpdated;
	}

	public void setUserLastUpdated(String userLastUpdated) {
		this.userLastUpdated = userLastUpdated;
	}

	public String getRevision() {
		return revision;
	}

	public void setRevision(String revision) {
		this.revision = revision;
	}

	public String getDatetimestamp() {
		return datetimestamp;
	}

	public void setDatetimestamp(String datetimestamp) {
		this.datetimestamp = datetimestamp;
	}

	public String getPngimage() {
		return pngimage;
	}

	public void setPngimage(String pngimage) {
		this.pngimage = pngimage;
	}

	public String getJpgimage() {
		return jpgimage;
	}

	public void setJpgimage(String jpgimage) {
		this.jpgimage = jpgimage;
	}

	public String getUserwhoupdated() {
		return userwhoupdated;
	}

	public void setUserwhoupdated(String userwhoupdated) {
		this.userwhoupdated = userwhoupdated;
	}

	public String getPages() {
		return pages;
	}

	public void setPages(String pages) {
		this.pages = pages;
	}

	public String getDocnumOnly() {
		return docnumOnly;
	}

	public void setDocnumOnly(String docnumOnly) {
		this.docnumOnly = docnumOnly;
	}

	public String getmLocation() {
		return mLocation;
	}

	public void setmLocation(String mLocation) {
		this.mLocation = mLocation;
	}
}
