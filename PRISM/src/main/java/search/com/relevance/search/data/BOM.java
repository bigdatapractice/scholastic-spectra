package com.relevance.search.data;


public class BOM {
	private IntegraAgilePart forward;
	private IntegraAgilePart backward;
	public IntegraAgilePart getForward() {
		return forward;
	}
	public void setForward(IntegraAgilePart forward) {
		this.forward = forward;
	}
	public IntegraAgilePart getBackward() {
		return backward;
	}
	public void setBackward(IntegraAgilePart backward) {
		this.backward = backward;
	}
}
