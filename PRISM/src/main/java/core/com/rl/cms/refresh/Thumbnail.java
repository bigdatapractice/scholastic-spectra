package com.rl.cms.refresh;

import com.rl.cms.refresh.Data;

public class Thumbnail {
	private Data data;

	public Data getData() {
		return data;
	}

	public void setData(Data data) {
		this.data = data;
	}
}
