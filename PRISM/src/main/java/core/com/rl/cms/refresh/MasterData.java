package com.rl.cms.refresh;

import java.util.List;

public class MasterData {

	private List<Content> data;
	private Meta meta;
	private List<Content> included;
	
	public List<Content> getData() {
		return data;
	}
	public void setData(List<Content> data) {
		this.data = data;
	}
	public List<Content> getIncluded() {
		return included;
	}
	public void setIncluded(List<Content> included) {
		this.included = included;
	}
	public Meta getMeta() {
		return meta;
	}
	public void setMeta(Meta meta) {
		this.meta = meta;
	}	
}
