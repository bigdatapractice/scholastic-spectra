package com.relevance.prism.rest;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import com.relevance.prism.service.Service;
import com.relevance.prism.service.ServiceLocator;
import com.relevance.prism.util.Log;
import com.relevance.prism.util.PrismHandler;

@Path("/SecurityMasterData")
@Consumes(MediaType.APPLICATION_JSON)
public class SecurityMasterResource extends BaseResource {
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/searchMasterData")
	public String getSecurityMasterData(@QueryParam("type") String varX,
			@QueryParam("q") String searchParam){
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());	
		Log.Info("Recieved Security getSecurityMasterData Request with param " + varX + " searchParam " + searchParam);
		 
		String response = null;
		
		try{			
			Service service = ServiceLocator.getServiceInstance("securitymaster");
			response = (String) service.getJsonObject(varX, searchParam);
		}catch(Exception ex){
			response = PrismHandler.handleException(ex);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return response;
	}

}