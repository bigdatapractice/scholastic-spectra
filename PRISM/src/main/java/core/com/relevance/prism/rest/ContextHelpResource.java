package com.relevance.prism.rest;

import java.util.List;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import com.google.gson.Gson;
//import com.relevance.e2emf.dao.UserDao;
import com.relevance.prism.data.ContextHelp;
//import com.relevance.e2emf.domain.User;
import com.relevance.prism.service.ContextHelpService;
//import com.relevance.e2emf.service.UserService;
import com.relevance.prism.util.AppException;
import com.relevance.prism.util.Log;
import com.relevance.prism.util.PrismHandler;


@Path("/ContextHelpResource")
public class ContextHelpResource extends BaseResource{	
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Path("/addfaq")
	public String createFaq(@FormParam("question") String question, 
			@FormParam("answer") String answer,
			@FormParam("screen") String screen,
			@FormParam("role") String role,
			@FormParam("enabled") long enabled)
					{
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		String response = null;
		try{
			ContextHelpService contextHelpService = new ContextHelpService();
			response = contextHelpService.insertFaq(question,answer,screen,role,enabled);
		}catch(Exception ex){
			response = PrismHandler.handleException(ex);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return response;
	}
	
	
	
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Path("/updateFaq")
	public String updateFaq(@FormParam("id") long id,
			@FormParam("question") String question, 
			@FormParam("answer") String answer,
			@FormParam("screen") String screen,
			@FormParam("role") String role,
			@FormParam("enabled") long enabled) throws Exception
	{
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		String response = null;
		try{
			ContextHelpService contextHelpService = new ContextHelpService();
			response = contextHelpService.updateFaq(id, question, answer, screen, role, enabled);
			return response;
		}catch(Exception ex){
			response = PrismHandler.handleException(ex);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return response;
	}
	
	
	/*Delete faq*/
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Path("/deleteFaq")
	public String deleteFaq(@FormParam("id") long id) {
		String response = null;
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		try{
			ContextHelpService contextHelpService = new ContextHelpService();
			response = contextHelpService.deleteFaq(id);
		}catch(Exception ex){
			response = PrismHandler.handleException(ex);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return response;
	}
	
	
	
	/*get records from  faq based on id*/
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Path("/getfaq")
	public String getFaq(@FormParam("id") long id) {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		String response = null;
		try{
			ContextHelpService contextHelpService = new ContextHelpService();
			ContextHelp contextHelp = contextHelpService.getFaq(id);
			Gson gson = new Gson();
			response = gson.toJson(contextHelp);
		}catch(Exception ex){
			response = PrismHandler.handleException(ex);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return response;
	}
	
	
	/*get all records from faq*/
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Path("/getfaqlist")
	public String getFaqList(@FormParam("app") String app) {
		
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		String response = null;
		try{
			ContextHelpService contextHelpService = new ContextHelpService();
			List<ContextHelp> faqList = contextHelpService.getFaqList(app);
			Gson gson = new Gson();
			response = gson.toJson(faqList);
		}catch(Exception ex){
			response = PrismHandler.handleException(ex);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return response;
	}
	
}
