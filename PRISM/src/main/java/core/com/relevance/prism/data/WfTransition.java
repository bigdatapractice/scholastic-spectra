package com.relevance.prism.data;

/**
 *
 * @author: Shawkath Khan
 * @Created_Date: Feb 17, 2016
 * @Purpose: Entity for Workflow Transition	
 * @Modified_By: 
 * @Modified_Date:  
 */

public class WfTransition {

	private int key;
	private String currentState;
	private String action;
	private String role;
	private String targetState;
	private int displayOrder;
	
	public int getKey() {
		return key;
	}
	public void setKey(int key) {
		this.key = key;
	}
	
	public String getCurrentState() {
		return currentState;
	}
	public void setCurrentState(String currentState) {
		this.currentState = currentState;
	}
	public String getAction() {
		return action;
	}
	public void setAction(String action) {
		this.action = action;
	}
	public String getRole() {
		return role;
	}
	public void setRole(String role) {
		this.role = role;
	}
	public String getTargetState() {
		return targetState;
	}
	public void setTargetState(String targetState) {
		this.targetState = targetState;
	}
	public int getDisplayOrder() {
		return displayOrder;
	}
	public void setDisplayOrder(int displayOrder) {
		this.displayOrder = displayOrder;
	}	
}
