package com.relevance.prism.data;

public class RAOracleOutput {
	private String id;
	private int dCount;
	private int kCount;
	private String oracleDnumber;
	private String oracleKnumber;
	private String oracleProductCode;
	private String oracleGmdnCode;
	private String sheetDnumber;
	private String sheetKnumber;
	private String sheetProductCode;
	private String sheetGmdnCode;
	private String dnumberMatchStatus;
	private String knumberMatchStatus;
	private String productCodeMatchStatus;
	private String gmdnCodeMatchStatus;
	private String sheetKnumberFilledInternally;
	private String sheetOriginalApplicant;
	private String sheetReceviedDate;
	private String sheetVlearedDate;
	private String sheetDeviceClass;
	private String sheetdeviceListingStatus;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public int getdCount() {
		return dCount;
	}
	public void setdCount(int dCount) {
		this.dCount = dCount;
	}
	public int getkCount() {
		return kCount;
	}
	public void setkCount(int kCount) {
		this.kCount = kCount;
	}
	public String getOracleDnumber() {
		return oracleDnumber;
	}
	public void setOracleDnumber(String oracleDnumber) {
		this.oracleDnumber = oracleDnumber;
	}
	public String getOracleKnumber() {
		return oracleKnumber;
	}
	public void setOracleKnumber(String oracleKnumber) {
		this.oracleKnumber = oracleKnumber;
	}
	public String getOracleProductCode() {
		return oracleProductCode;
	}
	public void setOracleProductCode(String oracleProductCode) {
		this.oracleProductCode = oracleProductCode;
	}
	public String getOracleGmdnCode() {
		return oracleGmdnCode;
	}
	public void setOracleGmdnCode(String oracleGmdnCode) {
		this.oracleGmdnCode = oracleGmdnCode;
	}
	public String getSheetDnumber() {
		return sheetDnumber;
	}
	public void setSheetDnumber(String sheetDnumber) {
		this.sheetDnumber = sheetDnumber;
	}
	public String getSheetKnumber() {
		return sheetKnumber;
	}
	public void setSheetKnumber(String sheetKnumber) {
		this.sheetKnumber = sheetKnumber;
	}
	public String getSheetProductCode() {
		return sheetProductCode;
	}
	public void setSheetProductCode(String sheetProductCode) {
		this.sheetProductCode = sheetProductCode;
	}
	public String getSheetGmdnCode() {
		return sheetGmdnCode;
	}
	public void setSheetGmdnCode(String sheetGmdnCode) {
		this.sheetGmdnCode = sheetGmdnCode;
	}
	public String getDnumberMatchStatus() {
		return dnumberMatchStatus;
	}
	public void setDnumberMatchStatus(String dnumberMatchStatus) {
		this.dnumberMatchStatus = dnumberMatchStatus;
	}
	public String getKnumberMatchStatus() {
		return knumberMatchStatus;
	}
	public void setKnumberMatchStatus(String knumberMatchStatus) {
		this.knumberMatchStatus = knumberMatchStatus;
	}
	public String getProductCodeMatchStatus() {
		return productCodeMatchStatus;
	}
	public void setProductCodeMatchStatus(String productCodeMatchStatus) {
		this.productCodeMatchStatus = productCodeMatchStatus;
	}
	public String getGmdnCodeMatchStatus() {
		return gmdnCodeMatchStatus;
	}
	public void setGmdnCodeMatchStatus(String gmdnCodeMatchStatus) {
		this.gmdnCodeMatchStatus = gmdnCodeMatchStatus;
	}
	public String getSheetKnumberFilledInternally() {
		return sheetKnumberFilledInternally;
	}
	public void setSheetKnumberFilledInternally(String sheetKnumberFilledInternally) {
		this.sheetKnumberFilledInternally = sheetKnumberFilledInternally;
	}
	public String getSheetOriginalApplicant() {
		return sheetOriginalApplicant;
	}
	public void setSheetOriginalApplicant(String sheetOriginalApplicant) {
		this.sheetOriginalApplicant = sheetOriginalApplicant;
	}
	public String getSheetReceviedDate() {
		return sheetReceviedDate;
	}
	public void setSheetReceviedDate(String sheetReceviedDate) {
		this.sheetReceviedDate = sheetReceviedDate;
	}
	public String getSheetVlearedDate() {
		return sheetVlearedDate;
	}
	public void setSheetVlearedDate(String sheetVlearedDate) {
		this.sheetVlearedDate = sheetVlearedDate;
	}
	public String getSheetDeviceClass() {
		return sheetDeviceClass;
	}
	public void setSheetDeviceClass(String sheetDeviceClass) {
		this.sheetDeviceClass = sheetDeviceClass;
	}
	public String getSheetdeviceListingStatus() {
		return sheetdeviceListingStatus;
	}
	public void setSheetdeviceListingStatus(String sheetdeviceListingStatus) {
		this.sheetdeviceListingStatus = sheetdeviceListingStatus;
	}
}
