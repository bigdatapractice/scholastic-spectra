package com.relevance.prism.util;

import java.sql.Connection;
import java.sql.SQLException;

public class AppException extends Exception{

	private static final long serialVersionUID = 1L;
	
	private String message = null;
	private String customMessage = null;
	private int type = 0;
	private Throwable cause;
	private boolean isException;
	private StackTraceElement stackTraceElement[];
	public AppException() {         
		super();     
	}       
	
	public AppException(String message) {         
		super(message);         
		this.message = message;     
	}       
	
	public AppException(Throwable cause) {         
		super(cause);     
	}       
	
	public AppException(String customMessage, int type){
		super(customMessage);
		this.customMessage = customMessage;
		this.type = type;
		
	}
	
	public AppException(String message, String customMessage, int type, Throwable cause, boolean isException){
		super(message);
		this.message = message;
		this.customMessage = customMessage;
		this.type = type;
		this.cause = cause;
		this.isException = isException;
		//insertLog(type, message);
	}
	
	public AppException(String message, StackTraceElement[] stackTraceElement, String customMessage, int type, Throwable cause, boolean isException){
		super(message);
		this.message = message;
		this.customMessage = customMessage;
		this.type = type;
		this.cause = cause;
		this.isException = isException;
		this.setStackTraceElement(stackTraceElement);
		//insertLog(type, message);
	}
	
	public AppException(String customMessage, int type, Throwable cause){
		super(customMessage, cause);
		this.customMessage = customMessage;
		this.type = type;
		this.cause = cause;
	}
	
	@Override    
	public String toString() {  
		//String stackTrace = stackTraceToString(this);
		//return "{\"message\":\""+ this.message +"\",\"stackTrace\":\""+ stackTrace + "\",\"customMessage\":\""+ this.customMessage +"\",\"type\":\""+  this.type +"\",\"cause\":\""+ this.cause +"\",\"isException\":\"" + this.isException+ "\"}";
		return "{\"message\":\""+ this.message.replaceAll("\"", "\'").replaceAll("\n", ".") + "\",\"customMessage\":\""+ this.customMessage +"\",\"type\":\""+  this.type +"\",\"cause\":\""+ this.cause +"\",\"isException\":\"" + this.isException+ "\"}";
	}       
	
	@Override    
	public String getMessage() {         
		return message;    
	} 
	
	public void setMessage(String message) {
		this.message = message;
	} 
	
	public String getCustomMessage() {         
		return customMessage;    
	}

	public void setCustomMessage(String customMessage) {
		this.customMessage = customMessage;
	}

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}

	public Throwable getCause() {
		return cause;
	}

	public void setCause(Throwable cause) {
		this.cause = cause;
	}

	public boolean isException() {
		return isException;
	}

	public void setException(boolean isException) {
		this.isException = isException;
	}

	public StackTraceElement[] getStackTraceElement() {
		return stackTraceElement;
	}

	public void setStackTraceElement(StackTraceElement[] stackTraceElement) {
		this.stackTraceElement = stackTraceElement;
	}
	
	/*private String stackTraceToString(Throwable e) {
	    StringBuilder sb = new StringBuilder();
	    sb.append("[");
	    StackTraceElement [] element = e.getStackTrace();
	    for (int i=0; i < element.length;i++) {
	    	if(i == (element.length-1)){
	        sb.append(element[i].toString());	        
	    	}else{
	    		sb.append(element[i].toString());
	    		sb.append(",");
	    	}
	    }
	    sb.append("]");
	    return sb.toString();
	}
	
	private void insertLog(int type, String message){
		Connection connection = null;
		try {
			connection = getPostGresDBConnection();
			Statement stmt = connection.createStatement();
			java.util.Calendar cal = Calendar.getInstance();
			java.sql.Date sqlDate = new java.sql.Date(cal.getTime().getTime()); // your sql date
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	        String dateForMySql = sdf.format(sqlDate);

			String insertLogQuery = "insert into logs (datetime, type, message) values("
					+ "'"
					+ dateForMySql
					+ "',"
					+ type
					+ ",'"
					+ message 
					+ "'"
					+")";
			stmt.executeUpdate(insertLogQuery);
		} catch (SQLException sql) {
			Log.Error("SQLException while inserting Log Information in table." + sql.getStackTrace());
			Log.Error(sql);			
		} catch (Exception e) {
			Log.Error("Exception while inserting Log Information in table." + e.getStackTrace());
			Log.Error(e);
		} finally {
			if (connection != null) {
				try {
					connection.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();					
				}
			}
		}
	}
	*/
	public Connection getPostGresDBConnection() throws SQLException {
		PrismDbUtil dbUtil = new PrismDbUtil();
		Connection connection = dbUtil.getPostGresConnectionForLogs();
		// Following coding is for PostGres DB Connection in 192.168.101.43
		// server
		// String url = "jdbc:postgresql://192.168.101.43/JANDJ";
		// Properties props = new Properties();
		// props.setProperty("user","jjuser");
		// props.setProperty("password","jjuser");
		// Following coding is for PostGres DB Connection in localhost
		// String url = "jdbc:postgresql://localhost/JANDJ";
		// Properties props = new Properties();
		// props.setProperty("user","postgres");
		// props.setProperty("password","admin");
		// Connection connection = DriverManager.getConnection(url, props);
		return connection;
	}
}
