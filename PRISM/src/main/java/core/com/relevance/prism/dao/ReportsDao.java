package com.relevance.prism.dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

import com.relevance.prism.data.Report;
import com.relevance.prism.util.Log;
import com.relevance.prism.util.PrismHandler;

public class ReportsDao extends BaseDao{

	public List<Report> getReportsList() throws Exception {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		List<Report> reportsList = new ArrayList<>();
		Report reportToStore = null;
		Connection connection = null;
		Statement stmt1 = null;
		ResultSet rs = null;
		try {
			String query = "select id, name, description, query, param, source, roleid, dbname from report";
			connection = dbUtil.getPostGresConnection();
			stmt1 = connection.createStatement();
			rs = stmt1.executeQuery(query);

			if (rs != null) {
				while (rs.next()) {
					reportToStore = new Report();
					reportToStore.setId(rs.getString("id"));
					reportToStore.setName(rs.getString("name"));
					reportToStore.setDescription(rs.getString("description"));
					reportToStore.setQuery(rs.getString("query"));
					reportToStore.setParam(rs.getString("param"));
					reportToStore.setSource(rs.getString("source"));
					reportToStore.setRoleid(rs.getInt("roleid"));
					reportToStore.setDbname(rs.getString("dbname"));
					reportsList.add(reportToStore);
				}
			}
		} catch(Exception ex){
			PrismHandler.handleException(ex, true);
		}
		finally {
			PrismHandler.handleFinally(connection, rs, stmt1);
		}	
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return reportsList;
	}
	
	public String insertReport(Object... obj) throws Exception {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		Report param = null;
		long max_report_id = 0;
		Connection connection = null;
		Statement stmt = null;
		ResultSet rs = null;
		try {
			param = (Report) obj[0];
			Log.Info("Inserting  Values for Report Object : " + param);

			
			connection = dbUtil.getPostGresConnection();
			stmt = connection.createStatement();
			String selectMaxQuery = "select max(id) from report";
			rs = stmt.executeQuery(selectMaxQuery);
			if (rs != null) {
				while (rs.next()) {
					max_report_id = rs.getInt(1);
				}
			}
			max_report_id = max_report_id + 1;
			
			String insertQuery = "insert into report (id, name, description, query, param, source, roleid, dbname) values("
					+ max_report_id
					+ ",'"
					+ param.getName()
					+ "','"
					+ param.getDescription() 
					+ "','"
					+ param.getQuery()
					+ "','"
					+ param.getParam()
					+ "','"
					+ param.getSource()
					+ "',"
					+ param.getRoleid()
					+ ",'"
					+ param.getDbname()
					+"')";
						
			stmt.executeUpdate(insertQuery);
		} catch(Exception ex){
			PrismHandler.handleException(ex, true);
		}
		finally {
			PrismHandler.handleFinally(connection, rs, stmt);
		}	
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return "{\"message\":\"successfully inserted Report in table\"}";
		
	}
	
	public String deleteReport(Object... obj) throws Exception {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		Report param = null;
		Connection connection = null;
		Statement stmt = null;
		try {
			param = (Report) obj[0];
			Log.Info("Deleting the Report from DB.");
			connection = dbUtil.getPostGresConnection();
			stmt = connection.createStatement();
			String reportDeleteQuery = "delete from report where id = " + param.id;
			stmt.executeUpdate(reportDeleteQuery);
			
		} catch(Exception ex){
			PrismHandler.handleException(ex, true);
		}
		finally {
			PrismHandler.handleFinally(connection, null, stmt);
		}	
		return "{\"message\" : \"successfully deleted the Report from DB\"}";
	}
	
	public String updateReport(Object... obj) throws Exception {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		Report param = null;
		Connection connection = null;
		Report resultReport = null;
		Statement stmt = null;
		ResultSet rs = null;
		try {
			param = (Report) obj[0];
			Log.Info("Updating  Values for Report Object : " + param);
			connection = dbUtil.getPostGresConnection();
			stmt = connection.createStatement();
			
			String selectReportQuery = "select id, name, description, query, param, source, roleid, dbname from report where id=" + param.getId();
			
			String deleteQueryForName = "update report set name='"	+ param.getName() + "' where id = " + param.getId();
			String deleteQueryForDescription = "update report set description='"	+ param.getDescription() + "' where id = " + param.getId();		
			String deleteQueryForQuery = "update report set query= '"	+ param.getQuery() + "' where id = " + param.getId();
			String deleteQueryForParam = "update report set param= '"	+ param.getParam() + "' where id = " + param.getId();
			String deleteQueryForSource = "update report set source='"	+ param.getSource() + "' where id = " + param.getId();
			String deleteQueryForRoleid = "update report set roleid= "	+ param.getRoleid() + " where id = " + param.getId();
			String deleteQueryForDbname = "update report set dbname= '"	+ param.getDbname() + "' where id = " + param.getId();
			
			stmt.executeUpdate(deleteQueryForName);
			stmt.executeUpdate(deleteQueryForDescription);
			stmt.executeUpdate(deleteQueryForQuery);
			stmt.executeUpdate(deleteQueryForParam);
			stmt.executeUpdate(deleteQueryForSource);
			stmt.executeUpdate(deleteQueryForRoleid);
			stmt.executeUpdate(deleteQueryForDbname);
			
			rs = stmt.executeQuery(selectReportQuery);
			if (rs != null) {
				while (rs.next()) {
					resultReport = new Report();
					resultReport = new Report();
					resultReport.setId(rs.getString("id"));
					resultReport.setName(rs.getString("name"));
					resultReport.setDescription(rs.getString("description"));
					resultReport.setQuery(rs.getString("query"));
					resultReport.setParam(rs.getString("param"));
					resultReport.setSource(rs.getString("source"));
					resultReport.setRoleid(rs.getInt("roleid"));
					resultReport.setDbname(rs.getString("dbname"));	
				}
			}
			
		} catch(Exception ex){
			PrismHandler.handleException(ex, true);
		}
		finally {
			PrismHandler.handleFinally(connection, rs, stmt);
		}	
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return "{\"message\":\"successfully updated Report in table\"}";
	}
	
	public List <LinkedHashMap <String,Object>> getPPVReportsByDynamicQuery(Object... obj) throws Exception{
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		Report report = null;
		Report inputReport = null;
		String reportName = null;
		String year = null;
		String month = null;
		String plant = null;
		String material = null;
		String supplier = null;
		String ponumber = null;
		String invoicenumber = null;
		String queryToExecute = null;
		StringBuilder where = null;
		List <LinkedHashMap <String,Object>>reportdata = new ArrayList<>();
		try{
				if(obj != null){
					reportName = (String) obj[0];
					year = (String) obj[1];
					month = (String) obj[2];
					plant = (String) obj[3];
					material = (String) obj[4];
					supplier = (String) obj[5];
					ponumber = (String) obj[6];
					invoicenumber = (String) obj[7];
					
					String tableName = "EMEA_L2_PPV";
					UserDao userDao = new UserDao();
					inputReport = new Report();
					inputReport.name = reportName;
					report = userDao.getReportQueryByReportName(inputReport);
					String initialQuery = "SELECT * FROM ";
					
					if(year != null && month != null && plant != null && material != null && supplier != null && ponumber != null && invoicenumber != null){
						where = new StringBuilder(" where posting_year = '").append(year).append("'");
						where.append(" and posting_month = '").append(month).append("'");
						where.append(" and sap_plantcode = '").append(plant).append("'");
						where.append(" and material_code = '").append(material).append("'");
						where.append(" and vendor_no = '").append(supplier).append("'");
						where.append(" and purchase_doc_no = '").append(ponumber).append("'");
						where.append(" and invoice_number = '").append(invoicenumber).append("'");
						queryToExecute = new StringBuilder(initialQuery).append(tableName).append(where.toString()).toString();
					}
					else if(year == null && month == null && plant == null && material == null && supplier == null && ponumber == null && invoicenumber == null){
						queryToExecute = new StringBuilder(initialQuery).append(tableName).toString();
					}
					else{
						
						if(year != null && month == null && plant == null && material == null && supplier == null && ponumber == null && invoicenumber == null){
							where = new StringBuilder(" where posting_year = '").append(year).append("'");
						}else if(year == null && month != null && plant == null && material == null && supplier == null && ponumber == null && invoicenumber == null){
							where = new StringBuilder(" where posting_month = '").append(month).append("'");
						}else if(year == null && month == null && plant != null && material == null && supplier == null && ponumber == null && invoicenumber == null){
							where = new StringBuilder(" where sap_plantcode = '").append(plant).append("'");
						}else if(year == null && month == null && plant == null && material != null && supplier == null && ponumber == null && invoicenumber == null){
							where = new StringBuilder(" where material_code = '").append(material).append("'");
						}else if(year == null && month == null && plant == null && material == null && supplier != null && ponumber == null && invoicenumber == null){
							where = new StringBuilder(" where vendor_no = '").append(supplier).append("'");
						}else if(year == null && month == null && plant == null && material == null && supplier == null && ponumber != null && invoicenumber == null){
							where = new StringBuilder(" where purchase_doc_no = '").append(ponumber).append("'");
						}else if(year == null && month == null && plant == null && material == null && supplier == null && ponumber == null && invoicenumber != null){
							where = new StringBuilder(" where invoice_number = '").append(invoicenumber).append("'");
						}
						if (year == null || year.equalsIgnoreCase(null) || "NULL".equalsIgnoreCase(year)) {
							where = new StringBuilder(" where posting_year is not null");							
						} else { 
							where = new StringBuilder(" where posting_year = '").append(year).append("'");
						}
						
						if (month == null || month.equalsIgnoreCase(null) || "NULL".equalsIgnoreCase(month)) {
						} else { 
							where.append(" and posting_month = '").append(month).append("'");
						}
						
						if (plant == null || plant.equalsIgnoreCase(null) || "NULL".equalsIgnoreCase(plant)) {
						} else { 
							where.append(" and sap_plantcode = '").append(plant).append("'");
						}
						
						if (material == null || material.equalsIgnoreCase(null) || "NULL".equalsIgnoreCase(material)) {
						} else { 
							where.append(" and material_code = '").append(material).append("'");
						}
						
						if (supplier == null || supplier.equalsIgnoreCase(null) || "NULL".equalsIgnoreCase(supplier)) {
						} else { 
							where.append(" and vendor_no = '").append(supplier).append("'");
						}
						
						if (ponumber == null || ponumber.equalsIgnoreCase(null) || "NULL".equalsIgnoreCase(ponumber)) {
						} else { 
							where.append(" and purchase_doc_no = '").append(ponumber).append("'");
						}
						
						if (invoicenumber == null || invoicenumber.equalsIgnoreCase(null) || "NULL".equalsIgnoreCase(invoicenumber)) {
						} else { 
							where.append(" and invoice_number = '").append(invoicenumber).append("'");
						}
						
						queryToExecute = new StringBuilder(initialQuery).append(tableName).append(where.toString()).toString();
					}
					report.query = queryToExecute;
					reportdata =  (ArrayList <LinkedHashMap <String,Object>>) userDao.getReports(report);
					Log.Info("Fetched Report object from DAO " + report);
				}
		} catch(Exception ex){
			PrismHandler.handleException(ex, true);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return reportdata;		
	}
	
	public List <LinkedHashMap <String,Object>> getEMEAPPVProfileL2ReportByFilter(Object... obj) throws Exception {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		List <LinkedHashMap <String,Object>> reportdata = new ArrayList<>();
		Report report = null;
		Report inputReport = null;
		String reportName = null;
		StringBuilder where = null;
		StringBuilder queryToExecute = null;
		String plantname = null;
		String posting_year= null;
		String posting_month= null;
		String posting_date_doc= null;
		String material_type= null;
		String purchase_currency= null;
		String invoice_currency = null;
		String purchasing_uom= null;
		String stocking_uom= null;
		String payment_terms = null;
		String documentcategory = null;
		String totalinvoicepricequartile = null;
		String standardcostquartile = null;
		String variance = null;
		String ppv2quartile = null;
		String incotermcode = null;
		String gssstatus = null;
		String vendorFreqQuantityQuartile = null;
		String vendorFreqPosQuartile = null;
		String materialFreqQuantityQuartile = null;
		String MaterialFreqPosQuartile = null;
		String materialCode = null;
		String vendorNumber = null;
		String materialSpecRef = null;
		
		try {
				if (obj != null && obj.length >= 1) {
					plantname = (String) obj[0];
					posting_year = (String) obj[1];
					posting_month = (String) obj[2];
					posting_date_doc = (String) obj[3];
					material_type = (String) obj[4];
					purchase_currency = (String) obj[5];
					invoice_currency = (String) obj[6];
					purchasing_uom = (String) obj[7];
					stocking_uom = (String) obj[8];
					payment_terms = (String) obj[9];
					documentcategory = (String) obj[10];
					totalinvoicepricequartile = (String) obj[11];
					standardcostquartile = (String) obj[12];
					variance = (String) obj[13];
					ppv2quartile = (String) obj[14];
					incotermcode = (String) obj[15];
					gssstatus = (String) obj[16];
					vendorFreqQuantityQuartile = (String) obj[17];
					vendorFreqPosQuartile = (String) obj[18];
					materialFreqQuantityQuartile = (String) obj[19];
					MaterialFreqPosQuartile = (String) obj[20];
					materialCode = (String) obj[21];
					vendorNumber = (String) obj[22];
					materialSpecRef = (String) obj[23];
					reportName = (String) obj[24];
					
					Log.Info("Fetching the L2 View for EMEA PPV Profile ...");
					String tableName = "EMEA_L2_PPV";
					UserDao userDao = new UserDao();
					inputReport = new Report();
					inputReport.name = reportName;
					report = userDao.getReportQueryByReportName(inputReport);
					String initialQuery = "SELECT * FROM ";
					
					if(plantname == null && posting_year == null && posting_month == null && posting_date_doc == null && material_type == null && purchase_currency == null && invoice_currency == null && purchasing_uom == null && payment_terms == null && documentcategory == null && totalinvoicepricequartile == null 
						&& standardcostquartile == null && variance == null && ppv2quartile == null && incotermcode == null && gssstatus == null && vendorFreqQuantityQuartile == null && vendorFreqPosQuartile == null && materialFreqQuantityQuartile == null && MaterialFreqPosQuartile == null && materialCode == null && vendorNumber == null && materialSpecRef == null){
						queryToExecute = new StringBuilder(initialQuery).append(tableName);
					}else{
						if (plantname == null || plantname.equalsIgnoreCase(null) || "NULL".equalsIgnoreCase(plantname)) {
							where = new StringBuilder(" where plant_name is not null");
						} else { 
							where = new StringBuilder(" where plant_name = '").append(plantname).append("'");
						}
						
						if (posting_year == null || posting_year.equalsIgnoreCase(null) || "NULL".equalsIgnoreCase(posting_year)) {
						} else { 
							where.append(" and posting_year = '").append(posting_year).append("'");
						}
						
						if (posting_month == null || posting_year.equalsIgnoreCase(null) || "NULL".equalsIgnoreCase(posting_month)) {
						} else { 
							where.append(" and posting_month = '").append(posting_month).append("'");
						}
						
						if (posting_date_doc == null || posting_date_doc.equalsIgnoreCase(null) || "NULL".equalsIgnoreCase(posting_date_doc)) {
						} else { 
							where.append(" and posting_date_doc = '").append(posting_date_doc).append("'");
						}
						
						if (material_type == null || material_type.equalsIgnoreCase(null) || "NULL".equalsIgnoreCase(material_type)) {
						} else { 
							where.append(" and material_type = '").append(material_type).append("'");
						}
											
						if (purchase_currency == null || purchase_currency.equalsIgnoreCase(null) || "NULL".equalsIgnoreCase(purchase_currency)) {
						} else { 
							where.append(" and purchase_currency = '").append(purchase_currency).append("'");
						}
						
						if (invoice_currency == null || invoice_currency.equalsIgnoreCase(null) || "NULL".equalsIgnoreCase(invoice_currency)) {
						} else { 
							where.append(" and invoice_currency = '").append(invoice_currency).append("'");
						}
						
												
						if (purchasing_uom == null || purchasing_uom.equalsIgnoreCase(null) || "NULL".equalsIgnoreCase(purchasing_uom)) {
						} else { 
							where.append(" and purchasing_uom = '").append(purchasing_uom).append("'");
						}
						
						if (stocking_uom == null || stocking_uom.equalsIgnoreCase(null) || "NULL".equalsIgnoreCase(stocking_uom)) {
						} else { 
							where.append(" and stocking_uom = '").append(stocking_uom).append("'");
						}
						
						if (payment_terms == null || payment_terms.equalsIgnoreCase(null) || "NULL".equalsIgnoreCase(payment_terms)) {
						} else { 
							where.append(" and payment_terms = '").append(payment_terms).append("'");
						}
						if (documentcategory == null || documentcategory.equalsIgnoreCase(null) || "NULL".equalsIgnoreCase(documentcategory)) {
						} else { 
							where.append(" and document_category = '").append(documentcategory).append("'");
						}
						
						if (totalinvoicepricequartile == null || totalinvoicepricequartile.equalsIgnoreCase(null) || "NULL".equalsIgnoreCase(totalinvoicepricequartile)) {
						} else { 
							where.append(" and totalnvoicepricequartile = '").append(totalinvoicepricequartile).append("'");
						}
						
						if (standardcostquartile == null || standardcostquartile.equalsIgnoreCase(null) || "NULL".equalsIgnoreCase(standardcostquartile)) {
						} else { 
							where.append(" and standardcostquartile = '").append(standardcostquartile).append("'");
						}
						
						if (variance == null || variance.equalsIgnoreCase(null) || "NULL".equalsIgnoreCase(variance)) {
						} else { 
							where.append(" and variance = '").append(variance).append("'");
						}
						
						if (ppv2quartile == null || ppv2quartile.equalsIgnoreCase(null) || "NULL".equalsIgnoreCase(ppv2quartile)) {
						} else { 
							where.append(" and ppv2quartile = '").append(ppv2quartile).append("'");
						}
						
						if (incotermcode == null || incotermcode.equalsIgnoreCase(null) || "NULL".equalsIgnoreCase(incotermcode)) {
						} else { 
							where.append(" and incoterm_code = '").append(incotermcode).append("'");
						}
						if (gssstatus == null || gssstatus.equalsIgnoreCase(null) || "NULL".equalsIgnoreCase(gssstatus)) {
						} else { 
							where.append(" and gss_status = '").append(gssstatus).append("'");
						}
						
						if (vendorFreqQuantityQuartile == null || vendorFreqQuantityQuartile.equalsIgnoreCase(null) || "NULL".equalsIgnoreCase(vendorFreqQuantityQuartile)) {
						} else { 
							where.append(" and vendor_freq_quantityquartile = '").append(vendorFreqQuantityQuartile).append("'");
						}
						
						if (vendorFreqPosQuartile == null || vendorFreqPosQuartile.equalsIgnoreCase(null) || "NULL".equalsIgnoreCase(vendorFreqPosQuartile)) {
						} else { 
							where.append(" and vendor_freq_posquartile = '").append(vendorFreqPosQuartile).append("'");
						}
						
						if (materialFreqQuantityQuartile == null || materialFreqQuantityQuartile.equalsIgnoreCase(null) || "NULL".equalsIgnoreCase(materialFreqQuantityQuartile)) {
						} else { 
							where.append(" and material_freq_quantityquartile = '").append(materialFreqQuantityQuartile).append("'");
						}
						
						if (MaterialFreqPosQuartile == null || MaterialFreqPosQuartile.equalsIgnoreCase(null) || "NULL".equalsIgnoreCase(MaterialFreqPosQuartile)) {
						} else { 
							where.append(" and material_freq_posquartile = '").append(MaterialFreqPosQuartile).append("'");
						}
						
						if (materialCode == null || materialCode.equalsIgnoreCase(null) || "NULL".equalsIgnoreCase(materialCode)) {
						} else { 
							where.append(" and material_code  in ('").append(materialCode).append("')");
						}
						
						if (vendorNumber == null || vendorNumber.equalsIgnoreCase(null) || "NULL".equalsIgnoreCase(vendorNumber)) {
						} else { 
							where.append(" and vendor_no in ('").append(vendorNumber).append("')");
						}
						
						if (materialSpecRef == null || materialSpecRef.equalsIgnoreCase(null) || "NULL".equalsIgnoreCase(materialSpecRef)) {
						} else { 
							where.append(" and material_spec_ref in ('").append(materialSpecRef).append("')");
						}
						
						queryToExecute = new StringBuilder(initialQuery).append(tableName).append(where.toString());
					}
										
					report.query = queryToExecute.toString();
					reportdata =  (ArrayList <LinkedHashMap <String,Object>>) userDao.getReports(report);
					Log.Info("Fetched Report object from DAO " + report);
					}			
		} catch(Exception ex){
			PrismHandler.handleException(ex, true);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return reportdata;
	}
	public List <LinkedHashMap <String,Object>> getPOReportsByDynamicQuery(Object... obj) throws Exception{
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		Report report = null;
		Report inputReport = null;
		String reportName = null;
		String plant = null;
		String companycode = null;
		String material = null;
		String supplier = null;
		String ponumber = null;
		String queryToExecute = null;
		StringBuilder where = null;
		List <LinkedHashMap <String,Object>>reportdata = new ArrayList<>();
		try{
				if(obj != null){
					reportName = (String) obj[0];
					plant = (String) obj[1];
					companycode = (String) obj[2];
					material = (String) obj[3];
					supplier = (String) obj[4];
					ponumber = (String) obj[5];
					
					String tableName = "EMEA_L2_PO";
					UserDao userDao = new UserDao();
					inputReport = new Report();
					inputReport.name = reportName;
					report = userDao.getReportQueryByReportName(inputReport);
					String initialQuery = "SELECT * FROM ";
					
					if(plant != null && companycode != null && material != null && supplier != null && ponumber != null){
						where = new StringBuilder(" where sapplantcode = '").append(plant).append("'");						
						where.append(" and company_code = '").append(companycode).append("'");
						where.append(" and po_material_number in ('").append(material).append("')");
						where.append(" and vendor_number  ('").append(supplier).append("')");
						where.append(" and po_number = '").append(ponumber).append("'");
						queryToExecute = new StringBuilder(initialQuery).append(tableName).append(where.toString()).toString();
					}
					else if(plant == null && companycode == null && material == null && supplier == null && ponumber == null){
						queryToExecute = new StringBuilder(initialQuery).append(tableName).toString();
					}
					else{
						
						if(plant != null && companycode == null && material == null && supplier == null && ponumber == null){
							where = new StringBuilder(" where sapplantcode = '").append(plant).append("'");
						}else if(plant == null && companycode != null && material == null && supplier == null && ponumber == null){
							where = new StringBuilder(" where company_code = '").append(companycode).append("'");
						}else if(plant == null && companycode == null && material != null && supplier == null && ponumber == null){
							where = new StringBuilder(" where po_material_number in ('").append(material).append("')");
						}else if(plant == null && companycode == null && material == null && supplier != null && ponumber == null){
							where = new StringBuilder(" where vendor_number in ('").append(supplier).append("')");
						}else if(plant == null && companycode == null && material == null && supplier == null && ponumber != null){
							where = new StringBuilder(" where po_number = '").append(ponumber).append("'");
						}
						
						if (plant == null || plant.equalsIgnoreCase(null) || "NULL".equalsIgnoreCase(plant)) {
							where = new StringBuilder(" where sapplantcode is not null");
						} else {
							where = new StringBuilder(" where sapplantcode = '").append(plant).append("'");							
						}
						
						if (companycode == null || companycode.equalsIgnoreCase(null) || "NULL".equalsIgnoreCase(companycode)) {
						} else { 
							where.append(" and company_code = '").append(companycode).append("'");
						}
						
						if (material == null || material.equalsIgnoreCase(null) || "NULL".equalsIgnoreCase(material)) {
						} else { 
							where.append(" and po_material_number in ('").append(material).append("')");
						}
						
						if (supplier == null || supplier.equalsIgnoreCase(null) || "NULL".equalsIgnoreCase(supplier)) {
						} else { 
							where.append(" and vendor_number in ('").append(supplier).append("')");
						}
						
						if (ponumber == null || ponumber.equalsIgnoreCase(null) || "NULL".equalsIgnoreCase(ponumber)) {
						} else { 
							where.append(" and po_number = '").append(ponumber).append("'");
						}
						
						queryToExecute = new StringBuilder(initialQuery).append(tableName).append(where.toString()).toString();
					}
					report.query = queryToExecute;
					reportdata =  (ArrayList <LinkedHashMap <String,Object>>) userDao.getReports(report);
					Log.Info("Fetched Report object from DAO " + report);
				}
		} catch(Exception ex){
			PrismHandler.handleException(ex, true);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return reportdata;		
	}
	
	public List <LinkedHashMap <String,Object>> getQuotesReportsByDynamicQuery(Object... obj) throws Exception{
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		Report report = null;
		Report inputReport = null;
		String reportName = null;
		String plant = null;
		String material = null;
		String supplier = null;
		String year = null;
		String queryToExecute = null;
		StringBuilder where = null;
		List <LinkedHashMap <String,Object>>reportdata = new ArrayList<>();
		try{
				if(obj != null){
					reportName = (String) obj[0];
					year = (String) obj[1];
					plant = (String) obj[2];					
					material = (String) obj[3];
					supplier = (String) obj[4];
										
					String tableName = "EMEA_L2_QT";
					UserDao userDao = new UserDao();
					inputReport = new Report();
					inputReport.name = reportName;
					report = userDao.getReportQueryByReportName(inputReport);
					String initialQuery = "SELECT * FROM ";
					
					if(plant != null && year != null && material != null && supplier != null){
						where = new StringBuilder(" where sap_plant = '").append(plant).append("'");						
						where.append(" and qt_year = '").append(year).append("'");
						where.append(" and qt_material_number = '").append(material).append("'");
						where.append(" and qt_vendor_number = '").append(supplier).append("'");
						queryToExecute = new StringBuilder(initialQuery).append(tableName).append(where.toString()).toString();
					}
					else if(plant == null && year == null && material == null && supplier == null){
						queryToExecute = new StringBuilder(initialQuery).append(tableName).toString();
					}
					else{
						
						if(plant != null && year == null && material == null && supplier == null){
							where = new StringBuilder(" where sap_plant = '").append(plant).append("'");
						}else if(plant == null && year != null && material == null && supplier == null){
							where = new StringBuilder(" where qt_year = '").append(year).append("'");
						}else if(plant == null && year == null && material != null && supplier == null){
							where = new StringBuilder(" where qt_material_number = '").append(material).append("'");
						}else if(plant == null && year == null && material == null && supplier != null){
							where = new StringBuilder(" where qt_vendor_number = '").append(supplier).append("'");
						}
						
						if (plant == null || plant.equalsIgnoreCase(null) || "NULL".equalsIgnoreCase(plant)) {
							where = new StringBuilder(" where sap_plant is not null");
						} else {
							where = new StringBuilder(" where sap_plant = '").append(plant).append("'");							
						}
						
						if (year == null || year.equalsIgnoreCase(null) || "NULL".equalsIgnoreCase(year)) {
						} else { 
							where.append(" and qt_year = '").append(year).append("'");
						}
						
						if (material == null || material.equalsIgnoreCase(null) || "NULL".equalsIgnoreCase(material)) {
						} else { 
							where.append(" and qt_material_number = '").append(material).append("'");
						}
						
						if (supplier == null || supplier.equalsIgnoreCase(null) || "NULL".equalsIgnoreCase(supplier)) {
						} else { 
							where.append(" and qt_vendor_number = '").append(supplier).append("'");
						}
						
						queryToExecute = new StringBuilder(initialQuery).append(tableName).append(where.toString()).toString();
					}
					report.query = queryToExecute;
					reportdata =  (ArrayList <LinkedHashMap <String,Object>>) userDao.getReports(report);
					Log.Info("Fetched Report object from DAO " + report);
					return reportdata;
				}
		} catch(Exception ex){
			PrismHandler.handleException(ex, true);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return reportdata;		
	}
	
	public List <LinkedHashMap <String,Object>> getEMEAPPAPOProfileL2ReportByFilter(Object... obj) throws Exception {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		List <LinkedHashMap <String,Object>> reportdata = new ArrayList<>();
		Report report = null;
		Report inputReport = null;
		String reportName = null;
		StringBuilder where = null;
		StringBuilder queryToExecute = null;
		String gssStatus = null;
		String currency= null;
		String itemType= null;
		String sseCI= null;
		String cI= null;
		String sseIMI= null;
		String priceVariance = null;
		String moqMisMatch= null;
		String currencyMisMatch= null;
		String unitMisMatch = null;
		String leadTimeMisMatch = null;
		String pVPQuartile = null;
		String incoTermMismatch = null;
		String netvarianceQuartile = null;
		String poCostQuartile = null;
		String plantDesc = null;
		String poYear = null;
		String fromPoReqDate = null;
		String toPoReqDate = null;
		String materiallist = null;
		String supplierlist = null;
		
		try {
				if (obj != null && obj.length >= 1) {
					reportName = (String) obj[0];
					gssStatus = (String) obj[1];
					currency = (String) obj[2];
					itemType = (String) obj[3];
					sseCI = (String) obj[4];
					cI = (String) obj[5];
					sseIMI = (String) obj[6];
					priceVariance = (String) obj[7];
					moqMisMatch = (String) obj[8];
					currencyMisMatch = (String) obj[9];
					unitMisMatch = (String) obj[10];
					leadTimeMisMatch = (String) obj[11];
					pVPQuartile = (String) obj[12];
					incoTermMismatch = (String) obj[13];
					netvarianceQuartile = (String) obj[14];
					poCostQuartile = (String) obj[15];
					plantDesc = (String) obj[16];
					poYear = (String) obj[17];
					fromPoReqDate = (String) obj[18];
					toPoReqDate = (String) obj[19];
					materiallist = (String) obj[20];
					supplierlist = (String) obj[21];
					
					Log.Info("Fetching the L2 View for EMEA PO Profile ...");
					String tableName = "EMEA_L2_PO";
					UserDao userDao = new UserDao();
					inputReport = new Report();
					inputReport.name = reportName;
					report = userDao.getReportQueryByReportName(inputReport);
					String initialQuery = "SELECT * FROM ";
					
					if(gssStatus == null && currency == null && itemType == null && sseCI == null && cI == null && sseIMI == null && priceVariance == null && moqMisMatch == null && currencyMisMatch == null && unitMisMatch == null && leadTimeMisMatch == null 
							&& pVPQuartile == null && incoTermMismatch == null && netvarianceQuartile == null && poCostQuartile == null && plantDesc == null && poYear == null	&& fromPoReqDate == null && materiallist == null && supplierlist==null){
						queryToExecute = new StringBuilder(initialQuery).append(tableName);
					}else{
						if (gssStatus == null || gssStatus.equalsIgnoreCase(null) || "NULL".equalsIgnoreCase(gssStatus)) {
							where = new StringBuilder(" where gss_status is not null");
						} else { 
							where = new StringBuilder(" where gss_status in ('").append(gssStatus).append("')");
						}
						
						if (currency == null || currency.equalsIgnoreCase(null) || "NULL".equalsIgnoreCase(currency)) {
						} else { 
							where.append(" and po_currency in ('").append(currency).append("')");
						}
						
						if (itemType == null || itemType.equalsIgnoreCase(null) || "NULL".equalsIgnoreCase(itemType)) {
						} else { 
							where.append(" and po_item_type in ('").append(itemType).append("')");
						}
						
						if (sseCI == null || sseCI.equalsIgnoreCase(null) || "NULL".equalsIgnoreCase(sseCI)) {
						} else { 
							where.append(" and sse_contract_found in ('").append(sseCI).append("')");
						}
						
						if (cI == null || cI.equalsIgnoreCase(null) || "NULL".equalsIgnoreCase(cI)) {
						} else { 
							where.append(" and compliant_indicator in ('").append(cI).append("')");
						}
											
						if (sseIMI == null || sseIMI.equalsIgnoreCase(null) || "NULL".equalsIgnoreCase(sseIMI)) {
						} else { 
							where.append(" and sse_match_found in ('").append(sseIMI).append("')");
						}
						
						if (priceVariance == null || priceVariance.equalsIgnoreCase(null) || "NULL".equalsIgnoreCase(priceVariance)) {
						} else { 
							where.append(" and favourable in ('").append(priceVariance).append("')");
						}
						
						if (moqMisMatch == null || moqMisMatch.equalsIgnoreCase(null) || "NULL".equalsIgnoreCase(moqMisMatch)) {
						} else { 
							where.append(" and moq_mismatch_ind in ('").append(moqMisMatch).append("')");
						}
						
						if (currencyMisMatch == null || currencyMisMatch.equalsIgnoreCase(null) || "NULL".equalsIgnoreCase(currencyMisMatch)) {
						} else { 
							where.append(" and currency_mismatch_ind in ('").append(currencyMisMatch).append("')");
						}
						
						if (unitMisMatch == null || unitMisMatch.equalsIgnoreCase(null) || "NULL".equalsIgnoreCase(unitMisMatch)) {
						} else { 
							where.append(" and unit_mismatch_ind in ('").append(unitMisMatch).append("')");
						}
						
						if (leadTimeMisMatch == null || leadTimeMisMatch.equalsIgnoreCase(null) || "NULL".equalsIgnoreCase(leadTimeMisMatch)) {
						} else { 
							where.append(" and lead_time_mismatch_ind in ('").append(leadTimeMisMatch).append("')");
						}
						
						if (pVPQuartile == null || pVPQuartile.equalsIgnoreCase(null) || "NULL".equalsIgnoreCase(pVPQuartile)) {
						} else { 
							where.append(" and pricevariancepercentagequartile in ('").append(pVPQuartile).append("')");
						}
						
						if (incoTermMismatch == null || incoTermMismatch.equalsIgnoreCase(null) || "NULL".equalsIgnoreCase(incoTermMismatch)) {
						} else { 
							where.append(" and incoterm_mismatch_ind in ('").append(incoTermMismatch).append("')");
						}
						
						if (netvarianceQuartile == null || netvarianceQuartile.equalsIgnoreCase(null) || "NULL".equalsIgnoreCase(netvarianceQuartile)) {
						} else { 
							where.append(" and netvariancequartile in ('").append(netvarianceQuartile).append("')");
						}
						
						if (poCostQuartile == null || poCostQuartile.equalsIgnoreCase(null) || "NULL".equalsIgnoreCase(poCostQuartile)) {
						} else { 
							where.append(" and pocostquartile in ('").append(poCostQuartile).append("')");
						}
						
						if (plantDesc == null || plantDesc.equalsIgnoreCase(null) || "NULL".equalsIgnoreCase(plantDesc)) {
						} else { 
							where.append(" and plantdesc in ('").append(plantDesc).append("')");
						}
						if (poYear == null || poYear.equalsIgnoreCase(null) || "NULL".equalsIgnoreCase(poYear)) {
						} else { 
							where.append(" and po_req_date like '%/").append(poYear).append("'");
						}
						
						if (fromPoReqDate == null || fromPoReqDate.equalsIgnoreCase(null) || "NULL".equalsIgnoreCase(fromPoReqDate)) {
						} else { 
							where.append(" AND unix_timestamp(po_req_date, 'MM/dd/yyyy') >= unix_timestamp('").append(fromPoReqDate).append("', 'MM/dd/yyyy') and unix_timestamp(po_req_date, 'MM/dd/yyyy') <= unix_timestamp('").append(toPoReqDate).append("', 'MM/dd/yyyy')");
						}
						
						if (materiallist == null || materiallist.equalsIgnoreCase(null) || "NULL".equalsIgnoreCase(materiallist)) {
						} else { 
							where.append(" and po_material_number in ('").append(materiallist).append("')");
						}
						
						if (supplierlist == null || supplierlist.equalsIgnoreCase(null) || "NULL".equalsIgnoreCase(supplierlist)) {
						} else { 
							where.append(" and vendor_number in ('").append(supplierlist).append("')");
						}
						
						where.append(" limit 35000 ");
						queryToExecute = new StringBuilder(initialQuery).append(tableName).append(where.toString());
					}
					
					report.query = queryToExecute.toString();
					reportdata =  (ArrayList <LinkedHashMap <String,Object>>) userDao.getReports(report);
					Log.Info("Fetched Report object from DAO " + report);
					}			
		} catch(Exception ex){
			PrismHandler.handleException(ex, true);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return reportdata;
	}
	
	
}
