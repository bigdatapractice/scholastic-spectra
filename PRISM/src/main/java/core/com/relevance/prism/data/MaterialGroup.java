
package com.relevance.prism.data;

import java.util.ArrayList;
import java.util.List;

public class MaterialGroup extends E2emfBusinessObject{
	
    private String materialGroupName;

    private List<Material> children = new ArrayList<Material>();

	public String getMaterialGroupName() {
		return materialGroupName;
	}

	public void setMaterialGroupName(String materialGroupName) {
		this.materialGroupName = materialGroupName;
	}

	public List<Material> getChildren() {
		return children;
	}

	public void setChildren(List<Material> children) {
		this.children = children;
	}

}
