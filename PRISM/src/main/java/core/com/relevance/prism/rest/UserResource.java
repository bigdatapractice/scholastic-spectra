package com.relevance.prism.rest;

import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Set;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;
import javax.ws.rs.core.StreamingOutput;

import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.json.JSONObject;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.relevance.prism.data.Report;
import com.relevance.prism.data.Result;
import com.relevance.prism.data.Role;
import com.relevance.prism.data.SecondaryRole;
import com.relevance.prism.data.User;
import com.relevance.prism.data.UserAdditionalDetails;
import com.relevance.prism.data.UserSecondaryRole;
import com.relevance.prism.service.UserService;
import com.relevance.prism.util.E2emfAppUtil;
import com.relevance.prism.util.E2emfConstants;
import com.relevance.prism.util.Log;
import com.relevance.prism.util.PrismHandler;
import com.relevance.prism.util.UserAuthenticator;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.filter.HTTPBasicAuthFilter;

@Path("/user")
public class UserResource extends BaseResource {

	@Context
	ServletContext context;

	public UserResource(@Context ServletContext value) {
		this.context = value;
	}

	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Path("/getuser")
	public String getUser(@FormParam("username") String username)
			throws Exception {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		String response = null;
		try {
			UserService userService = new UserService();
			User user = new User();
			user.setUsername(username);
			user = (User) userService.getUser(user);
			Gson gson = new Gson();
			response = gson.toJson(user);
		} catch (Exception ex) {
			response = PrismHandler.handleException(ex);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return response;
	}

	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Path("/sendCredentialsToUser")
	public String sendCredentialsToUser(@FormParam("username") String username,
			@FormParam("message") String message,
			@FormParam("subject") String subject) throws Exception {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		String response = null;
		try {
			UserService userService = new UserService();
			response = userService.sendCredentialsToUser(subject, message,
					username);
			return response;
		} catch (Exception ex) {
			response = PrismHandler.handleException(ex);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return response;
	}
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Path("/sendpassword")
	public String sendpasswordtouser(@FormParam("username") String username,@FormParam("subject") String subject) throws Exception {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		String response = null;
		try {
			UserService userService = new UserService();
			response = userService.sendPasswordToUser(username,subject);
			return response;
		} catch (Exception ex) {
			response = PrismHandler.handleException(ex);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return response;
	}
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Path("/adduser")
	public String addUser(@FormParam("username") String username,
			@FormParam("firstname") String firstname,
			@FormParam("lastname") String lastname,
			@FormParam("password") String password,
			@FormParam("enabled") String enabled,
			@FormParam("themename") String themename,
			@FormParam("roles") String roles,
			@FormParam("menuontop") String menuOnTop,
			@FormParam("primaryrole") String primaryrole,
			@FormParam("businessrole") String businessrole,
			@FormParam("emailId") String email,
			@FormParam("timezone") String timezone,
			@FormParam("datename") String dateformat,
			@FormParam("status") String status,
			@FormParam("region") String region,
			@FormParam("remarks") String remarks,
			@FormParam("additionalDetails") String additionalDetails) throws Exception {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		String response = null;
		User user;
		try {

			user = new User();
			user.setUsername(username);
			user.setFirstName(firstname);
			user.setLastName(lastname);
			user.setPassword(password);
			user.setEnabled(Long.parseLong(enabled));
			user.setThemeName(themename);
			String[] role = roles.split(",");
			user.setRolesAsArray(role);
			user.setRolesAsArray(user.getRolesAsArray());
			user.setMenuOnTop((menuOnTop == null) ? 0 : Long.parseLong(menuOnTop));
			user.setPrimaryRole(primaryrole);
			user.setBusinessRole(businessrole);
			user.setEmail(email);
			user.setTimezone(timezone);
			user.setDateformat(dateformat);
			user.setStatus(status);
			user.setRegion(region);
			user.setRemarks(remarks);
			
			if(additionalDetails != null && !additionalDetails.isEmpty()) {
				Gson gson = new Gson();
				UserAdditionalDetails additionalDetail = new UserAdditionalDetails();
				additionalDetail = gson.fromJson(additionalDetails, UserAdditionalDetails.class);
				user.setAdditionalDetails(additionalDetail);
			}
			UserService userService = new UserService();
			response = userService.insertUser(user);
		} catch (Exception ex) {
			response = PrismHandler.handleException(ex);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return response;
	}
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Path("/resetpassword")
	public String resetPassword(@FormParam("username") String username,
			@FormParam("password") String password,
			@FormParam("newpassword") String newpassword){
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		String response = null;
		try {
			UserService userService = new UserService();
			response = userService.resetPassword(username,password,newpassword);
		} catch (Exception ex) {
			response = PrismHandler.handleException(ex);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return response;
	};
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Path("/updateuser")
	public String updateUser(@FormParam("username") String username,
			@FormParam("firstname") String firstname,
			@FormParam("lastname") String lastname,
			@FormParam("enabled") String enabled,
			@FormParam("themename") String themename,
			@FormParam("roles") String roles,
			@FormParam("menuontop") String menuontop,
			@FormParam("primaryrole") String primaryrole,
			@FormParam("businessrole") String businessrole,
			@FormParam("emailId") String email,
			@FormParam("timezone") String timezone,
			@FormParam("datename") String dateformat,
			@FormParam("status") String status,
			@FormParam("region") String region,
			@FormParam("remarks") String remarks,
			@FormParam("additionalDetails") String additionalDetails) throws Exception {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		String response = null;
		User user;
		try {
			user = new User();
			user.setUsername(username);
			user.setFirstName(firstname);
			user.setLastName(lastname);
			user.setEnabled(Long.parseLong(enabled));
			user.setThemeName(themename);
			String[] role = roles.split(",");
			user.setRolesAsArray(role);
			user.setRolesAsArray(user.getRolesAsArray());
			user.setMenuOnTop(Long.parseLong(menuontop));
			user.setPrimaryRole(primaryrole);
			user.setBusinessRole(businessrole);
			user.setEmail(email);
			user.setRegion(region);
			user.setStatus(status);
			user.setTimezone(timezone);
			user.setRemarks(remarks);
			user.setDateformat(dateformat);
			if(additionalDetails != null && !additionalDetails.isEmpty()) {
				Gson gson = new Gson();
				UserAdditionalDetails additionalDetail = new UserAdditionalDetails();
				additionalDetail = gson.fromJson(additionalDetails, UserAdditionalDetails.class);
				user.setAdditionalDetails(additionalDetail);
			}
			UserService userService = new UserService();
			response = userService.updateUser(user);
		} catch (Exception ex) {
			response = PrismHandler.handleException(ex);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return response;
	}

	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Path("/updateUserPrimaryRole")
	public String updateUserPrimaryRole(
			@FormParam("primaryrole") String primaryrole,
			@Context HttpServletRequest req) throws Exception {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		String response = null;
		try {
			HttpSession session = req.getSession(true);
			String userName = session.getAttribute("userName").toString();
			UserService userService = new UserService();

			response = userService.updateUserPrimaryRole(userName, primaryrole);
			session.setAttribute("app", primaryrole);
		} catch (Exception ex) {
			response = PrismHandler.handleException(ex);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return response;
	}

	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Path("/deleteuser")
	public String deleteUser(@FormParam("username") String username)
			throws Exception {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		String response = null;
		try {
			UserService userService = new UserService();
			response = userService.deleteUser(username);
		} catch (Exception ex) {
			response = PrismHandler.handleException(ex);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return response;
	}

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/getuserlist")
	public String getUserList() {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		String response = null;
		try {
			UserService userService = new UserService();
			List<User> userList = userService.getUserList();
			Gson gson = new Gson();
			response = gson.toJson(userList);
		} catch (Exception ex) {
			response = PrismHandler.handleException(ex);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return response;
	}

	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Path("/adduserrole")
	public String addUserRole(@FormParam("username") String username,
			@FormParam("role") String role) throws Exception {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		String response = null;
		try {
			UserService userService = new UserService();
			User user = userService.insertUserRole(username, role);
			Gson gson = new Gson();
			response = gson.toJson(user);
		} catch (Exception ex) {
			response = PrismHandler.handleException(ex);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return response;
	}

	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Path("/deleteuserroles")
	public String deleteUserRole(@FormParam("username") String username,
			@FormParam("roles") String roles) throws Exception {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		String response = null;
		try {
			UserService userService = new UserService();
			response = userService.deleteUserRole(username, roles);
		} catch (Exception ex) {
			response = PrismHandler.handleException(ex);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return response;
	}

	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Path("/getroles")
	public String getUserRoles(@Context HttpServletRequest req)
			throws Exception {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		String response = null;
		try {
			Gson gson = new Gson();
			HttpSession session = req.getSession(true);
			ArrayList<Role> roles = null;
			if (session.getAttribute("userrolesfordisplay") != null) {
				String userRoleActions = session.getAttribute(
						"userrolesfordisplay").toString();
				roles = gson.fromJson(userRoleActions,
						new TypeToken<ArrayList<Role>>() {
						}.getType());
			}
			UserService userService = new UserService();
			List<Role> roleList = userService.getRoles(roles);
			response = gson.toJson(roleList);
		} catch (Exception ex) {
			response = PrismHandler.handleException(ex);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return response;
	}

	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Path("/getrole")
	public String getUserRoles(@FormParam("roleName") String roleName)
			throws Exception {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		String response = null;
		try {
			UserService userService = new UserService();
			Role role = userService.getRole(roleName);
			Gson gson = new Gson();
			response = gson.toJson(role);
		} catch (Exception ex) {
			response = PrismHandler.handleException(ex);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return response;
	}

	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Path("/addrole")
	public String addRole(@FormParam("displayname") String displayname,
			@FormParam("name") String name,
			@FormParam("favicon") String favicon,
			@FormParam("logo") String logo,
			@FormParam("loginContent") String loginContent,
			@FormParam("themeName") String themeName,
			@FormParam("menuOnTop") long menuOnTop,
			@FormParam("displayOrder") long displayOrder,
			@FormParam("widgetClass") String widgetClass,
			@FormParam("icon") String icon,
			@FormParam("widgetColor") String widgetColor,
			@FormParam("fixedHeader") long fixedHeader,
			@FormParam("fixedNavigation") long fixedNavigation,
			@FormParam("fixedRibbon") long fixedRibbon,
			@FormParam("sso") long sso,
			@FormParam("signUp") long signUp,
			@FormParam("parentrole") String parentrole,
			@FormParam("topRightIcon") String topRightIcon) throws Exception {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		String response = null;
		try {
			UserService userService = new UserService();
			response = userService.insertRole(displayname, name, favicon, logo,
					loginContent, themeName, menuOnTop, displayOrder,
					widgetClass, icon, widgetColor, fixedHeader,
					fixedNavigation, fixedRibbon, sso, signUp, parentrole,topRightIcon);
		} catch (Exception ex) {
			response = PrismHandler.handleException(ex);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return response;
	}

	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Path("/updaterole")
	public String updateRole(@FormParam("displayname") String displayname,
			@FormParam("name") String name,
			@FormParam("favicon") String favicon,
			@FormParam("logo") String logo,
			@FormParam("loginContent") String loginContent,
			@FormParam("themeName") String themeName,
			@FormParam("menuOnTop") long menuOnTop,
			@FormParam("displayOrder") long displayOrder,
			@FormParam("widgetClass") String widgetClass,
			@FormParam("icon") String icon,
			@FormParam("widgetColor") String widgetColor,
			@FormParam("fixedHeader") long fixedHeader,
			@FormParam("fixedNavigation") long fixedNavigation,
			@FormParam("fixedRibbon") long fixedRibbon,
			@FormParam("sso") long sso,
			@FormParam("signUp") long signUp,
			@FormParam("parentrole") String parentrole,
			@FormParam("topRightIcon") String topRightIcon) throws Exception {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		String response = null;
		try {
			UserService userService = new UserService();
			response = userService.updateRole(displayname, name, favicon, logo,
					loginContent, themeName, menuOnTop, displayOrder,
					widgetClass, icon, widgetColor, fixedHeader,
					fixedNavigation, fixedRibbon, sso, signUp, parentrole,topRightIcon);

		} catch (Exception ex) {
			response = PrismHandler.handleException(ex);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return response;
	}

	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Path("/getuserroleactions")
	public String getUserRoleActionList(@FormParam("username") String username)
			throws Exception {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		String response = null;
		try {
			UserService userService = new UserService();
			response = userService.getUserRoleActionList(username);

		} catch (Exception ex) {
			response = PrismHandler.handleException(ex);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return response;
	}

	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Path("/getsecondaryroles")
	public String getSecondaryRoles(@FormParam("email") String email)
			throws Exception {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		String response = null;
		try {
			UserService userService = new UserService();
			List<SecondaryRole> secondaryRoleList = userService
					.getSecondaryRoles(email);
			Gson gson = new Gson();
			response = gson.toJson(secondaryRoleList);
		} catch (Exception ex) {
			response = PrismHandler.handleException(ex);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return response;
	}

	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Path("/getusersecondaryroles")
	public String getSecondaryRoles(@FormParam("app") String app,
			@FormParam("username") String username) throws Exception {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		String response = null;
		try {
			UserService userService = new UserService();
			List<UserSecondaryRole> secondaryRoleList = userService
					.getUserSecondaryRoles(app, username);
			Gson gson = new Gson();
			response = gson.toJson(secondaryRoleList);
		} catch (Exception ex) {
			response = PrismHandler.handleException(ex);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return response;
	}

	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Path("/getdatasourcesforuser")
	public String getDataSourcesForUser(@FormParam("app") String app,
			@FormParam("username") String username) throws Exception {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		String response = null;
		try {
			UserService userService = new UserService();
			List<UserSecondaryRole> secondaryRoleList = userService
					.getDataSourcesForUser(app, username);
			Gson gson = new Gson();
			response = gson.toJson(secondaryRoleList);
		} catch (Exception ex) {
			response = PrismHandler.handleException(ex);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return response;
	}

	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Path("/addusersecondaryrole")
	public String addUserSecondaryRole(@FormParam("username") String username,
			@FormParam("secondaryrole") String secondaryrole,
			@FormParam("datasource") String datasource) throws Exception {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		String response = null;
		try {
			UserService userService = new UserService();
			response = userService.insertUserSecondaryRole(username,
					secondaryrole, datasource);
		} catch (Exception ex) {
			response = PrismHandler.handleException(ex);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return response;
	}

	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Path("/deleteusersecondaryrole")
	public String deleteUserSecondaryRole(
			@FormParam("usersecondaryroleid") String usersecondaryroleid)
			throws Exception {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		String response = null;
		try {
			UserService userService = new UserService();
			response = userService.deleteUserSecondaryRole(usersecondaryroleid);
		} catch (Exception ex) {
			response = PrismHandler.handleException(ex);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return response;
	}

	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Path("/jiraissue")
	public String createJiraIssue(@FormParam("input") String input)
			throws Exception {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		String issueKey = null;
		String jiraURL = null;
		String jiraUser = null;
		String jiraPwd = null;
		try {
			jiraURL = E2emfAppUtil.getAppProperty(E2emfConstants.jiraUrl);
			jiraUser = E2emfAppUtil.getAppProperty(E2emfConstants.jiraUser);
			jiraPwd = E2emfAppUtil.getAppProperty(E2emfConstants.jiraPwd);
			Client client = Client.create();
			client.addFilter(new HTTPBasicAuthFilter(jiraUser, jiraPwd));
			WebResource webResource = client.resource(jiraURL);
			ClientResponse response = webResource.type("application/json")
					.post(ClientResponse.class, input);

			String output = response.getEntity(String.class);

			Log.Info("Output from JIRA Server .... \n");
			Log.Info(output);
			JSONObject jsonObj = new JSONObject(output);
			if (jsonObj.has("id")) {
				issueKey = jsonObj.getString("id");
			}

		} catch (Exception e) {
			Log.Error("Exception while retrieving the role action list "
					+ e.getMessage());
			Log.Error(e);
			String failureMessage = "{\"message\":\"failure\"}";
			return failureMessage;
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return issueKey;
	}

	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Path("/getreports")
	public String getReports(@FormParam("id") String id,
			@FormParam("name") String name) throws Exception {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		String response = null;

		try {
			UserService userService = new UserService();
			Gson gson = new Gson();
			if (id.equalsIgnoreCase("reportgrid")) {
				List<Report> listReport = userService.getReportsGridView(id);
				response = gson.toJson(listReport);
			} else {
				ArrayList<LinkedHashMap<String, Object>> reportdata = userService
						.getReports(id);
				response = gson.toJson(reportdata);

				if (name.equalsIgnoreCase("PROFITCENTERRPT")) {
					this.context.setAttribute("profitcenterrpt", response);
				} else if (name.equalsIgnoreCase("COSTCENTERRPT")) {
					this.context.setAttribute("costcenterrpt", response);
				} else if (name.equalsIgnoreCase("MATRPLANTRPT")) {
					this.context.setAttribute("matrplantrpt", response);
				} else if (name.equalsIgnoreCase("GLSYSTEMRPT")) {
					this.context.setAttribute("glsystemrpt", response);
				} else if (name.equalsIgnoreCase("GLCOMCDRPT")) {
					this.context.setAttribute("glcompanyrpt", response);
				} else if (name.equalsIgnoreCase("MATNRRPT")) {
					this.context.setAttribute("matnrrpt", response);
				} else if (name.equalsIgnoreCase("JDEPO")) {
					this.context.setAttribute("jdeporpt", response);
				} else if (name.equalsIgnoreCase("JDESO")) {
					this.context.setAttribute("jdesorpt", response);
				} else if (name.equalsIgnoreCase("JDESOOTHERS")) {
					this.context.setAttribute("jdesoothrrpt", response);
				} else if (name.equalsIgnoreCase("JDEWO")) {
					this.context.setAttribute("jdeworpt", response);
				}
				/*
				 * else if(name.equalsIgnoreCase("PARTNERFUNCTIONRPT")) {
				 * this.context.setAttribute("partnrfuncrpt", response); }
				 */
				else if (name.equalsIgnoreCase("KITSRPT")) {
					this.context.setAttribute("kitsrpt", response);
				} else if (name.equalsIgnoreCase("ERPUNMATCHEDREPORT")) {
					this.context.setAttribute("erpunmatchrpt", response);
				} else if (name.equalsIgnoreCase("EMEA_PO_SSE_RPT")) {
					this.context.setAttribute("EMEA_PO_SSE_RPT", response);
				} else if (name.equalsIgnoreCase("EMEA_QT_RPT")) {
					this.context.setAttribute("EMEA_QT_RPT", response);
				} else if (name.equalsIgnoreCase("EMEA_PPV_RPT")) {
					this.context.setAttribute("EMEA_PPV_RPT", response);
				} else if (name.equalsIgnoreCase("EMEA_SSE_RPT")) {
					this.context.setAttribute("EMEA_SSE_RPT", response);
				} else if (name.equalsIgnoreCase("EMEA_RPT_VOLUME")) {
					this.context.setAttribute("EMEA_RPT_VOLUME", response);
				}
			}
		} catch (Exception ex) {
			response = PrismHandler.handleException(ex);
		}

		/*
		 * JSONArray jsonstr = new JSONArray(json); String xml =
		 * XML.toString(jsonstr);
		 * 
		 * StringBuilder builder = new StringBuilder(); builder
		 * .append("<?xml version=\"1.0\"?>") .append("<Batch>") .append(xml)
		 * .append("</Batch>"); xml = builder.toString();
		 */
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return response;
	}

	/*
	 * private static boolean addAttachmentToIssue(String issueKey, String path)
	 * throws FileNotFoundException, URISyntaxException{
	 * 
	 * File file = new File(path); String imageName = file.getName();
	 * FileInputStream fileStreamPath = new FileInputStream(file);
	 * 
	 * final java.net.URI jiraServerUri = new
	 * java.net.URI("http://jira.relevancelab.com:8080"); NullProgressMonitor pm
	 * = new NullProgressMonitor(); JerseyJiraRestClientFactory factory = new
	 * JerseyJiraRestClientFactory(); JiraRestClient restClient =
	 * factory.createWithBasicHttpAuthentication
	 * (jiraServerUri,"nagaraj","nagaraj"); Issue issue1 =
	 * restClient.getIssueClient().getIssue(issueKey, pm); final java.net.URI
	 * AttachmentUri = new
	 * java.net.URI(jiraServerUri+"/rest/api/2/issue/"+issueKey+"/attachments");
	 * 
	 * System.out.println("URI    :"+issue1.getAttachmentsUri());
	 * restClient.getIssueClient().addAttachment(pm, AttachmentUri,
	 * fileStreamPath, imageName); System.out.println("Completed...");
	 * 
	 * return true;
	 * 
	 * }
	 */

	@POST
	@Produces(MediaType.TEXT_PLAIN)
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Path("/gettestmethod")
	public String testMethod(MultivaluedMap<String, String> parameter1)
			throws Exception {

		String response = " Form parameters :\n";
		try {
			for (String key : parameter1.keySet()) {
				response += key + " : " + parameter1.getFirst(key) + "\n";
			}
			PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		} catch (Exception ex) {
			response = PrismHandler.handleException(ex);
		}
		return response;
	}

	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Path("/getUserPrimaryRole/")
	public String getUserPrimaryRole(@FormParam("username") String username) {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		long startTime = System.currentTimeMillis();
		long endTime = 0L;
		String response = null;
		try {
			UserService userService = new UserService();
			String userPrimaryRole = userService.getUserPrimaryRole(username);
			if (userPrimaryRole.trim().equals(E2emfConstants.ROLE_ADMIN)) {
				response = "{\"isAdmin\":\"true\"}";
			} else {
				response = "{\"isAdmin\":\"false\"}";
			}
		} catch (Exception ex) {
			response = PrismHandler.handleException(ex);
		}
		Log.Info("Returning User Primary Role to Presentation, total Time taken is "
				+ E2emfAppUtil.getTotalElapsedTime(startTime, endTime)
				+ " seconds");
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return response;
	}

	@GET
	@Produces("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")
	@Path("/reportbyfilter")
	public Response downloadReportByFilter(@QueryParam("env") String env,
			@QueryParam("db") String db, @QueryParam("table") String table,
			@QueryParam("wherecondition") String whereCondition)
			throws Exception, IOException {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		int rowNum = 1;
		String fileName = "Report.xlsx";
		StreamingOutput streamOutput = null;
		ResponseBuilder response = null;
		ArrayList<LinkedHashMap<String, String>> reportdata = null;
		try {
			UserService userService = new UserService();
			reportdata = (ArrayList<LinkedHashMap<String, String>>) userService
					.getColumnValueList(env, db, table, whereCondition);
			if (reportdata != null) {
				Set<String> keysListHeader;
				LinkedHashMap<String, String> columnMap = reportdata.get(0);
				keysListHeader = columnMap.keySet();
				final XSSFWorkbook hwb = new XSSFWorkbook();
				XSSFSheet sheet = hwb.createSheet("Report");

				XSSFRow row = sheet.createRow(rowNum);

				CellStyle style = hwb.createCellStyle();
				style.setFillForegroundColor(IndexedColors.YELLOW.getIndex());
				style.setFillPattern(CellStyle.SOLID_FOREGROUND);
				Iterator<String> columnSetIterator = keysListHeader.iterator();
				int x = 0;
				while (columnSetIterator.hasNext()) {
					XSSFCell cell = row.createCell(x);
					cell.setCellStyle(style);
					cell.setCellValue(columnSetIterator.next());
					x++;
				}

				for (int i = 0; i < reportdata.size(); i++) {
					rowNum++;
					LinkedHashMap<String, String> valueMap = reportdata.get(i);
					Set<String> keys = columnMap.keySet();
					Iterator<String> keysIterator = keys.iterator();
					XSSFRow nextRow = sheet.createRow(rowNum);
					int cellNumber = 0;
					while (keysIterator.hasNext()) {
						nextRow.createCell(cellNumber).setCellValue(
								valueMap.get(keysIterator.next()));
						cellNumber++;
					}
				}

				streamOutput = new StreamingOutput() {
					public void write(OutputStream output) throws IOException,
							WebApplicationException {
						try {
							hwb.write(output);
						} catch (Exception e) {
							throw new WebApplicationException(e);
						}
					}
				};
			}
			response = Response.ok((Object) streamOutput,
					"application/ms-excel");
			response.header("Content-Disposition", "attachment; filename="
					+ fileName);
			return response.build();
		} catch (Exception ex) {
			PrismHandler.handleException(ex);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		if (response != null) {
			return response.build();
		} else {
			return null;
		}
	}

	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Path("/getReportQueryByReportName/")
	public String getReportQueryByReportName(@FormParam("name") String name) {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		String response = null;

		try {
			UserService userService = new UserService();
			Gson gson = new Gson();
			Report reportData = userService.getReportQueryByReportName(name);
			response = gson.toJson(reportData);

		} catch (Exception ex) {
			response = PrismHandler.handleException(ex);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return response;
	}

	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Path("/userauthentication")
	public String userAuthentication(@FormParam("userName") String userName,
			@FormParam("password") String password,
			@FormParam("authenticationType") String authenticationType)
			throws Exception {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		User user;
		String authentication;
		String loginNTType = "NT";
		Boolean results;
		Result result = new Result();
		Gson gson = new Gson();
		User users = new User();
		users.setUsername(userName);
		if (authenticationType != null
				&& authenticationType.equals(loginNTType)) {
			UserAuthenticator userAuthenticate = new UserAuthenticator();
			results = userAuthenticate.login(userName, password);
			//authentication = "{\"result\":\"" + results + "\"}";
			if(results){
				UserService userService = new UserService();
				user = userService.getUser(users);
				user.setResponse(true);
				authentication = gson.toJson(user);
			}else{
				result.setResponse(false);
				authentication = gson.toJson(result);
			}
		} else {
			try {
				UserService userService = new UserService();
				
				users.setPassword(password);
				user = userService.userAuthentication(users);
				if (user != null && user.getId() > 0
						&& user.getUsername() != null) {
					user.setUserId(user.getId());
					user.setResponse(true);
					authentication = gson.toJson(user);
				} else {
					result.setResponse(false);
					authentication = gson.toJson(result);
				}

			} catch (Exception ex) {
				authentication = PrismHandler.handleException(ex);
			}
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return authentication;

	}
	
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Path("/getEmailAddressByBusinessRole")
	public String getEmailAddressByBusinessRole(@FormParam("businessRoleName") String businessRoleName) throws Exception {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		Gson gson = new Gson();
		String response = null;
		List<String> usersList = null;
		try {
			UserService userService = new UserService();
			usersList = userService.getEmailAddressByBusinessRole(businessRoleName);
			response = gson.toJson(usersList);
		} catch (Exception ex) {
			response = PrismHandler.handleException(ex);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return response;
	}
}
