package com.relevance.prism.data;


public class DataTableStringView  implements E2emfView {

	String data;
	String recordsTotal;
	String recordsFiltered;
	String summaryData;
	public String getData() {
		return data;
	}
	public void setData(String data) {
		this.data = data;
	}
	public String getRecordsTotal() {
		return recordsTotal;
	}
	public void setRecordsTotal(String recordsTotal) {
		this.recordsTotal = recordsTotal;
	}
	public String getRecordsFiltered() {
		return recordsFiltered;
	}
	public void setRecordsFiltered(String recordsFiltered) {
		this.recordsFiltered = recordsFiltered;
	}
	public String getSummaryData() {
		return summaryData;
	}
	public void setSummaryData(String summaryData) {
		this.summaryData = summaryData;
	}
	
}
