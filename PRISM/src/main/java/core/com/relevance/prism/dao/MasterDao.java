package com.relevance.prism.dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.relevance.prism.data.E2emfBusinessObject;
import com.relevance.prism.data.E2emfView;
import com.relevance.prism.data.FlowData;
import com.relevance.prism.data.GoodsHistoryList;
import com.relevance.prism.data.Master;
import com.relevance.prism.data.MasterItem;
import com.relevance.prism.data.MaterialReportList;
import com.relevance.prism.util.E2emfConstants;
import com.relevance.prism.util.Log;
import com.relevance.prism.util.PrismHandler;
/**
 * 
 * E2emf Master Data Access Object  
 *
 */
public class MasterDao extends BaseDao implements E2emfDao{
	 Connection con = null;
	 Statement stmt = null;
	    
    @Override
	public GoodsHistoryList getGoodsHistory(Object... obj){
		return null;
	}
	@Override
	public E2emfBusinessObject getBusinessObject() {
		return null;
	}

	@Override
	public E2emfBusinessObject getBusinessObject(Object... obj) throws SQLException, NullPointerException, Exception{		
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		String param = null;
		String searchParam = null;
		String masterSearchQuery = null;
		String source = null;
		ResultSet rs = null;
		List<MasterItem>  masterPojoList = new ArrayList<>();
		Master master = new Master();
		
		
		if(obj.length >1){			
			param = (String) obj[0];
			searchParam = (String) obj[1];
			source = (String) obj[2];
			
			Log.Info("Getting Filter details for param " + param + " and search Str : " +searchParam);
		}else{
			Log.Info("No params specified for Master Search ");
		}
		
		if( param != null && searchParam != null){
				masterSearchQuery = dbUtil.buildMasterSearchQuery(param, searchParam, source);
				try{
					con = dbUtil.getHiveConnection(E2emfConstants.e2emfDB);
					if(con != null){
						stmt = con.createStatement();
						System.out.println(masterSearchQuery);
						rs = stmt.executeQuery(masterSearchQuery); 
				        Log.Info("Fetched resultset with fetchsize " + rs.getFetchSize() + " for the searchQuery : \n " + masterSearchQuery );
					   	if(rs != null) {        	
				        	while(rs.next()){
				        		MasterItem masterPojo = new MasterItem();
				        		String name = rs.getString("name");
				        		masterPojo.setName(name);
				        		masterPojo.setId(rs.getString("id"));
				        		masterPojoList.add(masterPojo);
				        	}
				        	master.setMasterDataList(masterPojoList);
				        } 
					} else {
						Log.Info("Connection object is null");
					}
					
				} catch(Exception ex){
					PrismHandler.handleException(ex, true);
				}
				finally {
					PrismHandler.handleFinally(con, rs, stmt);
				}	
					
		}else{
			Log.Info("Criteria not set to fetch Master Details..");
		}
		
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return master;
		}

	@Override
	public List<FlowData> getFlowDataList(Object... obj) {
		return new ArrayList<>();
	}

	@Override
	public String executeQuery(String masterSearchQuery) {
		return null;
	}

	@Override
	public String executeQuery(Object... obj) {
		return null;
	}

	@Override
	public E2emfView getLevel2ViewDetails(Object... obj) {
		return null;
	}

	@Override
	public E2emfView getLevel3ViewDetails(Object... obj) {
		return null;
	}

	@Override
	public E2emfView getDefaultViewDetails(Object... obj) {
		return null;
	}
	@Override
	public MaterialReportList getMaterialReport(Object... obj) {
		return null;
	}
	@Override	
	public E2emfBusinessObject getActions(Object[] obj) throws SQLException, NullPointerException, Exception{
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		Master master = new Master();
		Connection connection = null;
		String param = null;
		String searchParam = null;
		ResultSet rs = null;
		try {
			connection = dbUtil.getPostGresConnection();
			stmt = connection.createStatement();
			if(obj.length >1){			
				param = (String) obj[0];
				searchParam = (String) obj[1];
				
				Log.Info("Getting Filter details for param " + param + " and search Str : " +searchParam);
			}else{
				Log.Info("No params specified for Master Search ");
			}
			String masterSearchQuery = dbUtil.buildActionSearchQuery(param, searchParam);
			System.out.println(masterSearchQuery);
			rs = stmt.executeQuery(masterSearchQuery);
			List<MasterItem>  masterPojoList = new ArrayList<>();
					
			if (rs != null) {
				while (rs.next()) 
				{
					MasterItem masterPojo = new MasterItem();
	        		String name = rs.getString("name");
	        		masterPojo.setName(name);
	        		masterPojo.setId(rs.getString("id"));
	        		masterPojoList.add(masterPojo);
				}
				master.setMasterDataList(masterPojoList);
			}
		} catch(Exception ex){
			PrismHandler.handleException(ex, true);
		}
		finally {
			PrismHandler.handleFinally(connection, rs, stmt);
		}	
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return master;		
	}
	
	public E2emfBusinessObject getRoles(Object[] obj) throws SQLException, NullPointerException, Exception{
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		Master master = new Master();
		Connection connection = null;
		String param = null;
		String searchParam = null;
		ResultSet rs = null;
		try {
			connection = dbUtil.getPostGresConnection();
			stmt = connection.createStatement();
			if(obj.length >1){			
				param = (String) obj[0];
				searchParam = (String) obj[1];
				
				Log.Info("Getting Filter details for param " + param + " and search Str : " +searchParam);
			}else{
				Log.Info("No params specified for Master Search ");
			}
			String masterSearchQuery = dbUtil.buildActionSearchQuery(param, searchParam);
			System.out.println(masterSearchQuery);
			rs = stmt.executeQuery(masterSearchQuery);
			List<MasterItem>  masterPojoList = new ArrayList<>();
					
			if (rs != null) {
				while (rs.next()) 
				{
					MasterItem masterPojo = new MasterItem();
	        		String name = rs.getString("name");
	        		masterPojo.setName(name);
	        		masterPojo.setId(rs.getString("id"));
	        		masterPojoList.add(masterPojo);
				}
				master.setMasterDataList(masterPojoList);
			}
		} catch(Exception ex){
			PrismHandler.handleException(ex, true);
		}
		finally {
			PrismHandler.handleFinally(connection, rs, stmt);
		}	
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return master;		
	}
	@Override
	public E2emfBusinessObject getPrimaryRoles(Object[] obj) throws SQLException, NullPointerException, Exception{
		Master master = new Master();
		Connection connection = null;
		String param = null;
		String searchParam = null;
		ResultSet rs = null;
		try {
			connection = dbUtil.getPostGresConnection();
			stmt = connection.createStatement();
			if(obj.length >1){			
				param = (String) obj[0];
				searchParam = (String) obj[1];
				
				Log.Info("Getting Filter details for param " + param + " and search Str : " +searchParam);
			}else{
				Log.Info("No params specified for Master Search ");
			}
			String masterSearchQuery = dbUtil.buildActionSearchQuery(param, searchParam);
			System.out.println(masterSearchQuery);
			rs = stmt.executeQuery(masterSearchQuery);
			List<MasterItem>  masterPojoList = new ArrayList<>();
					
			if (rs != null) {
				while (rs.next()) 
				{
					MasterItem masterPojo = new MasterItem();
	        		String name = rs.getString("name");
	        		masterPojo.setName(name);
	        		masterPojo.setId(rs.getString("id"));
	        		masterPojoList.add(masterPojo);
				}
				master.setMasterDataList(masterPojoList);
			}
		}  catch(Exception ex){
			PrismHandler.handleException(ex, true);
		}
		finally {
			PrismHandler.handleFinally(connection, rs, stmt);
		}	
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return master;		
	}
}
