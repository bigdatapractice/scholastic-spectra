package com.relevance.prism.service;

import java.util.ArrayList;
import java.util.List;

import com.relevance.prism.dao.AdminDao;
import com.relevance.prism.data.Action;
import com.relevance.prism.data.Role;
import com.relevance.prism.util.Log;
import com.relevance.prism.util.PrismHandler;

public class AdminService extends BaseService {

	public String insertAction(Object... obj) throws Exception
	{
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		Action action = new Action();
		String responseMessage = null;
		try{
			Log.Info("insertAction method called on UserService");
			AdminDao adminDao = new AdminDao();
			action.setAction(obj[0].toString());
			action.setDisplayname(obj[1].toString());
			action.setParent(Integer.parseInt(obj[2].toString()));
			action.setParentName(obj[3].toString());
			action.setSequence(Integer.parseInt(obj[4].toString()));
			action.setIcon(obj[5].toString());
			action.setEnabled(Integer.parseInt(obj[6].toString()));
			action.setSource(obj[7].toString());
			responseMessage = adminDao.insertAction(action);
			Log.Info("Action inserted in DB.");
			return responseMessage;		
		}catch(Exception e){
			PrismHandler.handleException(e, true);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return responseMessage;
	}
	
	public Action updateAction(Object... obj) throws Exception{
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		Action resultAction = new Action();
		try{
			Log.Info("updateAction method called on UserService");
			AdminDao adminDao = new AdminDao();
			Action action = new Action();
			action.setId(Integer.parseInt(obj[0].toString()));
			action.setAction(obj[1].toString());
			action.setDisplayname(obj[2].toString());
			action.setParent(Integer.parseInt(obj[3].toString()));
			action.setParentName(obj[4].toString());
			action.setSequence(Integer.parseInt(obj[5].toString()));
			action.setIcon(obj[6].toString());
			action.setEnabled(Integer.parseInt(obj[7].toString()));
			action.setSource(obj[8].toString());
			resultAction = adminDao.updateAction(action);
			return resultAction;
		}catch(Exception e){
			PrismHandler.handleException(e, true);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return resultAction;
	}
	
	public Action getAction(Object... obj) throws Exception{
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		Action action = null;
		try{
			Log.Info("deleteAction method called on UserService");
			AdminDao adminDao = new AdminDao();
			action = new Action();
			action.setId(Integer.parseInt(obj[0].toString()));
			action = adminDao.getAction(action);
			return action;
		}catch(Exception e){
			PrismHandler.handleException(e, true);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return action;
	}
	
	public String deleteAction(Object... obj) throws Exception{
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		String successMessage = null;
		try{
			Log.Info("deleteAction method called on UserService");
			AdminDao adminDao = new AdminDao();
			Action action = new Action();
			action.setId(Integer.parseInt(obj[0].toString()));
			successMessage = adminDao.deleteAction(action);
			return successMessage;
		}catch(Exception e){
			PrismHandler.handleException(e, true);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return successMessage;
	}
	
	public List<Action> getActionList() throws Exception{
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		List<Action> actionList = null;
		try{
			Log.Info("getActionList method called on UserService");
			AdminDao adminDao = new AdminDao();
			actionList = adminDao.getActionList();
			return actionList;
		}catch(Exception e){
			PrismHandler.handleException(e, true);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return actionList;
	}
	
	public String insertRoleAction(Object... obj) throws Exception{
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		AdminDao adminDao = new AdminDao();
		String responseMessage = null;
		try{
			Role role = new Role();
			role.setRole(obj[0].toString());
			role.setActionId(Long.parseLong(obj[1].toString()));
			Log.Info("insertRoleAction method called on UserService");
			responseMessage = adminDao.insertRoleAction(role);
			return responseMessage;	
		}catch(Exception e){
			PrismHandler.handleException(e, true);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return responseMessage;
	}
	
	public String deleteRoleAction(Object... obj) throws Exception{
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		String successMessage = null;
		try{
			Log.Info("deleteRoleAction method called on UserService");
			AdminDao adminDao = new AdminDao();
			Role role = new Role();
			role.setRoleActionId(Integer.parseInt(obj[0].toString()));
			//role.setRole(obj[1].toString());
			//role.setActionId(Long.parseLong(obj[2].toString()));
			successMessage = adminDao.deleteRoleAction(role);
			return successMessage;
		}catch(Exception e){
			PrismHandler.handleException(e, true);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return successMessage;
	}
	
	public List<Role> getRoleActionList() throws Exception{
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		List<Role> roleList = new ArrayList<Role>();
		try{
			Log.Info("getRoleActionList method called on AdminService");
			AdminDao adminDao = new AdminDao();
			roleList = adminDao.getRoleActionList();
			return roleList;
		}catch(Exception e){
			PrismHandler.handleException(e, true);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return roleList;
	}
	
	public String insertLookupValue(Object... obj) throws Exception{
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		String responseMessage = null;
		try{
			Log.Info("insertLookupValue method called on UserService");
			AdminDao adminDao = new AdminDao();
			String key = obj[0].toString();
			String displayname = obj[1].toString();
			responseMessage = adminDao.insertLookupValue(key, displayname);
			Log.Info("Lookup Value inserted in DB.");
			return responseMessage;		
		}catch(Exception e){
			PrismHandler.handleException(e, true);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return responseMessage;
	}
	
	public String updateLookupValue(Object... obj) throws Exception{
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		String key = null;
		String displayname = null;
		String responseMessage = null;
		try{
			Log.Info("updateLookupValue method called on UserService");
			AdminDao adminDao = new AdminDao();
			key = obj[0].toString();
			displayname = obj[1].toString();
			responseMessage = adminDao.updateLookupValue(key, displayname);
			return responseMessage;
		}catch(Exception e){
			PrismHandler.handleException(e, true);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return responseMessage;
	}
	
	public String deleteLookupValue(Object... obj) throws Exception{
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		String key = null;
		//String displayname = null;
		String responseMessage = null;
		try{
			Log.Info("deleteLookupValue method called on UserService");
			AdminDao adminDao = new AdminDao();
			key = obj[0].toString();
			responseMessage = adminDao.deleteLookupValue(key);
			return responseMessage;
		}catch(Exception e){
			PrismHandler.handleException(e, true);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return responseMessage;
	}
	
	public String getLookUpValueList() throws Exception{
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		AdminDao adminDao = new AdminDao();
		String resultString = null;
		try{
			Log.Info("getLookUpValues method called on UserService");
			resultString = adminDao.getLookUpValueList();			
			/*if(resultString == null || resultString == "" || resultString == "[]" || resultString.length() == 0){
				return "{\"message\":\"The logged in user does not have any role assigned\"}";
			}*/
			return resultString;
		}catch(Exception e){
			PrismHandler.handleException(e, true);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return resultString;
	}
	
	public Role getLogo(Object... obj) throws Exception{
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		AdminDao adminDao = new AdminDao();
		Role resultObj = new Role();
		try{
			String name = obj[0].toString();
			Log.Info("getLookUpValues method called on UserService");
			resultObj = adminDao.getLogo(name);			
			return resultObj;
		}catch(Exception e){
			PrismHandler.handleException(e, true);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return null;
	}
}
