package com.relevance.prism.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.apache.commons.lang.StringUtils;

import com.relevance.prism.data.RAOracleOutput;
import com.relevance.prism.data.RAOutput;
import com.relevance.prism.data.Relationship;
import com.relevance.prism.util.PrismHandler;

public class RegulatoryAffairDao extends BaseDao {
	public static final String QfindRA = "QFIND_RA";

	public String generateMapping() throws Exception {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		List<RAOutput> raOutputList = new ArrayList<>();
		StringBuilder query = new StringBuilder();
		ResultSet rs = null;
		Connection con = null;
		Statement stmt = null;
		try {

			HashMap<String, HashSet<Relationship>> kToD = getRelationShip("knumber");
			HashMap<String, HashSet<Relationship>> dToK = getRelationShip("d_number");

			HashMap<Integer, HashSet<String>> gudidDnumber = getGUDIDDnumber();
			HashMap<Integer, HashSet<String>> gudidKnumber = getGUDIDKnumber();
			HashMap<Integer, HashSet<String>> gudidProductCodes = getGUDIProductCodes();

			con = dbUtil.getPostGresConnection(QfindRA);
			query.append("SELECT id FROM gudid_device");
			if (con != null) {
				stmt = con.createStatement(java.sql.ResultSet.TYPE_FORWARD_ONLY, java.sql.ResultSet.CONCUR_READ_ONLY);
				rs = stmt.executeQuery(query.toString());
				while (rs.next()) {
					int refid = rs.getInt("id");
					HashSet<String> dNumbers = gudidDnumber.get(refid);
					HashSet<String> kNumbers = gudidKnumber.get(refid);
					HashSet<String> productCodes = gudidProductCodes.get(refid);
					int dCount = dNumbers != null ? dNumbers.size() : 0;
					int kCount = kNumbers != null ? kNumbers.size() : 0;
					if (dNumbers != null) {
						Iterator<String> dIterator = dNumbers.iterator();
						while (dIterator.hasNext()) {
							String dNumber = dIterator.next();
							RAOutput raOutput = new RAOutput();
							raOutput.setId(refid);
							raOutput.setdCount(dCount);
							raOutput.setkCount(kCount);
							raOutput.setGudidDnumber(dNumber);
							HashSet<Relationship> kNumbersSheet = dToK.get(dNumber);
							boolean knumberFound = false;
							if (kNumbersSheet != null) {
								Iterator<Relationship> kNumbersSheetIterator = kNumbersSheet.iterator();
								while (kNumbersSheetIterator.hasNext()) {
									Relationship kNumberRelationShip = kNumbersSheetIterator.next();
									String sheetKnumber = kNumberRelationShip.getkNumber();
									String sheetProductCodes = kNumberRelationShip.getProductCodes();
									HashSet<String> sheetProductCodesSet = new HashSet<>(
											Arrays.asList(sheetProductCodes.split(";")));
									if (kNumbers != null && kNumbers.contains(sheetKnumber)) {
										raOutput.setSheetKnumber(sheetKnumber);
										raOutput.setSheetKnumberFilledInternally(
												kNumberRelationShip.getkNumberFilledInternally());
										raOutput.setSheetDeviceClass(kNumberRelationShip.getDeviceClass());
										raOutput.setSheetdeviceListingStatus(
												kNumberRelationShip.getDeviceListingStatus());
										raOutput.setSheetOriginalApplicant(kNumberRelationShip.getOriginalApplicant());
										raOutput.setSheetReceviedDate(kNumberRelationShip.getReceviedDate());
										raOutput.setSheetVlearedDate(kNumberRelationShip.getClearedDate());
										raOutput.setKnumberMatchStatus("Yes");
										kNumbers.remove(sheetKnumber);
										raOutput.setSheetDnumber(dNumber);
										raOutput.setGudidKnumber(sheetKnumber);
										raOutput.setDnumberMatchStatus("Yes");
										raOutput.setGudidProductCode(StringUtils.join(productCodes, ";"));
										raOutput.setSheetProductCode(StringUtils.join(sheetProductCodesSet, ";"));
										if (sheetProductCodes != null) {
											if (isIdenticalHashSet(sheetProductCodesSet, productCodes))
												raOutput.setProductCodeMatchStatus("Yes");
											else
												raOutput.setProductCodeMatchStatus("No");

										}
										knumberFound = true;
										break;
									}
									if (sheetProductCodes != null && sheetProductCodes.length() > 0) {
										if (isIdenticalHashSet(sheetProductCodesSet, productCodes))
											raOutput.setProductCodeMatchStatus("Yes");
										else
											raOutput.setProductCodeMatchStatus("No");

									} else {
										raOutput.setProductCodeMatchStatus("Not Found");
									}
									raOutput.setSheetKnumberFilledInternally(
											kNumberRelationShip.getkNumberFilledInternally());
									raOutput.setSheetDeviceClass(kNumberRelationShip.getDeviceClass());
									raOutput.setSheetdeviceListingStatus(kNumberRelationShip.getDeviceListingStatus());
									raOutput.setSheetOriginalApplicant(kNumberRelationShip.getOriginalApplicant());
									raOutput.setSheetReceviedDate(kNumberRelationShip.getReceviedDate());
									raOutput.setSheetVlearedDate(kNumberRelationShip.getClearedDate());

									raOutput.setGudidProductCode(StringUtils.join(productCodes, ";"));
									raOutput.setSheetProductCode(StringUtils.join(sheetProductCodesSet, ";"));
									raOutput.setSheetKnumber(sheetKnumber);
									raOutput.setDnumberMatchStatus("No");

								}
							} else {
								raOutput.setDnumberMatchStatus("Not Found");
								raOutput.setProductCodeMatchStatus("Not Found");
							}

							if (!knumberFound) {
								if ((kNumbers != null && kNumbers.size() == 1)
										&& (dIterator == null || !dIterator.hasNext())) {
									String kNumber = kNumbers.toArray()[0].toString();

									raOutput.setGudidKnumber(kNumber);
									HashSet<Relationship> dNumbersSheet = kToD.get(kNumber);
									if (dNumbersSheet != null) {
										Iterator<Relationship> dNumbersSheetIterator = dNumbersSheet.iterator();
										while (dNumbersSheetIterator.hasNext()) {
											Relationship dNumberRelationShip = dNumbersSheetIterator.next();
											String sheetdNumber = dNumberRelationShip.getDeviceListing();
											String sheetProductCodes = dNumberRelationShip.getProductCodes();
											HashSet<String> sheetProductCodesSet = new HashSet<>(
													Arrays.asList(sheetProductCodes.split(";")));
											if (dNumbers != null && dNumbers.contains(sheetdNumber)) {
												raOutput.setSheetDnumber(sheetdNumber);
												raOutput.setDnumberMatchStatus("Yes");
												dNumbers.remove(sheetdNumber);
												raOutput.setSheetDnumber(dNumber);
												raOutput.setGudidDnumber(sheetdNumber);
												raOutput.setKnumberMatchStatus("Yes");
												raOutput.setSheetKnumberFilledInternally(
														dNumberRelationShip.getkNumberFilledInternally());
												raOutput.setSheetDeviceClass(dNumberRelationShip.getDeviceClass());
												raOutput.setSheetdeviceListingStatus(
														dNumberRelationShip.getDeviceListingStatus());
												raOutput.setSheetOriginalApplicant(
														dNumberRelationShip.getOriginalApplicant());
												raOutput.setSheetReceviedDate(dNumberRelationShip.getReceviedDate());
												raOutput.setSheetVlearedDate(dNumberRelationShip.getClearedDate());
												raOutput.setGudidProductCode(StringUtils.join(productCodes, ";"));
												if (sheetProductCodesSet != null)
													raOutput.setSheetProductCode(
															StringUtils.join(sheetProductCodesSet, ";"));
												if (sheetProductCodes != null && sheetProductCodes.length() > 0) {
													if (isIdenticalHashSet(sheetProductCodesSet, productCodes))
														raOutput.setProductCodeMatchStatus("Yes");
													else
														raOutput.setProductCodeMatchStatus("No");
												} else {
													raOutput.setProductCodeMatchStatus("Not Found");
												}

												break;
											}
											if (sheetProductCodes != null && sheetProductCodes.length() > 0) {
												if (isIdenticalHashSet(sheetProductCodesSet, productCodes))
													raOutput.setProductCodeMatchStatus("Yes");
												else
													raOutput.setProductCodeMatchStatus("No");

											} else {
												raOutput.setProductCodeMatchStatus("Not Found");
											}
											raOutput.setSheetKnumberFilledInternally(
													dNumberRelationShip.getkNumberFilledInternally());
											raOutput.setSheetDeviceClass(dNumberRelationShip.getDeviceClass());
											raOutput.setSheetdeviceListingStatus(
													dNumberRelationShip.getDeviceListingStatus());
											raOutput.setSheetOriginalApplicant(
													dNumberRelationShip.getOriginalApplicant());
											raOutput.setSheetReceviedDate(dNumberRelationShip.getReceviedDate());
											raOutput.setSheetVlearedDate(dNumberRelationShip.getClearedDate());
											raOutput.setGudidProductCode(StringUtils.join(productCodes, ";"));
											raOutput.setSheetProductCode(StringUtils.join(sheetProductCodesSet, ";"));

											raOutput.setSheetDnumber(sheetdNumber);
											raOutput.setKnumberMatchStatus("No");
										}
									} else {
										raOutput.setKnumberMatchStatus("Not Found");
									}
									kNumbers.remove(kNumber);
								} else {
									raOutput.setKnumberMatchStatus("Not Found");
								}
							}
							raOutputList.add(raOutput);
							dIterator.remove();
						}
					} else if (kNumbers == null || kNumbers.size() == 0) {
						RAOutput raOutput = new RAOutput();
						raOutput.setId(refid);
						raOutput.setdCount(dCount);
						raOutput.setkCount(kCount);
						raOutput.setKnumberMatchStatus("Not Found");
						raOutput.setDnumberMatchStatus("Not Found");
						raOutput.setProductCodeMatchStatus("Not Found");
						raOutputList.add(raOutput);
					}
					if (kNumbers != null) {
						Iterator<String> kIterator = kNumbers.iterator();
						while (kIterator.hasNext()) {
							String kNumber = kIterator.next();
							RAOutput raOutput = new RAOutput();
							raOutput.setId(refid);
							raOutput.setdCount(dCount);
							raOutput.setkCount(kCount);
							raOutput.setGudidKnumber(kNumber);

							raOutput.setGudidKnumber(kNumber);
							HashSet<Relationship> dNumbersSheet = kToD.get(kNumber);
							if (dNumbersSheet != null) {
								Iterator<Relationship> dNumbersSheetIterator = dNumbersSheet.iterator();
								while (dNumbersSheetIterator.hasNext()) {
									Relationship dNumberRelationShip = dNumbersSheetIterator.next();
									String sheetdNumber = dNumberRelationShip.getDeviceListing();
									String sheetProductCodes = dNumberRelationShip.getProductCodes();
									HashSet<String> sheetProductCodesSet = new HashSet<>(
											Arrays.asList(sheetProductCodes.split(";")));
									if (dNumbers != null && dNumbers.contains(sheetdNumber)) {
										raOutput.setSheetDnumber(sheetdNumber);
										raOutput.setDnumberMatchStatus("Yes");
										dNumbers.remove(sheetdNumber);
										raOutput.setSheetDnumber(sheetdNumber);
										raOutput.setKnumberMatchStatus("Yes");
										raOutput.setSheetKnumberFilledInternally(
												dNumberRelationShip.getkNumberFilledInternally());
										raOutput.setSheetDeviceClass(dNumberRelationShip.getDeviceClass());
										raOutput.setSheetdeviceListingStatus(
												dNumberRelationShip.getDeviceListingStatus());
										raOutput.setSheetOriginalApplicant(dNumberRelationShip.getOriginalApplicant());
										raOutput.setSheetReceviedDate(dNumberRelationShip.getReceviedDate());
										raOutput.setSheetVlearedDate(dNumberRelationShip.getClearedDate());

										raOutput.setGudidProductCode(StringUtils.join(productCodes, ";"));
										raOutput.setSheetProductCode(StringUtils.join(sheetProductCodesSet, ";"));
										if (sheetProductCodes != null && sheetProductCodes.length() > 0) {
											if (isIdenticalHashSet(sheetProductCodesSet, productCodes))
												raOutput.setProductCodeMatchStatus("Yes");
											else
												raOutput.setProductCodeMatchStatus("No");
										} else {
											raOutput.setProductCodeMatchStatus("Not Found");
										}

										break;
									}
									if (sheetProductCodes != null && sheetProductCodes.length() > 0) {
										if (isIdenticalHashSet(sheetProductCodesSet, productCodes))
											raOutput.setProductCodeMatchStatus("Yes");
										else
											raOutput.setProductCodeMatchStatus("No");

									} else {
										raOutput.setProductCodeMatchStatus("Not Found");
									}
									raOutput.setSheetKnumberFilledInternally(
											dNumberRelationShip.getkNumberFilledInternally());
									raOutput.setSheetDeviceClass(dNumberRelationShip.getDeviceClass());
									raOutput.setSheetdeviceListingStatus(dNumberRelationShip.getDeviceListingStatus());
									raOutput.setSheetOriginalApplicant(dNumberRelationShip.getOriginalApplicant());
									raOutput.setSheetReceviedDate(dNumberRelationShip.getReceviedDate());
									raOutput.setSheetVlearedDate(dNumberRelationShip.getClearedDate());

									raOutput.setGudidProductCode(StringUtils.join(productCodes, ";"));
									raOutput.setSheetProductCode(StringUtils.join(sheetProductCodesSet, ";"));

									raOutput.setSheetDnumber(sheetdNumber);
									raOutput.setKnumberMatchStatus("No");
									raOutput.setDnumberMatchStatus("Not Found");
								}
							} else {
								raOutput.setKnumberMatchStatus("Not Found");
								raOutput.setDnumberMatchStatus("Not Found");
							}
							raOutputList.add(raOutput);
						}
					}
				}
			}
			pushRAOutputToDB(raOutputList);
		} catch (Exception ex) {
			ex.printStackTrace();
			PrismHandler.handleException(ex, true);
		} finally {
			PrismHandler.handleFinally(con, rs, stmt);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return null;
	}

	public boolean isIdenticalHashSet(Set<String> h1, Set<String> h2) {
		if (h1.size() != h2.size()) {
			return false;
		} else if (h1.containsAll(h2) && h2.containsAll(h1))
			return true; // will only return true if sets are equal
		else
			return false;
	}

	private String pushRAOutputToDB(List<RAOutput> raOutputList) throws Exception {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		Connection con = null;
		PreparedStatement pstmt = null;
		int counter = 0;
		int raOutputLength;
		Statement stmt;
		try {
			raOutputLength = raOutputList.size();
			con = dbUtil.getPostGresConnection(QfindRA);
			stmt = con.createStatement();
			stmt.executeUpdate("DELETE FROM ra_output");
			stmt.close();
			pstmt = con.prepareStatement(
					"INSERT INTO ra_output(refid, d_count, k_count, gudid_dnumber, gudid_knumber, gudid_procodes, sheet_d_number, sheet_k_number, sheet_procodes, k_match_status, d_match_status, procode_match_status,sheet_original_applicant, sheet_letter_filled_internally_k510, sheet_received_date, sheet_cleared_date, sheet_device_class, sheet_device_listing_status) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
			for (RAOutput raOutput : raOutputList) {
				counter++;
				pstmt.setInt(1, raOutput.getId());
				pstmt.setInt(2, raOutput.getdCount());
				pstmt.setInt(3, raOutput.getkCount());
				pstmt.setString(4, raOutput.getGudidDnumber());
				pstmt.setString(5, raOutput.getGudidKnumber());
				pstmt.setString(6, raOutput.getGudidProductCode());
				pstmt.setString(7, raOutput.getSheetDnumber());
				pstmt.setString(8, raOutput.getSheetKnumber());
				pstmt.setString(9, raOutput.getSheetProductCode());
				pstmt.setString(10, raOutput.getKnumberMatchStatus());
				pstmt.setString(11, raOutput.getDnumberMatchStatus());
				pstmt.setString(12, raOutput.getProductCodeMatchStatus());
				pstmt.setString(13, raOutput.getSheetOriginalApplicant());
				pstmt.setString(14, raOutput.getSheetKnumberFilledInternally());
				pstmt.setString(15, raOutput.getSheetReceviedDate());
				pstmt.setString(16, raOutput.getSheetVlearedDate());
				pstmt.setString(17, raOutput.getSheetDeviceClass());
				pstmt.setString(18, raOutput.getSheetdeviceListingStatus());
				pstmt.addBatch();
				if (counter % 1000 == 0 || counter == raOutputLength) {
					pstmt.executeBatch();
				}
			}
		} catch (Exception ex) {
			PrismHandler.handleException(ex, true);
		} finally {
			PrismHandler.handleFinally(con, null, pstmt);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return null;
	}

	private HashMap<String, HashSet<Relationship>> getRelationShip(String type) throws Exception {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		HashMap<String, HashSet<Relationship>> relationshipMap = new HashMap<>();

		StringBuilder query = new StringBuilder();
		ResultSet rs = null;
		Connection con = null;
		Statement stmt = null;
		try {
			con = dbUtil.getPostGresConnection(QfindRA);
			query.append("SELECT * from knumber_dnumber_integra");
			if (con != null) {
				stmt = con.createStatement(java.sql.ResultSet.TYPE_FORWARD_ONLY, java.sql.ResultSet.CONCUR_READ_ONLY);
				rs = stmt.executeQuery(query.toString());
				while (rs.next()) {
					Relationship relationship = new Relationship();
					HashSet<Relationship> relationshipList = new HashSet<>();
					String typeNumber = rs.getString(type);
					relationship.setkNumber(rs.getString("knumber"));
					relationship.setkNumberFilledInternally(rs.getString("letter_filled_internally_k510"));
					relationship.setOriginalApplicant(rs.getString("original_applicant"));
					relationship.setReceviedDate(rs.getString("received_date"));
					relationship.setClearedDate(rs.getString("cleared_date"));
					relationship.setProductCodes(rs.getString("product_codes"));
					relationship.setDeviceClass(rs.getString("device_class"));
					relationship.setDeviceListing(rs.getString("d_number"));
					relationship.setDeviceListingStatus(rs.getString("device_listing_status"));
					if (relationshipMap.containsKey(typeNumber)) {
						relationshipList = relationshipMap.get(typeNumber);
					}
					relationshipList.add(relationship);
					relationshipMap.put(typeNumber, relationshipList);
				}
			}
		} catch (Exception ex) {
			ex.printStackTrace();
			PrismHandler.handleException(ex, true);
		} finally {
			PrismHandler.handleFinally(con, rs, stmt);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return relationshipMap;
	}

	private HashMap<String, HashSet<Relationship>> getRelationShipFURLS(String type) throws Exception {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		HashMap<String, HashSet<Relationship>> relationshipMap = new HashMap<>();

		StringBuilder query = new StringBuilder();
		ResultSet rs = null;
		Connection con = null;
		Statement stmt = null;
		try {
			con = dbUtil.getPostGresConnection(QfindRA);
			query.append("SELECT * from fda_listing_ag");
			if (con != null) {
				stmt = con.createStatement(java.sql.ResultSet.TYPE_FORWARD_ONLY, java.sql.ResultSet.CONCUR_READ_ONLY);
				rs = stmt.executeQuery(query.toString());
				while (rs.next()) {
					Relationship relationship = new Relationship();
					HashSet<Relationship> relationshipList = new HashSet<>();
					String typeNumber = rs.getString(type);
					relationship.setkNumber(rs.getString("premarket_submission_number"));
					relationship.setProductCodes(rs.getString("product_codes"));
					relationship.setDeviceClass(rs.getString("deviceclasses"));
					relationship.setDeviceListing(rs.getString("listing_number"));
					if (relationshipMap.containsKey(typeNumber)) {
						relationshipList = relationshipMap.get(typeNumber);
					}
					relationshipList.add(relationship);
					relationshipMap.put(typeNumber, relationshipList);
				}
			}
		} catch (Exception ex) {
			ex.printStackTrace();
			PrismHandler.handleException(ex, true);
		} finally {
			PrismHandler.handleFinally(con, rs, stmt);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return relationshipMap;
	}
	private HashMap<Integer, HashSet<String>> getGUDIDDnumber() throws Exception {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		HashMap<Integer, HashSet<String>> gudidDnumberMap = new HashMap<>();

		StringBuilder query = new StringBuilder();
		ResultSet rs = null;
		Connection con = null;
		Statement stmt = null;
		try {
			con = dbUtil.getPostGresConnection(QfindRA);
			query.append("SELECT * from gudid_fda_listing");
			if (con != null) {
				stmt = con.createStatement(java.sql.ResultSet.TYPE_FORWARD_ONLY, java.sql.ResultSet.CONCUR_READ_ONLY);
				rs = stmt.executeQuery(query.toString());
				while (rs.next()) {
					HashSet<String> dNumbers = new HashSet<>();
					Integer typeNumber = rs.getInt("refid");
					if (gudidDnumberMap.containsKey(typeNumber)) {
						dNumbers = gudidDnumberMap.get(typeNumber);
					}
					dNumbers.add(rs.getString("fda_listing_number"));
					gudidDnumberMap.put(typeNumber, dNumbers);
				}
			}
		} catch (Exception ex) {
			PrismHandler.handleException(ex, true);
		} finally {
			PrismHandler.handleFinally(con, rs, stmt);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return gudidDnumberMap;
	}

	private HashMap<Integer, HashSet<String>> getGUDIDKnumber() throws Exception {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		HashMap<Integer, HashSet<String>> gudidKnumberMap = new HashMap<>();

		StringBuilder query = new StringBuilder();
		ResultSet rs = null;
		Connection con = null;
		Statement stmt = null;
		try {
			con = dbUtil.getPostGresConnection(QfindRA);
			query.append("SELECT * from gudid_pre_market_submission");
			if (con != null) {
				stmt = con.createStatement(java.sql.ResultSet.TYPE_FORWARD_ONLY, java.sql.ResultSet.CONCUR_READ_ONLY);
				rs = stmt.executeQuery(query.toString());
				while (rs.next()) {
					HashSet<String> kNumbers = new HashSet<>();
					Integer typeNumber = rs.getInt("refid");
					if (gudidKnumberMap.containsKey(typeNumber)) {
						kNumbers = gudidKnumberMap.get(typeNumber);
					}
					kNumbers.add(rs.getString("submission_number"));
					gudidKnumberMap.put(typeNumber, kNumbers);
				}
			}
		} catch (Exception ex) {
			PrismHandler.handleException(ex, true);
		} finally {
			PrismHandler.handleFinally(con, rs, stmt);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return gudidKnumberMap;
	}

	private HashMap<Integer, HashSet<String>> getGUDIProductCodes() throws Exception {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		HashMap<Integer, HashSet<String>> gudidProductCodeMap = new HashMap<>();

		StringBuilder query = new StringBuilder();
		ResultSet rs = null;
		Connection con = null;
		Statement stmt = null;
		try {
			con = dbUtil.getPostGresConnection(QfindRA);
			query.append("SELECT * from gudid_product_code");
			if (con != null) {
				stmt = con.createStatement(java.sql.ResultSet.TYPE_FORWARD_ONLY, java.sql.ResultSet.CONCUR_READ_ONLY);
				rs = stmt.executeQuery(query.toString());
				while (rs.next()) {
					HashSet<String> productCodes = new HashSet<>();
					Integer typeNumber = rs.getInt("refid");
					if (gudidProductCodeMap.containsKey(typeNumber)) {
						productCodes = gudidProductCodeMap.get(typeNumber);
					}
					productCodes.add(rs.getString("product_code"));
					gudidProductCodeMap.put(typeNumber, productCodes);
				}
			}
		} catch (Exception ex) {
			PrismHandler.handleException(ex, true);
		} finally {
			PrismHandler.handleFinally(con, rs, stmt);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return gudidProductCodeMap;
	}

	public String generateOracleItemMapping() throws Exception {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		List<RAOracleOutput> raOracleOutputList = new ArrayList<>();
		StringBuilder query = new StringBuilder();
		ResultSet rs = null;
		Connection con = null;
		Statement stmt = null;
		try {

			HashMap<String, HashSet<Relationship>> kToD = getRelationShipFURLS("premarket_submission_number");
			HashMap<String, HashSet<Relationship>> dToK = getRelationShipFURLS("listing_number");

			con = dbUtil.getPostGresConnection(QfindRA);
			query.append("SELECT * FROM oracle_item_master_ra");
			if (con != null) {
				stmt = con.createStatement(java.sql.ResultSet.TYPE_FORWARD_ONLY, java.sql.ResultSet.CONCUR_READ_ONLY);
				rs = stmt.executeQuery(query.toString());
				while (rs.next()) {
					String refid = rs.getString("item_number");
					String dNumberString = rs.getString("vendor_device_num");
					String kNumberString = rs.getString("k_num");
					String productCodeString = rs.getString("fda_code");
					String gmdnCode = rs.getString("gmdn_code");
					HashSet<String> dNumbers = new HashSet<>();
					HashSet<String> kNumbers = new HashSet<>();
					HashSet<String> productCodes = new HashSet<>();
					
					if (dNumberString != null && !dNumberString.isEmpty())
						dNumbers = new HashSet<>(Arrays.asList(dNumberString.split(",")));
					
					if (kNumberString != null && !kNumberString.isEmpty())
						kNumbers = new HashSet<>(Arrays.asList(kNumberString.split(",")));
					
					if (productCodeString != null && !productCodeString.isEmpty()){
						String[] productCodeArray =  productCodeString.split(",");
						for (String proCode : productCodeArray){
							if (proCode != null && proCode.length() == 5){
								productCodes.add(proCode.substring(proCode.length() - 3));
							}else{
								productCodes.add(proCode);
							}
						}
					}
					int dCount = dNumbers != null ? dNumbers.size() : 0;
					int kCount = kNumbers != null ? kNumbers.size() : 0;
					if (dNumbers != null) {
						Iterator<String> dIterator = dNumbers.iterator();
						while (dIterator.hasNext()) {
							String dNumber = dIterator.next();
							RAOracleOutput raOracleOutput = new RAOracleOutput();
							raOracleOutput.setId(refid);
							raOracleOutput.setdCount(dCount);
							raOracleOutput.setkCount(kCount);
							raOracleOutput.setOracleDnumber(dNumber);
							raOracleOutput.setOracleGmdnCode(gmdnCode);
							HashSet<Relationship> kNumbersSheet = dToK.get(dNumber);
							boolean knumberFound = false;
							if (kNumbersSheet != null) {
								Iterator<Relationship> kNumbersSheetIterator = kNumbersSheet.iterator();
								while (kNumbersSheetIterator.hasNext()) {
									Relationship kNumberRelationShip = kNumbersSheetIterator.next();
									String sheetKnumber = kNumberRelationShip.getkNumber();
									String sheetProductCodes = kNumberRelationShip.getProductCodes();
									HashSet<String> sheetProductCodesSet = new HashSet<>(
											Arrays.asList(sheetProductCodes.split(";")));
									if (kNumbers != null && kNumbers.contains(sheetKnumber)) {
										raOracleOutput.setSheetKnumber(sheetKnumber);
										raOracleOutput.setSheetKnumberFilledInternally(
												kNumberRelationShip.getkNumberFilledInternally());
										raOracleOutput.setSheetDeviceClass(kNumberRelationShip.getDeviceClass());
										raOracleOutput.setSheetdeviceListingStatus(
												kNumberRelationShip.getDeviceListingStatus());
										raOracleOutput.setSheetOriginalApplicant(kNumberRelationShip.getOriginalApplicant());
										raOracleOutput.setSheetReceviedDate(kNumberRelationShip.getReceviedDate());
										raOracleOutput.setSheetVlearedDate(kNumberRelationShip.getClearedDate());
										raOracleOutput.setKnumberMatchStatus("Yes");
										kNumbers.remove(sheetKnumber);
										raOracleOutput.setSheetDnumber(dNumber);
										raOracleOutput.setOracleKnumber(sheetKnumber);
										raOracleOutput.setDnumberMatchStatus("Yes");
										raOracleOutput.setOracleProductCode(StringUtils.join(productCodes, ";"));
										raOracleOutput.setSheetProductCode(StringUtils.join(sheetProductCodesSet, ";"));
										if (sheetProductCodes != null) {
											if (isIdenticalHashSet(sheetProductCodesSet, productCodes))
												raOracleOutput.setProductCodeMatchStatus("Yes");
											else
												raOracleOutput.setProductCodeMatchStatus("No");

										}
										knumberFound = true;
										break;
									}
									if (sheetProductCodes != null && sheetProductCodes.length() > 0) {
										if (isIdenticalHashSet(sheetProductCodesSet, productCodes))
											raOracleOutput.setProductCodeMatchStatus("Yes");
										else
											raOracleOutput.setProductCodeMatchStatus("No");

									} else {
										raOracleOutput.setProductCodeMatchStatus("Not Found");
									}
									raOracleOutput.setSheetKnumberFilledInternally(
											kNumberRelationShip.getkNumberFilledInternally());
									raOracleOutput.setSheetDeviceClass(kNumberRelationShip.getDeviceClass());
									raOracleOutput.setSheetdeviceListingStatus(kNumberRelationShip.getDeviceListingStatus());
									raOracleOutput.setSheetOriginalApplicant(kNumberRelationShip.getOriginalApplicant());
									raOracleOutput.setSheetReceviedDate(kNumberRelationShip.getReceviedDate());
									raOracleOutput.setSheetVlearedDate(kNumberRelationShip.getClearedDate());

									raOracleOutput.setOracleProductCode(StringUtils.join(productCodes, ";"));
									raOracleOutput.setSheetProductCode(StringUtils.join(sheetProductCodesSet, ";"));
									raOracleOutput.setSheetKnumber(sheetKnumber);
									raOracleOutput.setDnumberMatchStatus("No");

								}
							} else {
								raOracleOutput.setDnumberMatchStatus("Not Found");
								raOracleOutput.setProductCodeMatchStatus("Not Found");
							}

							if (!knumberFound) {
								if ((kNumbers != null && kNumbers.size() == 1)
										&& (dIterator == null || !dIterator.hasNext())) {
									String kNumber = kNumbers.toArray()[0].toString();

									raOracleOutput.setOracleKnumber(kNumber);
									HashSet<Relationship> dNumbersSheet = kToD.get(kNumber);
									if (dNumbersSheet != null) {
										Iterator<Relationship> dNumbersSheetIterator = dNumbersSheet.iterator();
										while (dNumbersSheetIterator.hasNext()) {
											Relationship dNumberRelationShip = dNumbersSheetIterator.next();
											String sheetdNumber = dNumberRelationShip.getDeviceListing();
											String sheetProductCodes = dNumberRelationShip.getProductCodes();
											HashSet<String> sheetProductCodesSet = new HashSet<>(
													Arrays.asList(sheetProductCodes.split(";")));
											if (dNumbers != null && dNumbers.contains(sheetdNumber)) {
												raOracleOutput.setSheetDnumber(sheetdNumber);
												raOracleOutput.setDnumberMatchStatus("Yes");
												dNumbers.remove(sheetdNumber);
												raOracleOutput.setSheetDnumber(dNumber);
												raOracleOutput.setOracleDnumber(sheetdNumber);
												raOracleOutput.setKnumberMatchStatus("Yes");
												raOracleOutput.setSheetKnumberFilledInternally(
														dNumberRelationShip.getkNumberFilledInternally());
												raOracleOutput.setSheetDeviceClass(dNumberRelationShip.getDeviceClass());
												raOracleOutput.setSheetdeviceListingStatus(
														dNumberRelationShip.getDeviceListingStatus());
												raOracleOutput.setSheetOriginalApplicant(
														dNumberRelationShip.getOriginalApplicant());
												raOracleOutput.setSheetReceviedDate(dNumberRelationShip.getReceviedDate());
												raOracleOutput.setSheetVlearedDate(dNumberRelationShip.getClearedDate());
												raOracleOutput.setOracleProductCode(StringUtils.join(productCodes, ";"));
												if (sheetProductCodesSet != null)
													raOracleOutput.setSheetProductCode(
															StringUtils.join(sheetProductCodesSet, ";"));
												if (sheetProductCodes != null && sheetProductCodes.length() > 0) {
													if (isIdenticalHashSet(sheetProductCodesSet, productCodes))
														raOracleOutput.setProductCodeMatchStatus("Yes");
													else
														raOracleOutput.setProductCodeMatchStatus("No");
												} else {
													raOracleOutput.setProductCodeMatchStatus("Not Found");
												}

												break;
											}
											if (sheetProductCodes != null && sheetProductCodes.length() > 0) {
												if (isIdenticalHashSet(sheetProductCodesSet, productCodes))
													raOracleOutput.setProductCodeMatchStatus("Yes");
												else
													raOracleOutput.setProductCodeMatchStatus("No");

											} else {
												raOracleOutput.setProductCodeMatchStatus("Not Found");
											}
											raOracleOutput.setSheetKnumberFilledInternally(
													dNumberRelationShip.getkNumberFilledInternally());
											raOracleOutput.setSheetDeviceClass(dNumberRelationShip.getDeviceClass());
											raOracleOutput.setSheetdeviceListingStatus(
													dNumberRelationShip.getDeviceListingStatus());
											raOracleOutput.setSheetOriginalApplicant(
													dNumberRelationShip.getOriginalApplicant());
											raOracleOutput.setSheetReceviedDate(dNumberRelationShip.getReceviedDate());
											raOracleOutput.setSheetVlearedDate(dNumberRelationShip.getClearedDate());
											raOracleOutput.setOracleProductCode(StringUtils.join(productCodes, ";"));
											raOracleOutput.setSheetProductCode(StringUtils.join(sheetProductCodesSet, ";"));

											raOracleOutput.setSheetDnumber(sheetdNumber);
											raOracleOutput.setKnumberMatchStatus("No");
										}
									} else {
										raOracleOutput.setKnumberMatchStatus("Not Found");
									}
									kNumbers.remove(kNumber);
								} else {
									raOracleOutput.setKnumberMatchStatus("Not Found");
								}
							}
							raOracleOutputList.add(raOracleOutput);
							dIterator.remove();
						}
					} else if (kNumbers == null || kNumbers.size() == 0) {
						RAOracleOutput raOracleOutput = new RAOracleOutput();
						raOracleOutput.setId(refid);
						raOracleOutput.setdCount(dCount);
						raOracleOutput.setkCount(kCount);
						raOracleOutput.setKnumberMatchStatus("Not Found");
						raOracleOutput.setDnumberMatchStatus("Not Found");
						raOracleOutput.setProductCodeMatchStatus("Not Found");
						raOracleOutputList.add(raOracleOutput);
					}
					if (kNumbers != null) {
						Iterator<String> kIterator = kNumbers.iterator();
						while (kIterator.hasNext()) {
							String kNumber = kIterator.next();
							RAOracleOutput raOracleOutput = new RAOracleOutput();
							raOracleOutput.setId(refid);
							raOracleOutput.setdCount(dCount);
							raOracleOutput.setkCount(kCount);
							raOracleOutput.setOracleKnumber(kNumber);

							raOracleOutput.setOracleKnumber(kNumber);
							HashSet<Relationship> dNumbersSheet = kToD.get(kNumber);
							if (dNumbersSheet != null) {
								Iterator<Relationship> dNumbersSheetIterator = dNumbersSheet.iterator();
								while (dNumbersSheetIterator.hasNext()) {
									Relationship dNumberRelationShip = dNumbersSheetIterator.next();
									String sheetdNumber = dNumberRelationShip.getDeviceListing();
									String sheetProductCodes = dNumberRelationShip.getProductCodes();
									HashSet<String> sheetProductCodesSet = new HashSet<>(
											Arrays.asList(sheetProductCodes.split(";")));
									if (dNumbers != null && dNumbers.contains(sheetdNumber)) {
										raOracleOutput.setSheetDnumber(sheetdNumber);
										raOracleOutput.setDnumberMatchStatus("Yes");
										dNumbers.remove(sheetdNumber);
										raOracleOutput.setSheetDnumber(sheetdNumber);
										raOracleOutput.setKnumberMatchStatus("Yes");
										raOracleOutput.setSheetKnumberFilledInternally(
												dNumberRelationShip.getkNumberFilledInternally());
										raOracleOutput.setSheetDeviceClass(dNumberRelationShip.getDeviceClass());
										raOracleOutput.setSheetdeviceListingStatus(
												dNumberRelationShip.getDeviceListingStatus());
										raOracleOutput.setSheetOriginalApplicant(dNumberRelationShip.getOriginalApplicant());
										raOracleOutput.setSheetReceviedDate(dNumberRelationShip.getReceviedDate());
										raOracleOutput.setSheetVlearedDate(dNumberRelationShip.getClearedDate());

										raOracleOutput.setOracleProductCode(StringUtils.join(productCodes, ";"));
										raOracleOutput.setSheetProductCode(StringUtils.join(sheetProductCodesSet, ";"));
										if (sheetProductCodes != null && sheetProductCodes.length() > 0) {
											if (isIdenticalHashSet(sheetProductCodesSet, productCodes))
												raOracleOutput.setProductCodeMatchStatus("Yes");
											else
												raOracleOutput.setProductCodeMatchStatus("No");
										} else {
											raOracleOutput.setProductCodeMatchStatus("Not Found");
										}

										break;
									}
									if (sheetProductCodes != null && sheetProductCodes.length() > 0) {
										if (isIdenticalHashSet(sheetProductCodesSet, productCodes))
											raOracleOutput.setProductCodeMatchStatus("Yes");
										else
											raOracleOutput.setProductCodeMatchStatus("No");

									} else {
										raOracleOutput.setProductCodeMatchStatus("Not Found");
									}
									raOracleOutput.setSheetKnumberFilledInternally(
											dNumberRelationShip.getkNumberFilledInternally());
									raOracleOutput.setSheetDeviceClass(dNumberRelationShip.getDeviceClass());
									raOracleOutput.setSheetdeviceListingStatus(dNumberRelationShip.getDeviceListingStatus());
									raOracleOutput.setSheetOriginalApplicant(dNumberRelationShip.getOriginalApplicant());
									raOracleOutput.setSheetReceviedDate(dNumberRelationShip.getReceviedDate());
									raOracleOutput.setSheetVlearedDate(dNumberRelationShip.getClearedDate());

									raOracleOutput.setOracleProductCode(StringUtils.join(productCodes, ";"));
									raOracleOutput.setSheetProductCode(StringUtils.join(sheetProductCodesSet, ";"));

									raOracleOutput.setSheetDnumber(sheetdNumber);
									raOracleOutput.setKnumberMatchStatus("No");
									raOracleOutput.setDnumberMatchStatus("Not Found");
								}
							} else {
								raOracleOutput.setKnumberMatchStatus("Not Found");
								raOracleOutput.setDnumberMatchStatus("Not Found");
							}
							raOracleOutputList.add(raOracleOutput);
						}
					}
				}
			}
			pushRAOracleOutputToDB(raOracleOutputList);
		} catch (Exception ex) {
			ex.printStackTrace();
			PrismHandler.handleException(ex, true);
		} finally {
			PrismHandler.handleFinally(con, rs, stmt);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return null;
	}
	
	private String pushRAOracleOutputToDB(List<RAOracleOutput> raOracleOutputList) throws Exception {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		Connection con = null;
		PreparedStatement pstmt = null;
		int counter = 0;
		int raOracleOutputLength;
		Statement stmt;
		try {
			raOracleOutputLength = raOracleOutputList.size();
			con = dbUtil.getPostGresConnection(QfindRA);
			stmt = con.createStatement();
			stmt.executeUpdate("DELETE FROM ra_oracle_output");
			stmt.close();
			pstmt = con.prepareStatement(
					"INSERT INTO ra_oracle_output(refid, d_count, k_count, oracle_dnumber, oracle_knumber, oracle_procodes, sheet_d_number, sheet_k_number, sheet_procodes, k_match_status, d_match_status, procode_match_status, sheet_original_applicant, sheet_letter_filled_internally_k510, sheet_received_date, sheet_cleared_date, sheet_device_class, sheet_device_listing_status, oracle_gmdn_code, gmdn_code_match_status, sheet_gmdn_code)VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
			for (RAOracleOutput raOracleOutput : raOracleOutputList) {
				counter++;
				pstmt.setString(1, raOracleOutput.getId());
				pstmt.setInt(2, raOracleOutput.getdCount());
				pstmt.setInt(3, raOracleOutput.getkCount());
				pstmt.setString(4, raOracleOutput.getOracleDnumber());
				pstmt.setString(5, raOracleOutput.getOracleKnumber());
				pstmt.setString(6, raOracleOutput.getOracleProductCode());
				pstmt.setString(7, raOracleOutput.getSheetDnumber());
				pstmt.setString(8, raOracleOutput.getSheetKnumber());
				pstmt.setString(9, raOracleOutput.getSheetProductCode());
				pstmt.setString(10, raOracleOutput.getKnumberMatchStatus());
				pstmt.setString(11, raOracleOutput.getDnumberMatchStatus());
				pstmt.setString(12, raOracleOutput.getProductCodeMatchStatus());
				pstmt.setString(13, raOracleOutput.getSheetOriginalApplicant());
				pstmt.setString(14, raOracleOutput.getSheetKnumberFilledInternally());
				pstmt.setString(15, raOracleOutput.getSheetReceviedDate());
				pstmt.setString(16, raOracleOutput.getSheetVlearedDate());
				pstmt.setString(17, raOracleOutput.getSheetDeviceClass());
				pstmt.setString(18, raOracleOutput.getSheetdeviceListingStatus());
				pstmt.setString(19, raOracleOutput.getOracleGmdnCode());
				pstmt.setString(20, raOracleOutput.getGmdnCodeMatchStatus());
				pstmt.setString(21, raOracleOutput.getSheetGmdnCode());
				pstmt.addBatch();
				if (counter % 1000 == 0 || counter == raOracleOutputLength) {
					pstmt.executeBatch();
				}
			}
		} catch (Exception ex) {
			PrismHandler.handleException(ex, true);
		} finally {
			PrismHandler.handleFinally(con, null, pstmt);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return null;
	}
}
