package com.relevance.prism.service;

import java.sql.SQLException;
import java.util.ArrayList;

import com.relevance.prism.dao.WorkflowDao;
import com.relevance.prism.data.WfEntity;
import com.relevance.prism.data.WfTransition;
import com.relevance.prism.util.PrismHandler;

/**
 *
 * @author: Shawkath Khan & Manikantan
 * @Created_Date: Feb 17, 2016
 * @Purpose: Workflow Engine Methods
 * @Modified_By: 
 * @Modified_Date:  
 */

public class WorkFlowService extends BaseService {

	
	 /**
	 * @purpose  Get the entity object
	 * @return  current entity object
	 */
	public WfEntity getEntity(String entityId) throws Exception
	{
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		WfEntity wfEntity =  null;
		try {
			WorkflowDao dao = new WorkflowDao();
			wfEntity = dao.getEntity(entityId);
			
			
		}catch(Exception e){
			PrismHandler.handleException(e, true);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return wfEntity;
	}
	
	public ArrayList<WfEntity>  getEntityList() throws Exception
	{
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		ArrayList<WfEntity> entityList = null;
		try {
			WorkflowDao dao = new WorkflowDao();
			entityList = dao.getEntityList();			
		}catch(Exception e){
			PrismHandler.handleException(e, true);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return entityList;
	}
	/**
	 * @purpose  Add to workflow
	 * @return  void
	 */
	public String addToWorkflow(String userId, String entityId, String startState, String comments) throws SQLException, Exception
	{
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		String responseMessage = null;
		try {
			//call dao to insert record into wf_entity table
			WorkflowDao workFlowDao = new WorkflowDao();
			WfEntity wfEntity = new WfEntity();
			wfEntity.setEntityId(entityId);
			wfEntity.setUser(userId);
			wfEntity.setState(startState);
			wfEntity.setComments(comments);
			responseMessage = workFlowDao.addToWorkflow(wfEntity);			
		} catch(Exception e){
			PrismHandler.handleException(e, true);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return responseMessage;
	}

	
	/**
	 * @purpose   Gets the list of allowed transitions for the user based on role
	 * @return array of allowed workflow transitions
	 */
	public ArrayList<WfTransition> getNextTransitions(WfEntity entity, String userRole) throws Exception
	{
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		ArrayList<WfTransition> wfTransitionList = null;
		try 
		{
			WorkflowDao dao = new WorkflowDao();
			wfTransitionList = dao.getNextTransitions(entity, userRole);
			
		} catch(Exception e){
			PrismHandler.handleException(e, true);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return wfTransitionList;
	}
	
	
	/**
	 * @Purpose   Sets the next allowed workflow transactions for the user
	 * @return void
	 */
	public String applyNextTransition(WfEntity entity,  String action, String userId, String userRole, String comments) throws Exception
	{
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		String responseMessage = null;
		try {
			
			WorkflowDao dao = new WorkflowDao();
			ArrayList<WfTransition> transitions = dao.getNextTransitions(entity, userRole);
			
			WfTransition tr = null;
			for (WfTransition trans : transitions)
			{
				if (trans.getAction() != null && trans.getAction().equals(action))
				{
					tr = trans;
					break;
				}
			}
			
			if (tr != null)
			{
				responseMessage = dao.applyNextTransition(entity, tr, userId, comments);
			}else{
				responseMessage = "{\"message\":\"Entity Not inserted the table\"}";
				return responseMessage;
			}

		}catch(Exception e){
			PrismHandler.handleException(e, true);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return responseMessage;
	}
	
	public ArrayList<WfTransition> getAllTransitions() throws Exception
	{
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		ArrayList<WfTransition> wfTransitionList = null;
		try 
		{
			WorkflowDao dao = new WorkflowDao();
			PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
			wfTransitionList = dao.getTransitionList();			
		}catch(Exception e){
			PrismHandler.handleException(e, true);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return wfTransitionList;
	}
	public boolean updateworkflowstatus(String entityid, String status,String updatedby,String comments) throws Exception{
		 
		try {
			WorkflowDao dao = new WorkflowDao();
			dao.updateWorkFlowStatus(entityid,status,updatedby,comments);
		} catch (Exception e) {
			PrismHandler.handleException(e, true);
			return false;
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return true;
	}
	public ArrayList<WfEntity>  getWorkFlowAuditList(String entityid) throws Exception
	{
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		ArrayList<WfEntity> entityList = null;
		try {
			WorkflowDao dao = new WorkflowDao();
			entityList = dao.getWorkFlowAuditList(entityid);			
		}catch(Exception e){
			PrismHandler.handleException(e, true);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return entityList;
	}
}
