package com.relevance.prism.data;

import java.util.ArrayList;
import java.util.List;

public class NGramList {
	private Integer profileId;
	private String userName;
	private Integer ngramID;
	private String ngramKey;
	private String ngramValue;
	private String ngramType;
	private List<NGram> nGramAnalyser = new ArrayList<NGram>();
	
	

	public String getuserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getNgramKey() {
		return ngramKey;
	}

	public void setNgramKey(String ngramKey) {
		this.ngramKey = ngramKey;
	}

	public String getNgramValue() {
		return ngramValue;
	}

	public void setNgramValue(String ngramValue) {
		this.ngramValue = ngramValue;
	}
	
	public List<NGram> getNGramAnalyser() {
		return nGramAnalyser;
	}
	
	public void setNGramAnalyser(List<NGram> nGramAnalyser) {
		this.nGramAnalyser = nGramAnalyser;
	}

	public String getNgramType() {
		return ngramType;
	}

	public void setNgramType(String ngramType) {
		this.ngramType = ngramType;
	}

	public Integer getProfileId() {
		return profileId;
	}

	public void setProfileId(Integer profileId) {
		this.profileId = profileId;
	}

	public Integer getNgramID() {
		return ngramID;
	}

	public void setNgramID(Integer ngramID) {
		this.ngramID = ngramID;
	}
}
