package com.relevance.prism.dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.NumberUtils;

import com.relevance.prism.data.NGram;
import com.relevance.prism.data.NGramList;
import com.relevance.prism.util.E2emfAppUtil;
import com.relevance.prism.util.E2emfConstants;
import com.relevance.prism.util.PrismHandler;

public class DataCorrectionsDao extends BaseDao{

	Connection con = null;
	Statement stmt = null;

	String emea_db = null;

	public DataCorrectionsDao() {
		emea_db = E2emfAppUtil.getAppProperty(E2emfConstants.e2emfDB);
	}

	public NGramList getUnCorrectedData() throws Exception {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		ResultSet resultSet = null;
		NGramList gramView = new NGramList();
		try 
		{
			con = dbUtil.getHiveConnection(E2emfConstants.e2emfDB);

			stmt = con.createStatement();
		
			List<NGram> unCorrectWords = new ArrayList<>();
			String QUERY = "select key,value from uncorrect_words order by value desc limit 1000";
	
			resultSet = stmt.executeQuery(QUERY);
	
			while (resultSet.next()) {
				NGram nggram = new NGram();
				String value = resultSet.getString(1);
				if (value.length() > 2 && !NumberUtils.isNumber(value)
						&& !"material:".equalsIgnoreCase(value)
						&& !"products.".equalsIgnoreCase(value)
						&& !"sheet:hh".equalsIgnoreCase(value)
						&& !"sheet=".equalsIgnoreCase(value)
						&& !"date:".equalsIgnoreCase(value)
						&& !"by:".equalsIgnoreCase(value)
						&& !"no.".equalsIgnoreCase(value)
						&& !"scale:".equalsIgnoreCase(value)
						&& !"notes:".equalsIgnoreCase(value)
						&& !"company.".equalsIgnoreCase(value)
						&& !"products,".equalsIgnoreCase(value)
						&& !"shown.".equalsIgnoreCase(value)
						&& !"number:".equalsIgnoreCase(value)
						&& !"number.".equalsIgnoreCase(value)
						&& !"approved:".equalsIgnoreCase(value)
						&& !"engineer:".equalsIgnoreCase(value)
						&& !"high.".equalsIgnoreCase(value)
						&& !"other:".equalsIgnoreCase(value)
						&& !"reserved.".equalsIgnoreCase(value)
						&& !"approval.".equalsIgnoreCase(value)) {
					nggram.setText(resultSet.getString(1));
					nggram.setSize(resultSet.getInt(2));
					unCorrectWords.add(nggram);
				}
	
			}
			
			if (!unCorrectWords.isEmpty() && unCorrectWords != null) {
				gramView.setNGramAnalyser(unCorrectWords);
			}
		}
		catch(Exception ex)
		{
			PrismHandler.handleException(ex, true);
		}
		finally {
			PrismHandler.handleFinally(con, null, stmt);
		}	

		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return gramView;

	}

	public NGramList getCorrectedData() throws Exception {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		NGramList gramView = new NGramList();
		ResultSet resultSet = null;
		try {
			con = dbUtil.getHiveConnection(E2emfConstants.e2emfDB);

			stmt = con.createStatement();
		
			List<NGram> correctWords = new ArrayList<>();
			String QUERY = "select key,value from correct_words order by value desc limit 1000";
	
			resultSet = stmt.executeQuery(QUERY);
	
			while (resultSet.next()) {
				NGram nggram = new NGram();
				nggram.setText(resultSet.getString(1));
				nggram.setSize(resultSet.getInt(2));
				correctWords.add(nggram);
	
			}
			
			if (!correctWords.isEmpty() && correctWords != null) {
				gramView.setNGramAnalyser(correctWords);
			}
		}
		catch(Exception ex){
			PrismHandler.handleException(ex, true);
		}
		finally {
			PrismHandler.handleFinally(con, resultSet, stmt);
		}	
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return gramView;

	}
}
