package com.relevance.prism.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.StringTokenizer;
 
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import com.relevance.prism.util.Log;
 
public class SessionFilter implements Filter {
 
    private ArrayList<String> urlList;
    private int customSessionExpiredErrorCode = 901;
    
    @Override
    public void destroy() throws UnsupportedOperationException{
    	
    }
    
    @Override
    public void doFilter(ServletRequest req, ServletResponse res,
            FilterChain chain) throws IOException, ServletException {
    	
    	System.out.println("within doFilter of Session Filter");
		System.out.println("*********************For testing***************************");
    	Log.Info("within doFilter of Session Filter");
 
        HttpServletRequest request = (HttpServletRequest) req;
        HttpServletResponse response = (HttpServletResponse) res;
        String url = request.getRequestURI();
               
        boolean allowedRequest = false;
        if(urlList.contains(url) || url.matches(".*(css|jpg|png|gif|js|html|jsp|dsplogin|indexpageforcustomusers)")) {
        	System.out.println("url :::::" +url);
        	Log.Info("url :::::" +url);
            allowedRequest = true;            
        }             
        if (!allowedRequest) {
        	
            HttpSession session = request.getSession(false);
            //System.out.println("session is:" +session);
            //Log.Info("session is:" +session);
            if (null == session) {
            	response.sendError(this.customSessionExpiredErrorCode);
            }
            else{
            	chain.doFilter(req, res); 
            }
        }else {
        	//System.out.println("Chain.doFilter()..");
        	//Log.Info("Chain.doFilter()..");
            chain.doFilter(request, response);
        }
  }
    @Override
    public void init(FilterConfig config) throws ServletException {
        String urls = config.getInitParameter("avoid-urls");
        StringTokenizer token = new StringTokenizer(urls, ",");
 
        urlList = new ArrayList<>();
 
        while (token.hasMoreTokens()) {
            urlList.add(token.nextToken());
 
        }
    }
}