package com.relevance.prism.util;

import java.util.Iterator;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.relevance.prism.rest.BaseResource;

public class DataObfuscator extends BaseResource {

	private static final int OBFUSCATE = 1;
	private static final int DEOBFUSCATE = 2;

	public static final int OBF_TYPE_CHAR = 1;
	public static final int OBF_TYPE_WORD = 2;

	private DataObfuscator() {
	}

	public static String deObfuscateToken(String str) throws Exception {
		//PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		String response = null;
		try {
			// System.out.println("String :" +str);

			if (str == null)
				return null;

			if ("true".equalsIgnoreCase(E2emfAppUtil
					.getAppProperty(E2emfConstants.dataObfuscation))) {
				StringBuilder sb = new StringBuilder();
				for (int i = 0; i < str.length(); i++) {
					sb.append(getDeObfuscateChar(str.charAt(i)));
				}
				response = sb.toString();
			} else {
				response = str;
			}
		} catch (Exception ex) {
			response = PrismHandler.handleException(ex, true);
		}
		//PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return response;
	}

	private static Character getDeObfuscateChar(Character c) throws Exception {
		//PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		Character response = null;
		try {
			if (E2emfAppUtil.getRevCharMap().get(c) == null)
				response = c;
			else
				response = E2emfAppUtil.getRevCharMap().get(c)
						.getCharacterFrom();
		} catch (Exception ex) {
			PrismHandler.handleException(ex, true);
		}
		//PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return response;

	}

	private static String obfuscateToken(String str, int type) throws Exception {
		//PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		String response = null;
		try {
			if (str == null)
				return null;

			if (type == OBF_TYPE_CHAR) {
				StringBuilder sb = new StringBuilder();
				for (int i = 0; i < str.length(); i++) {
					sb.append(getObfuscateChar(str.charAt(i)));
				}

				response = sb.toString();
			} else if (type == OBF_TYPE_WORD) {
				String[] words = str.split(" ");
				StringBuilder sb = new StringBuilder();
				for (int i = 0; i < words.length; i++) {
					if (E2emfAppUtil.getWordMap().get(words[i]) != null)
						sb.append(E2emfAppUtil.getWordMap().get(words[i])
								.getWordTo());
					else
						sb.append(words[i]);

					sb.append(" ");
				}

				response = sb.toString();
			}

		} catch (Exception ex) {
			response = PrismHandler.handleException(ex, true);
		}
		//PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return response;
	}

	private static Character getObfuscateChar(Character c) throws Exception {
		//PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		Character response = null;
		try {

			if (E2emfAppUtil.getCharMap().get(c) == null)
				response = c;
			else
				response = E2emfAppUtil.getCharMap().get(c).getCharacterTo();
		} catch (Exception ex) {
			PrismHandler.handleException(ex, true);
		}
		//PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return response;
	}

	private static void processArray(JSONArray jsonArray, int type,
			String baseLookupKey) throws JSONException, Exception {
		//PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		try {

			for (int i = 0; i < jsonArray.length(); i++) {
				Object val = jsonArray.get(i);

				if (val.getClass().equals(JSONObject.class)) {
					processObject((JSONObject) val, type, baseLookupKey);
				} else if (val.getClass().equals(JSONArray.class)) {
					processArray((JSONArray) val, type, baseLookupKey);
				}

			}
		} catch (Exception ex) {
			PrismHandler.handleException(ex, true);
		}
		//PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
	}

	private static void processObject(JSONObject json, int type,
			String baseLookupKey) throws JSONException, Exception {
		//PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		try {

			Iterator<String> keys = json.keys();
			String prefix = "";
			if (baseLookupKey != null)
				prefix = baseLookupKey + ".";

			while (keys.hasNext()) {
				String key = keys.next();
				String lookUpKey = prefix + key;

				if (!json.isNull(key)) {
					Object val = json.get(key);

					if (val != null && val.getClass().equals(JSONObject.class)) {
						processObject((JSONObject) val, type, lookUpKey);
					} else if (val != null
							&& val.getClass().equals(JSONArray.class)) {
						processArray((JSONArray) val, type, lookUpKey);
					} else if (val != null
							&& val.getClass().equals(String.class)) {
						FormData formData = E2emfAppUtil.getFormDataMap().get(
								lookUpKey);
						if (formData != null) {
							if (type == OBFUSCATE && formData.isObfuscate())
								json.put(
										key,
										obfuscateToken((String) val,
												formData.getObfType()));
							else if (type == DEOBFUSCATE
									&& formData.isDeObfuscate())
								json.put(key, deObfuscateToken((String) val));
							else
								Log.Debug("Obfuscate type incorrect");
						}

					} else {
						Log.Debug("Unprocessed Obfuscation object type "
								+ val.getClass() + " from attribute "
								+ lookUpKey);
					}
				}
			}
		} catch (Exception ex) {
			PrismHandler.handleException(ex, true);
		}
		//PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
	}

	public static String obfuscate(String json) throws JSONException, Exception {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		String response = null;
		try {
			if ("true".equalsIgnoreCase(E2emfAppUtil
					.getAppProperty(E2emfConstants.dataObfuscation))) {
				if (json.charAt(0) == '[') {
					JSONArray jsonArray = new JSONArray(json);
					processArray(jsonArray, OBFUSCATE, null);
					response = jsonArray.toString();
				} else {
					JSONObject jsonObject = new JSONObject(json);
					processObject(jsonObject, OBFUSCATE, null);
					response = jsonObject.toString();
				}
			}else{
				response = json;
			}
		} catch (Exception ex) {
			response = PrismHandler.handleException(ex, true);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return response;
	}

	public static JSONObject obfuscate(JSONObject json, String baseKey)
			throws JSONException, Exception {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		try {			
			if ("true".equalsIgnoreCase(E2emfAppUtil
					.getAppProperty(E2emfConstants.dataObfuscation))) {
				processObject(json, OBFUSCATE, baseKey);
			}
		} catch (Exception ex) {
			PrismHandler.handleException(ex, true);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return json;
	}

	public static JSONArray obfuscate(JSONArray json, String baseKey)
			throws JSONException, Exception {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		try {			
			if ("true".equalsIgnoreCase(E2emfAppUtil
					.getAppProperty(E2emfConstants.dataObfuscation))) {
				processArray(json, OBFUSCATE, baseKey);
			}
		} catch (Exception ex) {
			PrismHandler.handleException(ex, true);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return json;
	}

	public static String deObfuscate(String json) throws JSONException,
			Exception {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		String response = null;
		try {			
			JSONObject jsonObject = new JSONObject(json);
			processObject(jsonObject, DEOBFUSCATE, null);
			response = jsonObject.toString();
		} catch (Exception ex) {
			response = PrismHandler.handleException(ex, true);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return response;
	}
}
