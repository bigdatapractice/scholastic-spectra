package com.relevance.prism.rest;

import java.util.List;
import javax.servlet.ServletContext;
import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import com.google.gson.Gson;
import com.relevance.prism.util.PrismHandler;
import com.relevance.prism.data.BOM;
import com.relevance.prism.service.MerckService;
import com.relevance.prism.service.NGramService;

@Path("/merck")
public class MerckResource extends BaseResource {

	@Context
	ServletContext context;

	public MerckResource(@Context ServletContext value) {
		this.context = value;
		//System.out.println("Conext:" + this.context);
	}
	
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Path("/getCrystalBOM")
	public String getCrystalBOM(@FormParam("key") String key, @FormParam("database") String database) {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		BOM bom = null;
		String response = null;
		try {
			MerckService merckService = new MerckService();
			bom = merckService.getCrystalBOM(key, database);
			Gson gson = new Gson();
			response = gson.toJson(bom);
		} catch (Exception ex) {
			response = PrismHandler.handleException(ex);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return response;
	}
	
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Path("/getMongoDBCollectionList")
	
public String getMongoDBCollectionList(@FormParam("database") String database) {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		List<String> collectionList = null;
		String response = null;
		try {
			MerckService merckService = new MerckService();
			collectionList = merckService.getMongoDBCollectionList(database);
			Gson gson = new Gson();
			response = gson.toJson(collectionList);
		} catch (Exception ex) {
			response = PrismHandler.handleException(ex);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return response;
	}
	
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Path("/getMongoDBConnection")
	public String getMongoDBConnection(	@FormParam("database") String database,
			@FormParam("collection") String collection) {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		String response ;
		final String[] responsearray ;
		try {
			MerckService merckService = new MerckService();
			responsearray = merckService.getMongoDBConnection(database, collection);
			Gson gson = new Gson();
			response = gson.toJson(responsearray);
		} catch (Exception ex) {
			response = PrismHandler.handleException(ex);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return response;
	}
	
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Path("/executeMongoQuery")
	public String executeMongoQuery(@FormParam("database") String database, 
			@FormParam("collection") String collection,@FormParam("query") String query,@FormParam("query") String options) {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		StringBuilder response = null;
		List<String> documentList = null;
		try {
			MerckService merckService = new MerckService();
			documentList = merckService.executeMongoQuery(database, collection, query, options);
			response=new StringBuilder("[");
			for(String json:documentList){
				response.append(json);
				if(documentList.indexOf(json)!=documentList.size()-1)
					response.append(",");
				
			}		
			response.append("]");
		} catch (Exception ex) {
			response = new StringBuilder(PrismHandler.handleException(ex));
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return response.toString();
	}
	
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Path("/updateDuplicate")
	public String updateNgramFilter(
			@FormParam("seqNo") Integer seqNo,
			@FormParam("status") String status,
			@FormParam("groupId") String groupId) {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		String response = null;
		try {
			MerckService merckService = new MerckService();
			response = merckService.updateDuplicate(seqNo, status, groupId);
		} catch (Exception ex) {
			response = PrismHandler.handleException(ex);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return response;
	}
}
