package com.relevance.prism.dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.relevance.prism.util.PrismHandler;
import com.relevance.search.data.SmartFindDocument;

public class GalaxieDao extends BaseDao {
	public List<SmartFindDocument> getDetailsByPartNumber(String partNumber) throws Exception {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		List<SmartFindDocument> smartFindDocumentList = new ArrayList<>();
		StringBuilder query = new StringBuilder();
		ResultSet rs = null;
		Connection con = null;
		Statement stmt = null;
		try {
			con = dbUtil.getPostGresConnection();
			query.append("SELECT title, docnum, manufacturer, cspeciality, categoryl0, partnumber, finished_goods_number, component_number, mlocation,components,imagepath,pngimage FROM solrmaster");
				query.append(" WHERE partnumber='").append(partNumber).append("'");
			if (con != null) {
				stmt = con.createStatement();
				rs = stmt.executeQuery(query.toString());
				while (rs.next()) {
					SmartFindDocument smartFindDocument = new SmartFindDocument();
					smartFindDocument.setTitle(rs.getString("title"));
					smartFindDocument.setDocnum(rs.getString("docnum"));
					smartFindDocument.setManufacturer(rs.getString("manufacturer"));
					smartFindDocument.setCspeciality(rs.getString("cspeciality"));
					smartFindDocument.setCategoryl0(rs.getString("categoryl0"));
					smartFindDocument.setPartnumber(rs.getString("partnumber"));
					smartFindDocument.setFinishedGoodsNumber(rs.getString("finished_goods_number"));
					smartFindDocument.setComponentNumber(rs.getString("component_number"));
					smartFindDocument.setmLocation(rs.getString("mlocation"));
					smartFindDocument.setComponents(rs.getString("components"));
					smartFindDocument.setImagepath(rs.getString("imagepath"));
					smartFindDocument.setImageset(rs.getString("pngimage"));
					smartFindDocumentList.add(smartFindDocument);
				}
			}
		} catch (Exception ex) {
			PrismHandler.handleException(ex, true);
		} finally {
			PrismHandler.handleFinally(con, rs, stmt);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return smartFindDocumentList;
	}
}
