package com.relevance.prism.rest;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.google.gson.Gson;
import com.relevance.prism.service.GalaxieService;
import com.relevance.prism.util.PrismHandler;
import com.relevance.search.data.SmartFindDocument;


@Path("/Galaxie")
public class GalaxieResource extends BaseResource{
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Path("/getDetailsByPartNumber")
	public String getDetailsByPartNumber(@FormParam("partNumber") String partNumber)
			{
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		List<SmartFindDocument> smartFindDocumentList = null;
		String response = null;
		try {
			GalaxieService galaxieService = new GalaxieService();
			smartFindDocumentList = galaxieService.getDetailsByPartNumber(partNumber);
			Gson gson = new Gson();
			response = gson.toJson(smartFindDocumentList);
		}catch(Exception ex){
			response = PrismHandler.handleException(ex);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return response;
	}
}
	