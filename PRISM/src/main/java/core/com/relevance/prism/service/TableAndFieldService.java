package com.relevance.prism.service;

import java.util.ArrayList;
import java.util.List;

import com.relevance.prism.dao.TableAndFieldDao;
import com.relevance.prism.data.E2emfField;
import com.relevance.prism.data.E2emfTable;
import com.relevance.prism.util.Log;
import com.relevance.prism.util.PrismHandler;

public class TableAndFieldService extends BaseService {

	public ArrayList<E2emfTable> getTablesMap() throws Exception{
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		TableAndFieldDao tableAndFieldDao = new TableAndFieldDao();
		ArrayList<E2emfTable> e2emfTableMap = tableAndFieldDao.getTablesMap();
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return e2emfTableMap;
	}
	
	public String insertField(Object... obj) throws Exception{
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		String responseMessage = null;
		try{
			Log.Info("insertField method called on TableAndFieldService");
			TableAndFieldDao tableAndFieldDao = new TableAndFieldDao();
			responseMessage = tableAndFieldDao.insertField(obj);
			return responseMessage;		
		}catch(Exception e){
			PrismHandler.handleException(e, true);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return responseMessage;	
	}
	
	public String updateField(Object... obj) throws Exception{
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		String responseMessage = null;
		try{
			Log.Info("updateField method called on TableAndFieldService");
			TableAndFieldDao tableAndFieldDao = new TableAndFieldDao();
			responseMessage = tableAndFieldDao.updateField(obj);
			return responseMessage;
		}catch(Exception e){
			PrismHandler.handleException(e, true);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return responseMessage;
	}
	
	public String deleteField(Object... obj) throws Exception{
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		String successMessage = null;
		try{
			Log.Info("deleteField method called on TableAndFieldService");
			TableAndFieldDao tableAndFieldDao = new TableAndFieldDao();
			successMessage = tableAndFieldDao.deleteField(obj);
			return successMessage;
		}catch(Exception e){
			PrismHandler.handleException(e, true);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return successMessage;
	}
	
	public List<E2emfField> getFieldReport(Object... obj) throws Exception{
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		TableAndFieldDao tableAndFieldao = new TableAndFieldDao();
		List<E2emfField> e2emfFieldList = tableAndFieldao.getFieldReport(obj);
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return e2emfFieldList;
	}
}
