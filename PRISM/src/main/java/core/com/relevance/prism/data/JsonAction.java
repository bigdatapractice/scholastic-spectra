package com.relevance.prism.data;

public class JsonAction {
	
	private String id;
	private String order;
	private String profileKey;
	private String profileDisplayName;
	
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getOrder() {
		return order;
	}
	public void setOrder(String order) {
		this.order = order;
	}
	public String getProfileKey() {
		return profileKey;
	}
	public void setProfileKey(String profileKey) {
		this.profileKey = profileKey;
	}
	public String getProfileDisplayName() {
		return profileDisplayName;
	}
	public void setProfileDisplayName(String profileDisplayName) {
		this.profileDisplayName = profileDisplayName;
	}
	
}
