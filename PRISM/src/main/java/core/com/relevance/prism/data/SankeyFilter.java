package com.relevance.prism.data;

public class SankeyFilter {
	
	public static final String FIELD_TYPE_DATE = "DATE";
	public static final String FIELD_TYPE_NUM = "NUM";
	public static final String FIELD_TYPE_STR = "STR";
	
	
	private String key;
	private String searchField;
	private String displayName;
	private String fieldType;
	private String lookUpQuery;
	private String value1;
	private String value2;
	
	
	public String getValue1() {
		return value1;
	}
	public void setValue1(String value1) {
		this.value1 = value1;
	}
	public String getValue2() {
		return value2;
	}
	public void setValue2(String value2) {
		this.value2 = value2;
	}
	public String getKey() {
		return key;
	}
	public void setKey(String key) {
		this.key = key;
	}
	public String getSearchField() {
		return searchField;
	}
	public void setSearchField(String searchField) {
		this.searchField = searchField;
	}
	public String getDisplayName() {
		return displayName;
	}
	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}
	public String getFieldType() {
		return fieldType;
	}
	public void setFieldType(String fieldType) {
		this.fieldType = fieldType;
	}
	public String getLookUpQuery() {
		return lookUpQuery;
	}
	public void setLookUpQuery(String lookUpQuery) {
		this.lookUpQuery = lookUpQuery;
	}
}
