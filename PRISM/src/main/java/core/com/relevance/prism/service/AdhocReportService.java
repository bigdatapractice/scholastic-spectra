package com.relevance.prism.service;

import java.util.ArrayList;
import java.util.List;

import com.relevance.prism.dao.AdhocReportDao;
import com.relevance.prism.data.AdhocReport;
import com.relevance.prism.util.Log;
import com.relevance.prism.util.PrismHandler;

public class AdhocReportService extends BaseService {
	public List<AdhocReport> getAdhocReports() throws Exception{
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		AdhocReportDao adhocReportDao = new AdhocReportDao();
		List<AdhocReport> adhocReportList = new ArrayList<>();
		try{
			Log.Info("getAdhocReports method called on AdhocReportService");
			adhocReportList = adhocReportDao.getAdhocReports();
			Log.Info("Fetched AdhocReport object from DAO " + adhocReportList);
			return adhocReportList;	
		}catch(Exception e){
			PrismHandler.handleException(e, true);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return adhocReportList;	
	}
	
	public AdhocReport getAdhocReport(long id) throws Exception
	{
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		AdhocReportDao adhocReportDao = new AdhocReportDao();
		AdhocReport adhocReport = null;
		try
		{
			adhocReport = adhocReportDao.getAdhocReport(id);
		}catch(Exception e){
			PrismHandler.handleException(e, true);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return adhocReport;	
	}
	
	public String createAdhocReport(AdhocReport adhocReport) throws Exception{
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		String responseMessage = null;
		String logMessage = "Fetched AdhocReport object from DAO";
		try{
			Log.Info("createAdhocReport method called on AdhocReportService");
			AdhocReportDao adhocReportDao = new AdhocReportDao();
			responseMessage = adhocReportDao.createAdhocReport(adhocReport);
			Log.Info(logMessage + adhocReport);
			return responseMessage;		
		}catch(Exception e){
			PrismHandler.handleException(e, true);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return responseMessage;	

	}
	
	public String updateAdhocReport(AdhocReport adhocReport) throws Exception{
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		String responseMessage = null;
		String logMessage = "Fetched AdhocReport object from DAO";
		try{
			Log.Info("createAdhocReport method called on AdhocReportService");
			AdhocReportDao adhocReportDao = new AdhocReportDao();
			responseMessage = adhocReportDao.updateAdhocReport(adhocReport);
			Log.Info(logMessage + adhocReport);
			return responseMessage;		
		}catch(Exception e){
			PrismHandler.handleException(e, true);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return responseMessage;	

	}
}
