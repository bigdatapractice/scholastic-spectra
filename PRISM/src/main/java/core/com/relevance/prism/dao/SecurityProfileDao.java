package com.relevance.prism.dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import com.relevance.prism.data.SecurityProfile;
import com.relevance.prism.util.E2emfAppUtil;
import com.relevance.prism.util.E2emfConstants;
import com.relevance.prism.util.Log;
import com.relevance.prism.util.PrismHandler;

public class SecurityProfileDao extends BaseDao{

	static String securityDefaultQuery = "SELECT source, source_role, source_tcodes_count, source_tcodes, target, target_role, target_tcodes_count, target_tcodes, match_count, matched_tcodes, rankpercentage, rank, type, no_match_count_source_to_target, no_match_tcodes_source_to_target, no_match_count_target_to_source, no_match_tcodes_target_to_source, percentage_match_source_to_target, percentage_match_target_to_source, roletype FROM ";
	
	Connection con = null;
	Statement stmt = null;
	
	String security_db = null;
	
	public SecurityProfileDao() {
		security_db = E2emfAppUtil.getAppProperty(E2emfConstants.securityDB);
	} 
	
	public ArrayList<SecurityProfile> getSecurityViewDetails(Object... obj) throws SQLException, NullPointerException, Exception {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		ArrayList<SecurityProfile> securityViewList = new ArrayList<SecurityProfile>();
		String source = null;
		String target = null;
		String sourceRole = null;
		StringBuilder queryToExecute = null;
		ResultSet rs = null;
		String whereClause = null;
		try {
			if (obj != null && obj.length >= 1) {
				source = (String) obj[0];
				target = (String) obj[1];
				sourceRole = (String) obj[2];
				Log.Info("Fetching the View for Security view.");
				String tableName = "SECURITYANALYSIS";
				con = dbUtil.getHiveConnection(E2emfConstants.securityDB);
				Log.Info("Using SECURITY HIVE Connection...");
				
				if(source == null && target == null && sourceRole == null)
					queryToExecute = new StringBuilder(securityDefaultQuery).append(tableName);
				else{

					if(source != null && target != null && sourceRole != null)
						whereClause = " where source in ('"+source+"') and target in ('"+target+"') and source_role in ('" + sourceRole + "')"; 
					if(source == null && target != null && sourceRole != null)		
						whereClause = " where target in ('"+target+"') and source_role in ('" + sourceRole + "')"; 
					if(target == null && source !=null && sourceRole !=null )	
						whereClause = " where source in ('"+source+"') and source_role in ('" + sourceRole + "')"; 
					if(sourceRole == null && source !=null && target !=null )	
						whereClause = " where source in ('"+source+"') and target in ('" + target + "')";
					if(source == null && target == null && sourceRole != null)		
						whereClause = " where source_role in ('" + sourceRole + "')"; 
					if(target == null && source !=null && sourceRole ==null )	
						whereClause = " where source in ('"+source+"')";
					if(sourceRole == null && source ==null && target !=null )	
						whereClause = " where target in ('" + target + ")'";
					
					queryToExecute = new StringBuilder(securityDefaultQuery).append(tableName).append(whereClause);
					
				}
				//queryToExecute = new StringBuilder(slbDefaultQuery).append(slob_db).append(".").append(tableName);
				
				stmt = con.createStatement();

				rs = stmt.executeQuery(queryToExecute.toString());
				if (rs != null) {					
					while (rs.next()) {
						SecurityProfile securityView = new SecurityProfile();
						securityView.setSource(rs.getString("source"));
						securityView.setSourceRole(rs.getString("source_role"));
						securityView.setSourceTcodesCount(rs.getString("source_tcodes_count"));
						securityView.setSourceTcodes(rs.getString("source_tcodes"));
						securityView.setTarget(rs.getString("target"));
						securityView.setTargetRole(rs.getString("target_role"));
						securityView.setTargetTcodesCount(rs.getString("target_tcodes_count"));
						securityView.setTargetTcodes(rs.getString("target_tcodes"));
						securityView.setMatchCount(rs.getString("match_count"));
						securityView.setMatchedTcodes(rs.getString("matched_tcodes"));
						securityView.setRankPercentage(rs.getString("rankpercentage"));
						securityView.setRank(rs.getString("rank"));
						securityView.setType(rs.getString("type"));
						securityView.setNoMatchCountSourceToTarget(rs.getString("no_match_count_source_to_target"));
						securityView.setNoMatchTcodesSourceToTarget(rs.getString("no_match_tcodes_source_to_target"));
						securityView.setNoMatchCountTargetToSource(rs.getString("no_match_count_target_to_source"));
						securityView.setNoMatchTcodesTargetToSource(rs.getString("no_match_tcodes_target_to_source"));
						securityView.setPercentageMatchSourceToTarget(rs.getString("percentage_match_source_to_target"));
						securityView.setPercentageMatchTargetToSource(rs.getString("percentage_match_target_to_source"));
						securityView.setRoleType(rs.getString("roletype"));
						
						securityViewList.add(securityView);						
					}
				}		
			}
		}catch(Exception ex){
			PrismHandler.handleException(ex, true);
		}
		finally {
			PrismHandler.handleFinally(con, rs, stmt);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return securityViewList;

	}
}
