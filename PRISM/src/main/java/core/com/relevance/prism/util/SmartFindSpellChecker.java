package com.relevance.prism.util;

import java.io.File;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.relevance.search.data.NGramEntry;
import com.swabunga.spell.engine.SpellDictionary;
import com.swabunga.spell.engine.SpellDictionaryHashMap;
import com.swabunga.spell.engine.Word;
import com.swabunga.spell.event.SpellCheckEvent;
import com.swabunga.spell.event.SpellCheckListener;
import com.swabunga.spell.event.SpellChecker;
import com.swabunga.spell.event.StringWordTokenizer;
import com.swabunga.spell.event.TeXWordFinder;

public class SmartFindSpellChecker implements SpellCheckListener {

	private static SpellChecker spellChecker;
	private static List<String> misspelledWords;
		
	private static String dictFile = null;
	private static String phonetFile = null;
	
	private static final int ONE_TO_THOUSAND = 1;
	private static final int THOUSANDANDONE_TO_TWOTHOUSAND = 2;
	private static final int TWOTHOUSANDANDONE_TO_THREETHOUSAND = 3;
	private static final int THREETHOUSANDANDONE_TO_FOURTHOUSAND = 4;
	private static final int FOURTHOUSANDANDONE_TO_FIVETHOUSAND = 5;
	private static final int FIVETHOUSANDANDONE_AND_ABOVE = 6;
	private static final int DEFAULT = 0;
	
	private static final String ERROR_MESSAGE = "Exception Occurred in Smart Find nGram Analyser : ";
	/**
	 * 
	 */
	public SmartFindSpellChecker() {
		misspelledWords = new ArrayList<>();
		dictFile = "jazzy-master/dict/english.0";
		phonetFile = "jazzy-master/dict/phonet.en";
		initialize();
	}
	
	/**
	 * get a list of misspelled words from the text
	 * 
	 * @param text
	 */
	public static List<String> getMisspelledWords(String text) {
		StringWordTokenizer texTok = new StringWordTokenizer(text, new TeXWordFinder());
		spellChecker.checkSpelling(texTok);
		return misspelledWords;
	}
	
	/**
	 * Initialize the Spell Checker
	 */
	private void initialize() {
		try{
			SpellDictionary dictionary = new SpellDictionaryHashMap(new File(this.getClass().getClassLoader().getResource(dictFile).getFile()), new File(this.getClass().getClassLoader().getResource(phonetFile).getFile()));
			spellChecker = new SpellChecker(dictionary);
			spellChecker.addSpellCheckListener(this);
		}catch(Exception ex){
			Log.Error("Exception Occured in Initializing the Spell Checker : " + ex);
		}
	}

	/**
	 * correct the misspelled words in the input string and return the result
	 * @param line
	 * @return
	 */
	public static String getCorrectedLine(String line) {
		List<String> misSpelledWords = getMisspelledWords(line);

		for (String misSpelledWord : misSpelledWords) {
			List<String> suggestions = getSuggestions(misSpelledWord);
			if (suggestions.size() == 0)
				continue;
			String bestSuggestion = suggestions.get(0);
			line = line.replace(misSpelledWord, bestSuggestion);
		}
		return line;
	}
	
	/**
	 * Get the Corrected Text
	 * @param line
	 * @return
	 */
	public String getCorrectedText(String line) {
		StringBuilder builder = new StringBuilder();
		String[] tempWords = line.split(" ");
		for (String tempWord : tempWords) {
			if (!spellChecker.isCorrect(tempWord)) {
				List<Word> suggestions = spellChecker.getSuggestions(tempWord, 0);
				if (suggestions.size() > 0) {
					builder.append(spellChecker.getSuggestions(tempWord, 0)
							.get(0).toString());
				} else
					builder.append(tempWord);
			} else {
				builder.append(tempWord);
			}
			builder.append(" ");
		}
		return builder.toString().trim();
	}
	
	/**
	 * Get suggestios for the misspelled words
	 * @param misspelledWord
	 * @return
	 */
	public static List<String> getSuggestions(String misspelledWord) {
		@SuppressWarnings("unchecked")
		List<Word> su99esti0ns = spellChecker.getSuggestions(misspelledWord, 0);
		List<String> suggestions = new ArrayList<>();
		for (Word suggestion : su99esti0ns) {
			suggestions.add(suggestion.getWord());
		}
		return suggestions;
	}
	
	/**
	 * Findout the Spelling Error in the string Passed
	 */
	@Override
	public void spellingError(SpellCheckEvent event) {
		List suggestions = event.getSuggestions();
		if (suggestions.size() > 0) {
			Log.Info("MISSPELT WORD: " + event.getInvalidWord());
			for (Iterator suggestedWord = suggestions.iterator(); suggestedWord.hasNext();) {
				Log.Info("\tSuggested Word: " + suggestedWord.next());
			}
		} else {
			Log.Info("MISSPELT WORD: " + event.getInvalidWord());
			Log.Info("\tNo suggestions");
		}
		// Null actions
	}
	
	/**
	 * Check the Spelling Mistake in the nGramList passed
	 * @param nGramList
	 * @param isDictionaryWords
	 * @param range
	 * @return
	 */
	public static List<NGramEntry> checkSpelling(List<NGramEntry> nGramList, boolean isDictionaryWords, int range) {
		SmartFindSpellChecker smartFindSpellChecker = new SmartFindSpellChecker();
		List<NGramEntry> resultNGramListForDictionaryWords = new ArrayList<>();
		List<NGramEntry> resultNGramListForNonDictionaryWords = new ArrayList<>();
		if(range == ONE_TO_THOUSAND){
			for(NGramEntry nGramEntry : nGramList){
				if(nGramEntry.getSize() >= 1 && nGramEntry.getSize() < 1000){
				try {			
						int correction = spellChecker.checkSpelling(new StringWordTokenizer(nGramEntry.getText() + " "));
						if(!isDictionaryWords && (correction > -1)){
							resultNGramListForNonDictionaryWords.add(nGramEntry);
						}else if(isDictionaryWords && (correction == -1)){
							resultNGramListForDictionaryWords.add(nGramEntry);
						}
					} catch (Exception e) {
						Log.Error(ERROR_MESSAGE + e);
					}
				}
			}			
		}
		if(range == THOUSANDANDONE_TO_TWOTHOUSAND){
			for(NGramEntry nGramEntry : nGramList){
				if(nGramEntry.getSize() >= 1001 && nGramEntry.getSize() <= 2000){
					try {			
						int correction = spellChecker.checkSpelling(new StringWordTokenizer(nGramEntry.getText() + " "));
						if(!isDictionaryWords && (correction > -1)){
							resultNGramListForNonDictionaryWords.add(nGramEntry);
						}else if(isDictionaryWords && (correction == -1)){
							resultNGramListForDictionaryWords.add(nGramEntry);
						}
					} catch (Exception e) {
						Log.Error(ERROR_MESSAGE + e);
					}
				}
			}
		}
		if(range == TWOTHOUSANDANDONE_TO_THREETHOUSAND){
			for(NGramEntry nGramEntry : nGramList){
				if(nGramEntry.getSize() >= 2001 && nGramEntry.getSize() <= 3000){
					try {			
						int correction = spellChecker.checkSpelling(new StringWordTokenizer(nGramEntry.getText() + " "));
						if(!isDictionaryWords && (correction > -1)){
							resultNGramListForNonDictionaryWords.add(nGramEntry);
						}else if(isDictionaryWords && (correction == -1)){
							resultNGramListForDictionaryWords.add(nGramEntry);
						}
					} catch (Exception e) {
						Log.Error(ERROR_MESSAGE + e);
					}
				}
			}
		}
		if(range == THREETHOUSANDANDONE_TO_FOURTHOUSAND){
			for(NGramEntry nGramEntry : nGramList){
				if(nGramEntry.getSize() >= 3001 && nGramEntry.getSize() <= 4000){
					try {			
						int correction = spellChecker.checkSpelling(new StringWordTokenizer(nGramEntry.getText() + " "));
						if(!isDictionaryWords && (correction > -1)){
							resultNGramListForNonDictionaryWords.add(nGramEntry);
						}else if(isDictionaryWords && (correction == -1)){
							resultNGramListForDictionaryWords.add(nGramEntry);
						}
					} catch (Exception e) {
						Log.Error(ERROR_MESSAGE + e);
					}
				}
			}
		}
		if(range == FOURTHOUSANDANDONE_TO_FIVETHOUSAND){
			for(NGramEntry nGramEntry : nGramList){
				if(nGramEntry.getSize() >= 4001 && nGramEntry.getSize() <= 5000){
					try {			
						int correction = spellChecker.checkSpelling(new StringWordTokenizer(nGramEntry.getText() + " "));
						if(!isDictionaryWords && (correction > -1)){
							resultNGramListForNonDictionaryWords.add(nGramEntry);
						}else if(isDictionaryWords && (correction == -1)){
							resultNGramListForDictionaryWords.add(nGramEntry);
						}
					} catch (Exception e) {
						Log.Error(ERROR_MESSAGE + e);
					}
				}
			}
		}
		if(range == FIVETHOUSANDANDONE_AND_ABOVE){
			for(NGramEntry nGramEntry : nGramList){
				if(nGramEntry.getSize() >= 5001){
					try {			
						int correction = spellChecker.checkSpelling(new StringWordTokenizer(nGramEntry.getText() + " "));
						if(!isDictionaryWords && (correction > -1)){
							resultNGramListForNonDictionaryWords.add(nGramEntry);
						}else if(isDictionaryWords && (correction == -1)){
							resultNGramListForDictionaryWords.add(nGramEntry);
						}
					} catch (Exception e) {
						Log.Error(ERROR_MESSAGE + e);
					}
				}
			}
		}
		if(range == DEFAULT){
			for(NGramEntry nGramEntry : nGramList){					
				try {			
					int correction = spellChecker.checkSpelling(new StringWordTokenizer(nGramEntry.getText() + " "));
					if(!isDictionaryWords && (correction > -1)){
						resultNGramListForNonDictionaryWords.add(nGramEntry);
					}else if(isDictionaryWords && (correction == -1)){
						resultNGramListForDictionaryWords.add(nGramEntry);
					}
				} catch (Exception e) {
					Log.Error(ERROR_MESSAGE + e);
				}
			}	
		}		
		if(isDictionaryWords){
			return resultNGramListForDictionaryWords;
		}else{
			return resultNGramListForNonDictionaryWords;
		}
	}	
}
