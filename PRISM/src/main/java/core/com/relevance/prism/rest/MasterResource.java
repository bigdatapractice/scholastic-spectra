package com.relevance.prism.rest;


import javax.servlet.ServletContext;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import com.relevance.prism.service.Service;
import com.relevance.prism.service.ServiceLocator;
import com.relevance.prism.util.DataObfuscator;
import com.relevance.prism.util.Log;
import com.relevance.prism.util.PrismHandler;


/**
 * 
 * E2emf Master Resource 
 *
 */
@Path("/MasterData")
@Consumes(MediaType.APPLICATION_JSON)
public class MasterResource extends BaseResource {
	
	@Context
	ServletContext context;
	
	public MasterResource(@Context ServletContext value) {
		this.context = value;
		//System.out.println("Conext:" + this.context);
	}
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/searchMasterDatanew")
	public String getMasterData(@QueryParam("type") String varX,
			@QueryParam("q") String searchParam, @QueryParam("source") String source)  throws Exception{
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());

		
		Log.Info("Recieved searchMasterDatanew Request with param " + varX + " searchParam " + searchParam);
		 
		String response = null;
		
		try{
			
			Service service = ServiceLocator.getServiceInstance("master");
			searchParam = DataObfuscator.deObfuscateToken(searchParam);
			response = (String) service.getJsonObject(varX, searchParam,source);
						
			response = DataObfuscator.obfuscate(response);
		}catch(Exception ex){
			response = PrismHandler.handleException(ex, false);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return response;
	}
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/searchMasterDataFilter")
	public String getMasterDataFilter(@QueryParam("type") String varX,
			@QueryParam("q") String searchParam, @QueryParam("source") String source)  throws Exception{
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		Log.Info("Recieved searchMasterDataFilter Request with param " + varX + " searchParam " + searchParam);
		 
		String response = null;
		
		try{
			
			Service service = ServiceLocator.getServiceInstance("master");
			//searchParam = DataObfuscator.deObfuscateToken(searchParam);
			response = (String) service.getActions(varX, searchParam,source);
		}catch(Exception ex){
			response = PrismHandler.handleException(ex, false);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return response;
	}
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/searchRoleDatanew")
	public String getRoleData(@QueryParam("type") String varX,
			@QueryParam("q") String searchParam, @QueryParam("source") String source)  throws Exception{
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		
		Log.Info("Recieved searchRoleDatanew Request with param " + varX + " searchParam " + searchParam);
		 
		String response = null;
		
		try{
			
			Service service = ServiceLocator.getServiceInstance("master");
			//searchParam = DataObfuscator.deObfuscateToken(searchParam);
			response = (String) service.getRoles(varX, searchParam,source);
		}catch(Exception ex){
			response = PrismHandler.handleException(ex, false);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return response;
	}
	
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/searchPrimaryRoles")
	public String getPrimaryRoles(@QueryParam("type") String varX,
			@QueryParam("q") String searchParam, @QueryParam("source") String source)  throws Exception{
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		Log.Info("Recieved SearchPrimaryRoles Request with param " + varX + " searchParam " + searchParam);
		 
		String response = null;
		
		try{
			
			Service service = ServiceLocator.getServiceInstance("master");
			//searchParam = DataObfuscator.deObfuscateToken(searchParam);
			response = (String) service.getPrimaryRoles(varX, searchParam,source);		 	
		}catch(Exception ex){
			response = PrismHandler.handleException(ex, false);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return response;
	}
	
}
