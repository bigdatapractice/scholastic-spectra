package com.relevance.prism.rest;

import java.util.ArrayList;

import javax.servlet.ServletContext;
import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.relevance.prism.data.SecurityProfile;
import com.relevance.prism.service.SecurityProfileService;
import com.relevance.prism.util.Log;
import com.relevance.prism.util.PrismHandler;

@Path("/security")
public class SecurityProfileResource extends BaseResource {

	@Context
	ServletContext context;

	public SecurityProfileResource(@Context ServletContext value) {
		this.context = value;
		// System.out.println("Conext:" + this.context);
	}

	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Path("/securityview/")
	public String getSecurityhViewDetails(
			@FormParam("sourcelist") String sourceList,
			@FormParam("targetlist") String targetList,
			@FormParam("sourcerolelist") String sourceRoleList) {
		String response = null;
		try {

			Log.Info("Recieved request security profile view to fetch Security Details");

			Gson gson = new GsonBuilder().disableHtmlEscaping().create();

			String key = "securityview";
			if (this.context.getAttribute(key) != null) {
				Log.Info("Reading object Security View for from context");
				response = this.context.getAttribute(key).toString();

			} else {

				SecurityProfileService securityProfileService = new SecurityProfileService();
				ArrayList<SecurityProfile> securityViewList = securityProfileService
						.getSecurityProfileViewDetails(sourceList, targetList,
								sourceRoleList);

				response = gson.toJson(securityViewList);
				if (response != null) {
					Log.Info("Security View JSON created having size : "
							+ response.length());
					this.context.setAttribute(key, response);
				}
			}

		} catch (Exception ex) {
			response = PrismHandler.handleException(ex);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return response;
	}
}
