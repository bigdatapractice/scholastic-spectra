package com.relevance.prism.job;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONObject;
import org.quartz.JobDetail;
import org.quartz.JobExecutionContext;
import org.springframework.scheduling.quartz.QuartzJobBean;

import com.relevance.prism.service.DataAnalyserService;
import com.relevance.prism.util.E2emfAppUtil;
import com.relevance.prism.util.E2emfConstants;
import com.relevance.prism.util.Log;
import com.relevance.prism.util.PrismDbUtil;
import com.relevance.prism.util.PrismHandler;

public class Prisma_TestSuite_Job extends QuartzJobBean  {
	private PrismDbUtil dbUtil = new PrismDbUtil();
	private static final String HIVE = "HIVE";
	protected void executeInternal(JobExecutionContext context) {
		try {
			ResultSet rs = null;
			Connection con = null;
			Statement stmt = null;
			String displayname=null;
			ArrayList<String> profileIds = new ArrayList<String>();
			ArrayList<String> criticality = new ArrayList<String>();
			String str1;
			System.out.println("--------------------------------------------------------------------");
			System.out.println("Starting Job " + context.getJobDetail().getFullName() + " at " + context.getFireTime());
			JobDetail jobDetail = context.getJobDetail();
			System.out.println(context.getJobDetail().getFullName() + " at " + context.getJobRunTime() + ", key: "
					+ jobDetail.getKey());
			System.out.println(
					context.getJobDetail().getFullName() + "'s next scheduled time: " + context.getNextFireTime());
			
			con = getDatabaseConnection(E2emfAppUtil.getAppProperty(E2emfConstants.suiteSource),E2emfAppUtil.getAppProperty(E2emfConstants.suiteDB));
			stmt = con.createStatement();
			String sql = (E2emfAppUtil.getAppProperty(E2emfConstants.suiteQuery));
			rs = stmt.executeQuery(sql);
			String users = null;
			if(rs.next()) { 
				displayname = rs.getString("displayname"); 
				str1 = rs.getString("config");
				JSONObject jObject  = new JSONObject(str1); 
				String data = jObject.getString("query"); // get data object
				users = jObject.getString("users");
				JSONArray arrJSON = new JSONArray(data);
				System.out.println("arrJSON.length()"+arrJSON.length());
				int x=0;
				for (int j=0; j<arrJSON.length(); j++) {
					JSONObject item = arrJSON.getJSONObject(j);
					profileIds.add(item.getString("id"));
					if(item.has("criticality")){
					criticality.add(item.getString("criticality"));
					}else{
						criticality.add("low");
					}
					x++;
				}
			}
			DataAnalyserService dataAnalyserService = new DataAnalyserService();
            String  profileId = String.join(",", profileIds);
            String  order = String.join(",", criticality);
            dataAnalyserService.queryExecutor(profileId, users, displayname ,order,true);
		} catch (Exception e) {
			Log.Error("Exception Occured....." + e);
		}
	}
	
	private String getDatabaseName(String database)  throws Exception  {
		//PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		StringBuilder query = new StringBuilder();
		ResultSet rs = null;
		Connection con = null;
		Statement stmt = null;
		String paramCon = null;
		try {
			query.append("select name from app_properties where name ='")
					.append(database)
					.append("' or (value like '%jdbc%' and value like '%")
					.append(database).append("%')");
			con = dbUtil.getPostGresConnection();
			stmt = con.createStatement(java.sql.ResultSet.TYPE_FORWARD_ONLY,
					java.sql.ResultSet.CONCUR_READ_ONLY);
			rs = stmt.executeQuery(query.toString());
			while (rs.next()) {
				paramCon = rs.getString("name");
			}
		} catch (Exception ex) {
			PrismHandler.handleException(ex, true);
		} finally {
			PrismHandler.handleFinally(con, rs, stmt);
		}
		//PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return paramCon;
	}

	public Connection getDatabaseConnection(String type, String database)
			throws Exception {
		//PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		Connection con = null;
		String paramCon;
		try {
			paramCon = getDatabaseName(database);
			if ((HIVE).equalsIgnoreCase(type)) {
				con = dbUtil.getHiveConnection(paramCon);
			} else {
				con = dbUtil.getPostGresConnection(paramCon);
			}
		} catch (Exception ex) {
			PrismHandler.handleException(ex, true);
		}
		//PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return con;
	}

}
