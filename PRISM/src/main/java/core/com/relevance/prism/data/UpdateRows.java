package com.relevance.prism.data;

import java.util.List;

public class UpdateRows {

	List<UpdateColumns> rowsList;
	String userName;
	String userId;
	
	public List<UpdateColumns> getRowsList() {
		return rowsList;
	}

	public void setRowsList(List<UpdateColumns> rowsList) {
		this.rowsList = rowsList;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}
	
	
}
