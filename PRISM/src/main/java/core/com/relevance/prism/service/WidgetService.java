package com.relevance.prism.service;

import com.relevance.prism.dao.WidgetDao;
import com.relevance.prism.data.Widget;
import com.relevance.prism.util.PrismHandler;

public class WidgetService extends BaseService {

	public Widget getWidgetByName(String widgetName) throws Exception {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		Widget widgetResponse = null;
		try {
			WidgetDao dao = new WidgetDao();
			widgetResponse = dao.getWidgetByName(widgetName);
		}catch(Exception e){
			PrismHandler.handleException(e, true);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return widgetResponse;
	}
}
