package com.relevance.prism.data;

import java.util.List;


/**
 * @author Saurabh
 * Column Detail Properties Pojo Object  
 */
public class ColumnDetail {
	private String key;
	private int count;
	
	public int getCount() {
		return count;
	}
	public void setCount(int count) {
		this.count = count;
	}
	
	public String getKey() {
		return key;
	}
	public void setKey(String key) {
		this.key = key;
	}
}
