package com.relevance.prism.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.relevance.prism.data.ProfileAction;
import com.relevance.prism.util.PrismHandler;

public class ProfileActionsDao  extends BaseDao {
	public List<ProfileAction> getProfileActions(int profileID, int id,String profileIds) throws Exception {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		List<ProfileAction> profileActionList = new ArrayList<>();
		StringBuilder query = new StringBuilder();
		ResultSet rs = null;
		Connection con = null;
		Statement stmt = null;
		try {
			con = dbUtil.getPostGresConnection();
			query.append("SELECT id,profile_id,name,linked_profile_key,linked_profile_name,field_mapping,type,action_type,enabled FROM profile_actions");
			if (profileIds != null && !"".equalsIgnoreCase(profileIds)) {
				query.append(" WHERE profile_id in (" + profileIds + ")");
			}
			else if (profileID > 0) {
				query.append(" WHERE profile_id="+profileID);
			} else if (id > 0) {
				query.append(" WHERE id="+id);
			} 
			if (con != null) {
				stmt = con.createStatement(
						java.sql.ResultSet.TYPE_FORWARD_ONLY,
						java.sql.ResultSet.CONCUR_READ_ONLY);
				rs = stmt.executeQuery(query.toString());
				while (rs.next()) {
					ProfileAction profileAction = new ProfileAction();
					profileAction.setId(rs.getInt("id"));
					profileAction.setProfileID(rs.getInt("profile_id"));
					profileAction.setName(rs.getString("name"));
					profileAction.setFieldMapping(rs.getString("field_mapping"));
					profileAction.setType(rs.getString("type"));
					profileAction.setLinkedProfileName(rs.getString("linked_profile_name"));
					profileAction.setLinkedProfileKey(rs.getString("linked_profile_key"));
					profileAction.setActionType(rs.getString("action_type"));
					profileAction.setEnabled(rs.getBoolean("enabled"));
					profileActionList.add(profileAction);
				}
			}
		} catch (Exception ex) {
			PrismHandler.handleException(ex, true);
		} finally {
			PrismHandler.handleFinally(con, rs, stmt);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return profileActionList;
	}
	
	public List<ProfileAction> saveProfileAction(ProfileAction profileAction) throws Exception {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		PreparedStatement ps = null;
		Connection con = null;
		int id;
		int profileId;
		String name;
		ResultSet rs = null;
		Statement stmt = null;
		List<ProfileAction> profileActionList = null;
		StringBuilder query = new StringBuilder();
		try {
			con = dbUtil.getPostGresConnection();
			stmt = con.createStatement();
			profileId = profileAction.getProfileID();
			name = profileAction.getName();
			id = profileAction.getId();
			if (id == 0) {
				query = new StringBuilder(
						"INSERT INTO public.profile_actions( profile_id, name, linked_profile_key,linked_profile_name, field_mapping, type,action_type,enabled) VALUES (?, ?, ?, ?, ?, ?,?,?)");
			} else {
				query = new StringBuilder(
						"UPDATE public.profile_actions SET profile_id=?, name=?, linked_profile_key=?, linked_profile_name=?, field_mapping=?, type=?, action_type=?, enabled=? where id = ?");

			}
			ps = con.prepareStatement(query.toString());
			ps.setInt(1, profileId);
			ps.setString(2, name);
			ps.setString(3, profileAction.getLinkedProfileKey());
			ps.setString(4, profileAction.getLinkedProfileName());
			ps.setString(5, profileAction.getFieldMapping());
			ps.setString(6, profileAction.getType());
			ps.setString(7, profileAction.getActionType());
			ps.setBoolean(8, profileAction.isEnabled());
			if (id != 0)
				ps.setInt(9, id);
			ps.addBatch();
			ps.executeBatch();

			profileActionList = getProfileActions(profileId,0,null);
		} catch (Exception ex) {
			PrismHandler.handleException(ex, true);
		} finally {
			if (ps != null) {
				ps.close();
			}
			PrismHandler.handleFinally(con, rs, stmt);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return profileActionList;
	}
}
