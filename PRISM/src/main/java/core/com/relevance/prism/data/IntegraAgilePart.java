package com.relevance.prism.data;

import java.util.List;

public class IntegraAgilePart {

	
	private String parent;
	private String materialName;
	private String parentName;
	private List<IntegraAgilePart> forwardChildren;
	private List<IntegraAgilePart> backwardParent;
	private List<IntegraAgilePart> children;
	
	private int rowId;
	private int level;
	private String description;
	private String partNumber;
	private String key;	
	private String partType;
	private String rev;
	private String revReleaseDate;
	private String functionalTerm;
	private String bomItmtype;
	private String bomIncorpDate;
	private String plant;
	
	
	
	public String getMaterialName() {
		return materialName;
	}
	public void setMaterialName(String materialName) {
		this.materialName = materialName;
	}
	public String getParentName() {
		return parentName;
	}
	public void setParentName(String parentName) {
		this.parentName = parentName;
	}

	public String getParent() {
		return parent;
	}
	public void setParent(String parent) {
		this.parent = parent;
	}

	public List<IntegraAgilePart> getChildren() {
		return children;
	}
	public void setChildren(List<IntegraAgilePart> children) {
		this.children = children;
	}
	public List<IntegraAgilePart> getForwardChildren() {
		return forwardChildren;
	}
	public void setForwardChildren(List<IntegraAgilePart> forwardChildren) {
		this.forwardChildren = forwardChildren;
	}
	public List<IntegraAgilePart> getBackwardParent() {
		return backwardParent;
	}
	public void setBackwardParent(List<IntegraAgilePart> backwardParent) {
		this.backwardParent = backwardParent;
	}

	public int getRowId() {
		return rowId;
	}
	public void setRowId(int rowId) {
		this.rowId = rowId;
	}
	public int getLevel() {
		return level;
	}
	public void setLevel(int level) {
		this.level = level;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getPartNumber() {
		return partNumber;
	}
	public void setPartNumber(String partNumber) {
		this.partNumber = partNumber;
	}
	public String getKey() {
		return key;
	}
	public void setKey(String key) {
		this.key = key;
	}
	public String getPartType() {
		return partType;
	}
	public void setPartType(String partType) {
		this.partType = partType;
	}
	public String getRev() {
		return rev;
	}
	public void setRev(String rev) {
		this.rev = rev;
	}
	public String getRevReleaseDate() {
		return revReleaseDate;
	}
	public void setRevReleaseDate(String revReleaseDate) {
		this.revReleaseDate = revReleaseDate;
	}
	public String getFunctionalTerm() {
		return functionalTerm;
	}
	public void setFunctionalTerm(String functionalTerm) {
		this.functionalTerm = functionalTerm;
	}
	public String getBomItmtype() {
		return bomItmtype;
	}
	public void setBomItmtype(String bomItmtype) {
		this.bomItmtype = bomItmtype;
	}
	public String getBomIncorpDate() {
		return bomIncorpDate;
	}
	public void setBomIncorpDate(String bomIncorpDate) {
		this.bomIncorpDate = bomIncorpDate;
	}
	public String getPlant() {
		return plant;
	}
	public void setPlant(String plant) {
		this.plant = plant;
	}

}
