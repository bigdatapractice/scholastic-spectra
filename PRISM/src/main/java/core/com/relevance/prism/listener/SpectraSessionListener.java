package com.relevance.prism.listener;

import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;
import com.relevance.prism.util.Log;

/**
 * @author Hiresh Roy
 * @CreatedOn Mar 28, 2017
 * @Purpose to implement the HttpSession counter via HttpSessionListener callback methods
 * @ModifiedBy
 * @ModifiedDate
 */

 /*
 a listener to be configured in web.xml like this
 <listener-class>com.relevance.prism.util.SpectraSessionListener</listener-class>
 */

public class SpectraSessionListener implements HttpSessionListener {

  private static int totalActiveSessions=0;

  public static int getTotalActiveSession(){
	 return totalActiveSessions;
  }

  @Override
  public void sessionCreated(HttpSessionEvent event) {
	 totalActiveSessions++;
	 Log.Info("{S:"+event.getSession().getId()+"}{N:"+event.getSession().getAttribute("user")+"}"+"sessionCreated - add one session into counter, revised session counter value : " + totalActiveSessions);
	 //Log.Info("sessionCreated - add one session into counter, revised session counter value : " + totalActiveSessions);
	 
  }

  @Override
  public void sessionDestroyed(HttpSessionEvent event) {
	 totalActiveSessions--;
	 Log.Info("{S:"+event.getSession().getId()+"}{N:"+event.getSession().getAttribute("user")+"}"+"sessionDestroyed - deduct one session from counter, revised session counter value : " + totalActiveSessions);
	 //Log.Info("sessionDestroyed - deduct one session from counter, revised session counter value : " + totalActiveSessions);
  }

}//end-of-class