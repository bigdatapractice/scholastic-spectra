package com.relevance.prism.data;

public class Theme {
	private String themeName;
	private long isDefault;
	private long enabled;
	private long menuOnTop;
	private String primaryRole;

	public String getThemeName() {
		return themeName;
	}

	public void setThemeName(String themeName) {
		this.themeName = themeName;
	}

	public long getIsDefault() {
		return isDefault;
	}

	public void setIsDefault(long isDefault) {
		this.isDefault = isDefault;
	}

	public long getEnabled() {
		return enabled;
	}

	public void setEnabled(long enabled) {
		this.enabled = enabled;
	}

	public long getMenuOnTop() {
		return menuOnTop;
	}

	public void setMenuOnTop(long menuOnTop) {
		this.menuOnTop = menuOnTop;
	}

	public String getPrimaryRole() {
		return primaryRole;
	}

	public void setPrimaryRole(String primaryRole) {
		this.primaryRole = primaryRole;
	}
}
