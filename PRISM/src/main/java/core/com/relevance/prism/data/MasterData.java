package com.relevance.prism.data;

import java.util.ArrayList;
import java.util.List;

public class MasterData {
	
	List<Attribute> masterdata=new ArrayList<Attribute>();
	private String sku_number;
	private String userID;
	private String userName;
	public String getUserID() {
		return userID;
	}
	public void setUserID(String userID) {
		this.userID = userID;
	}
	
	
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public List<Attribute> getMasterdata() {
		return masterdata;
	}
	public void setMasterdata(List<Attribute> masterdata) {
		this.masterdata = masterdata;
	}
	public String getSku_number() {
		return sku_number;
	}
	public void setSku_number(String sku_number) {
		this.sku_number = sku_number;
	}
}
