package com.relevance.prism.rest;

import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.lang.reflect.Type;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;
import javax.ws.rs.core.StreamingOutput;
import org.apache.log4j.Logger;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.xssf.streaming.SXSSFCell;
import org.apache.poi.xssf.streaming.SXSSFRow;
import org.apache.poi.xssf.streaming.SXSSFSheet;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFColor;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.json.JSONArray;
import org.json.JSONObject;

import com.google.common.io.CharStreams;
import com.google.common.reflect.TypeToken;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.relevance.prism.dao.UserDao;
import com.relevance.prism.data.Action;
import com.relevance.prism.data.AdHocReportQuery;
import com.relevance.prism.data.ChartConfiguration;
import com.relevance.prism.data.Column;
import com.relevance.prism.data.ColumnInformation;
import com.relevance.prism.data.DataAnalyserView;
import com.relevance.prism.data.DataTable;
import com.relevance.prism.data.DataTableView;
import com.relevance.prism.data.FieldList;
import com.relevance.prism.data.ProfileConfig;
import com.relevance.prism.data.ProfileDetails;
import com.relevance.prism.data.ProfileView;
import com.relevance.prism.data.Report;
import com.relevance.prism.data.UpdateColumns;
import com.relevance.prism.data.UpdateRows;
import com.relevance.prism.service.DataAnalyserService;
import com.relevance.prism.util.DataObfuscator;
import com.relevance.prism.util.E2emfAppUtil;
import com.relevance.prism.util.E2emfConstants;
import com.relevance.prism.util.EmailClient;
import com.relevance.prism.util.PrismHandler;
import com.sun.jersey.core.header.FormDataContentDisposition;
import com.sun.jersey.multipart.FormDataParam;

@Path("/dataAnalyser")
public class DataAnalyserResource extends BaseResource {
	public Logger _logger = Logger.getLogger(this.getClassName());
	@Context
	ServletContext context;

	public DataAnalyserResource(@Context ServletContext value) {
		this.context = value;
		// System.out.println("Conext:" + this.context);
	}

	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Path("/getDatabaseList")
	public String getDatabaseList(@FormParam("source") String source) {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		List<String> databaseList = null;
		String response = null;
		try {
			DataAnalyserService dataAnalyserService = new DataAnalyserService();
			databaseList = dataAnalyserService.getDatabaseList(source);
			Gson gson = new Gson();
			response = gson.toJson(databaseList);
		} catch (Exception ex) {
			response = PrismHandler.handleException(ex);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return response;
	}

	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Path("/getChartConfigurationList")
	public String getChartConfigurationList() {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		ChartConfiguration chartMap = null;
		String response = null;
		try {
			DataAnalyserService dataAnalyserService = new DataAnalyserService();
			chartMap = dataAnalyserService.getChartConfigurationList();
			Gson gson = new Gson();
			response = gson.toJson(chartMap);
		} catch (Exception ex) {
			response = PrismHandler.handleException(ex);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return response;
	}

	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Path("/getcolumnInfo")
	public String getcolumnInfo(@FormParam("source") String source, @FormParam("database") String database,
			@FormParam("table") String table, @FormParam("column") String column, @FormParam("filters") String filters,
			@FormParam("isDetails") boolean isDetails) {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		ColumnInformation columnInfo;
		String response = null;
		try {
			DataAnalyserService dataAnalyserService = new DataAnalyserService();
			columnInfo = dataAnalyserService.getcolumnInfo(source, database, table, column, filters, isDetails);
			Gson gson = new Gson();
			response = gson.toJson(columnInfo);
		} catch (Exception ex) {
			response = PrismHandler.handleException(ex);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return response;
	}

	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Path("/getTableList")
	public String getTableList(@FormParam("source") String source, @FormParam("database") String database) {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		List<String> tableList = null;
		String response = null;
		try {
			DataAnalyserService dataAnalyserService = new DataAnalyserService();
			tableList = dataAnalyserService.getTableList(source, database);
			Gson gson = new Gson();
			response = gson.toJson(tableList);
		} catch (Exception ex) {
			response = PrismHandler.handleException(ex);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return response;
	}

	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Path("/getColumnList")
	public String getColumnList(@FormParam("source") String source, @FormParam("database") String database,
			@FormParam("table") String table, @FormParam("query") String query) {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		List<Column> columnList = null;
		String response = null;
		try {
			DataAnalyserService dataAnalyserService = new DataAnalyserService();
			columnList = dataAnalyserService.getColumnList(source, database, table, query);
			Gson gson = new Gson();
			response = gson.toJson(columnList);
		} catch (Exception ex) {
			
			response = PrismHandler.handleException(ex);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return response;
	}

	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Path("/getMultiColumnList")
	public String getMultiColumnList(@FormParam("source") String source, @FormParam("database") String database,
			@FormParam("tables") String tables, @FormParam("query") String query) {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		Map<String, List<Column>> columnList = null;
		String response = null;
		try {
			DataAnalyserService dataAnalyserService = new DataAnalyserService();
			columnList = dataAnalyserService.getMultiColumnList(source, database, tables, query);
			Gson gson = new Gson();
			response = gson.toJson(columnList);
		} catch (Exception ex) {
			response = PrismHandler.handleException(ex);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return response;
	}

	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Path("/getUpdatedColumns")
	public String getUpdatedColumns(@FormParam("sourceName") String sourceName, @FormParam("dbName") String dbName,
			@FormParam("tableName") String tableName, @FormParam("primaryKeyColumn") String primaryKeyColumn,
			@FormParam("primaryKeys") String primaryKeys, @FormParam("filter") String filter,
			@FormParam("orderBy") String orderBy, @FormParam("columns") String columns,
			@FormParam("groupBy") String groupBy) {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		Map<String, List<String>> dataList = null;
		String response = null;
		try {
			DataAnalyserService dataAnalyserService = new DataAnalyserService();
			dataList = dataAnalyserService.getUpdatedRows(sourceName, dbName, tableName, primaryKeyColumn, primaryKeys,
					filter, orderBy, columns, groupBy);
			Gson gson = new Gson();
			response = gson.toJson(dataList);
		} catch (Exception ex) {
			response = PrismHandler.handleException(ex);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return response;
	}

	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Path("/getData")
	public String getData(@FormParam("source") String source, @FormParam("database") String database,
			@FormParam("table") String table, @FormParam("query") String query, @FormParam("columns") String columns,
			@FormParam("filters") String filters, @FormParam("profilekey") String profilekey,
			@FormParam("masterdata") String masterdata, @FormParam("groupByEnabled") String groupByEnabled, @FormParam("cacheDisabled") String cacheDisabled) {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		DataAnalyserView view = null;
		String response = null;
		Boolean disabledCache;
		ProfileConfig config;
		String configString = "";
		try {
			DataAnalyserService dataAnalyserService = new DataAnalyserService();
			Gson gson = new Gson();
			config = new ProfileConfig();
			config.setSource(source);
			config.setTable(table);
			config.setDatabase(database);
			config.setColumns(columns);
			config.setQuery(query);
			config.setFilter(filters);
			config.setGroupByEnabled(Boolean.parseBoolean(groupByEnabled));
			if (masterdata != null && !"".equalsIgnoreCase(masterdata.trim())) {
				FieldList fieldAndDescriptionList = gson.fromJson(masterdata, FieldList.class);
				config.setDescription(fieldAndDescriptionList);
			}
			configString = gson.toJson(config);
			disabledCache = Boolean.parseBoolean(cacheDisabled);
			int requestHashCode = configString.hashCode();
			Object requestObject = null;
			if(disabledCache == false || disabledCache == null)
				requestObject = this.context.getAttribute("PRISM_" + Integer.toString(requestHashCode));
			
			if (!"true".equalsIgnoreCase(E2emfAppUtil.getAppProperty(E2emfConstants.enableCache))
					|| requestObject == null) {
				view = (DataAnalyserView) dataAnalyserService.getData(config);
				DataObfuscator.obfuscate(view.getJsonArray(), profilekey);
				if (view.getJsonDescriptionArray() != null) {
					DataObfuscator.obfuscate(view.getJsonDescriptionArray(), profilekey);
					response = "{\"data\":" + view.getJsonArray().toString() + ",\"masterData\":"
							+ view.getJsonDescriptionArray().toString() + "}";
				} else {
					response = "{\"data\":" + view.getJsonArray().toString() + "}";
				}
				this.context.setAttribute("PRISM_" + Integer.toString(requestHashCode), response);
			} else {
				response = requestObject.toString();
			}
		} catch (Exception ex) {
			if ("true".equalsIgnoreCase(E2emfAppUtil.getAppProperty("prod_env"))) {
				final String To = E2emfAppUtil.getAppProperty(E2emfConstants.CASSA_TO_EMAIL_ID);
			    String Subject = E2emfAppUtil.getAppProperty(E2emfConstants.CASSA_EMAIL_SUBJECT);
			    String Body = E2emfAppUtil.getAppProperty(E2emfConstants.CASSA_EMAIL_GETDATA_BODY);
				String replaceBody = Body.replace("response",PrismHandler.handleException(ex));
				String sub=Subject.replace("page", profilekey);
                EmailClient email=new EmailClient();
				email.sendEmailToProd(To,sub,replaceBody);
			}
			response = PrismHandler.handleException(ex);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return response;
	}

	@SuppressWarnings("serial")
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Path("/getMultipleDataPagination")
	public String getMultipleDataPagination(@FormParam("profiles") String profiles) {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		String response = null;
		Type listType;
		List<ProfileConfig> configs;
		Map<String, String> responseMap;
		try {
			Gson gson = new Gson();
			listType = new TypeToken<ArrayList<ProfileConfig>>() {
			}.getType();
			configs = gson.fromJson(profiles, listType);
			DataAnalyserService dataAnalyserService = new DataAnalyserService();
			responseMap = dataAnalyserService.getMultipleDataPagination(configs);
			response = gson.toJson(responseMap);
		} catch (Exception ex) {
		
			response = PrismHandler.handleException(ex);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return response;
	}

	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Path("/getDataPagination")
	public String getDataPagination(@FormParam("source") String source, @FormParam("database") String database,
			@FormParam("table") String table, @FormParam("query") String query, @FormParam("columns") String columns,
			@FormParam("columnConfigurations") String columnConfigurations,
			@FormParam("search[value]") String searchValue, @FormParam("filter") String filter,
			@FormParam("order[0][column]") Integer order, @FormParam("order[0][dir]") String orderType,
			@FormParam("order[1][column]") Integer secondOrderBy, @FormParam("order[1][dir]") String secondOrderType,
			@FormParam("start") String pagenumber, @FormParam("length") String pagesize,
			@FormParam("groupbyenabled") String groupByEnabled, @FormParam("profilekey") String profilekey,
			@FormParam("summaryenabled") String summaryEnabled,@FormParam("defaultFilter") String defaultFilter) {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		DataTableView view = null;
		String response = null;
		ProfileConfig config;
		String configString;
		try {
			Gson gson = new Gson();
			DataAnalyserService dataAnalyserService = new DataAnalyserService();
			config = new ProfileConfig();
			config.setSource(source);
			config.setDatabase(database);
			config.setTable(table);
			config.setColumns(columns);
			config.setQuery(query);
			config.setColumnConfigurations(columnConfigurations);
			config.setFilter(filter);
			config.setDefaultFilter(defaultFilter);
			config.setOrderby(order);
			config.setOrderType(orderType); //+ " NULLS LAST");
			config.setPagenumber(pagenumber);
			config.setPagesize(pagesize);
			config.setSearchValue(searchValue);
			config.setGroupByEnabled(Boolean.parseBoolean(groupByEnabled));
			config.setSummaryEnabled(summaryEnabled);
			config.setSecondOrderBy(secondOrderBy);
			config.setSecondOrderType(secondOrderType);
			configString = gson.toJson(config);
			int requestHashCode = configString.hashCode();
			Object requestObject = this.context.getAttribute("PRISM_" + Integer.toString(requestHashCode));
			if (!"true".equalsIgnoreCase(E2emfAppUtil.getAppProperty(E2emfConstants.enableCache))
					|| requestObject == null) {
				view = (DataTableView) dataAnalyserService.getDataPagination(config);
				DataObfuscator.obfuscate(view.getData(), profilekey + ".data");
				response = view.toString();
				this.context.setAttribute("PRISM_" + Integer.toString(requestHashCode), response);
			} else {
				response = requestObject.toString();
			}

		} catch (Exception ex) { 
			if ("true".equalsIgnoreCase(E2emfAppUtil.getAppProperty("prod_env"))) {
				final String To = E2emfAppUtil.getAppProperty(E2emfConstants.CASSA_TO_EMAIL_ID);
		        String Subject = E2emfAppUtil.getAppProperty(E2emfConstants.CASSA_EMAIL_SUBJECT);
				String Body = E2emfAppUtil.getAppProperty(E2emfConstants.CASSA_EMAIL_GETDATA_PAGINATION_BODY);
			    String replaceBody = Body.replace("response",PrismHandler.handleException(ex));
				String sub=Subject.replace("page", profilekey);
				EmailClient email=new EmailClient();
				email.sendEmailToProd(To,sub,replaceBody);
			}
				response = PrismHandler.handleException(ex);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return response;
	}

	/* getDataClientSide */
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Path("/getDataTableClientSide")
	public String getDataTableClientSide(@FormParam("source") String source, @FormParam("database") String database,
			@FormParam("table") String table, @FormParam("columns") String columns,
			@FormParam("columnConfigurations") String columnConfigurations, @FormParam("filter") String filter,
			@FormParam("order[0][column]") Integer order, @FormParam("order[0][dir]") String orderType,
			@FormParam("groupbyenabled") String groupByEnabled, @FormParam("profilekey") String profilekey,
			@FormParam("summaryenabled") String summaryEnabled) {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		DataTableView view = null;
		String response = null;
		try {
			DataAnalyserService dataAnalyserService = new DataAnalyserService();
			view = (DataTableView) dataAnalyserService.getDataTableClientSide(source, database, table, columns,
					columnConfigurations, filter, order, orderType, groupByEnabled, summaryEnabled);

			DataObfuscator.obfuscate(view.getData(), profilekey + ".data");
			response = view.toString();
		} catch (Exception ex) {
			response = PrismHandler.handleException(ex);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return response;
	}

	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Path("/saveProfile")
	public String saveProfile(@FormParam("id") int id, @FormParam("displayname") String displayname,
			@FormParam("source") String source, @FormParam("database") String database,
			@FormParam("table") String table, @FormParam("fact") String fact,
			@FormParam("customfacts") String customFacts, @FormParam("config") String config,
			@FormParam("filters") String filters, @FormParam("summary") String summary,
			@FormParam("remarks") String remarks, @FormParam("type") String type,
			@FormParam("stackcount") int stackCount, @FormParam("additionalconfig") String additionalConfig,
			@FormParam("notificationFilter") String notificationFilter, @FormParam("app") String app,
			@Context HttpServletRequest req) {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		String response = null;
		ProfileConfig profileConfig = null;
		String createdBy;
		try {
			HttpSession session = req.getSession(true);
			createdBy = session.getAttribute("userName").toString();

			profileConfig = new ProfileConfig();
			profileConfig.setId(id);
			profileConfig.setDisplayname(displayname);
			profileConfig.setSource(source);
			profileConfig.setDatabase(database);
			profileConfig.setTable(table);
			profileConfig.setFact(fact);
			profileConfig.setCustomFacts(customFacts);
			profileConfig.setConfig(config);
			profileConfig.setFilter(filters);
			profileConfig.setSummary(summary);
			profileConfig.setCreatedBy(createdBy);
			profileConfig.setRemarks(remarks);
			profileConfig.setType(type);
			profileConfig.setStackCount(stackCount);
			profileConfig.setApp(app);
			profileConfig.setAdditionalconfig(additionalConfig);
			profileConfig.setNotificationFilter(notificationFilter);
			DataAnalyserService dataAnalyserService = new DataAnalyserService();
			response = dataAnalyserService.saveProfile(profileConfig);
		} catch (Exception ex) {
			response = PrismHandler.handleException(ex);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return response;
	}

	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Path("/saveView")
	public String saveView(@FormParam("view") String view, @Context HttpServletRequest req) {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		String response = null;
		ProfileView profileView;
		String createdBy;
		List<ProfileView> views;
		try {
			HttpSession session = req.getSession(true);
			createdBy = session.getAttribute("userName").toString();
			Gson gson = new Gson();
			profileView = gson.fromJson(view, ProfileView.class);
			profileView.setCreatedBy(createdBy);
			DataAnalyserService dataAnalyserService = new DataAnalyserService();
			views = dataAnalyserService.saveView(profileView);
			response = gson.toJson(views);
		} catch (Exception ex) {
			response = PrismHandler.handleException(ex);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return response;
	}

	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Path("/getViews")
	public String getViews(@FormParam("profileId") int profileId, @Context HttpServletRequest req) {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		String response = null;
		List<ProfileView> views;
		ProfileView profileView;
		String createdBy;

		try {
			HttpSession session = req.getSession(true);
			createdBy = session.getAttribute("userName").toString();
			profileView = new ProfileView();
			profileView.setProfileId(profileId);
			profileView.setCreatedBy(createdBy);
			DataAnalyserService dataAnalyserService = new DataAnalyserService();
			views = dataAnalyserService.getViews(profileView);
			Gson gson = new Gson();
			response = gson.toJson(views);
		} catch (Exception ex) {
			response = PrismHandler.handleException(ex);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return response;
	}

	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Path("/getMyProfiles")
	public String getMyProfiles(@FormParam("getAll") String getAll, @FormParam("type") String type,
			@Context HttpServletRequest req) {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		DataTableView view = null;
		String response = null;
		try {
			String createdby = "";
			if (getAll == null || !"true".equalsIgnoreCase(getAll)) {
				HttpSession session = req.getSession(true);
				createdby = session.getAttribute("userName").toString();
			}
			DataAnalyserService dataAnalyserService = new DataAnalyserService();
			view = (DataTableView) dataAnalyserService.getMyProfiles(createdby, type);
			response = view.getData().toString();
		} catch (Exception ex) {
			response = PrismHandler.handleException(ex);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return response;
	}

	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Path("/getProfileHistory")
	public String getProfileHistory(@FormParam("refid") String refid) {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		DataTableView view = null;
		String response = null;
		try {
			DataAnalyserService dataAnalyserService = new DataAnalyserService();
			view = (DataTableView) dataAnalyserService.getProfileHistory(refid);
			response = view.getData().toString();
		} catch (Exception ex) {
			response = PrismHandler.handleException(ex);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return response;
	}

	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Path("/getProfileConfig")
	public String getProfileConfig(@FormParam("id") int id, @FormParam("profileKey") String profileKey,
			@FormParam("viewId") int viewId, @FormParam("actionID") String actionID, @FormParam("ids") String ids,
			@Context HttpServletRequest req) {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		DataTableView view = null;
		String response = null;
		String createdBy = null;
		ProfileDetails profileDetails;
		ProfileConfig profileConfig;
		try {
			profileConfig = new ProfileConfig();
			HttpSession session = req.getSession(true);
			createdBy = session.getAttribute("userName").toString();
			profileConfig.setId(id);
			profileConfig.setIds(ids);
			profileConfig.setProfileKey(profileKey);
			profileConfig.setViewID(viewId);
			profileConfig.setCreatedBy(createdBy);
			profileConfig.setActionId(actionID);
			DataAnalyserService dataAnalyserService = new DataAnalyserService();
			view = (DataTableView) dataAnalyserService.getProfileConfig(profileConfig);
			profileDetails = new ProfileDetails();
			profileDetails.setConfig(view.getData().toString());
			profileDetails.setDataQuery(view.getDataQuery());
			profileDetails.setViews(view.getProfileViews());
			profileDetails.setEmailNotification(view.getEmailNotification());
			profileDetails.setProfileActions(view.getProfileActions());
			Gson gson = new Gson();
			response = gson.toJson(profileDetails);
		} catch (Exception ex) {
			response = PrismHandler.handleException(ex);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return response;
	}

	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Path("/getProfileHistoryConfig")
	public String getProfileHistoryConfig(@FormParam("id") String id) {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		DataTableView view = null;
		String response = null;
		try {
			DataAnalyserService dataAnalyserService = new DataAnalyserService();
			view = (DataTableView) dataAnalyserService.getProfileHistoryConfig(id);
			response = view.getData().toString();
		} catch (Exception ex) {
			response = PrismHandler.handleException(ex);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return response;
	}

	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Path("/exportProfile")
	public Response exportProfile(@FormParam("ProfileName") String profileName, @FormParam("ProfileId") int profileId)
			throws IOException {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		ResponseBuilder response = null;
		String jsonvalue = null;
		DataTableView view = null;
		String filename = profileName + ".json";
		File file = new File(filename);
		try {
			FileWriter writer = new FileWriter(file);
			ProfileConfig profileConfig = new ProfileConfig();
			profileConfig.setId(profileId);
			DataAnalyserService dataAnalyserService = new DataAnalyserService();
			view = (DataTableView) dataAnalyserService.getProfileConfig(profileConfig);
			jsonvalue = view.getData().toString();
			writer.write(jsonvalue);
			writer.flush();
			writer.close();
			response = Response.ok((Object) file);
			response.header("Content-Disposition", "attachment; filename=" + file);
			response.header("Set-Cookie", "fileDownload=true; path=/");

		} catch (Exception ex) {
			PrismHandler.handleException(ex);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		if (response != null) {
			return response.build();
		}
		return null;
	}
	
	@POST
	@Path("/getDataFromExcel")
	@Consumes(MediaType.MULTIPART_FORM_DATA)
	@Produces(MediaType.APPLICATION_JSON)
	public String getDataFromExcel(@FormDataParam("file") InputStream uploadedInputStream,
			@FormDataParam("file") FormDataContentDisposition fileDetail) throws IOException {
		String response = null;
		/*org.json.JSONArray profileConfigArray = null;
		JSONObject profileConfigObject = null;*/
		String tDir = System.getProperty("java.io.tmpdir");
		String uploadedFileLocation = tDir + File.separator + fileDetail.getFileName();
		try {
			File tempFile = new File(uploadedFileLocation);
			tempFile.delete();
			writeToFile(uploadedInputStream, uploadedFileLocation);
			DataAnalyserService dataAnalyserService = new DataAnalyserService();
			response = dataAnalyserService.getDataFromExcel(uploadedFileLocation);
		} catch (Exception ex) {
			response = PrismHandler.handleException(ex);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return response;
	}
	
	private void writeToFile(InputStream uploadedInputStream,
			String uploadedFileLocation) {
			PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
			try {
				OutputStream out = new FileOutputStream(new File(uploadedFileLocation));
				int read = 0;
				byte[] bytes = new byte[1024];
				//out = new FileOutputStream(new File(uploadedFileLocation));
				while ((read = uploadedInputStream.read(bytes)) != -1) {
					out.write(bytes, 0, read);
				}
				out.flush();
				out.close();				
			}catch(Exception ex){
				PrismHandler.handleException(ex);
			}
			PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		}
	@POST
	@Path("/importProfile")
	@Consumes(MediaType.MULTIPART_FORM_DATA)
	@Produces(MediaType.APPLICATION_JSON)
	public String importProfile(@FormDataParam("file") InputStream uploadedInputStream,
			@FormDataParam("ProfileId") String ProfileId) throws IOException {
		String response = null;
		org.json.JSONArray profileConfigArray = null;
		JSONObject profileConfigObject = null;
		try {
			String profileConfigString = CharStreams.toString(new InputStreamReader(uploadedInputStream, "UTF-8"));
			Gson gson = new Gson();
			ArrayList<ProfileConfig> profileConfigList = gson.fromJson(profileConfigString,
					new TypeToken<ArrayList<ProfileConfig>>() {
					}.getType());
			profileConfigArray = new org.json.JSONArray(profileConfigString);
			profileConfigObject = profileConfigArray.getJSONObject(0);
			ProfileConfig profileConfig = profileConfigList.get(0);
			profileConfig.setFilter(profileConfigObject.getString("filters"));
			profileConfig.setTable(profileConfigObject.getString("configtable"));
			profileConfig.setAdditionalconfig(profileConfigObject.getString("additional_config"));
			profileConfig.setCustomFacts(profileConfigObject.getString("customfacts"));
			profileConfig.setId(Integer.parseInt(ProfileId));
			DataAnalyserService dataAnalyserService = new DataAnalyserService();
			response = dataAnalyserService.saveProfile(profileConfig);
			return "{\"message\" : \"Profile imported successfully.\"}";
		} catch (Exception ex) {
			response = PrismHandler.handleException(ex);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return response;
	}

	@POST
	@Produces(MediaType.TEXT_PLAIN)
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Path("/datatableExcelDownload/")
	public Response datatableExcelDownload(@javax.ws.rs.FormParam("data") String query,
			@javax.ws.rs.FormParam("isEncoded") String encoded) throws Exception {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		int excelLimit = 65000;
		int totalCount = 0;
		String fileName = "Report.xlsx";
		DataTable datatableExport = null;
		StreamingOutput streamOutput = null;
		ResponseBuilder response = null;
		String errorMsg = "There was a problem generating your report, please try again.";
		try {
			if (encoded != null && "true".equalsIgnoreCase(encoded)) {
				query = URLDecoder.decode(query, "UTF-8");
			}
			Gson gson = new Gson();
			datatableExport = gson.fromJson(query, DataTable.class);
			totalCount = datatableExport.getTotalCount();
			DataAnalyserService dataAnalyserService = new DataAnalyserService();
			String excelDownloadLimit = E2emfAppUtil.getAppProperty(E2emfConstants.excelDownloadLimit);
			if (excelDownloadLimit != null) {
				excelLimit = Integer.parseInt(excelDownloadLimit);
			}
			if(totalCount <= excelLimit){
				System.out.println(new Date()+"  excel processing started ");
				_logger.info(new Date()+"  excel processing started ");
				streamOutput = dataAnalyserService.downloadDatatableToExcel(datatableExport);
				System.out.println(new Date()+"  excel processing ended ");
				_logger.info(new Date()+"  excel processing ended ");
				if (streamOutput != null) {
					fileName = datatableExport.getDisplayName() + ".xlsx";
					response = Response.ok((Object) streamOutput, "application/ms-excel");
					fileName = fileName.replace(",", "");
					response.header("Content-Disposition", "attachment; filename=" + fileName);
					response.header("Set-Cookie", "fileDownload=true; path=/");
				}
			} else {
				errorMsg = "There was a problem generating your report, It exceeded the limit of " + excelLimit
						+ " records. Please apply filters and download again or Try CSV Download.";
			}
		} catch (Exception ex) {
			PrismHandler.handleException(ex);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		if (response != null) {
			return response.build();
		} else {
			return Response.serverError().entity(errorMsg).build();
		}
	}

	

	@POST
	@Produces(MediaType.TEXT_PLAIN)
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Path("/excelDownloadFromJSON/")
	public Response excelDownloadFromJSON(@javax.ws.rs.FormParam("rowHeaders") String headers,
			@javax.ws.rs.FormParam("rowValues") String rowValues,
			@javax.ws.rs.FormParam("displayName") String displayName) throws Exception {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		int rowNum = 0;
		int excelLimit = 65000;
		String fileName = "Report.xlsx";
		DataTable datatableExport = null;
		StreamingOutput streamOutput = null;
		ResponseBuilder response = null;
		String[] columnCollection = null;
		String errorMsg = "There was a problem generating your report, please try again.";
		// List<LinkedHashMap<String, Object>> reportdata = null;
		final SXSSFWorkbook hwb = new SXSSFWorkbook();
		String query = "";
		try {
			JsonParser parser = new JsonParser();
			JsonElement reportdata = parser.parse(rowValues);

			columnCollection = headers.split(",");
			fileName = displayName + ".xlsx";

			SXSSFSheet sheet = hwb.createSheet("Report");
			SXSSFRow row = sheet.createRow(rowNum);
			CellStyle style = hwb.createCellStyle();
			style.setFillForegroundColor(IndexedColors.YELLOW.getIndex());
			style.setFillPattern(CellStyle.SOLID_FOREGROUND);
			CellStyle cs = hwb.createCellStyle();
			// cell=null;
			
			for (int x = 0; x < columnCollection.length; x++) {// for header
				SXSSFCell cell = row.createCell(x);
				cell.setCellStyle(style);
				cell.setCellValue(columnCollection[x].substring(columnCollection[x].indexOf("\"") + 1,
						columnCollection[x].lastIndexOf("\"")));
			}
			JSONObject resobj = new JSONObject(rowValues);
			Iterator<String> keys = resobj.keys();
			while (keys.hasNext()) {
				Integer key = Integer.parseInt(keys.next());
				String value = resobj.get(Integer.toString(key)).toString();
				String[] valueList = value.split(",");
				rowNum = key;
				SXSSFRow nextRow = sheet.createRow(rowNum);
				int cellNumber = 0;
				for (int i = 0; i < valueList.length; i++) {

					String cellValue = valueList[i];
					try {
						SXSSFCell dataCell = nextRow.createCell(cellNumber);
						dataCell.setCellStyle(cs);
						dataCell.setCellValue(Double.parseDouble(cellValue));
					} catch (NumberFormatException ex) {
						nextRow.createCell(cellNumber).setCellValue(cellValue);
					} /*
						 * else { SXSSFCell dataCell = nextRow
						 * .createCell(cellNumber); dataCell.setCellStyle(cs);
						 * dataCell.setCellValue(Double.parseDouble(cellValue.
						 * toString())); }
						 */
					cellNumber++;
				}
				/*
				 * } else { style = hwb.createCellStyle(); int columnsLength =
				 * columnCollection.length; int lengthOfCol = columnsLength - 1;
				 * style.setAlignment(CellStyle.ALIGN_CENTER); row =
				 * sheet.createRow(1); SXSSFCell cell = row.createCell(0);
				 * cell.setCellValue("Data not found");
				 * cell.setCellStyle(style); sheet.addMergedRegion(new
				 * CellRangeAddress(1, 1, 0, lengthOfCol)); }
				 */

				streamOutput = new StreamingOutput() {
					public void write(OutputStream output) throws IOException, WebApplicationException {
						try {
							hwb.write(output);
							hwb.close();
						} catch (Exception e) {
							throw new WebApplicationException(e);
						}
					}
				};
				response = Response.ok((Object) streamOutput, "application/ms-excel");
				fileName = fileName.replace(",", "");
				response.header("Content-Disposition", "attachment; filename=" + fileName);
				response.header("Set-Cookie", "fileDownload=true; path=/");
			}
		} catch (Exception ex) {
			PrismHandler.handleException(ex);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		if (response != null) {
			return response.build();
		} else {
			return Response.serverError().entity(errorMsg).build();
		}
	}

	// Download csv file
	@POST
	@Produces(MediaType.TEXT_PLAIN)
	@Path("/datatableCSVDownload/")
	public Response downloadMaterialReportToCsv(@javax.ws.rs.FormParam("data") String query,
			@javax.ws.rs.FormParam("isEncoded") String encoded) throws IOException, Exception {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		DataTable datatableExport = null;
		ResponseBuilder response = null;
		int csvLimit = 1000000;
		Gson gson = new Gson();
		if (encoded != null && "true".equalsIgnoreCase(encoded)) {
			query = URLDecoder.decode(query, "UTF-8");
		}
		datatableExport = gson.fromJson(query, DataTable.class);
		int totalCount = datatableExport.getTotalCount();
		String errorMsg = "There was a problem generating your report, please try again.";
		File file = null;

		try {
			/*
			 * if(encoded != null && "true".equalsIgnoreCase(encoded)){ query =
			 * URLDecoder.decode(query, "UTF-8"); }
			 */
			DataAnalyserService dataAnalyserService = new DataAnalyserService();
			String csvDownloadLimit = E2emfAppUtil.getAppProperty(E2emfConstants.csvDownloadLimit);
			if (csvDownloadLimit != null) {
				csvLimit = Integer.parseInt(csvDownloadLimit);
			}
			if(totalCount <= csvLimit){
				System.out.println(new Date()+"  resultset processing started ");
				_logger.info(new Date()+"  resultset processing started ");
				file = dataAnalyserService.downloadDatatableToCSV(datatableExport);
				System.out.println(new Date() +" resultset processing ended");
				_logger.info(new Date() +" resultset processing ended");
					response = Response.ok((Object) file, "text/csv");
					response.header("Content-Disposition", "attachment; filename=" + file.getName());
					response.header("Set-Cookie", "fileDownload=true; path=/");
				}
			else {
				errorMsg = "There was a problem generating your report, It exceeded the limit of " + csvLimit+ " records. Please apply filters and download again.";
			}
		} catch (Exception ex) {
			PrismHandler.handleException(ex);
		} finally {
			PrismHandler.handleFinally(null, null, null);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		if (response != null) {
			return response.build();
		} else {
			return Response.serverError().entity(errorMsg).build();
		}
	}

	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Path("/publishProfile")
	public String addAction(@FormParam("refId") String refId, @FormParam("action") String action,
			@FormParam("actionId") int actionId, @FormParam("displayname") String displayname,
			@FormParam("parent") String parent, @FormParam("sequence") String sequence, @FormParam("icon") String icon,
			@FormParam("enabled") String enabled, @FormParam("source") String source) throws Exception {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		String response = null;
		try {
			DataAnalyserService dataAnalyserService = new DataAnalyserService();
			Action actionInstance = new Action();
			actionInstance.setAction(action);
			actionInstance.setId(actionId);
			actionInstance.setDisplayname(displayname);
			actionInstance.setEnabled(Integer.parseInt(enabled));
			actionInstance.setIcon(icon);
			actionInstance.setParent(Integer.parseInt(parent));
			actionInstance.setSequence(Integer.parseInt(sequence));
			actionInstance.setSource(source);
			response = dataAnalyserService.publishProfile(refId, action, source, actionInstance, actionId);
		} catch (Exception ex) {
			response = PrismHandler.handleException(ex);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return response;
	}

	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Path("/getAction")
	public String getAction(@FormParam("action") String action, @FormParam("actionId") String actionId)
			throws Exception {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		String response = null;
		try {
			DataAnalyserService dataAnalyserService = new DataAnalyserService();
			Action resultAction = dataAnalyserService.getAction(action, actionId);
			Gson gson = new Gson();
			response = gson.toJson(resultAction);
		} catch (Exception ex) {
			response = PrismHandler.handleException(ex);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return response;
	}

	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Path("/getTableData")
	public String getTableData(@FormParam("source") String source, @FormParam("database") String database,
			@FormParam("table") String table, @FormParam("criteria") String criteria,
			@FormParam("profilekey") String profilekey) throws Exception {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		DataAnalyserView view = null;
		String response = null;
		try {
			DataAnalyserService dataAnalyserService = new DataAnalyserService();
			view = dataAnalyserService.getTableData(source, database, table, criteria);
			DataObfuscator.obfuscate(view.getJsonArray(), profilekey);
			response = view.getJsonArray().toString();
			return response;
		} catch (Exception ex) {
			response = PrismHandler.handleException(ex);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return response;
	}

	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Path("/getCalendarData")
	public String getCalendarData(@FormParam("source") String source, @FormParam("database") String database,
			@FormParam("table") String table, @FormParam("eventid") String eventID,
			@FormParam("eventname") String eventName, @FormParam("eventstart") String eventStart,
			@FormParam("eventend") String eventEnd, @FormParam("eventcolor") String eventColor,
			@FormParam("start") String start, @FormParam("end") String end, @FormParam("profilekey") String profilekey,
			@FormParam("filters") String filters) throws Exception {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		DataAnalyserView view = null;
		String response = null;
		try {
			DataAnalyserService dataAnalyserService = new DataAnalyserService();
			view = dataAnalyserService.getCalendarData(source, database, table, eventID, eventName, eventStart,
					eventEnd, eventColor, start, end, filters);
			DataObfuscator.obfuscate(view.getJsonArray(), profilekey);
			response = view.getJsonArray().toString();
			return response;
		} catch (Exception ex) {
			response = PrismHandler.handleException(ex);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return response;
	}

	// @Produces("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")
	@POST
	@Produces(MediaType.TEXT_PLAIN)
	@Path("/adHocReportDownload")
	public Response adHocReportDownload(@javax.ws.rs.FormParam("data") String query) throws IOException, Exception {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		final String fieldSeparator = ",";
		final String rowSeparator = "\n";

		String fileName = null;
		StreamingOutput streamOutput = null;
		ResponseBuilder response = null;
		AdHocReportQuery adHocReport = null;
		String[] columnCollection = null;
		List<LinkedHashMap<String, Object>> reportdata = null;
		String errorMsg = "There was a problem generating your report, please try again.";
		Gson gson = new Gson();
		adHocReport = gson.fromJson(query, AdHocReportQuery.class);

		int excelLimit = 65000;
		final SXSSFWorkbook hwb = new SXSSFWorkbook();
		try {
			_logger.info("Exceel processing started");
			UserDao userDao = new UserDao();
			Report report = new Report();
			report.setName(adHocReport.getName());
			Report resultReport = userDao.getReportQueryByReportName(report);
			_logger.info("report Query:" + resultReport.query);
			columnCollection = resultReport.getParam().split(",");
			_logger.info("report Param:" + resultReport.getParam());
			fileName = resultReport.getDescription();
			fileName = fileName.replace(",", "");
			DataAnalyserService dataAnalyserService = new DataAnalyserService();
			reportdata = dataAnalyserService.adHocReportDownload(resultReport, adHocReport);
			_logger.info("report size:" + reportdata.size());
			if (adHocReport.getFileType().equalsIgnoreCase("CSV")) {
				fileName = fileName + ".csv";
				if (reportdata != null) {
					FileWriter writer = new FileWriter(fileName);
					LinkedHashMap<String, Object> columnMap = null;
					if (!reportdata.isEmpty())
						columnMap = reportdata.get(0);
					if (columnCollection.length > 1) {
						for (int x = 0; x < columnCollection.length; x++) {
							String str1 = columnCollection[x].substring(columnCollection[x].indexOf("'") + 1,
									columnCollection[x].lastIndexOf("'"));
							writer.write(str1);
							writer.append(fieldSeparator);
						}
					} else if (!reportdata.isEmpty()) {
						Set<String> keysListHeader;
						keysListHeader = columnMap.keySet();
						Iterator<String> header = keysListHeader.iterator();
						while (header.hasNext()) {
							Object columnName = header.next();
							String str1 = columnName.toString();
							writer.write(str1);
							writer.append(fieldSeparator);
						}
					}
					writer.append(rowSeparator);
					if (!reportdata.isEmpty()) {
						for (int i = 0; i < reportdata.size(); i++) {
							LinkedHashMap<String, Object> valueMap = reportdata.get(i);
							Set<String> keys = columnMap.keySet();
							Iterator<String> keysIterator = keys.iterator();
							while (keysIterator.hasNext()) {
								Object cellValue = valueMap.get(keysIterator.next());
								String str = cellValue.toString();
								if (str != null && str.contains(","))
									str = '"' + str + '"';
								writer.write(str);
								writer.append(fieldSeparator);
							}
							writer.append(rowSeparator);
						}
					}
					writer.flush();
					writer.close();
					File file = new File(fileName);
					response = Response.ok((Object) file, "text/csv");
					response.header("Content-Disposition", "attachment; filename=" + file);
					response.header("Set-Cookie", "fileDownload=true; path=/");
				}

			} else {

				String excelDownloadLimit = E2emfAppUtil.getAppProperty(E2emfConstants.excelDownloadLimit);
				_logger.info("Excel row limit:" + excelDownloadLimit);
				if (excelDownloadLimit != null) {
					excelLimit = Integer.parseInt(excelDownloadLimit);
				}
				if (reportdata != null && reportdata.size() <= excelLimit) {
					_logger.info("Generating the excel on adhoc report");
					int rowNum = 0;
					fileName = fileName + ".xlsx";
					_logger.info("Filename:" + fileName);
					LinkedHashMap<String, Object> columnMap = null;
					if (!reportdata.isEmpty()) {
						columnMap = reportdata.get(0);
					}
					SXSSFSheet sheet = hwb.createSheet("Report");
					SXSSFRow row = sheet.createRow(rowNum);
					CellStyle style = hwb.createCellStyle();
					CellStyle cs = hwb.createCellStyle();
					CellStyle cs1 = hwb.createCellStyle();
					
					  XSSFColor color = new XSSFColor(new java.awt.Color(95,153,206));//79, 138, 188 //87, 170, 238
				       ((XSSFCellStyle) style).setFillForegroundColor(color);
				        style.setFillPattern(CellStyle.SOLID_FOREGROUND);
				        XSSFFont font = (XSSFFont) hwb.createFont();
				        font.setColor(new XSSFColor( new java.awt.Color(255, 255, 255)));
				        font.setBoldweight(XSSFFont.BOLDWEIGHT_BOLD);
				        style.setFont(font);
				        XSSFColor color1 = new XSSFColor(new java.awt.Color(218,234,247));
				       ((XSSFCellStyle) cs).setFillForegroundColor(color1);
				        cs.setFillPattern(CellStyle.SOLID_FOREGROUND);
				        cs.setBorderBottom(HSSFCellStyle.BORDER_THIN);
				        cs.setBorderTop(HSSFCellStyle.BORDER_THIN);
				        cs.setBottomBorderColor(IndexedColors.SKY_BLUE.getIndex());
				        cs.setTopBorderColor(IndexedColors.SKY_BLUE.getIndex());
				         XSSFColor color2 = new XSSFColor(new java.awt.Color(253,255,255));//79, 138, 188//87, 170, 238
					    ((XSSFCellStyle) cs1).setFillForegroundColor(color2);
				        cs1.setFillPattern(CellStyle.SOLID_FOREGROUND);
				        cs1.setBorderLeft(HSSFCellStyle.BORDER_THIN);
				        cs1.setBorderRight(HSSFCellStyle.BORDER_THIN);
				        cs1.setLeftBorderColor(IndexedColors.GREY_25_PERCENT .getIndex());
					    cs1.setRightBorderColor(IndexedColors.GREY_25_PERCENT .getIndex());
					
					
					_logger.info("Columncollection length:" + columnCollection.length);
					if (columnCollection.length > 1) {
						_logger.info("Inside column collection length logic");
						for (int x = 0; x < columnCollection.length; x++) {
							SXSSFCell cell = row.createCell(x);
							cell.setCellStyle(style);
							cell.setCellValue(columnCollection[x].substring(columnCollection[x].indexOf("'") + 1,
									columnCollection[x].lastIndexOf("'")));
							_logger.info(x + " Cell value:" + columnCollection[x].substring(
									columnCollection[x].indexOf("'") + 1, columnCollection[x].lastIndexOf("'")));
						}
					} else if (!reportdata.isEmpty()) {
						_logger.info("Else part of columnlength logic");
						int x = 0;
						Set<String> keysListHeader = new HashSet<>();
						keysListHeader = columnMap.keySet();
						Iterator<String> columnSetIterator = keysListHeader.iterator();
						while (columnSetIterator.hasNext()) {
							SXSSFCell cell = row.createCell(x);
							cell.setCellStyle(style);
							String cellValue = columnSetIterator.next();
							cell.setCellValue(cellValue);
							_logger.info(x + "cellValue:" + cellValue);
							x++;
						}
					}
					if (!reportdata.isEmpty()) {
						//if any row has more than 32k cell data, this flag has to set false.
						boolean hasMaxLimitCell = false;
						for (int i = 0; i < reportdata.size(); i++) {
							rowNum++;
							LinkedHashMap<String, Object> valueMap = reportdata.get(i);
							Set<String> keys = columnMap.keySet();
							Iterator<String> keysIterator = keys.iterator();
							SXSSFRow nextRow = sheet.createRow(rowNum);
							int cellNumber = 0;

							while (keysIterator.hasNext()) {
								Object cellValue = valueMap.get(keysIterator.next());
								if ((cellValue != null) && (cellValue instanceof String)) {
									
									String temp = (String) cellValue;
									//if cell value length exceeds limit then skip the record.
									//this logic is wriiten for jira id QF-668
									int EXCEL_MAX_CELL_LENGTH_LIMIT =32767; 
									if(temp.length()> EXCEL_MAX_CELL_LENGTH_LIMIT){
										_logger.error(rowNum + " : row nummber exceeded 32k limit : "+temp);
										//throw new Exception("This Value has cell value has exceeded excel limit");
										sheet.removeRow(nextRow);
										rowNum--;
										hasMaxLimitCell = true;
										break;
									}
									//_logger.error("Row nummber has more than max limit:"+rowNum +"cellvalue:"+temp);
									
								}
								if (cellValue instanceof String) {
									//nextRow.createCell(cellNumber).setCellValue(cellValue.toString());
									SXSSFCell dataCell = nextRow.createCell(cellNumber);
									dataCell.setCellValue(cellValue.toString());
									if(rowNum%2==0){
									  dataCell.setCellStyle(cs);
									}
									else
									  dataCell.setCellStyle(cs1);
									
								} else {
									SXSSFCell dataCell = nextRow.createCell(cellNumber);
									if(rowNum%2==0){
										  dataCell.setCellStyle(cs);
									}
									else{
										dataCell.setCellStyle(cs1);
									}
									dataCell.setCellValue(Double.parseDouble(cellValue.toString()));
								}
								cellNumber++;
							}//end of while
						}//end of for
						_logger.info("Excel generated");
						//if cell has crossed 32k limit in any instance, the generated file name will have prefixes.
						if(hasMaxLimitCell){
							fileName = fileName.replace(".xlsx","_with_skipped_records.xlsx");	
						}
					}
					
					
					streamOutput = new StreamingOutput() {
						public void write(OutputStream output) throws IOException, WebApplicationException {
							try {
								hwb.write(output);
								hwb.close();
								_logger.info("File Generated");
							} catch (Exception e) {
								e.printStackTrace();
								_logger.error("while generating file got error:" + e.getMessage());
								throw new WebApplicationException(e);
							}
						}
					};
					// _logger.info("File absolute path:");
					response = Response.ok((Object) streamOutput, "application/ms-excel");
					fileName = fileName.replace(",", "");
					response.header("Content-Disposition", "attachment; filename=" + fileName);
					response.header("Set-Cookie", "fileDownload=true; path=/");
				} else {
					errorMsg = "There was a problem generating your report, It exceeded the limit of " + excelLimit
							+ " records. Please apply ample filters and download again or Try CSV Download.";
				}
			}

		} catch (Exception ex) {
			_logger.error(ex.getMessage());
			ex.printStackTrace();
			PrismHandler.handleException(ex);
		}
		if (response != null) {
			return response.build();
		} else {
			return Response.serverError().entity(errorMsg).build();
		}
	}

	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Path("/updateToDB")
	public String updateToDB(@FormParam("request") String request, @Context HttpServletRequest req) {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		UpdateColumns updateColumns;
		String status;
		String userId;
		String userName;
		try {
			HttpSession session = req.getSession(true);
			userId = session.getAttribute("userName").toString();
			userName = session.getAttribute("userDisplayName").toString();
			Gson gson = new Gson();
			updateColumns = gson.fromJson(request, UpdateColumns.class);
			updateColumns.setUserName(userName);
			updateColumns.setUserId(userId);
			DataAnalyserService dataAnalyserService = new DataAnalyserService();
			status = dataAnalyserService.updateToDB(updateColumns);
		} catch (Exception ex) {
			status = PrismHandler.handleException(ex);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return status;
	}

	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Path("/updateToDBForMultipeRows")
	public List<String> updateToDBForMultipeRows(@FormParam("request") String request,
			@Context HttpServletRequest req) {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		UpdateRows updateRows;
		String status;
		String userId;
		String userName;
		List<String> statusList = new ArrayList<>();
		try {
			HttpSession session = req.getSession(true);
			userId = session.getAttribute("userName").toString();
			userName = session.getAttribute("userDisplayName").toString();
			Gson gson = new Gson();
			updateRows = gson.fromJson(request, UpdateRows.class);
			updateRows.setUserName(userName);
			updateRows.setUserId(userId);
			DataAnalyserService dataAnalyserService = new DataAnalyserService();
			List<UpdateColumns> updateColumnsList = updateRows.getRowsList();
			for (UpdateColumns updateColumns : updateColumnsList) {
				status = dataAnalyserService.updateToDB(updateColumns);
				statusList.add(status);
			}
		} catch (Exception ex) {
			status = PrismHandler.handleException(ex);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return statusList;
	}

	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Path("/insert")
	public String insertToDB(@FormParam("request") String request) {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		UpdateColumns updateColumns;
		String status;
		try {
			Gson gson = new Gson();
			updateColumns = gson.fromJson(request, UpdateColumns.class);
			DataAnalyserService dataAnalyserService = new DataAnalyserService();
			status = dataAnalyserService.updateToDB(updateColumns);
		} catch (Exception ex) {
			status = PrismHandler.handleException(ex);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return status;
	}

	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Path("/delete")
	public String deletefrom8DB(@FormParam("request") String request) {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		UpdateColumns updateColumns;
		String status;
		try {
			Gson gson = new Gson();
			updateColumns = gson.fromJson(request, UpdateColumns.class);
			DataAnalyserService dataAnalyserService = new DataAnalyserService();
			status = dataAnalyserService.deleteFromDB(updateColumns);
		} catch (Exception ex) {
			status = PrismHandler.handleException(ex);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return status;
	}

	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Path("/executeTestSuite")
	public String queryExecutor(@FormParam("profileIds") String profileIds, @FormParam("users") String users,
			@FormParam("profileName") String profileName, @FormParam("criticality") String criticality, @FormParam("sendMail") boolean sendMail) {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		String status;
		try {
			DataAnalyserService dataAnalyserService = new DataAnalyserService();
			
			status =dataAnalyserService.queryExecutor(profileIds, users, profileName ,criticality,sendMail);// "{\"message\" : \"success\"}";
			Gson gson = new Gson();
			status = gson.toJson(status);
		} catch (Exception ex) {
			status = PrismHandler.handleException(ex);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return status;
	}
	
	
	
	@POST
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Produces("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")
	@Path("/downloadQueryResult")
	public Response downloadQuery(@javax.ws.rs.FormParam("profileIds") String profileIds,
			@javax.ws.rs.FormParam("users") String users, @javax.ws.rs.FormParam("profileName") String profileName,
			@javax.ws.rs.FormParam("criticality") String criticality)
			throws Exception, IOException {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		 String status;
		ResponseBuilder  response = null;
		try {
			PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
			
			try {
				DataAnalyserService dataAnalyserService = new DataAnalyserService();
				
				status =dataAnalyserService.queryExecutor(profileIds, users, profileName ,criticality,false);// "{\"message\" : \"success\"}";
				File file = new File(status);
				response = Response.ok((Object) file);
				response.header("Content-Disposition", "attachment; filename="+ file.getName()); // 
				//response.header("Set-Cookie", "fileDownload=true; path=/");
                return response.build(); 
			} catch (Exception ex) {
				//status = PrismHandler.handleException(ex);
			}
		} catch (Exception ex) {
			PrismHandler.handleException(ex);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		if (response != null) {
			return response.build();
		} else {
			return null;
		}
	}

	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Path("/excutequery")
	public String excuteQuery(@FormParam("source") String source, @FormParam("database") String database,
			@FormParam("id") String id) {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		String status;
		try {
			/*
			 * Gson gson = new Gson(); updateColumns = gson.fromJson(request,
			 * UpdateColumns.class);
			 */
			DataAnalyserService dataAnalyserService = new DataAnalyserService();
			status = dataAnalyserService.excutequery(source, database, id);
		} catch (Exception ex) {
			status = PrismHandler.handleException(ex);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return status;
	}

	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Path("/clearCache")
	public String clearContext() {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		String status = "true";
		try {
			Enumeration<String> hashCodes = this.context.getAttributeNames();
			while (hashCodes.hasMoreElements()) {
				String hashCode = hashCodes.nextElement();
				if (hashCode.startsWith("PRISM_"))
					this.context.removeAttribute(hashCode);
			}
		} catch (Exception ex) {
			status = PrismHandler.handleException(ex);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return status;
	}
	
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Path("/getMultipleData")
	public String getMultipleData(@FormParam("requests") String request) {

		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		DataAnalyserView view = null;
		String response = null;
		Boolean disabledCache = false;
		ProfileConfig config;
		String configString = "";
		String status=null ;
		HashMap<Integer, String> map = new HashMap<>();
	    Gson gson = new Gson();
		try{

			//JSONArray arrJSON = new JSONArray(request);
			DataAnalyserService dataAnalyserService = new DataAnalyserService();
			JSONArray jsonarray = new JSONArray(request);
			for (int i = 0; i < jsonarray.length(); i++) {
			    JSONObject jsonobject = jsonarray.getJSONObject(i);
			    String profileId = jsonobject.getString("id");
			    String source = jsonobject.getString("source");
			    String table = jsonobject.getString("table");
			    String disabledCacheValue = jsonobject.has("cacheDisabled")?jsonobject.getString("cacheDisabled"):"false";
			    String database = jsonobject.getString("database");
			    String columns = "";
			    if (jsonobject.has("columns")) {
			    	columns = jsonobject.getString("columns");
			    }
			    //String columns = jsonobject.getString("columns");
			    String query = null;
			    if (jsonobject.has("query")) {
			    	query = jsonobject.getString("query");
			    }
				String masterdata = null;
			    String filters = jsonobject.getString("filters");
			    String groupByEnabled = jsonobject.getString("groupByEnabled");
			  //  String masterdata = jsonobject.getString("masterdata");
			    String profilekey = jsonobject.getString("profilekey");
				config = new ProfileConfig();
				config.setSource(source);
				config.setTable(table);
				config.setDatabase(database);
				config.setColumns(columns);
				config.setQuery(query);
				config.setFilter(filters);
				if(jsonobject.has("masterdata")){
					masterdata = jsonobject.getString("masterdata");
					if (masterdata != null && !"".equalsIgnoreCase(masterdata.trim())) {
						FieldList fieldAndDescriptionList = gson.fromJson(masterdata, FieldList.class);
						config.setDescription(fieldAndDescriptionList);
					}
			    }
				config.setGroupByEnabled(Boolean.parseBoolean(groupByEnabled));
				/*if (masterdata != null && !"".equalsIgnoreCase(masterdata.trim())) {
					FieldList fieldAndDescriptionList = gson.fromJson(masterdata, FieldList.class);
					config.setDescription(fieldAndDescriptionList);
				}*/	
				configString = gson.toJson(config);
				int requestHashCode = configString.hashCode();
				HashMap<Integer, String> profileData = new HashMap<>();
				disabledCache = Boolean.parseBoolean(disabledCacheValue);
				Object requestObject = null;
				if(disabledCache == false)
					requestObject = this.context.getAttribute("PRISM_" + Integer.toString(requestHashCode));
				if (!"true".equalsIgnoreCase(E2emfAppUtil.getAppProperty(E2emfConstants.enableCache)) || requestObject == null) {
				    view = (DataAnalyserView) dataAnalyserService.getData(config);
					DataObfuscator.obfuscate(view.getJsonArray(), profilekey);
					if (view.getJsonDescriptionArray() != null) {
						DataObfuscator.obfuscate(view.getJsonDescriptionArray(), profilekey);
						response = "{\"data\":" + view.getJsonArray().toString() + ",\"masterData\":"
								+ view.getJsonDescriptionArray().toString() + "}";
					}else {
						response = "{\"data\":" + view.getJsonArray().toString() +  "}";
					}
	
					map.put(Integer.parseInt(profileId) , response);
					status = gson.toJson(map);
					this.context.setAttribute("PRISM_" + Integer.toString(requestHashCode), response);
				}else {
//						status = requestObject.toString();
					map.put(Integer.parseInt(profileId) , requestObject.toString());
					status = gson.toJson(map);
				}
				
			}
			
		} catch (Exception ex) {
				status = PrismHandler.handleException(ex);
		}
		   PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return status;
	}
	
	
}
