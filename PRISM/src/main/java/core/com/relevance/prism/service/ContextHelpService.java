package com.relevance.prism.service;

import java.util.List;

import com.relevance.prism.dao.ContextHelpDao;
//import com.relevance.e2emf.dao.UserDao;
import com.relevance.prism.data.ContextHelp;
//import com.relevance.e2emf.commons.EmailClient;
import com.relevance.prism.util.Log;
import com.relevance.prism.util.PrismHandler;
//import com.relevance.e2emf.domain.User;

public class ContextHelpService extends BaseService {

	public String insertFaq(Object... obj) throws Exception{
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		ContextHelp contextHelp = new ContextHelp();
		String responseMessage = null;
		try{
			Log.Info("insertFaq method called on ContextHelpService");
			ContextHelpDao contextHelpDao = new ContextHelpDao();
				if (obj[0] != null)
					contextHelp.question = obj[0].toString();
				contextHelp.answer = obj[1].toString();
				contextHelp.screen  = obj[2].toString();
				contextHelp.role=obj[3].toString();
			//user.enabled  = Long.parseLong(obj[2].toString());
				/*String [] role = (obj[3].toString()).split(",");
				contextHelp.roles   = role;*/
				
				//contextHelp.enable_disable  = obj[4].toString();
				contextHelp.enabled=Long.parseLong(obj[4].toString());
			
				responseMessage = contextHelpDao.insertFaq(contextHelp);
			
			
			Log.Info("Fetched Faq object from DAO " + contextHelp);
			return responseMessage;		
		}catch(Exception e){
			PrismHandler.handleException(e, true);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return responseMessage;	
	}
	
	/*Update Frequently Asked Questions*/
	public String updateFaq(Object... obj) throws Exception{
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		//User resultUser = new User();
		String responseMessage = null;
		try{
			Log.Info("updateFaq method called on ContextHelpService");
			ContextHelpDao contextHelpDao = new ContextHelpDao();
			ContextHelp contextHelp = new ContextHelp();
			System.out.println(Long.parseLong(obj[0].toString()));
			contextHelp.id = (Long.parseLong(obj[0].toString()));
			contextHelp.question = obj[1].toString();
			contextHelp.answer = obj[2].toString();
			contextHelp.screen = (obj[3].toString());
			contextHelp.role  = obj[4].toString();
			//contextHelp.enable_disable = obj[5].toString();
			/*contextHelp [] roles = (obj[4].toString()).split(",");
			user.rolesAsArray   = roles;*/
			contextHelp.setEnabled(Long.parseLong(obj[5].toString()));
			
			
			/*contextHelp [] roles = (obj[4].toString()).split(",");
			user.rolesAsArray   = roles;
			user.setPrimaryRole(obj[5].toString());
			//resultUser = userDao.updateUser(user);
*/			responseMessage = contextHelpDao.updateFaq(contextHelp);
			return responseMessage;
		}catch(Exception e){
			PrismHandler.handleException(e, true);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return responseMessage;
	}
	
	
	/*delete faq*/
	public String deleteFaq(Object... obj) throws Exception{
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		String successMessage = null;
		try{
			Log.Info("deletefaq method called on ContextHelpService");
			ContextHelpDao contextHelpDao = new ContextHelpDao();
			ContextHelp contextHelp = new ContextHelp();
			contextHelp.id = Long.parseLong(obj[0].toString());
			successMessage = contextHelpDao.deleteFaq(contextHelp);
			return successMessage;
		}catch(Exception e){
			PrismHandler.handleException(e, true);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return successMessage;
	}
	
	
	/*get records from db based on id*/
	public ContextHelp getFaq(Object... obj) throws Exception{
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		ContextHelp contextHelp = new ContextHelp();
		try{
			Log.Info("getFaq method called on ContextHelpService");
			ContextHelpDao contextHelpDao = new ContextHelpDao();
			contextHelp.id = Long.parseLong(obj[0].toString());
			contextHelp = contextHelpDao.getFaq(contextHelp);				
			Log.Info("Fetched contextHelp object from DAO " + contextHelp);
				
		}catch(Exception e){
			PrismHandler.handleException(e, true);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return contextHelp;
	}
	
	/*get all records from faq*/
	public List<ContextHelp> getFaqList(String roles) throws Exception{
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		List<ContextHelp> faqList = null;
		try{
			Log.Info("getUserList method called on UserService");
			ContextHelpDao contextHelpDao = new ContextHelpDao();
			faqList = contextHelpDao.getFaqList(roles);
			return faqList;
		}catch(Exception e){
			PrismHandler.handleException(e, true);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return faqList;
	}
	
}
