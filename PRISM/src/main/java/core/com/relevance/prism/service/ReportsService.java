package com.relevance.prism.service;

import java.util.LinkedHashMap;
import java.util.List;

import com.relevance.prism.dao.ReportsDao;
import com.relevance.prism.data.Report;
import com.relevance.prism.util.Log;
import com.relevance.prism.util.PrismHandler;

public class ReportsService extends BaseService {

	public List<Report> getReportsList() throws Exception{
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		List<Report> reportsList = null;
		try{
			Log.Info("getReportList() method called on ReportService");
			ReportsDao reportDao = new ReportsDao();
			reportsList = reportDao.getReportsList();
			return reportsList;
		}catch(Exception e){
			PrismHandler.handleException(e, true);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return reportsList;
	}
	
	public String insertReport(Object... obj) throws Exception{
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		Report report = new Report();
		String responseMessage = null;
		try{
			Log.Info("insertReport() method called on ReportsService");
			ReportsDao reportsDao = new ReportsDao();
			report.setName((String) obj[0]);
			report.setDescription((String) obj[1]);
			report.setQuery((String) obj[2]);
			report.setParam((String) obj[3]);
			report.setSource((String) obj[4]);
			report.setRoleid(Integer.valueOf((String) obj[5]).intValue());
			report.setDbname((String) obj[6]); 
			responseMessage = reportsDao.insertReport(report);
			Log.Info("Inserted Report in table.");
			return responseMessage;		
		}catch(Exception e){
			PrismHandler.handleException(e, true);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return responseMessage;	
	}
	
	public String deleteReport(Object... obj) throws Exception{
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		String successMessage = null;
		try{
			Log.Info("deleteReport() method called on ReportsService");
			ReportsDao reportsDao = new ReportsDao();
			Report report = new Report();
			report.setId(obj[0].toString());
			successMessage = reportsDao.deleteReport(report);
			return successMessage;
		}catch(Exception e){
			PrismHandler.handleException(e, true);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return successMessage;
	}
	
	public String updateReport(Object... obj) throws Exception{
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		Report report = new Report();
		String responseMessage = null;
		try{
			Log.Info("updateReport() method called on ReportsService");
			ReportsDao reportsDao = new ReportsDao();
			report.setId((String) obj[0]);
			report.setName((String) obj[1]);
			report.setDescription((String) obj[2]);
			report.setQuery((String) obj[3]);
			report.setParam((String) obj[4]);
			report.setSource((String) obj[5]);
			report.setRoleid(Integer.valueOf((String) obj[6]).intValue());
			report.setDbname((String) obj[7]); 
			responseMessage = reportsDao.updateReport(report);
			Log.Info("Updated Report in table.");
			return responseMessage;		
		}catch(Exception e){
			PrismHandler.handleException(e, true);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return responseMessage;	
	}
	
	public List <LinkedHashMap <String,Object>> getPPVReportsByDynamicQuery(Object... obj) throws Exception{
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		Log.Info("getPPVReportsByDynamicQuery() method called on ReportsService");
		ReportsDao reportsDao = new ReportsDao();
		List <LinkedHashMap <String,Object>> reportsData = reportsDao.getPPVReportsByDynamicQuery(obj);
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return reportsData;
	}
	
	public List <LinkedHashMap <String,Object>> getEMEAPPVProfileL2ReportByFilter(Object... obj) throws Exception{
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		Log.Info("getPPVReportsByDynamicQuery() method called on ReportsService");
		ReportsDao reportsDao = new ReportsDao();
		List <LinkedHashMap <String,Object>> reportsData = reportsDao.getEMEAPPVProfileL2ReportByFilter(obj);
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return reportsData;
	}
	
	public List <LinkedHashMap <String,Object>> getPOReportsByDynamicQuery(Object... obj) throws Exception{
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		Log.Info("getPOReportsByDynamicQuery() method called on ReportsService");
		ReportsDao reportsDao = new ReportsDao();
		List <LinkedHashMap <String,Object>> reportsData = reportsDao.getPOReportsByDynamicQuery(obj);
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return reportsData;
	}
	
	public List <LinkedHashMap <String,Object>> getQuotesReportsByDynamicQuery(Object... obj) throws Exception{
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		Log.Info("getQuotesReportsByDynamicQuery() method called on ReportsService");
		ReportsDao reportsDao = new ReportsDao();
		List <LinkedHashMap <String,Object>> reportsData = reportsDao.getQuotesReportsByDynamicQuery(obj);
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return reportsData;
	}
	
	public List <LinkedHashMap <String,Object>> getEMEAPPAPOProfileL2ReportByFilter(Object... obj) throws Exception{
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		Log.Info("getEMEAPPAPOProfileL2ReportByFilter() method called on ReportsService");
		ReportsDao reportsDao = new ReportsDao();
		List <LinkedHashMap <String,Object>> reportsData = reportsDao.getEMEAPPAPOProfileL2ReportByFilter(obj);
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return reportsData;
	}
}
