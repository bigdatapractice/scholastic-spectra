package com.relevance.prism.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.URISyntaxException;
import java.util.Date;
import java.util.Properties;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.Authenticator;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import com.relevance.prism.dao.MailDao;
 
/**
 * @author Crunchify.com
 * 
 */
 
public class EmailClient {
 
	static Properties mailServerProperties;
    static Session getMailSession;
    static MimeMessage msg;
 
    /*public static void main(String args[]) throws AddressException, MessagingException, URISyntaxException, IOException {
 
        System.out.println("\n1st ===> setup Mail Server Properties..");
 
        final String sourceEmail = "nagaraj.ps@relevancelab.com"; // requires valid Gmail id
        final String password = "nagaraj@123"; // correct password for Gmail id
        final String toEmail = "nagaraj.ps@relevancelab.com"; // any destination email id
 
        Properties props = new Properties();
        props.put("mail.smtp.host", "smtp.gmail.com");
        //props.put("mail.smtp.port", "587");
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.starttls.enable", "true");
        //props.put("mail.transport.protocol", "smtp");
 
        System.out
                .println("\n2nd ===> create Authenticator object to pass in Session.getInstance argument..");
 
        Authenticator authentication = new Authenticator() {
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(sourceEmail, password);
            }
        };
        Session session = Session.getInstance(props, authentication);
        //generateAndSendEmail(
        //        session,
        //        toEmail,
        //       "Test Image Attachment",
        //        "Greetings, <br><br>Test email by Nagaraj. Please find here attached Image."
        //                + "<br><br> Regards, <br>Nagaraj");
 
    }*/
    
    public static boolean sendGenericMailX(String from, String to, String subject, String content) {
    		boolean response = true;
    		try {
    			  Log.Info("Sendding Mail,");
    			  System.out.println("Sendding Mail");
    			  String host = "127.0.0.1";
    			  
    			  Properties properties = System.getProperties();
    			  properties.setProperty("mail.smtp.host", host);
    			  Session session = Session.getDefaultInstance(properties);
    			// Create a default MimeMessage object.
    		         MimeMessage message = new MimeMessage(session);

    		         // Set From: header field of the header.
    		         message.setFrom(new InternetAddress(from));

    		         // Set To: header field of the header.
    		         message.addRecipients(Message.RecipientType.TO, InternetAddress.parse(to));
    		         
    		         

    		         // Set Subject: header field
    		         message.setSubject(subject);
    		         // Now set the actual message
    		         message.setText(content, "UTF-8", "html");

    		         // Send message
    		         Transport.send(message);
    		         Log.Info("Mail Sent");
       			 System.out.println("Mail Sent");

    		} catch(Exception ex) {
    			Log.Error("Exception Occurred While Sending the Email : - Message " + ex.getMessage() + " Stacktrace " + ex.getStackTrace());
    			response = false;
    		}
    		MailDao dao = new MailDao();
      	dao.insertMailLog(from, to, subject, content, (response) ? "Success" : "Failed");
    		return response;
    }
    
    public static String sendEmailInLinuxServers(final String toEmail, String subject, String body, String filePath) throws Exception{
    	//String [] emailIds = null;
    	String linuxCommand = null;
    	StringBuffer output = new StringBuffer();
    	Process p;
    	BufferedReader reader = null;
    	StringBuffer finalBody = new StringBuffer(body);
    	finalBody.append("\n").append("\n").append("\n");
    	finalBody.append(E2emfAppUtil.getAppProperty(E2emfConstants.CONFIDENTIALITY_NOTE));
    	if(toEmail.contains(",")){
			linuxCommand = "echo \""+ finalBody + "\" | mutt -a \""+ filePath +"\" -s \""+ subject.replace("\"", "\'") +"\" -- "+ toEmail;
			//emailIds = toEmail.split(",");			
			//
		}else if(toEmail.contains(";")){
			//emailIds = toEmail.split(";");
			linuxCommand = "echo \""+ finalBody + "\" | mutt -a \""+ filePath +"\" -s \""+ subject.replace("\"", "\'") +"\" -- "+ toEmail.replace(";", ",");
		}else{
			linuxCommand = "echo \""+ finalBody + "\" | mutt -a \""+ filePath +"\" -s \""+ subject.replace("\"", "\'") +"\" -- "+ toEmail;
		}
    	Log.Info("Linux Command to Send Email :" + linuxCommand);
    	System.out.println("Linux Command to Send Email :" + linuxCommand);
    	try {
    		String [] command = {"sh","-c",linuxCommand}; 
			p = Runtime.getRuntime().exec(command);
			p.waitFor();
			reader = new BufferedReader(new InputStreamReader(p.getInputStream()));
            String line = "";			
			while ((line = reader.readLine())!= null) {
				output.append(line + "\n");
			}

		} catch(NullPointerException ne){
			Log.Error("Exception Occurred While Sending the Email : " + ne);
		}catch (Exception e) {
			Log.Error("Exception Occurred While Sending the Email : " + e);
		}
    	finally{
    		try{
    			if(reader != null){
    				reader.close();
    			}
    		}catch(NullPointerException ne){
    			Log.Error("Exception Occurred While Sending the Email : " + ne);
    		}catch(IOException ioe){
    			Log.Error("Exception Occurred While Sending the Email : " + ioe);
    		}
    	}
    	Log.Info("Email Sent Successfully : " + output.toString());
		return "{\"message\":\"Email is Sent Successfully\"}";
    }
    
    public static String sendEmail(final String toEmail, String subject, String body, String filePath){
    	try{
	    	//System.out.println("\n1st ===> setup Mail Server Properties..");
	    	 
	        //final String sourceEmail = "nagaraj.ps@relevancelab.com"; // requires valid Gmail id
	        //final String password = "nagaraj@123"; // correct password for Gmail id
	        //final String toEmail = "nagaraj.ps@relevancelab.com"; // any destination email id
    		String [] emailIds = null;
    		if(toEmail.contains(",")){
    			emailIds = toEmail.split(",");
    		}else if(toEmail.contains(";")){
    			emailIds = toEmail.split(";");
    		}
    		
	        Properties props = new Properties();
	        final String sourceEmail = E2emfAppUtil.getAppProperty(E2emfConstants.SMARTFIND_EMAIL_ID);
	        final String password = E2emfAppUtil.getAppProperty(E2emfConstants.SMARTFIND_EMAIL_PASSWORD);
	        String smtpHost = E2emfAppUtil.getAppProperty(E2emfConstants.SMTP_HOST);
	        props.put("mail.smtp.host", smtpHost);
	        //props.put("mail.smtp.port", "587");
	        props.put("mail.smtp.auth", "true");
	        props.put("mail.smtp.starttls.enable", "true");
	        //props.put("mail.transport.protocol", "smtp");
	        
	        
	        //System.out.println("\n2nd ===> create Authenticator object to pass in Session.getInstance argument..");
	        
	        Authenticator authentication = new Authenticator() {
	            protected PasswordAuthentication getPasswordAuthentication() {
	                return new PasswordAuthentication(sourceEmail, password);
	            }
	        };
	        Session session = Session.getInstance(props, authentication);
	        if(emailIds.length > 1){	        	
	        	for(String toEmailId : emailIds){			        
			        generateAndSendEmail(
			                session,
			                sourceEmail,
			                toEmailId,
			                subject,
			                body,
			                filePath);
	        	}
	        }
        	else{
        		generateAndSendEmail(
		                session,
		                sourceEmail,
		                emailIds[0],
		                subject,
		                body,
		                filePath);
        	}
	        return "{\"message\":\"Email is Sent Successfully\"}";
    	}catch(Exception ex){
    		Log.Error("Exception Occurred while sending Email : " + ex);
    	}
    	 return "{\"message\":\"Email is Sent Successfully\"}";
    }
    
    public static void generateAndSendEmail(Session session, String sourceEmail, String toEmail, String subject,
            String body, String filePath) throws URISyntaxException, IOException {
        try {
            //System.out.println("\n3rd ===> generateAndSendEmail() starts..");
 
            MimeMessage crunchifyMessage = new MimeMessage(session);
            crunchifyMessage.addHeader("Content-type", "text/HTML; charset=UTF-8");
            crunchifyMessage.addHeader("format", "flowed");
            crunchifyMessage.addHeader("Content-Transfer-Encoding", "8bit");
 
            crunchifyMessage.setFrom(new InternetAddress(sourceEmail,
                    "NoReply"));
            crunchifyMessage.setReplyTo(InternetAddress.parse(sourceEmail, false));
            crunchifyMessage.setSubject(subject, "UTF-8");
            crunchifyMessage.setSentDate(new Date());
            crunchifyMessage.setRecipients(Message.RecipientType.TO,
                    InternetAddress.parse(toEmail, false));
 
            // Create the message body part
            BodyPart messageBodyPart = new MimeBodyPart();
            messageBodyPart.setContent(body, "text/html");
 
            // Create a multipart message for attachment
            Multipart multipart = new MimeMultipart();
 
            // Set text message part
            multipart.addBodyPart(messageBodyPart);
 
            messageBodyPart = new MimeBodyPart();
 
            // Valid file location
            //String filename = "D:/Watermark/311.43.pdf";
           // String fileName = readFileFromRemote();
            
            //URL url = new URL("http://itsusral00904/bigimg/00000255.png");
            //String tDir = System.getProperty("java.io.tmpdir"); 
            //String path = tDir + "00000255" + ".png"; 
            
            //URL url = new URL(filePath);
            //String tDir = System.getProperty("java.io.tmpdir");
            //String fileName = filePath.substring(filePath.lastIndexOf("/")+1);
            //String path = tDir + fileName;
            //File file = new File(path);
            //FileUtils.copyURLToFile(url, file);            
           // File file =  new File(url.toURI());
            //DataSource source = new FileDataSource(file);
            DataSource source = new FileDataSource(filePath);
            String fileName = filePath.substring(filePath.lastIndexOf(File.separator)+1);
            messageBodyPart.setDataHandler(new DataHandler(source));
            messageBodyPart.setFileName(fileName);
            // Trick is to add the content-id header here
            messageBodyPart.setHeader("Content-ID", "image_id");
            multipart.addBodyPart(messageBodyPart);
 
            //System.out.println("\n4th ===> third part for displaying image in the email body..");
            messageBodyPart = new MimeBodyPart();
            messageBodyPart.setContent("<br><h3>Find below attached image</h3>" + "<img src='cid:image_id'>", "text/html");
            multipart.addBodyPart(messageBodyPart);
            crunchifyMessage.setContent(multipart);
 
            //System.out.println("\n5th ===> Finally Send message..");
 
            // Finally Send message
            Transport.send(crunchifyMessage);
 
            //System.out.println("\n6th ===> Email Sent Successfully With Image Attachment. Check your email now..");
            //System.out.println("\n7th ===> generateAndSendEmail() ends..");
 
        } catch (MessagingException e) {
           Log.Error("Messaging Exception Occured " + e);
        } catch (UnsupportedEncodingException e) {
            Log.Error("Unsupported Encoding Exception Occured " + e);
        }
    }
    
    /*private static URI readFileFromRemote() throws IOException, URISyntaxException{
    	
    	URL url = new URL("http://itsusral00904/bigimg/302_994_001-A.png");
    	url.toURI();
    	InputStream is = url.openStream();
    	BufferedImage image = ImageIO.read(is);
    	
    	
		return null;
    	
    }*/
    public static String sendEmailToProd(final String toEmail, String subject, String body){
    	try{
    		String [] emailIds = null;
    		if(toEmail.contains(",")){
    			emailIds = toEmail.split(",");
    		}else if(toEmail.contains(";")){
    			emailIds = toEmail.split(";");
    		}
    		
	        Properties props = new Properties();
	        final String sourceEmail = E2emfAppUtil.getAppProperty(E2emfConstants.CASSA_SOURCE_EMAIL_ID);
	        final String password = E2emfAppUtil.getAppProperty(E2emfConstants.CASSA_EMAIL_PASSWORD);
	        String smtpHost = E2emfAppUtil.getAppProperty(E2emfConstants.CASSA_SMTP_HOST);
	        props.put("mail.smtp.host", smtpHost);
	        //props.put("mail.smtp.port", "587");
	        props.put("mail.smtp.auth", "true");
	        props.put("mail.smtp.starttls.enable", "true");
	        //props.put("mail.transport.protocol", "smtp");
	        
	        
	        Authenticator authentication = new Authenticator() {
	            protected PasswordAuthentication getPasswordAuthentication() {
	                return new PasswordAuthentication(sourceEmail, password);
	            }
	        };
	        Session session = Session.getInstance(props, authentication);
	        if(emailIds.length > 1){	        	
	        	for(String toEmailId : emailIds){			        
			        generateAndSendEmail1(
			                session,
			                sourceEmail,
			                toEmailId,
			                subject,
			                body);
			        System.out.println("hi");
	        	}
	        }
        	else{
        		generateAndSendEmail1(
		                session,
		                sourceEmail,
		                emailIds[0],
		                subject,
		                body);
        		 System.out.println("hii");
        	}
	        return "{\"message\":\"Email is Sent Successfully\"}";
    	}catch(Exception ex){
    		Log.Error("Exception Occurred while sending Email : " + ex);
    	}
    	 return "{\"message\":\"Email is Sent Successfully\"}";
    }
    
    public static void generateAndSendEmail1(Session session, String sourceEmail, String toEmail, String subject,
            String body) throws URISyntaxException, IOException {
        try {
 
            MimeMessage crunchifyMessage = new MimeMessage(session);
            crunchifyMessage.addHeader("Content-type", "text/HTML; charset=UTF-8");
            crunchifyMessage.addHeader("format", "flowed");
            crunchifyMessage.addHeader("Content-Transfer-Encoding", "8bit");
 
            crunchifyMessage.setFrom(new InternetAddress(sourceEmail,
                    "CASSA-ERROR"));
            crunchifyMessage.setReplyTo(InternetAddress.parse(sourceEmail, false));
            crunchifyMessage.setSubject(subject, "UTF-8");
            crunchifyMessage.setSentDate(new Date());
            crunchifyMessage.setRecipients(Message.RecipientType.TO,
                    InternetAddress.parse(toEmail, false));
 
            // Create the message body part
            BodyPart messageBodyPart = new MimeBodyPart();
            messageBodyPart.setContent(body, "text/html");
 
            // Create a multipart message for attachment
            Multipart multipart = new MimeMultipart();
 
            // Set text message part
            multipart.addBodyPart(messageBodyPart);
 
            messageBodyPart = new MimeBodyPart();
            messageBodyPart.setContent("<br>" + "<img src='cid:image_id'>", "text/html");
            multipart.addBodyPart(messageBodyPart);
            crunchifyMessage.setContent(multipart);
 
            // Finally Send message
            Transport.send(crunchifyMessage);
 
        } catch (MessagingException e) {
           Log.Error("Messaging Exception Occured " + e);
        } catch (UnsupportedEncodingException e) {
            Log.Error("Unsupported Encoding Exception Occured " + e);
        }
    }
}