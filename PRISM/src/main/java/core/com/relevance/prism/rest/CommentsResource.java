package com.relevance.prism.rest;

import java.util.HashMap;
import java.util.List;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import com.google.gson.Gson;
import com.relevance.prism.dao.CommentsDao;
import com.relevance.prism.data.Comment;
import com.relevance.prism.service.CommentsService;
import com.relevance.prism.util.PrismHandler;
@Path("/comments")
public class CommentsResource extends BaseResource{
	
	@Context
	ServletContext context;

	public CommentsResource(@Context ServletContext value) {
		this.context = value;
	}
	
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Path("/addComment")
	public String addComment(@FormParam("profileName") String profileName,
			@FormParam("commentSectionName") String commentPanelId,
			@FormParam("commentContent") String commentContent,
			@FormParam("filterQuery") String filterQuery,
			@Context HttpServletRequest req) throws Exception{
			PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
			String response = null;
			Comment comment = null;
			try {
				HttpSession session  = req.getSession(true);
				String username = session.getAttribute("userName").toString();
				comment = new Comment();
				comment.setProfileName(profileName);
				comment.setCommentPanelId(commentPanelId);
				comment.setUserName(username);
				comment.setComment(commentContent);
				comment.setUserImage("default");
				comment.setFilterQuery(filterQuery);
				CommentsService commentsService = new CommentsService();
				response = commentsService.insertComment(comment);
			} catch(Exception ex) {
				response = PrismHandler.handleException(ex);
			}	
		return response;
	}
	
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Path("/getComments")
	public String getComments(@FormParam("profileName") String profile_name,@FormParam("commentSectionName") String commentPanelId,@FormParam("filterQuery") String filterQuery,@FormParam("setLastRead")  boolean setLastRead,@Context HttpServletRequest req) throws Exception{
		String response = null;
		Comment comment = null;
		try 
		{
			HttpSession session  = req.getSession(true);
			String username = session.getAttribute("userName").toString();
			comment = new Comment();
			comment.setCommentPanelId(commentPanelId);			
			comment.setUserName(username);	
			comment.setProfileName(profile_name);
			comment.setFilterQuery(filterQuery);			
			CommentsService commentsService = new CommentsService();
			HashMap<String,String> hp = commentsService.getComments(comment,setLastRead);
			Gson gson = new Gson();
			response = gson.toJson(hp);					
		} catch(Exception ex) {
			response = PrismHandler.handleException(ex);
		}
		return response;
	}
}
