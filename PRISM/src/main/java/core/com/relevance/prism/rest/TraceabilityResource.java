package com.relevance.prism.rest;

import java.util.Map;

import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.google.gson.Gson;
import com.relevance.prism.data.Traceability;
import com.relevance.prism.data.TraceabilityView;
import com.relevance.prism.service.TraceabilityService;
import com.relevance.prism.util.PrismHandler;

@Path("/traceability")
public class TraceabilityResource extends BaseResource {
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Path("/getTraceability")
	public String getTraceability(@FormParam("source") String source,
			@FormParam("database") String database,
			@FormParam("table") String table, @FormParam("traceabilityConfig") String traceabilityConfig) {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		TraceabilityView traceabilityView = null;
		Traceability traceability;
		String response = null;
		Gson gson;
		try {
			gson = new Gson();
			traceability = gson.fromJson(traceabilityConfig, Traceability.class);
			TraceabilityService traceabilityService = new TraceabilityService();
			traceabilityView = traceabilityService.getTraceability(source,database,table,traceability);
			response = gson.toJson(traceabilityView);
		} catch (Exception ex) {
			response = PrismHandler.handleException(ex);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return response;
	}
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Path("/getBOM")
	public String getTraceability(@FormParam("key") String key,
			@FormParam("direction") String direction) {
		PrismHandler.logMethodEntry(getClassName(), getCurrentMethodName());
		Map<String,String> traceabilityView = null;
		Traceability traceability;
		String response = null;
		Gson gson;
		try {
			gson = new Gson();
			TraceabilityService traceabilityService = new TraceabilityService();
			traceabilityView = traceabilityService.getBOM(key,direction);
			response = gson.toJson(traceabilityView);
		} catch (Exception ex) {
			response = PrismHandler.handleException(ex);
		}
		PrismHandler.logMethodExit(getClassName(), getCurrentMethodName());
		return response;
	}
}
