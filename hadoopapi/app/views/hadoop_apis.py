from flask import request, make_response, jsonify, abort
from app import app
from app.modles.hadoop_fs import Hadoop
from app.exception.custom_exception import SolrDataImportError
import http
# import sqlite3
from flask import g
# from datetime import datetime


# DATABASE = '/home/python/sqllite_data'
# def get_db():
#     db = getattr(g, '_database', None)
#     if db is None:
#         db = g._database = sqlite3.connect(DATABASE)
#     return db
#
# @app.teardown_appcontext
# def close_connection(exception):
#     db = getattr(g, '_database', None)
#     if db is not None:
#         db.close()

def get_con_dict():
    dict = getattr(g,'_dictionary',None)
    if dict is None:
        dict =g._dictionary =  {}
    return dict

cache = {}

@app.route('/hadoop/hdfs', methods=['POST'])
def hdfs_get(**kwargs):
    json_obj = request.get_json()
    print(json_obj)
    try:
        hadoop = Hadoop()
        response = make_response(jsonify(hadoop.list_files(json_obj)))
    except Exception as e:
        response = make_response(jsonify({"ERROR": str(e)}), http.HTTPStatus.INTERNAL_SERVER_ERROR)
    return response


@app.route('/hadoop/hdfs', methods=['DELETE'])
def hdfs_delete(**kwargs):
    json_obj = request.get_json()
    try:
        hadoop = Hadoop()
        rep = hadoop.delete_files(json_obj)
        response = make_response(jsonify(rep))
    except Exception as e:
        response = make_response(jsonify({"ERROR File Cannot be deleted": str(e)}), http.HTTPStatus.INTERNAL_SERVER_ERROR)
    return response

@app.route('/hadoop/hdfs/<dir_name>', methods=['PUT'])
def hdfs_put(dir_name,**kwargs):
    try:
        hadoop = Hadoop()
        rep = hadoop.put_files(dir_name)
        response = make_response(jsonify(rep))
    except Exception as e:
        response = make_response(jsonify({"ERROR File Cannot be Uploaded": str(e)}), http.HTTPStatus.INTERNAL_SERVER_ERROR)
    return response


@app.route('/local/<dir_name>', methods=['GET'])
def local_get(dir_name,**kwargs):
    try:
        hadoop = Hadoop()
        rep = hadoop.get_local_files(dir_name)
        response = make_response(jsonify(rep))
    except Exception as e:
        response = make_response(jsonify({"ERROR While getting Local Dir": str(e)}), http.HTTPStatus.INTERNAL_SERVER_ERROR)
    return response

@app.route('/impala/refresh', methods=['PUT'])
def impala_query(**kwargs):
    json_obj = request.get_json()
    try:
        hadoop = Hadoop()
        rep = hadoop.impala_refresh(json_obj)
        response = make_response(jsonify(rep))
    except Exception as e:
        response = make_response(jsonify({"ERROR While Impala Refresh": str(e)}), http.HTTPStatus.INTERNAL_SERVER_ERROR)
    return response


@app.route('/solr/dataimport/status', methods=['GET'])
def solr_data_import_status(**kwargs):
    try:
        hadoop = Hadoop()
        rep = hadoop.solr_import_status()
        response = make_response(jsonify(rep))
    except Exception as e:
        response = make_response(jsonify({"ERROR While getting Solr Import Status": str(e)}),
                                 http.HTTPStatus.INTERNAL_SERVER_ERROR)
    return response


@app.route('/solr/dataimport/entity', methods=['POST'])
def solr_data_import_src(**kwargs):
    try:
        hadoop = Hadoop()
        rep = hadoop.solr_import_src()
        response = make_response(jsonify(rep),http.HTTPStatus.ACCEPTED)
    except SolrDataImportError as e:
        response = make_response(jsonify({"ERROR While Calling Data Import Handler": str(e)}),
                                 http.HTTPStatus.CONFLICT)
    except Exception as e:
        response = make_response(jsonify({"ERROR While Calling Data Import Handler": str(e)}),
                                 http.HTTPStatus.INTERNAL_SERVER_ERROR)
    return response


@app.route('/solr/dataimport/abort', methods=['POST'])
def solr_dataimport_abort(**kwargs):
    try:
        hadoop = Hadoop()
        rep = hadoop.solr_import_abort()
        response = make_response(jsonify(rep))
    except Exception as e:
        response = make_response(jsonify({"ERROR While Aborting Import Process": str(e)}),
                                 http.HTTPStatus.INTERNAL_SERVER_ERROR)
    return response

@app.route('/solr/collection', methods=['DELETE'])
def solr_delete_collection_data(**kwargs):
    try:
        hadoop = Hadoop()
        rep = hadoop.solr_delete_data()
        response = make_response(jsonify(rep))
    except Exception as e:
        response = make_response(jsonify({"ERROR While Deleting Solr Data": str(e)}),
                                 http.HTTPStatus.INTERNAL_SERVER_ERROR)
    return response


# @app.route('/sample', methods=['GET'])
# def sample(**kwargs):
#     with app.app_context():
#         db = get_db()
#         c = db.cursor()
#         dt = datetime.today().strftime('%Y%m%d%H%M%S')
#
#         #c.execute('''CREATE TABLE stocks
#         #             (date text, trans text, symbol text, qty real, price real)''')
#         c.execute("INSERT INTO stocks VALUES ('?,'BUY','XXX',100,35.14)",dt)
#         db.commit()
#         t = ('XXX',)
#         c.execute('SELECT * FROM stocks WHERE symbol=?', t)
#         print(c.fetchone())
#         return "RR"


@app.route('/sample', methods=['GET'])
def sample(**kwargs):
    try:
        hadoop = Hadoop()
        rep = hadoop.update_cache(cache)
        response = make_response(jsonify(rep))
    except Exception as e:
        response = make_response(jsonify({"ERROR Cannot read": str(e)}),
                                 http.HTTPStatus.INTERNAL_SERVER_ERROR)
    return response


























