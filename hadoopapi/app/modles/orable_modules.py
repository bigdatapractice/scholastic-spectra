import os
import subprocess
from app.exception.custom_exception import UploadError
import datetime
import set_env
import configparser
from random import randint
from datetime import datetime
import json
import requests
import time
import base64

def run_cmd(args_list):
    """
    run linux commands
    """
    # import subprocess
    print('Running system command: {0}'.format(' '.join(args_list)))
    proc = subprocess.Popen(args_list, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    s_output, s_err = proc.communicate()
    s_return = proc.returncode
    return s_return, s_output, s_err

def get_log_details(input_file):
    status = "Running"
    f = open(input_file, "r")
    content = f.read()
    f.close()
    if "Exception" in content:
        status = "Failed"
    elif "Program End Date" in content:
        status = "Completed"
    return  status,content


def get_sec(time_str):
    """Get Seconds from time."""
    h, m, s = time_str.split(':')
    return int(h) * 3600 + int(m) * 60 + int(s)


def format_oracle_files(o_json,f_json,t_start_time,mbps,num_of_tasks):
    job_remaining_time = 0;
    rlist = []
    d = {}
    t_json = {'resp':o_json}
    for i in t_json['resp']:
        fname = i['fileName']
        d[fname]=get_actual_size(i['fileSize'])

    for i in f_json:
        for k in i.keys():
            mb_size = 1024
            if k in d:
                mb_size = d[k]
            task_start_time = t_start_time
            total_time_requied_sec = ((mb_size * 60)/ mbps)*num_of_tasks
            total_time_requied = time.strftime('%H:%M:%S', time.gmtime(round(total_time_requied_sec)))
            j = {k:{'fileName':k,'request_id':i[k],'mb_size':mb_size,'task_start_time':task_start_time,
                    'task_status':'Running','_time_required':total_time_requied,'_time_required_sec':total_time_requied_sec,
                    'time_remaining':total_time_requied,'percent_complete':0,'log':''}}
            rlist.append(j)
            job_remaining_time = job_remaining_time +total_time_requied_sec
    avg_time_required = round(job_remaining_time/num_of_tasks)
    job_time_required_return = time.strftime('%H:%M:%S', time.gmtime(avg_time_required))
    return job_time_required_return,{'tasks':rlist}


def get_actual_size(size):
    mb_size = size/(1024*1024)
    return mb_size * 50


def date_format(i_date):
    return datetime.strptime(i_date, "%Y-%m-%d %H:%M:%S")


def update_json(in_json, mbps):
    active_taks = in_json['running_tasks']
    failed_tasks = in_json['failed_tasks']
    completed_tasks = in_json['completed_tasks']
    num_taks = in_json['num_tasks']

    o_list=[]
    job_percent_complete = 0
    job_remaining_time = 0
    for i in in_json['tasks']:
        for k in i.keys():
            t_dict = i[k]
            remaining_time, percent = calc_percent(t_dict['mb_size'],mbps,t_dict['task_start_time'],
                                                   t_dict['_time_required_sec'],active_taks)
            req_id = t_dict['request_id']
            log_file = f'/tmp/{req_id}.log'
            status,content = get_log_details(log_file)
            if status == 'Failed':
                percent = 0
                remaining_time = '01:00:00'
                if t_dict['task_status'] == 'Running':
                    active_taks = active_taks - 1
                    failed_tasks = failed_tasks + 1
            elif status == 'Completed':
                percent = 100
                remaining_time = '00:00:00'
                if t_dict['task_status'] == 'Running':
                    active_taks = active_taks -1
                    completed_tasks = completed_tasks +1


            a = {k: {'fileName': k, 'request_id': t_dict['request_id'], 'mb_size': t_dict['mb_size'],
                     'task_start_time': t_dict['task_start_time'], '_time_required': t_dict['_time_required'],
                     '_time_required_sec': t_dict['_time_required_sec'], 'percent_complete': round(percent),
                     'task_status': status, 'log': content,'time_remaining':remaining_time}}

            job_percent_complete = job_percent_complete + percent
            job_remaining_time = job_remaining_time + get_sec(remaining_time)
            o_list.append(a)
    if active_taks <= 0:
        return_job_remaining_time = '00:00:00'
        return_job_percent = 100
        active_taks ==0
    else:
        return_job_percent = round(job_percent_complete / num_taks)
        return_job_remaining_time = time.strftime('%H:%M:%S', time.gmtime(job_remaining_time/active_taks))

    a = {'failed_tasks': failed_tasks,
         'job_id': in_json['job_id'],
         'job_instance': in_json['job_instance'],
         'job_start_time': in_json['job_start_time'],
         'num_tasks': in_json['num_tasks'],
         'job_percent_complete': abs(return_job_percent),
         'running_tasks': active_taks,
         'job_remaining_time': return_job_remaining_time,
         'completed_tasks':completed_tasks,
         'tasks': o_list}

    return a


def calc_percent(mb_size,mbps,task_start_time,total_time_requried_sec,active_tasks):
    TRESH_HOLD_PERCENT = 90
    tresh_hold_time = round((total_time_requried_sec *5)/100)

    tst = date_format(task_start_time) #task start time in Y-m-d HH:MM:SS
    date_now = datetime.today()
    time_elapsed_seconds = (date_now - tst).total_seconds()
    remaining_time =  total_time_requried_sec - time_elapsed_seconds

    if remaining_time <= tresh_hold_time:
        remaining_time = tresh_hold_time
    return_remaining_time = time.strftime('%H:%M:%S', time.gmtime(round(remaining_time)))
    percent = (mbps * time_elapsed_seconds * 100) / (mb_size * 60)

    if percent >= TRESH_HOLD_PERCENT:
        percent = TRESH_HOLD_PERCENT
    return return_remaining_time,abs(percent)

class Oracle:

    def __init__(self):

        abs_path = os.path.abspath(os.path.dirname(__file__))
        config_path = os.path.join(abs_path, '..//..//config//config.ini')
        config = configparser.ConfigParser()
        config.read(config_path)
        self.configs = config
        set_env.set_env()

    def load_oracle_data(self,f_config):
        o_config_path = self.configs['oracle']['o_cofig_path']
        #seed(40)
        value = randint(0, 10000)
        dt = datetime.today().strftime('%Y%m%d%H%M%S')
        json_config_path = f'/tmp/{value}{dt}_j.json'
        log_file = f'/tmp/{value}{dt}_o.log'
        text_file = open(json_config_path, "w")
        n = text_file.write(json.dumps(f_config))
        text_file.close()
        #(ret, out, err) = run_cmd(['touch', log_file, '&'])
        (ret, out, err) = run_cmd(['sh', '/home/python/oracle_di/run_oracle.sh', o_config_path, json_config_path, log_file, '&'])
        #ret = 0
        target_file = f_config[0]['inputfile']
        if ret != 0:
            raise UploadError(f'Stdout: {out} Error: {err}')
        return {target_file:log_file.replace("/tmp/","").replace('.log','')}

    def read_log(self,cache,job_id,instance_id):
        key = f'{job_id}-{instance_id}'
        job_json = cache[key]

        job_complete_percent = job_json['job_percent_complete']
        o_json = ''
        if job_complete_percent != 100:
            mbps = int(self.configs['oracle']['mbps'])
            o_json = update_json(job_json, mbps)
            cache[key] = o_json
        else:
            o_json = job_json
        return  o_json

    def run_job(self,input_json,cache):
        mbps =int(self.configs['oracle']['mbps'])
        dict_json = input_json
        job_instance = dict_json['instanceId']
        job_id = dict_json['jobId']
        target_objs = dict_json['targetObjects']
        start_time = datetime.now().strftime(("%Y-%m-%d %H:%M:%S"))
        j_key = f'{job_id}-{job_instance}'


        ret_list = []
        num_tasks = 0
        for i in target_objs:
            response_id = self.load_oracle_data([i])
            ret_list.append(response_id)
            num_tasks = num_tasks + 1
        job_remaining_time,f_json = format_oracle_files(self.get_oracle_files('BI_CON'),ret_list,start_time,mbps,num_tasks)
        js = {job_id: {'job_id': job_id, 'job_instance': job_instance, 'job_start_time': start_time,
                       'num_tasks': num_tasks, 'running_tasks': num_tasks, 'failed_tasks': 0,
                       'job_remaining_time': job_remaining_time, 'job_percent_complete': 0,'completed_tasks':0}}
        return_json = {**js[job_id],**f_json}
        cache[j_key]=return_json
        return return_json


    def get_oracle_files(self,container_name):
        api_url = self.configs['oracle']['get_api']
        api_url = f'{api_url}/{container_name}'
        response = requests.get(api_url)
        return response.json()

    def job_status(self,cache):
        return ""


    def download_file(self,file_name):
        target_path = 'alaska.zip'
        username = 'cloud_bi_user'
        password = 'TestCloud2020'
        userpass = "{0}:{1}".format(username, password)
        url = 'https://uscom-east-1.storage.oraclecloud.com/v1/Storage-schlprodpaas/BI_CON/file_fscmtopmodelam_costtransactionam_costtransactionpvo-batch2044181594-20200515_155342.zip'
        value = base64.b64encode(bytes(userpass, "utf-8")).decode().replace('\n', '')
        headers = {'Authorization': 'Basic {}'.format(value), "content-type": "application/zip"}
        r = requests.get(url=url, headers=headers, verify=True, stream=True)
        handle = open(target_path, "wb")
        for chunk in r.iter_content(chunk_size=512):
            if chunk:  # filter out keep-alive new chunks
                handle.write(chunk)
        handle.close()
        return  {'success':'suceess'}

    def get_containers(self):
        api_url = self.configs['oracle']['oracle_url']
        username = 'cloud_bi_user'
        password = 'TestCloud2020'
        userpass = "{0}:{1}".format(username, password)
        value = base64.b64encode(bytes(userpass, "utf-8")).decode().replace('\n', '')
        headers = {'Authorization': 'Basic {}'.format(value), "content-type": "text/plain"}
        r = requests.get(url=api_url, headers=headers, verify=True, stream=True)
        ss = str(r.content).split('\\n')
        l = []
        for s in ss:
            l.append(s)
        return {'Containers':l}

