(function(){
	var THIS = {};
	THIS.queryEditor = {};
	$("#addEditor").unbind('click').bind('click',function(){
		addEditor($("#divScriptEditor").find("table").length);
	});
	$("[name='widgetTemplate']").unbind("click").bind("click", function() {
		widgetonClick(this);
	});
	$("#saveProfile").unbind('click').bind('click',function(){
		saveProfile();
	});
	$("#addTable").unbind('click').bind('click',function(){
		addTable();
	});
	$("#addProfiles").unbind('click').bind('click',function(){
		addProfiles();
	});
	$("#profileBack").unbind("click").bind("click",function() {
		$("#widgets").show(300);
		$("#myprofiles").hide(300);
		$("#templateWidgets").hide(300);
	});
	$("#frmTemplateConf").validate({
		ignore: [],
		rules : {
			profilename : {
				required : true
			},
			source : {
				required : true
			},
			table : {
				required : true
			}
		},

		// Messages for form validation
		messages : {
			profilename : {
				required : 'Please enter profile name'
			},
			source : {
				required : 'Please select source'
			},
			table : {
				required : 'Please enter table'
			}
		},

		// Do not change code below
		errorPlacement : function(error, element) {
			error.insertAfter(element);
		}
	});	
	var getSources=function(){
		var source = lookUpTable.get("source"),sourceArray = [];
		if(source){
			source = source.split(",");
			for(var i=0;i<source.length;i++){
				sourceArray.push({id:source[i],text:source[i]});
			}
		}
		return sourceArray;
	};
	$("#source").select2({
		placeholder : "Select Source",
		data :  getSources()
	});
	var widgetonClick = function(obj) {
		var widget = $(obj).data("source");
		if (widget === "create") {
			$("#templateWidgets").show(300);
			$("#widgets").hide();
			THIS.queryEditor = {};
			$("#divScriptEditor").find("table").remove();
			$( '#frmTemplateConf' )[0].reset();
			THIS.id = 0;
			$("#configTable").find("tbody").html("");
			addEditor(0);
			addTable();
		} else if (widget === "saved") {
			request = {
				"type" : "template"
			};
			enableLoading();
			callAjaxService("getMyProfiles", callBackGetMyProfiles,callBackFailure, request, "POST");
		}
	};
	var callBackGetMyProfiles = function(response) {
		disableLoading();
		$("#myprofiles").show(300);
		$("#widgets").hide(300);
		if (response && response.isException) {
			showNotification("error", response.customMessage);
			return;
		}
		if (response !== undefined && response !== null) {
			// response = JSON.parse(response);
			var data = response;
			// data = JSON.parse(data);
			$("#myProfileList").empty();
			for ( var i = 0; i < data.length; i++) {
				$("#myProfileList").append(
						$("<li/>").addClass("list-group-item").data({
							"refid" : data[i].id
						}).append(
								$("<a/>").append($("<span/>").addClass("text-left col-md-3").html(data[i].displayname))
										 .append($("<span/>").addClass("text-left col-md-3").html(data[i].source))
										 .append($("<span/>").addClass("text-left col-md-3").html(data[i].lastmodifiedby))
										 .append($("<span/>").addClass("badge text-center col-md-3").html(getManipulatedDate("day",0,data[i].lastmodifiedon,"%d-%m-%Y %H:%M:%S")))
										 ));
			}
			$("#myProfileList li").unbind("click").bind("click", function() {
				myProfileTemplateSelect(this);
			});
		}
		$("[data-target^=#profilePublishModal]").unbind("click").bind("click",
				function(e) {
					e.stopPropagation();
					previewPublishProfile(this);
				});
	};
	var myProfileTemplateSelect = function(obj) {
		var id = $(obj).data("refid");
		var request = {
			"id" : id
		};
		enableLoading();
		callAjaxService("getProfileConfig", callBackGetProfileConfig,
				callBackFailure, request, "POST");
	};
	var callBackGetProfileConfig = function(response) {
		$("#templateWidgets").show(300);
		$("#widgets").hide(300);
		$("#myprofiles").hide(300);
		THIS.queryEditor = {};
		$("#divScriptEditor").find("table").remove();
		$( '#frmTemplateConf' )[0].reset();
		THIS.id = 0;
		var data = JSON.parse(response["config"])[0];
		if (data) {
			THIS.id = data.id;
			$("#frmTemplateConf").find("[name='profilename']").val(data.displayname);
			if(data.source){
				$("#frmTemplateConf").find("[name='source']").select2("val",data.source);
			}
			$("#frmTemplateConf").find("[name='table']").val(data.configtable);
			if (data.config) {
				var config = JSON.parse(data.config);
				var index = 0;
				if(config && Object.keys(config).length>0){
					for (var key in config) {
						addEditor(index++,config[key]);
					}
				}
			}
			if(data.additional_config){
				$("#configTable").find("tbody").html("");
				var additionalconfig = JSON.parse(data.additional_config);
				if(additionalconfig && additionalconfig.tables){
					for (var i=0;i<additionalconfig.tables.length;i++) {
						addTable(additionalconfig.tables[i]);
					}
				}
			}
		}
	};
	function createQueryEditor(ele,data){
		var queryEditor = CodeMirror.fromTextArea(ele, {
			lineNumbers : true,
			mode : "text/x-sql",
			autofocus : true,
			matchBrackets : true,
			styleActiveLine : true,
			showCursorWhenSelecting : true
		});
		if(data){
			queryEditor.setValue(data.replace(/@/g, "'"));
		}
		setTimeout(function() {
			queryEditor.refresh();
		}, 100);
		THIS.queryEditor[$(ele).attr("levelname")] = queryEditor;
	};
	function addEditor(index,data){
		var textarea = $("<textarea/>").attr({"name":"levelScript","levelname":"L"+(index+1)});
		var removeButton = $("<a/>").attr({"name":"removeEditor"}).addClass("button-icon pull-right cursor-pointer").append($("<i/>").addClass("fa fa-fw fa-lg fa-remove"));
		removeButton.unbind('click').bind('click',function(){
			var levelName = $(this).parents("table").find("[name='levelScript']").attr("levelname");
			THIS.queryEditor[levelName].toTextArea();
			THIS.queryEditor[levelName] = null;
			$(this).parents("table").remove();
		});
		$("#divScriptEditor").append($("<table/>").addClass("table table-bordered margin-bottom-10 padding-bottom-10").append($("<thead/>").append($("<tr/>")
				.append($("<th>").append($("<span/>").html("L"+(index+1)))
						.append(removeButton))))
						.append($("<tbody/>").append($("<tr/>").append($("<td/>").append($("<div/>").addClass("well").append(textarea))))));
		createQueryEditor(textarea[0],data);
	};
	var addTable = function(data){
		var removeButton = $("<a/>").addClass("button-icon pull-right cursor-pointer")
		 .append($("<i/>").addClass("fa fa-fw fa-lg fa-remove"));
		removeButton.unbind('click').bind('click',function(){
			$(this).parents("tr").remove();
		});
		var tableRow = $("<tr/>").append($("<td/>").append($("<label/>").addClass("input width-100per")
				.append($("<div/>").addClass("col-md-12").append($("<input/>").val((data && data.key)?data.key:'').attr({"name":"tablekey","placeholder":"Key"})))))
				.append($("<td/>").append($("<label/>").addClass("input width-100per")
				.append($("<div/>").addClass("col-md-11").append($("<input/>").val((data && data.value)?data.value:'').attr({"name":"tablevalue","placeholder":"Value"}))))
				.append(removeButton));
		$("#configTable").find("tbody").append(tableRow);
	};
	var getQuery = function(){
		var config = {};
		if(THIS.queryEditor && Object.keys(THIS.queryEditor).length>0){
			for (var key in THIS.queryEditor) {
				config[key] = THIS.queryEditor[key].getValue();
			}
		}
		return config;
	};
	var getTables = function(){
		var tables = [];
		$("#configTable").find("tbody").find("tr").each(function(){
			tables.push({
				key:$(this).find("[name='tablekey']").val(),
				value:$(this).find("[name='tablevalue']").val()
			});
		});
		return tables;
	};
	var saveProfile = function() {
		if(!$("#frmTemplateConf").valid())
			return;
		var config = getQuery();
		var tables = getTables();
		var basicInfo = $("#frmTemplateConf").serializeArray();
		var additionalconfig = {};
		additionalconfig.tables = tables;
		var request = {
			"source" : $("#source").select2("val"),
			"additionalconfig" : JSON.stringify(additionalconfig),
			"config" : JSON.stringify(config),
			"displayname" : basicInfo[0].value,
			"type" : "template",
			//"app":appName,
			"id" : THIS.id || 0
		};
		enableLoading();
		callAjaxService("saveProfile", callBackSuccessHandler,
				callBackFailure, request, "POST");
	};
	var callBackSuccessHandler = function(response){
		if ((response && response.isException)|| (response.message && response.message ==="failure")) {
			showNotification("error", response.customMessage||response.message);
			return;
		}
		if (response !== undefined && response !== null) {
			disableLoading();
			showNotification("success", "Profile saved successfully.");
			THIS.id = response;
		}
	};
})();