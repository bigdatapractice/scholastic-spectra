pageSetUp();
var sankeyGenerator = function(){
	console.log("Sankey Generator");
	var THIS = this;
	var sankeyZoomForward =  null;
	var sankeyZoomBackward =  null;
	$(".jarviswidget-toggle-btn").bind('click',function(){
		$(this).find("i").toggleClass("fa-minus fa-plus");
		$(this).parents("header").next().toggle();
	});
	$("#chat-container").find(".chat-list-open-close").unbind("click").bind("click",function(){
		$(this).parent().toggleClass("open");
	});
	
	$("#createNode").unbind("click").bind("click",function(){addNodeConfig();});
	$("#addFilters").unbind("click").bind("click",function(){addFilterConfig($('#filterConfigSection').find('section').length);});
	var validate = function(){
		$("#frmConf").validate({
			rules : {
				source : {
					required : true
				},
				database : {
					required : true
				}
			},
			// Messages for form validation
			messages : {
				source : {
					required : 'Select source'
				},
				database : {
					required : 'Select database'
				}
			},
			// Do not change code below
			errorPlacement : function(error, element) {
				error.insertAfter(element);
			}
		});
		var $registerForm = $("#frmSankeyFilter").validate({
			rules : {
				profilename : {
					required : true
				},
				description : {
					required: true
				}
			},
			messages : {
				profilename : {
					required : 'Please enter sankey name'
				},
				description : {
					required : 'Please describe profile'
				}
			},
			errorPlacement : function(error, element) {
				error.insertAfter(element.parent());
			}
		});
	};	
	function padZero(str, len) {
	    len = len || 2;
	    var zeros = new Array(len).join('0');
	    return (zeros + str).slice(-len);
	}
	var init = function(){
		validate();
		$("#frmSankeyFilter").submit(function(e) {
		    e.preventDefault();
		});
		$("#sankeyOnOffswitch").click(function(e){
			e.stopPropagation();
		});
		$("#myProfiles").hide();
		$("#selectTable,#mySankey").show();
		$("#preview").unbind("click").bind("click",function(){preview();});
		$("#save").unbind("click").bind("click",function(){invokeSaveSankeyProfile();});
		$("#saveAs").unbind("click").bind("click",function(){invokeSaveSankeyProfile(true);});
///		$("#myModalthredlevel  #btnCreate").unbind("click").bind("click",function(){preview(true,true);});
		$("#backToDesign").unbind("click").bind("click",function(){backToDesign();});
		$("#profileBack").unbind("click").bind("click",function(){$("#mySankey").hide();$("#myProfiles").show();});
		$("#mySavedSankey").unbind("click").bind("click",function(){getMySankey();});
		var sourceData =[{
			"id":"hive","text":"HIVE"},{
			"id":"postgreSQL","text":"PostgreSQL"}];
		var stackData =[{
			"id":"2","text":"2"},{
			"id":"3","text":"3"},{
			"id":"4","text":"4"},{
			"id":"5","text":"5"},{
			"id":"6","text":"6"},{
			"id":"7","text":"7"},{
			"id":"8","text":"8"},{
			"id":"9","text":"9"}];
		$("#source").select2({
			placeholder : "Choose Source",
			data :  sourceData
		}).on("change", function(e) {
			sourceSelect(e.val);
		});
		$("#database").select2({
			placeholder : "Choose Database",
			data : []
		}).on("change", function(e) {
			getTableList(e.val);
		});
		$("#createSankey").unbind("click").bind("click",function(){stackSelect();});
		generateConfig();
	};
	var sourceSelect = function(source,selectedValue){
		if (source  === "hive" || source  === "postgreSQL"){
			var request ={
				"source":source
			};
			enableLoading();
			callAjaxService("getDatabaseList",  function(response){callBackGetDatabaseList(response,selectedValue);},
					callBackFailure, request, "POST");
		}
	};
	var backToDesign = function(){
		$("#mySankey,#myProfiles").hide(300);
		$("#selectTable").show(300);
	};
	var callBackGetDatabaseList = function(response,selectedValue){
		disableLoading();
		if (response && response.isException){
			showNotification("error",response.customMessage);
			return;
		}
		if(response !== undefined && response !== null ){
			var databaseList = [];
			for (var i=0;i<response.length;i++){
				databaseList.push({"id":response[i],"text":response[i]});
			}
			$("#database").select2({
				placeholder : "Choose Database",
				allowClear: true,
				data :  databaseList
			});
			if (selectedValue != undefined && selectedValue != null && selectedValue != ""){
				$("#database").select2("val",selectedValue);
				getTableList();
			}
				
		}
	};
	var generateConfig = function(){
		var tempOption = $("<div/>");
		tempOption.append($("<option/>").attr("value","").html("Choose"));
		$("#actionBtn").hide();
		$("#mySankey").hide();
		addNodeConfig();
		addConfig(0);
		addFilterConfig(0);
	};
	var getTableList = function(database){
		var request ={
			"source":$("#source").select2('val'),
			"database":database?database:$("#database").select2('val')
		};
		enableLoading();
		callAjaxService("getTableList", callBackGetTableList,callBackFailure, request, "POST",null,true);
	};
	var callBackGetTableList = function(response){
		disableLoading();
		if (response && response.isException){
			showNotification("error",response.customMessage);
			return;
		}
		if(response !== undefined && response !== null){
			var tableList = [];
			tableList.push({"id":"","text":"Select table"});
			for (var i=0;i<response.length;i++){
				tableList.push({"id":response[i],"text":response[i]});
			}
			THIS.tableList = tableList;
			$('#nodeConfigSection,#configSection').find('input[type=select2]').each(function(){
				if($(this).val() && $(this).parents('section').find('table').length == 0){
					getColumnList(this);
				}
				$(this).select2({
					placeholder : "Select table",
					allowClear: true,
					data :  tableList
				}).on('change',function(){
					getColumnList(this);
				});
			});
		}
	};
	var getColumnList = function(obj){
		if(THIS.headers && THIS.headers[$(obj).val()]){
			constructHeader(obj);
		}
		else{
			if ($(obj).val()!= ""){
				var request ={
						"source":$("#source").select2('val'),
						"database":$("#database").select2('val'),
						"table":$(obj).val()
				};
				enableLoading();
				callAjaxService("getColumnList",function(response){callBackGetdatatableSelect(response,obj);},callBackFailure, request, "POST");
			} else{
				$(obj).parents('section').find('div[name=coloumns]').empty();
			}
		
		}
	};	
	var callBackGetdatatableSelect = function(response,obj){
		disableLoading();
		if (response && response.isException){
			showNotification("error",response.customMessage);
			return;
		}
		if(!THIS.headers){
			THIS.headers = {};
		}
		THIS.headers[$(obj).val()] = response;
		constructHeader(obj);
		
	};
	var constructHeader = function(obj,data){
		var headersData = [];
		if(data && data.length>0){
			headersData = data;
		}else if(THIS.headers && THIS.headers[$(obj).val()]){
			for(var i=0;i<THIS.headers[$(obj).val()].length;i++){
				headersData.push({
					data:THIS.headers[$(obj).val()][i].columnName,
					title:THIS.headers[$(obj).val()][i].columnName,
				});
			}
		}
		if(headersData !== undefined && headersData !== null){
			var tr = $('<tr/>').addClass('header ui-sortable');
			var datatableHeader = $('<table/>').addClass('table table-hover dc-data-table dataTable no-footer').append($('<thead/>').append(tr));			
			for (var i=0; i<headersData.length;i++){
				tr.append(
						$("<th/>").addClass("numbercolumn sorting").attr({"tabindex":"0","aria-controls":"DataTables_Table_0","rowspan":"1","colspan":"1","aria-label":headersData[i].data})
						.append($("<div/>").attr({"data-field":headersData[i].data}).addClass("sortable-header").html(headersData[i].title)));
				 
			}
			$(obj).parents('section').find('div[name=coloumns]').css({"max-width": $('#selectTable').css('width').substring(0, 4)-100,"overflow": "scroll"}).addClass('dataTables_scroll margin-20').html('').append(datatableHeader);
//			$("table","#wid-id-datatable").append($("<tbody/>").append($("<tr/>").addClass("bg-color-white").append($("<td/>").addClass("dataTables_empty").attr({"valign":"top","colspan":response.length+1}).html("Data will appear on preview"))));
//			$("[rel=popover]","#wid-id-datatable").popover();
			$(".header").sortable({placeholder: "ui-state-highlight",helper:'clone'});
			//$('#nodeConfigSection,#configSection,#filterConfigSection').find('section').find('div[name=coloumns]').css({'width' : ($(window).width()-300)});
			$(".dragRemove").droppable({
			      activeClass: "ui-state-default",
			      hoverClass: "ui-state-hover",
			      accept: ":not(.btn-fact)",
			      drop: function( event, ui ) {
			    	  if (ui.draggable.hasClass("sortable-header")){
			    	  	ui.draggable.parent().remove();
			    		ui.draggable.remove();
			    	  } else if (ui.draggable.hasClass("chart-div")){
			    		  ui.draggable.empty().append($("<span/>").addClass("dimension-placeholder"));
			    	  }
			      }
			});
			$(".sortable-header").draggable({
				 start: function( event, ui ) {$(".dragRemove").show();},
				 stop:function( event, ui ) { $(".dragRemove").hide();},
			     appendTo: "body",
			     helper: "clone"
			    }).on("click",function(){
			    	$(this).hide();
			    	if($(this).parent().find('input').length == 0){
			    		$(this).parent().append($("<input/>").addClass("edit-header").attr({"type":"input"}).val($(this).text()));
			    	}else{
			    		$(this).parent().find('input').show();
			    	}
			    	$(".edit-header").keyup(function() {
					    var value = $( this ).val();
					    $(this).siblings().text(value);
					  });
			    });
			
			$(document).mouseup(
					function(e) {
						$(obj).parents('section').find('div[name=coloumns]').find('label').removeClass('expanddiv');
						if (!$('.edit-header').is(e.target)
								&& $('.edit-header').has(
										e.target).length === 0) {
							$(".sortable-header").show();
							$(".edit-header").hide();
						}
					});
		}
	};
	var addNodeConfig =function(nodeList){
		//$('#nodeConfigSection').html('');
     var nodeLength = 1  ,node = {};//nodelength for creating number of node if already saved it will take length from Db otherwise it will take length as 1
		var nodeCount = $('#nodeConfigSection').find('section').length;//used for creating stackcolor unique id 
		if(nodeCount !=0 && nodeList === undefined ){
			$('#configSection').find('select').append($("<option/>").attr("value",nodeCount+1).html(nodeCount+1));
		}
			
	   if(nodeList && nodeList !=null && nodeList !=undefined ){
		     $('#nodeConfigSection').html('');
				nodeLength = nodeList.length;
	        }
			for (var i=1; i<= nodeLength; i++){
				nodeCount =nodeCount+1;
				$("#nodeConfigSection").find("[name='removeLink']").remove();
				if(nodeList && nodeList.length>0 && nodeList[i-1]){
					node = nodeList[i-1];
					nodeCount =i;
				}
				
				$('#nodeConfigSection').append($('<section/>').css({'outline':'1px solid #ccc'}).addClass("padding-10 margin-bottom-10")
						.append($('<div/>').addClass("row").attr("name","nodeSection").append($('<div/>').addClass("col col-1")
								.append($('<strong/>').addClass("pull-left").html($('#nodeConfigSection').find('section').length+1))
								.append($("<input/>").addClass("pull-right").val(node.colorCode?node.colorCode:'')
								.attr({"type":"color","id":"stackColor"+nodeCount,"title":"Pick color for Stack"})))
						.append($('<div/>').addClass("col col-1").append($("<strong/>").addClass("note").html("Name"))
								.append($('<input/>').val(node.legend?node.legend:'').attr({'placeholder':'Enter Legend'}).addClass('col-sm-8 input-xs')))		
						.append($('<div/>').addClass("col col-2").append($("<strong/>").addClass("note").html("Table"))
								.append($("<input>").attr({'type':'select2'}).addClass('form-control').val(node.table?node.table:'')))
						.append($('<div/>').addClass("col col-1").append($("<strong/>").addClass("note").html("Group By"))
								.append($("<label>").addClass('checkbox').attr({"title":"Group By"}).append($("<input/>").attr({"type":"checkbox","name":"groupby"}))
										.append($("<i/>"))))		
						.append($('<div/>').addClass("col col-2").append($("<strong/>").addClass("note").html("Filter Query"))
								.append($('<input/>').attr({'placeholder':'Enter click query','type':'clickquery'}).addClass('input-xs').val(node.clickquery?node.clickquery:'')))
						.append($('<div/>').addClass("col col-2").append($("<strong/>").addClass("note").html("Image Path"))
								.append($('<input/>').attr({'placeholder':'Image path','type':'imagepath'}).addClass('input-xs').val(node.imagepath?node.imagepath:'')))
						.append($('<div/>').addClass("col col-1").append($("<strong/>").addClass("note").html("Prefix Suffix"))
								.append($('<input/>').attr({'title':'Prefix','type':'prefix'}).addClass('col-1 input-xs').val(node.prefix?node.prefix:''))
								.append($('<input/>').attr({'title':'Suffix','type':'suffix'}).css({"margin-left":"5px"}).addClass('col-1 input-xs').val(node.suffix?node.suffix:'')))
						
						.append( $("<div/>").addClass("col col-1").append( $("<strong/>").html("Include").addClass("note"))
								.append($("<label/>").addClass("toggle pull-left")
										.append($("<input/>").attr({"type":"checkbox","name":"isIncluded","checked":"checked"}))
										.append($("<i/>").attr({"data-swchon-text":"On","data-swchoff-text":"Off"}))))	
						.append($("<div/>").addClass("col col-1 no-padding").attr("name","removeLink")
										.append($("<i/>").addClass("fa fa-2x fa-md fa-fw fa fa-remove txt-color-red cursor-pointer pull-right")
										.attr("title","Remove Node").bind("click",function(){removeConfig(this,$('#nodeConfigSection').find('section').length-1);}))))
						.append($('<div/>').addClass('row').append($('<div/>').attr({'name':'coloumns'}))));
				if(node){
					if(node.columns && node.columns.length>0){					
						constructHeader($('#nodeConfigSection').find('section:eq('+(i-1)+')').find('input[type=select2]')[0],node.columns);
					}
					if(!node.included){
						$('#nodeConfigSection').find('section:eq('+(i-1)+')').find('input[name="isIncluded"]').prop('checked', false);
					}
					if(node.groupby){
						$('#nodeConfigSection').find('section:eq('+(i-1)+')').find('input[name="groupby"]').attr({"checked":"checked"});
					}
					if(node.table){					
						$('#nodeConfigSection').find('section:eq('+(i-1)+')').find('input[type=select2]').select2('val',node.table);
					}else{
						$('#nodeConfigSection').find('section:eq('+(i-1)+')').find('input[type=select2]').select2({
							placeholder : "Select table",
							allowClear: true,
							data :  THIS.tableList || []
						}).on('change',function(){
							getColumnList(this);
						});
					}
				}
				$('#nodeConfigSection').find('section:eq('+($('#nodeConfigSection').find('section').length-1)+')').find('input[type=select2]').select2({
					placeholder : "Select table",
					allowClear: true,
					data :  THIS.tableList || []
				}).on('change',function(){
					getColumnList(this);
				});
				
			}
	};
	var addFilterConfig = function(length,data){
		$("#filterConfigSection")
		.append($("<section/>").css({'outline':'1px solid #ccc'}).attr({"id":'sectionFieldConfig'+length,"key":"filter"+(length+1)}).addClass('padding-10 margin-bottom-10')
				.append($("<div>").addClass("row").append($("<div>").addClass("col col-2")
				.append($("<strong/>").addClass("note").html("Field Name"))
				.append($("<input/>").attr({'placeholder':'Field Name'}).addClass("input-xs").val(data && data.searchField?data.searchField:'')))
		.append($("<div/>").addClass("col col-2")
				.append($("<strong/>").addClass("note").html("Display Name"))
				.append($("<input/>").attr({'placeholder':'Display Name'}).addClass("input-xs").val(data && data.displayName?data.displayName:'')))
		.append($("<div/>").addClass("col col-2")
				.append($("<strong/>").addClass("note").html("Type"))
				.append($("<Select/>").change(function(){
					onChangeTypeInFieldConfig(this,length,data?data:null);					
				}).addClass("input-xs")
						.append($("<option/>").attr("value","TEXT").html("Text"))
						.append($("<option/>").attr("value","DATE").html("Date"))
						.append($("<option/>").attr("value", "dynamicDate").html("Dynamic Date"))
						.append($("<option/>").attr("value","NUM").html("Number"))
						.append($("<option/>").attr("value", "lookupdropdown").html("Advancd Look Up"))
						.append($("<option/>").attr("value", "select").html("Drop Down"))
						.append($("<option/>").attr("value","lookup").html("Look Up")).val(data && data.fieldType?data.fieldType:'')))
		.append($("<div/>").addClass("col col-2")
				.append($("<strong/>").addClass("note").html("value"))
				.append($('<div/>').attr({'id':'divValueOfFieldType'+length}).addClass('row')
						.append($("<input/>").val(data && data.fieldType && data.fieldType == 'TEXT'?data.value1.replace(/'/g, ""):'')
								.addClass("input-xs"))))
				
		.append($("<div/>").addClass("col col-3")
				.append($("<strong/>").addClass("note").html("Link"))
				.append($("<label/>").attr({"name":"linkDropdownLabel"}).data("selectedlinks",(data && data.links)?data.links:'').addClass("input")))		
		.append($("<div/>").addClass("col col-1 no-padding").append($("<i/>").addClass("note")).append($("<div/>")
				.append($("<i/>").addClass("fa fa-2x fa-md fa-fw fa-remove txt-color-red cursor-pointer")
						.bind('click',function(){$(this).parents('section').remove();}))))
				
		).append($("<div>").toggle(data && data.fieldType && data.fieldType == 'lookup').attr('id','divLookupTextArea'+length).addClass("row").append($("<div>").addClass("col col-10 margin-20")
				.append($("<input/>").val(data && data.lookUpQuery?data.lookUpQuery:'')
				.attr({'placeholder':'Look Up Query'}).addClass('form-control input-xs'))))
		);
		THIS.onChangeOfStack();	
		onChangeTypeInFieldConfig($('#sectionFieldConfig'+length).find('select:eq(0)'),length,data);
	};
	var onChangeTypeInFieldConfig = function(obj,index,data){
		$('#divLookupTextArea'+index).hide();
		if($(obj).val() == 'DATE'|| $(obj).val() == 'dynamicDate' || $(obj).val() == 'NUM'){
			$('#divValueOfFieldType'+index).html('')
			.append($('<span/>').addClass('col-sm-2').html('From'))
			.append($('<input/>').addClass('col-sm-2 input-xs').val((data && data.value1)?data.value1:''))
			.append($('<span/>').addClass('col-sm-1').html('To'))
			.append($('<input/>').addClass('col-sm-2 input-xs').val((data && data.value2)?data.value2:''));
			if($(obj).val() == 'DATE'){
				$('#divValueOfFieldType'+index).find('input:eq(0)').datepicker({
				      defaultDate: "+1w",
				      changeMonth: true,
				      onClose: function( selectedDate ) {
				    	  $('#divValueOfFieldType'+index).find('input:eq(1)').datepicker( "option", "minDate", selectedDate );
				      }
				    });
				$('#divValueOfFieldType'+index).find('input:eq(1)').datepicker({
				      defaultDate: "+1w",
				      changeMonth: true,
				      onClose: function( selectedDate ) {
				    	  $('#divValueOfFieldType'+index).find('input:eq(0)').datepicker( "option", "maxDate", selectedDate );
				      }
				    });
			}
		}else{
			if($(obj).val() == 'lookup'){
				$('#divValueOfFieldType'+index).html('').append($("<input/>")
						.val((data && data.fieldType == 'TEXT' && data.value1)?data.value1.replace(/'/g, ""):'')
						.addClass("input-xs"));
				$('#divLookupTextArea'+index).show();
				var selectedData,lookupquery = $('#divLookupTextArea'+index).find('input').val(),
				placeholder = "Enter "+ ((data && data.displayName) ? (data.displayName):($('#sectionFieldConfig'+index).find('input:eq(1)').val())?($('#sectionFieldConfig'+index).find('input:eq(1)').val()):'value');
				if(data && data.defaultValue){
					selectedData = data.defaultValue;
				}
				constructSelect2($('#divValueOfFieldType'+index).find('input'),lookupquery,true,selectedData,"",null,placeholder,$('#source').select2('val'),$('#database').select2('val'));
			}
			else if($(obj).val() === 'lookupdropdown'){
				$('#divLookupTextArea'+index).show();
				var selectedData,lookupquery = $('#divLookupTextArea'+index).find('input').val(),
				placeholder = "Enter "+ ((data && data.displayName) ? (data.displayName):($('#sectionFieldConfig'+index).find('input:eq(1)').val())?($('#sectionFieldConfig'+index).find('input:eq(1)').val()):'value');
				if(data && data.defaultValue){
					selectedData = data.defaultValue;
				}
				$('#divValueOfFieldType'+index).empty();
				constructDropDown($('#divValueOfFieldType'+index),lookupquery,true,selectedData,"",placeholder,$('#source').select2('val')|| THIS.source,$('#database').select2('val') || THIS.database,{buttonWidth:"150"});
			}
			else if($(obj).val() === 'select'){
				/*$('#divValueOfFieldType'+index).html('').append($("<input/>")
						.val((data && data.fieldType == 'TEXT' && data.value1)?data.value1.replace(/'/g, ""):'')
						.addClass("input-xs"));*/
				$('#divLookupTextArea'+index).show();
				var selectdata =  $('#divLookupTextArea'+index).find('input').val();
				if(selectdata){
					selectdata = Jsonparse(selectdata);
					if(!selectdata || Object.keys(selectdata).length === 0){
						selectdata = [];
						var values =  $('#divLookupTextArea'+index).find('input').val();
						values = values.split(',');
						if(values && values.length>0){
							for(var i=0;i<values.length;i++){
								selectdata.push({value:values[i],label:values[i]});
							}								
						}
					}else if(selectdata.filter(function(o){return o.id}).length>0){
						var tempData = [];
						for(var i=0;i<selectdata.length;i++){
							tempData.push({value:selectdata[i].id,label:selectdata[i].text});	
						}
						selectdata = tempData;
					}
				}
				var selectedData;
				if(data && data.defaultValue){
					selectedData = data.defaultValue;
				}
				var placeHolder = "Select "+ ((data && data.displayName) ? (data.displayName):($('#sectionFieldConfig'+index).find('input:eq(1)').val())?($('#sectionFieldConfig'+index).find('input:eq(1)').val()):'value');
				constructDropDown($('#divValueOfFieldType'+index),selectdata,true,selectedData,"",placeHolder,'','',{buttonWidth:"150"});
			}
		}
		
	};

	var addConfig = function(length,data){
		var tempOption = $("<div/>");
		tempOption.append($("<option/>").attr("value","").html("Choose"));
		var stackCount = $('#nodeConfigSection').find('section').length;
		for (var i=1; i<= stackCount; i++){
			tempOption.append($("<option/>").attr("value",i).html(i));
		}
		$("#configSection").append($("<section/>").attr({'rowIndex':length}).css({'outline':'1px solid #ccc'}).addClass(' padding-10 margin-bottom-10')
				.append($("<div>").addClass("row")
						.append($("<div>").addClass("col col-1").html('')
							.append($("<strong/>").addClass("note").html("Name"))	
							.append($('<input/>').blur(function() {
								THIS.onChangeOfStack(length);
							}).addClass('form-control').val('#'+(length+1)) ))
						.append($("<div>").addClass("col col-1")
								.append($("<strong/>").addClass("note").html("From"))
								.append($("<label/>").addClass("select").append($("<select>").change(function(){
										THIS.onChangeOfStack(length);
								}).addClass("input-sm customRequired")
								.attr({"name":"stack","type":"from","id":"selectStateConfigFrom"+length})
								.append(tempOption.html())).append($("<i/>"))))
						.append($("<div/>").addClass("col col-1")
								.append($("<strong/>").addClass("note").html("To"))
							.append($("<label/>").addClass("select").append($("<select>").change(function(){
								THIS.onChangeOfStack(length);
							}).addClass("input-sm customRequired")
							.attr({"name":"stack","type":"to","id":"selectStateConfigTo"+length})
							.append(tempOption.html())).append($("<i/>"))))
						.append($("<div/>").addClass("col col-8")
								.append($("<strong/>").addClass("note").html("Query"))
							.append($("<label/>").addClass("textarea textarea-expandable").append($("<textarea>").addClass("custom-scroll customRequired")
									.val(data && data.query?data.query:'')
									.attr({"rows":"3","name":"query","type":"query"}))))		
						.append($("<div/>").addClass("col col-1 no-padding")
								.append($("<i/>").addClass("fa fa-md fa-fw fa-plus-square color-blue cursor-pointer")
								.attr("title","add link").bind("click",function(){addConfig($('#configSection').find('section').length);}))
								.append($("<i/>").addClass("fa fa-2x fa-md fa-fw fa-remove txt-color-red cursor-pointer pull-right")
								.attr("title","Remove link").bind("click",function(){removeConfig(this,$('#configSection').find('section').length-1);}))))
					.append($("<div>").addClass("row")
							.append($("<div>").addClass("col col-1")
									.append($("<input/>").addClass('margin-10').val(data && data.linkColor?data.linkColor:'')
											.attr({"type":"color","name":"link-color","title":"Pick color for Stack"})))
							.append($("<div>").addClass("col col-2 text-center margin-top-2")
									.append($("<strong/>").addClass("note").html("Legend"))
									.append($("<input/>").val(data && data.legendName?data.legendName:'').addClass('input-xs').attr({'name':'legend',"type":"text","placeHolder":"Enter Legend"})))
							.append($("<div/>").addClass("col col-4").append($("<strong/>").addClass("note").html("Table")).append($("<input/>").addClass('form-control').attr({"type":"select2"}).val(data && data.table?data.table:'')))
							.append($('<div/>').addClass("col col-1").append($("<strong/>").addClass("note").html("Group By")).append($("<label>").addClass('checkbox').attr({"title":"Group By"}).append($("<input/>").attr({"type":"checkbox","name":"groupby"})).append($("<i/>"))))
							.append($('<div/>').addClass("col col-2").append($("<strong/>").addClass("note").html("Filter")).append($('<input/>').attr({'placeholder':"columnFrom in ('<F>') and columnTo in ('<T>')",'type':'clickquery'}).addClass('input-xs').val(data && data.clickquery?data.clickquery:'')))
							.append($('<div/>').addClass("col col-1").append($("<strong/>").addClass("note").html("Column Info")).append($("<label/>").addClass("toggle").append($("<input/>").attr({"type":"checkbox","name":"columninfo"})).append($("<i/>").attr({"data-swchon-text":"Yes","data-swchoff-text":"No"}))))
//							.append($('<div/>').addClass("col col-1").append($('<input/>').attr({'placeholder':'Suffix','type':'suffix'}).addClass('col-sm-8 input-xs').val(data && data.suffix?data.suffix:'')))
//							.append($('<div/>').addClass("col col-1").append($('<input/>').attr({'placeholder':'Prefix','type':'prefix'}).addClass('col-sm-8 input-xs').val(data && data.prefix?data.prefix:'')))
							)
							
					.append($('<div/>').addClass("row").append($('<div/>').attr({'name':'coloumns'}))));
		if(data){
			if(data.groupby){
				$('#configSection').find('section:eq('+(length)+')').find('input[name="groupby"]').attr({"checked":"checked"});
			}
			if(data.columninfo){
				$('#configSection').find('section:eq('+(length)+')').find('[name="columninfo"]').prop({"checked":true});
			}
			if(data.fromStack){
				$('#selectStateConfigFrom'+length).val(data.fromStack);
			}
			if(data.toStack){
				$('#selectStateConfigTo'+length).val(data.toStack);
			}
			if(data.columns && data.columns.length>0){					
				constructHeader($('#configSection').find('section:eq('+(length)+')').find('input[type=select2]')[0],data.columns);
			}
		}else{
			$('#configSection').find('input[type=select2]').each(function(){
				if($(this).val() && $(this).parents('section').find('table').length == 0){
					getColumnList(this);
				}
				$(this).select2({
					placeholder : "Select table",
					allowClear: true,
					data :  THIS.tableList || []
				}).on('change',function(){
					getColumnList(this);
				});
			});
		}
	};
	var stackSelect = function(stackCount){
		$("#actionBtn,#mySankey").show(300);
		$('#nodeConfigSection').html('');
		$("[name=stack]").empty().append($("<option/>").attr("value","").html("Choose"));
		//for (var i=1; i<= stackCount; i++){
			$("[name=stack]").append($("<option/>").attr("value","1").html("1"));
		//}
		$("#saveAs").hide();
		
		addNodeConfig();
	};
	this.onChangeOfStack = function(rowIndex){
		var options = [];
		$("#configSection").find("section").each(function(){
			var value = ($(this).find("input:eq(0)").val()|| "")+":"+($(this).find("select[type='from']").val() || "")+"-"+($(this).find("select[type='to']").val() || "");
			options.push({
				value:value,
				label:value,
				title:value
			});
		});
		$("#filterConfigSection").find("[name='linkDropdownLabel']").each(function(){
			var selectedData =  $(this).data("selectedlinks");
			constructDropDown(this,options,true,selectedData,null,"Select Links");
		});
	};
	var removeConfig = function(obj,index){
		$('[id^="optionselectStateConfigFrom'+index+'selectStateConfigTo'+index+'"]').each(function(){
			$(this).remove();
		});
			$(obj).parents("section").remove();
			var removeValue =$('#nodeConfigSection').find('section').length+1;
			if($(obj).parents("#nodeConfigSection")){
				$('#configSection').find('select').find('option[value='+removeValue+']').remove();
			}
			
		if($("#nodeConfigSection").find("[name='removeLink']").length === 0){
			$("#nodeConfigSection").find('section:last').find('[name="nodeSection"]').append($("<div/>").addClass("col col-1 no-padding").attr("name","removeLink")
					.append($("<i/>").addClass("fa fa-2x fa-md fa-fw fa-remove txt-color-red cursor-pointer pull-right")
							.attr("title","Remove Node").bind("click",function(){removeConfig(this,$('#nodeConfigSection').find('section').length-1);})));
		}
		
	};
	
	
	var preview = function(){
		var config = generateSankeyConfig();
		var configRequest = {
			sankeyFilterMap :  config.sankeyFilterMap
		};
		configRequest.nodeList = config.nodeList.filter(function(obj){return obj.included === true});
		var includedNode = configRequest.nodeList.map(function(obj){return obj.nodeindex});
		configRequest.sankeyLinkList = config.sankeyLinkList.filter(function(obj){return includedNode.indexOf(parseInt(obj.fromStack))>-1 && includedNode.indexOf(parseInt(obj.toStack))>-1})
		if(configRequest.sankeyLinkList && configRequest.sankeyLinkList.length>0){
			var limitCount = $("#generalSection").find("[name='defaultslidervalue']").val() || "20",
			orderType = $("#generalSection").find("[name='ordertype']").val() || "DESC";
			for(var i=0;i<configRequest.sankeyLinkList.length;i++){
				if(configRequest.sankeyLinkList[i].query.indexOf("<LIMIT>")>-1){
					configRequest.sankeyLinkList[i].query = configRequest.sankeyLinkList[i].query.replace("<LIMIT>",limitCount);
				}
				if(configRequest.sankeyLinkList[i].query.indexOf("<ORDERTYPE>")>-1){
					configRequest.sankeyLinkList[i].query = configRequest.sankeyLinkList[i].query.replace("<ORDERTYPE>",orderType);
				}
			}
		}
		enableLoading();
			var request = {
					"source":$("#source").select2('val'),
					"database":$("#database").select2('val'),
					"config":JSON.stringify(configRequest)
			};
			callAjaxService("getSankeyJSON",  function(response){callBackGetSankeyJSON(response);},
					callBackFailure, request, "POST");
	};
	
	
	var invokeSaveSankeyProfile = function(isSaveAs){
		var saveSankeyProfile = function(){
			var callBackSuccessHandler;
			var configRequest = generateSankeyConfig();
			var remarks;
			var displayName;
			
			if(!isSaveAs){
				if(($("#save").data("refId") == null || $("#save").data("refId") == "")){ // Update values if saving on first time
					$("#save").data("remarks",$("#myModalthredlevel #description").val());
					$("#save").data("displayname",$("#myModalthredlevel #sankeyname").val());
					remarks = $("#myModalthredlevel #description").val();
					displayName = $("#myModalthredlevel #sankeyname").val();
				}else{
					remarks = $("#save").data("remarks");
					displayName =  $("#save").data("displayname");
				}
				callBackSuccessHandler = callBackSaveSankey;
			}else{
				callBackSuccessHandler = function(response){
					disableLoading();
					$('#myModalthredlevel').modal('hide');
					showNotification("success","Sankey saved as New Profile successfully..");
					$("#save").data("refId",response);
				};
				remarks = $("#myModalthredlevel #description").val();
				displayName = $("#myModalthredlevel #sankeyname").val();
			}
			var additionalconfig =  {};
			if ($('#hideMinified').is(':checked')) {
				additionalconfig.isMinified = true;
			};
			if($("#sankeyOnOffswitch").is(":checked")){
				additionalconfig.isFilterInline=true;
			};
			if(Object.keys(additionalconfig).length>0){
				additionalconfig = JSON.stringify(additionalconfig);	
			}else{
				additionalconfig = "";
			}
			var request = {
				"source":$("#source").select2('val'),
				"database":$("#database").select2('val'),
				"config":JSON.stringify(configRequest),
				"type":"sankey",
				"stackcount":$('#nodeConfigSection').find('section').length,//$("#stackCount").select2("val"),
				"remarks" : remarks,
				"displayname" : displayName,
				"additionalconfig": additionalconfig,
				"id" : isSaveAs ==true ?undefined : $("#save").data("refId")
			};
			enableLoading();
			callAjaxService("saveProfile", callBackSuccessHandler,
					callBackFailure, request, "POST");
		};
		
		if(isSaveAs || ($("#save").data("refId")== null || $("#save").data("refId")== "")){
			$("#myModalthredlevel #description").val("");
			$("#myModalthredlevel #sankeyname").val("");
			$("#myModalthredlevel").modal('show');
			$("#myModalthredlevel #btnCreate").unbind("click").bind("click",function(){
				if (!$("#frmSankeyFilter").valid())
					return;
				saveSankeyProfile();});
		}else{
			saveSankeyProfile();
		}
		
	};
	
	
	/**
	 * Will Generate Sankey Config and return config Object
	 */
	this.generateSankeyConfig = function(){
		if (!$("#frmConf").valid())
			return;
		var configRequest = {
			sankeyLinkList:[],
			nodeList:[],
			source:	$("#source").select2('val'),
			database:$("#database").select2('val'),
			sankeyFilterMap:{},
			general:{}
		};
		var valid = true;
		$("section",$("#nodeConfigSection")).each(function(index,ele){
			var columns = [];
			$("th",$(this)).each(function(index,thEle){
				columns.push({
					"data":$(thEle).find("div").data("field"),
					"defaultContent":"",
					 "title":$(thEle).find("div").text()
				  });
			  });
			var tempNode = {
					colorCode:$('#stackColor'+(index+1)).val(),
					legend:$(this).find('input:eq(1)').val(),
					table:$(this).find('input[type=select2]').select2('val'),
					clickquery:$(this).find('input[type=clickquery]').val(),
					suffix:$(this).find('input[type=suffix]').val(),
					prefix:$(this).find('input[type=prefix]').val(),
					imagepath:$(this).find('input[type=imagepath]').val(),
					nodeindex:index+1,
					columns:columns
				};
			if($(this).find('input[name="isIncluded"]').is(":checked")){
				tempNode.included = true;
			}
			if($(this).find('input[name="groupby"]').is(":checked")){
				tempNode.groupby = true;
			}
			if ($(this).find('input[type=select2]').select2('val')  == "[object Object]")
				tempNode.table = "";
			else
				tempNode.table = $(this).find('input[type=select2]').select2('val');
			configRequest.nodeList.push(tempNode);
		});
		$("section",$("#filterConfigSection")).each(function(index,ele){
			var key = 'filter'+(index+1);
			$(this).find("em").remove();
			if (!$('#sectionFieldConfig'+index).find('input:eq(0)').val()){
				valid = false;
				$($('#sectionFieldConfig'+index).find('input:eq(0)').parent()).removeClass("state-success").addClass("state-error").append($("<em/>").html('').addClass("invalid").html("required"));
			}else{
				$($('#sectionFieldConfig'+index).find('input:eq(0)').parent()).removeClass("state-error").addClass("state-success").find("em").remove();
			}
			
			if (!$('#sectionFieldConfig'+index).find('input:eq(1)').val()){
				valid = false;
				$($('#sectionFieldConfig'+index).find('input:eq(1)').parent()).removeClass("state-success").addClass("state-error").append($("<em/>").addClass("invalid").html("required"));
			}else{
				$($('#sectionFieldConfig'+index).find('input:eq(1)').parent()).removeClass("state-error").addClass("state-success").find("em").remove();
			}
			var selectedLinks = $('#sectionFieldConfig'+index).find("[name='linkDropdownLabel']").find("select").val();
			if(selectedLinks && selectedLinks.length>0){
				$($('#sectionFieldConfig'+index).find("[name='linkDropdownLabel']").parent()).removeClass("has-error").addClass("state-success").find("em").remove();
				var spaceIndex = selectedLinks.indexOf(" ");
				  if(spaceIndex>-1)
					  selectedLinks.splice(spaceIndex,1);
			}else{
				valid = false;
				$($('#sectionFieldConfig'+index).find("[name='linkDropdownLabel']").parent()).removeClass("state-success").addClass("has-error").append($("<em/>").addClass("invalid").html("required"));
			}
			configRequest.sankeyFilterMap[key] = {};
			configRequest.sankeyFilterMap[key].key = key;
			configRequest.sankeyFilterMap[key].searchField = $('#sectionFieldConfig'+index).find('input:eq(0)').val();
			configRequest.sankeyFilterMap[key].displayName = $('#sectionFieldConfig'+index).find('input:eq(1)').val();
			configRequest.sankeyFilterMap[key].fieldType = $('#sectionFieldConfig'+index).find('select:eq(0)').val();
			configRequest.sankeyFilterMap[key].links = selectedLinks;
			if($('#sectionFieldConfig'+index).find('select:eq(0)').val() == 'NUM'|| $('#sectionFieldConfig'+index).find('select:eq(0)').val() == 'DATE'){
				configRequest.sankeyFilterMap[key].value1 = $('#divValueOfFieldType'+index).find('input:eq(0)').val();
				configRequest.sankeyFilterMap[key].value2 = $('#divValueOfFieldType'+index).find('input:eq(1)').val();
				if(configRequest.sankeyFilterMap[key].fieldType == 'NUM'){
					configRequest.sankeyFilterMap[key].value1 = parseInt(configRequest.sankeyFilterMap[key].value1);
					configRequest.sankeyFilterMap[key].value2 = parseInt(configRequest.sankeyFilterMap[key].value2);
				}
			}else if($('#sectionFieldConfig'+index).find('select:eq(0)').val() == 'TEXT'){
				configRequest.sankeyFilterMap[key].value1 = $('#divValueOfFieldType'+index).find('input:eq(0)').val();
				configRequest.sankeyFilterMap[key].value1 = configRequest.sankeyFilterMap[key].value1?"'"+ configRequest.sankeyFilterMap[key].value1.replace(/,/g, "','") +"'":'';
			}else{
				configRequest.sankeyFilterMap[key].lookUpQuery = $('#divLookupTextArea'+index).find('input').val();
				var selectType = $('#sectionFieldConfig'+index).find('select:eq(0)').val();
				if(selectType === 'lookup'){
					configRequest.sankeyFilterMap[key].defaultValue = $('#divValueOfFieldType'+index).find('input').select2('data');
					if(configRequest.sankeyFilterMap[key].defaultValue && configRequest.sankeyFilterMap[key].defaultValue.length>0){
						configRequest.sankeyFilterMap[key].value1 = configRequest.sankeyFilterMap[key].defaultValue.map(function(a) {return "'"+a.id +"'";}).toString();
					}else{
						configRequest.sankeyFilterMap[key].value1 = "";
					}
					if (!$('#divLookupTextArea'+index).find('input').val()){
						valid = false;
						$('#divLookupTextArea'+index).removeClass("state-success").addClass("state-error").find('div').append($("<em/>").addClass("invalid").html("required"));
					}else{
						$('#divLookupTextArea'+index).removeClass("state-error").addClass("state-success").find("em").remove();
					}
				}
				else if($('#sectionFieldConfig'+index).find('select:eq(0)').val() == 'lookupdropdown' || selectType === 'select'){
					configRequest.sankeyFilterMap[key].defaultValue = $('#divValueOfFieldType'+index).find("select").val();
					if(configRequest.sankeyFilterMap[key].defaultValue){
						var indexVal = configRequest.sankeyFilterMap[key].defaultValue.indexOf(" ");
						if(indexVal>-1)
							configRequest.sankeyFilterMap[key].defaultValue.splice(indexVal,1);
					}
					if(configRequest.sankeyFilterMap[key].defaultValue && configRequest.sankeyFilterMap[key].defaultValue.length>0){
						configRequest.sankeyFilterMap[key].value1 = configRequest.sankeyFilterMap[key].defaultValue.map(function(a) {return "'"+a +"'";}).toString();
					}else{
						configRequest.sankeyFilterMap[key].value1 = "";
					}
				}
			}			
		});
		var generalConfig = $("#generalSection").serializeArray();
		if(generalConfig && generalConfig.length>0){
			for(var i=0;i<generalConfig.length;i++){
				if(generalConfig[i].value && generalConfig[i].value.trim())
					configRequest.general[generalConfig[i].name] = generalConfig[i].value;
			}
		}
		if (!valid)
			return;
		$("section",$("#configSection")).each(function(index,ele){
			var columns = [];
			$("th",$(this)).each(function(index,thEle){
				columns.push({
						  "data":$(thEle).find("div").data("field"),
						  "defaultContent":"",
						  "title":$(thEle).find("div").text()
				  });
			  });
			var tempValid = true;
			var tempRequest ={};
			var fromStack = $("[type=from]",$(ele));
			var toStack = $("[type=to]",$(ele));
			var query = $("[type=query]",$(ele));
			$(toStack.parent()).addClass("state-error").find("em").remove();
			$(query.parent()).addClass("state-error").find("em").remove();
			$(fromStack.parent()).addClass("state-error").find("em").remove();
			if (fromStack.val() ==undefined || $.trim(fromStack.val()) == ""){
				valid = false;
				tempValid = false;
				$(fromStack.parent()).addClass("state-error").removeClass("state-success").append($("<em/>").addClass("invalid").html("required"));
			};
			tempRequest["fromStack"] = fromStack.val();
			tempRequest["fromStackColor"] = $('#stackColor'+fromStack.val()).val();
				//$("[name=stack-color"+fromStack.val()+"]",$(ele)).val();
			
			if (toStack.val() ==undefined || $.trim(toStack.val()) == ""){
				valid = false;
				tempValid = false;
				$(toStack.parent()).addClass("state-error").removeClass("state-success").append($("<em/>").addClass("invalid").html("required"));
			};
			tempRequest["toStack"] = toStack.val();
			tempRequest["toStackColor"] = $('#stackColor'+toStack.val()).val();
				//$("[name=stack-color"+toStack.val()+"]",$(ele)).val();
			tempRequest.key = $(this).find('input:eq(0)').val()+':'+tempRequest.fromStack +'-'+tempRequest.toStack;
			/*if (tempValid && fromStack.val() >= toStack.val()){
				$(toStack.parent()).addClass("state-error").removeClass("state-success").append($("<em/>").addClass("invalid").html("invalid"));
			}*/
			
			if (query.val() ==undefined || $.trim(query.val()) == ""){
				valid = false;
				$(query.parent()).removeClass("state-success").addClass("state-error").append($("<em/>").addClass("invalid").html("required"));;
			};
			tempRequest["query"] = query.val();
			tempRequest["linkColor"] = $("[name=link-color]",$(ele)).val();
			tempRequest.legendName = $("[name=legend]",$(ele)).val();
			if($(this).find('input[name="groupby"]').is(":checked")){
				tempRequest.groupby = true;
			}
			if($(this).find('input[name="columninfo"]').is(":checked")){
				tempRequest.columninfo = true;
			}
			if ($(this).find('input[type=select2]').select2('val')  == "[object Object]")
				tempRequest.table = "";
			else
				tempRequest.table = $(this).find('input[type=select2]').select2('val');
			tempRequest["limit"] =10;
			tempRequest.columns = columns;
			tempRequest.clickquery = $(this).find('input[type=clickquery]').val(),
//			tempRequest.suffix = $(this).find('input[type=suffix]').val(),
//			tempRequest.prefix = $(this).find('input[type=prefix]').val(),
			tempRequest.filterKeyList = [];
			Object.keys(configRequest.sankeyFilterMap).forEach(function(key) {
				if(configRequest.sankeyFilterMap[key].links.indexOf(tempRequest.key) > -1){
					tempRequest.filterKeyList.push(key);
				}
			});
			configRequest.sankeyLinkList.push(tempRequest);
		});
		if (!valid)
			return;
		THIS.sankeyConfig = configRequest;
		return configRequest;
	};
	this.getSankeyJSON = function(data){
		$("#dataTableDiv").hide();
		data.config = data.config.replace(/@/g, "'");
		var request = {
			"source":data.source,
			"database":data.database
		};
		var config = Jsonparse(data.config),rowDiv,mainSection,rowIndex = 0,filterContentDiv;
		if(data.additional_config){
			THIS.additionalconfig = JSON.parse(data.additional_config);
		}
		if(THIS.additionalconfig && THIS.additionalconfig.isMinified){
			if(THIS.additionalconfig.isMinified === true){
				$("body").addClass('minified');
				}
		}
		
		if (THIS.additionalconfig && THIS.additionalconfig.isFilterInline) {
			$('#shankeyFilterModal').modal('hide');
			$("#sankeyInlineFilter").show();
			$("#filter").hide();
			filterContentDiv = "sankeyFilterContent";
		} else {
			$("#filterProfile").show();
			$("#filter").show();
			$("#sankeyInlineFilter").hide();
			filterContentDiv = "sankeyModalFilterContent";
			$("#filter").unbind('click').bind('click',function(){
				$('#filterModal').modal('show');
			});
		}		
		if(config.sankeyFilterMap && Object.keys(config.sankeyFilterMap).length>0){
			
			$('#'+filterContentDiv).html('');
			$.each(config.sankeyFilterMap, function(index, obj) {
				
					if(rowIndex%3 == 0){
						rowDiv = $('<div/>').addClass('row');
						$('#'+filterContentDiv).append(rowDiv);
					}
					if(obj.fieldType == 'dynamicDate'){
						obj.value1 = eval(obj.value1);
						obj.value2 = eval(obj.value2);
					}
				mainSection = $('<section/>').addClass('col col-4 col-sm-4').attr({'key':index,'type':obj.fieldType,'field':obj.searchField});
				rowDiv.append(mainSection
						.append($('<div/>').addClass(THIS.additionalconfig && THIS.additionalconfig.isFilterInline?'':'note').append($('<strong/>').addClass("note").html(obj.displayName+' :')))
						.append($('<label/>').addClass('input width-100per')
						.append(
								obj.fieldType == 'TEXT'?$('<input/>').val(obj.value1.replace(/'/g, "")).attr({'placeholder':'Enter '+obj.displayName}).addClass('form-control'):
								(obj.fieldType == 'lookup' || obj.fieldType == 'lookupdropdown' || obj.fieldType == 'select')?$('<div/>').attr({"name":"dropdowndiv"}).addClass((obj.innotin?'col-md-9 width-100per':'col-md-12  width-100per')).append($('<input/>').val(obj.value1).addClass('input')):
									$('<div/>').addClass('row margin-2')
									.append($('<div/>').addClass('col-sm-6').append($('<div/>').addClass('note col-sm-3').append($('<strong/>').html('From')))
											.append($('<label/>').addClass('input col-sm-8').append($('<input/>').addClass('form-control').val(obj.value1))))
									.append($('<div/>').addClass('col-sm-6').append($('<div/>').addClass('note col-sm-2').append($('<strong/>').html('To')))
											.append($('<label/>').addClass('input col-sm-9').append($('<input/>').addClass('form-control').val(obj.value2))))
									))
				);
				if(obj.fieldType == 'DATE' || obj.fieldType == 'dynamicDate'){
					mainSection.find('input:eq(0)').datepicker({
					      defaultDate: "+1w",
					      changeMonth: true,
					      onClose: function(selectedDate) {						    	  
					    	  $('div[key="' + index + '"]').find('input:eq(1)').datepicker( "option", "minDate", selectedDate );
					      }
					});
					mainSection.find('input:eq(1)').datepicker({
					      defaultDate: "+1w",
					      changeMonth: true,
					      onClose: function(selectedDate) {
					    	  $('div[key="' + index + '"]').find('input:eq(0)').datepicker( "option", "maxDate", selectedDate );
					      }
					});
				}
				else if(obj.fieldType == 'select'){
					var selectdata =  obj.lookUpQuery;
					if(selectdata){
						selectdata = Jsonparse(selectdata);
						if(!selectdata || Object.keys(selectdata).length === 0){
							selectdata = [];
							var values =  obj.lookUpQuery.split(',');
							if(values && values.length>0){
								for(var i=0;i<values.length;i++){
									selectdata.push({value:values[i],label:values[i]});	
								}								
							}
						}else if(selectdata.filter(function(o){return o.id}).length>0){
							var tempData = [];
							for(var i=0;i<selectdata.length;i++){
								tempData.push({value:selectdata[i].id,label:selectdata[i].text});	
							}
							selectdata = tempData;
						}
					}
					var selectedData;
					if(obj && obj.defaultValue){
						selectedData = obj.defaultValue;
					}
					constructDropDown(mainSection.find("[name='dropdowndiv']"),selectdata,true,selectedData,"","Select "+obj.displayName,'','',{buttonWidth:obj.innotin?"200":"300"});
				}
				else if(obj.fieldType == 'lookupdropdown'){
					$(mainSection).data("defaultvalue",obj.defaultValue);
					setTimeout(function(selecter){
						constructDropDown(selecter.find("[name='dropdowndiv']"),obj.lookUpQuery,true,obj.defaultValue,null,"Enter "+ obj.displayName,data.source,data.database,{buttonWidth:obj.innotin?"200":"300",filterId:"#sankeyFilterContent"});
						}, 100,mainSection); 
				}
				else if(obj.fieldType == 'lookup'){
					if(obj.defaultValue && obj.defaultValue.length>0){
						config.sankeyFilterMap[index].value1 = obj.defaultValue.map(function(a) {return "'"+a.id +"'";}).toString();
					}else{
						config.sankeyFilterMap[index].value1 = "";
					}
					mainSection.find('input').addClass('width-100per');
					constructSelect2(mainSection.find('input'),obj.lookUpQuery,true,obj.defaultValue,"",null,"Enter "+ obj.displayName,data.source,data.database);
				}
				rowIndex++;
			});
		}
		$('[name="applyFilter"],[name="applynodes"]').unbind('click').bind('click',function(){
			$("#chat-container").removeClass("open");
			applyGetData(data.config,filterContentDiv);
		});
		THIS.sankeyConfig = Jsonparse(data.config);
		THIS.displayName = data.displayname;
		THIS.sankeyConfig.source = data.source;
		THIS.sankeyConfig.database = data.database;
		$("#chat-users").empty();
		var selectAllCheckbox = $("<input/>").attr({"type":"checkbox","name":"nodecheckbox"});
		$("#chat-users").append($("<li/>").append( $("<label/>").addClass("checkbox")
				.append(selectAllCheckbox).append($("<i/>")).append("Select All")));
		for(var i=0;i<THIS.sankeyConfig.nodeList.length;i++){
			var nodeCheckbox = $("<input/>").attr({"type":"checkbox","name":"nodecheckbox"+THIS.sankeyConfig.nodeList[i].nodeindex}).prop('checked', THIS.sankeyConfig.nodeList[i].included);
				$("#chat-users").append($("<li/>").append( $("<label/>").addClass("checkbox").css({"background":THIS.sankeyConfig.nodeList[i].colorCode,"color":invertColor(THIS.sankeyConfig.nodeList[i].colorCode,true)})
						.append(nodeCheckbox).append($("<i/>")).append(THIS.sankeyConfig.nodeList[i].legend)));
		}
		$(selectAllCheckbox).change(function() {
			$("[name^='nodecheckbox']").prop('checked', $(this).is(":checked"));
		});
		var configRequest = {
			sankeyFilterMap:config.sankeyFilterMap	
		};
		configRequest.nodeList = config.nodeList.filter(function(obj){return obj.included === true});
		var includedNode = configRequest.nodeList.map(function(obj){return obj.nodeindex});
		configRequest.sankeyLinkList = config.sankeyLinkList.filter(function(obj){return includedNode.indexOf(parseInt(obj.fromStack))>-1 && includedNode.indexOf(parseInt(obj.toStack))>-1});

		if(THIS.sankeyConfig.general){
			if(THIS.sankeyConfig.general.maxslidervalue){
				$("#sankeySliderSetting").show();
				$("#sankeyslider").slider({
					min: 0,
					max: parseInt(THIS.sankeyConfig.general.maxslidervalue),
					value:parseInt(THIS.sankeyConfig.general.defaultslidervalue),
					//tooltip: 'always',
					//tooltip: 'show',
					//scale: 'logarithmic',
					step: 1
				});
				$("#sankeyslider").val(THIS.sankeyConfig.general.defaultslidervalue).on("slide", function(slide) {
					$("#sankeySliderSetting").find("[name='slidervalue']").html(slide.value);
				});
				$("#sankeyslider").val(THIS.sankeyConfig.general.defaultslidervalue).on("slideStop", function(slide) {
					applyGetData(data.config,filterContentDiv);
				});
				$("#sankeyordertype").on("change",function(){
					applyGetData(data.config,filterContentDiv);
				});
				$("#sankeySliderSetting").find("[name='slidervalue']").html(THIS.sankeyConfig.general.defaultslidervalue);
				$("#sankeySliderSetting").find("[name='toRange']").html(THIS.sankeyConfig.general.maxslidervalue);
			}
			if(THIS.sankeyConfig.general.ordertype && THIS.sankeyConfig.general.ordertype === "ASC"){
				$("#sankeyordertype").prop({"checked":"checked"});
			}
			if(configRequest.sankeyLinkList && configRequest.sankeyLinkList.length>0){
				for(var i=0;i<configRequest.sankeyLinkList.length;i++){
					if(configRequest.sankeyLinkList[i].query.indexOf("<LIMIT>")>-1){
						configRequest.sankeyLinkList[i].query = configRequest.sankeyLinkList[i].query.replace("<LIMIT>",THIS.sankeyConfig.general.defaultslidervalue || 20);
					}
					if(configRequest.sankeyLinkList[i].query.indexOf("<ORDERTYPE>")>-1){
						configRequest.sankeyLinkList[i].query = configRequest.sankeyLinkList[i].query.replace("<ORDERTYPE>",THIS.sankeyConfig.general.ordertype || "DESC");
					}
				}
			}
		}
		request.config = JSON.stringify(configRequest);
		enableLoading();
		callAjaxService("getSankeyJSON",  function(response){callBackGetSankeyJSON(response);},callBackFailure, request, "POST",null,true);
	};
	var applyGetData = function(config,filterContentDiv){
		config = JSON.parse(config);
		$('#'+filterContentDiv).find('section').each(function(){
			if(config.sankeyFilterMap[$(this).attr('key')].fieldType == 'Date' || config.sankeyFilterMap[$(this).attr('key')].fieldType == 'NUM'){
				config.sankeyFilterMap[$(this).attr('key')].value1 = $(this).find('input:eq(0)').val();
				config.sankeyFilterMap[$(this).attr('key')].value2 = $(this).find('input:eq(1)').val();
			}
			else if(config.sankeyFilterMap[$(this).attr('key')].fieldType == 'lookup'){
				if($(this).find('input').select2('val').length>0){
					config.sankeyFilterMap[$(this).attr('key')].value1 =  "'"+ $(this).find('input').select2('val').toString().replace(/,/g, "','") +"'";
				}else{
					config.sankeyFilterMap[$(this).attr('key')].value1 =  "";
				}
			}else if(config.sankeyFilterMap[$(this).attr('key')].fieldType == 'lookupdropdown' || config.sankeyFilterMap[$(this).attr('key')].fieldType == 'select'){
				var selectedValue = $(this).find("select").val();
				if(selectedValue && selectedValue.length>0){
					var index = selectedValue.indexOf(" ");
					  if(index>-1)
						  selectedValue.splice(index,1);
					config.sankeyFilterMap[$(this).attr('key')].value1 = selectedValue.map(function(o){return "'"+o+"'";}).toString();
				}else{
					config.sankeyFilterMap[$(this).attr('key')].value1 = "";
				}
			}
			else{
				config.sankeyFilterMap[$(this).attr('key')].value1 = $(this).find('input').val()? "'"+ $(this).find('input').val().replace(/,/g, "','") +"'":"";
			}
		});
		THIS.sankeyConfig = config;
		var configRequest = {
			sankeyFilterMap:config.sankeyFilterMap	
		};
		configRequest.nodeList = config.nodeList.filter(function(obj){
			return $("[name ='nodecheckbox"+obj.nodeindex+"']").is(":checked");
		});
		var includedNode = configRequest.nodeList.map(function(obj){return obj.nodeindex});
		configRequest.sankeyLinkList = config.sankeyLinkList.filter(function(obj){return includedNode.indexOf(parseInt(obj.fromStack))>-1 && includedNode.indexOf(parseInt(obj.toStack))>-1});			
		if(configRequest.sankeyLinkList && configRequest.sankeyLinkList.length>0){
			var limitCount = $("#sankeyslider").val() || 20,
			orderType = $("#sankeyordertype").is(":checked")?"ASC":"DESC";
			for(var i=0;i<configRequest.sankeyLinkList.length;i++){
				if(configRequest.sankeyLinkList[i].query.indexOf("<LIMIT>")){
					configRequest.sankeyLinkList[i].query = configRequest.sankeyLinkList[i].query.replace("<LIMIT>",limitCount);
				}
				if(configRequest.sankeyLinkList[i].query.indexOf("<ORDERTYPE>")){
					configRequest.sankeyLinkList[i].query = configRequest.sankeyLinkList[i].query.replace("<ORDERTYPE>",orderType);
				}
			}
		}
		var request = {
			source:THIS.sankeyConfig.source,
			database:THIS.sankeyConfig.database
		};
		request.config = JSON.stringify(configRequest);
		enableLoading();
		callAjaxService("getSankeyJSON",  function(response){callBackGetSankeyJSON(response,configRequest.nodeList);$('#filterModal').modal('hide');},
				callBackFailure, request, "POST",null,true);
	};
	var enableSankeyZoom= function(){
		if ($('#sankeyZoomOnOffswitch').is(":checked")){
			sankeyZoomForward = PanZoomsvg('materialflow');
			sankeyZoomBackward = PanZoomsvg('materialflow');
			if (sankeyZoomForward && sankeyZoomForward != null){
				sankeyZoomForward.enableZoom();
				sankeyZoomForward.enablePan();
			}
			if (sankeyZoomBackward && sankeyZoomBackward != null){
				sankeyZoomBackward.enableZoom();
				sankeyZoomBackward.enablePan();
			}
		} else{
			if (sankeyZoomForward && sankeyZoomForward != null){
				sankeyZoomForward.disableZoom();
				sankeyZoomForward.disablePan();
			}
			if (sankeyZoomBackward && sankeyZoomBackward != null){
				sankeyZoomBackward.disableZoom();
				sankeyZoomBackward.disablePan();
			}
		}
	}
	var callBackSaveSankey = function(response){
		disableLoading();
		$('#myModalthredlevel').modal('hide');
		showNotification("success","Sankey saved successfully..");
		$("#save").data("refId",response);
		$("#saveAs").show();
	};
	var getMySankey = function(obj){
		$('#mySankey').hide();
		$('#myprofilediv').show();
		$('#allprofilediv').hide();
		$('#selectTable').hide();
		var requiredCol = [ {
			"sWidth": '13%',
			"mDataProp" : "displayname",
			"title":"Name",
			"sDefaultContent" : ""
		},{
			"mDataProp" : "profilekey",
			"sWidth": '13%',
			"title":"Action",
			"sDefaultContent" : ""
		}, {
			"mDataProp" : "source",
			"sWidth": '13%',
			"title":"Source",
			"sDefaultContent" : ""
		}, {
			"mDataProp" : "database",
			"sWidth": '13%',
			"title":"Data Base",
			"sDefaultContent" : ""
		}, {
			"mDataProp" : "lastmodifiedby",
			"sWidth": '12%',
			"title":"Modified By",
			"sDefaultContent" : ""
		}, {
			"mDataProp" : "lastmodifiedon",
			"sWidth": '13%',
			"title":"Modified On",
			"sDefaultContent" : ""
		}];
		var profileParam = {
				"getAll" : true,
				"profileType" : "sankey",
		        "columns" : requiredCol,
		        "isHistory" : false,
		        "isPublish" : true
		};
		enableLoading();
		var profileUtitlyObj = new profileUtiltyClass(profileParam,function(e){mySankeySelect(e,this);});
		profileUtitlyObj.getMyProfile();
	};
	
	var mySankeySelect = function(obj){
		var id = $(obj).data("refid");
		var request = {
				"id":id
		};
		enableLoading();
		callAjaxService("getProfileConfig", callBackGetSankeyConfig,
				callBackFailure, request, "POST");
	};
	var callBackGetSankeyConfig = function(response){
		if (response && response.isException){
			showNotification("error",response.customMessage);
			return;
		}
		if(response !== undefined && response !== null){
			disableLoading();
			$("#selectTable").hide();
			$("#mySankey").show(300);
			$("#saveAs").show();
			$("#myProfiles").hide(300);
			var data = Jsonparse(response.config);
			if(data && data.length > 0)
				data = data[0];
			$("#profileBack").parents("header").find("h2").html("Sankey View : "+data.displayname||'');
			$("#save").data("remarks",data["remarks"]);
			$("#save").data("displayname",data["displayname"]);
			$("#save").data("refId",data["id"]);
			var config = data["config"].replace(/@/g, "'");
			config = Jsonparse(config);
			//config = config["sankeyConfigList"];
			$("#source").select2('val',data["source"]);
			sourceSelect(data["source"],data["database"]);
			THIS.source = data["source"];
			THIS.database = data["database"];
			//$("#database").select2('val',data["database"]);
			//$("#stackCount").select2('val',data["stackcount"]);
			$("#configSection,#filterConfigSection").empty().show();
			var configSection = $("#configSection");
			$("#configSection").append(configSection);
			$('#hideMinified').prop('checked', false);
			if(data.additional_config){
				var additionalconfig = JSON.parse(data.additional_config);
				if(additionalconfig && additionalconfig.isFilterInline){
					$("#sankeyOnOffswitch").prop('checked', true);
				}
				if(additionalconfig && additionalconfig.isMinified){
					if(additionalconfig.isMinified === true){
						$("body").addClass('minified');
						$('#hideMinified').prop('checked', true);
						}
				}
				
			}
			if(config){
				if(config.sankeyLinkList && config.sankeyLinkList.length>0){
					var stackColors = {};
					addNodeConfig(config.nodeList);
					for (var j=0;j<config.sankeyLinkList.length;j++){
						addConfig(j,config.sankeyLinkList[j]);
						stackColors[config.sankeyLinkList[j].fromStack] = config.sankeyLinkList[j].fromStackColor;
						stackColors[config.sankeyLinkList[j].toStack] = config.sankeyLinkList[j].toStackColor;
					}
					
				}
				if(config.sankeyFilterMap && Object.keys(config.sankeyFilterMap).length>0){
					var i = 0; 
					$.each(config.sankeyFilterMap, function(index, value) {
						addFilterConfig(i++,config.sankeyFilterMap[index]);
					});
				}
				$("#generalSection").find("[name]").val("");
				if(config.general){
					$("#generalSection").find("[name]").each(function(){
						if(config.general[$(this).attr("name")]){
							$(this).val(config.general[$(this).attr("name")]);
						}
					});
				}
			}
			
		}
	};
	var constructGridOnClickSankey = function(obj,type){
	    $("#dataTableDiv").show();
		var config = {
				html: '#sankeyDataGrid',
				url: serviceCallMethodNames["getDataPagination"].url,
				source:THIS.sankeyConfig.source,
				filter:'1=1',
				database:THIS.sankeyConfig.database
			};
		if(type == 'link'){
			for(var i=0;i<THIS.sankeyConfig.sankeyLinkList.length;i++){
				if(THIS.sankeyConfig.sankeyLinkList[i].fromStack == obj.source.stack && THIS.sankeyConfig.sankeyLinkList[i].toStack == obj.target.stack){
					if(THIS.sankeyConfig.sankeyLinkList[i].filterKeyList && THIS.sankeyConfig.sankeyLinkList[i].filterKeyList.length>0){
						for(var j=0;j<THIS.sankeyConfig.sankeyLinkList[i].filterKeyList.length;j++){
							if(THIS.sankeyConfig.sankeyFilterMap[THIS.sankeyConfig.sankeyLinkList[i].filterKeyList[j]].value1 && THIS.sankeyConfig.sankeyFilterMap[THIS.sankeyConfig.sankeyLinkList[i].filterKeyList[j]].value1 && THIS.sankeyConfig.sankeyFilterMap[THIS.sankeyConfig.sankeyLinkList[i].filterKeyList[j]].value1 != null && $.trim(THIS.sankeyConfig.sankeyFilterMap[THIS.sankeyConfig.sankeyLinkList[i].filterKeyList[j]].value1)!= ""){								
								config.filter += config.filter?' and ':'';
								if(THIS.sankeyConfig.sankeyFilterMap[THIS.sankeyConfig.sankeyLinkList[i].filterKeyList[j]].searchField.indexOf("<Q>")>-1){
									config.filter+= THIS.sankeyConfig.sankeyFilterMap[THIS.sankeyConfig.sankeyLinkList[i].filterKeyList[j]].searchField
									.replace("<Q>",THIS.sankeyConfig.sankeyFilterMap[THIS.sankeyConfig.sankeyLinkList[i].filterKeyList[j]].value1);
								}else{
									config.filter+= THIS.sankeyConfig.sankeyFilterMap[THIS.sankeyConfig.sankeyLinkList[i].filterKeyList[j]].searchField +' in ('
									+THIS.sankeyConfig.sankeyFilterMap[THIS.sankeyConfig.sankeyLinkList[i].filterKeyList[j]].value1+')';
									if(THIS.sankeyConfig.sankeyFilterMap[THIS.sankeyConfig.sankeyLinkList[i].filterKeyList[j]].value2){
										config.filter += config.filter?' and ':'';
										config.filter+= THIS.sankeyConfig.sankeyFilterMap[THIS.sankeyConfig.sankeyLinkList[i].filterKeyList[j]].searchField +' in ('
										+THIS.sankeyConfig.sankeyFilterMap[THIS.sankeyConfig.sankeyLinkList[i].filterKeyList[j]].value2+')';
									}
								}
							}
						}
					}
					if(THIS.sankeyConfig.sankeyLinkList[i].columninfo){
						config.columninfo = true;
					}
					if(THIS.sankeyConfig.sankeyLinkList[i].clickquery){
						config.filter += config.filter?' and ':'';
						config.filter += THIS.sankeyConfig.sankeyLinkList[i].clickquery.replace("<F>", obj.source.name).replace("<T>", obj.target.name);
					}
					if(THIS.sankeyConfig.sankeyLinkList[i].groupby){
						config.groupByEnabled = true;
					}
					config.table = THIS.sankeyConfig.sankeyLinkList[i].table;
					config.columns = THIS.sankeyConfig.sankeyLinkList[i].columns;
					config.dataColumns = $.map(THIS.sankeyConfig.sankeyLinkList[i].columns, function(val) { return val.data; }).join(',');					
					break;
				}
			}
		}else{
			for(var i=0;i<THIS.sankeyConfig.nodeList.length;i++){
				if(obj.stack == i+1){
					if(THIS.sankeyConfig.sankeyFilterMap){
						Object.keys(THIS.sankeyConfig.sankeyFilterMap).forEach(function(key) {
							if(THIS.sankeyConfig.sankeyFilterMap[key].links && THIS.sankeyConfig.sankeyFilterMap[key].links.length>0){
								for(var s=0;s<THIS.sankeyConfig.sankeyFilterMap[key].links.length;s++){
									var str = THIS.sankeyConfig.sankeyFilterMap[key].links[s];
									if((str.substr(str.indexOf(':')+1,str.length).indexOf(obj.stack))>-1 && THIS.sankeyConfig.sankeyFilterMap[key].value1 && THIS.sankeyConfig.sankeyFilterMap[key].value1!= null && $.trim(THIS.sankeyConfig.sankeyFilterMap[key].value1)!= ""){
										config.filter += config.filter?' and ':'';
										config.filter+= THIS.sankeyConfig.sankeyFilterMap[key].searchField +' in ('
										+THIS.sankeyConfig.sankeyFilterMap[key].value1+')';
										if(THIS.sankeyConfig.sankeyFilterMap[key].value2){
											config.filter += config.filter?' and ':'';
											config.filter+= THIS.sankeyConfig.sankeyFilterMap[key].searchField +' in ('
											+THIS.sankeyConfig.sankeyFilterMap[key].value2+')';
										}
									}
								}
							}
						});
					}
					if(THIS.sankeyConfig.nodeList[i].clickquery){
						var strings = THIS.sankeyConfig.nodeList[i].clickquery.split(/and|or /);
						for(var s=0;s<strings.length;s++){
							if(strings[s].indexOf("()")== -1 && strings[s].indexOf("('')")== -1 && strings[s].indexOf("('<>')")== -1){
								config.filter += config.filter?' and ':'';
								if(strings[s].indexOf('<Q>') > -1){
									config.filter += strings[s].replace("<Q>", obj.name);
								}else{
									config.filter += strings[s];
								}
							}							
						}
					}
					if(THIS.sankeyConfig.nodeList[i].groupby){
						config.groupByEnabled = true;
					}
					config.table = THIS.sankeyConfig.nodeList[i].table;
					config.columns = THIS.sankeyConfig.nodeList[i].columns;
					config.dataColumns = $.map(THIS.sankeyConfig.nodeList[i].columns, function( val) { return val.data; }).join(',');
					break;
				}
			}			
		}
		if(config.table){
			if($.fn.DataTable.isDataTable('#sankeyDataGrid')){
				$("#sankeyDataGrid").DataTable().destroy();
				$("#sankeyDataGrid").empty();				
			}
			constructGrid(config);
			$('#downloadExcel').parent().show();
			 $("#downloadExcel,#downloadCSV").unbind("click").bind("click",function(e){
				 downloadExcelOrCsv(config,e);
			 });
		}
	};
	var downloadExcelOrCsv = function(config,event){
		var tableSorting = $($(event.currentTarget).attr("data-datatable")).dataTable().fnSettings().aaSorting;
		var columnObj  = config.columns;
		var columns = "";
		for (var col=0;col< columnObj.length ; col++){
			if (col==0)
				columns += columnObj[col].data + " as '" +   columnObj[col].title+"'";
			else
				columns += ","+columnObj[col].data + " as '" +   columnObj[col].title+"'";
		};
		var request = {
				"columns":columns,
				"databaseColumns":config.dataColumns,
				"orderField":tableSorting[0][0],
				"orderType":tableSorting[0][1],
				"searchValue":$('.dataTables_filter input').val(),
				"source":config.source,
				"database":config.database,
				"table":config.table,
				"filter":localStorage["datatablefilter"],
				"displayName":$("#save").data("displayname")|| THIS.displayName
		};
		if($(event.currentTarget).attr('id') == 'downloadExcel'){
			$.fileDownload('rest/dataAnalyser/datatableExcelDownload?request='+JSON.stringify(request));
		}else{
			$.fileDownload('rest/dataAnalyser/datatableCSVDownload?request='+JSON.stringify(request));
		}
			
	};
	var callBackGetSankeyJSON = function(energy,nodeList){
		var nodeDiv,linkDiv,legendDiv = $('<div/>'),nodeimage,stackCount;
		$("#sankeynodeimagediv").empty();
		$("#sankey").html("");
		if(!nodeList || nodeList.length === 0){
			nodeList = THIS.sankeyConfig.nodeList.filter(function(obj){return obj.included === true});
		}
		var includedNode = nodeList.map(function(obj){return obj.nodeindex});
		var sankeyLinkList = THIS.sankeyConfig.sankeyLinkList.filter(function(obj){return includedNode.indexOf(parseInt(obj.fromStack))>-1 && includedNode.indexOf(parseInt(obj.toStack))>-1});
		if(nodeList.length>0){
			stackCount = nodeList.length;
			var generalConfig = {};
			if(THIS.sankeyConfig.general){
				generalConfig = THIS.sankeyConfig.general;
			}
			var sankeyWidth = (generalConfig.sankeywidth?parseInt(generalConfig.sankeywidth): $("#content").width()) -(nodeList.length*5);
			nodeDiv = $('<div/>');
			$("#sankey").siblings().css({"width":sankeyWidth+100});
			for(var i=0;i<nodeList.length;i++){
				nodeimage ="&nbsp;";
				if(nodeList[i].legend){
					nodeDiv.append($('<label/>').addClass('padding-10')
							.append($('<i/>').addClass('fa fa-square').css({'margin-right':'5px','color':nodeList[i].colorCode}))
									  .append($('<span/>').html(nodeList[i].legend))).appendTo(legendDiv);
				}
				if(nodeList[i].imagepath){
					nodeimage = $("<img/>").addClass("img-responsive img-thumbnail").css({"border-width":"2px","border-color":nodeList[i].colorCode,"margin-bottom":"5px","height":"100px"})//,"margin-left":i>0?((100/THIS.sankeyConfig.nodeList.length)+i*5)+"%":"10%"
					.attr({"width":"100","src":nodeList[i].imagepath});
				}
				var imagewidth = i === nodeList.length-1?(sankeyWidth/(nodeList.length*2)):
					(sankeyWidth - sankeyWidth/(nodeList.length*2))/(nodeList.length-1);
				$("#sankeynodeimagediv").append($("<span/>")
						.css({"width":imagewidth+"px","float":"left"})//.addClass("col-md-3 col-md-15") img/smartFind/clinicalSpeciality/cmf.png
						.append(nodeimage));				
			}
		}	
		if(sankeyLinkList.length>0){
			linkDiv = $('<div/>');
			for(var i=0;i<sankeyLinkList.length;i++){
				if(sankeyLinkList[i].legendName){
					linkDiv.append($('<label/>').addClass('padding-10')
					.append($('<i/>').addClass('fa fa-square').css({'margin-right':'5px','color':sankeyLinkList[i].linkColor}))
							  .append($('<span/>').html(sankeyLinkList[i].legendName))).appendTo(legendDiv);
				}
			}
		}
		$('#sankeyZoomOnOffswitch').change(function(){
			enableSankeyZoom();
		}).parent().parent().show();		
		disableLoading();
		
		$('#sankeyProfile').show();
		if (energy && energy.isException){
			showNotification("error",energy.customMessage);
			return;
		}
		if (!energy.nodes || energy.nodes.length == 0) {
			$("#sankey").html('<div class="alert alert-danger fade in"><a class="close" data-dismiss="alert" href="#">×</a><h4 class="alert-heading"><i class="fa fa-exclamation-triangle"></i>&nbsp;OOPS!</h4><p class="text-align-left">No matching results found for selected filters.</p></div>');
			$("#sankey").show();
			return;
		}else{
			$("#sankey").empty();
		}
		drawSankey(energy,stackCount,legendDiv);
		// callAjaxService("getE2EMF",request,callBackSuccessgetE2EMF,
		// callBackfailuregetE2EMF);
		disableLoading();
		
	};
	
	function drawSankey(energy,stackCount,legendDiv){
		function dragmove(d) {
			// d3.select(this).attr("transform", "translate(" + d.x + "," + (d.y
			// =
			// Math.max(0, Math.min(height - d.dy, d3.event.y))) + ")");
			d3.select(this).attr(
					"transform",
					"translate("
							+ (d.x = Math.max(0, Math.min(width - d.dx,
									d3.event.x)))
							+ ","
							+ (d.y = Math.max(0, Math.min(height - d.dy,
									d3.event.y))) + ")");

			sankey.relayout();
			link.attr("d", function(d) {
				var x1;
	            if(d.source.x<d.target.x)
	            	x1 = d.target.x-Math.max(1, d.dy/2);
	            else{
	            	x1 = d.target.x+Math.max(1, d.dy/2)+35;
	            }	
		        var x0 = d.source.x + d.source.dx,
		            xi = d3.interpolateNumber(x0, x1),
		            x2 = xi(0.5),
		            x3 = xi(1 - 0.5),
		            y0 = d.source.y + d.sy + d.dy / 2,
		            y1 = d.target.y + d.ty + d.dy / 2;
		        return "M" + x0 + "," + y0
		                    + "C" + x2 + "," + y0
		                    + " " + x3 + "," + y1
		                    + " " + x1 + "," + y1;
			});
		}
		var generalConfig = {};
		if(THIS.sankeyConfig.general){
			generalConfig = THIS.sankeyConfig.general;
		}
		var margin = {
				top : $("#sankey").siblings().find("img").length > 0?0:80,
				right : 70,
				bottom : 50,
				left : 20
			}, width = $("#content").width() - margin.left - margin.right, height = generalConfig.containerheight?parseInt(generalConfig.containerheight):500;
					- margin.top - margin.bottom;
			// console.log("content width " + $("#content").width());
			var formatNumber = d3.format(",.0f"), format = function(d) {
				return formatNumber(d);
			}, color = d3.scale.category20();
		$("#sankey").css({"max-width":$("#content").width(),"overflow":"auto","max-height":"800px"});
		var svg = d3.select("#sankey").append("svg").attr("id", "materialflow")
				.attr("width", (generalConfig.sankeywidth?parseInt(generalConfig.sankeywidth):width) + margin.left + margin.right).attr(
						"height", height+ margin.top + margin.bottom).append(
						"g").attr("transform",
						"translate(" + margin.left + "," + margin.top + ")");
		var sankey = d3.sankey().nodeWidth(35).nodePadding(10).sinksRight(true).size(
				[generalConfig.sankeywidth?parseInt(generalConfig.sankeywidth):width,generalConfig.sankeyheight?parseInt(generalConfig.sankeyheight):400 ])
				.fullwidth(generalConfig.sankeywidth?parseInt(generalConfig.sankeywidth):width).totalstacks(stackCount);
		var nodeMap = {};
		energy.nodes.forEach(function(x) {
			x.suffix = THIS.sankeyConfig.nodeList[x.stack-1].suffix;
			x.prefix = THIS.sankeyConfig.nodeList[x.stack-1].prefix;
			nodeMap[x.name] = x;
		});
		energy.links = energy.links.map(function(x) {
			return {
				source : nodeMap[x.source],
				target : nodeMap[x.target],
				value : x.value,
				color : x.color,
				text : x.text,
				sourcename : x.sourcename,
				targetname : x.targetname,
				linkValueSuffix : x.linkValueSuffix,
				linkValuePrefix : x.linkValuePrefix,
				linkToolTip:parseFloat(x.linkToolTip) ?parseFloat(x.linkToolTip):'' 
			};
		});
		sankey.nodes(energy.nodes).links(energy.links).layout(32);
		var path = sankey.link().curvature(0.15);
		var link = svg
				.append("g")
				.selectAll(".link")
				.data(energy.links)
				.enter()
				.append("path")
				.attr("class", "link")
				.attr("d", function(d) {
					var x1;
		            if(d.source.x<d.target.x)
		            	x1 = d.target.x-Math.max(1, d.dy/2);
		            else{
		            	x1 = d.target.x+Math.max(1, d.dy/2)+35;
		            }	
			        var x0 = d.source.x + d.source.dx,
			            xi = d3.interpolateNumber(x0, x1),
			            x2 = xi(0.5),
			            x3 = xi(1 - 0.5),
			            y0 = d.source.y + d.sy + d.dy / 2,
			            y1 = d.target.y + d.ty + d.dy / 2;
			        return "M" + x0 + "," + y0
			                    + "C" + x2 + "," + y0
			                    + " " + x3 + "," + y1 
			                    + " " + x1 + "," + y1;
				})
				.on("click",
						function(d) {
					        $('#resetLink').show();
                            $('path').css({'stroke-opacity':'0.2'})
							$(this).css({'stroke-opacity':'1'});	
							constructGridOnClickSankey(d,'link');
						}).style("stroke-width", function(d) {
					return Math.max(1, d.dy);
				}).style("stroke", function(d) {
					return d.color;
				}).style("marker-end", function (d) {
				      return 'url(#arrowHead'+(d.source.stack)+')';
			    });
		var tip = d3.tip().offset([-8, 0]).attr('class', 'd3-tip').html(function(d){
			var toolTip= $("<div/>");
			var value =	 format(d.value) ,toolTipValue = d.linkToolTip>0?format(d.linkToolTip):value;
			var sourceDescription = d.sourcename?"-"+d.sourcename:"";
			var iconClass = "fa fa-long-arrow-left";
			if(d.source.x<d.target.x){
				iconClass = "fa fa-long-arrow-right";
			}
			var targetDescription = d.targetname?"-"+d.targetname:"";
			   toolTip.append($('<table/>').attr({"class":"d3-tip-table"}).append( $('<tr/>')
					   .append($('<td/>').html("From :"+d.source.name +sourceDescription).attr({"class":"d3-tip-key"})))
					   .append($('<tr/>').append($('<td/>').html("To :"+d.target.name +targetDescription).attr({"class":"d3-tip-key"}))
					  ).append($('<tr/>').append( $('<td/>').html('Value : ' + (d.linkValuePrefix ||"") + toolTipValue + (d.linkValueSuffix ||"")).attr({"class":"font-bold"}))));
			   return toolTip[0].outerHTML;
		});
		d3.selectAll("#materialflow").selectAll(".link").call(tip);
		d3.selectAll("#materialflow").selectAll(".link").on('mouseover', tip.show)
                .on('mouseout', tip.hide);		
		var node = svg.append("g").selectAll(".node").data(energy.nodes)
				.enter().append("g").attr("class", "node").attr("transform",
						function(d) {
							return "translate(" + d.x + "," + d.y + ")";
						}).on("click", function(d,i) {
							constructGridOnClickSankey(d,'node');
						}).call(d3.behavior.drag().origin(function(d) {
					return d;
				}).on("dragstart", function() {
					this.parentNode.appendChild(this);
				}).on("drag", dragmove));
		
		node.append("rect").attr("height", function(d) {
			return d.dy;
		}).attr("width", sankey.nodeWidth()).style("fill", function(d) {
			return d.color;
		}).style("stroke", function(d) {
			return d3.rgb(d.color).darker(2);
		}).append("title").text(function(d) {
		var totalValue=0,totalSource=0,totalTarget=0;
		if(d.sourceLinks[0] !=undefined && d.sourceLinks[0].linkToolTip !='' && d.sourceLinks[0].linkToolTip !=undefined && d.sourceLinks[0].linkToolTip !=null){
		for(var i=0;i< d.sourceLinks.length;i++){
			
				totalSource += d.sourceLinks[i].linkToolTip;
			}
		}
		if(d.targetLinks[0] !=undefined && d.targetLinks[0].linkToolTip !='' && d.targetLinks[0].linkToolTip !=undefined && d.targetLinks[0].linkToolTip !=null){
		for(var i=0;i< d.targetLinks.length;i++){
			
				totalTarget += d.targetLinks[i].linkToolTip;
			}
		}
		if(totalTarget>totalSource){
			totalValue=totalTarget;
		}else if(totalSource>totalTarget){
			totalValue=totalSource;
		}else{
			totalValue=d.value;
		}
		
			var value= (d.prefix || "")+ format(totalValue) + (d.suffix || "");
			return d.name + ' - ' + d.text + ' - ' + value;
		});

		node.append("text").attr("x", -6).attr("y", function(d) {
			if (d.dy == 0) {
				return 0;
			}
			//var textvalue = Math.pow(2,d.dy);  
			return d.dy / 2;
		}).attr("dy", ".35em").attr("text-anchor", "end").text(function(d) {
			return d.name;
		}).filter(function(d) {
			return d.x < width / 2;
		}).attr("x", 6 + sankey.nodeWidth()).attr("text-anchor", "start");

		var defs = svg.append("defs");
		for(var i=0;i<THIS.sankeyConfig.sankeyLinkList.length;i++){
			defs.append("marker")
			  .style("fill", THIS.sankeyConfig.sankeyLinkList[i].fromStackColor)
			  .attr("id", "arrowHead"+THIS.sankeyConfig.sankeyLinkList[i].fromStack)
			  .attr("viewBox", "0 0 6 10")
			  .attr("refX", "1")
			  .attr("refY", "5")
			  .attr("markerUnits", "strokeWidth")
			  .attr("markerWidth", "1")
			  .attr("markerHeight", "1")
			  .attr("orient", "auto")
			  .append("path")
			    .attr("d", "M 0 0 L 1 0 L 6 5 L 1 10 L 0 10 z");
		}
		$('rect').tipsy({
			gravity : $.fn.tipsy.autoNS,
			html : true,
			title : function() {
				var nodename = $(this).text();
				return nodename;
			}
		});
		
		$('path').tipsy({
			gravity : $.fn.tipsy.autoNS,
			html : true,
			title : function() {
				var pathflow = $(this).text();
				return pathflow;
			}
		});
		$("#sankeyLegend").html(legendDiv);
	}
	
	$('#resetLink').unbind('click').bind('click', function() {
	$('path').css({'stroke-opacity':'1'});
	$('#resetLink').hide();
});
	init();	
};
sankeyGenerator();