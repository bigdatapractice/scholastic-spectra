'use strict';
pageSetUp();
var SOReportInstance = function() {
	SOReportObject = new SOReportObject();
	SOReportObject.Load();
};
var SOReportObject = function(){
	var THIS = this;
	$("#btnPrintL3").click(function(e){
    	printElement('#poDocView');
    });
    $("#btnSaveL3").click(function(e){
        savePDF('#poDocView');
    });

	this.Load = function(source) {
		enableLoading();
		source = source || "SAP";
		//var request = "viewType=soTypes&source="+source
		var request = {
			"source" : source,
			"viewType" : "materialreport"
		};
		callAjaxService("MaterialReportPost", SOReportPostSuccessCallback, callBackFailure,
				request, "POST");
	};
	
   this.ExportToExcel = function(){
		
	   $.fileDownload('rest/material/materialreportexcel');
	  
	};

	var SOReportPostSuccessCallback = function(data){
		disableLoading();
		if (data && data.isException){
			showNotification("error",data.customMessage);
			return;
		}
		var numberFormat2 = d3.format(",.2f");
		if(data !== undefined 
			&& data !== null 
			&& data['aggregatedMaterialReportList'] !==undefined 
			&& data['aggregatedMaterialReportList'] !== null){

			data = data['aggregatedMaterialReportList'];

			THIS.dataTableObj = $("#meterialTable")
					.DataTable(
							{
								"sDom": "frtiS",
								"sScrollY": "300px",
								"bDeferRender": true,
								"aaData" : data,
								 "iDisplayLength":100,
								"aoColumns" : [{											
										
											"mData" : "materialnumber",
											"sDefaultContent" : ""
										},{

											"mData" : "materialdescription",
											"sDefaultContent" : ""
										},{

											"mData" : "transactiontype",
											"sDefaultContent" : ""
										},{

											"mData" : "materialtype",
											"sDefaultContent" : ""
										}, {
											"mDataProp" : "netprice",
											"sDefaultContent" : "",
											"mRender" : function(data, type,
													full) {
												return numberFormat2(data);
											},
											"sClass" : "numbercolumn"
										}]
							});
			//callAjaxService("ExcelDownload", ExcelDownloadSuccessCallback, callBackFailure,null, "GET");
						
			
		}
	}
	var ExcelDownloadSuccessCallback = function(){
		
	};

};

SOReportInstance();