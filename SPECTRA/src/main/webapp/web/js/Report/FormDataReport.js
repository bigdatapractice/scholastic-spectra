'use strict';
pageSetUp();
var formDataReport;
var FormDataReportInstance = function() {
	formDataReport = new formDataReportObject();
	formDataReport.Load();
};
var formDataReportObject = function(){
	var THIS = this;
	 $("#btnCreate").unbind("click").bind("click", function(){formDataReport.Create();});
	 $("#frmSankeyFilter").submit(function(e) {
		    e.preventDefault();
		});
	 var $registerForm = $("#frmSankeyFilter").validate({

			// Rules for form validation
			rules : {
				propertyName : {
					required : true
				},
				obfType : {
					required: true
				}
			},

			// Messages for form validation
			messages : {
				login : {
					required : 'Please enter your login'
				},
				obfType : {
					required : 'Please select your Role'
				}
			},
			errorPlacement : function(error, element) {
				error.insertAfter(element.parent());
			}
		});
	this.Load = function(source) {
		enableLoading();
		var request = {
			//"source" : source,
			//"viewType" : "woTypes"
		};
		callAjaxService("FormDataReportPost", FormDataReportCallback, callBackFailure,
				request, "POST");
	};

	var FormDataReportCallback = function(data){
		disableLoading();
		if (data && data.isException){
			showNotification("error",data.customMessage);
			return;
		}
		var deleteDiv = $("#deleteDiv").html();
		if(data !== undefined 
			&& data !== null ){
			var dataTableData = data;
			var dataTable = $(".dc-data-table")
			.DataTable(
					{

						"dom":"<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-3 col-xs-12 hidden-xs'l><'col-sm-3 col-xs-12 hidden-xs'C>r>"+
						"t"+
						"<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
						"processing" : true,
						"lengthChange" : true,
						"bSort" : true,
						"info" : true,
						"jQueryUI" : false,
						"scrollX" : true,
						"data" : dataTableData,
						 "bAutoWidth": false,
						"tableTools" : {
							"aButtons" : [ {
								"sExtends" : "collection",
								"sButtonText" : 'Export <span class="caret" />',
								"aButtons" : [ "csv", "pdf" ]
							} ],
							"sSwfPath" : "js/plugin/datatables/media/swf/copy_csv_xls_pdf.swf"
						},
						"destroy" : true,
						"columns" : [{											
							
								"data" : "name",
								"defaultContent" : ""
							},{

								"data" : "isObfuscate",
								"defaultContent" : ""
							},{
								"data" : "isDeObfuscate",
								"defaultContent" : ""
							},{

								"data" : "remarks",
								"defaultContent" : ""
							},
							{
								"data" : "obfType",
								"defaultContent" : ""
							}	,{
								"data" : "name",
								"defaultContent" :"",
								"render" : function(data, type,
										full) {
									return deleteDiv.replace("$","'"+data+"'").replace(/#/g,"formDataReport");
								}}],
						"rowCallback" : function(nRow, aData,
								iDisplayIndex, iDisplayIndexFull) {
								$(nRow).attr({
									'data-target' : '#myModalthredlevel',
									'data-toggle' : 'modal'
								}).unbind('click').bind('click', aData,
										formDataReport.Edit);
						}
					});
		}
	};
	this.Create = function(){
		if (!$("#frmSankeyFilter").valid())
			return;
		var obfuscationStatus =0;
		if ($("#obfusecationStatus").is(':checked'))
			obfuscationStatus = 1;
		var deobfuscationStatus =0;
		if ($("#deobfusecationStatus").is(':checked'))
			deobfuscationStatus = 1;
		var request = {
				"propertyname":$("[name=propertyName]").val(),
				"obfstatus": obfuscationStatus,
				"deobfstatus": deobfuscationStatus,
				"remarks": $("[name=remarks]").val(),
				"obftype": $("[name=obfType]").val()
		};
		callAjaxService("addformdata",callbackSucessCreateFormData,callBackFailure,request,"POST");
	};
	
	var callbackSucessCreateFormData = function(response){
		if (response && response.isException){
			showNotification("error",response.customMessage);
			return;
		} else {
			$('#myModalthredlevel').modal('hide');
			$("#smart-form-register input").val("");
			$("#type").select2("val",null);
			THIS.Load();
			showNotification("success","Form Data Added Sucessfully..");
		};
	};
	this.CreateView = function(){
		$("#searchForm").submit(function(e) {
		    e.preventDefault();
		});
		$("[name=propertyName]").removeAttr("readonly");
		$("#smart-form-register input").val("");
		$("#smart-form-register textarea").val("");
		$("#btnCreate").html("").append($("<i/>").addClass("fa fa-plus")).append(" Create").unbind('click').bind('click',function(){formDataReport.Create();});
		$("#frmSankeyFilter em").remove();
		$(".state-error").removeClass("state-error");
		$("#smart-form-register input[type=checkbox]").removeAttr("checked");
		$("#obfType").val(0);
	};
	this.Edit = function(dataObj){
		dataObj = dataObj.data;
		$("#smart-form-register input").val("");
		$("#btnCreate").html("").append($("<i/>").addClass("fa fa-pencil")).append(" Update").unbind('click').bind('click',function(){formDataReport.Update();});
		$("#frmSankeyFilter em").remove();
		$(".state-error").removeClass("state-error");
		$("[name=propertyName]").val(dataObj.name).attr("readonly","true");
		
		if (dataObj.isObfuscate == true || dataObj.isObfuscate == "true"){
			$("#obfusecationStatus").prop('checked', true).attr("checked","true");
		}
		else {
			$("#obfusecationStatus").removeAttr("checked");
		}
		if (dataObj.isDeObfuscate == true || dataObj.isDeObfuscate == "true"){
			$("#deobfusecationStatus").prop('checked', true).attr("checked","true");
		}
		else{ 
			$("#deobfusecationStatus").removeAttr("checked");
		}
		$("[name=remarks]").val(dataObj.remarks);
		$("[name=obfType]").val(dataObj.obfType);
	};
	
	this.Update = function(){
	
		if (!$("#frmSankeyFilter").valid())
			return;
		var obfuscationStatus =0;
		if ($("#obfusecationStatus").is(':checked'))
			obfuscationStatus = 1;
		var deobfuscationStatus =0;
		if ($("#deobfusecationStatus").is(':checked'))
			deobfuscationStatus = 1;
		var request = {
				"propertyname":$("[name=propertyName]").val(),
				"obfstatus": obfuscationStatus,
				"deobfstatus": deobfuscationStatus,
				"remarks": $("[name=remarks]").val(),
				"obftype": $("[name=obfType]").val()
		};
		callAjaxService("UpdateFormDataPost",callbackSucessUpdateFormData,callBackFailure,request,"POST");
	};
	var callbackSucessUpdateFormData = function(response){
		if (response && response.isException){
			showNotification("error",response.customMessage);
			return;
		} else {
			$('#myModalthredlevel').modal('hide');
			$("#smart-form-register input").val("");
			$("#type").select2("val",null);
			THIS.Load();
			showNotification("success","Form Data updated Sucessfully..");
		};
	};
	this.Delete = function(event,key){
		 event.stopPropagation();
		$.confirm({
		    text: "Are you sure you want to delete that Form Data",
		    confirm: function() {
		    	var request = {
						"propertyname":key
				};
				callAjaxService("deleteFormData",callbackDeleteFormData, callBackFailure,
						request, "POST");
		    },
		    cancel: function() {
		        // nothing to do
		    }
		});
	};
	this.CancelDelete = function(event){
		 event.stopPropagation();
		 $(".hidden-tablet.open").removeClass("open");
	};
	var callbackDeleteFormData = function(response){
		if (response && response.isException) {
			showNotification("error", response.customMessage);
			return;
		} else {
			THIS.Load();
			showNotification("success", "Form data Deleted Sucessfully..");
		}
	};
	this.SystemPropertiesRefresh = function() {
		callAjaxService("SystemPropertiesRefreshAction", callbackSucessSystemPropertiesRefreshAction, callBackFailure);
	};
	var callbackSucessSystemPropertiesRefreshAction = function(response) {
		if (response && response.isException) {
			showNotification("error", response.customMessage);
			return;
		} else {
			THIS.Load();
			showNotification("success", "System properties Refreshed Sucessfully.");
		}
	};
};
FormDataReportInstance();