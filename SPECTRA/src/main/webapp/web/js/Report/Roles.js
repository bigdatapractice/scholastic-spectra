'use strict';
pageSetUp();
var Roles;
var RolesInstance = function() {
	Roles = new RolesObject();
	Roles.Load();
};
var RolesObject = function(){
	var THIS = this;
	$("#btnCreate").unbind('click').bind('click', function() {
		Roles.Create();
	});
	
    var traceabilityValues = [ {
		"id" : "smart-style-2",
		"text" : "Default"
	}, {
		"id" : "smart-style-3",
		"text" : "Smart Admin"
	},{
		"id" : "smart-style-4",
		"text" : "Google Skin"
	},{
		"id" : "smart-style-1",
		"text" : "Dark Elegance"
	}, {
		"id" : "smart-style-8",
		"text" : "Ultra Light"
	} , {
		"id" : "smart-style-5",
		"text" : "Glass"
	}, {
		"id" : "smart-style-6",
		"text" : "PixelSmash"
	}, {
		"id" : "smart-style-7",
		"text" : "Material Design"
	} , {
		"id" : "smart-style-9",
		"text" : "Solenis Theme"
	}];
    
    $('#themeName').select2({
		data : traceabilityValues
	});
    
    var widgetClassValues = [{
    	"id" : "widget1x1",
    	"text" : "widget 1x1"
    },{
    	"id" : "widget2x2",
    	"text" : "widget 2x2"
    },{
    	"id" : "widget4x2",
    	"text" : "widget 4x2"
    }];
    $('#widgetClass').select2({
		data : widgetClassValues
	});
    
    var widgetColors = [{
    	"id" : "widget_darkred",
    	"text" : "Dark red"
    },{
    	"id" : "widget_blue",
    	"text" : "Blue"
    },{
    	"id" : "widget_red",
    	"text" : "Red"
    },{
    	"id" : "widget_darkblue",
    	"text" : "Dark blue"
    },{
    	"id" : "widget_green",
    	"text" : "Green"
    },{
    	"id" : "widget_darkgreen",
    	"text" : "Dark green"
    },{
    	"id" : "widget_yellow",
    	"text" : "Yellow" 
    },{
    	"id" : "widget_purple",
    	"text" : "Purple"
    },{
    	"id" : "widget_mareana_blue",
    	"text" : "Mareana Blue"
    }];
    $('#widgetColor').select2({
		data : widgetColors
	});
	var $registerForm = $("#frmSankeyFilter").validate({

		// Rules for form validation
		rules : {
			displayname : {
				required : true
			},
			name : {
				required: true
			},
			loginContent: {
				required: true
			},
			logo: {
				required: true
			},
			favicon: {
				required: true
			},
			displayOrder: {
				min: -1
			}
		},
		// Messages for form validation
		messages : {
			login : {
				displayname : 'Please enter display name'
			},
			name : {
				required : 'Please enter role'
			},
			loginContent:{
				required : 'Please login Content'
			},
			logo:{
				required : 'Please enter logo'
			},
			favicon:{
				required : 'Please enter favicon'
			}
		},

		// Do not change code below
		errorPlacement : function(error, element) {
			error.insertAfter(element.parent());
		}
	});
	$("#frmSankeyFilter").submit(function(e) {
	    e.preventDefault();
	});
		
	
	this.Load = function(source) {
		enableLoading();
		
		var request = {
			
		};
		callAjaxService("GetAllRoles", RolesCallback,  callBackFailure,
				request, "POST");
	};

	var RolesCallback = function(data){
		disableLoading();
		if (data && data.isException){
			showNotification("error",data.customMessage);
			return;
		}
		if(data !== undefined && data !== null ){
			var dataTableData = data;
			var dataTable = $(".dc-data-table")
			.DataTable(
					{

						"dom":"<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-3 col-xs-12 hidden-xs'l><'col-sm-3 col-xs-12 hidden-xs'C>r>"+
						"t"+
						"<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
						"processing" : true,
						"lengthChange" : true,
						"bSort" : true,
						"info" : true,
						"jQueryUI" : false,
						"scrollX" : true,
						"data" : dataTableData,
						 "bAutoWidth": false,
						"tableTools" : {
							"aButtons" : [ {
								"sExtends" : "collection",
								"sButtonText" : 'Export <span class="caret" />',
								"aButtons" : [ "csv", "pdf" ]
							} ],
							"sSwfPath" : "js/plugin/datatables/media/swf/copy_csv_xls_pdf.swf"
						},
						"destroy" : true,
						"columns" :[{											
							
							"data" : "displayName",
							"defaultContent" : ""
						},{											
							
							"data" : "name",
							"defaultContent" : ""
						},{
						
							"data" : "parentrole",
							"defaultContent" : ""
						},{
						
							"data" : "favicon",
							"defaultContent" : ""
						},{

							"data" : "logo",
							"defaultContent" : ""
						},{

							"data" : "themeName",
							"defaultContent" : ""
						},{

							"data" : "menuOnTop",
							"defaultContent" : ""
						},{
							"data" : "displayOrder",
							"defaultContent" : ""
							
						},{
							"data" : "widgetClass",
							"defaultContent" : ""
							
						},{
							"data" : "icon",
							"defaultContent" : ""
						},{
							"data" : "widgetColor",
							"defaultContent" : ""
						},{
							"data" : "fixedHeader",
							"defaultContent" : ""
						},{
							"data" : "fixedNavigation",
							"defaultContent" : ""
						},{
							"data" : "fixedRibbon",
							"defaultContent" : ""
						}
						/*{
							"data" : "name",
							"defaultContent" :"",
							"render" : function(data, type,
									full) {
								
							}
						}*/
						],
						"rowCallback" : function(nRow, aData,
								iDisplayIndex, iDisplayIndexFull) {
							$(nRow)
							.attr(
									{
										'onClick' : "javascript:Roles.Edit('"+ aData.name+ "')"
									});
						}
					});

		
		}
	};
	this.CreateView = function(){
		$("#smart-form-register input").val("");
		$("#widgetClass").select2("val",null);
		$("#widgetColor").select2("val",null);
		$("#themeName").select2("val",null);
		$("#loginContent").val("");
		$("#frmSankeyFilter").find(".state-success").removeClass("state-success");
		$("#btnCreate").html("Create").unbind('click').bind('click',function(){Roles.Create();});
		$("#frmSankeyFilter em").remove();
		$(".state-error").removeClass("state-error");
		$("[name=name]").removeAttr("readonly");
		constructDropDown("[name='parentrole']","select name,displayname from roles where name like '%<Q>%'",false, null, null, "Select parent role	","","",{buttonWidth:470});
		$("#singleSignOn").prop("checked", false);
		$("#enableSignUp").prop("checked", false);
	};	
	this.Create = function() {
		if (!$("#frmSankeyFilter").valid())
			return;
		var menuOnTop =0;
		if ($("#menuOnTop").is(':checked'))
			menuOnTop = 1;
		var fixedHeader =0;
		if ($("#fixedHeader").is(':checked'))
			fixedHeader = 1;
		var fixedNavigation =0;
		if ($("#fixedNavigation").is(':checked'))
			fixedNavigation = 1;
		
		var sso =0;
		if ($("#singleSignOn").is(':checked'))
			sso = 1;
		var signUp =0;
		if ($("#enableSignUp").is(':checked'))
			signUp = 1;
		
		var fixedRibbon =0;
		if ($("#fixedRibbon").is(':checked'))
			fixedRibbon = 1;
		var themeName = '';
		if ( $("#themeName").select2("val") != "")
			themeName = $("#themeName").select2("val");
		var widgetClass = '';
		if ( $("#widgetClass").select2("val") != "")
			widgetClass = $("#widgetClass").select2("val");
		var widgetColor = '';
		if ( $("#widgetColor").select2("val") != "")
			widgetColor = $("#widgetColor").select2("val");
		var request = {
				"displayname" : $("[name=displayname]").val(),
				"name" : $("[name=name]").val(),
				"parentrole" : $("[name=parentrole]").find("select").val(),
				"favicon": $("[name=favicon]").val(),
				"logo" : $("[name=logo]").val(),
				"loginContent":  $("[name=loginContent]").val(),
				"icon":  $("[name=icon]").val(),
				"displayOrder":  $("[name=displayOrder]").val(),
				"widgetClass" : widgetClass,
				"widgetColor" : widgetColor,
				"themeName" : themeName,
				"menuOnTop" : menuOnTop,
				"fixedHeader" : fixedHeader,
				"fixedNavigation" : fixedNavigation,
				"fixedRibbon" : fixedRibbon,
				"sso": sso,
				"signUp": signUp,
				"topRightIcon":  $("[name=topRightIcon]").val()
		};
		callAjaxService("CreateRoles", callbackSucessCreateRole, callBackFailure,
				request, "POST");
	};
	var callbackSucessCreateRole = function(response) {
		if (response && response.isException) {
			showNotification("error", response.customMessage);
			return;
		} else {
			THIS.Load();
			$('#myModalthredlevel').modal('hide');
			showNotification("success", "Role value Created Sucessfully..");
		}
	};
	
	/*THIS.Edit = function(displayname,name,favicon,logo,themeName,menuOnTop,displayOrder){*/
	THIS.Edit = function(name){
		enableLoading();
		  $("#frmSankeyFilter").validate().resetForm();
		var request = {
				"roleName":name
		};
		callAjaxService("GetRole",callbackSucessGetRole, callBackFailure, request, "POST");	
		
	};
	var callbackSucessGetRole = function(response){
		disableLoading();
		if (response && response.isException) {
			showNotification("error", response.customMessage);
			return;
		} else {
			$("#myModalthredlevel").modal("show");
			$("#smart-form-register input").val("");
			$("#btnCreate").html("Update").unbind('click').bind('click',function(){Roles.Update();});
			$("#frmSankeyFilter em").remove();
			$(".state-error").removeClass("state-error");
			$("[name=displayname]").val(response.displayName);
			$("[name=name]").val(response.name).attr("readonly","true");
			constructDropDown("[name='parentrole']","select name,displayname from roles where name like '%<Q>%'",false,response.parentRole, null, "Select parent role	","","",{buttonWidth:470});
			$("[name=favicon]").val(response.favicon);
			$("[name=logo]").val(response.logo);
			$("[name=loginContent]").val(response.loginContent);
			$("[name=icon]").val(response.icon);
			$("#widgetClass").select2("val",$.trim(response.widgetClass));
			$("#widgetColor").select2("val",$.trim(response.widgetColor));
			$("#themeName").select2("val",$.trim(response.themeName));
			$("[name=displayOrder]").val(response.displayOrder);
			$("#singleSignOn").prop('checked', (response.sso !== 0) ? true : false)
			$("#enableSignUp").prop('checked', (response.signUp !== 0) ? true : false)
			$("#menuOnTop").prop('checked', (response.menuOnTop !== 0) ? true : false);
			$("[name=topRightIcon]").val(response.topRightIcon);
		}
	};
	this.Update = function(){
		if (!$("#frmSankeyFilter").valid())
			return;
		var menuOnTop =0;
		if ($("#menuOnTop").is(':checked'))
			menuOnTop = 1;
		var fixedHeader =0;
		if ($("#fixedHeader").is(':checked'))
			fixedHeader = 1;
		var fixedNavigation =0;
		if ($("#fixedNavigation").is(':checked'))
			fixedNavigation = 1;
		var fixedRibbon =0;
		if ($("#fixedRibbon").is(':checked'))
			fixedRibbon = 1;
		var themeName = '';
		if ( $("#themeName").select2("val") != "")
			themeName = $("#themeName").select2("val");
		var widgetClass = '';
		if ( $("#widgetClass").select2("val") != "")
			widgetClass = $("#widgetClass").select2("val");
		var widgetColor = '';
		if ( $("#widgetColor").select2("val") != "")
			widgetColor = $("#widgetColor").select2("val");
		
		var sso =0;
		if ($("#singleSignOn").is(':checked'))
			sso = 1;
		var signUp =0;
		if ($("#enableSignUp").is(':checked'))
			signUp = 1;
		var request = {
				"displayname" : $("[name=displayname]").val(),
				"name" : $("[name=name]").val(),
				"parentrole" : $("[name=parentrole]").find("select").val(),
				"favicon": $("[name=favicon]").val(),
				"logo" : $("[name=logo]").val(),
				"loginContent":  $("[name=loginContent]").val(),
				"widgetClass" : widgetClass,
				"widgetColor" : widgetColor,
				"icon" :  $("[name=icon]").val(),
				"themeName" : themeName,
				"menuOnTop" : menuOnTop,
				"fixedHeader" : fixedHeader,
				"fixedNavigation" : fixedNavigation,
				"fixedRibbon" : fixedRibbon,
				"displayOrder" :  $("[name=displayOrder]").val(),
				"sso": sso,
				"signUp": signUp,
				"topRightIcon":  $("[name=topRightIcon]").val()
		};
		callAjaxService("UpdateRoles",callbackSucessUpdateRole,callBackFailure,request,"POST");
	};
	var callbackSucessUpdateRole = function(response){
		if (response && response.isException){
			showNotification("error",response.customMessage);
			return;
		} else {
			$('#myModalthredlevel').modal('hide');
			$("#smart-form-register input").val("");
			$("#type").select2("val",null);
			THIS.Load();
			showNotification("success","Role updated Sucessfully..");
		};
	};
	
};
RolesInstance();