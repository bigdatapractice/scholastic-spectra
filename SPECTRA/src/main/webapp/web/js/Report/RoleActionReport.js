'use strict';
pageSetUp();
var RoleActionReport;
var RoleActionReportInstance = function() {
	RoleActionReport = new RoleActionReportObject();
	RoleActionReport.Load();
};
var RoleActionReportObject = function() {
	var THIS = this;
	$("#btnCreate").unbind('click').bind('click', function() {
		RoleActionReport.Create();
	});
	var $registerForm = $("#frmSankeyFilter").validate({

		// Rules for form validation
		rules : {
			role : {
				required : true
			},
			Action : {
				required: true
			}
		},

		// Messages for form validation
		messages : {
			role : {
				key : 'Please enter Role'
			},
			Action : {
				required : 'Please enter Display Name'
			}
		},

		// Do not change code below
		errorPlacement : function(error, element) {
			error.insertAfter(element.parent());
		}
	});
	$("#frmSankeyFilter").submit(function(e) {
	    e.preventDefault();
	});
	this.Load = function(source) {
		enableLoading();
		source = source || "JDE";
		// var request = "viewType=userTypes&source="+source
		var request = {
			"source" : source
		};
		// console.log(request);
		callAjaxService("RoleActionReportPost",
				RoleActionReportPostSuccessCallback, callBackFailure, request,
				"GET");
	};

	var RoleActionReportPostSuccessCallback = function(data) {
		disableLoading();
		if (data && data.isException) {
			showNotification("error", data.customMessage);
			return;
		}
		if (data !== undefined && data !== null) {

			// data = data['userTypeList'].splice(0,100);
			// console.log(data);
			var deleteDiv = $("#deleteDiv").html();
			var dataTable = $(".dc-data-table")
					.DataTable(
							{
								"dom":"<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-3 col-xs-12 hidden-xs'l><'col-sm-3 col-xs-12 hidden-xs'C>r>"+
								"t"+
								"<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
								"processing" : true,
								"lengthChange" : true,
								"sort" : true,
								"info" : true,
								"jQueryUI" : false,
								"scrollX" : true,
								"data" : data,
								"stateSave": true,
								"order": [[ 0	, "desc" ]],
								"tableTools" : {
									"aButtons" : [ {
										"sExtends" : "collection",
										"sButtonText" : 'Export <span class="caret" />',
										"aButtons" : [ "csv", "pdf" ]
									} ],
									"sSwfPath" : "js/plugin/datatables/media/swf/copy_csv_xls_pdf.swf"
								},
								"destroy" : true,
								"columns" : [ {

									"data" : "roleActionId",
									"defaultContent" : ""
								}, {

									"data" : "role",
									"defaultContent" : ""
								},{
									"data" : "actionId",
									"defaultContent" : ""
								},{
									"data" : "actionName",
									"defaultContent" : ""
								},{
									"data" : "roleActionId",
									"defaultContent" :"",
									"render" : function(data, type,
											full) {
										return deleteDiv.replace("$","'"+data+"'").replace("#","RoleActionReport");
									}
								}]
							/*
							 * "fnRowCallback" : function(nRow, aData,
							 * iDisplayIndex, iDisplayIndexFull) { $(nRow)
							 * .attr( { 'onClick' :
							 * "javascript:RoleActionReport.Edit( '"+
							 * aData.roleActionId+ "')", 'data-target' :
							 * '#myModalthredlevel', 'data-toggle' : 'modal' }); }
							 */
							/*
							 * "fnRowCallback" : function(nRow, aData,
							 * iDisplayIndex, iDisplayIndexFull) };
							 */
							});

		}
	};
	this.CreateView = function(){
		
		$("#smart-form-register input").val("");
		$("#btnCreate").html("").append($("<i/>").addClass("fa fa-plus")).append(" Create").unbind('click').bind('click',function(){RoleActionReport.Create();});
		$("#frmSankeyFilter em").remove();
		$(".state-error").removeClass("state-error");
		var masterURL = directoryName + '/rest/MasterData/searchMasterDataFilter';


		function formatResult(resp) {
			return resp.id + ' - ' + resp.name;
		}


		function formatSelection(resp) {
			return  resp.name;
		}
		$("#actionId").select2({
			placeholder : "Enter Action",
			minimumInputLength : 1,
			maximumSelectionSize: 1,
	        formatSelectionTooBig: function (limit) {

	            // Callback

	            return 'You have already selected action';
	        },
			multiple : true,
			ajax : { // instead of writing the function to execute the request we
				// use
				// Select2's convenient helper
				url : masterURL,
				dataType : 'json',
				type : 'GET',
				data : function(term) {
					return {
						q : term,
						type : 'A',
						source : THIS.datasource
					};
				},
				results : function(data) { // parse the results into the format
					// expected by Select2.
					// since we are using custom formatting functions we do not need
					// to alter remote JSON data
					// console.log(data);
					return {
						results : data
					};
				}
			},
			formatResult : formatResult, // omitted for brevity, see the source
			// of
			// this page
			formatSelection : formatResult, // omitted for brevity, see the
			// source of this page
			dropdownCsclass : "bigdrop", // apply css that makes the dropdown
			// taller
			escapeMarkup : function(m) {
				return m;
			} // we do not want to escape
		// markup since we are
		// displaying html in
		// results
		});
		//var masterRolURL = directoryName + '/rest/MasterData/searchRoleDatanew';


		
		$("#role").select2({
			placeholder : "Enter Role",
			minimumInputLength : 1,
			 maximumSelectionSize: 1,
		        formatSelectionTooBig: function (limit) {

		            // Callback

		            return 'You have already selected Role';
		        },
			multiple : true,
			ajax : { // instead of writing the function to execute the request we
				// use
				// Select2's convenient helper
				url : masterURL,
				dataType : 'json',
				type : 'GET',
				data : function(term) {
					return {
						q : term,
						type : 'R',
						source : THIS.datasource
					};
				},
				results : function(data) { // parse the results into the format
					// expected by Select2.
					// since we are using custom formatting functions we do not need
					// to alter remote JSON data
					// console.log(data);
					return {
						results : data
					};
				}
			},
			formatResult : formatSelection, // omitted for brevity, see the source
			// of
			// this page
			formatSelection : formatSelection, // omitted for brevity, see the
			// source of this page
			dropdownCsclass : "bigdrop", // apply css that makes the dropdown
			// taller
			escapeMarkup : function(m) {
				return m;
			} // we do not want to escape
		// markup since we are
		// displaying html in
		// results
		});
	};
	this.Create = function(){
		if (!$("#frmSankeyFilter").valid())
			return;
		var request = {
				"role":$("[name=role]").select2('val')[0],
				"actionid":$("[name=Action]").select2('val')[0]
		};
		callAjaxService("CreateRoleAction",callbackSucessRoleAction,callBackFailure,request,"POST");
	};
	var callbackSucessRoleAction = function(response){
		if (response && response.isException){
			showNotification("error",response.customMessage);
			return;
		} else {
			THIS.Load();
			$('#myModalthredlevel').modal('hide');
			showNotification("success","Role Action Created Sucessfully..");
		};
	};
	this.Delete = function(event,key){
		$.confirm({
		    text: "Are you sure you want to delete that Role Action Value?",
		    confirm: function() {
		    	var request = {
						"key":key
				};
				callAjaxService("DeleteRoleAction",callbackDeleteCreateRoleAction, callBackFailure,
						request, "POST");
		    },
		    cancel: function() {
		        // nothing to do
		    }
		});
	};
	var callbackDeleteCreateRoleAction = function(response){
		if (response && response.isException) {
			showNotification("error", response.customMessage);
			return;
		} else {
			THIS.Load();
			showNotification("success", "Role Action Deleted Sucessfully..");
		}
	};
};
RoleActionReportInstance();