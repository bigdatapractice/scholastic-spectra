var searchHistory;
pageSetUp();
var searchHistoryInstance = function() {
    searchHistory = new searchHistoryObject();
    searchHistory.Load();
};
var searchHistoryObject = function() {
    var THIS = this;

    this.Load = function(source) {
        enableLoading();
        
        //var request = "viewType=userTypes&source="+source
        var location = localStorage.getItem("searchLocation");
        if(location) {
        	location = location.replace('#', '');
        }
        var request = {
            "type": "History",
            "moduleName": location 
        };
        //console.log(request);
        if(localStorage.getItem("searchLocation")) {
        	callAjaxService("getSearchLogs", UserReportPostSuccessCallback, callBackFailure,
                request, "POST");
    	}  else {
    		if (System.secondaryRoles && System.secondaryRoles.length > 0) {
                callAjaxService("integraSearhcHistory", UserReportPostSuccessCallback, callBackFailure,
                        request, "POST");
    		} else {
    			callAjaxService("searchHistoryPost", UserReportPostSuccessCallback, callBackFailure,
    					request, "POST");
    		}
    	}
    };

    var UserReportPostSuccessCallback = function(data) {
        disableLoading();
        if (data && data.isException) {
            showNotification("error", data.customMessage);
            return;
        }
        var searchHistoryTable = $("#searchHistoryTable");
        if (data !== undefined && data !== null && data.length > 0) {

            for (var i = 0; i < data.length; i++) {
                $("<tr/>")
                    .append($("<td/>").css({ "width": "2px" }))
                    .append($("<td/>").append($("<h4/>").append($("<a/>").html(data[i].displayQuery))))
                    .append($("<td/>").html(data[i].Module))
                    .append($("<td/>").append($("<i/>").html(data[i].logDate)))
                    .appendTo(searchHistoryTable);
            }
        } else {
            $(".well").empty();
            /*$("<tr/>")
            .append($("<td/>").
            		append(*/
            $("<div/>").html("No Search history found.").addClass("alert alert-warning fade in")
                .prepend($("<button/>").addClass("close").data("dismiss", "alert").html("×"))
                .prepend($("<i/>").addClass("fa-fw fa fa-warning")).appendTo($(".well"));
            /*<div class="alert alert-warning fade in">
			<button class="close" data-dismiss="alert">
				×
			</button>
			<i class="fa-fw fa fa-warning"></i>
			<strong>Warning</strong> Your monthly traffic is reaching limit.
		</div>*/
        }
        $(".table-forum h4").addClass("cursor-pointer").unbind("click").bind("click", function() {
            searchKey = $(this).text();
            if(localStorage.getItem("searchLocation")) {
        		window.location.hash = localStorage.getItem("searchLocation");
        	} else { 
        		if (System.secondaryRoles && System.secondaryRoles.length > 0) {
            		window.location.hash = "#integrasearch";
                } else {
                	window.location.hash = "#catalogSearch";
                }
        	}
            
        });
    };
};
searchHistoryInstance();
