'use strict';
pageSetUp();
var ActionReport;
var DataObfuscateInstance = function() {
	ActionReport = new ActionReportObject();
	ActionReport.Load();
};
var ActionReportObject = function() {
	var THIS = this;
	$("#btnSave").unbind('click').bind('click', function() {
		ActionReport.Save();
	});
	   	this.Load = function() {
		enableLoading();
		//source = source || "JDE";
		// var request = "viewType=userTypes&source="+source
		//var request = {
		//	"source" : source
		//};
		// console.log(request);
		//callAjaxService("DataObfuscationGetAction", ActionReportPostSuccessCallback,callBackFailure, request, "GET");
		
		/*callAjaxService(
				"DataObfuscationGetAction",
				function (response){callbackSucessGetUser(response);}, callBackFailure, "GET");
				*/
	};

	var callbackSucessGetUser = function(response) {
		
		/*disableLoading();
		if (data && data.isException) {
			showNotification("error", data.customMessage);
			return;
		}*/
		
		if (response.enabled == "1"){
						
			$("#dataObfuscation").attr("checked","true");
		}
		else 
			$("#dataObfuscation").removeAttr("checked");
	
	};
	
/*	this.CreateView = function() {

		$("#smart-form-register input").val("");
		$("#btnCreate").html("Create").unbind('click').bind('click',
				function() {
					ActionReport.Create();
				});
		$("#frmSankeyFilter em").remove();
		$(".state-error").removeClass("state-error");
		$("#sources").select2("val",null);
	};*/
	
	this.Save = function() {
		
		var enable = 0;
		if ($("#dataObfuscation").is(':checked'))
			enable = 1;
			
		var request = {			
			"enabled" : enable
			
		};
		callAjaxService("SystemPropertiesRefreshAction", callbackSucessRole, callBackFailure,
				request, "GET");
	};
	var callbackSucessRole = function(response) {
		if (response && response.isException) {
			showNotification("error", response.customMessage);
			return;
		} else {
			THIS.Load();
			$('#myModalthredlevel').modal('hide');
			showNotification("success", "Data Obfuscation Flag Updated..");
		}
	};
	
};
DataObfuscateInstance();