'use strict';
pageSetUp();
var SOReportInstance = function() {
	SOReportObject = new SOReportObject();
	SOReportObject.Load();
};
var SOReportObject = function(){

	$("#btnPrintL3").click(function(e){
    	printElement('#poDocView');
    });
    $("#btnSaveL3").click(function(e){
        savePDF('#poDocView');
    });

	this.Load = function(source) {
		enableLoading();
		source = source || "JDE";
		//var request = "viewType=soTypes&source="+source
		var request = {
			"source" : source,
			"viewType" : "soOthersTypesDetail"
		};
		console.log(request);
		callAjaxService("SOReportPost", SOReportPostSuccessCallback, callBackFailure,
				request, "POST");
	};

	var SOReportPostSuccessCallback = function(data){
		disableLoading();
		if (data && data.isException){
			showNotification("error",data.customMessage);
			return;
		}
		if(data !== undefined 
			&& data !== null 
			&& data['level2SalesOrderList'] !==undefined 
			&& data['level2SalesOrderList'] !== null){

			data = data['level2SalesOrderList'].splice(0,100);
			console.log(data);

			var dataTable = $(".dc-data-table")
					.DataTable(
							{
								"sDom" : "<'dt-top-row'Tlf>r<'dt-wrapper't><'dt-row dt-bottom-row'<'row'<'col-sm-6'i><'col-sm-6 text-right'p>>",
								"bProcessing" : true,
								"bLengthChange" : true,
								"bSort" : true,
								"bInfo" : true,
								"bJQueryUI" : false,
								"scrollX" : true,
								"aaData" : data,
								"oTableTools" : {
									"aButtons" : [ {
										"sExtends" : "collection",
										"sButtonText" : 'Export <span class="caret" />',
										"aButtons" : [ "csv", "pdf" ]
									} ],
									"sSwfPath" : "js/plugin/datatables/media/swf/copy_csv_xls_pdf.swf"
								},
								"bDestroy" : true,
								"processing" : true,
								"aoColumns" : [{											
										
											"mData" : "salesDoc",
											"sDefaultContent" : ""
										},{

											"mData" : "customerId",
											"sDefaultContent" : ""
										},{

											"mData" : "netValue",
											"sDefaultContent" : ""
										},{

											"mData" : "plant",
											"sDefaultContent" : ""
										},{
											"mData" : "materialNumber",
											"sDefaultContent" : ""
										},{
											"mData" : "orderType",
											"sDefaultContent" : ""
										},{
											"mData" : "relatedOrderType",
											"sDefaultContent" : ""
										}	],
								/*"fnRowCallback" : function(nRow, aData,
										iDisplayIndex, iDisplayIndexFull)
									};
								*/	
							});





		}
	}

}
SOReportInstance();