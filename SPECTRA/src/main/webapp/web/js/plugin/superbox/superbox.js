/*
	SuperBox v1.0.0 (modified by bootstraphunter.com)
	by Todd Motto: http://www.toddmotto.com
	Latest version: https://github.com/toddmotto/superbox
	
	Copyright 2013 Todd Motto
	Licensed under the MIT license
	http://www.opensource.org/licenses/mit-license.php

	SuperBox, the lightbox reimagined. Fully responsive HTML5 image galleries.
*/
;(function($) {
		
	$.fn.SuperBox = function(options) {
		
		var superbox      = $('<div class="superbox-show" style="background-color:#222222 !important"></div>'),
			superboximg   = $('<img src="" class="superbox-current-img" onclick="$(".detailImg").remove();"><div class="zoomCaption text-center">Roll over image to zoom in</div><div id="imgInfoBox" class="superbox-imageinfo inline-block"> <h3>Image Title</h3><span><p class="no-padding no-margin"><i>Doc Number : </i><span id="docNumber" class="text-bold superbox-img-description no-padding"></span></p><div><p class="no-padding no-margin"><i>Company : </i><span id="company" class="text-bold superbox-img-description no-padding"></span></p></div><div><p class="no-padding no-margin"><i>Material/Components : </i><div id="components" class="superbox-img-description text-bold no-padding"></div></p></p></div></span><p><a id="downloadPDF" class="btn btn-primary btn-sm">Download</a>'),
			superboxclose = $('<div class="superbox-close txt-color-white" ><i class="fa fa-times fa-lg"></i></div>');
		
		superbox.append(superboximg).append(superboxclose);
		
		var imgInfoBox = $('.superbox-imageinfo');
		
		return this.each(function() {
			
			$('.superbox-list').click(function() {
				$this = $(this);
				console.log($this);
				var currentimg = $this.find('.superbox-img'),
					imgData = currentimg.data('img'),
					imgId = currentimg.attr('id'),
					imgHelpfulcnt = currentimg.attr('helpfulcnt'),
					imgTotalcnt = currentimg.attr('totalcnt'),
					imgTags = currentimg.attr('tags'),
					imgDescription = currentimg.attr('alt') || "No description",
					docnum  = currentimg.attr('docnum'),
					company  = currentimg.attr('company'),
					imgLink = imgData,
					imgTitle = currentimg.attr('title') || "No Title";
					
				//console.log("fierd")
				
				if($('.superbox-current-img').css('opacity') == 0) {
					$('.superbox-current-img').animate({opacity: 1});
				}
				
				if ($(this).parent().next().hasClass('superbox-show')) {
					if ($this.hasClass('active')){
						$('.superbox-list').removeClass('active');
						superbox.toggle();
					} else {
						$('.superbox-list').removeClass('active');
						$this.addClass('active');
						superbox.show();
					}
					
				} else {
					superbox.insertAfter($(this).parent()).css('display', 'block');
					$('.superbox-list').removeClass('active');
					$this.addClass('active');
				}
				/*
				$('html, body').animate({
					scrollTop:superbox.position().top - currentimg.width()
				}, 'medium');*/
				/*$('.tagsinput').tagsinput('refresh');
				$('.tagsinput').tagsinput('removeAll');
				$('.tagsinput').tagsinput('add',imgTags);
				$('.tagsinput').on('itemAdded', function(event) {
					  tagItemAdded();
						// event.item: contains the item
					});
				//console.log(imgId);
				$('.tagsinput').on('itemRemoved', function(event) {
					tagItemAdded();
					});*/
				$("#helpfulcnt").html(imgHelpfulcnt);
				$("#totalcnt").html(imgTotalcnt);
				
				$("#downloadPDF").attr({"href":currentimg.attr('pdf'),"download":currentimg.attr('pdf')});
				//console.log(imgData, imgDescription, imgLink, imgTitle)
				console.log($this);
			superboximg.attr('src', imgData);
			$("#docNumber").html(docnum);
			/*$('.superbox-list').removeClass('active');
			$this.addClass('active');*/
			
			//$('#imgInfoBox em').text(imgLink);
			$('#imgInfoBox >:first-child').html(imgTitle);
			if (imgDescription.length > 450)
				imgDescription = imgDescription.slice(0,450)+ " ....";
			$('#components').html(imgDescription);
			$('#company').html(company);
			$(".detailImg").remove();
			$(".superbox-current-img").elevateZoom({ zoomType: "lens",minZoomLevel:0.5, lensShape : "round", lensSize : 300,scrollZoom : true,customClass:"detailImg" });
			});
						
			$('.superbox').on('click', '.superbox-close', function() {
				$('.superbox-list').removeClass('active');
				$(".detailImg").remove();
				$('.superbox-current-img').animate({opacity: 0}, 200, function() {
					$('.superbox-show').slideUp();
				});
				
			});
			
		});
	};
})(jQuery);