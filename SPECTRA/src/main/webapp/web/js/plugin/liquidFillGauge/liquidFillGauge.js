/*!
 * @license Open source under BSD 2-clause (http://choosealicense.com/licenses/bsd-2-clause/)
* Copyright (c) 2015, Curtis Bratton
 * All rights reserved.
 *
 * Liquid Fill Gauge v1.1
 */

function loadLiquidFillGauge(elementId, value, config,valueMax) {
    if(config == null) config = liquidFillGaugeDefaultSettings();

    var gauge = d3.select("#" + elementId);
    var radius = Math.min(parseInt(gauge.style("width")), parseInt(gauge.style("height")))/2;
    var locationX = parseInt(gauge.style("width"))/2 - radius;
    var locationY = parseInt(gauge.style("height"))/2 - radius;
    var fillPercent = .55;
    if (config.maxValue && config.maxValue > 0)
    	fillPercent = Math.max(config.minValue, Math.min(config.maxValue, value))/config.maxValue;

    var waveHeightScale;
    if(config.waveHeightScaling){
        waveHeightScale = d3.scaleLinear()
            .range([0,config.waveHeight,0])
            .domain([0,50,100]);
    } else {
        waveHeightScale = d3.scaleLinear()
            .range([config.waveHeight,config.waveHeight])
            .domain([0,100]);
    }

    var textPixels = (config.textSize*radius/2);
    var detailTextPixels = (config.detailTextSize*radius/2);
    var descTextPixels = (config.descTextSize*radius/2);
    var textFinalValue = (value instanceof Date && !isNaN(value.valueOf())) ? value:parseFloat(value).toFixed(2);
    var textFinalMaxValue = (valueMax instanceof Date && !isNaN(valueMax.valueOf())) ? valueMax:parseFloat(valueMax).toFixed(2);
    var textStartValue = config.valueCountUp?config.minValue:textFinalValue;
    //var percentText = config.displayPercent?"%":"";
    var suffixText = config.suffix;
    var prefixText = config.prefix;
    var fieldType = config.fieldType;
    var circleThickness = config.circleThickness * radius;
    var circleFillGap = config.circleFillGap * radius;
    var fillCircleMargin = circleThickness + circleFillGap;
    var fillCircleRadius = radius - fillCircleMargin;
    var waveHeight = fillCircleRadius*waveHeightScale(fillPercent*100);
    
    var detailText = config.detailText;
    var  descText = config.descText;
    var waveLength = fillCircleRadius*2;
    if (config.waveCount && config.waveCount> 1)
    	waveLength = fillCircleRadius*2/config.waveCount;
    var waveClipCount = 1+config.waveCount;
    var waveClipWidth = waveLength*waveClipCount;

    // Rounding functions so that the correct number of decimal places is always displayed as the value counts up.
    var textRounder = function(value){ 
    	if (config.aggregate != "range"){
	    	if (config.fieldType != "string" ){
	    		if (value instanceof Date && !isNaN(value.valueOf()))
	    			return d3.timeFormat(config.textFormat)(value);
	    		else 
	    			return value;
	    	}
	    	else
	    		return d3.format(config.textFormat)(value);
    	}else{
    		var values = new value.split("-");
    		if (config.fieldType != "string" ){
	    		if (values[0] instanceof Date && !isNaN(values[0].valueOf()))
	    			return d3.timeFormat(config.textFormat)(values[0])+" - "+ d3.timeFormat(config.textFormat)(values[1]);
	    		else 
	    			return value[0]+ " - "+ value[1];
	    	}
	    	else
	    		return d3.format(config.textFormat)(value[0])+" - "+ d3.format(config.textFormat)(value[1]);
    	}
    };
    /*var prefix = d3.formatPrefix(value);//d3.select(d).data()[0].data.value
	var prefixScale = Math.round(prefix.scale(value),0);
	textRounder = prefixScale + prefix.symbol;*/
    /*if(parseFloat(textFinalValue) != parseFloat(textRounder(textFinalValue))){
        textRounder = function(value){ return parseFloat(value).toFixed(1); };
    }
    if(parseFloat(textFinalValue) != parseFloat(textRounder(textFinalValue))){
        textRounder = function(value){ return parseFloat(value).toFixed(2); };
    }*/

    // Data for building the clip wave area.
    var data = [];
    for(var i = 0; i <= 40*waveClipCount; i++){
        data.push({x: i/(40*waveClipCount), y: (i/(40))});
    }

    // Scales for drawing the outer circle.
    var gaugeCircleX = d3.scaleLinear().range([0,2*Math.PI]).domain([0,1]);
    var gaugeCircleY = d3.scaleLinear().range([0,radius]).domain([0,radius]);

    // Scales for controlling the size of the clipping path.
    var waveScaleX = d3.scaleLinear().range([0,waveClipWidth]).domain([0,1]);
    var waveScaleY = d3.scaleLinear().range([0,waveHeight]).domain([0,1]);

    // Scales for controlling the position of the clipping path.
    var waveRiseScale = d3.scaleLinear()
        // The clipping area size is the height of the fill circle + the wave height, so we position the clip wave
        // such that the it will overlap the fill circle at all when at 0%, and will totally cover the fill
        // circle at 100%.
        .range([(fillCircleMargin+fillCircleRadius*2+waveHeight),(fillCircleMargin-waveHeight)])
        .domain([0,1]);
    var waveAnimateScale = d3.scaleLinear()
        .range([0, waveClipWidth-fillCircleRadius*2]) // Push the clip area one full wave then snap back.
        .domain([0,1]);

    // Scale for controlling the position of the text within the gauge.
    var textRiseScaleY = d3.scaleLinear()
        .range([fillCircleMargin+fillCircleRadius*2,(fillCircleMargin+textPixels*0.7)])
        .domain([0,1]);

    // Center the gauge within the parent SVG.
    var gaugeGroup = gauge.append("g")
        .attr('transform','translate('+locationX+','+locationY+')');

    // Draw the outer circle.
    var gaugeCircleArc = d3.svg.arc()
        .startAngle(gaugeCircleX(0))
        .endAngle(gaugeCircleX(1))
        .outerRadius(gaugeCircleY(radius))
        .innerRadius(gaugeCircleY(radius-circleThickness));
    gaugeGroup.append("path")
        .attr("d", gaugeCircleArc)
        .style("fill", config.circleColor)
        .attr('transform','translate('+radius+','+radius+')');

    // Text where the wave does not overlap.
    var text1 = gaugeGroup.append("text")
        .text(detailText)
        .attr("class", "liquidFillGaugeText")
        .attr("text-anchor", "middle")
        .attr("font-size", textPixels + "px")
        .style("fill", config.textColor)
        .attr('transform','translate('+radius+','+textRiseScaleY(config.textVertPosition)+')');
    var textDetail1 = undefined;
    if (detailText != ""){
	    textDetail1 = gaugeGroup.append("text")
	    .text(detailText)
	    .attr("class", "liquidFillGaugeText")
	    .attr("text-anchor", "middle")
	    .attr("font-size", detailTextPixels + "px")
	    .style("fill", config.detailTextColor)
	    .attr('transform','translate('+radius+','+textRiseScaleY(config.detailTextVertPosition)+')');
    }
    var textDesc1 = undefined;
    if (descText != ""){
	    textDesc1 = gaugeGroup.append("text")
	    .text(descText)
	    .attr("class", "liquidFillGaugeText")
	    .attr("text-anchor", "middle")
	    .attr("font-size", descTextPixels + "px")
	    .style("fill", config.descTextColor)
	    .attr('transform','translate('+radius+','+textRiseScaleY(config.descTextVertPosition)+')');
    }

    // The clipping wave area.
    var clipArea = d3.svg.area()
        .x(function(d) { return waveScaleX(d.x); } )
        .y0(function(d) { return waveScaleY(Math.sin(Math.PI*2*config.waveOffset*-1 + Math.PI*2*(1-config.waveCount) + d.y*2*Math.PI));} )
        .y1(function(d) { return (fillCircleRadius*2 + waveHeight); } );
    var waveGroup = gaugeGroup.append("defs")
        .append("clipPath")
        .attr("id", "clipWave" + elementId);
    var wave = waveGroup.append("path")
        .datum(data)
        .attr("d", clipArea)
        .attr("T", 0);

    // The inner circle with the clipping wave attached.
    var fillCircleGroup = gaugeGroup.append("g")
        .attr("clip-path", "url(#clipWave" + elementId + ")");
    fillCircleGroup.append("circle")
        .attr("cx", radius)
        .attr("cy", radius)
        .attr("r", fillCircleRadius)
        .style("fill", config.waveColor);

    // Text where the wave does overlap.
    var text2 = fillCircleGroup.append("text")
        .text(prefixText + textRounder(textStartValue) + suffixText)
        .attr("class", "liquidFillGaugeText")
        .attr("text-anchor", "middle")
        .attr("font-size", textPixels + "px")
        .style("fill", config.waveTextColor)
        .attr('transform','translate('+radius+','+textRiseScaleY(config.textVertPosition)+')');
    var textDetail2 = undefined;
    if (detailText != ""){
	    textDetail2 = fillCircleGroup.append("text")
	    .text(detailText)
	    .attr("class", "liquidFillGaugeText")
	    .attr("text-anchor", "middle")
	    .attr("font-size", detailTextPixels + "px")
	    .style("fill", config.detailTextColor)
	    .attr('transform','translate('+radius+','+textRiseScaleY(config.detailTextVertPosition)+')');
    }
    var textDesc2 = undefined;
    if (descText != ""){
	    textDesc2 = fillCircleGroup.append("text")
	    .text(descText)
	    .attr("class", "liquidFillGaugeText")
	    .attr("text-anchor", "middle")
	    .attr("font-size", descTextPixels + "px")
	    .style("fill", config.descTextColor)
	    .attr('transform','translate('+radius+','+textRiseScaleY(config.descTextVertPosition)+')');
    }
    // Make the value count up.
    if(config.valueCountUp){
        var textTween = function(){
        	if (textFinalMaxValue && textFinalMaxValue != 0){
	        	if (fieldType == "string"){
	        		var i = d3.interpolate(this.textContent, textFinalValue);
	        		var j = d3.interpolate(this.textContent, textFinalMaxValue);
	            	return function(t) { this.textContent = prefixText + textRounder(i(t))+ " - " + textRounder(j(t))+ suffixText; };
	        	} else{
	        		return function(t) {this.textContent = prefixText + textRounder(textFinalValue) + " - " + textRounder(textFinalMaxValue) + suffixText; };
	        	}
        	} else{
        		if (fieldType == "string"){
	        		var i = d3.interpolate(this.textContent, textFinalValue);
	            	return function(t) { this.textContent = prefixText + textRounder(i(t)) + suffixText; };
	        	} else{
	        		return function(t) {this.textContent = prefixText + textRounder(textFinalValue) + suffixText; };
	        	}
        	}
        };
        var textTweenDetail = function(){
            var i = d3.interpolate(this.textContent, detailText);
            return function(t) { this.textContent = i(t); };
        };
        var textTweenDesc = function(){
            var i = d3.interpolate(this.textContent, descText);
            return function(t) { this.textContent = i(t); };
        };
        text1.transition()
            .duration(config.waveRiseTime)
            .tween("text", textTween);
        if (textDesc1 != undefined){
	        textDesc1.transition()
	        .duration(config.waveRiseTime)
	        .tween("text", textTweenDesc);
        }
        if (textDetail1 != undefined){
	        textDetail1.transition()
	        .duration(config.waveRiseTime)
	        .tween("text", textTweenDetail);
        }
        if (textDesc2 != undefined){
	        textDesc2.transition()
	        .duration(config.waveRiseTime)
	        .tween("text", textTweenDesc);
        }
        if (textDetail2 != undefined){
	        textDetail2.transition()
	        .duration(config.waveRiseTime)
	        .tween("text", textTweenDetail);
        }
        text2.transition()
            .duration(config.waveRiseTime)
            .tween("text", textTween);
    }

    // Make the wave rise. wave and waveGroup are separate so that horizontal and vertical movement can be controlled independently.
    var waveGroupXPosition = fillCircleMargin+fillCircleRadius*2-waveClipWidth;
    if(config.waveRise){
        waveGroup.attr('transform','translate('+waveGroupXPosition+','+waveRiseScale(0)+')')
            .transition()
            .duration(config.waveRiseTime)
            .attr('transform','translate('+waveGroupXPosition+','+waveRiseScale(fillPercent)+')')
            .each("start", function(){ wave.attr('transform','translate(1,0)'); }); // This transform is necessary to get the clip wave positioned correctly when waveRise=true and waveAnimate=false. The wave will not position correctly without this, but it's not clear why this is actually necessary.
    } else {
        waveGroup.attr('transform','translate('+waveGroupXPosition+','+waveRiseScale(fillPercent)+')');
    }

    if(config.waveAnimate) animateWave();

    function animateWave() {
        wave.attr('transform','translate('+waveAnimateScale(wave.attr('T'))+',0)');
        wave.transition()
            .duration(config.waveAnimateTime * (1-wave.attr('T')))
            .ease('linear')
            .attr('transform','translate('+waveAnimateScale(1)+',0)')
            .attr('T', 1)
            .each('end', function(){
                wave.attr('T', 0);
                animateWave(config.waveAnimateTime);
            });
    }

    function GaugeUpdater(){
        this.update = function(value){
            var newFinalValue = parseFloat(value).toFixed(2);
            var textRounderUpdater = function(value){ return Math.round(value); };
            if(parseFloat(newFinalValue) != parseFloat(textRounderUpdater(newFinalValue))){
                textRounderUpdater = function(value){ return parseFloat(value).toFixed(1); };
            }
            if(parseFloat(newFinalValue) != parseFloat(textRounderUpdater(newFinalValue))){
                textRounderUpdater = function(value){ return parseFloat(value).toFixed(2); };
            }

            var textTween = function(){
                var i = d3.interpolate(this.textContent, parseFloat(value).toFixed(2));
                return function(t) { this.textContent = prefixText + textRounderUpdater(i(t)) + suffixText; };
            };

            text1.transition()
                .duration(config.waveRiseTime)
                .tween("text", textTween)
            		.transition()
	            .duration(config.waveRiseTime)
	            .tween("text", textTween);
            descText.transition()
	            .duration(config.waveRiseTime)
	            .tween("text", textTween);
            text2.transition()
                .duration(config.waveRiseTime)
                .tween("text", textTween);

            var fillPercent = Math.max(config.minValue, Math.min(config.maxValue, value))/config.maxValue;
            var waveHeight = fillCircleRadius*waveHeightScale(fillPercent*100);
            var waveRiseScale = d3.scaleLinear()
                // The clipping area size is the height of the fill circle + the wave height, so we position the clip wave
                // such that the it will overlap the fill circle at all when at 0%, and will totally cover the fill
                // circle at 100%.
                .range([(fillCircleMargin+fillCircleRadius*2+waveHeight),(fillCircleMargin-waveHeight)])
                .domain([0,1]);
            var newHeight = waveRiseScale(fillPercent);
            var waveScaleX = d3.scaleLinear().range([0,waveClipWidth]).domain([0,1]);
            var waveScaleY = d3.scaleLinear().range([0,waveHeight]).domain([0,1]);
            var newClipArea;
            if(config.waveHeightScaling){
                newClipArea = d3.svg.area()
                    .x(function(d) { return waveScaleX(d.x); } )
                    .y0(function(d) { return waveScaleY(Math.sin(Math.PI*2*config.waveOffset*-1 + Math.PI*2*(1-config.waveCount) + d.y*2*Math.PI));} )
                    .y1(function(d) { return (fillCircleRadius*2 + waveHeight); } );
            } else {
                newClipArea = clipArea;
            }

            var newWavePosition = config.waveAnimate?waveAnimateScale(1):0;
            wave.transition()
                .duration(0)
                .transition()
                .duration(config.waveAnimate?(config.waveAnimateTime * (1-wave.attr('T'))):(config.waveRiseTime))
                .ease('linear')
                .attr('d', newClipArea)
                .attr('transform','translate('+newWavePosition+',0)')
                .attr('T','1')
                .each("end", function(){
                    if(config.waveAnimate){
                        wave.attr('transform','translate('+waveAnimateScale(0)+',0)');
                        animateWave(config.waveAnimateTime);
                    }
                });
            waveGroup.transition()
                .duration(config.waveRiseTime)
                .attr('transform','translate('+waveGroupXPosition+','+newHeight+')');
        };
    }

    return new GaugeUpdater();
}