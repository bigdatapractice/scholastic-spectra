 var searchConfig = {};
 var searchConfigurator = function(cfg, obj, isOverrideScroll) {

	 var searchConfigTemplate = {
         title: '',
         appName: '',
         key: '',
         description: '',
         "id": 0,
         configFields: {},
         viewSettings: {}
     };

     var aSearchUID = 0;

     var searchConfigObject = {};
     var mode = 'View'; //Config & Preview
     var pageRows = 0;
     var noRows = 10;
     var queryCollections = [],
         queryCollectionsMap = {};
     var isLoading;

     var currentSearch = {},
         configFieldMap = {},
         defaultTab = {};     
     
     var currentRow = 0;
     var endCount = 0;
     var currentSearchText = '';
     var isSpellCheck = false;
     var wrongSpellCheckText = '';
     var correctSpellCheckText = '';
     var overParams = '';
     var globalVars = {"facet" : ''};
     var shortListedItems = [];
     var shortListedIds = [];
     var additionalConfig  = {};
     var isExactSearch = false;
     var exactSearchField = '';
     var currentPageRow = 0;
     var disableSecondayTabs = false;
     var isDistributedCollection = true;
     var shortListTemplate = {
    		 "refid":  '',
    		 "solrId": '',
    		 "moduleName": window.location.hash.replace('#','')
     };
     var emails = [];
     var emailTemplate = {
    		 "subject": '',
    		 "recepients": '',
    		 "content": '',
    		 "attachments": ''
     };
     var subMode = '';
     var databaseList = [];
     var totalPages = 0;
     var editor;
     var additional_config = {"text" : ""}
     var enableOverRideOnSearch = false;
     var noResultsFoundMsg = '';
     var canStoreHistory = false;
     var isOverrideGA = false;
     
     $('#searchconfigterm').bind("keypress", function(e) {
 		if (e.keyCode == 13) {
 			 $('#search').trigger("click");
 		}
	});
     activate();
     
     function activate() { 
    	 	 $('#searchTabsContainer').hide();
         if (!cfg) {
             searchConfigObject = JSON.parse(JSON.stringify(searchConfigTemplate)); //Deep Copy             
         } else {
             searchConfigObject = JSON.parse(JSON.stringify(cfg)); //Deep Copy....
         }
         configFieldMap = {
             search: { "id": 0, enabled: true, extra: '' },
             suggest: { "id": 0, enabled: true, extra: '' },
             defaultSearch: { "id": 0, enabled: true, extra: '', defaultView: '', shortListView : '' },
             spellcheck: { "id": 0, enabled: true, extra: '' },
             partialSearch: { enabled: true, extra: ''},
             tabs: [],
             roles: {roleDbName: '', lookUpQuery: "", enabled: true, searchQuery : ''},
         };
         $('#btnOpenSearchModal').unbind('click').bind('click', function() {
        	 $('#btnSaveSearch').show();
        	 $('#btnCloneSaveSearch').hide();
             $('#myModalSaveSearch').modal('show');
         });
         $('#btnOpenCloneSearchModal').unbind('click').bind('click', function() {
        	 $('.profile-duplicate-error').hide();
        	 $('#btnSaveSearch').hide();
        	 $('#btnCloneSaveSearch').show();
             $('#myModalSaveSearch').modal('show');
         });
         $('#btnOpenLogicModal').unbind('click').bind('click', function() {
        	 $('#myModalLogicConfig').modal('show');
        	 initiatePLogicView();
         });
         $('#btnSavePLogic').unbind('click').bind('click', function() {
        	 savePLogicView();
        	 $('#myModalLogicConfig').modal('hide');
         });
         $('#btnSaveSearch').unbind('click').bind('click', function(e) {
             callSaveButtonAction(e, 'save');
         });
         $('#btnCloneSaveSearch').unbind('click').bind('click', function(e) {
             callSaveButtonAction(e, 'clone');
         });
         function callSaveButtonAction(e, param){
        	 e.preventDefault();
             if (!$('#profilename').val()) {
                 showNotification("error", 'Enter profile name');
                 return;
             }
             searchConfigObject.title = $('#profilename').val();
             searchConfigObject.description = $('#description').val();
             if(param === 'clone') {
            	 var indicator = true;
	             $.each(queryCollections, function( index, value ) {
	            	if(searchConfigObject.title == value.text) {
	            		$('.profile-duplicate-error').show();
	            		indicator = false;
	            	}
	             });
	             if(indicator) {
	            	 saveConfig(param);
	             }
             } else {
                 saveConfig(param);
             }
             console.log(queryCollections);
         }
         $("#saveConfigBookmark").unbind("click").bind("click", function() { bookMarkSearch(); });
         $('.dropdown-menu input, .dropdown-menu label').click(function(e) {
             e.stopPropagation();
         });
         $("[id*=count]").html(0);
         $("form").submit(function(e) {
             e.preventDefault();
         });
         $('#btnInitPreview').unbind('click').bind('click', function() { initiatePreview(); });
         $('#btnInitConfig').unbind('click').bind('click', function() { initiateConfig(); });
         $('#btn-pagination').hide();

         if(isOverrideScroll) {
//        	 	isLoading = true;
//        	 	loadNextPage();
         } else {
        	 	$(window).scroll(function() {
                 if (($(document).height() - $(window).scrollTop()- $(window).height() < 90) && ($('#btn-pagination').prop('data-paginate') && $('#btn-pagination').prop('data-paginate') !== '' && $('#btn-pagination').prop('data-paginate') === 'true' && !isLoading)) {
                    	 isLoading = true;
                    	 loadNextPage();
                 }
             });
         }
         if(localStorage.getItem('searchKey')){ 
        	 searchKey = localStorage.getItem('searchKey');
        	 localStorage.setItem('searchKey', '');
         }    
         Handlebars.registerHelper('checkShortList', function(key) {
			if(!key) {
				return false;
			}
			return (shortListedIds && shortListedIds.indexOf(key) > -1)? true : false;				
		});
     }
     
     function savePLogicView() {
    	 if(!additional_config) {
    		additional_config = {"text": ''};
    	 }
    	 additional_config['text'] = btoa(editor.getValue());
     }
     
     function initiatePLogicView() {
    	 if(editor) {
    		 return;
    	 }
    	 editor = ace.edit("editor");
    	 editor.setTheme("ace/theme/monokai");
    	 editor.getSession().setMode("ace/mode/javascript");
     }
     
     function getDatabaseList(callBackFn) {
    	var sourceType = "postgreSQL";
 		var request;
 		if (sourceType === "hive" || sourceType === "postgreSQL") {
 			request = {
 				"source" : sourceType
 			};
 			enableLoading();
 			callAjaxService("getDatabaseList", function(response){ 
 				callBackgetDatabaseList(response, callBackFn);
 			}, callBackFailure, request, "POST");
 		}
     }
     
     function callBackgetDatabaseList (response, callBackFn) {
 		disableLoading();
 		if (response && response.isException) {
 			showNotification("error", response.customMessage);
 		} else if (response !== undefined && response !== null) {
 			databaseList = [];
 			for ( var i = 0; i < response.length; i++) {
 				databaseList.push({
 					"id" : response[i],
 					"text" : response[i]
 				});
 			} 			
 			$('.tabs-database-name').select2({
 	             placeholder: 'Database',
 	             data: databaseList
 	         }).on("change", function(e) {
 	             if (e.target && $(e.target).length > 0) {
 	            	var data = $(e.target).val();
 	            	if(!configFieldMap.roles) {
 						configFieldMap.roles = {roleDbName: '', lookUpQuery: "", enabled: true, searchQuery : ''}
 					}
 	            	configFieldMap.roles.roleDbName = data;
 	            	updateQueryAndDataBase();
 	            	//$('.tabs-role-names').select2("val", "");
 	             }
 	         });
 			$('.tabs-database-lookup').on('keyup', function(e){
 				if (e.keyCode == 13) {
 					if(!$(this).val()) {
 						showNotification('error', 'Lookup Query is required');
 					}
 					if(!configFieldMap.roles) {
 						configFieldMap.roles = {roleDbName: '', lookUpQuery: "", enabled: true, searchQuery : ''} 						
 					}
 					configFieldMap.roles.lookUpQuery = $(this).val(); 					
 					configFieldMap.roles.searchQuery = configFieldMap.roles.lookUpQuery.split('where')[0];
 					getLookUpQuery();
 					updateQueryAndDataBase();
 				}
 			});
 			$('.chkRoles').on('change', function(){
 				if($(this).is(':checked')) {
 					$('.roles-section').show();
 					configFieldMap.roles.enabled = true; 
 				} else {
 					$('.roles-section').hide();
 					configFieldMap.roles.enabled = false;
 				}
 				updateQueryAndDataBase();
 			});
 		}
 		if(callBackFn) {
   		  callBackFn();
 		}
 	 }
 	 
     function updateQueryAndDataBase() {
    	 configFieldMap.tabs.map(function(tab) {
    		 tab.rolesEnabled = configFieldMap.roles.enabled;
    		 tab.roleDB = configFieldMap.roles.roleDbName; 
    		 tab.roleQuery = configFieldMap.roles.searchQuery; 
    	 });
     }
     
     function getLookUpQuery() {
//    	 $('.tabs-role-names').select2("val", "");
    	 $('.tabs-role-names').select2({
				placeholder : "Enter Role",
				minimumInputLength : 1,
				allowClear: true,
				width: 'resolve',
				multiple : true,
				ajax : {
					url : serviceCallMethodNames["suggestionsLookUp"].url,
					dataType : 'json',
					type : 'GET',
					data : function(term) {
						if(!configFieldMap.roles.roleDbName) {
							return;
						}
						return {
							q : term,
							source : 'postgreSQL',
							database : configFieldMap.roles.roleDbName,
							lookupQuery : configFieldMap.roles.lookUpQuery
						};
					},
					results : function(data) {
						return {
							results : data
						};
					}
				},
				initSelection : function(element, callback) {
					callback($.map(element.val().split(','),
							function(id) {
								id = id.split(":");
								return {
									id : id[0],
									name : id[1]
								};
							}));
				},
				formatResult : formatResult,
				formatSelection : formatSelection,
				dropdownCsclass : "bigdrop",
				escapeMarkup : function(m) {
					return m;
				}
			});
     }
     
     function formatResult(resp) {
		return resp.id + ' - ' + resp.name;
	 }

	function formatSelection(resp) {
		return resp.id + ' - ' + resp.name;
	}

		
     function getQueries() {
         var request = {
             type: 'search',
             getAll: 'true'
         };
         enableLoading();
         callAjaxService("getMyProfiles", callBackGetQueries, callBackFailure, request, "POST");
     }

     function callBackGetQueries(response) {
         if (response && response.isException) {
             showNotification("error", response.customMessage);
             disableLoading();
             return;
         }
         if (response && response.length > 0) {
             queryCollections = [];
             response.map(function(obj) {
                 if (obj.fact === 'queryBuilder') {
                     queryCollections.push({ "id": obj.id, text: obj.displayname, extra: obj.database });
                     queryCollectionsMap[obj.id] = obj;
                 }
             });
             $('.chkEnable').on('change', function() {
                 if ($(this).is(':checked')) {
                     if (configFieldMap[$(this).attr('data-map')]) {
                         configFieldMap[$(this).attr('data-map')].enabled = true;
                     }
                 } else {
                     if (configFieldMap[$(this).attr('data-map')]) {
                         configFieldMap[$(this).attr('data-map')].enabled = false;
                     }
                 }
             });
         }
         disableLoading();
         if (mode.toLowerCase() !== 'config' && obj && obj.id !== 0) {
             getById(obj.id);
         } else if (mode.toLowerCase() === 'config') {
        	 initiatePLogicView();
             constructSelect2ForDefaults();
         }
     }          
     
     function addTabRow(tabRow) {
         var obj = (tabRow) ? tabRow : { "id": 0, defaultTab: false, index: aSearchUID, resultView: '', labelView: '', tabName: '', roleNames: '', rolesEnabled: false, roleDB: '', roleQuery: '' }; //id => query id
         var value = '';
         if (!tabRow) {
             configFieldMap.tabs.push(obj);
         } else {
             value = obj.id;
             aSearchUID = tabRow.index;
         }
         //Update View
         var configBtns = '<div class="smart-form"><div class="col-sm-11"><label data-index="' + aSearchUID + '" class="checkbox font-bold"><input data-index="' + aSearchUID + '" class="tabs-chkbx-' + aSearchUID + '" type="checkbox"> <i></i>Default</label><label data-index="' + aSearchUID + '" class="checkbox font-bold"><input data-index="' + aSearchUID + '" class="tabs-secondaryrole-' + aSearchUID + '" type="checkbox"> <i></i>Enable Secondary Role</label><label data-index="' + aSearchUID + '" class="checkbox font-bold"><input data-index="' + aSearchUID + '" class="tabs-edit-' + aSearchUID + '" type="checkbox"> <i></i>Editable</label></div>';
         configBtns += '<div class="col-sm-1 margin-right-10"><i title="Delete" data-index="' + aSearchUID + '" class="tabsConfig-area-tbody-tr-td-delete fa fa-md fa-fw fa-remove txt-color-red cursor-pointer" aria-hidden="true"></i></div></div>';
         //<div class="col-sm-1"><label style="width: 50%" data-index="' + aSearchUID + '" class="checkbox"><input data-index="' + aSearchUID + '" class="tabs-chkbx-' + aSearchUID + '" type="checkbox"> <i></i>Default</label></div></div>';
         
         var configDBTbl = '<input type="text" data-index="'+aSearchUID+'" class="width-100per tabsConfig-eDB-'+aSearchUID+'" /> <br />';
         configDBTbl += '<input type="text" data-index="'+aSearchUID+'" class="width-100per tabsConfig-eTB-'+aSearchUID+'" />';
         
         var str = '<fieldset class="tabsConfig-area-tbody-tr-' + aSearchUID + '">';
         str += '<section class="col col-3"> <label class="label font-bold">Tab Name </label> ';
         str += '<label class="input"><input type="text" class="width-100per tabs-textname-' + aSearchUID + '" data-index="' + aSearchUID + '" /> </label> ';
         str += '</label></section>';
         str += '<section class="col col-3"> <label class="label font-bold">Query </label>';
         str += '<label class="input"><input type="text" value="' + value + '" data-index="' + aSearchUID + '" data-map="tabs" class="width-100per tabCboSolrCollection tabsConfig-area-tbody-tr-td-' + aSearchUID + '" />';
         str += '</label></section>';
         str += '<section class="col col-3"> <label class="label font-bold"> Solr Collections  & DB Configurations </label>';
         str += '<div title="" style="white-space: nowrap;width: 20em;overflow: hidden;text-overflow: ellipsis;" class="tabs-lbl-' + aSearchUID + '"></div>';
         str += '<br /><div title="" style="white-space: nowrap;width: 20em;">'+configDBTbl+'</div>';
         str += '</label></section>';
         str += '<section class="col col-3">';
         str += '<td>' + configBtns + '</td>';
         str += '</section>';
         str += '<section class="col col-12 roles-section">';
         str += '<div class="well col-sm-12 col-md-12 margin-bottom-10 padding-bottom-10">';
         str += '<section class="col col-12">';
         str += '<section class="col col-12" style="padding-left: 0px;"> <label class="label font-bold">Roles </label>';
         str += '<label class="input"><input type="text" value="' + value + '" data-index="' + aSearchUID + '" data-map="tabs" class="width-100per tabs-role-names tabs-role-name-' + aSearchUID + '" />';
         str += '</label></section>';
         str += '</section>';
         str += '</div>';
         str += '</section>';
         str += '<section class="col col-12"><label class="label font-bold"> Result View </label>';
         str += '<label class="textarea textarea-resizable"><textarea rows="10" class="custom-scroll tabs-textarea-' + aSearchUID + '" data-index="' + aSearchUID + '"></textarea>';
         str += '</label></section>';
         str += '<hr/>';
         str += '</fieldset>';

         $('#tabsConfig-area-tbody').append(str);

         $('.tabsConfig-area-tbody-tr-td-delete').unbind('click').bind('click', function() {
             deleteTabRow($(this).attr('data-index'));
         });
         
         $('.tabs-chkbx-' + aSearchUID).on('change', function() { 
             var index = getQueryConfigIndex($(this).attr('data-index'));
             if (index === -1) {
                 return;
             }
             if ($(this).is(':checked')) { 
                 var obj = configFieldMap.tabs[index];
                 obj.defaultTab = true;
                 configFieldMap.tabs[index] = obj;
                 if (defaultTab && Object.keys(defaultTab).length > 0 && defaultTab.index !== -1) {
                     index = getQueryConfigIndex(defaultTab.index);
                     if (defaultTab.className && defaultTab.className !== '' && $(defaultTab.className).length > 0) {
                         $(defaultTab.className).prop('checked', false);
                     }
                     if (index === -1) {
                         return;
                     }
                     configFieldMap.tabs[index].defaultTab = false;
                 }
                 defaultTab = { index: obj.index, className: '.tabs-chkbx-' + $(this).attr('data-index') };
             } else {
                 var obj = configFieldMap.tabs[index];
                 obj.defaultTab = false;
                 configFieldMap.tabs[index] = obj;
                 defaultTab = {};
             }
         });
         
         $('.tabs-textname-' + aSearchUID).on('keyup', function() {
             var index = getQueryConfigIndex($(this).attr('data-index'));
             var obj = configFieldMap.tabs[index];
             obj.tabName = $(this).val();
             configFieldMap.tabs[index] = obj;
         });
         
         $('.tabs-textarea-' + aSearchUID).on('keyup', function() {
             var index = getQueryConfigIndex($(this).attr('data-index'));
             var obj = configFieldMap.tabs[index];
             obj.resultView = $(this).val();
             configFieldMap.tabs[index] = obj;
         });

         $('.tabs-secondaryrole-' + aSearchUID).on('change', function() {
             var index = getQueryConfigIndex($(this).attr('data-index'));
             if (index === -1) {
                 return;
             }
             var obj = configFieldMap.tabs[index];
             obj.enableSecondaryRole = $(this).is(':checked');
             configFieldMap.tabs[index] = obj;
         });
         
         if (tabRow) {
             value = $('.tabsConfig-area-tbody-tr-td-' + aSearchUID).val();
             $('.tabs-lbl-' + aSearchUID).text(tabRow.labelView);
             $('.tabs-lbl-' + aSearchUID).prop('title', tabRow.labelView);
             $('.tabs-chkbx-' + aSearchUID).prop('checked', !!tabRow.defaultTab);
             $('.tabs-secondaryrole-' + aSearchUID).prop('checked', !!tabRow.enableSecondaryRole);
             if (!!tabRow.defaultTab) {
                 defaultTab = { index: tabRow.index, className: '.tabs-chkbx-' + aSearchUID };
             }
             var resultView = tabRow.resultView;
             $('.tabs-textarea-' + aSearchUID).val(resultView);
             var code= $('.tabs-textarea-' + aSearchUID);
             var val= $.replace_tag(code.minify());
             var el=$("<div></div>").html(val);
             $.prettify_code(el);
             code.css('white-space', 'pre').show_code($.undo_tag(el.html()));
             $('.tabs-textarea-' + aSearchUID).val(resultView);
             $('.tabs-textname-' + aSearchUID).val(tabRow.tabName);
             $('.tabs-secondaryrole-' + aSearchUID).val(tabRow.tabName || '');
             $('.tabs-role-name-' + aSearchUID).val(tabRow.roleNames);
         }

         $('.tabsConfig-area-tbody-tr-td-' + aSearchUID).select2({
             placeholder: 'Queries',
             data: queryCollections
         }).on("change", function(e) {
             if (e.target && $(e.target).length > 0) {
                 var data = $(e.target);
                 var index = getQueryConfigIndex(data.attr('data-index'));
                 var obj = configFieldMap.tabs[index];
                 obj.id = data.val();
                 obj.labelView = $(this).select2('data').extra;
                 configFieldMap.tabs[index] = obj;
                 $('.tabs-lbl-' + data.attr('data-index')).text($(this).select2('data').extra);
                 $('.tabs-lbl-' + data.attr('data-index')).attr("title", $(this).select2('data').extra);
             }
         });
         
         $('.tabs-role-name-' + aSearchUID).select2({
             placeholder: 'Roles',
             data: [],
             multiple: true
         }).on("change", function(e) {
             if (e.target && $(e.target).length > 0) {
            	 //roleNames
                 var data = $(e.target);
                 var index = getQueryConfigIndex(data.attr('data-index'));
                 if(!configFieldMap.tabs[index].roleNames) {
                	 configFieldMap.tabs[index].roleNames = '';
                 }                 
                 if(!configFieldMap.tabs[index].rolesEnabled) {
                	 configFieldMap.tabs[index].rolesEnabled = configFieldMap.roles.enabled || false;
                 }
                 if(!configFieldMap.tabs[index].roleDB) {
                	 configFieldMap.tabs[index].roleDB = configFieldMap.roles.roleDbName || '';
                 }
                 if(!configFieldMap.tabs[index].roleQuery) {
                	 configFieldMap.tabs[index].roleQuery = configFieldMap.roles.searchQuery || '';
                 }
                 configFieldMap.tabs[index].roleNames = data.val();
             }
         });
         $('.tabsConfig-eTB-' + aSearchUID).select2({placeholder : "Table", data : []})
			$('.tabsConfig-eDB-'+aSearchUID+'').select2({
	             placeholder: 'Database',
	             data: databaseList
	         }).on("change", function(e) {
	             if (e.target && $(e.target).length > 0) {
	              var data = $(e.target);
	              var index = getQueryConfigIndex(data.attr('data-index'));
	              configFieldMap.tabs[index].database = data.val();
	               //-------------------------------------
	              var request = {
	                      "source": 'PostgresSQL',
	                      "database": data.val()
	                  };
	              getTables(index, request);
	              //--------------------------------------
	             }
	         });
			
         getLookUpQuery();
         if (value !== '') {
             $('.tabsConfig-area-tbody-tr-td-' + aSearchUID).select2('val', value);
         }

         if (!tabRow) {
             aSearchUID += 1;
         }

     }
     
     function getTables(index, request) {
    	    enableLoading();
         callAjaxService("getTableList", function(response){
       	      disableLoading();
             if (response && response.isException) {
                 showNotification("error", response.customMessage);
                 return;
             }
             if (response && response.length > 0) {
           	      var tables = [];
           	      for (var i = 0; i < response.length; i++) {
           	    	  		tables.push({ "id": response[i], "text": response[i] });
                 }
                 $('.tabsConfig-eTB-' + index).select2({"placeholder" : 'Table', "data" : tables }).on("change", function(e){
               	  	if (e.target && $(e.target).length > 0) {
               	  		var data = $(e.target);
               	  		var index = getQueryConfigIndex(data.attr('data-index'));
               	  		configFieldMap.tabs[index].table = data.val();
               	  	}
                 });
             }
         }, callBackFailure, request, "POST");
     }
     
     function constructViewTab(tabRow) { 
         if (!tabRow) {
             return;
         }
         var defaultClass = "";
         var str = '';
         if (tabRow.defaultTab === true) {
             defaultTab = JSON.parse(JSON.stringify(tabRow)); //Deep copy..
             defaultTab.className = '.searchTab-' + tabRow.index;
             defaultClass = "active";
         } 
         var str = '<li class="searchTab '+ defaultClass +' searchTab-' + tabRow.index + '" data-query="' + tabRow.id + '" data-enable-sr="' + tabRow.enableSecondaryRole + '" data-index="' + tabRow.index + '">';
         
         str += '<a href="#tab-r1" data-toggle="tab"><span class="badge bg-color-greenLight txt-color-white tab-result-counts ' + tabRow.tabName.replaceAll(' ', '_') + '-result-count" class="badge bg-color-blue txt-color-white">0</span> ' + tabRow.tabName + ' </a>';
         str += '</li>';
         var arr;
         var name = tabRow.tabName.replaceAll(' ', '_');
         var tabCalssName = '.' + tabRow.tabName.replaceAll(' ', '_') + '-result-count';
         if(localStorage.getItem('searchTabs')) {
        	 arr = JSON.parse(localStorage.getItem('searchTabs'));
         } else {
        	 arr = {};
         }
         arr[name] = tabCalssName;
         localStorage.setItem('searchTabs', JSON.stringify(arr));
         $('#searchTabsContainer').append(str);
     }

     function constructTabs() {
         if (!configFieldMap.tabs) 
        	 return;
         $('#searchTabsContainer').empty();
         for (var i = 0; i < configFieldMap.tabs.length; i += 1) {
             var obj = configFieldMap.tabs[i];
             if (!!obj && obj.id !== '') {
            	 constructViewTab(obj);
             }
         }
         $('.searchTab').unbind('click').bind('click', function() { 
             currentSearch = {"index" : "", "queryId": "", "class" : "", "resultView" : ""};
             currentSearch.index = $(this).attr('data-index');
             currentSearch.enableSC = $(this).attr('data-enable-sr');
             currentSearch.queryId = $(this).attr('data-query');
             currentSearch['class'] = '.searchTab-' + $(this).attr('data-index');
             currentSearch.resultView = configFieldMap.tabs[$(this).attr('data-index')].resultView;
             currentSearch.tabs = configFieldMap.tabs;
             currentRow = 0;
             searchSolr('tab');
         });
     }
     
     function loadPage(page, noRows) {
    	 	 if(noRows) {
    	 		pageRows = noRows;
    	 		if(page === 1) {
    	 			currentPageRow = 0;
    	 			$('#pagination-goto').val('1');
    	 		} else {
    	 			currentPageRow = ((page - 1) * noRows);
    	 			$('#pagination-goto').val(page);
    	 		}
    	 		
    	 	 }
		 searchSolr('pagination', null);
     }
     
     function searchSolr(type, searchKey, overRidingObject, callBackFn) {
		 var isAppend;   
         var storeHistory;
         if(type === 'pagination' ) {
        	   if(!isOverrideScroll) {
        		   isAppend = true;
        	   } else {
        		   isAppend = false;
        	   }
         } else {
        	 currentPageRow = 0;
        	 $('#pagination-goto').val('1');
        	 if(parseInt($('#pagination-perpage').val())) {
        		 pageRows = $($('#pagination-perpage').length > 0) ?  parseInt($('#pagination-perpage').val()) : pageRows;
        	 }
        	 if(searchKey) {
        		 currentSearchText = searchKey;
        	 } else if (correctSpellCheckText && correctSpellCheckText !== '') {
        		 currentSearchText = correctSpellCheckText
        	 } else {
        		 
        		 //
        		 if((!$('#searchconfigterm').val())) {
        			 currentSearchText = '*';
        		 } else {
        			 currentSearchText =  $('#searchconfigterm').val();
        		 }
        	 }
         }
         if(type === 'defaultSearch') {
        	 $('#searchTabsContainer').hide();
         } else if(type === 'searchAll' && !System.getShortlist) {
        	 $("#configBookMarkName").val(currentSearchText);
        	 storeHistory = true;
         }
         if(!canStoreHistory) {
        	 	storeHistory = false;
         }
         if(overParams && overParams !== '') {
        	 	overRidingObject = overParams;
         }
         if(!overRidingObject) {
        	 	overRidingObject = null;
         }
         var secondaryTabs = null;
         if(!disableSecondayTabs){
	         if (currentSearch && currentSearch.tabs && currentSearch.tabs.length){
	        	 for (var i = 0, len = currentSearch.tabs.length; i < len; i++) {
	        		 if (currentSearch.tabs[i].id !== currentSearch.queryId){
	        			 secondaryTabs = secondaryTabs ? secondaryTabs+"&tabBr&" : "";
	        			 currentSearch.tabs[i].enableSecondaryRole = currentSearch.tabs[i].enableSecondaryRole? (globalVars.enableSC !== null && typeof globalVars.enableSC !== 'undefined') ? globalVars.enableSC : currentSearch.tabs[i].enableSecondaryRole : false;
	        			 secondaryTabs += currentSearch.tabs[i].id + ":" +  currentSearch.tabs[i].enableSecondaryRole;
	        			 secondaryTabs += ":" + (currentSearch.tabs[i].rolesEnabled || false) + ":" + (currentSearch.tabs[i].roleDB || ' ') + ":" + (currentSearch.tabs[i].roleQuery?currentSearch.tabs[i].roleQuery.replaceAll("@", "'"):'' || ' ');
	        			 secondaryTabs += ":" + (currentSearch.tabs[i].roleNames || ' ');
	        		 }
	        	 }
	         }
         }
//           var facetQuery = searchActionCallbackHandler.docRestrictedStr;
//           if(searchActionCallbackHandler.facetingQuery) {
//        	   	facetQuery += ' AND ' + searchActionCallbackHandler.facetingQuery;
//           }
        	   var obj = {
        			mm : searchActionCallbackHandler.mmLimit,
        	        sort: searchActionCallbackHandler.sortQuery,
        	        fq :  searchActionCallbackHandler.facetingQuery,
        	        rawquery: searchActionCallbackHandler.docRestrictedStr || ''
        	   }
        	  var overRideMap = JSON.stringify(obj);
        	   if(obj.mm) {
        		   overRidingObject = null;
        	   }
         if(!currentPageRow || !isOverrideScroll) {
        	    currentPageRow = (currentRow * pageRows);
         }
        
         var request = { 
             profileId: '' + currentSearch.queryId,
             solrcollectionname: '',
             query: currentSearchText,
             row: currentPageRow || 0,
             noRow: (pageRows === 0) ? 10 : (pageRows || 10),
             overRideParams: overRidingObject,
             enableSC: (currentSearch.enableSC === 'true')  ? (globalVars.enableSC !== null && typeof globalVars.enableSC !== 'undefined') ? '' + globalVars.enableSC : '' + currentSearch.enableSC : "false",
             storeHistory: !!storeHistory,
             moduleName: window.location.hash.replace('#', ''),
             secondaryTabs: secondaryTabs,
             overRideMap: overRideMap
         };
         if(type !== 'suggest') {
        	 enableLoading();
         }
         if(!isOverrideGA) {
        	 	callGoogleAnalytics();
         }
         callAjaxService("solrSearch", function(response) {
        	 if(callBackFn) {
        		 callBackFn(response, isAppend, type);
        	 } else {
        		 callBackSearchSolr(response, isAppend, type);
        	 }             
         }, callBackFailure, request, "POST", null, true);
     }
     
     function prepareElevateZoom() {
    	 $('.zoomContainer').remove();
    	 $(".elevateZoom-window").elevateZoom({
             zoomWindowPosition: 2,
             scrollZoom: true,
             tint: true,
             tintColour: 'red',
             tintOpacity: 0.3,
             minZoomLevel: 0.3
         });
         $(".elevateZoom-lens").elevateZoom({
             zoomType: "lens",
             lensShape: "round",
             lensSize: 150,
             scrollZoom: true,
             minZoomLevel: 0.05,
         });
         
     }
     
     function prepareSlick() {
         $(".slick_regular").not('.slick-initialized').slick({
	            dots: false,
	            infinite: false,
	            slidesToShow: 1,
	            slidesToScroll: 1,
	            prevArrow:'<button type="button" class="slick-prev prev-slick"></button>',
	            nextArrow:'<button type="button" class="slick-next next-slick"></button>'
	     });
         $('.prev-slick').on('click', function(){
        	 prepareElevateZoom();
         });
         $('.next-slick').on('click', function(){
        	 prepareElevateZoom();
         });
     }
     
     function loadNextPage() {
	 	 if(endCount) {
	 		 return;
	 	 }
		 currentRow += 1;
		 if(isOverrideScroll) {
    		 searchSolr('pagination', null);
		 } else {
			 var resultCountStr = '<div class="alert alert-warning fade in col-md-12"><h3 class="no-margin">Page '+ (currentRow + 1) +'</h3><i class="pull-right page-loading fa fa-circle-o-notch fa-spin" style="font-size:24px"></i></div>';
			 $('#searchTabContent').append(resultCountStr); 
    		 searchSolr('pagination', null);
		 }
     }
     
     function clearAllShortlists() {
    	 	shortListedIds = [];
    	 	localStorage.setItem('shortLists', JSON.stringify({shortlist: shortListedIds}));
     }
     
     function callBackSearchSolr(response, isAppend, type) {
         disableLoading();
         isLoading = false;
         if (response && response.isException) {
             showNotification("error", response.customMessage);
             return;
         }
         if(type === 'defaultSearch') {
        	 $('#searchTabsContainer').hide();
         } else {
        	 $('#searchTabsContainer').show();
         }
         if (!isAppend) {
             $('#searchTabContent').empty();             
         } else {
        	 if(isOverrideScroll){
        		 $('#searchTabContent').empty();    
        	 }
        	 //Intentionally do nothing....
         }
         $(".tt-dropdown-menu").hide();
         $(".tt-hint").val("");
         var str = currentSearch.resultView;
         if (str && response) {
        	 var responseWithTabs = JSON.parse(response);
        	 response =  JSON.parse(responseWithTabs[currentSearch.queryId]);
        	 $("#searchTabsContainer li").each(function(){
        		 if (responseWithTabs[$(this).data("query")]){
	        		 var tabResponse =  JSON.parse(responseWithTabs[$(this).data("query")]);
	        		 if (tabResponse.response.numFound){
	        			 $(this).show().find("span").html(tabResponse.response.numFound);
	        		 } else {
	        			 if(isDistributedCollection)
	        				 $(this).hide();
	        			 else
	        				 $(this).show().find("span").html(0);
	        		 }
        		 }
        	 });
        	 if(response.response && response.response.docs && response.response.docs.length > 0) {
        		 if(response.response.numFound > response.response.docs.length) {
        			 $('#btn-pagination').prop('data-paginate', 'true');
        			 $('#btn-pagination').hide();
        			 if(currentRow < 1) {
        				 pageRows = response.response.docs.length;
        				 totalPages = Math.ceil((response.response.numFound / pageRows));
        			 }
        		 } else {
        			 $('#btn-pagination').prop('data-paginate', 'false');
        			 $('#btn-pagination').hide();
        		 }
        		 
        		 if(!isAppend && type !== 'defaultSearch') {
        			 var resultCountStr = '';
        			 $("#search-result").remove();
        			 endCount =  (response.response.numFound <= pageRows)? true : false;
        			 
        			 
        			 if(isSpellCheck) {
        				 resultCountStr += '<div id="search-result" class="search-result-summary search-notfound-summary"><span><span>No results found for </span><span><b>'+ wrongSpellCheckText +'</b>, Showing <b>'+currentSearchText+'</b> ('+addCommasToNumbers(response.response.numFound)+' found) instead</span><span></span></span></div></div>';   //instead
//        				 resultCountStr +='<div ><h1 class=""><span><span> showing <span>'++'</span><span> results for "</span><span><span class="W-gt5y">'+currentSearchText+'</span> instead </span><span>"</span></span></h1>';
        				 $('#resultSetCount').text('No Results found.').css({"font-size":"14px"});
        				 $("#paginationContainer").hide();
        				 if($('#resultSetCount').length === 0) {
        					 
        				 } else {
        					 $('#search-body').prepend(resultCountStr);
        				 }
        			 } else {
        				 resultCountStr +='<div id="search-result"><h1 class="search-result-summary"><span><span>Showing <span>'+addCommasToNumbers(response.response.numFound)+'</span><span> results </span ><span style="display: none;"><span class="W-gt5y currentSearchSpan" >"'+currentSearchText+'"</span></span><span></span></span></h1>';
        				 $('#resultSetCount').data("result",'Showing ' + addCommasToNumbers(response.response.numFound) + ' results').text('Showing ' + addCommasToNumbers(response.response.numFound) + ' results').css({"font-size":"14px"});
        				 $("#paginationContainer").show();
        				 if($('#resultSetCount').length === 0) {
        					 $('#search-body').prepend(resultCountStr);
        				 }
        			 }
            		 resultCountStr += '</div>';
                    
                     $('#searchTabContent').addClass("tab-content");
        		 } else {
        			 $('.page-loading').hide();
        			 endCount =  (totalPages <= (currentRow + 1))? true : false;
        		 }
        		 var docs = response.response.docs;
        		 var positionalKvp = {};
        		 var positionalKvpKey ={};
        		 
        		 if(globalVars['facet'] && globalVars['facet'] !== '') {
        			 var responseKvp = globalVars['facet'](type, response.facet_counts, response.response.numFound, currentSearchText, response.response.docs, response);
        			 if(responseKvp && responseKvp.isOverride === true) {
        				 docs = responseKvp.overrideValue;
        			 }
        		 }
        		 if(response.highlighting) {
        			 docs.map(function(a) {
        				 if(a.id) {
        					 positionalKvp[a.id] = a; 
        				 } else if(a.key) {
        					 positionalKvpKey[a.key] = a; 
        				 }
        			 });
        			 for(var i in response.highlighting) {
        				 if(response.highlighting[i] && Object.keys(response.highlighting[i]).length > 0) {
        					 for(var j in response.highlighting[i]) {
        						 if(response.highlighting[i][j] && response.highlighting[i][j].length> 0) {
        							 if(positionalKvp[i] && positionalKvp[i][j]) {
        								 positionalKvp[i][j] = '';
            							 response.highlighting[i][j].map(function(a){
            								 positionalKvp[i][j] +=  a;
            							 })
        							 } else if(positionalKvpKey[i] && positionalKvpKey[i][j]) {
        								 positionalKvpKey[i][j] = '';
            							 response.highlighting[i][j].map(function(a){
            								 positionalKvpKey[i][j] +=  a;
            							 })
        							 }
        							 
        						 }
            				 }
        				 }
        			 }        			
        		 }
        			 
                 var res = templates.getResultView(str, { serchResponse: docs });
                 $('#searchTabContent').append(res);
                 if(subMode === 'shortlist'){
                	 var str = '<div style="border-bottom: 1px solid #ccc"><div class="pull-right" style="display: none;"><a class="btn btn-warning pull-right backToProduct" href="javascript:void(0);" title="Back to Search Results"><i class="fa fa-mail-reply"></i> Back</a></div><div><h1 class="txt-color-blueDark"><i class="fa-fw fa fa-star-o"></i> My Favorites</h1></div></div>';
                	 $('#search-result').html(str);
                	 $('#searchForm').hide();
                	 $('#searchTabsContainer').hide();
                	 $('.backToProduct').on('click', function(){
                		location.reload();
                	 });
        		 }
                 /*var res = templates.getResultView(str, { serchResponse: docs });
                 $('#searchTabContent').append(res); */
                 prepareSlick();
                 prepareElevateZoom();
        	 } else {
        		 $('#btn-pagination').hide();
        		 $('#btn-pagination').prop('data-paginate', 'false');
        		 if(configFieldMap.spellcheck.enabled && !isSpellCheck && type === 'searchAll') {
        			 getSpellChecks();
        		 } else {
        			 isSpellCheck = false;
        			 if(type !== 'defaultSearch') {
        				 if(localStorage.getItem('searchTabs') && type !== 'tab' && isDistributedCollection) {
        					 var arr = JSON.parse(localStorage.getItem('searchTabs'));
								var keys = Object.keys(arr);
								for(var i = 0; i<keys.length; i += 1) {
									$(arr[keys[i]]).text('0');
								}
        				 }
        				 if(globalVars['facet'] && globalVars['facet'] !== '') {
                			 var responseKvp = globalVars['facet'](type, response.facet_counts, response.response.numFound, currentSearchText, response.response.docs, response);
                		 }
        				 var resultCountStr = getNoResultsFound();
        				 $("#paginationContainer").hide();
        				 $('#resultSetCount').text('No Results found.').css({"font-size":"14px"});
            			 $('#searchTabContent').append(resultCountStr);
            			 $('.search-result-summary').html('');
        			 }        			 
        		 }        		 
        	 }            	 
         }
     }
     
     function getSpellChecks() {
    	 if(configFieldMap.spellcheck && configFieldMap.spellcheck.enabled && configFieldMap.spellcheck.id && parseInt(configFieldMap.spellcheck.id) > 0) {
    		 var overRidingObject = {"q": "", "spellcheck.q" : currentSearchText};
    		 overRidingObject = JSON.stringify(overRidingObject);
    		 var request = {
    	             profileId: '' + configFieldMap.spellcheck.id,
    	             solrcollectionname: '',
    	             query: currentSearchText,
    	             overRideParams: overRidingObject
    	         };
    	         enableLoading();
    	         callAjaxService("solrSearch", function(response) {
    	        	 callBackGetSpellChecks(response, currentSearchText);
    	         }, callBackFailure, request, "POST", null, true);
    	 }
     }
     
     function callBackGetSpellChecks(response, searchText) {
    	 disableLoading();
         if (response && response.isException) {
             showNotification("error", response.customMessage);
             return;
         }
         if (response) {
        	 response = JSON.parse(response);
        	 if(response.response && response.spellcheck &&  response.spellcheck.suggestions && response.spellcheck.suggestions.length > 0) {
        		 response = response.spellcheck.suggestions;
        		 var spellcheckText = '';
        		 for(var i = 0; i<response.length; i+=1) {
        				 if((response[i].toString().toLowerCase() === searchText) && response[i+1] && typeof response[i+1] === 'object' && response[i+1].suggestion && response[i+1].suggestion.length > 0) {
        					 spellcheckText = response[i+1].suggestion[0];
        					 break;
        				 }
        		 }
        		 isSpellCheck = true;
        		 wrongSpellCheckText = searchText;
        		 correctSpellCheckText = spellcheckText;
        		 searchSolr('searchAll', spellcheckText);
        	 } else {
        		 var resultCountStr = getNoResultsFound();
    			 $('#searchTabContent').html(resultCountStr);
    			 $('#searchTabsContainer').hide();
    			 $('.search-result-summary').html('');
        	 }            	 
         }
     }
     
     function getNoResultsFound() {
    	 //var str = '<div class="_2-FwU3"><div style="text-align: center"><img src="img/NoResultsFound.png"><div><div>Sorry, no results found!</div><div>Please check the spelling or try searching for something else</div></div></div></div>';
    	 	var str = noResultsFoundMsg ? noResultsFoundMsg : '<div class="_2-FwU3"><div style="text-align: center"> <h3 style="margin-left:-30%">No results found for query <i><strong>' + currentSearchText + '</strong></i>.</h3> </div>';
    	 	str = str.replaceAll('{{currentSearchText}}', currentSearchText);
    	 	str = str.replaceAll('{{currentSearchConfigText}}', $('#searchconfigterm').val());
    	 	$('#search-faceting').hide();
    	 	return str;
     }
     
     function constructSelect2ForDefaults() {
         $('.cboSolrCollection').each(function() {
             var value = $(this).val();

             $(this).select2({
                 placeholder: 'Queries',
                 data: queryCollections,
                 allowClear: true
             }).on("change", function(e) {
                 if (e.target && $(e.target).length > 0) {
                     var data = $(e.target);
                     if (configFieldMap[data.attr('data-map')]) {
                    	 if(data.val() === '') {
                    		 configFieldMap[data.attr('data-map')].id = null;
                    		 configFieldMap[data.attr('data-map')].extra = null;
                    	 } else {
                    		 configFieldMap[data.attr('data-map')].id = data.val();
                             configFieldMap[data.attr('data-map')].extra = $(this).select2('data').extra;
                    	 }
                     }
                     if($(this).select2('data') && $(this).select2('data').extra) {
                    	 $('#' + data.attr('data-map') + '-lbl').text($(this).select2('data').extra);
                     } else {
                    	 $('#' + data.attr('data-map') + '-lbl').text('');
                     }
                     
                 }
             });
             if (value !== '') {
                 $(this).select2('val', value)
             }
         });
     }

     function constructConfigRows() {
         //Update Static Rows...
         var defaultRows = ['search', 'suggest', 'spellcheck', 'defaultSearch', 'partialSearch', 'roles'];
         for (var i = 0; i < defaultRows.length; i += 1) {
             if (configFieldMap[defaultRows[i]]) {
                 var element = $('#autoSuggest-area-tbody').find("[data-map='" + defaultRows[i] + "']");
                 $(element).each(function() {
                     if ($(this).is(':text') && $(this).hasClass('cboSolrCollection')) {
                         $(this).val(configFieldMap[defaultRows[i]].id);
                         $('#' + defaultRows[i] + '-lbl').text(configFieldMap[defaultRows[i]].extra);
                     } else if($(this).is(':text') && $(this).hasClass('cboDrpDwn')){
                    	 $(this).val(configFieldMap[defaultRows[i]].roleDbName);
                    	 $(this).select2('val', configFieldMap[defaultRows[i]].roleDbName);
                     } else if ($(this).is(':checkbox') && $(this).hasClass('chkEnable')) {
                         $(this).prop('checked', !!configFieldMap[defaultRows[i]].enabled);
                     } else if($(this).is(':text')) {
                    	 $(this).val(configFieldMap[defaultRows[i]].extra || ((configFieldMap[defaultRows[i]].lookUpQuery)? configFieldMap[defaultRows[i]].lookUpQuery.replaceAll("@", "'") : ''));
                    	 configFieldMap[defaultRows[i]].lookUpQuery = (configFieldMap[defaultRows[i]].lookUpQuery)? configFieldMap[defaultRows[i]].lookUpQuery.replaceAll("@", "'") : '';
                     }
                 });

             }
         }
         if(configFieldMap.defaultSearch && configFieldMap.defaultSearch.defaultView) {
        	 $('#defaultSearch-view').val(configFieldMap.defaultSearch.defaultView);
    	 }
         if(configFieldMap.defaultSearch && configFieldMap.defaultSearch.shortListView) {
        	 $('#shortlist-view').val(configFieldMap.defaultSearch.shortListView);
         }         
         constructSelect2ForDefaults();
         //Update Tab Rows...
         if (configFieldMap.tabs) {
             $('#tabsConfig-area-tbody').empty();
             var uniqueIndices = [];
             for (var i = 0; i < configFieldMap.tabs.length; i += 1) {
                 addTabRow(configFieldMap.tabs[i]);                 
                 uniqueIndices.push(configFieldMap.tabs[i].index);
             }
             if (uniqueIndices.length > 0) {
                 aSearchUID = Math.max.apply(Math, uniqueIndices) + 1;
             }
         }

     }

     function getQueryConfigIndex(queryIndex) {
         queryIndex = parseInt(queryIndex);
         if (configFieldMap.tabs) {
             var returnIndex = -1;
             configFieldMap.tabs.some(function(obj, index, arr) {
                 if (obj.index === queryIndex) {
                     returnIndex = index;
                 }
                 return obj.index === queryIndex;
             });
             return returnIndex;
         }
         return -1;
     }

     function deleteTabRow(tabId) {
         if (!tabId) {
             return;
         }
         var index = getQueryConfigIndex(parseInt(tabId));
         if (index > -1) {
             $('.tabsConfig-area-tbody-tr-' + tabId).remove();
             configFieldMap.tabs.splice(index, 1);
         }
     }

     function getById(id) {
         var request = {
             "id": id
         };
         enableLoading();
         callAjaxService("getProfileConfig", callBackGetById, callBackFailure, request, "POST", null, true);
     }

     function callBackGetById(response) {
         if (response && response.isException) {
             showNotification("error", response.customMessage);
             disableLoading();
             return;
         }
             if (response && response.config) {
                 var search = JSON.parse(response.config);
                 if (search.length > 0) {
                     search = search[0];
                 }
                 additionalConfig = (search && search.additional_config && search.additional_config) ? search.additional_config : null;
                 if (search && search.config) {
                     searchConfigObject = JSON.parse(search.config);                     
                     if(!searchConfigObject.roleDbName) {
                    	 searchConfigObject.roleDbName = '';
                    	 
                     }
                     if (mode === 'id' || mode === 'preview' || mode === 'view') {
                         $('#profilename').val(searchConfigObject.title);
                         $('#description').val(searchConfigObject.description);
                         searchConfigObject.id = obj.id;
                         if (mode === 'id' && searchConfigObject.configFields) {
                             //Add Tab Rows.
                             //Update Defaults...
                                 configFieldMap = JSON.parse(JSON.stringify(searchConfigObject.configFields));
                                 constructConfigRows();
                         }
                         
                         if(configFieldMap.defaultSearch && !configFieldMap.defaultSearch.defaultView) {
                        		 configFieldMap.defaultSearch.defaultView = '';
                         }
                         if (mode === 'preview' || mode === 'view') {
                        	 configFieldMap = JSON.parse(JSON.stringify(searchConfigObject.configFields));                        	 
                             constructTabs();                             
                             enableSuggestion();                                                          
                             if(mode === 'preview' || mode === 'view') {                            	 
                            	 loadScript("js/SearchConfig/searchCallBackHandler.js", function() {                            		 
                            		 loadScript("js/SearchConfig/shoppingCart.js", function() {
                            			 
                            			 if (search && search.additional_config && search.additional_config){
                                        	 try{
                                        		 searchActionCallbackHandler.load(atob(JSON.parse(search.additional_config).text));
                                        	 } catch (e) {
                                        		 searchActionCallbackHandler.load();
                                        	 }
                                        	
                                         }
                            			 getCart();
                            			 if(System.getShortlist) {
                            				 getShortlists(true, null);
                            				 System.getShortlist = false;
                            				 return;
                            			 }
                                		 makeDefaultSearchAndView();   
                                		                             		 
                            			 if(searchKey && searchKey !== '') {
                            				 $('#searchconfigterm').val(searchKey);
                            				 searchKey = '';
                            	             currentSearch = {"index" : "", "queryId": "", "class" : "", "resultView" : ""};            
                            	             currentSearch.index = defaultTab.index;
                            	             currentSearch.queryId = defaultTab["id"];
                            	             currentSearch['class'] = defaultTab.className;
                            	             currentSearch.resultView = configFieldMap.tabs[defaultTab.index].resultView;
                            	             currentSearch.tabs = configFieldMap.tabs
                            	             currentRow = 0;
                            	             $(defaultTab.className).addClass('active');
                            	             overParams = '';
                            	             correctSpellCheckText = '';
                            				 searchSolr('searchAll');
                            			 }
                            		 }, "#dataAnalyserProfile");
                          		}, "#dataAnalyserProfile");
                             }
                         }
                     }                                          
                 }
                 if ((mode !== 'preview' && mode !== 'view') && search && search.additional_config && search.additional_config){
                	 initiatePLogicView();
                	 try{
                		 additional_config = JSON.parse(search.additional_config);
                		 editor.setValue(atob(JSON.parse(search.additional_config).text));
                	 } catch (e) {
                		 editor.setValue('');
                	 }
                	
                 }
                 
             }             
         disableLoading();
     }
     
     function makeDefaultSearchAndView() {
    	 $('#previewSearchConfig').fadeIn('slow');
    	 if(configFieldMap.defaultSearch && configFieldMap.defaultSearch.enabled && configFieldMap.defaultSearch.id && parseInt(configFieldMap.defaultSearch.id) > 0) {
    		 var resultViews = (configFieldMap.defaultSearch.defaultView && configFieldMap.defaultSearch.defaultView !== '')? configFieldMap.defaultSearch.defaultView : configFieldMap.tabs[defaultTab.index].resultView;
             currentSearch = {"index" : "", "queryId": "", "class" : "", "resultView" : ""};
             currentSearch.index = defaultTab.index;
             currentSearch.queryId = configFieldMap.defaultSearch.id;
             currentSearch['class'] = defaultTab.className;
             currentSearch.resultView = resultViews;
             currentSearch.tabs = configFieldMap.tabs;
             currentRow = 0;
             $(defaultTab.className).addClass('active');
             $('#searchTabsContainer').hide();
             searchSolr('defaultSearch');
         } else {
        	 if(configFieldMap.defaultSearch.defaultView && configFieldMap.defaultSearch.defaultView !== '') {
        		 $('#searchTabsContainer').hide();
        		 $('#searchTabContent').html(configFieldMap.defaultSearch.defaultView);
        	 }
         }
     }
     
     function initiateConfig(id) {
         $('#previewConfig').fadeOut('slow');
         $('#editProfile').fadeIn('slow');
         $('#tabsConfig-area-tbody').empty();
         mode = 'config';
         if (id && parseInt(id) > 0) {
             mode = 'id';
             obj = {
                 "id": parseInt(id)
             };
         } else if (configFieldMap) {
             constructConfigRows();
         } else if (obj && obj.id > 0) {
             mode = 'id';
             id = obj.id;
         }
         getDatabaseList(function(){        	 
        	 if (!queryCollections || queryCollections.length === 0) {
                 getQueries();
             } else if (obj && obj.id > 0 && parseInt(searchConfigObject.id) !== obj.id) {
                 mode = 'id';
                 getById(id);
             }
         })         
         $('#btn-add-tab').unbind('click').bind('click', function() { addTabRow(); });
     }

     function initiatePreview(id) {
         mode = 'preview';
         if (id && id > 0) {
             mode = 'preview';
             obj = {
                 "id": id
             };
             getById(id);
         } else if (!!configFieldMap) {
             constructTabs();
             enableSuggestion();
             makeDefaultSearchAndView();
         } else if (obj.id) {
             id = obj.id;
             getById(id);
         }       
         
         if (additionalConfig){
        	 try{
        		 searchActionCallbackHandler.load(atob(JSON.parse(additionalConfig).text));
        	 } catch (e) {
        		 searchActionCallbackHandler.load();
        	 }
        	
         }
         $('#previewConfig').fadeIn('slow');
         $('#editProfile').fadeOut('slow');
         $('#btn-pagination').unbind('click').bind('click', function() { loadNextPage(); })
         $('#search').unbind('click').bind('click', function(e) {
             e.preventDefault();
             isSpellCheck = false;

             $('.searchTab').each(function() { 
            	 if($(this).hasClass('active')) { 
            		 $(this).removeClass('active'); 
            	 } 
             });
           
             currentSearch = {"index" : "", "queryId": "", "class" : "", "resultView" : ""};            
             currentSearch.index = defaultTab.index;
             currentSearch.queryId = defaultTab["id"];
             currentSearch['class'] = defaultTab.className;
             currentSearch.resultView = configFieldMap.tabs[defaultTab.index].resultView;
             currentSearch.tabs = configFieldMap.tabs;
             currentRow = 0;
             $(defaultTab.className).addClass('active');
             if(!enableOverRideOnSearch) {
            	 	overParams = '';
             }
             correctSpellCheckText = '';
             var searchStr = $('#searchconfigterm').val();
             if($('#searchconfigterm').val() && configFieldMap.partialSearch && configFieldMap.partialSearch.enabled && configFieldMap.partialSearch.extra) {
            	 var advArr = configFieldMap.partialSearch.extra.split(',');
            	 var splitString = '';
            	 advArr.map(function(i){
            		 var fieldName = i.split("#")            		
            		 if(fieldName.length > 1 && $('#searchconfigterm').val().toLowerCase().startsWith(fieldName[1].toLowerCase())) {
            			splitString =  fieldName[1];
            			fieldName[1] = fieldName[1].replace(/[^A-Za-z_]/g, "")
            			var param = {"fq" : '' + fieldName[0] + ': "' + fieldName[1] + '"'}; // '+':\"'++'\"'}
            			overParams = JSON.stringify(param);
            		}
            	 });
            	 searchStr = $('#searchconfigterm').val();
            	 if(splitString) {
            		 searchStr = searchStr.toLowerCase().replace(splitString.toLowerCase(), '');
            		 $('.typeahead').typeahead('val', searchStr);
            		 $('#searchconfigterm').val(searchStr)
            	 }
             }
             if(isExactSearch === 0) {
            	   searchStr = exactSearchField +  '"' + searchStr +'"';
             } else if(isExactSearch === 1) {
            	 	searchStr = searchStr.replaceAll(' ', ' OR ');
             }
             searchSolr('searchAll', searchStr, overParams);
         });
     }          
     
     function enableSuggestion() {
         if (configFieldMap.suggest && configFieldMap.suggest.enabled && configFieldMap.suggest.id && parseInt(configFieldMap.suggest.id) > 0) {
        	 $('.typeahead').unbind();
             $('.typeahead').typeahead({
                 hint: true,
                 highlight: true,
                 minLength: 0
             }, {
                 name: 'states',
                 displayKey: 'value',
                 source: function(query, process) {
                     $.ajaxQ.abortAll();
                     query = $.trim(query);
                     if (query !== "") {
                         var queryParameter = queryCollectionsMap[configFieldMap.suggest.id];
                         if (!!queryParameter || !!configFieldMap.suggest.id) {
                        	 var profileId =  (!!queryParameter && !!queryParameter.id) ? queryParameter.id : configFieldMap.suggest.id;
                             return $.post(serviceCallMethodNames["solrSearch"].url, { query: query, profileId: profileId }, function(data) {
                            	 if(data && data.isException) {
                            		 //Intentionally do nothing...
                            		 return;
                            	 }
                            	 data = JSON.parse(data);
                            	 if(data.suggest && data.suggest.mySuggester) {
                            		 data = data.suggest.mySuggester;
                            		 if(Object.keys(data).length > 0) {
                            			 data = data[Object.keys(data)[0]];
                            			 if(data.suggestions && data.suggestions.length > 0) {
                            				 data = data.suggestions;
                            				 var temp = [];
                                             for (var i = 0; i < data.length; i++) {
                                               temp.push({ "value": removeHTMLTags(data[i].term) });
                                             }
                                             return process(temp);
                            			 }                            			
                            		 }
                            	 }                            	 
                             });
                         }
                     } else {
                         $(".tt-dropdown-menu").hide();
                         $(".tt-hint").val("");
                     }
                 }
             }).bind("typeahead:selected", function() {
            	 $('.searchTab').each(function() { 
            		 if($(this).hasClass('active')) { 
            			   $(this).removeClass('active') 
            			 } 
            	  })
                 currentSearch = {"index" : "", "queryId": "", "class" : "", "resultView" : ""};
                 currentSearch.index = defaultTab.index;
                 currentSearch.queryId = defaultTab["id"];
                 currentSearch['class'] = defaultTab.className;
                 currentSearch.resultView = configFieldMap.tabs[defaultTab.index].resultView;
                 currentSearch.tabs = configFieldMap.tabs;
                 currentRow = 0;
                 $(defaultTab.className).addClass('active');
                 searchSolr('searchAll');
             });
         }
     }

     function initiateView(id,config) {
    	 mode = 'view';
         if (id && id > 0) {
             mode = 'view';
             obj = {
                 "id": id
             };
             if (config)
            	 callBackGetById(config);
             else
            	 getById(id);
         } else if (configFieldMap) {
             constructTabs();
             enableSuggestion();
             makeDefaultSearchAndView();
         } else if (obj.id) {
             id = obj.id;
             getById(id);
         }
         $('#btn-pagination').unbind('click').bind('click', function() { loadNextPage(); })
         $('#search').on('click', function(e) { 
             e.preventDefault(); 
             isSpellCheck = false;
             $('.searchTab').each(function() { 
            	 if($(this).hasClass('active')) { 
            	 		$(this).addClass('active') 
            	 		currentSearch.index = currentSearch.index;
                        currentSearch.queryId =  currentSearch.queryId;
                        currentSearch['class'] =  currentSearch['class'];
                        currentSearch.resultView =  currentSearch.resultView;
                        currentSearch.tabs =  currentSearch.tabs;
                        currentRow = 0;
                       
            	 } 
            	 if((!$('#searchconfigterm').val())) {
            		 currentSearch = {"index" : "", "queryId": "", "class" : "", "resultView" : ""};
                     currentSearch.index = defaultTab.index;
                     currentSearch.queryId = defaultTab["id"];
                     currentSearch['class'] = defaultTab.className;
                     currentSearch.resultView = configFieldMap.tabs[defaultTab.index].resultView;
                     currentSearch.tabs = configFieldMap.tabs;
                     currentRow = 0;
            		 $(this).removeClass('active') 
            		  $(defaultTab.className).addClass('active')
            	 }
             })
            /* currentSearch = {"index" : "", "queryId": "", "class" : "", "resultView" : ""};
             currentSearch.index = defaultTab.index;
             currentSearch.queryId = defaultTab["id"];
             currentSearch['class'] = defaultTab.className;
             currentSearch.resultView = configFieldMap.tabs[defaultTab.index].resultView;
             currentSearch.tabs = configFieldMap.tabs;
             currentRow = 0;
             
             $(defaultTab.className).addClass('active');*/
             if(!enableOverRideOnSearch) {
            	 	overParams = '';
             }
             correctSpellCheckText = '';
             var searchStr = $('#searchconfigterm').val();
             if($('#searchconfigterm').val() && configFieldMap.partialSearch && configFieldMap.partialSearch.enabled && configFieldMap.partialSearch.extra) {
            	 var advArr = configFieldMap.partialSearch.extra.split(',');
            	 var splitString = '';
            	 advArr.map(function(i){
            		 var fieldName = i.split("#")            		
            		 if(fieldName.length > 1 && $('#searchconfigterm').val().toLowerCase().startsWith(fieldName[1].toLowerCase())) {
            			splitString =  fieldName[1];
            			fieldName[1] = fieldName[1].replace(/[^A-Za-z_]/g, "")
            			var param = {"fq" : '' + fieldName[0] + ': "' + fieldName[1] + '"'}; // '+':\"'++'\"'}
            			overParams = JSON.stringify(param);
            		}
            	 });
            	 searchStr = $('#searchconfigterm').val();
            	 if(splitString) {
            		 searchStr = searchStr.toLowerCase().replace(splitString.toLowerCase(), '');
            		 $('.typeahead').typeahead('val', searchStr);
            		 $('#searchconfigterm').val(searchStr)
            	 }
             }
             
             var searchStrClone = searchStr;
             if(isExactSearch === 0) {
            	 	searchStrClone = exactSearchField +  '"' + searchStr +'"';
             } else if(isExactSearch === 2){
            	 	//None of the words
            	 	searchStrClone = '-"' + searchStr +'"';
             } else if(isExactSearch === 1) {
            	 	searchStrClone = searchStr.replaceAll(' ', ' OR ');
             } else {
            	 	searchStrClone = $('#searchconfigterm').val();
             }
             searchSolr('searchAll', searchStrClone, overParams);
         });
     }

     function saveConfig(param) {
    	 if(configFieldMap.defaultSearch) {
        	 configFieldMap.defaultSearch.defaultView = $('#defaultSearch-view').val();
        	 configFieldMap.defaultSearch.shortListView = $('#shortlist-view').val();
    	 }
    	 if(!configFieldMap.partialSearch) {
    		 configFieldMap.partialSearch = {};
    	 }
    	 configFieldMap.partialSearch["extra"] = $('#partial-search-keys').val();
    	 configFieldMap.partialSearch["enabled"] = $('.chkEnable[data-map="partialSearch"]').is(':checked') ? true : false;
    	 searchConfigObject.configFields = configFieldMap;
    	 
         var request = {
             "source": '',
             "database": '',
             "table": '',
             "fact": 'search',
             "customfacts": '',
             "config": JSON.stringify(searchConfigObject),
             "filters": JSON.stringify(configFieldMap),
             "summary": '',
             "remarks": searchConfigObject.description,
             "displayname": searchConfigObject.title,
             "additionalconfig": JSON.stringify(additional_config),
             "type": "search",
             "id": (param == 'clone')? 0: searchConfigObject.id
         };         
         
         queryCollections.push({ "id": request.id, text: request.displayname, extra: request.database });
         callAjaxService("saveProfile", callBackSaveConfig, callBackFailure, request, "POST");
     }

     function callBackSaveConfig(response) {
         if (response && response.isException) {
             showNotification("error", response.customMessage);
             disableLoading();
             return;
         }
         $('#myModalSaveSearch').modal('hide');
         showNotification("success", 'Configuration saved Successfully');
     }
     
     function getShortlists(isDisplay, callBackFn) {
    	 localStorage.setItem('shortLists', JSON.stringify({shortlist: []}));
    	 callAjaxService('searchGetShortLists', function(response) {
    		 shortListedItems = [];
             var key = '',
                 count = 0;
             shortListedIds = [];             
             if (response && response.length > 0) {
            	 shortListedItems = [].concat(response.map(function(a) {
                     if (a.refid) {
                         if (count == 0) {
                             key = 'id:' + a.refid;
                         } else {
                             key += ' OR id:' + a.refid;
                         }
                         count += 1;                         
                         shortListedIds.push(a.refid);
                         return a.refid;
                         
                     }
                 }));
            	 localStorage.setItem('shortLists', JSON.stringify({shortlist: shortListedIds}));
            	 if(isDisplay) {
            		 mode = 'shortlist';
                     currentSearch = {"index" : "", "queryId": "", "class" : "", "resultView" : ""};
                     currentSearch.index = defaultTab.index;
                     currentSearch.queryId = defaultTab["id"];
                     currentSearch['class'] = defaultTab.className;
                     currentSearch.resultView = configFieldMap.defaultSearch.shortListView;
                     currentSearch.tabs = configFieldMap.tabs;
                     currentRow = 0;
                     $(defaultTab.className).addClass('active');
                     subMode = 'shortlist';
            		 searchSolr('searchAll', key);
            	 }            	 
             } else {
            	 if(isDisplay) {
            		 $('#searchTabsContainer').hide();
            		 $('#searchForm').hide();
                	 $('#searchTabContent').html('<div class="alert alert-warning fade in col-md-12"><h3 class="no-margin">No Shortlists Found</h3></i></div>');
            	 }
             }    
             if(callBackFn) {
        		 callBackFn();
        	 }
         }, callBackFailure, {"module": window.location.hash.replace('#', '')}, "POST", null, true); 
     }
               
     function alterShortListItem(event, deleteElement, closestSelector) {
    	 var element = event.target;
    	 if(element.nodeName.toLowerCase() === 'span') {
    		 element = element.parentElement;
    	 }
    	 if($(element).attr('data-solrId')){
    		 if(shortListedIds.indexOf($(element).attr('data-solrId')) > -1) {
    				 shortListedIds.splice(shortListedIds.indexOf($(element).attr('data-solrId')), 1);
    				 localStorage.setItem('shortLists', JSON.stringify({shortlist: shortListedIds}));
    				 $(element).find('.margin-3').removeClass('fa-star');
    				 $(element).find('.margin-3').addClass('fa-star-o');
    				 $(element).find('.margin-3').css('cssText', 'color: #828282 !important');
    				 $(element).removeClass('shortlisted');
    				 $(element).find('.shortlist-text').html('Favourite')
    		 } else {
    			 shortListedIds.push($(element).attr('data-solrId'));
    			 localStorage.setItem('shortLists', JSON.stringify({shortlist: shortListedIds}));
    			 $(element).addClass('shortlisted');
    			 $(element).find('.margin-3').removeClass('fa-star-o');
			 $(element).find('.margin-3').addClass('fa-star');
			 $(element).find('.margin-3').css('cssText', 'color:gold !important');
    			 $(element).find('.shortlist-text').html('Favourited')
    		 }
    		 addShortList($(element).attr('data-solrId'));
    		 if(deleteElement) {
        		 var ele = deleteElement;
        		 if(closestSelector) {
        			 ele = $(element).closest(closestSelector);
        		 }
        		 $(ele).remove();
        	 }
    	 }
     }
     
     function bookMarkSearch() {
         var bookMarkName = $.trim($("#configBookMarkName").val());
//         if (bookMarkName == "")
//             return;
         $("#saveConfigBookmark").addClass("disabled");
         var request = {};
         request.key = currentSearchText;
         request.displayQuery = (!currentSearchText || currentSearchText === '') ? '*' : currentSearchText;
         request.bookmarkDesc = $("#configBookMarkDesc").val();
         request.bookmarkname = bookMarkName;
         request.type = "Bookmark";
         request.moduleName = window.location.hash.replace('#', '')
         enableLoading();
         callAjaxService("markSearch", callBackBookmarkSearchResult,
             callBackFailure, request, "POST");
     }
     
     function callBackBookmarkSearchResult (response) {
         $($("#searchConfigBookmark").parent()).removeClass("open");
         $("#saveConfigBookmark").removeClass("disabled");
         disableLoading();
         if (response && response.isException) {
             showNotification("error", response.customMessage);
             return;
         } else {
        	 if($("#saveConfigBookmark").attr("data-app") == 'saveSearchStrategy') {
        		 showNotification("success", "Save Search Strategy Added Successfully");
        	 } else {
        		 showNotification("success", response.message);
        	 }
         }
     }
          
     function addShortList(ids) {
    	 if(!ids) {
    		 return false;
    	 }
    	 var request = JSON.parse(JSON.stringify(shortListTemplate));
    	 request.solrId = ids;
    	 if(localStorage.getItem('shortLists')) {
			 var shortLists = JSON.parse(localStorage.getItem('shortLists'));
			 if(shortLists.shortlist) {
				 var sIds = shortLists.shortlist;
				 if(sIds.indexOf(request.solrId) > -1) {
					 sIds.splice(sIds.indexOf(request.solrId), 1);
				 } else {
					 sIds.push(request.solrId);
				 }
				 shortLists.shortlist = sIds;
				 localStorage.setItem('shortLists', JSON.stringify(shortLists));
			 }
		 }
    	 enableLoading();
    	 callAjaxService('shortListSearch', function(response) {
    		 if(response && response.isException) {
    			 showNotification("error", response.customMessage);                 
    		 }
    		 disableLoading();
    	 }, callBackFailure, request,"POST");
    	 return true;
     }
        
     function sendEmails (subject, content, files, recepients, callBackFn) { 
         var email = JSON.parse(JSON.stringify(emailTemplate)); //Deep Copy...
         email.subject = subject;
         email.content = content;
         email.files = (files && files !== '""') ? files : null;
         email.recepients = recepients;
         if(!emails) {
        	 emails = [];
         }
         emails.push(email);

    	 if(emails && emails.length > 0) {
    		 for(var i = 0; i < emails.length; i +=1) {
    				var request = {
    						"subject":emails[i].subject,
    						"content":emails[i].content,
    						"recepients":emails[i].recepients,
    						"attachments":emails[i].files,
    						};
    				}
    		  emails = [];
    	 }
    	 if(!callBackFn) {
    		 callBackFn = callbackSucessEmail;
    	 }
    	 enableLoading();
    	 callAjaxService("SendMailWithAttachments",callBackFn,callBackFailure,request,"POST");
     }
     
    function callbackSucessEmail(){
    		disableLoading();
 			$('#myModalEmail').modal('hide');
 			showNotification("success","E-Mail sent Sucessfully..");
 	}    
    
    function openEmailPopup(filesPath, title, content) {    	
    	$('#myModalEmail').modal('show');
		 $('.zoomContainer').remove();
	   	 $('.zoomWindowContainer').remove();
	   	 $('#files').val(filesPath);
	   	 if(content) {
	   		$('#message').val("Source : " + content);
	   	 }
	   	 
	   	 if(title && title != 'undefined') {
	   		$('#subject').val(title);	   		
	   	 }   	 	   		  
	   	 $('#attachmentDiv').hide();
	   	$('#myModalEmail').on('hidden.bs.modal', function () {
 		   prepareElevateZoom();
 		});
	   	$("#emailValidation").validate({
	   	  rules : {
	   	   emailmulti : {
	   	    required : true,
	   	    multiemail: true,
	   	    limitto: 10
	   	   },
	   	   message : {
	   	    required : true
	   	   },
	   	   subject : {
	   	    required : true
	   	   }

	   	  },
	   	  messages : {
	   	   emailmulti : {
	   	    multiemail: "Please enter valid email addresses (separate multiple email addresses using a semicolon)",
	   	     required : 'Please enter your email',
	   	     limitto: "Only maximum of {0} email can be added at a time."
	   	   },
	   	   message : {
	   	    required : 'Please enter your message'
	   	   },
	   	   subject : {
	   	    required : 'Please enter subject'
	   	   }
	   	  },
	   	 });
		 $("#btnSend").html("Send").unbind('click').bind('click', function() {
			var files;
			
			if (!$("#emailValidation").valid()) {
				  return;
			}			
			var recepient = $('#emailmulti').val();
			var subject = $('#subject').val();
			var content = $('#message').val();	
						
			files = $('#files').val() || null;			
		    searchConfig.sendEmails(subject, content, files, recepient);
		   });
    }
    
    function printItems() {  
    	sCart.printImageArea($(".printArea"), true, "<br />");		
    }
    
     return {
         initConfig: function(id) {
             initiateConfig(id);
         },
         initPreview: function(id) {
        	 
        	 getShortlists(false, function() {
        		 initiateView(id);
        	 });
         },
         initView: function(id,config) {
        	 initiateView(id,config);
        	 	getShortlists(false, function() {        		 
        	 		
        	 	});
        	 	
         },
         getShortListItems: function() {
        	 	getShortlists();
         },
         getShortListedItems: function() {
        	 	return  shortListedItems;
         },
         overRideSearch: function(type, param, overRidingObject, callBackFn) {
             currentSearch = {"index" : "", "queryId": "", "class" : "", "resultView" : ""};
             currentSearch.index = defaultTab.index;
             currentSearch.queryId = defaultTab["id"];
             currentSearch['class'] = defaultTab.className;
             currentSearch.resultView = configFieldMap.tabs[defaultTab.index].resultView;
             currentSearch.tabs = configFieldMap.tabs;
             currentRow = 0;
             $(defaultTab.className).addClass('active');
             overParams = overRidingObject;             
        	 	searchSolr(type, param, overRidingObject, callBackFn);
         },
         overRideSearchParam: function(param) {
        	  	if(param) {
        	  		overParams = param;
        	  	}
        		return overParams;
         },
         setExactSearch : function(checked, field) {
        	 	isExactSearch = checked;
        	 	exactSearchField = field;
         },
         setEnableOverRideOnSearch : function(isEnable) {
        	 	enableOverRideOnSearch = isEnable;
         },
         setOverRideSearchParam: function(param) {
        	 	overParams = param;
         },
         setNoResultsFoundText : function(msg) {
        	 	noResultsFoundMsg = msg;
         },
         setcanStoreHistory : function(value) {
        	 	canStoreHistory = !!value;
         },
         setOverRideGA : function(value) {
        	 	isOverrideGA = value;
         },
         globalVariables: function(obj) {
        	 if(!obj) {
        		 return globalVars;
        	 }
        	 for(var i in obj) {
        		 globalVars[i] = obj[i];
        	 }
         },
         setNoResults : function(count) {
        	 	noRows = count;
        	 	pageRows = count;
         },
         setDisableSecondayTabs: function(param){
        	 disableSecondayTabs = param;
         },
         setDistributedCollection: function(param){
        	 isDistributedCollection = param;
         },
         getFacetEntityData: function(key, row, overRidingParam, callback){
        	 $('#entities').unbind('click').bind('click', function() {
            	 currentSearch = {"index" : "", "queryId": "", "class" : "", "resultView" : ""};
                 currentSearch.index = $('.searchTab.active').attr('data-index');
                 currentSearch.enableSC = $('.searchTab.active').attr('data-enable-sr');
                 currentSearch.queryId = $('.searchTab.active').attr('data-query');
                 currentSearch['class'] = '.searchTab-' + $('.searchTab.active').attr('data-index');
                 currentSearch.resultView = configFieldMap.tabs[$('.searchTab.active').attr('data-index')].resultView;
                 currentSearch.tabs = configFieldMap.tabs;
             });
             currentRow = row;
        	 searchSolr('searchAll', key, JSON.stringify(overRidingParam), callback);
         },
         sendEmails:sendEmails,
         alterShortListItem: alterShortListItem,
         addShortList: addShortList,
         openEmailPopup: openEmailPopup,
         printItems: printItems,
         getShortlists: getShortlists,
         loadPageResults: loadPage,
         clearAllShortlists : clearAllShortlists
     };
 };
 
 var getConfigurator = function(cfg, obj, type, isOverrideScroll) {
     searchConfig = new searchConfigurator(cfg, obj, isOverrideScroll);
     return searchConfig;
 };
