//pageSetUp();
var DRL;
var drlInstance = function() {
	DRL = new drlObj();
	DRL.inti();
	$('body').toggleClass("minified");
};

var drlObj = function() {
	var THIS = this;
	var dateFormat = d3.timeFormat("%m/%d/%Y");
	var numberFormat = d3.format(".2f");
	var ndx1;
	var all1;
	var ndx2;
	var all2;
	var moveMonths;
	var volumeByMonthGroup;
	var moleculeGroup;
	var molecule;
	var x;
	var salesMonths;
	var monthlySalesGroup;
	var salesMolecule;
	this.inti = function() {
		enableLoading();
		drawMultiSeriesLineChart();
		drawTimeSeriesSales();
	};

	var drawMultiSeriesLineChart = function() {
		THIS.moveChart = dc.lineChart("#monthly-move-chart1");
		THIS.volumeChart = dc.barChart("#monthly-volume-chart");
		d3.csv("web/data/demo_molecules.csv", function(dataLine) {
			disableLoading();
			dataLine.forEach(function(d) {
				d.dd = dateFormat.parse(d.patent_expiry);
				d.month = d3.time.month(d.dd); // pre-calculate month for
				d.salesVolume = 1; // better performance
				d.salesVolume = +d.salesVolume; // coerce to number
			});

			ndx1 = crossfilter(dataLine);
			all1 = ndx1.groupAll();
			moveMonths = ndx1.dimension(function(d) {
				return d.month;
			});
			var volumeByMonthGroup = moveMonths.group().reduceSum(function(d) {
				return d.salesVolume;
			});

			molecule = ndx1.dimension(function(d) {
				return d.molecule;
			});
			moleculeGroup = molecule.group().reduceSum(function(d) {
				return d.salesVolume;
			});
			x = ndx1.dimension(function(fact) {
				return fact.molecule;
			});
			drawBubbleChart();
			drawMultiSelect();
			var monthlyMoveGroup = moveMonths.group().reduceSum(function(d) {
				return Math.abs(d.salesVolume);
			});

			var indexAvgByMonthGroup = moveMonths.group().reduce(
					function(p, v) {
						++p.days;
						p.total += (v.salesVolume) / 2;
						p.avg = Math.round(p.total / p.days);
						return p;
					}, function(p, v) {
						--p.days;
						p.total -= (v.salesVolume) / 2;
						p.avg = p.days ? Math.round(p.total / p.days) : 0;
						return p;
					}, function() {
						return {
							days : 0,
							total : 0,
							avg : 0
						};
					});
			THIS.moveChart.renderArea(true).width($('#content').width())
					.height(200).transitionDuration(1000).margins({
						top : 30,
						right : 50,
						bottom : 25,
						left : 40
					}).dimension(moveMonths).mouseZoomable(true).rangeChart(
							THIS.volumeChart).x(
							d3.time.scale().domain(
									[ new Date(2008, 0, 1),
											new Date(2033, 11, 31) ])).round(
							d3.time.month.round).xUnits(d3.time.months)
					.elasticY(true).renderHorizontalGridLines(true).legend(
							dc.legend().x(800).y(10).itemHeight(13).gap(5))
					.brushOn(false).group(indexAvgByMonthGroup,
							"Monthly Index Average").valueAccessor(function(d) {
						return d.value.avg;
					}).stack(monthlyMoveGroup, "Monthly Index Move",
							function(d) {
								return d.value;
							}).title(function(d) {
						var value = d.value.avg ? d.value.avg : d.value;
						if (isNaN(value))
							value = 0;
						return dateFormat(d.key) + "\n" + numberFormat(value);
					}).on('filtered', function(chart) {
						THIS.refreshTable(moveMonths);
					});
			THIS.volumeChart.width($('#content').width() * 2).height(200)
					.margins({
						top : 0,
						right : 50,
						bottom : 20,
						left : 40
					}).dimension(moveMonths).group(volumeByMonthGroup)
					.centerBar(true).gap(1).x(
							d3.time.scale().domain(
									[ new Date(2008, 0, 1),
											new Date(2033, 11, 31) ])).round(
							d3.time.month.round).alwaysUseRounding(true)
					.xUnits(d3.time.months);
			dc.dataCount(".dc-data-count").dimension(ndx1).group(all1);
			dc.renderAll();
		});

	};
	var drawTimeSeriesSales = function() {
		THIS.salesChart = dc.lineChart("#monthly-sales-chart");
		d3.csv("web/data/demo_sales_sample.csv", function(dataSales) {
			disableLoading();
			/* since its a csv file we need to format the data a bit */
			var dateFormat = d3.timeFormat("%m/%d/%Y");
			var numberFormat = d3.format(".2f");
			dataSales.forEach(function(d) {
				d.dd = dateFormat.parse(d.salesdate);
				d.month = d3.time.month(d.dd); // pre-calculate month for
				d.salesvalue = +d.salesvalue; // coerce to number
			});

			ndx2 = crossfilter(dataSales);
			all2 = ndx2.groupAll();

			salesMonths = ndx2.dimension(function(d) {
				return d.month;
			});
			salesMolecule = ndx2.dimension(function(d) {
				return d.molecule;
			});
			salesMoleculeGroup = salesMolecule.group().reduceSum(function(d) {
				return d.salesvalue;
			});

			drawTime();

		});
	};

	var drawTime = function() {
		monthlySalesGroup = salesMonths.group().reduceSum(function(d) {
			return d.salesvalue;
		});
		indexAvgByMonthGroup = salesMonths.group().reduce(function(p, v) {
			++p.days;
			p.total += v.salesvalue;
			// p.avg = Math.round(p.total / p.days);
			return p;
		}, function(p, v) {
			--p.days;
			p.total -= v.salesvalue;
			// p.avg = p.days ? Math.round(p.total / p.days) : 0;
			return p;
		}, function() {
			return {
				days : 0,
				total : 0
			};
		});

		THIS.salesChart.renderArea(true).width($('#content').width()).height(
				200).transitionDuration(1000).margins({
			top : 30,
			right : 50,
			bottom : 25,
			left : 100
		}).dimension(salesMonths).mouseZoomable(true).x(
				d3.time.scale().domain(
						[ new Date(2001, 0, 1), new Date(2014, 11, 31) ]))
				.round(d3.time.month.round).xUnits(d3.time.months).elasticY(
						true).renderHorizontalGridLines(true).legend(
						dc.legend().x(800).y(14).itemHeight(13).gap(2))
				.brushOn(false).group(indexAvgByMonthGroup,
						"Monthly Revenue").valueAccessor(function(d) {
					return d.value.total;
				}).title(function(d) {
					var value = d.value.total ? d.value.total : d.value;
					if (isNaN(value))
						value = 0;
					return dateFormat(d.key) + "\n" + numberFormat(value);
				});

		dc.dataCount(".dc-data-count").dimension(ndx2).group(all2);
		dc.renderAll();
		drawWorldMap();
		drawEasyPie();
		salesMolecule.filterAll();
	};
	var drawMultiSelect = function() {
		var multiSelectList = moleculeGroup.top(10000).sort(function(a, b) {
			return b.value - a.value;
		});
		var moleculeList = new Array();
		multiSelectList.forEach(function(node) {
			if (node.value > 0) {
				moleculeList.push({
					id : node.key,
					text : node.key
				});
			}
		});

		$("#moleculeSearch").select2({
			data : moleculeList,
			multiple : true,
			width : "300px"
		});

		$('#moleculeSearch').on("select2-selecting", function(e) {
			var filterValues = $('#moleculeSearch').select2('val');
			filterValues.push(e.val);
			x.filter(function(d) {
				return filterValues.indexOf(d) > -1;
			});
			salesMolecule.filter(function(d) {
				return filterValues.indexOf(d) > -1;
			});
			$(".bubble").css("fill-opacity","0.2");
			drawBubbleChart();
			drawTime();
		});

		$('#moleculeSearch').on("select2-removing", function(e) {
			var filterValues = $('#moleculeSearch').select2('val');
			if (filterValues.length > 0) {
				x.filter(function(d) {
					return filterValues.indexOf(d) > -1;
				});
				salesMolecule.filter(function(d) {
					return filterValues.indexOf(d) > -1;
				});
			}
			;
			drawBubbleChart();
			drawTime();
		});
	};
	var drawEasyPie = function() {
		$('.easy-pie-chart').easyPieChart({
			"barColor" : "#4D7686",
			"trackColor" : false,
			"scaleColor" : false,
			"size" : 100,
			"lineWidth" : 10
		});
	};
	var drawWorldMap = function() {
		console.log(ndx2.groupAll().reduceSum(function(fact) {
			return fact.salesvalue;
		}).value());
		var data_array = {
			"US" : ndx2.groupAll().reduceSum(function(fact) {
				return fact.salesvalue;
			}).value()
		};
		$('#world-map').empty();
		$('#world-map').vectorMap({
			map : 'world_mill_en',
			backgroundColor : '#fff',
			regionStyle : {
				initial : {
					fill : '#c4c4c4'
				},
				hover : {
					"fill-opacity" : 1
				}
			},
			series : {
				regions : [ {
					values : data_array,
					scale : [ '#85a8b6', '#4d7686' ],
					normalizeFunction : 'polynomial'
				} ]
			},
			onRegionLabelShow : function(e, el, code) {
				if (typeof data_array[code] == 'undefined') {
					e.preventDefault();
				} else {
					var countrylbl = data_array[code];
					el.html(el.html() + ': ' + countrylbl);
				}
			}
		});
	};
	var drawBubbleChart = function() {

		var diameter = 540, format = d3.format(",d"), color = d3.scale
				.category20c();

		var bubble = d3.layout.pack().sort(null).size([ diameter, diameter ])
				.padding(1.5);
		$("#bubbleChart svg").remove();
		var svg = d3.select("#bubbleChart").append("svg").attr("width",
				diameter).attr("height", diameter).attr("class", "bubble");
		moleculeGroup.top(1000);
		var root = {
			"name" : "bubble",
			"children" : moleculeGroup.top(10000)
		/*
		 * .sort(function(a, b) { return b.value - a.value; })
		 */
		};
		var node = svg.selectAll(".node").data(
				bubble.nodes(classes(root)).filter(function(d) {
					return !d.children;
				})).enter().append("g").attr("class", "node").attr("transform",
				function(d) {
					return "translate(" + d.x + "," + d.y + ")";
				});

		node.append("title").text(function(d) {
			return d.className + ": " + format(d.value);
		}) 

		node.append("circle").attr("r", function(d) {
			return d.r;
		}).style("fill", function(d) {
			return color(d.className);
		}).on("mousedown",clickOnCircleFunc).on("touchstart",clickOnCircleFunc)
	    .attr("r", function(d) {return d.r;})
	    .attr("class", "nodecircle")
	    .attr("data-classname", function(d) {return d.className;});

	    

		node.append("text").attr("dy", ".3em").style("text-anchor", "middle")
				.text(function(d) {
					return d.className.substring(0, d.r / 3);
				}) .on("mousedown",clickOnCircleFunc).on("touchstart",clickOnCircleFunc)
			    .attr("text-anchor", "middle")
			    .attr("class", "nodetext")
			    .attr("data-classname", function(d) {return d.className;})
			  /*  .attr("style", function(d) {var szd = d.r/5;return "font-size:" + szd+"px";})*/;

		// Returns a flattened hierarchy containing all leaf
		// nodes under the root.
		function classes(root) {
			var classes = [];

			function recurse(name, node) {
				if (node.children)
					node.children.forEach(function(child) {
						recurse(node.name, child);
					});
				else if (node.value > 0)
					classes.push({
						packageName : name,
						className : node.key,
						value : node.value
					});
			}

			recurse(null, root);
			// drawMultiSelect(moleculeList);
			if (classes.length > 10){
				$("#divNews .widget-body").removeClass("hide");
				$("#tableDiv > table>tbody").removeClass("hide");
			} else {
				$("#divNews .widget-body").addClass("hide");
				$("#tableDiv > table>tbody").addClass("hide");
				var divIds= "";
				var tabledivIds= "";
				for (var l=0;l<classes.length;l++){
					if (l==0){
						divIds = "#"+classes[l]["className"].toLowerCase().replace(" ","");
						tabledivIds = "#table"+classes[l]["className"].toLowerCase().replace(" ","");
					}
					else{
						divIds += ", #"+classes[l]["className"].toLowerCase().replace(" ","");
						tabledivIds += ", #table"+classes[l]["className"].toLowerCase().replace(" ","");
					}
				}
				//divIds = "'"+ divIds +"'";
				if ($(divIds).length > 0){
					$(divIds).removeClass("hide");
					$(tabledivIds).removeClass("hide");
				}
				else {
					$("#divNews .widget-body").removeClass("hide");
					$("#tableDiv > table>tbody").removeClass("hide");
				}
			}
			
			
			return {
				children : classes
			};
		}
		
		x.filterAll();

		d3.select(self.frameElement).style("height", diameter + "px");
	};
	function clickOnCircleFunc(){
		  d3.event.preventDefault();
		  d3.event.stopPropagation();
		  $this = $(this);
	
		/*   $(".nodecircle[data-select^=active]").css('fill-opacity', 1);*/
		  if(d3.event.target.nodeName === "text" || d3.event.target.nodeName === "tspan"){
		    var asdf = $(this).attr('data-classname');
		    $this = $('circle[data-classname="'+asdf+'"]');
		  }
		    if ($this.attr('data-select') !== "active") {
		      $this.attr('data-select','active').css('fill-opacity', 1);
		      $this.next().css('fill-opacity', 1);
		    }
		    else {
		      $this.attr('data-select','inactive').css('fill-opacity', 0.2);
		      $this.next().css('fill-opacity', 0.2);
		    }
		    if ( $(".nodecircle[data-select^=active]").length > 0){
		    	$(".nodecircle[data-select!=active]").css('fill-opacity', 0.2);
				$(".nodecircle[data-select!=active]").next().css('fill-opacity', 0.2);
		    	var filterValues = new Array();
			    $(".nodecircle[data-select^=active]").each(function(){
			    	filterValues.push($(this).attr('data-classname'));
			    });
			    salesMolecule.filter(function(d) {
					return filterValues.indexOf(d) > -1;
				});
			    
			    
			    if (filterValues.length > 10){
					$("#divNews .widget-body").removeClass("hide");
					$("#tableDiv > table>tbody").removeClass("hide");
				} else {
					$("#divNews .widget-body").addClass("hide");
					$("#tableDiv > table>tbody").addClass("hide");
					var divIds= "";
					var tabledivIds= "";
					for (var l=0;l<filterValues.length;l++){
						if (l==0){
							divIds = "#"+filterValues[l].toLowerCase().replace(" ","");
							tabledivIds = "#table"+filterValues[l].toLowerCase().replace(" ","");
						}else{
							divIds += ", #"+filterValues[l].toLowerCase().replace(" ","");
							tabledivIds += ", #table"+filterValues[l].toLowerCase().replace(" ","");
						}
					}
					//divIds = "'"+ divIds +"'";
					if ($(divIds).length > 0){
						$(divIds).removeClass("hide");
						$(tabledivIds).removeClass("hide");
					}
					else{ 
						$("#divNews .widget-body").removeClass("hide");
						$("#tableDiv > table>tbody").removeClass("hide");
					}
				}
		    } else {
		    	$("#divNews .widget-body").removeClass("hide");
		    	$(".nodecircle[data-select!=active]").css('fill-opacity', 1);
				$(".nodecircle[data-select!=active]").next().css('fill-opacity', 1);
				$("#tableDiv > table>tbody").removeClass("hide");
		    }
		    drawTime();
		}
	this.refreshTable = function() {
		drawBubbleChart();
		drawMultiSelect();
	};
};
