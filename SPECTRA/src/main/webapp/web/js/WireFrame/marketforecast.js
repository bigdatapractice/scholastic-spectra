(function(){
	pageSetUp();
	var This  = {};
	This.source = "hive";
	This.database = "APOTEXDB";
	/*if(location.hash === "#marketforecast_hive"){
		This.source = "hive";
		This.database = "APOTEXDB";
	}*/
	$("#bootstrap-wizard-1").find("a[href='#planning'],[href='#financials']").css("opacity",0.4);
	$("#downloadUnitExcel").click(function(){
		downloadUnitExcel();
	});
	$("#downloadFinExcel").click(function(){
		downloadFinExcel();
	});
	$("#editForecast").unbind("click").bind("click",function(){
		$("#searchTabContent").find("input,select,textarea").removeAttr("disabled");
		$("#searchTabContent").find("svg").css({"pointer-events":"all"});
		$("#searchTabContent").find(".slider-horizontal").css({"pointer-events":"all"});
		$("#searchTabContent").find("a[mode='edit']").show();
		$("#strenthContent").find("a").css({"pointer-events":"all"});
		$(this).hide();
		 if(!$("#editForecast").is(':visible'))
			  $("#reprocessForecast").show();
		 else
			 $("#reprocessForecast").hide();
	});
	$('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
		 // e.target // activated tab
		  //e.relatedTarget // previous tab
		  $(".d3-tip").css("opacity",0);
		  $("#bootstrap-wizard-1").find("a").css("opacity",0.4);
		  $(e.target).css("opacity",1);
		  if($(e.target).attr("href") === "#hierarchy"){
			  $("#btnPrevious").parent().addClass("disabled");
		  }else{
			  $("#btnPrevious").parent().removeClass("disabled");  
		  }
		  if($(e.target).attr("href") === "#financials"){
			  $("#btnNext").parent().addClass("disabled");
		  }else{
			  $("#btnNext").parent().removeClass("disabled");  
		  }
		  if($(e.target).attr("href") === "#planning" || $(e.target).attr("href") === "#financials"){
			  if(!$("#editForecast").is(':visible'))
				  $("#reprocessForecast").show();  
		  }else{
			  $("#reprocessForecast").hide();
		  }
		  if($(e.relatedTarget).attr("href") === "#hierarchy" && !This.myForecast){
			  var selectedStrengthAndPacks = getFilteredData("",true);
			  var strengthAndPacks = {};
			  $("[name=marketShareTable] tbody").find("tr").each(function(){
				  var strength = $(this).attr("strength");
				  var packSize = $(this).attr("packSize");
				  if(!strengthAndPacks[strength])
					  strengthAndPacks[strength] = [];
				  strengthAndPacks[strength].push(packSize);
			  });
			  if(JSON.stringify(strengthAndPacks) !== JSON.stringify(selectedStrengthAndPacks)){
				  constructDropDown("[name='competitorsName']","select distinct qplan_mfr,CASE  WHEN labeller_type ='A' THEN '(Approved)' ELSE '' END from l2_aptx_qplan_source where labeller_type in ('M','A') and lower(qplan_molecule_name) = '"+This.productKey.toLowerCase()+"' and source_mfr like '%<Q>%'",true,null,null,"Competitor",This.source,This.database,{buttonWidth:236});
				  //processPlanningAssumptions();
				  getForecastMetaData(["PRICE, PLANNING, FINANCIAL"]);
			  }
		  }
	});
	$("#backForecast").unbind("click").bind("click",function(){
		$("#search-body").hide();
		$("#serach-param").show();
		$(".d3-tip").css("opacity",0);
	});
	$("#reprocessForecast").unbind("click").bind("click",function(){
		var href = $("#bootstrap-wizard-1").find("ul").find("li.active").find("a").attr("href");
		if(href === "#planning"){
			if(This.forecastType === "presubmission")
				getForecastMetaData(["PLANNING","FINANCIAL"]);
			else
				getForecastMetaData(["PLANNING"]);
		}
		else if(href === "#financials")
			getForecastMetaData(["FINANCIAL"]);
	});
	$("[name='planningHorizon']").slider({
		min: 12,
		max: 60,
		value:36,
		//tooltip: 'always',
		//tooltip: 'show',
		//scale: 'logarithmic',
		step: 6
	}).on("slide", function(slide) {
		$("[name='planningHorizon']").parent().prev().find("strong").html(slide.value+" Months");
	});
	$("[name='revisionPeriodValue']").slider({
		min: 3,
		max: 12,
		value:12,
		//tooltip: 'always',
		//tooltip: 'show',
		//scale: 'logarithmic',
		step: 3
	}).on("slide", function(slide) {
		$("[name='revisionPeriodValue']").parent().prev().find("strong").html(slide.value+" Months");
	});
	$("#btnPrevious").unbind("click").bind("click",function(){
		var hrefObj = {
			"#financials":"#planning",
			"#planning":"#hierarchy"
		};
		var href = $("#bootstrap-wizard-1").find("ul").find("li.active").find("a").attr("href");
		if(hrefObj[href])
			$("a[href='"+hrefObj[href]+"']").trigger("click");
	});
	$("#btnNext").unbind("click").bind("click",function(){
		var href = $("#bootstrap-wizard-1").find("ul").find("li.active").find("a").attr("href");
		var hrefObj = {
			"#hierarchy":"#planning",
			"#planning":"#financials"
		};
		if(hrefObj[href])
			$("a[href='"+hrefObj[href]+"']").trigger("click");
	});
	/*$("[name=strengthSelectAll]").click(function(){
		if ($(this).is(":checked")){
			dc.chartRegistry.list()[0].filterAll();
			dc.redrawAll();
			$("[name='packContent']").find(".panel-default").each(function(){
				$(this).find("h4").find("a").removeClass("collapsed");
				$(this).find(".panel-collapse").addClass("in");
			});
		}
		else{
			dc.chartRegistry.list()[0].filter('');
			dc.redrawAll();
			$("[name='packContent']").find(".panel-default").each(function(){
				$(this).find("h4").find("a").addClass("collapsed");
				$(this).find(".panel-collapse").removeClass("in");
			});
		}
	});*/
	var getYearAndMonthsByDates= function(start,end){
		var dates = {};
		for (var i = start.getFullYear(); i < end.getFullYear() + 1; i++) {
			while(start <= end){
				if(!dates[start.getFullYear()])
					dates[start.getFullYear()] = [];
				dates[start.getFullYear()].push(start.getMonth()+1);
				start.setMonth(start.getMonth()+1);
			}
		}
		return dates;
	};
	var saveForeCast = function(isUpdate){
		var name = $("[name=forecastname]").val();
		var gpiName = $("[name='searchdropdown']").find("select").val();
		var packSize = getFilteredData("strength",true);
		var strength = Object.keys(packSize);
		packSize = JSON.stringify(packSize);
		var combination =  $("[name='searchdropdown']").find("ul").find(".active ").text();
		//var targetMarketShare= $("[name='currentApotexMarketShare']").val();
		var revisionPeriodType = $("[name='marketsharetrend']").val();
		var revisionPeriod = $("[name='revisionPeriodValue']").val();
		/*var substitutionRate = $("[name='substitutionRate']").val();
		var substitutionGrowthRate = $("[name='substitutionGrowthRate']").val();
		var annualPriceErosion = $("[name='annualPriceErosion']").val();*/
		var planningCycleDate = $("[name='planningCycleStart']").val();
		var submissionDate = $('[name=submissionDate]').val();
		var planningMonth =  $('[name=planningMonth]').val() || 0;
		var marketSizeRate = $("[name='marketSizeRate']").val();
		var forecastType = $("[name='forecastType']").val();
		var competitorType = $("[name='competitorType']").val();
		var pricingSource =  $("[name='pricingsourcecombo']").val();
		var cogs = $("[name='cogs']").val();
		var pricesType = $("[name='pricesType']").val();
		var units = $("[name='pricesType']").val();
		var forecastPlanType = This.forecastType || $("[name='forecastPlantype']:checked").val();
		var planingScenario = "";
		var financials = {};
		var planningHorizon = $("[name='planningHorizon']").val();
		var sharetrand = $("[name='marketshareprice']").val() || 0;
		//var unitsCalculationType = {};
		var headername = {};
		var counter = 0;
		var marketShares = [];
		if(This.forecastType === "presubmission"){
			planingScenario = $("[name='forecastPlanScenario']").find("select").val();
			$("[name=marketShareTable] tbody tr").each(function(){
				var marketShare = {
					"strength": $(this).find("td:nth-child(1)").text(),
					"packsize": $(this).find("td:nth-child(2)").text(),
					"apotexGmShare": $(this).find("td:nth-child(3) input").val(),
					"gmConversion": $(this).find("td:nth-child(4) input").val(),
					"initialApotexPrice": $(this).find("td:nth-child(5) input").val(),
					"apotexPriceChange": $(this).find("td:nth-child(6) input").val(),
					"initialRebateLevel":$(this).find("td:nth-child(7) input").val(),
					"year1": $(this).find("td:nth-child(8) input").val(),
					"year2": $(this).find("td:nth-child(9) input").val(),
					"year3": $(this).find("td:nth-child(10) input").val(),
					"year4": $(this).find("td:nth-child(11) input").val(),
					"year5": $(this).find("td:nth-child(12) input").val(),
					"launch_price": $(this).find("td:nth-child(13) input").val()
				};
				marketShares.push(marketShare);
			});
		}else{
			$("[name=marketShareTable] tbody tr").each(function(){
				var marketShare = {
					"strength": $(this).find("td:nth-child(1)").text(),
					"packsize": $(this).find("td:nth-child(2)").text(),
					"marketshare": $(this).find("td:nth-child(3) input").val(),
					"subsitutitionrate": $(this).find("td:nth-child(4) input").val(),
					"substitutiongrowthrate": $(this).find("td:nth-child(5) input").val(),
					"priceerosionrate": $(this).find("td:nth-child(6) input").val(),
					"price":$(this).find("td:nth-child(7) input").val(),
					"df":$(this).attr("df")
				};
				marketShares.push(marketShare);
			});
		}
		$("[name=financialTable] tbody tr").each(function(tr){
			var strength = $(this).attr("strength"),
			packsize = $(this).attr("packsize");
			if(strength){
				if(!financials[strength])
					financials[strength] = {};
				if(!financials[strength][packsize])
					financials[strength][packsize] = {};
				$(this).find("td").each(function(){
					var month = $(this).attr("month");
					if(month){
						financials[strength][packsize][month] = {
							unit:$(this).attr("quantity"),
							revenue:$(this).attr("revenue")
						};
					}
				});
			}
		});
		if(This.myForecast){
			/*var isEdited = false;
			if(This.myForecast.name !== name)
				isEdited = true;
			if(This.myForecast.strength !== strength.join(","))
				isEdited = true;*/
			if(This.myForecast.planingScenario){
				planingScenario = This.myForecast.planingScenario;
			}
		}
		var totalRevenue = $("#financials").data("totalrevenue");
		var planStatus = $("[name='planStatus']").find("input[name='status']:checked").val();
		var forecastVersion = {
				"gpiName":gpiName,
				"name":name,
				"description":$("[name='planDescription']").val()||"",
				"strength":strength.join(","),
				"packSize":packSize,
				"shareTrend":sharetrand,
				"combination":combination,
				"forecastPlanType":forecastPlanType,
				"planingScenario":planingScenario,
				//"targetMarketShare":parseInt(targetMarketShare),
				"totalRevenue":totalRevenue?parseInt(totalRevenue):0,
				//"monthRange":monthRange,
				//"shareTrend":parseInt(shareTrend),
				"revisionPeriodType":revisionPeriodType,
				"revisionPeriod":parseInt(revisionPeriod),
				"planStatus":planStatus,
				//"substitutionRate":parseInt(substitutionRate),
				//"substitutionGrowthRate":parseInt(substitutionGrowthRate),
				//"annualPriceErosion":parseInt(annualPriceErosion),
				"planningCycleDate":planningCycleDate,
				"submissionDate":submissionDate,
				"planningMonth":planningMonth,
				"marketSizeRate":marketSizeRate,
				"forecastType":forecastType,
				"competitorType":competitorType,
				"pricingSource":pricingSource,
				"cogs":cogs,
				"pricesType":pricesType,
				//"units":units,
				"financials":JSON.stringify(financials),
				"planningHorizon":parseInt(planningHorizon),
				//"unitsCalculationType":JSON.stringify(unitsCalculationType),
				"marketShares":JSON.stringify(marketShares),
				"createdBy": System.userName
		};
		if(This.myForecast && This.myForecast.id && isUpdate){
			forecastVersion.id = This.myForecast.id;
			forecastVersion.version = This.myForecast.version;
			var request = {forecastVersion : JSON.stringify(forecastVersion)};
			callAjaxService("updateForecast", function(response){
				showNotification("success", response);
				$("#myModalsaveForecast").modal('hide');
				$("#search-body").hide();
				$("#serach-param").show();
				checkSearchKey();
			},callBackFailure, request, "POST", null, true);
		}else{
			forecastVersion.id = 0;
			var request = {forecastVersion : JSON.stringify(forecastVersion)};
			callAjaxService("addForecast", function(response){
				showNotification("success", response);
				$("#myModalsaveForecast").modal('hide');
				$("#search-body").hide();
				$("#serach-param").show();
				checkSearchKey();
			},callBackFailure, request, "POST", null, true);
		}
	};
	$("#saveForecast").unbind("click").bind("click",function(){
		var name = $("[name=forecastname]").val();
		if(name === "" || name.trim() === ""){
			showNotification("notify","Please enter forecast name");
			return;
		}
		if(This.myForecast && This.myForecast.id){
			$("#myModalsaveForecast").modal('show');
			$("#myModalsaveForecast").find("[name='saveForecastData']").unbind("click").bind("click",function(){
				if($(this).attr("type") === "update"){
					saveForeCast(true);
				}else{
					saveForeCast();
				}
			});
		}else{
			saveForeCast();
		}		
	});
	/*$("[href='#collapseThree']").unbind("click").bind("click",function(){
		if(!$("#collapseThree").is(':visible'))
			loadSalesBoard();
	});*/
	$("#backSalesDashBoard").unbind("click").bind("click",function(){
		$("#wid-id-details").show();
		$("#salesDashboardDiv").hide();
	});
	$("[name='viewIMSTable']").unbind("click").bind("click",function(){
		$("#wid-id-details").hide();
		$("#salesDashboardDiv").show();
		loadSalesBoard();
	});
	var loadSalesBoard = function(){
		loadScript("js/WireFrame/salesDashboard.js", function() {
			var filters = " qplan_name in ('"+This.productKey+"')";
			$("#divsalesDashboard").empty();
			$("#divsalesDashboard").append($("<div/>").attr({"id":"trendchart"}));
			var selectedStrength = getFilteredData("strength",true);
			if(selectedStrength && Object.keys(selectedStrength).length>0){
				filters+=" AND (";
				var j=0;
				for(strength in selectedStrength){
					if(selectedStrength.hasOwnProperty(strength)){
						if(j>0)
							filters+=" OR "
						filters+=" ( strength = '"+strength+"'";
						if(selectedStrength[strength].length>0){
							filters+=" AND pack_size IN ("+selectedStrength[strength].map(function(e) {return "'" + e.replace(/ /g,'')+ "'"}).toString()+")";
						}
						filters+=" )";
						j++;
					}
				}
				filters+=" )";
			}
			var sb = new salesDashboard(filters);
			sb.load();
		});
	};
	$("[name='viewpricingTable']").unbind("click").bind("click",function(){
		$("#myModalLabel").find('strong').html("Price :"+This.productKey);
		//var filterData = getFilteredData("ndc");
		var profileKey = $(this).attr("profilekey");
		if(profileKey === "pricerx_qplan"){
			$("#myModalLabel span").show();
		}else{
			$("#myModalLabel span").hide();
		}
		loadScript("js/DataAnalyser/DataAnalyser.js", function() {
			var filters = " 1=1 ";
			var selectedStrength = getFilteredData("strength",true);
			if(selectedStrength && Object.keys(selectedStrength).length>0){
				filters+=" AND (";
				var j=0;
				for(strength in selectedStrength){
					if(selectedStrength.hasOwnProperty(strength)){
						if(j>0)
							filters+=" OR "
						filters+=" ( strength = '"+strength+"'";
						if(selectedStrength[strength].length>0){
							filters+=" AND pack_size IN ("+selectedStrength[strength].map(function(e) {return "'" + e.replace(/ /g,'')+ "'"}).toString()+")";
						}
						filters+=" )";
						j++;
					}
				}
				filters+=" )";
			}
			var moleculeName = "";
			if(This.productKey){
				moleculeName = This.productKey.split("-");
				moleculeName = (moleculeName.length>0 && moleculeName[1])?moleculeName[1].trim():""; 
			}
            dataAnalyser = new DataAnalyser();
            dataAnalyser.getProfileForModal({linkedProfileKey:profileKey},filters,{qplan_name:This.productKey,molecule_name:moleculeName});
        });		
	});
	var getFilteredData = function(columnName,stengthAndPacks){
		var strength = {},filterData;
		if(This.forecastType === "presubmission"){
			$("[name='preSubmission']").find("table").find("tbody").find("tr").each(function(){
				var strengthValue = $(this).find("[name='strength']").val();
				var packsizeValue = $(this).find("[name='packsize']").val();
				if(strengthValue && packsizeValue){
					strength[strengthValue] = packsizeValue.split(',');
				}
			});
			return strength;
		}else{
			/*var allCharts = dc.chartRegistry.list();
			var strengthChart  = allCharts.filter(function(chart){return chart.anchor() === "#strenthContent"});
			var selectedStrength = strengthChart[0].filters();*/
			$("#strenthContent").find("table").each(function(){
				var strengthName = $(this).attr("name");
				$(this).find("a").each(function(){
					var isSelected = $(this).data("isSelected");
					if(isSelected){
						if(!strength[strengthName])
							strength[strengthName] = [];
						strength[strengthName].push($(this).attr("name"));
					}
				});
			});
			/*for(var i=0;i<selectedStrength.length;i++){
				var packChart = allCharts.filter(function(chart){return chart.anchor() === "#packSizeChart_"+selectedStrength[i].replace(/[%./#-]/gi, '_')});
				if(packChart && packChart.length>0){
					strength[selectedStrength[i]] = packChart[0].filters();
					strength[selectedStrength[i]] = strength[selectedStrength[i]].filter(function(o){ return o !== ""});
				}
			}*/
			var filterProductMasters = This.productMasters.filter(function(o,index,self){ 
				return (strength[o.strength] && (strength[o.strength].length === 0 || strength[o.strength].indexOf(o.packsize)) > -1);
			});
			if(stengthAndPacks){
				filterData = strength;
			}else{
				if(columnName)
					filterData = filterProductMasters.map(function(o){return o[columnName]}).filter(function(obj,index,self){return self.indexOf(obj) == index});
				else
					filterData = filterProductMasters;
			}
			return filterData;
			
		}
	};
	var constructMarketShareGrid =function(marketShareData,isSaved){
		$("[name=marketShareTable] tbody").empty();
		marketShareData.sort(function(a,b){
			if(a.strength+a.packsize >b.strength+b.packsize){
				return 1;
			}
			else 
				return -1;
		});
		var strengthPacks = getFilteredData("",true);
		var savedMarketShare = [];
		if(This.myForecast && This.myForecast.marketShares){
			savedMarketShare = Jsonparse(This.myForecast.marketShares);
		}
		var priceColumn = $("[name='pricingsourcecombo']").val();
		var priceeaderText = $("[name='pricingsourcecombo']").find("option[value='"+priceColumn+"']").html()|| "IMS";
		if(This.forecastType === "postsubmission")
			$("[name='marketShareTable']").find("th:last").html("Price in $ ("+priceeaderText+")");
		if(strengthPacks && Object.keys(strengthPacks).length>0){
			for(strength in strengthPacks){
				if(strengthPacks.hasOwnProperty(strength) && strengthPacks[strength].length>0){
					for (var i=0; i< strengthPacks[strength].length ; i++){
						var packSize = strengthPacks[strength][i],priceValue=0,savedData = {};
						data = marketShareData.filter(function(obj){ return obj.strength === strength && obj.packsize === strengthPacks[strength][i]});
						if(data && data.length>0)
							data = data[0];
						else
							data = {};
						if(This.productPrices && This.productPrices.length>0){
							var filteredData = This.productPrices.filter(function(obj){ return obj.strength === strength && obj.packsize === packSize});
							var pricetype = $("[name='pricesType']").val();
							if(pricetype === "simpleAvg"){
								priceValue= d3.mean(filteredData, function(o){return o[priceColumn]});
							}else if(pricetype === "minimum"){
								priceValue = Math.min.apply(Math,filteredData.map(function(o){return o[[priceColumn]];}));
							}
							data.price = priceValue?d3.format(".2f")(priceValue):"";
						}
						var selectedProductmaster = This.productMasters.filter(function(o){return o.strength === strength && o.packsize === packSize});
						if(selectedProductmaster && selectedProductmaster.length>0){
							selectedProductmaster = selectedProductmaster.map(function(o){return o.df});
							selectedProductmaster = selectedProductmaster.filter(function(item, pos) {
							    return selectedProductmaster.indexOf(item) == pos;
							});
						}
							
						if(!isSaved){
							if(This.forecastType === "presubmission"){
								data = (marketShareData && marketShareData.length>0)?marketShareData[0]:{};
							}else{
								savedData = savedMarketShare.filter(function(obj){ return obj.strength === strength && obj.packsize === strengthPacks[strength][i]});
								if(savedData.length>0){
									savedData = savedData[0];
								}
								data.marketshare = savedData.marketshare?savedData.marketshare:data.marketshare?d3.format(".2f")(data.marketshare*100):"";
								data.subsitutitionrate = savedData.subsitutitionrate?savedData.subsitutitionrate:data.subsitutitionrate?d3.format(".2f")(data.subsitutitionrate):"";
								data.substitutiongrowthrate = savedData.substitutiongrowthrate?savedData.substitutiongrowthrate:data.substitutiongrowthrate?d3.format(".2f")(data.substitutiongrowthrate):"";
								data.priceerosionrate = savedData.priceerosionrate?savedData.priceerosionrate:data.priceerosionrate?d3.format(".2f")(data.priceerosionrate):"";
								data.price = savedData.price?savedData.price:data.price?d3.format(".2f")(data.price):""; 
							}
						}
						if(This.forecastType === "presubmission"){
							$("[name=marketShareTable] tbody").append(
									$("<tr/>").attr({"strength":strength,"packSize":packSize})
									.append( $("<td/>").append(strength))	
									.append($("<td/>").append(packSize))
									.append($("<td/>").append($("<input/>").addClass("form-control").css({"text-align":"right"}).attr({"type":"number","col":"3","name":"apotex_gm_share"+strength.replace(/[%./#-]/gi, '_')+"_"+packSize}).val(data.apotexGmShare)))
									.append($("<td/>").append($("<input/>").addClass("form-control").css({"text-align":"right"}).attr({"type":"number","col":"4","name":"gm_conversion"+strength.replace(/[%./#-]/gi, '_')+"_"+packSize}).val(data.gmConversion)))
									.append($("<td/>").append($("<input/>").addClass("form-control").css({"text-align":"right"}).attr({"type":"number","col":"5","name":"initial_apotex_price"+strength.replace(/[%./#-]/gi, '_')+"_"+packSize}).val(data.initialApotexPrice)))
									.append($("<td/>").append($("<input/>").addClass("form-control").css({"text-align":"right"}).attr({"type":"number","col":"6","name":"apotex_price_change"+strength.replace(/[%./#-]/gi, '_')+"_"+packSize}).val(data.apotexPriceChange)))
									.append($("<td/>").append($("<input/>").addClass("form-control").css({"text-align":"right"}).attr({"type":"number","col":"7","name":"initial_rebate_level"+strength.replace(/[%./#-]/gi, '_')+"_"+packSize}).val(data.initialRebateLevel)))
									.append($("<td/>").append($("<input/>").addClass("form-control").css({"text-align":"right"}).attr({"type":"number","col":"8","name":"year1"+strength.replace(/[%./#-]/gi, '_')+"_"+packSize}).val(data.year1)))
									.append($("<td/>").append($("<input/>").addClass("form-control").css({"text-align":"right"}).attr({"type":"number","col":"8","name":"year2"+strength.replace(/[%./#-]/gi, '_')+"_"+packSize}).val(data.year2)))
									.append($("<td/>").append($("<input/>").addClass("form-control").css({"text-align":"right"}).attr({"type":"number","col":"8","name":"year3"+strength.replace(/[%./#-]/gi, '_')+"_"+packSize}).val(data.year3)))
									.append($("<td/>").append($("<input/>").addClass("form-control").css({"text-align":"right"}).attr({"type":"number","col":"8","name":"year4"+strength.replace(/[%./#-]/gi, '_')+"_"+packSize}).val(data.year4)))
									.append($("<td/>").append($("<input/>").addClass("form-control").css({"text-align":"right"}).attr({"type":"number","col":"8","name":"year5"+strength.replace(/[%./#-]/gi, '_')+"_"+packSize}).val(data.year5)))
									.append($("<td/>").append($("<input/>").addClass("form-control").css({"text-align":"right"}).attr({"type":"number","col":"8","name":"launch_price"+strength.replace(/[%./#-]/gi, '_')+"_"+packSize}).val(data.launch_price)))
									);
						}else{
							$("[name=marketShareTable] tbody").append(
									$("<tr/>").attr({"df":selectedProductmaster.toString(),"strength":strength,"packSize":packSize})
									.append($("<td/>").append(strength))	
									.append($("<td/>").append(packSize))
									.append($("<td/>").append($("<input/>").css({"text-align":"right"}).attr({"type":"number","col":"3","name":"currentApotexMarketShare_"+strength.replace(/[%./#-]/gi, '_')+"_"+packSize}).val(data.marketshare)))
									.append($("<td/>").append($("<input/>").css({"text-align":"right"}).attr({"type":"number","col":"4","name":"substitutionRate_"+strength.replace(/[%./#-]/gi, '_')+"_"+packSize}).val(data.subsitutitionrate)))
									.append($("<td/>").append($("<input/>").css({"text-align":"right"}).attr({"type":"number","col":"5","name":"substitutionGrowthRate_"+strength.replace(/[%./#-]/gi, '_')+"_"+packSize}).val(data.substitutiongrowthrate)))
									.append($("<td/>").append($("<input/>").css({"text-align":"right"}).attr({"type":"number","col":"6","name":"annualPriceErosion_"+strength.replace(/[%./#-]/gi, '_')+"_"+packSize}).val(data.priceerosionrate)))
									.append($("<td/>").append($("<input/>").css({"text-align":"right"}).attr({"type":"number","col":"6","name":"manualPrice"+strength.replace(/[%./#-]/gi, '_')+"_"+packSize}).val(data.price)))
							);							
						}
					}
				}
			}
		}
	};
	var createPriceTypeDropDown = function(){
		$("[name='pricesType']").empty();
		$("[name='pricesType']").append($("<option/>").attr({"value":"minimum"}).html("Min Mkt Price"))
		.append($("<option/>").attr({"value":"simpleAvg","selected":"true"}).html("Simple Avg. Price"));
		var priceColumn = $("[name='pricingsourcecombo']").val();
		if(This.productPrices && This.productPrices.length>0){
			var productPrices = This.productPrices.sort(function(a,b){return a[priceColumn]>b[priceColumn]?1:a[priceColumn]<b[priceColumn]?-1:0});
			var competitors = productPrices.map(function(o){return o.labeller});
			competitors = competitors.filter(function(item, pos) {
			    return competitors.indexOf(item) == pos;
			});
			if(competitors && competitors.length>0){
				for(var i=0;i<competitors.length;i++){
					$("[name='pricesType']").append($("<option/>").attr({"value":competitors[i]}).html("Adopt from - "+competitors[i]));
				}
			}
		}
		if(This.myForecast && This.myForecast.pricesType){
			$("[name='pricesType']").val(This.myForecast.pricesType);
		}
	};
	var getForecastMetaData = function(metaTypes){
		//["PRICE, PLANNING, FINANCIAL"]
		var metadataRequest = {planningType:This.forecastType,metatype:metaTypes,qplan:This.productKey.toLowerCase()};
		var selectedStrength = getFilteredData("",true),selectedFilters="";
		if(This.forecastType === "postsubmission"){
			if(selectedStrength && Object.keys(selectedStrength).length>0){
				selectedFilters+=" (";
				var j=0;
				for(strength in selectedStrength){
					if(selectedStrength.hasOwnProperty(strength)){
						if(j>0)
							selectedFilters+=" OR "
						selectedFilters+=" ( strength = '"+strength+"'";
						if(selectedStrength[strength].length>0){
							selectedFilters+=" AND pack_size IN ("+selectedStrength[strength].map(function(e) {return "'" + e.replace(/ /g,'')+ "'"}).toString()+")";
						}
						selectedFilters+=" )";
						j++;
					}
				}
				selectedFilters+=" )";
			}
			metadataRequest.strengthPack = selectedFilters;
			metadataRequest.forecastType = $("[name='forecastType']").val() || "moving_average";
		}else{
			var scenario;
			if(This.myForecast && This.myForecast.planingScenario){
				scenario = This.myForecast.planingScenario;
			}else{
				scenario = $("[name='forecastPlanScenario']").find("select").val();
			}
			metadataRequest.scenario = scenario;
		}
		var fromDate = new Date($("[name='planningCycleStart']").val()),dateHorizon = $("[name='planningHorizon']").val() || 36;
		var toDate = new Date(getManipulatedDate("month",dateHorizon-1,$("[name='planningCycleStart']").val()));
		var dates = getYearAndMonthsByDates(fromDate,toDate),monthFilters = "";
		if(dates && Object.keys(dates).length>0){
			var index = 0;
			monthFilters += " ("//month in ("+Object.keys(dates).map(function(e) {return "'" + e+ "'"}).toString()+")";
			for(key in dates){
				monthFilters+= "(year in ('"+key+"') and month in ("+dates[key].map(function(e) {return "'" + e+ "'"}).toString()+"))";
				index++;
				if(index<Object.keys(dates).length)
					monthFilters+= " OR ";
				//months = months.concat(dates[key]);
			}
			monthFilters+= ")";
			//request.filters += " AND year in ("+months.map(function(e) {return "'" + e+ "'"}).toString()+")";
		}
		metadataRequest.monthFilters = monthFilters;
		var request = {
			ForecastMetadataRequest:JSON.stringify(metadataRequest)	
		};
		enableLoading();
		callAjaxService("getForecastMetaData", function(response){
			disableLoading();
			var marketShares = [];
			if(response.prices && response.prices.length>0)
				This.productPrices = response.prices;
			if(This.forecastType === "presubmission"){
				if(response.prePlannings){
					marketShares = response.prePlannings;
					if(marketShares.length===0){
						marketShares[0] = {};
					}
					if(response.financials){
						var selectedStrength = getFilteredData("",true),noOfRows = 0;
						if(selectedStrength && Object.keys(selectedStrength).length>0){
							for(strength in selectedStrength){
								if(selectedStrength.hasOwnProperty(strength) && selectedStrength[strength].length>0){
									noOfRows+=selectedStrength[strength].length;
								}
							}
						}
						var yearsData = response.financials;
						if(yearsData && yearsData.length>0){
							yearsData= yearsData.sort(function(a,b){return a.year>b.year?1:a.year<b.year?-1:0});
							for(var i=0;i<yearsData.length;i++){
								marketShares[0]["year"+(i+1)] = parseInt(yearsData[i].value/noOfRows);
							}
						}
						constructMarketShareGrid(marketShares || []);	
					}
				}
				
			}else{
				if(response.postPlannings){
					constructMarketShareGrid(response.postPlannings || []);
				}
			}
			if(This.productPrices && This.productPrices.length>0)
				createPriceTypeDropDown();
			if(response.financials){
				callBackSuccessUnitData(response.financials);
			}
		},callBackFailure, request, "POST", null, true);
	};
	var processPlanningAssumptions = function(isReprocess){
		/*var filterLabeler = getFilteredData("manufacturer");
		$("[name='competitorsCount']").val(filterLabeler.length);*/
		/*var competitors = [];
		for(var i=0;i<filterLabeler.length;i++){
			competitors.push({
				label:filterLabeler[i],
				value:filterLabeler[i]
			});
		}*/ 
		constructDropDown("[name='competitorsName']","select distinct qplan_mfr,CASE  WHEN labeller_type ='A' THEN '(Approved)' ELSE '' END from  l2_aptx_qplan_source where labeller_type in ('M','A') and lower(qplan_molecule_name) = '"+This.productKey.toLowerCase()+"' and source_mfr like '%<Q>%'",true,null,null,"Competitor",This.source,This.database,{buttonWidth:236});
		if(This.myForecast && !isReprocess){
			if(This.myForecast.marketShares){
				var marketShares = Jsonparse(This.myForecast.marketShares);
				constructMarketShareGrid(marketShares,true);
			}
			processUnitAndFinancial();
		}
	};
	$("[name='pricesType'],[name='pricingsourcecombo']").on("change",function(){
		onChangeOfPriceCriteria();
		if($(this).attr("name") === "pricingsourcecombo"){
			createPriceTypeDropDown();
		}
	});
	var onChangeOfPriceCriteria = function(){
		var selectedStrength = getFilteredData("",true);
		var priceColumn = $("[name='pricingsourcecombo']").val();
		var pricetype = $("[name='pricesType']").val(),priceValue=0;
		var priceeaderText = $("[name='pricingsourcecombo']").find("option[value='"+priceColumn+"']").html()|| "IMS";
		$("[name='marketShareTable']").find("th:last").html("Price in $ ("+priceeaderText+")");
		if(This.productPrices && This.productPrices.length>0){
			if(selectedStrength && Object.keys(selectedStrength).length>0){
				for(strength in selectedStrength){
					if(selectedStrength.hasOwnProperty(strength) && selectedStrength[strength].length>0){
						for(var i=0;i<selectedStrength[strength].length;i++){
							var filteredData = This.productPrices.filter(function(obj){ return obj.strength === strength && obj.packsize === selectedStrength[strength][i]});
							if(pricetype === "simpleAvg"){
								priceValue= d3.mean(filteredData, function(o){return o[priceColumn]});
							}else if(pricetype === "minimum"){
								priceValue = Math.min.apply(Math,filteredData.map(function(o){return o[[priceColumn]];}));
							}else{
								filteredData = filteredData.filter(function(o){ return o.labeller === pricetype});
								if(filteredData.length>0){
									priceValue = filteredData[0][priceColumn];	
								}else{
									priceValue=0;
								}
							}
							$("[name='manualPrice"+strength.replace(/[%./#-]/gi, '_')+"_"+selectedStrength[strength][i]+"']").val(priceValue?d3.format(".2f")(priceValue):"");
						}
					}
				}
			}
		}
	};
	$("[href='#planning']").unbind("click").bind("click",function(){
		$("[name=competitorsName] label:first").hide();
		$("[name=competitorsName] i").hide();
		var allCompetitors = $("[name=competitorsName]").find("select").find("option").length-1;
		var appliedCompetitors = $("[name=competitorsName]").find("select").find("option:contains('(Approved)')").length;
		$("[name='competitorsCount']").val(allCompetitors-appliedCompetitors);
		$("[name='appliedCompetitorsCount']").html(appliedCompetitors);
	});
	var processUnitAndFinancial = function(isReprocess){
		if(This.myForecast && This.myForecast.financials && !isReprocess){
			var startMonth = $("[name='planningCycleStart']").val();
			var noOfMonths = $("[name='planningHorizon']").val();
			var financials = Jsonparse(This.myForecast.financials);
			$("#financials").find("table").empty();
			$("#financials").find("table").append($("<thead/>").append($("<tr/>")
					.append($("<th/>").html("Unit Sales")).append($("<th/>").html("Strength")).append($("<th/>").html("Pack Size"))));
			startMonth = getManipulatedDate("month",0,startMonth,"%b %Y");
			for(var i=0;i<noOfMonths;i++){
				$("#financials").find("table").find("thead").find("tr").append($("<th/>").html(startMonth));
				startMonth = getManipulatedDate("month",1,startMonth,"%b %Y");
			}
			$("#financials").find("table").find("thead").find("tr").append($("<th/>").html("Total"));
			if(financials){
				$("#financials").find("table").append($("<tbody/>"));
				for(var strength in financials){
					if(financials.hasOwnProperty(strength)){
						for(var packsize in financials[strength]){
							if(financials[strength].hasOwnProperty(packsize)){
								var row = $("<tr/>").css({"background-color":"rgba(241, 241, 143, 0.99)"})
								.attr({"type":"units","name":strength+"_"+packsize,"strength":strength,"packsize":packsize}),total = 0,totalRevenue = 0;
								row.append($("<th/>").html("Unit")).append($("<td/>").html(strength)).append($("<td/>").html(packsize));
								startMonth = getManipulatedDate("month",0,$("[name='planningCycleStart']").val(),"%Y/%m");
								
								var priceRow = $("<tr/>").addClass("warning");
								var revenueRow = $("<tr/>").addClass("danger").attr({"type":"revenue","strength":strength,"packsize":packsize});
								var cogsRow = $("<tr/>").css({"background-color":"antiquewhite"}).addClass("info1");
								var marginRow = $("<tr/>").css({"background-color":"honeydew"}).addClass("info1");
								var priceValue = $("[name='manualPrice"+strength.replace(/[%./#-]/gi, '_')+"_"+packsize+"']").val() || 0;
								if(This.forecastType === "presubmission"){
									priceValue = $("[name='launch_price"+strength.replace(/[%./#-]/gi, '_')+"_"+packsize+"']").val() || 0;
								}
								priceRow.append($("<th/>").attr({"colspan":"3"}).html("Price/Unit"));
								revenueRow.append($("<th/>").attr({"colspan":"3"}).html("Revenue"));
								cogsRow.append($("<th/>").attr({"colspan":"3"}).html('COGS'));
								marginRow.append($("<th/>").attr({"colspan":"3"}).html('Margin'));
								for(var i=0;i<noOfMonths;i++){
									var value = financials[strength][packsize][startMonth] || {};
									//if(financials[strength][packsize][startMonth]){
									var input = $("<input/>").attr({"month":startMonth}).addClass("form-control").css({"width":"100px","text-align":"right","display":"none"});
										row.append($("<td>").unbind("click").bind("click",function(){
											$(this).parents("table").find("span").show();
											$(this).parents("table").find("input").hide();
											$(this).find("input").focus();
											$(this).find("span").hide();
											$(this).find("input").show();
										}).append($("<span/>").html(getFormatedData(value.unit || 0))).append(input));
										
										input.val(value.unit?d3.format(",f")(value.unit):0).on('blur',function(){
											var inputVal = $(this).val();
											var currentMonth = $(this).attr("month");
											var currentStrength = $(this).parents("tr").attr("strength");
											var currentPack = $(this).parents("tr").attr("packsize");
											var revenueRow = $(this).parents("table").find("tr[type='revenue'][strength='"+currentStrength+"'][packsize='"+currentPack+"']");
											var price = $("[name='manualPrice"+currentStrength.replace(/[%./#-]/gi, '_')+"_"+currentPack+"']").val() || 0;
											if(inputVal)
												inputVal = inputVal.replaceAll(",","");
											else
												inputVal = 0;
											revenueRow.find("td[month='"+currentMonth+"']").attr({"quantity":inputVal,"revenue":(inputVal*price)})
											.html(price?("$"+getFormatedData(price*inputVal)):"");
											$(this).parent().find("span").show().html(getFormatedData(inputVal));
											$(this).hide();
											calculateUnitAndRevenueTotal();
										});
										priceRow.append($("<td/>").css({"text-align":"right"}).html(priceValue));
										cogsRow.append($("<td/>").css({"text-align":"right"}).html("0"));
										marginRow.append($("<td/>").css({"text-align":"right"}).html("100%"));
										
										revenueRow.append($("<td/>").css({"text-align":"right"})
												.attr({"month":startMonth,"quantity":value.unit || 0,"revenue":value.revenue || 0})
												.html(priceValue?("$"+getFormatedData(value.revenue || 0)):""));
										
										/*row.append($("<td/>").append($("<input/>").css({"text-align":"right"}).val(d3.format(",f")(financials[strength][packsize][startMonth]))
												.attr({"month":startMonth})));*/
										total+=parseInt(value.unit || 0);
										totalRevenue+=parseInt(value.revenue || 0);
									//}
									startMonth = getManipulatedDate("month",1,startMonth,"%Y/%m");
								}
								row.append($("<td/>").html(getFormatedData(total)));
								priceRow.append($("<td>").html(getFormatedData(totalRevenue)));
								cogsRow.append($("<td>"));
								marginRow.append($("<td>"));
								revenueRow.append($("<td>"));
								$("#financials").find("table").find("tbody").append(row);
								$("#financials").find("table").find("tbody").append(priceRow);
								$("#financials").find("table").find("tbody").append(cogsRow);
								$("#financials").find("table").find("tbody").append(revenueRow);
								$("#financials").find("table").find("tbody").append(marginRow);
								$("#financials").find("table").find("tbody").append($("<tr/>").append($("<td/>").html("&nbsp;").attr({"colspan":noOfMonths+4})));
							}
						}
					}
				}
				var totalUnitRow = $("<tr/>").attr("name","unittotal").addClass('success');
				totalUnitRow.append($("<th/>").attr({"colspan":"3"}).html("Total Unit Sales"));
				$("#financials").find("tbody").append(totalUnitRow);
				var totalrevenueRow = $("<tr/>").attr("name","revenuetotal").addClass('danger'),revenueGrandTotal = 0,unitSalesGrandTotal = 0;
				totalrevenueRow.append($("<th/>").attr({"colspan":"3"}).html("Total Revenue"));
				$("#financials").find("tbody").append(totalrevenueRow);
				startMonth = $("[name='planningCycleStart']").val();
				$("#financialPlanningHorizon").find("span").html(getManipulatedDate("month",0,startMonth,"%b %Y")+" To "+getManipulatedDate("month",noOfMonths-1,startMonth,"%b %Y"));
				startMonth = getManipulatedDate("month",0,startMonth,"%Y/%m");
				for(var i=0;i<noOfMonths;i++){
					totalUnitRow.append($("<th/>").attr("month",startMonth));
					totalrevenueRow.append($("<th/>").attr("month",startMonth));
					startMonth = getManipulatedDate("month",1,startMonth,"%Y/%m");
				}
				calculateUnitAndRevenueTotal();
			}
		}
	};
	var callBackSuccessUnitData = function(finanncialData){
		disableLoading();
		/*if (response && response.isException) {
			showNotification("error", response.customMessage);
			return;
		}*/
		var selectedStrengthPacks = getFilteredData("",true);
		//if (response && response.data && response.data.length>0) {
			/*var savedUnitData = {};
			if(This.myForecast && This.myForecast.units_calculation_type){
				savedUnitData = Jsonparse(This.myForecast.units_calculation_type);
			}*/
			var strengthObj = {},sales_2018 = 0,startMonth = $("[name='planningCycleStart']").val();
			var marketShare = $("[name='targetMarketShare']").val();
			var noOfMonths = $("[name='planningHorizon']").val() || 36;
			/*var generalInfo = $("#eventDetailsForm").serializeArray(),generalInfoObj ={};
			for(var i=0;i<generalInfo.length;i++){
				if(!isNaN(generalInfo[i].value)){
					generalInfoObj[generalInfo[i].name] = parseInt(generalInfo[i].value); 
				}else{
					generalInfoObj[generalInfo[i].name] = generalInfo[i].value;
				}
			}*/
			$("#financials").find("table").empty();
			$("#financials").find("table").append($("<thead/>").append($("<tr/>")
					.append($("<th/>")).append($("<th/>").html("Strength")).append($("<th/>").html("Pack Size"))));
			startMonth = getManipulatedDate("month",0,startMonth,"%b %Y");
			for(var i=0;i<noOfMonths;i++){
				$("#financials").find("table").find("thead").find("tr").append($("<th/>").html(startMonth));
				startMonth = getManipulatedDate("month",1,startMonth,"%b %Y");
			}
			$("#financials").find("table").find("thead").find("tr").append($("<th/>").html("Total"));
			$("#financials").find("table").append($("<tbody/>"));
			if(This.forecastType === "postsubmission"){
				for(var i=0;i<finanncialData.length;i++){
					var monthKey = finanncialData[i].month;
					if(finanncialData[i].month<10)
						monthKey ="0"+finanncialData[i].month;
					monthKey =  finanncialData[i].year+"/"+monthKey;
					if(!strengthObj[finanncialData[i].strength])
						strengthObj[finanncialData[i].strength] = {};
					if(!strengthObj[finanncialData[i].strength][finanncialData[i].packsize])
						strengthObj[finanncialData[i].strength][finanncialData[i].packsize] = [];
					if(!strengthObj[finanncialData[i].strength][finanncialData[i].packsize][monthKey])
						strengthObj[finanncialData[i].strength][finanncialData[i].packsize][monthKey] = 0;
					strengthObj[finanncialData[i].strength][finanncialData[i].packsize][monthKey] = finanncialData[i].value;
				}
			}
			for(var strength in selectedStrengthPacks){
				if(selectedStrengthPacks.hasOwnProperty(strength)){
					//for(packsize in selectedStrengthPacks[strength]){
					for(var j=0;j<selectedStrengthPacks[strength].length>0;j++){
						var packsize = selectedStrengthPacks[strength][j];
							var row = $("<tr/>").css({"background-color":"rgba(241, 241, 143, 0.99)"}).attr({"type":"units","name":strength+"_"+packsize,"strength":strength,"packsize":packsize});
							
							row.append($("<th/>").html("Unit Sales")).append($("<th/>")
									.html(strength)).append($("<th/>").html(packsize));
							var priceRow = $("<tr/>").addClass("warning");
							var revenueRow = $("<tr/>").addClass("danger").attr({"type":"revenue","strength":strength,"packsize":packsize});
							var cogsRow = $("<tr/>").css({"background-color":"antiquewhite"}).addClass("info1");
							var marginRow = $("<tr/>").css({"background-color":"honeydew"}).addClass("info1");
							var priceValue = $("[name='manualPrice"+strength.replace(/[%./#-]/gi, '_')+"_"+packsize+"']").val() || 0;
							if(This.forecastType === "presubmission"){
								priceValue = $("[name='launch_price"+strength.replace(/[%./#-]/gi, '_')+"_"+packsize+"']").val() || 0;
							}
							if(priceValue){
								priceValue = parseFloat(parseFloat(priceValue).toFixed(2));
							}
							priceRow.append($("<th/>").attr({"colspan":"3"}).html("Price/Unit"));
							revenueRow.append($("<th/>").attr({"colspan":"3"}).html("Revenue"));
							cogsRow.append($("<th/>").attr({"colspan":"3"}).html('COGS'));
							marginRow.append($("<th/>").attr({"colspan":"3"}).html('Margin'));
							$("#financials").find("table").find('tbody').append(row);
							$("#financials").find("table").find('tbody').append(priceRow);
							$("#financials").find("table").find('tbody').append(cogsRow);
							$("#financials").find("table").find('tbody').append(revenueRow);
							$("#financials").find("table").find('tbody').append(marginRow);
							$("#financials").find("table").find("tbody").append($("<tr/>").append($("<td/>").html("&nbsp;").attr({"colspan":noOfMonths+4})));
							var totalValue = 0,totalRevenue = 0,yearIndex = 0;
							startMonth = $("[name='planningCycleStart']").val();
							startMonth = getManipulatedDate("month",0,startMonth,"%Y/%m");
							for(var i=0;i<noOfMonths;i++){
								var input = $("<input/>").attr({"month":startMonth}).addClass("form-control").css({"width":"100px","text-align":"right","display":"none"});
								var value = 0;
								if(This.forecastType === "postsubmission"){
									if(strengthObj[strength] && strengthObj[strength][packsize][startMonth])
										value = strengthObj[strength][packsize][startMonth];
									else if(strengthObj["NA"] && strengthObj["NA"]["NA"] && strengthObj["NA"]["NA"][startMonth])
										value = strengthObj["NA"]["NA"][startMonth];
								}else{
									if(i%12 === 0){
										yearIndex++;
									}
									var yearlyValue = $("[name='year"+yearIndex+""+strength+"_"+packsize+"']").val();
									value = yearlyValue/12;
								}
								var calculatedObj = getCalculatedUnitValue(value,priceValue,strength,packsize,i);
								value = calculatedObj.value;
								priceValue = parseFloat(parseFloat(calculatedObj.priceValue).toFixed(2));
								
								row.append($("<td>").unbind("click").bind("click",function(){
									$(this).find("span").hide();
									$(this).find("input").show();
								}).append($("<span/>").html(getFormatedData(value))).append(input));
								
								input.val(value?d3.format(",f")(value):0).on('blur',function(){
									var inputVal = $(this).val();
									var currentMonth = $(this).attr("month");
									var currentStrength = $(this).parents("tr").attr("strength");
									var currentPack = $(this).parents("tr").attr("packsize");
									var revenueRow = $(this).parents("table").find("tr[type='revenue'][strength='"+currentStrength+"'][packsize='"+currentPack+"']");
									var price = $("[name='manualPrice"+currentStrength.replace(/[%./#-]/gi, '_')+"_"+currentPack+"']").val() || 0;
									if(inputVal)
										inputVal = inputVal.replaceAll(",","");
									else
										inputVal = 0;
									revenueRow.find("td[month='"+currentMonth+"']").attr({"quantity":inputVal,"revenue":(inputVal*price)})
									.html(price?("$"+getFormatedData(price*inputVal)):"");
									$(this).parent().find("span").show().html(getFormatedData(inputVal));
									$(this).hide();
									calculateUnitAndRevenueTotal();
								});
								totalValue+= parseFloat(value);
								totalRevenue+=parseFloat(priceValue*value);
								priceRow.append($("<td/>").css({"text-align":"right"}).html(priceValue));
								cogsRow.append($("<td/>").css({"text-align":"right"}).html("0"));
								marginRow.append($("<td/>").css({"text-align":"right"}).html("100%"));
								
								revenueRow.append($("<td/>").css({"text-align":"right"})
										.attr({"month":startMonth,"quantity":value,"revenue":priceValue*value})
										.html(priceValue?("$"+getFormatedData(priceValue*value)):""));
								startMonth = getManipulatedDate("month",1,startMonth,"%Y/%m");
							}
							row.append($("<td>").append(getFormatedData(totalValue)));
							priceRow.append($("<td>"));
							cogsRow.append($("<td>"));
							marginRow.append($("<td>"));
							revenueRow.append($("<td>").append(getFormatedData(totalRevenue)));
						//}
					}
				}
			}

			var totalUnitRow = $("<tr/>").attr("name","unittotal").addClass('success');
			totalUnitRow.append($("<th/>").attr({"colspan":"3"}).html("Total Unit Sales"));
			$("#financials").find("tbody").append(totalUnitRow);
			var totalrevenueRow = $("<tr/>").attr("name","revenuetotal").addClass('danger'),revenueGrandTotal = 0,unitSalesGrandTotal = 0;
			totalrevenueRow.append($("<th/>").attr({"colspan":"3"}).html("Total Revenue"));
			$("#financials").find("tbody").append(totalrevenueRow);
			startMonth = $("[name='planningCycleStart']").val();
			$("#financialPlanningHorizon").find("span").html(getManipulatedDate("month",0,startMonth,"%b %Y")+" To "+getManipulatedDate("month",noOfMonths-1,startMonth,"%b %Y"));
			startMonth = getManipulatedDate("month",0,startMonth,"%Y/%m");
			for(var i=0;i<noOfMonths;i++){
				totalUnitRow.append($("<th/>").attr("month",startMonth));
				totalrevenueRow.append($("<th/>").attr("month",startMonth));
				startMonth = getManipulatedDate("month",1,startMonth,"%Y/%m");
			}
			calculateUnitAndRevenueTotal();
			//processFinancial();
		//}
	};
	var getCalculatedUnitValue = function(value,priceValue,strength,packsize,index){
		var generalInfo = $("#eventDetailsForm").serializeArray(),generalInfoObj ={};
		for(var i=0;i<generalInfo.length;i++){
			if(!isNaN(generalInfo[i].value)){
				generalInfoObj[generalInfo[i].name] = parseInt(generalInfo[i].value); 
			}else{
				generalInfoObj[generalInfo[i].name] = generalInfo[i].value;
			}
		}
		generalInfoObj.marketshareprice = $("[name=marketshareprice]").val() || 0;
		if(generalInfoObj.marketshareprice){
			generalInfoObj.marketshareprice = parseFloat(generalInfoObj.marketshareprice);
		}
		if(This.forecastType === "postsubmission"){
			generalInfoObj.substitutionRate = $("[name=substitutionRate_"+strength.replace(/[%./#-]/gi, '_')+ "_" + packsize +"]").val() || 0;
			if(generalInfoObj.substitutionRate){
				generalInfoObj.substitutionRate = parseFloat(generalInfoObj.substitutionRate);
			}
			generalInfoObj.targetMarketShare = $("[name=currentApotexMarketShare_"+strength.replace(/[%./#-]/gi, '_')+ "_" + packsize +"]").val() || 0;
			if(generalInfoObj.targetMarketShare){
				generalInfoObj.targetMarketShare = parseFloat(generalInfoObj.targetMarketShare);
			}
			generalInfoObj.substitutionGrowthRate = $("[name=substitutionGrowthRate_"+strength.replace(/[%./#-]/gi, '_')+ "_" + packsize +"]").val() || 0;
			if(generalInfoObj.substitutionGrowthRate){
				generalInfoObj.substitutionGrowthRate = parseFloat(generalInfoObj.substitutionGrowthRate);
			}
			generalInfoObj.annualPriceErosion = $("[name=annualPriceErosion_"+strength.replace(/[%./#-]/gi, '_')+ "_" + packsize +"]").val() || 0;
			if(generalInfoObj.annualPriceErosion){
				generalInfoObj.annualPriceErosion = parseFloat(generalInfoObj.annualPriceErosion);
			}
			if(index>0 && index%12 === 0 && generalInfoObj.substitutionRate<=98){
				generalInfoObj.substitutionRate = generalInfoObj.substitutionRate+(generalInfoObj.substitutionRate*generalInfoObj.substitutionGrowthRate/100)
				generalInfoObj.substitutionRate = generalInfoObj.substitutionRate>98?98:generalInfoObj.substitutionRate;
				priceValue = priceValue+(priceValue*generalInfoObj.annualPriceErosion/100);
			}
			if(generalInfoObj.substitutionRate !== "" && generalInfoObj.targetMarketShare !== ""){
				value = (value*generalInfoObj.substitutionRate)/100;
				//index>0 && index%generalInfoObj.revisionPeriodValue === 0 && 
				if(generalInfoObj.marketshareprice){
					generalInfoObj.targetMarketShare += generalInfoObj.marketshareprice*(generalInfoObj.marketsharetrend*parseInt(index/generalInfoObj.revisionPeriodValue));
				}
				value = (value*generalInfoObj.targetMarketShare)/100;
			}
		}else{
			generalInfoObj.targetMarketShare = $("[name=apotex_gm_share"+strength.replace(/[%./#-]/gi, '_')+ "_" + packsize +"]").val() || 0;
			if(generalInfoObj.targetMarketShare){
				generalInfoObj.targetMarketShare = parseFloat(generalInfoObj.targetMarketShare);
				var planScenario = (This.myForecast && This.myForecast.planingScenario)?This.myForecast.planingScenario:
					This.forecastPlanScenario?This.forecastPlanScenario:"";
				if(index>5 && (planScenario === "Forecast-Excl (FTF)") || planScenario === "Forecast-Shr Excl (FTF)"){
					generalInfoObj.targetMarketShare = 35;
				}
			}
			generalInfoObj.intApotexPrice = $("[name=initial_apotex_price"+strength.replace(/[%./#-]/gi, '_')+ "_" + packsize +"]").val() || 0;
			if(generalInfoObj.intApotexPrice){
				generalInfoObj.intApotexPrice = parseFloat(generalInfoObj.intApotexPrice);
			}
			generalInfoObj.gm_conversion = $("[name=gm_conversion"+strength.replace(/[%./#-]/gi, '_')+ "_" + packsize +"]").val() || 0;
			if(generalInfoObj.gm_conversion){
				generalInfoObj.gm_conversion = parseFloat(generalInfoObj.gm_conversion);
			}
			if(generalInfoObj.marketshareprice){
				generalInfoObj.targetMarketShare += (generalInfoObj.marketshareprice*parseInt(index/generalInfoObj.revisionPeriodValue))*generalInfoObj.marketsharetrend;//((generalInfoObj.targetMarketShare*generalInfoObj.marketshareprice)/100)*generalInfoObj.marketsharetrend;
			}
			if(generalInfoObj.gm_conversion){
				value = value*generalInfoObj.gm_conversion/100;
			}
			/*if(generalInfoObj.mkt_volume){
				generalInfoObj.targetMarketShare = ((generalInfoObj.mkt_volume*generalInfoObj.gm_conversion)/100)*generalInfoObj.targetMarketShare/100;
			}*/
			value = (value*generalInfoObj.targetMarketShare)/100;
		}
		return {value:value,priceValue:priceValue};
	};
	var calculateUnitAndRevenueTotal = function(){
		var revenueSubTotal ={},unitSubTotal = {},unitGrandTotal = 0,revenueGrandTotal = 0;
		$("[name='financialTable']").find("tr[type='revenue']").each(function(){
			$(this).find("td").each(function(){
				var month = $(this).attr("month"),quantity = $(this).attr("quantity"),revenue = $(this).attr("revenue");
				if(month){
					if(!revenueSubTotal[month])
						revenueSubTotal[month] = 0;
					if(!unitSubTotal[month])
						unitSubTotal[month] = 0;
					revenueSubTotal[month]+=parseInt(revenue);
					unitSubTotal[month]+=parseInt(quantity);
					unitGrandTotal+=parseInt(quantity);
					revenueGrandTotal+=parseInt(revenue);
				}
			});
		});
		$("[name='financialTable']").find("tr[name='unittotal']").find("th").each(function(){
			var month = $(this).attr("month");
			if(month && unitSubTotal[month]){
				$(this).html(getFormatedData(unitSubTotal[month]));
			}
		});
		$("[name='financialTable']").find("tr[name='revenuetotal']").find("th").each(function(){
			var month = $(this).attr("month");
			if(month && revenueSubTotal[month]){
				$(this).html(getFormatedData(revenueSubTotal[month]));
			}
		});
		$("#financialUnitSales").find("span").html(getFormatedData(unitGrandTotal));
		$("#financialRevenue").find("span").html("$"+getFormatedData(revenueGrandTotal));
		$("#financials").data("totalrevenue",revenueGrandTotal);
	};
	var getFormatedData = function(data){
		var prefix = d3.formatPrefix(data);
		return (prefix.scale(data).toFixed(2)+prefix.symbol);
	};
	/*$("[href='#tab-r5']").unbind("click").bind("click",function(){
		processFinancial();
	});
	$("[href='#tab-r4']").unbind("click").bind("click",function(){
		
	});*/
	$("[name='submissionDate']").datepicker({
		defaultDate : "+1w",
		changeMonth : true
	});
	$("[name='planningCycleStart']").datepicker({
		changeYear: true,
		changeMonth : true,
		showButtonPanel: true,
        dateFormat: 'MM yy'
        /*onClose: function(dateText, inst) { 
            $(this).datepicker('setDate', new Date(inst.selectedYear, inst.selectedMonth, 1));
        }*/
	}).datepicker('setDate',new Date(getManipulatedDate("month",1)));
	$("[name='forecastPlantype']").on("change",function(){
		This.forecastType = $(this).val();
		if(This.forecastType === "presubmission"){
			$("[name='forecastPlanScenario']").show();
			constructDropDown("[name='forecastPlanScenario']","select distinct scenario,'' from mst_assumption_scenario where lower(scenario) like '%<Q>%'",false, null, "", "Select Scenario",This.source, This.database);
		}else{
			$("[name='forecastPlanScenario']").hide().empty();
		}
	});
	$("[name='btnAddStrengthPacks']").bind("click",function(){
		addStrengthAndPacks();
	});
	var addStrengthAndPacks = function(strength,packs){
		var removeAnchor = $("<a/>").attr({"title":"Remove Row","mode":"edit"}).addClass("text-danger cursor-pointer pull-right")
		.append($("<i/>").addClass("fa fa-2x fa-remove"));
		var row  = $("<tr/>").append($("<td/>").append($("<input/>").addClass("form-control").attr({"name":"strength","placeholder":"Ex: 10MG"}).val(strength || "")))
		.append($("<td/>").append($("<input/>").addClass("form-control").attr({"name":"packsize","placeholder":"Ex: 5,10"}).val(packs?packs.toString():"")))
		.append($("<td/>").append(removeAnchor));
		$("[name='preSubmission']").find("table").find("tbody").append(row);
		removeAnchor.unbind("click").bind("click",function(){
			$(this).parents("tr").remove();
		});
	};
	$("#createForecast").unbind("click").bind("click",function(){
		$("#search-body").show();
		$("#serach-param").hide();
		$("[href='#hierarchy']").trigger("click");
		$("#saveForecast").show();
		$("[name='forecastname']").val(This.productKey);
		$("#editForecast").hide();
		$("#searchTabContent").find("input,select,textarea").removeAttr("disabled");
		$("#searchTabContent").find("svg").css({"pointer-events":"all"});
		$("#strenthContent").find("a").css({"pointer-events":"all"});
		$("#searchTabContent").find(".slider-horizontal").css({"pointer-events":"all"});
		$("#searchTabContent").find("a[mode='edit']").show();
		This.forecastType = $("[name='forecastPlantype']:checked").val();
		var schenarioText = "";
		if(This.forecastType === "presubmission"){
			$("[name='preSubmission']").show();
			$("[name='postSubmission']").hide();
			This.forecastPlanScenario =  $("[name='forecastPlanScenario']").find("select").val();
			schenarioText = "Pre Submission ("+This.forecastPlanScenario+")";
			//addStrengthAndPacks();
		}else{
			$("[name='preSubmission']").hide();
			$("[name='postSubmission']").show();
			schenarioText = "Post Submission";
		}
		$("#wid-id-0").find("header").find("h2").html("Forecast Details ("+This.productKey+") : "+schenarioText);
		This.myForecast = null;
		$("[name='noresultsfound']").hide();
		$("[name='planStatus']").find("label").removeClass("active");
		$("[name='planStatus']").find("input[name='status'][value='WIP']").prop("checked","checked").parent().addClass("active");
		$("#strenthContent").find("a").data("isSelected",false).css("opacity","0.2");
	});
	$("#planning").find("[name='marketsharetrend']").bind("change",function(){
		var selectedValue = $(this).val();
		if(selectedValue && selectedValue !== "0"){
			$("#planning").find("[name='marketshareprice']").removeAttr("disabled");
		}else{
			$("#planning").find("[name='marketshareprice']").attr({"disabled":"disabled"});
		}
	});
	var checkSearchKey = function(){
		//var key = $("#searchconfigterm").val();
		/*dc.deregisterAllCharts();
		dc.chartRegistry.clear();*/
		$("#strenthContent").empty();
		//$("[name='packContent']").empty();
		$("[name='createForecastDiv']").show();
		var selectedCombination =  $("[name='searchdropdown']").find("ul").find(".active ").text();
		$("[name='searchdropdown']").find("button").html(selectedCombination).append($("<b/>").addClass("caret"));
		This.productKey = $("[name='searchdropdown']").find("select").val();
		//callAjaxService("getAllForecast", function(){},callBackFailure, {}, "POST", null, true);
		var requests = [{qplanName:This.productKey.toLowerCase()},{qplanName:This.productKey}];
		enableLoading();
		callAjaxServices(["getProductMapData","getAllForecast"], callBackSuccessGetData,callBackFailure, requests, "POST", null, true);
	};
	var callBackSuccessGetData =  function(response){
		disableLoading();
		if (response && response.isException) {
			showNotification("error", response.customMessage);
			return;
		}
		if (response && response.length>0) {
			if(response[1] && response[1].length>0){
				$("#searchDivId").find("[name='noresultsfound']").hide();
				$("#searchDivId").find("[name='resultsfound']").show().find("p").html(response[1].length+" Forecast(s) found for the given search term ")
				.append($("<a/>").attr({"href":"javascript:void(0)"}).html("Show")).unbind("click").bind("click",function(){
					//constructForeCastGrid(response[1].data);
					$("[name='forecastgrid']").parent().prev().hide();
					$("[name='forecastgrid']").show();
				});
				constructForeCastGrid(response[1]);
			}else{
				$("#hierarchy").find("[name='forecastname']").val(This.productKey);
				$("#searchDivId").find("[name='noresultsfound']").show();
				$("#searchDivId").find("[name='resultsfound']").hide();
			}
			This.productMasters = response[0];
			This.strenthPack = {};
			if(This.productMasters && This.productMasters.length>0){
				for(var i=0;i<This.productMasters.length;i++){
					if(This.productMasters[i].strength){
						if(!This.strenthPack[This.productMasters[i].strength])
							This.strenthPack[This.productMasters[i].strength] = [];
						if(This.strenthPack[This.productMasters[i].strength].indexOf(This.productMasters[i].packsize) === -1)
							This.strenthPack[This.productMasters[i].strength].push(This.productMasters[i].packsize);
					}
				}
			}
			var ordered = []; Object.keys(This.strenthPack).sort().forEach(function(key) {
				  ordered[key] = This.strenthPack[key];
				});
			This.strenthPack = ordered;
			//var strenthArray = [];
			//constructStrengthChart({html:"#strenthContent",field:"strength",measure:"salesValue"});
			//$("[name='packContent']").empty();
			$("[name='preSubmission']").find("tbody").empty();
			for(var strengthKey in This.strenthPack) {
				if (This.strenthPack.hasOwnProperty(strengthKey)) {
					/*$("[name='packContent']").append( $("<div/>").addClass('panel panel-default').attr({"strength":strengthKey})
							.append( $("<div/>").attr({"name":"strengthKey"}).addClass('panel-heading').append($("<h4/>").addClass("panel-title")
									.append($("<a/>").addClass("collapsed").attr({"aria-expanded":"false","data-toggle":"collapse","href":"#strngth_"+strengthKey.replace(/[%./#-]/gi, '_')})
											.append($("<i/>").addClass("fa fa-fw fa-plus-circle txt-color-green"))
											.append($("<i/>").addClass("fa fa-fw fa-minus-circle txt-color-red"))
											.append(strengthKey+" ("+(This.strenthPack[strengthKey].length+")")))))
							.append($("<div/>").attr({"id":"strngth_"+strengthKey.replace(/[%./#-]/gi, '_')}).addClass("panel-collapse collapse")
							.append($("<div/>").addClass("panel-body").attr({"id":"packSizeChart_"+strengthKey.replace(/[%./#-]/gi, '_')}))));
					constructStrengthChart({html:"#packSizeChart_"+strengthKey.replace(/[%./#-]/gi, '_'),field:"packsize",measure:"salesValue",datafilter:strengthKey});*/
					if(This.strenthPack[strengthKey] && This.strenthPack[strengthKey].length>0){
						addStrengthAndPacks(strengthKey,This.strenthPack[strengthKey]);
					}
					constructStrengthChart(strengthKey);
				}
			}
			/*$("#strenthContent]").empty();
			$("[name='packContent']").empty();
			for(var key in This.strenthPack) {
				if (This.strenthPack.hasOwnProperty(key)) {
					var checkbox = $("<input/>").attr({"type":"checkbox","name":"strenth","key":key.trim()});
					$("#strenthContent]").append($("<label/>").addClass("btn btn-default").append(checkbox).append(key));
					checkbox.on("change",function(){
						onChangeOfStrength(this);
						//$("[name='strenthContent']").find("input").each(function(){
							
					});
				}
			}*/
		}
	};
	var constructStrengthChart =  function(strength){
		var productMasters = This.productMasters.filter(function(o){return o.strength === strength}),totalSalesValue = 0,
		totalWidth = $("#strenthContent").width();
		productMasters.sort(function(a,b){return a.salesValue<b.salesValue?1:a.salesValue>b.salesValue?-1:0 });
		productMasters.forEach(function(o){totalSalesValue+= o.salesValue});
		//productMasters.forEach(function(o){totalSalesValue+= o.salesValue>0?Math.log2(o.salesValue):0});
		var strengthbtn = $("<button/>").html(strength+  ":").addClass("btn btn-lg").css({"background-color":"#4B0082","color":"white"})
		var strengthDiv = $("<table/>").attr({"name":strength}).css({"width":"95%","float":"left"}).addClass("table table-bordered").append($("<tr/>"));
		strengthDiv.find("tr").append($("<td/>").append(strengthbtn));
		//strengthDiv.append($("<strong/>").addClass("pull-left").html(strength+" : ")).append($("<div/>").addClass("col-sm-11"));
		if(productMasters && productMasters.length>0){
			for(var i=0;i<productMasters.length;i++){
				var width = 100/productMasters.length;//productMasters[i].salesValue*100/totalSalesValue;
				var title = "<h1><em>Sales value:</em> "+(productMasters[i].salesValue?d3.format(",.3s")(productMasters[i].salesValue)+" ("+width.toFixed("2")+"%)":0)+"</h1>";
				strengthDiv.find("tr").append($("<td/>").css({"width":width+"%"}).append($("<a/>")
						.attr({"data-original-title":title,"name":productMasters[i].packsize,"data-placement":"top","data-html":true})
						.css({"opacity":"0.2","width":"100%","background-color":"#8A2BE2","color":"white"})
						.addClass("btn btn-lg").html(productMasters[i].packsize)));
			}
		}
		$("#strenthContent").append(strengthDiv);
		/*strengthbtn.unbind("click").bind("click",function(){
			var isSelected = $(this).data("isSelected");
			if(isSelected){
				$(this).css("opacity","0.2");
				$(this).data("isSelected",false);
				$(this).parents("table").find("a").data("isSelected",false).css("opacity","0.2");
			}else{
				$(this).css("opacity","1");
				$(this).data("isSelected",true);
				$(this).parents("table").find("a").data("isSelected",true).css("opacity","1");
			}	
		});*/
		$("#strenthContent").find("a").unbind("click").bind("click",function(){
			var isSelected = $(this).data("isSelected");
			if(isSelected){
				$(this).css("opacity","0.2");
				$(this).data("isSelected",false);
			}else{
				$(this).css("opacity","1");
				$(this).data("isSelected",true);
			}	
		}).tooltip();
	};
	/*var constructStrengthChart = function(config,data){
		try
		{
			data = data || This.productMasters;
			var width = 1000;
			var tip = d3.tip().offset([-8, 0]).attr('class', 'd3-tip').html(function(e){
				var percentage,toralValue;
				if(config.html === "#strenthContent"){
					toralValue = d3.sum(This.productMasters,function(o){ return o.salesValue});
				}else{
					var currentProduct = This.productMasters.filter(function(o){ return o.strength === config.datafilter});
					toralValue = d3.sum(currentProduct,function(o){ return o.salesValue});
				}
				percentage = e.data.value.salesValue/toralValue*100;
				percentage = Math.round(percentage);
				 var toolTip= $("<div/>");
				   toolTip.append($('<table/>').attr({"class":"d3-tip-table"}).append($('<tr/>')
						   .append($('<td/>').html( e.data.key +':').attr({"class":"d3-tip-key"}))
						   .append($('<td/>').html((d3.format(",.f")(e.data.value.salesValue))+(percentage?" ("+percentage+"%)":"")).attr({"class":"font-bold"}))));
				 return toolTip[0].outerHTML;  
			});
			if(config.datafilter){
				data = data.filter(function(d){ return d.strength === config.datafilter});
			}
			var dimFun = new Function('d', "return d['"+config.field+"'] || 'Others' ;");
			var ndx  = crossfilter(data),
		    dim  = ndx.dimension(dimFun),stacks = [config.measure];
		    grp = dim.group().reduce(
					function(p,v){
						p.count += +v._count;
						stacks.forEach(function(f) {
							if(v[f] && !isNaN(v[f]))
								p[f] += +v[f];
						});
						return p;
						}
					,function(p,v){
						stacks.forEach(function(f) {
							if(v[f] && !isNaN(v[f]))
								p[f] += +v[f];
						});
						return p;
					},
					function(){
						var ret = {};
						ret["count"] = 0;
						stacks.forEach(function(f) {
							ret[f] = 0;
						});
						return ret;
					});
		    if(config.html !== "#strenthContent"){
		    	width = grp.size()*100;
		    }
			var bar = dc.barChart(config.html);
			bar.margins({
				top : 20,
				left : 20,
				right : 20,
				bottom : 20
			}).width(width).height(200).x(d3.scaleOrdinal())
					.xUnits(dc.units.ordinal).brushOn(false)
					//.xAxisLabel("Strength")
					.elasticX(true)
					.yAxisLabel("")
					.dimension(dim)
					.gap(5)
					// .barPadding(0.1)
					//.renderLabel(true)
					.title(function (){return ""}).elasticY(true)
					.group(grp).valueAccessor(function(d){
						return d.value[config.measure]?parseInt(d.value[config.measure]):0;
					}).ordering(function(d){
						return d.value[config.measure]*-1;
					});
			bar.yAxis().ticks(0);
			bar.render();
			bar.on('filtered', function(chart,key) {
				var filters = chart.filters();
				if(chart.anchorName() === "strenthContent"){
					$("[name='packContent']").find(".panel-default").each(function(){
						if(filters.indexOf($(this).attr("strength"))>-1){
							$(this).find("h4").find("a").removeClass("collapsed");
							$(this).find(".panel-collapse").addClass("in");
						}else{
							$(this).find("h4").find("a").addClass("collapsed");
							$(this).find(".panel-collapse").removeClass("in");
						}
					});
				}
			});
			d3.selectAll(config.html).selectAll(".bar").call(tip);
			d3.selectAll(config.html).selectAll(".bar").on('mouseover', tip.show);
			d3.selectAll(config.html).on('mouseenter',function(){$(".d3-tip").css("opacity",0);});
			$(config.html).attr({"align":"center"}).prepend($("<div/>").addClass("clearfix")).prepend($("<a/>")).prepend($("<div/>").addClass("axisLabel").html("Show")
					.attr({"data-numformat":",f","data-prefix":"$","data-suffix":"","data-showpercentage":"yes"})
					.unbind("click").bind("click",function(){
						System.getDemisionValue(this);
					}));
		}catch(ex){
			console.log(ex);
		}
	};*/
	var onChangeOfStrength = function(obj){
		var key = $(obj).attr("key");
		if($(obj).is(":checked")){
			var packSizeDiv = $("<div/>").addClass("btn-group col-sm-12").attr({"data-toggle":"buttons","name":key});
			$("[name='packContent']").append($("<div/>").addClass("col-lg-12 padding-bottom-10").attr({"strenth":key})
					.append($("<label/>").addClass("col-sm-10").html(key))
					.append(packSizeDiv));
			if(This.strenthPack && This.strenthPack[key] && This.strenthPack[key].length>0){
				This.strenthPack[key].sort(function(a, b){
				    return parseInt(a) - parseInt(b);
				});
				for(var i=0;i<This.strenthPack[key].length;i++){
					packSizeDiv.append($("<label/>").addClass("btn btn-default")
							.append( $("<input/>").attr({"type":"checkbox","key":This.strenthPack[key][i]})).append(This.strenthPack[key][i]));
				}
			}
		}else{
			$("[name='packContent']").find("div[strenth='"+key+"']").remove();
		}
	};
	$("[name='submissionDate'],[name='planningMonth']").change(function(){
		var planningMonth = $("[name='planningMonth']").val();
		var submissionDate = $("[name='submissionDate']").val();
		if(submissionDate){
			$("[name='tld']").val(getManipulatedDate("month",planningMonth,submissionDate));
		}else{
			$("[name='tld']").val("");
		}
	});
	var callBackGetForcastData = function(response){
		disableLoading();
		if (response && response.isException) {
			showNotification("error", response.customMessage);
			return;
		}
		$("#search-body").show();
		$("#serach-param").hide();
		$("[href='#hierarchy']").trigger("click");
		/*$("[name='createForecastDiv']").hide();
		$("[name='resultsfound']").hide();*/
		if (response && Object.keys(response).length>0) {
			This.myForecast = response;
			if(This.myForecast.planStatus === "RELEASED" || This.myForecast.planStatus === "ARCHIVED"){
				$("#saveForecast").hide();
			}else{
				$("#saveForecast").show();
			}
			$("[name='preSubmission']").hide();
			$("[name='postSubmission']").show();
			
			//planing_scenario,forecast_plan_type
			This.forecastType = This.myForecast.forecastPlanType;
			var schenarioText = "";
			$("[name='submissionDate']").val(This.myForecast.submissionDate);
			$("[name='planningMonth']").val(This.myForecast.planningMonth);
			$("[name='tld']").val(getManipulatedDate("month",This.myForecast.planningMonth,This.myForecast.submissionDate));
			$("[name='revisionPeriodValue']").val(This.myForecast.revisionPeriod);
			$("[name='revisionPeriodValue']").slider('setValue', This.myForecast.revisionPeriod, true, false);
			$("[name='revisionPeriodValue']").parents("section").find("strong").html(This.myForecast.revisionPeriod+" Months");
			$("[name='planningCycleStart']").val(This.myForecast.planningCycleDate);
			$("[name='planningHorizon']").val(This.myForecast.planningHorizon);
			$("[name='planningHorizon']").slider('setValue', This.myForecast.planningHorizon, true, false);
			$("[name='planningHorizon']").parents("section").find("strong").html(This.myForecast.planningHorizon+" Months");
			if(This.myForecast.shareTrend && This.myForecast.shareTrend !== "0")
				$("[name='marketshareprice']").val(This.myForecast.shareTrend);
			$("[name='marketsharetrend']").val(This.myForecast.revisionPeriodType);
			$("[name='forecastname']").val(This.myForecast.name);
			$("[name='pricesType']").val(This.myForecast.pricesType);
			$("[name='pricingsourcecombo']").val(This.myForecast.pricingSource);
			$("[name='planDescription']").val(This.myForecast.description);
			$("[name='planStatus']").find("label").removeClass("active");
			$("[name='planStatus']").find("input[name='status'][value='"+This.myForecast.planStatus+"']").prop("checked","checked").parent().addClass("active");
			if(This.myForecast.packSize && Object.keys(This.myForecast.packSize).length>0){
				$("[name='preSubmission']").find("tbody").empty();
				if(This.forecastType === "presubmission"){
					$("[name='preSubmission']").show();
					$("[name='postSubmission']").hide();
					schenarioText = "Pre Submission ("+This.myForecast.planingScenario+")";
				}else{
					$("[name='preSubmission']").hide();
					$("[name='postSubmission']").show();
					schenarioText = "Post Submission";
				}
				$("#editForecast").show();
				$("#wid-id-0").find("header").find("h2").html("Forecast Details ("+This.productKey+") : "+schenarioText);
				var strengthAndPack = Jsonparse(This.myForecast.packSize);
				/*var allCharts = dc.chartRegistry.list();
				var strengthChart  = allCharts.filter(function(chart){return chart.anchor() === "#strenthContent"})*/
				if(Object.keys(strengthAndPack).length>0){
					//strengthChart[0].filterAll();
					for(key in strengthAndPack){
						if(strengthAndPack.hasOwnProperty(key)){
							if(This.forecastType === "presubmission"){
								addStrengthAndPacks(key,strengthAndPack[key]);
							}else{
								var packs = strengthAndPack[key];
								for(var i=0;i<packs.length;i++){
									 $("#strenthContent").find("table[name='"+key+"']").find("a[name='"+packs[i]+"']")
									 .data("isSelected",true).css({"opacity":"1"});
								}	
							}
						}
					}	
				}
				if(This.productPrices && This.productPrices.length>0)
					This.productPrices = [];
				dc.redrawAll();
				processPlanningAssumptions();
				$("#searchTabContent").find("input,select,textarea").attr({"disabled":"disabled"});
				$("#searchTabContent").find("svg").css({"pointer-events":"none"});
				$("#strenthContent").find("a").css({"pointer-events":"none"});
				$("#searchTabContent").find(".slider-horizontal").css({"pointer-events":"none"});
				$("#searchTabContent").find("a[mode='edit']").hide();
				$("#editForecast").show();
				getForecastMetaData(["PRICE"]);
			}
		}
	};
	var constructForeCastGrid = function(foreCastData){
		$("[name='forecastgrid']").show().find("tbody").empty();
		$("[name='forecastgrid']").parent().prev().hide();
		var pricingSource = {
			ims:"IMS",
			awp:"AWP",
			wac:"WAC"
		};
		foreCastData = foreCastData.sort(function(a,b){
			return a.version>b.version?1:a.version<b.version?-1:0;
		});
		for(var i=0;i<foreCastData.length;i++){
			var row = $("<tr/>").addClass("cursor-pointer").data("rowid",foreCastData[i].id),marketShare = [];
			if(foreCastData[i].marketShares)
				marketShare = Jsonparse(foreCastData[i].marketShares);
			var strengthRow = $("<tr/>").attr({"name":"strengthRow"}).hide(),strength;
			if(foreCastData[i].packSize){
				foreCastData[i].packSize = Jsonparse(foreCastData[i].packSize);
				var pricesType = "";
				if(foreCastData[i].pricesType){
					if(foreCastData[i].pricesType === "minimum")
						pricesType = "Min Mkt Price";
					else if(foreCastData[i].pricesType === "simpleAvg")
						pricesType = "Simple Avg. Price";
					else
						pricesType = "Adopt from - "+foreCastData[i].pricesType;
				}
				if(foreCastData[i].forecastPlanType === "postsubmission"){
					strengthRow.append($("<td/>").attr({"colspan":"10"}).append($("<table/>").addClass("width-100per table table-bordered table-striped").append($("<tr/>")
							.append($("<th/>").html("Strength")).append($("<th/>").html("Pack Size")).append($("<th/>").html("Dosage"))
							.append($("<th/>").html("Market Share(%)"))
							.append($("<th/>").html("Substitution Rate(%)"))
							.append($("<th/>").html("Substitution Growth Rate(%)"))
							.append($("<th/>").html("Annual Price Erosion(%)"))
							.append($("<th/>").html("Price in $ ("+pricingSource[foreCastData[i].pricingSource]+")</br>"+pricesType))
							)));
				}else{
					strengthRow.append($("<td/>").attr({"colspan":"10"}).append($("<table/>").addClass("width-100per table table-bordered table-striped").append($("<tr/>")
							.append($("<th/>").html("Strength")).append($("<th/>").html("Pack Size")).append($("<th/>").html("Mkt. Share(%)"))
							.append($("<th/>").html("Gen. Conversion(%)"))
							.append($("<th/>").html("Initial Price </br> Percent of GAWP(%)"))
							.append($("<th/>").html("Price Change(%)"))
							.append($("<th/>").html("Initial Rebate Level(%)"))
							.append($("<th/>").html("year 1"))
							.append($("<th/>").html("year 2"))
							.append($("<th/>").html("year 3"))
							.append($("<th/>").html("year 4"))
							.append($("<th/>").html("year 5"))
							.append($("<th/>").html("Launch Price"))
							)));
				}
				if(Object.keys(marketShare).length>0){
					strength = Object.keys(foreCastData[i].packSize).toString();
					for(var j=0;j<marketShare.length;j++){
						if(foreCastData[i].forecastPlanType === "postsubmission"){
							strengthRow.find("table").append($("<tr/>")
									.append($("<td/>").html(marketShare[j].strength))
									.append($("<td/>").html(marketShare[j].packsize))
									.append($("<td/>").html(marketShare[j].df))
									.append($("<td/>").css({"text-align":"right"}).html(marketShare[j].marketshare || ""))
									.append($("<td/>").css({"text-align":"right"}).html(marketShare[j].subsitutitionrate || ""))
									.append($("<td/>").css({"text-align":"right"}).html(marketShare[j].substitutiongrowthrate || ""))
									.append($("<td/>").css({"text-align":"right"}).html(marketShare[j].priceerosionrate || ""))
									.append($("<td/>").css({"text-align":"right"}).html(marketShare[j].price || "")))
						}else{
							strengthRow.find("table").append($("<tr/>")
									.append($("<td/>").html(marketShare[j].strength))
									.append($("<td/>").html(marketShare[j].packsize))
									.append($("<td/>").css({"text-align":"right"}).html(marketShare[j].apotexGmShare))
									.append($("<td/>").css({"text-align":"right"}).html(marketShare[j].gmConversion || ""))
									.append($("<td/>").css({"text-align":"right"}).html(marketShare[j].initialApotexPrice || ""))
									.append($("<td/>").css({"text-align":"right"}).html(marketShare[j].apotexPriceChange || ""))
									.append($("<td/>").css({"text-align":"right"}).html(marketShare[j].initialRebateLevel || ""))
									.append($("<td/>").css({"text-align":"right"}).html(marketShare[j].year1?(d3.format(",2f")(marketShare[j].year1)):""))
									.append($("<td/>").css({"text-align":"right"}).html(marketShare[j].year2?(d3.format(",2f")(marketShare[j].year2)):""))
									.append($("<td/>").css({"text-align":"right"}).html(marketShare[j].year3?(d3.format(",2f")(marketShare[j].year3)):""))
									.append($("<td/>").css({"text-align":"right"}).html(marketShare[j].year4?(d3.format(",2f")(marketShare[j].year4)):""))
									.append($("<td/>").css({"text-align":"right"}).html(marketShare[j].year5?(d3.format(",2f")(marketShare[j].year5)):""))
									.append($("<td/>").css({"text-align":"right"}).html(marketShare[j].launch_price || "")))
						}
					}
				}
			}
			var monthRange ="";
			if(foreCastData[i].planningCycleDate)
			  monthRange = getManipulatedDate("month",0,foreCastData[i].planningCycleDate,"%b %Y")+" To "+getManipulatedDate("month",36,foreCastData[i].planningCycleDate,"%b %Y");
			var releaseAnchor = $("<a/>").html("Release");
			if(foreCastData[i].planStatus !== "RELEASED" && foreCastData[i].planStatus !== "ARCHIVED"){
				releaseAnchor.addClass("btn-outline-warning btn btn-default").unbind("click").bind("click",foreCastData[i].id,function(e){
					e.preventDefault();
					e.stopPropagation();
					releasePlan(e.data);
				});
			}else{
				releaseAnchor.html("Released").addClass("btn-outline-success btn btn-default");
				releaseAnchor.unbind("click").bind("click",foreCastData[i].id,function(e){
					e.preventDefault();
					e.stopPropagation();
				});
			}
			var plantType = {presubmission:"PRE",postsubmission:"POST"};
			$("[name='forecastgrid']").append(row.append($("<td/>").append($("<a/>").attr({"name":"showstrength"}).append($("<i/>").addClass("fa fa-2x fa-plus-circle"))))
					//.append($("<td/>").html(foreCastData[i].gpi_name))
					.append($("<td/>").html(foreCastData[i].name))
					.append($("<td/>").html(strength))
					.append($("<td/>").css({"text-align":"right"}).html("$"+(d3.format(",2f")(foreCastData[i].totalRevenue))))
					.append($("<td/>").html(monthRange))
					.append($("<td/>").html(foreCastData[i].planStatus))
					.append($("<td/>").html(foreCastData[i].createdBy))
					.append($("<td/>").html(foreCastData[i].createdDate))
					.append($("<td/>").html("V"+foreCastData[i].version+" ("+plantType[foreCastData[i].forecastPlanType]+")"))
					.append($("<td/>").append(releaseAnchor))
					).append(strengthRow);
			row.unbind("click").bind("click",function(){
				enableLoading();
				var id = $(this).data("rowid");
				callAjaxService("getForecastById", callBackGetForcastData,callBackFailure, {forecastVersionId:id}, "POST", null, true);
			});
		}
		$("[name='showstrength']").parent().unbind("click").bind("click",function(e){
			$(this).find("i").toggleClass("fa fa-plus-circle fa fa-minus-circle");
			$(this).parents("tr").next().toggle();
			e.preventDefault();
			e.stopPropagation();
		});
	};
	var downloadUnitExcel = function(){
		var Headers = ""; 
		$("[name=unittable] th").each(function(){
			if (Headers != "")
				Headers += ",";
			Headers += "\""+ $(this).html()+"\""
		});
		var rowValues = {};
		var i =0;
		$("[name=unittable] tbody tr").each(function(){
			i++;
			var columns = ""; 
			$(this).find("td").each(function(){
				if (columns !== "")
					columns += ",";
				if ($(this).find("input") && $(this).find("input").val())
					columns += parseInt($(this).find("input").val().replaceAll(",",""))
				else
					columns +=  $(this).html().replaceAll(",","");
				});
			rowValues[i] = columns;
			});
		
		var request = {
			"rowHeaders" : Headers,
			"rowValues" : JSON.stringify(rowValues),
			"displayName" : $("[name=forecastname]").val() + "_UnitSales"
		};
		 var downloadURL = 'rest/dataAnalyser/excelDownloadFromJSON';
		$.fileDownload(downloadURL, {
			successCallback: function() {
             // $preparingFileModal.dialog('close');
          },
          failCallback: function(responseHtml) {
              //$preparingFileModal.dialog('close');
              $("#error-modal").html($(responseHtml).text());
              $("#error-modal").dialog({ modal: true });
          },
	        httpMethod: "POST",
	        data: request
	    });
	};
	
	var downloadFinExcel = function(){
		var Headers = ""; 
		$("[name=financialTable] thead th").each(function(){
			if (Headers != "")
				Headers += ",";
			Headers += "\""+ $(this).html()+"\""
		});
		var rowValues = {};
		var i =0;
		$("[name=financialTable] tbody tr").each(function(){
			//if ($(this).attr("class")){
				i++;
				var columns = ""; 
				$(this).children().each(function(){
					/*var colspanTD = $(this).attr("colspan");
					if (colspanTD)
						columns += ",,";*/
					if (columns !== "")
						columns += ",";
					if ($(this).find("input") && $(this).find("input").val())
						columns += parseInt($(this).find("input").val().replaceAll(",",""))
					else
						columns +=  $(this).html().replaceAll(",","").replaceAll("$","").replaceAll("&nbsp;","");
					
					var colspanTD = $(this).attr("colspan");
					if (colspanTD)
						columns += ",,";
					});
					/*$(this).find("th").each(function(){
						if (columns !== "")
							columns += ",";
						if ($(this).find("input") && $(this).find("input").val())
							columns += parseInt($(this).find("input").val().replaceAll(",",""))
						else
							columns +=  $(this).html().replaceAll(",","").replaceAll("$","").replaceAll("&nbsp;","");
						
					});*/
					rowValues[i] = columns;
				//}
			
			});
		/*$("[name=financialTable] tbody tr").each(function(){
			if ($(this).attr("class")){
				i++;
				var columns = ""; 
				$(this).find("th").each(function(){
					var colspanTD = $(this).attr("colspan");
					if (colspanTD)
						columns += ",,";
					if (columns !== "")
						columns += ",";
					if ($(this).find("input") && $(this).find("input").val())
						columns += parseInt($(this).find("input").val().replaceAll(",",""))
					else
						columns +=  $(this).html().replaceAll(",","").replaceAll("$","").replaceAll("&nbsp;","");
					});
					rowValues[i] = columns;
				}
			});*/
		var request = {
			"rowHeaders" : Headers,
			"rowValues" : JSON.stringify(rowValues),
			"displayName" : $("[name=forecastname]").val() + "_Financials"
		};
		 var downloadURL = 'rest/dataAnalyser/excelDownloadFromJSON';
		$.fileDownload(downloadURL, {
			successCallback: function() {
              //$preparingFileModal.dialog('close');
          },
          failCallback: function(responseHtml) {
              //$preparingFileModal.dialog('close');
              $("#error-modal").html($(responseHtml).text());
              $("#error-modal").dialog({ modal: true });
          },
	        httpMethod: "POST",
	        data: request
	    });
	};
	var releasePlan = function(versionId){
		var forecastVersion = {
			id : versionId,
			gpiName:This.productKey
		};
		var request = {forecastVersion : JSON.stringify(forecastVersion)};
		callAjaxService("updateArchive", function(response){
			showNotification("success", response);
			$("#myModalsaveForecast").modal('hide');
			$("#search-body").hide();
			$("#serach-param").show();
			checkSearchKey();
		},callBackFailure, request, "POST", null, true);
	};
	var load = function(){
		constructDropDown("[name='searchdropdown']","select qplan_molecule_name,combination from  l2_aptx_qplan_source where (lower(qplan_molecule_name) like '%<Q>%' or lower(te_brand) like '%<Q>%') and combination is not null group by qplan_molecule_name,combination order by qplan_molecule_name",false, null, checkSearchKey, "Select ",This.source, This.database,{displayType:"name"});
	};
	load();
}());