'use strict';
var templates = {};
templates.getWidget = function(options,callback){
	var max = 0;
	$('.jarviswidget').each(function(i,n){
		if ($(n).attr('id') && $(n).attr('id') !== null && $.trim($(n).attr('id'))!== ""){
		  var check = $(n).attr('id').split("-").splice(-1,1);
		  if(!isNaN(check) && check>max){
			  max = check;
		  }else if (check && check.length >0 && !isNaN(check[0])){
			  max = isNaN(check[0]);
		  }
		}
	});
	if (isNaN(max))
		max = 10;
	max = parseInt(max)+1;
	this.options = {
		"width":"col-md-12 col-xs-12 col-sm-12 col-lg-12",
		"widget-class":"jarviswidget jarviswidget-color-light",
		"id":"widget-"+max,
		"istemplate":true,
		"fullscreen":false,
		"icon":"fa fa-fw fa-bar-chart-o",
		"header":"Change Header by clicking Edit"
	};
	options = $.extend( {}, this.options, options );
	var callBackSuccessGetWidget = function(response){
		templates.getWidget.template = Handlebars.compile(response);
		callback(templates.getWidget.template(options));
	};
	callAjaxService("getWidget",callBackSuccessGetWidget,callBackFailure, null, "GET", "html");
};
templates.getImageWidget = function(options,callback){
	var max = 0;
	$('.jarviswidget').each(function(i,n){
		if ($(n).attr('id') && $(n).attr('id') !== null && $.trim($(n).attr('id'))!== ""){
		  var check = $(n).attr('id').split("-").splice(-1,1);
		  if(!isNaN(check) && check>max){
			  max = check;
		  }else if (check && check.length >0 && !isNaN(check[0])){
			  max = isNaN(check[0]);
		  }
		}
	});
	if (isNaN(max))
		max = 10;
	max = parseInt(max)+1;
	this.options = {
		"width":"col-md-12 col-xs-12 col-sm-12 col-lg-12",
		"widget-class":"jarviswidget jarviswidget-color-light",
		"id":"widget-id-image"+max,
		"istemplate":true,
		"fullscreen":false,
		"icon":"fa fa-fw fa-bar-chart-o",
		"header":"Change Header by clicking Edit"
	};
	options = $.extend( {}, this.options, options );
	var callBackSuccessGetWidget = function(response){
		templates.getImageWidget.template = Handlebars.compile(response);
		callback(templates.getImageWidget.template(options));
	};
	callAjaxService("getImageWidget",callBackSuccessGetWidget,callBackFailure, null, "GET", "html");
};
templates.getFlippedCard = function(options,callback){
	this.options = {};
	options = $.extend( {}, this.options, options );
	var callBackSuccessGetCard = function(response){
		templates.getFlippedCard.template = Handlebars.compile(response);
		callback(templates.getFlippedCard.template(options));
	};
	callAjaxService("getFlippedCard",callBackSuccessGetCard,callBackFailure, null, "GET", "html");
};

templates.getResultView = function(viewString, dynamicSearchObject) {
	if(!!!viewString || viewString === '' || !!!dynamicSearchObject) {
		return '';		
	}
	var compiledObj = Handlebars.compile(viewString); 
	return compiledObj(dynamicSearchObject);
};

Handlebars.registerHelper('json', function(context) {
    return JSON.stringify(context);
});

Handlebars.registerHelper('btoaJson', function(context) {
	//console.log(context);
	try{
		return btoa(unescape(encodeURIComponent(JSON.stringify(context))));//btoa(JSON.stringify(context));
	} catch(e) {
		return '';
	}
});

Handlebars.registerHelper('firstComaElement', function(context) {
	if(!context || context === '') {
		return '';
	}
	
	if(context.indexOf(',') === 0) {
		context = context.substring(1, context.length);
	}
	context.replaceAll(',,', '');
	
	context = context.split(',');
	if(context[0] !== '') {
		return context[0];
	}	   
});

Handlebars.registerHelper('getFromLookUp', function(lookUpStr) {
	return lookUpTable.get(lookUpStr);
});

Handlebars.registerHelper('compare_string', function(msg, matchMsg) {
	return msg === matchMsg?true:false;
});

Handlebars.registerHelper('replaceChar', function(msg, replaceChar, toBeReplacedChar) {
	return (!msg) ? msg : msg.replaceAll(replaceChar, toBeReplacedChar);
});

Handlebars.registerHelper('escapeHtmlChars', function(msg) {
	return escapeHtml(msg);
});

Handlebars.registerHelper('checkFromLocalSorageArray', function(storageItemName, keyName, itemName) {
	if(localStorage.getItem(storageItemName)) {
		var arr = JSON.parse(localStorage.getItem(storageItemName))[keyName];
		if(arr.indexOf(itemName)) {
			return true;
		}
	}
	return false;
});


Handlebars.registerHelper('splitToArray', function(splitChar, splitString) {
	if(!splitString || splitString === '') {
		return [];
	}
	if(!splitChar) {
		splitChar = ',';
	}
    return splitString.split(splitChar);
});

templates.getSummary = function(options,callback){
	this.options = {
			'popup-content':"",
			'placement':"left"
	};
	options = $.extend( {}, this.options, options );
	var callBackSuccessGetWidget = function(response){
		templates.getSummary.template = Handlebars.compile(response);
		callback(templates.getSummary.template(options));
	};
	callAjaxService("getSummary",callBackSuccessGetWidget,callBackFailure, null, "GET", "html");
};
templates.getFilterConfig = function(options,callback){
	this.options = {
			'popup-content':""
	};
	options = $.extend( {}, this.options, options );
	var callBackSuccessGetWidget = function(response){
		templates.getFilterConfig.template = Handlebars.compile(response);
		callback(templates.getFilterConfig.template(options));
	};
	callAjaxService("getFilterConfig",callBackSuccessGetWidget,callBackFailure, null, "GET", "html");
};
templates.getSummaryConfig = function(options,callback){
	this.options = {
			'popup-content':""
	};
	options = $.extend( {}, this.options, options );
	var callBackSuccessGetSummaryConfig = function(response){
		templates.getSummaryConfig.template = Handlebars.compile(response);
		callback(templates.getSummaryConfig.template(options));
	};
	callAjaxService("getSummaryConfig",callBackSuccessGetSummaryConfig,callBackFailure, null, "GET", "html");
};
templates.getPie = function(options,callback){
	this.options = {
		"name":"Click to Change",
		"piestyle":"width:170px;height:170px",
		"popup-content":'<div class="smart-form"><div class="note"><strong>Display name</strong></div><label class="input"> <input type="text" class="input-xs" name="displayname"></label></div>',
		"chart-type":"chart-pie",
		"chart-width":"auto",
		"chart-height":"auto"
	};
	options = $.extend( {}, this.options, options );
	var callBackSuccessGetPie = function(response){
		templates.getPie.template = Handlebars.compile(response);
		callback(templates.getPie.template(options));
	};
	callAjaxService("getPie",callBackSuccessGetPie,callBackFailure, null, "GET", "html");
};
templates.getDatatable = function(options,callback,_idCounter){
	this.options = {
			"table-header":"Data Table",
			"idCounter":_idCounter,
			"width":"col-md-12 col-xs-12 col-sm-12 col-lg-12"
	};
	options = $.extend( {}, this.options, options );
	var callBackSuccessGetDatatable = function(response){
		templates.getDatatable.template = Handlebars.compile(response);
		callback(templates.getDatatable.template(options));
	};
	callAjaxService("getDatatable",callBackSuccessGetDatatable,callBackFailure, null, "GET", "html");
};

templates.getCalendar = function(options,callback,_idCounter){
	this.options = {
			"table-header":"Calendar",
			"idCounter":_idCounter,
			"width":"col-md-12 col-xs-12 col-sm-12 col-lg-12"
	};
	options = $.extend( {}, this.options, options );
	var callBackSuccessGetCalendar = function(response){
		templates.getCalendar.template = Handlebars.compile(response);
		callback(templates.getCalendar.template(options));
	};
	callAjaxService("getCalendar",callBackSuccessGetCalendar,callBackFailure, null, "GET", "html");
};

templates.getNGram = function(options,callback,_idCounter){
	this.options = {
			"table-header":"NGram",
			"idCounter":_idCounter,
			"width":"col-md-12 col-xs-12 col-sm-12 col-lg-12"
	};
	options = $.extend( {}, this.options, options );
	var callBackSuccessGetNGram = function(response){
		templates.getNGram.template = Handlebars.compile(response);
		callback(templates.getNGram.template(options));
	};
	callAjaxService("getNGram",callBackSuccessGetNGram,callBackFailure, null, "GET", "html");
};

templates.getIFrame = function(options,callback,_idCounter){
	this.options = {
			"table-header":"IFrame",
			"idCounter":_idCounter,
			"width":"col-md-12 col-xs-12 col-sm-12 col-lg-12"
	};
	options = $.extend( {}, this.options, options );
	var callBackSuccessGetIFrame = function(response){
		templates.getIFrame.template = Handlebars.compile(response);
		callback(templates.getIFrame.template(options));
	};
	callAjaxService("getIFrame",callBackSuccessGetIFrame,callBackFailure, null, "GET", "html");
};

templates.getHTMLTemplate = function(options,callback,_idCounter){
	this.options = {
			"table-header":"HTML Template",
			"idCounter":_idCounter,
			"width":"col-md-12 col-xs-12 col-sm-12 col-lg-12"
	};
	options = $.extend( {}, this.options, options );
	var callBackSuccessGetHTMLTemplate = function(response){
		templates.getHTMLTemplate.template = Handlebars.compile(response);
		callback(templates.getHTMLTemplate.template(options));
	};
	callAjaxService("getHTMLTemplate",callBackSuccessGetHTMLTemplate,callBackFailure, null, "GET", "html");
};

/**
 *  Manages Profile Publishing
 */
templates.managePublishing = function(_publishData,_type,callbackMethod,formId){
	enableLoading();
	formId = formId|| "frmPublishProfile";
	function formatResult(resp) {return resp.id + ' - ' + resp.name;}
	function formatSelection(resp) {return  resp.name;}
	function initSelection(element, callback) {
	        callback($.map(element.val().split(','), function (id) {
	        	id = id.split(":");
	            return { id: id[0], name: id[1] };
	        }));
	    };
	function inputData(term) {return {q : term,	type : 'A'};};     
	function tabFormatResult(resp) {return resp.profilekey ;}
	function tabFormatSelection(resp) {return  resp.profilekey.replace(/ /g,'');}
	function tabInputData() {return {type : ''};};
	function manageCreateDropDown(_contextData,_dropDownData,tabIndex,isEnable){
		var source   = $("#tabInfo-partial").html();
		var template = Handlebars.compile(source);
		var tabHtml = template(_contextData);
		$('#'+formId+' fieldset').append(tabHtml);
		$('#'+formId+' fieldset .btnRemoveTab').unbind('click').bind('click',function(e){
			$(e.target).prev().remove();
			$(e.target).remove();
		});
		var selectedValue=null; 
		if(_dropDownData){
			selectedValue =_dropDownData.profilekey;
		}
		constructDropDown("#tabprofile_"+tabIndex,"select distinct(profilekey),displayname from profile_analyser where (lower(profilekey) like '%<Q>%' or lower(displayname) like '%<Q>%') and profilekey IS NOT NULL",false, selectedValue,function(e){onChangeDropdown(e);}, "Select Profile",null,null,{buttonWidth:"200px",displayType:"name"});		
//		createDropDown(formId+" #tabprofile_"+tabIndex,'rest/dataAnalyser/getMyProfiles',"POST",_dropDownData,tabFormatResult,tabFormatSelection,undefined,tabInputData,0,1,false,isEnable);
	}
	var onChangeDropdown = function(e){
		var tabIndex = $($(e).parent().parent())[0].id.split('_')[1];
		$("[name='tabname_"+tabIndex+"']").val($("#tabprofile_"+tabIndex).find('button').attr('title'));
	};
	
	var callbackSucessRole = function(response) {
		if (response && response.isException) {
			showNotification("error", response.customMessage);
			return;
		} else {
			showNotification("success", response.message);
			response.action = $("[name=action]").val();
			$('#profilePublishModal1').modal('hide');
			if(callbackMethod)
				callbackMethod(response);	
		}
	};
	function publishProfile() {
		if (!$("#"+formId).valid())
			return;
		var enable = 0;
		if ($("#"+formId+" #userEnabled").is(':checked'))
			enable = 1;
		
		var sources = [];
		$('#'+formId+' fieldset section.tabsection').each(function(index,item){
			var sourceObj = {};
			sourceObj.order = $(item).find("[name^=taborder]").val();
			sourceObj.profileKey = $("#tabprofile_"+Number(index+1)).find("select").val();
			sourceObj.profileDisplayName = $(item).find("[name^=tabname]").val();
			sources.push(sourceObj);
		});
		
		var request = {
			"refId":_publishData.refid,
			"action" : $("#"+formId+" [name=action]").val(),
			"actionId" : _publishData.actionId,
			"displayname" : $("#"+formId+" [name=displayname]").val(),
			"parent" : $("#"+formId+" [name=parent]").val(),
			"sequence" : $("#"+formId+" [name=sequence]").val(),
			"icon" : $("#"+formId+" [name=icon]").val(),
			"enabled" : enable,
			"source" : sources.length>0?JSON.stringify(sources):""
		};
		callAjaxService("PublishProfile", callbackSucessRole, callBackFailure,
				request, "POST");
	};
	
	
	
	var callBackPublishProfile = function(data){
		disableLoading();
		var tabIndex = 0;
		var parentObj = {};
		var publishprofileTemplate = Handlebars.compile(data)(_publishData);
		if($('#profilePublishModal1').length){
			$('#profilePublishModal1').remove();
		}
		$("body").append(publishprofileTemplate);
		$('#profilePublishModal1').modal('show');
		if(_publishData.enabled === 1 || _publishData.enabled === "1"){
			$("#"+formId+" #userEnabled").prop('checked', true);
		}else{
			$("#"+formId+" #userEnabled").prop('checked', false);
		}
		if(_type === "CREATE"){
			parentObj = undefined;
		}else if(_type ===" EDIT"){
			parentObj.id = _publishData.parent;
			parentObj.name = _publishData.parentName;
			$("#"+formId+" [name=action]").prop('disabled', true);
		}else if(_type === "VIEW"){
			parentObj.id = _publishData.parent;
			parentObj.name = _publishData.parentName;
		}else if(_type ==="EDIT"){
			parentObj.id = _publishData.parent;
			parentObj.name = _publishData.parentName;
			$("#"+formId+" [name=action]").prop('disabled', true);
		}
		
		createDropDown(formId+" #parent","rest/MasterData/searchMasterDataFilter","GET",parentObj,formatResult,formatSelection,initSelection,inputData,1,1,true);
		if(_publishData.source.length>30)// Contains Tab Info
		{
			$("#profilePublishModal1 #tabsEnabled").attr("checked","true").unbind("change");
				var tabInfo = JSON.parse(_publishData.source);
				for(var i=0; i<tabInfo.length; i++){
					var context = {index: tabInfo[i].order, value: tabInfo[i].order, profileName: tabInfo[i].profileKey, displayName: tabInfo[i].profileDisplayName};
					var tabProfileObj = {};
					tabProfileObj.id = tabInfo[i].id;
					tabProfileObj.displayname = tabInfo[i].profileDisplayName;
					tabProfileObj.profilekey = tabInfo[i].profileKey;
					tabIndex++;
					manageCreateDropDown(context,tabProfileObj,tabIndex);
				}
		}
		
		if(_type === "CREATE" || _type === "EDIT"){
			$("#profilePublishModal1 #publish").html("").append($("<i/>").addClass("fa fa-arrow-circle-up")).append(" Publish").unbind('click').bind('click',function(){
				publishProfile();}).removeClass("disabled");
			
			$("#profilePublishModal1 #btnAddTab").unbind('click').bind('click',function(){
				tabIndex = $('.tabsection').length+1;
				var context = {index: tabIndex, value: tabIndex};
				manageCreateDropDown(context,undefined,tabIndex);
			});
			
			$("#profilePublishModal1 #tabsEnabled").unbind('change').bind('change',function(e){
				if(e.currentTarget.checked){
					tabIndex = 1;
					$("#profilePublishModal1 #btnAddTab").removeClass("hidden");				
					var profileName = _publishData.displayname.replace(/ /g,'');
					var context = {index: tabIndex, value: tabIndex, profileName: profileName, displayName: _publishData.displayname};
					var tabProfileObj = {};
					tabProfileObj.id = _publishData.refid;
					tabProfileObj.displayname = _publishData.displayname;
					tabProfileObj.profilekey = _publishData.action;
					manageCreateDropDown(context,tabProfileObj,tabIndex);
				}else{
					$('#'+formId+' fieldset section.tabsection').remove();
					$("#profilePublishModal1 #btnAddTab").addClass("hidden");
					$('#'+formId+' fieldset .btnRemoveTab').remove();
					tabIndex=1;
				}
			});
			
			if(_type === "EDIT"){
				$("#profilePublishModal1 #btnAddTab").removeClass("hidden");
			}
		}else if(_type === "VIEW"){
			$("#"+formId+" [name^=tabprofile_]").select2("enable",false);
			$("#"+formId+" .input input").prop('disabled', true);
			$("#"+formId+" .checkbox input").prop('disabled', true);
			$('#'+formId+' fieldset .btnRemoveTab').remove();
			$("#publish").html("Published").unbind('click').addClass("disabled");
		}
	};
	
	/**
	 *  Creates Dropdown with give options & Sets value if exists
	 * 
	 */
	function createDropDown(id,url,_requestType,value,_formatResult,_formatSelection,_initSelection,_inputData,_minInputLength,_maxInputLength,_isMultiple,isEnable){
		url = url || 'rest/dataAnalyser/getMyProfiles';
		$("#"+id).select2('destroy'); 
		
		$("#"+id).select2({
			placeholder : "Tab Profile Menu",
			minimumInputLength : _minInputLength || 0,
			maximumSelectionSize: _maxInputLength || 1,
	        formatSelectionTooBig: function () {
	            // Callback
	            return 'You have already selected Profile';
	        },
			multiple : _isMultiple,
			ajax : { // instead of writing the function to execute the request we
				// use
				// Select2's convenient helper
				url : url,
				dataType : 'json',
				type : _requestType || "POST",
				data : _inputData,
				results : function(data) { 
					return {
						results : data
					};
				}
			},
		//	 initSelection:_initSelection,
			formatResult : _formatResult, // omitted for brevity, see the source
			// of
			// this page
			formatSelection : _formatSelection, // omitted for brevity, see the
			// source of this page
			dropdownCsclass : "bigdrop", // apply css that makes the dropdown
			// taller
			escapeMarkup : function(m) {
				return m;
			} // we do not want to escape
		// markup since we are
		// displaying html in
		// results
		});
		
		if(value){
			$("#"+id).select2("data",value);
		}
		$("#"+id).select2("enable",isEnable === undefined ? true : isEnable);
	};
	
	$("#frmPublishProfile").validate({
		rules : {
			action : {
				required : true
			},
			displayname : {
				required : true
			},
			parent : {
				required : true
			},
			sequence : {
				required : true
			}
		},

		// Messages for form validation
		messages : {
			action : {
				key : 'Please enter Action Key'
			},
			displayname : {
				required : 'Please enter Action Display Name'
			},
			parent : {
				key : 'Please enter Parent Key'
			},
			sequence : {
				required : 'Please enter Display Order'
			}
		},
		// Do not change code below
		errorPlacement : function(error, element) {
			error.insertAfter(element.parent());
		}
	});
	$("#frmPublishProfile").submit(function(e) {
		e.preventDefault();
	});
	
	// Called load Publish Profile Template
	callAjaxService("getPublishProfileTemplate", callBackPublishProfile,callBackFailure, null, "GET", "html");
};

