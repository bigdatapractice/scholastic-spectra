var isICAdmin = 'false';



function savePDF(id) 
{
    $("#btnSaveL3,#btnPrintL3,.close").css('display', 'none');
    var pdf = new jsPDF('p', 'pt', 'letter');
    var source = $(id)[0];

    var margins = {
        top: 80,
        bottom: 60,
        left: 20,
        width: 550
    };
    pdf.fromHTML(source, // HTML string or DOM elem ref.
        margins.left, // x coord
        margins.top, { // y coord
            'width': margins.width, // max width of content on PDF
            // 'elementHandlers': specialElementHandlers
        },
        function() {
            // dispose: object with X, Y of the last line add to the PDF
            // this allow the insertion of new lines after html
        }, margins);
    $("#btnSaveL3,#btnPrintL3,.close").css('display', '');
}

//IE Compatibility
if (!String.prototype.startsWith) {
    String.prototype.startsWith = function(searchString, position) {
        position = position || 0;
        return this.indexOf(searchString, position) === position;
    };
}

//Sort Object with keys in ascending order
function sortObject(obj) {
    return Object.keys(obj).sort().reduce(function(result, key) {
        result[key] = obj[key];
        return result;
    }, {});
}

function printElement(divName) {
    $("#btnSaveL3,#btnPrintL3,.close").hide();
    $(divName).printArea();
    $("#btnSaveL3,#btnPrintL3,.close").show();
}
function invertColor(hex, bw) {
    if (hex.indexOf('#') === 0) {
        hex = hex.slice(1);
    }
    if (hex.length === 3) {
        hex = hex[0] + hex[0] + hex[1] + hex[1] + hex[2] + hex[2];
    }
    if (hex.length !== 6) {
        throw new Error('Invalid HEX color.');
    }
    var r = parseInt(hex.slice(0, 2), 16),
        g = parseInt(hex.slice(2, 4), 16),
        b = parseInt(hex.slice(4, 6), 16);
    if (bw) {
        return (r * 0.299 + g * 0.587 + b * 0.114) > 186? '#000000': '#FFFFFF';
    }
    r = (255 - r).toString(16);
    g = (255 - g).toString(16);
    b = (255 - b).toString(16);
    return "#" + padZero(r) + padZero(g) + padZero(b);
};
function processEvidenceView(role) {
    if (role !== 'ROLE_Evidence' && role != 'ROLE_PUBMED') {
        $('#left-panel').show();
        $('#ribbon').show();
        $('#show-menu').show();
        $('#show-apps').show();
        $('#sendmail').show();
        $('#left-panel').css("visibility", "visible");

    } else {
        $('#left-panel').css("visibility", "hidden");
        $('#ribbon').remove();
        $('#show-menu').remove();
        $('#show-apps').remove();
        $('#sendmail').remove();
        if (role === 'ROLE_Evidence') {
        	$('#header').css("cssText", "background-color: #D51900 !important;");
        } else {
        	$('img#imgLogo').css({"margin-left": "-21px", "margin-top": "-6px", "height": "48px", "width": "auto", "border": "1px solid #eee"});
        	$('img#imgLogo').attr("src", "/SPECTRA/img/pubmed/qFindED-Logo.png");
        }
    }
    if (role === 'ROLE_Evidence_Governance') {
        $('#mainNavigationMenu').hide();
        $('#hide-menu').remove();
    }
	if (role !== 'ROLE_Evidence' ) {
       	$('#topIconContent').find('.navigation-menu-tile').each(function(){
			if($(this).attr('id') != 'userLogout'){
				$(this).hide();
			}
		});
		$('.navigation-menu').css({'min-width': '120px', 'min-height': '101px', 'text-align': 'center'});
    }
}
//processEvidenceView();

function constructSelect2(selector, lookupQuery, isMultiple, selectedValue, selectionType, onChangeFun, placeholder, source, database, otherOptions) {
    function formatResult(resp) {
        return resp.id + (resp.name ? ' - ' + resp.name : '');
    }

    function createSearchChoice(term, data) {
        if ($(data).filter(function() {
                return this.id.localeCompare(term) === 0;
            }).length === 0) {
            return {
                id: term,
                text: term
            };
        }
    }
    source = source || "postgreSQL";
    database = database || "E2EMFPostGres";
    $(selector).select2({
        placeholder: placeholder || "Enter app name",
        minimumInputLength: 1,
        allowClear: true,
        createSearchChoice: (otherOptions && otherOptions.tags) ? createSearchChoice : function() {},
        width: "resolve",
        multiple: isMultiple,
        ajax: {
            url: serviceCallMethodNames["suggestionsLookUp"].url,
            dataType: 'json',
            type: 'GET',
            data: function(term) {
                return {
                    q: term,
                    source: source,
                    database: database,
                    lookupQuery: lookupQuery
                };
            },
            results: function(data) {
                return {
                    results: data
                };
            }
        },
        formatResult: formatResult,
        formatSelection: function(resp) {
            if (selectionType)
                return resp[selectionType];
            else
                return resp.id + (resp.name ? ' - ' + resp.name : '');
        }, // formatSelection,
        dropdownCsclass: "bigdrop",
        escapeMarkup: function(m) {
            return m;
        }
    });
    if (onChangeFun) {
        $(selector).on("change", onChangeFun);
    }
    if (selectedValue) {
        if (typeof(selectedValue) === "object") {
            $(selector).select2("data", selectedValue, true);
        } else {
            $(selector).select2("val", selectedValue, true);
        }
    }
}

function getInterDependentFil(filObj,filterId,e,filterArray) {
  if(filObj !== undefined && filObj.allFil !== undefined){
	$("#applyFilter").attr("disabled", true);
    $('#applyFilter').find('i').removeClass( "fa fa-filter" ).addClass( "fa fa-refresh fa-spin" );
    var filterQuery,filterKey,filterIndex=0,lastFilId=true,filId;
	var filterArray = [];
	if(filObj.alertNotification === 'alertNotification'){
		filterIndex = filterIndex+$("#divSankeyFilter").find('section').length;
	}
	 var filterObj= {};
	 var onSelectFun=function(filObject) {
		 filterObj=filObject;
     	 getInterDependentFil(filterObj,filterId !== undefined && filterId !==null && filterId !=="" ? filterId:'#divSankeyFilter')
     	}
	 var preFil = Jsonparse(localStorage[filObj.divId]) ;
	 $(filterId).find("section").each(function() {
		 var obj = {};
		 var key = $(this).attr("key");
		 var field, value, filter;
		 obj.fieldName = $(this).data("field-name");
		 obj.chat = $(this).data("chat");
		 var keyId = parseInt(key);
		 if(keyId  !== filObj.id ){
			 if($(this).data("field-type") === 'lookupdropdown')
				 filId=keyId;

			 obj.defaultvalue = preFil !==undefined && preFil !==null && Object.keys(preFil).length>0 && preFil[keyId-filterIndex] !=="" ?preFil[keyId-filterIndex].defaultvalue:$(this).find("[name='dropdowndiv']").find("select").val();// $(this).data("defaultvalue")
		 }
		 else{
			 if(preFil !==undefined && preFil !==null && preFil[keyId-filterIndex] !=="")
				 preFil[keyId-filterIndex].defaultvalue = $(this).find("select").val();
			 obj.defaultvalue = $(this).find("[name='dropdowndiv']").find("select").val();
		 }
		 obj.invisible =$(this).data("invisible");
		 obj.displayName = $(this).data("display-name");
		 obj.fieldType = $(this).data("field-type");
		 obj.isSingle = $(this).data("isSingle");
		 obj.lookupquery = $(this).data("lookup-query");
		 obj.searchtype = $(this).data("searchtype");
		 if($(this).data("interdependentcolumns"))
			 obj.interdependentcolumns = $(this).data("interdependentcolumns");
		 obj.id = 'f' + (keyId + 1);
		 obj.dateFormat =   $(this).data("dateformat") || $(this).data("dateFormat");
		 filterArray.push(obj);
 	});
    	
	       
	        var previousFilters = getFilterQueryFromFilterConfig(filterArray,filObj.source,filObj.database);
	       var f =filterArray;//preFil !==undefined && preFil !==null && preFil.length>0 ?preFil:
	       var counter=0,filterLen=f.length;
	       for(var i=0;i<filterLen;i++){ 
	    	   $('#'+f[i].id).data({"prevFil":previousFilters})	
	    	   if(i === filId){
	    		   lastFilId=false;
	    		}else if(filObj.id === filterLen-1  && i === filterLen-2){
	    		   lastFilId=false;
	    		   }
	    		var currentIndex = i+filterIndex;
	        	if(currentIndex !== filObj.id){
					filterObj.index=i;
					var obj=f[i];
					var object={};
			    	   if($(filterId).length>0){
						if(f[i].invisible){
							counter =1;
						}
				    	 var configFil=[];
				    	 object.fieldName = f[i].fieldName;
				    	 if(f[i].interdependentcolumns)
				    		 object.interdependentcolumns = f[i].interdependentcolumns
				    	 configFil = jQuery.extend(true, [], f);
						 configFil.splice(i,1);
						 object.source= filObj.source;
						 object.database = filObj.database;
						 object.interdependentFil = true;
						 object.allFil = jQuery.extend(true, [], f);
						 object.id= i+filterIndex;
						 object.divId = filObj.divId;
						 object.alertNotification = filObj.alertNotification;
						 object.filId = filId;
						 if((i+filterIndex+1-counter) % 4 ===0)
							 object.popright=true;
						 if(filObj.alertNotification === 'alertNotification')
							 object.buttonWidth = '200px';
						 if(filObj.allFil)
							 var profileDatabase = filObj.allFil[i].database?filObj.allFil[i].database:filObj.database
						 var profileSource = object.allFil[i].source?object.allFil[i].source:filObj.source
						 var defaultFilters = getFilterQueryFromFilterConfig(configFil,profileSource,profileDatabase);
						 if(f[i].interdependentcolumns){
							 if(f[i].interdependentcolumns.length > 0){
								 var configFils = [];
								 for(var z = 0; z < f[i].interdependentcolumns.length; z++){
									 for(var x = 0; x < configFil.length; x++){
										 if(configFil[x].fieldName == f[i].interdependentcolumns[z]){
											 configFils.push(configFil[x])
										 }
									 }
								 }
								 configFils = jQuery.extend(true, [], configFils);
								 var interdependentFilters = getFilterQueryFromFilterConfig(configFils,profileSource,profileDatabase);
								 object.previousFilters= interdependentFilters;
								 object.defaultFilters = interdependentFilters;
						 	}	
						}
						 else{
							 object.previousFilters=previousFilters;
							 object.defaultFilters = defaultFilters
						 }
						
				     }
					if(f[i].fieldType === 'lookupdropdown'){
						
						constructDropDown($('#'+obj.id).find("[name='dropdowndiv']"),obj.lookupquery,!obj.isSingle,obj.defaultvalue,onSelectFun,"Select "+ obj.displayName,profileSource,profileDatabase,object,lastFilId);
					}else if(f[i].fieldType === 'select'){
						var selectdata =  obj.lookupquery,selectedValue = "";
						if(selectdata && typeof(obj.lookupquery) !=="object"){
							selectdata = Jsonparse(selectdata);
							if(!selectdata || Object.keys(selectdata).length === 0){
								selectdata = [];
								var values =  obj.lookupquery.split(',');
								if(values && values.length>0){
									for(var m=0;m<values.length;m++){
										selectdata.push({value:values[m],label:values[m]});	
									}								
								}
							}else if(selectdata.filter(function(o){return o.id}).length>0){
								var tempData = [];
								for(var m=0;m<selectdata.length;m++){
									tempData.push({value:selectdata[m].id,label:selectdata[m].text});	
								}
								selectdata = tempData;
							}
						}
						if (obj.defaultvalue && ((typeof o === 'object' && Object.keys(obj.defaultvalue).length>0) || obj.defaultvalue.length > 0)) {
							if(obj.isSingle){
								if(typeof(obj.defaultvalue) === "object" && obj.defaultvalue.id){
									selectedValue = obj.defaultvalue.id;
								}else{
									selectedValue = obj.defaultvalue;
								}
							}else if(obj.defaultvalue.length>0){
								selectedValue = obj.defaultvalue.map(function(o){return (typeof o === 'object' && o.id)?o.id:o});
							}
						}
						constructDropDown($('#'+obj.id).find("[name='dropdowndiv']"),selectdata,!obj.isSingle,selectedValue,onSelectFun,"Select "+ obj.displayName, profileSource ,profileDatabase,object,lastFilId);
					}	
				}
			}
  }
}
/////// filters 
function getFilterQueryFromFilterConfig(filterConfig,source){
	var filterQuery = "";
	if (filterConfig && filterConfig.length > 0) {
		filterQuery = "1=1";
		for ( var i = 0; i < filterConfig.length; i++) {
			var searchtype = filterConfig[i].searchtype || " IN ",
			fromDateOperator=">=",toDateOperator="<=";
			if($.trim(searchtype.toLowerCase()) === "not in"){
				fromDateOperator="<=";
				toDateOperator=">=";
			}
			if (filterConfig[i]["fieldType"] === 'lookup' || filterConfig[i]["fieldType"] === 'select' || filterConfig[i]["fieldType"] === 'lookupdropdown') {
				var lookupIds = "",selectedIds;
				var lookupData = filterConfig[i]["defaultvalue"];
				if (!lookupData || lookupData.length === 0)
					continue;
				if(filterConfig[i]["fieldType"] === 'lookupdropdown' && filterConfig[i].isSingle && !lookupData.trim())
					continue;
				if(lookupData && filterConfig[i].isSingle){
					if(filterConfig[i]["fieldType"] === 'select' && typeof(lookupData) === "object" && lookupData.id){
						lookupData = lookupData.id;
					 }
					var selectedValue;
					if((filterConfig[i]["fieldType"] === 'lookupdropdown' || filterConfig[i]["fieldType"] === 'select') && lookupData.trim()){
						lookupIds = lookupData;
					}else if(lookupData.id !== '""' && lookupData.id !== 'null'){
						lookupIds = lookupData.id;
					}
					if(lookupIds){
						if(lookupIds === "?"){
							if($.trim(searchtype.toLowerCase()) === "in" || $.trim(searchtype.toLowerCase()) === "like")
								filterQuery += " AND (" + addTildInColumnName(filterConfig[i]["fieldName"]) + " is null OR " + addTildInColumnName(filterConfig[i]["fieldName"]) + " = '')";
							else
								filterQuery += " AND (" + addTildInColumnName(filterConfig[i]["fieldName"]) +" is not null AND " + addTildInColumnName(filterConfig[i]["fieldName"]) + " <> '')";
						}else{
							if($.trim(searchtype.toLowerCase()) === "in" || $.trim(searchtype.toLowerCase()) === "not in"){
								lookupIds = ("'" + lookupIds + "'");
								filterQuery += " AND (" + addTildInColumnName(filterConfig[i]["fieldName"])
								+" "+ searchtype+ " (" + lookupIds + ")";
							}else{
								filterQuery+= addTildInColumnName(filterConfig[i]["fieldName"])+" "+searchtype + " '%" + lookupIds + "%'";
							}
						}
					}
				} else if (lookupData && lookupData.length > 0) {
					if( filterConfig[i]["fieldType"] === 'select')
						lookupData = lookupData.map(function(o){return (typeof o === 'object' && o.id)?o.id:o});
					if($.trim(searchtype.toLowerCase()) === "in" || $.trim(searchtype.toLowerCase()) === "not in"){
						if(filterConfig[i]["fieldType"] === 'lookupdropdown' || filterConfig[i]["fieldType"] === 'select'){
							var indexVal = lookupData.indexOf(" ");
							if(indexVal>-1)
								lookupData.splice(indexVal,1);
							if(lookupData.indexOf("?")>-1){
								lookupData = lookupData.map(function(o) { return o == "?" ? "" : o; });
								lookupData.push(null);
							}
							lookupIds = lookupData.map(function(e) { e = e !== undefined && e !== null && e.indexOf('`')>-1 ? e.replace(/`/g, '"') :e;e= e !== undefined && e !== null && e.indexOf("'")>-1?e.replace(/'/g, "\\'"):e; return "'" + e+ "'"}).toString();
						}else{
							for ( var x = 0; x < lookupData.length; x++) {
								if(lookupData[x].id !== '""' && lookupData[x].id !== 'null'){
									if (x === 0 )
										lookupIds = "'" + lookupData[x].id + "'";
									else
										lookupIds += ",'" + lookupData[x].id + "'";
								}
							}
						}	
						if(lookupIds){
							filterQuery += " AND (" + addTildInColumnName(filterConfig[i]["fieldName"])
							+" "+ searchtype+ " (" + lookupIds + ")";								
						}
					}else{
						filterQuery += " AND (";
						var operator = $.trim(searchtype.toLowerCase()) === 'like'?' OR ':' AND ';
						if(filterConfig[i]["fieldType"] === 'lookupdropdown' ||  filterConfig[i]["fieldType"] === 'select'){
							var indexVal = lookupData.indexOf(" ");
							if(indexVal>-1)
								lookupData.splice(indexVal,1);
							if(lookupData.indexOf("?")>-1){
								lookupData = lookupData.map(function(o) { return o == "?" ? null : o; });
							}
						}
						for(var x=0;x<lookupData.length;x++){
							(filterConfig[i]["fieldType"] === 'lookupdropdown' || filterConfig[i]["fieldType"] === 'select')?lookupIds=lookupData[x]:lookupData[x].id;
							if(x === 0)
								filterQuery+= addTildInColumnName(filterConfig[i]["fieldName"])+" "+searchtype + " '%" + lookupIds + "%'";
							else
								filterQuery+= operator +addTildInColumnName(filterConfig[i]["fieldName"])+" "+searchtype + " '%" + lookupIds + "%'";
						}
					}
				}
				
			if ((lookupIds === "" || lookupIds === "''" || lookupIds === '""' || lookupIds === 'null') && $.trim(searchtype.toLowerCase()) === "in")
				filterQuery += " AND (" + addTildInColumnName(filterConfig[i]["fieldName"]) + " is null OR " + addTildInColumnName(filterConfig[i]["fieldName"]) + " = '')";
			else if ((lookupIds === "" || lookupIds === "''" || lookupIds === '""' || lookupIds === 'null') && $.trim(searchtype.toLowerCase()) === "not in")
				filterQuery += " AND (" + addTildInColumnName(filterConfig[i]["fieldName"]) +" is not null AND " + addTildInColumnName(filterConfig[i]["fieldName"]) + " <> '')";
			else if ((lookupIds === "" || lookupIds === "''" || lookupIds === '""' || lookupIds === 'null') && $.trim(searchtype.toLowerCase()) === "like")
				filterQuery += " AND (" + addTildInColumnName(filterConfig[i]["fieldName"]) +" like '%null%' )";
			else if ((lookupIds === "" || lookupIds === "''" || lookupIds === '""' || lookupIds === 'null') && $.trim(searchtype.toLowerCase()) === "not like")
				filterQuery += " AND (" + addTildInColumnName(filterConfig[i]["fieldName"]) +" not like '%null% )";
			else
				filterQuery += " )";
			} else if (filterConfig[i]["fieldType"] === 'NUM') {
				if (filterConfig[i]["from-defaultvalue"]
						&& $.trim(filterConfig[i]["from-defaultvalue"]) !== "")
					filterQuery += " AND " + addTildInColumnName(filterConfig[i]["fieldName"])
							+ " >= " + filterConfig[i]["from-defaultvalue"];
				if (filterConfig[i]["to-defaultvalue"]
						&& $.trim(filterConfig[i]["to-defaultvalue"]) !== "")
					filterQuery += " AND " + addTildInColumnName(filterConfig[i]["fieldName"])
							+ " <= " + filterConfig[i]["to-defaultvalue"];
			}else if(filterConfig[i]["fieldType"] === 'dateRange'){
			var toDate,fromDate;
			var formatForDate = filterConfig[i]["dateFormat"];
			if(formatForDate === undefined){
				formatForDate = 'yyyy-MM-dd';
			}
            var returnedValue = dataAnalyser.storedDateRange(filterConfig[i]["defaultvalue"])
			if(returnedValue && returnedValue != ""){
				 fromDate = returnedValue[0].trim();
				 toDate = returnedValue[1].trim();
			}
			
			if (source ==="postgreSQL"){
				if (fromDate && $.trim(fromDate) !== "")
					filterQuery += " AND "
							+ addTildInColumnName(filterConfig[i]["fieldName"])
							+  fromDateOperator+" to_date('"
							+ fromDate
							+ "', 'MM/dd/yyyy') ";
				if (toDate && $.trim(toDate) !== "")
					filterQuery += " AND "
							+ addTildInColumnName(filterConfig[i]["fieldName"])
							+ toDateOperator+" to_date('"
							+ toDate
							+ "', 'MM/dd/yyyy')+1";
			}else{
				if (fromDate && $.trim(fromDate) !== "")
					filterQuery += " AND unix_timestamp("
							+ addTildInColumnName(filterConfig[i]["fieldName"])
							+ ", '"+formatForDate+"') "+fromDateOperator+" unix_timestamp('"
							+ fromDate
							+ "','MM/dd/yyyy') ";
				if (toDate && $.trim(toDate) !== "")
					filterQuery += " AND unix_timestamp("
							+ addTildInColumnName(filterConfig[i]["fieldName"])
							+ ",'"+formatForDate+"') "+toDateOperator+" unix_timestamp('"
							+ toDate
							+ "','MM/dd/yyyy')";
				}
			
			} else if (filterConfig[i]["fieldType"] === 'DATE' || filterConfig[i]["fieldType"] === 'dynamicDate') {
				var fromDate = filterConfig[i]["from-defaultvalue"];
				var toDate = filterConfig[i]["to-defaultvalue"];
				var formatForDate = filterConfig[i]["dateFormat"];
				if(formatForDate === undefined){
					formatForDate = 'MM/dd/yyyy';
				}
				if(filterConfig[i]["fieldType"] === 'dynamicDate' && isNaN(Date.parse(filterConfig[i]["from-defaultvalue"]))){
					fromDate = eval(filterConfig[i]["from-defaultvalue"]);
					toDate = eval(filterConfig[i]["to-defaultvalue"]);
				}
				if (source ==="postgreSQL"){
					if (filterConfig[i]["from-defaultvalue"]
					&& $.trim(filterConfig[i]["from-defaultvalue"]) !== "")
						filterQuery += " AND "
								+ addTildInColumnName(filterConfig[i]["fieldName"])
								+  fromDateOperator+" to_date('"
								+ fromDate
								+ "', '" +formatForDate+"') ";
					if (filterConfig[i]["to-defaultvalue"]
							&& $.trim(filterConfig[i]["to-defaultvalue"]) !== "")
						filterQuery += " AND "
								+ addTildInColumnName(filterConfig[i]["fieldName"])
								+ toDateOperator+" to_date('"
								+ toDate
								+ "','"+formatForDate+"')+1";
				}else{
					if (filterConfig[i]["from-defaultvalue"]
							&& $.trim(filterConfig[i]["from-defaultvalue"]) !== "")
						filterQuery += " AND unix_timestamp("
								+ addTildInColumnName(filterConfig[i]["fieldName"])
								+ ", '"+formatForDate+"') "+fromDateOperator+" unix_timestamp('"
								+ fromDate
								+ "','MM/dd/yyyy') ";
					if (filterConfig[i]["to-defaultvalue"]
							&& $.trim(filterConfig[i]["to-defaultvalue"]) !== "")
						filterQuery += " AND unix_timestamp("
								+ addTildInColumnName(filterConfig[i]["fieldName"])
								+ ",'"+formatForDate+"') "+toDateOperator+" unix_timestamp('"
								+ toDate
								+ "','MM/dd/yyyy')";
				}
			} else if(filterConfig[i]["fieldType"] === 'raw') {
				filterQuery += " AND ";
				filterQuery+=filterConfig[i]["defaultvalue"];
			}
			 else {
					if (filterConfig[i]["defaultvalue"] && $.trim(filterConfig[i]["defaultvalue"]) !== ""){
						if($.trim(searchtype.toLowerCase()) === "in" || $.trim(searchtype.toLowerCase()) === "not in"){
							filterQuery += " AND lower(" + addTildInColumnName(filterConfig[i]["fieldName"])
							+ ") "+searchtype+ " ('" + filterConfig[i]["defaultvalue"].toLowerCase()
							+ "')";
						}else{
							filterQuery += " AND lower(" + addTildInColumnName(filterConfig[i]["fieldName"])
							+ ") "+searchtype+ " '%" + filterConfig[i]["defaultvalue"].toLowerCase()
							+ "%'";
						}
						
					}
					
				}
					
			}
		}
	return filterQuery;
}

function constructDropDown(selector, lookupQuery, isMultiple, selectedValue, onChangeFun, placeholder, source, database, otherOptions,lastFilters) {	
	source = source || "postgreSQL";
	database = database || "E2EMFPostGres";
	var select = $("<select/>");
	var selectedIds = selectedValue;
	if (isMultiple)
	    select.attr({ multiple: "multiple" });
	$(selector).empty();
	$(selector).append(select);
	$(select).multiselect({
	    enableFiltering: true,
	    includeSelectAllOption: true,
	     maxHeight: 400,
	     buttonClass: (otherOptions && otherOptions.disAbledRound)?"btn btn-default textellipsis":"btn btn-default textellipsis border-round",
	    nonSelectedText: placeholder,
	    selectAllValue: "select all", 
	    dropRight: otherOptions? otherOptions.popright:false,
	    onSelectAll: function(event) {
	    	if(onChangeFun && onChangeFun !== undefined && onChangeFun !==""){
	    		if(otherOptions && otherOptions.allFil !==undefined){
	      		  localStorage[otherOptions.divId]= JSON.stringify(otherOptions.allFil);
	      	 }
	    		onChangeFun(otherOptions);
	    	}
	    	},
	    onChange:function(option, checked, select,event) { 
	    	$.ajaxQ.abortAll();
	    	if(onChangeFun && onChangeFun !== undefined && onChangeFun !==""){
	    		if(otherOptions && otherOptions.allFil !==undefined){
	      		  localStorage[otherOptions.divId]= JSON.stringify(otherOptions.allFil);
	      	 }
	    		
	    		 onChangeFun(otherOptions);
	    	}
	    		
	    	}, //onChangeFun(event,otherOptions) || function(event) {},
	    	onDeselectAll: function(event) {
	    		if(onChangeFun && onChangeFun !== undefined && onChangeFun !==""){
	    			if(otherOptions && otherOptions.allFil !==undefined){
	            		  localStorage[otherOptions.divId]= JSON.stringify(otherOptions.allFil);
	            	 }
	    			onChangeFun(otherOptions);
	    		}
	    		
	        },
	    buttonWidth: (otherOptions && otherOptions.buttonWidth) ? otherOptions.buttonWidth : $(selector).width(),
	    deselectAll: true,
	    selectAll:true,
	    includeResetOption: true,
	    resetText: "Clear all",
	    includeResetDivider: true,
	    enableCaseInsensitiveFiltering: true
	});
	function getDependentLookupQuery(query, filterId) {
	    $(filterId).find("section").each(function() {
	        var key = $(this).attr("key");
	        if (query.indexOf("<" + key + ">") > -1) {
	            var field, value, filter = " 1 = 1";
	            field = $(this).attr("field");
	            if ($(this).attr("type") === "lookupdropdown") {
	                value = $(this).find("select").val() || $(this).data("defaultvalue");
	                if (value && value.length > 0)
	                    filter = field + " in (" + (Array.isArray(value) ? value.map(function(e) { return "'" + e + "'" }).toString() : value) + ")";
	            }
	            query = query.replace("<" + key + ">", filter);
	        }
	    });
	    return query;
	}
	
	function getData(q, isGlobal) { 
		if (isGlobal) {
	        $.ajaxQ.abortAll();
	    }
		if ($(selector).find("button").find("i").length === 0)
	        $(selector).find("button").append($("<i/>").addClass("fa fa-refresh fa-spin"));
	    var modifiedQuery = "",defaultFilQuery='';
	    var prevFilter= $(selector).parent().parent().data("prevFil");
	    var lookupQry = lookupQuery;
	    selectedIds = selectedIds ? selectedIds:$(select).val();
	    if(otherOptions && otherOptions.defaultFilters )
	    	lookupQry = lookupQry.replace('where', ' where '+otherOptions.defaultFilters+' AND ');	
	    
	    if(!isGlobal && otherOptions !==undefined && otherOptions.interdependentFil === true && selectedIds !==undefined && selectedIds !== null && selectedIds !=="" && otherOptions.previousFilters ){
	    	var query = lookupQry.split('where');
	    	defaultFilQuery = query[0] +' where '+otherOptions.previousFilters;
	    	lookupQry = lookupQry 
	 } else if(isGlobal && prevFilter !==undefined && prevFilter !== null && prevFilter !=="" && selectedIds !==undefined && selectedIds !== null && selectedIds !==""){
	    	var query = lookupQry.split('where');
	    	defaultFilQuery = query[0] +' where '+prevFilter;
	    	lookupQry = lookupQry 
	    }
	    if (otherOptions && otherOptions.filterId)
	        modifiedQuery = getDependentLookupQuery(lookupQry, otherOptions.filterId);
	    else
	        modifiedQuery = lookupQry;
	    
	    
	    var request = {
	        q: q || "",
	        source: source,
	        database: database,
	        lookupQuery: modifiedQuery
	    };
	    
	    $.ajax({
	        url: serviceCallMethodNames["suggestionsLookUp"].url,
	        type: "get",
	        data: request,
	        global: isGlobal || false,
	        dataType: "json",
	        //enctype: 'multipart/form-data',
	        success: function(response) {
	            $(selector).find("button").find("i").remove();
	            //if(isSearch){
	            if (response && response.length > 0) {
	                var options = [];
	                if (!isMultiple)
	                    options.push({ label: placeholder, value: '' });
	                for (var i = 0; i < response.length; i++) {
	                    var optionHtml;
	                    if (otherOptions && otherOptions.displayType) {
	                        if (otherOptions.displayType === "name")
	                            optionHtml = (response[i].name || "") + (response[i].count ? " (" + response[i].count + ")" : "");
	                        else if (otherOptions.displayType === "id")
	                            optionHtml = (response[i].id || "") + (response[i].count ? " (" + response[i].count + ")" : "");
	                    } else {
	                        optionHtml = (response[i].id || '') + (response[i].name ? '-' + response[i].name : "") + (response[i].count ? " (" + response[i].count + ")" : "");
	                    }
	                    var resVal= response[i].id && response[i].id.indexOf('"')>-1?response[i].id.replace(/"/g, '`'):response[i].id;
	                    options.push({
	                        value: resVal || '?',
	                        label: optionHtml,
	                        title: optionHtml
	                    });
	                }
	                if ($(select).val())
	                    selectedIds = $(select).val();
	                if (selectedIds && selectedIds.length > 0) {
	                    if (isMultiple) {
	                    	 for (var i = 0; i < selectedIds.length; i++) {
	                             var selectedItem = options.filter(function(obj) { return obj.value === selectedIds[i] });
	                             if((otherOptions && otherOptions.interdependentFil === undefined || isGlobal) ){
	                            	 if (!selectedItem || selectedItem.length === 0) {
	                                     options.push({
	                                         value: selectedIds[i],
	                                         label: selectedIds[i],
	                                         title: selectedIds[i]
	                                     });
	                                 }
	                             }
	                            
	                         }
	                    } else {
	                        var selectedItem = options.filter(function(obj) { return obj.value === selectedIds });
	                        if (!selectedItem || selectedItem.length === 0)
	                            options.push({
	                                value: selectedIds,
	                                label: selectedIds,
	                                title: selectedIds
	                            });
	                    }
	                }
	                
	                $(select).multiselect('dataprovider', options);
	                $(select).val(selectedIds);
	                $(select).multiselect("refresh");
	        	 }
	            $(selector).find(".multiselect-search").val(q).focus();
	            if(lastFilters !== undefined && !lastFilters && otherOptions.filId === otherOptions.id){
	            	$("#applyFilter").attr("disabled", false);
	                $('#applyFilter').find('i').removeClass( "fa fa-refresh fa-spin" ).addClass( "fa fa-filter" );
	              }
	        
	            //}
	            /*else{
	            	if(response && response.length>0){
		     			for(var i=0;i<response.length;i++){
		     				var optionHtml = (response[i].name || response[i].id)+(response[i].count?"("+response[i].count+")":"");
		     				select.append($("<option/>").attr({"value":response[i].id}).html(optionHtml));
		     			}
		     		}
	            	
	            }*/
	            if (otherOptions && otherOptions.isOnEventSearch) {
	                $(selector).find(".multiselect-search").on("keypress", function(e) {
	                    var searchKey = $(this).val();
	                    if (searchKey && (e.charCode === 13 || e.keyCode === 13)) {
	                        getData(searchKey, true);
	                    }
	                });
	                $(selector).find(".input-group-addon").addClass("cursor-pointer").unbind("click").bind("click", function() {
	                    var searchKey = $(this).next().val();
	                    if (searchKey) {
	                        getData(searchKey, true);
	                    }
	                });
	            } else {
	                $(selector).find(".multiselect-search").on("input", function() {
	                    getData($(this).val(), true);
	                });
	            }
	         },
	   });
	}
	if (lookupQuery && typeof(lookupQuery) === "object") {
	//    if (!isMultiple)
	//       lookupQuery.unshift({ label: placeholder, value: '' });
	    $(select).multiselect('dataprovider', lookupQuery);
	    $(select).val(selectedIds);
	    if(lastFilters !== undefined && !lastFilters && otherOptions.filId === otherOptions.id){
	    	$("#applyFilter").attr("disabled", false);
	        $('#applyFilter').find('i').removeClass( "fa fa-refresh fa-spin" ).addClass( "fa fa-filter" );
	      }
	    $(select).multiselect("refresh");
	} else {
		getData();
	}
	
	/*if (onChangeFun){
		$(selector).on("change",onChangeFun);
	}
	if(selectedValue){
		if(typeof(selectedValue) === "object"){
			$(selector).select2("data", selectedValue,true);
		}else{
			$(selector).select2("val", selectedValue,true);	
		}		
	}*/
}

function callAjaxServices(methodNames, successHandler, errorHandler,
    request, serviceTypes, returnDataType, dontAbortAjax) {
    var requests = [];
    if (!dontAbortAjax)
        $.ajaxQ.abortAll();
    if (methodNames && methodNames.length > 0) {
        for (var i = 0; i < methodNames.length; i++) {
            var serviceType = Array.isArray(serviceTypes) ? serviceTypes[i] : serviceTypes;
            var url = (serviceCallMethodNames[methodNames[i]] && serviceCallMethodNames[methodNames[i]].url) ? serviceCallMethodNames[methodNames[i]].url : methodNames;
            var ajaxValue = {
                "type": serviceType,
                "url": url,
                // "contentType": "application/x-www-form-urlencoded;
                // charset=UTF-8",
                "dataType": returnDataType,
                "error": errorHandler,
                "cache": true,
                "complete": function() {

                }
            };
            if (serviceType === "POST") {
                ajaxValue.data = request[i];
            }
            requests.push($.ajax(ajaxValue));
        }
        $.when.apply(this, requests).then(function() {
            var responses = [];
            if (Array.isArray(this)) {
                for (var i = 0; i < arguments.length; i++) {
                    responses.push(arguments[i][0]);
                }
            } else {
                responses.push(arguments[0]);
            }
            successHandler(responses);
        });
    }
}

function callAjaxService(serviceCallMethodName, successHandler, errorHandler,
    request, serviceType, returnDataType, dontAbortAjax) {
    enableLoading();
    if (!dontAbortAjax)
        $.ajaxQ.abortAll();
    if (request && !request.TrimEnable) {
        request = System.TrimObject(request);
    }
    serviceType = serviceType || "GET";
    if (request) {
        $.extend(request, {
            "serviceName": serviceCallMethodName
        });
    } else {
        request = {
            "serviceName": serviceCallMethodName
        };
    }
    returnDataType = returnDataType || "json";
    var isSpecificToUser = false;
    if (!serviceCallMethodNames[serviceCallMethodName]) {
        serviceCallMethodName = "DynamicProfiling";
    }
    if (serviceCallMethodNames[serviceCallMethodName].url) {
        var url = serviceCallMethodNames[serviceCallMethodName].url;
        url = url.replace(".html", ".html?v=" + currentVersionForLocalStorage);
    } else {
        alert("Invalid URL..");
        return;
    }

    url = url.replace(".html", ".html?v=" + currentVersionForLocalStorage);
    var virtaulDir = "";
    if (virtaulDir == null) {
        virtaulDir = "";
    }
    if (virtaulDir.length > 0) {
        while (url.indexOf("..") !== -1) {
            url = url.substring(url.indexOf("..") + 2, url.length);
        }
        if (url.indexOf(virtaulDir, 0) !== 0) {
            url = virtaulDir + url;
        }
    }

    if (serviceCallMethodNames[serviceCallMethodName].EnableServerCaching) {
        var cacheKeyString = JSON.stringify(request);
        if (request.UserBasedServerCachingRequired) {
            isSpecificToUser = true;
        }
        cacheKeyString = modifyCacheKey(cacheKeyString, 0, isSpecificToUser);
        // request = $.(request, { "CacheKey": cacheKeyString });
        var index = System.CacheKeys.MethodNames.find(serviceCallMethodName);
        if (!index) {
            System.CacheKeys.MethodNames.push(serviceCallMethodName);
            System.CacheKeys.Keys.push(cacheKeyString);
        } else if (index.length === 1) {
            System.CacheKeys.MethodNames[index[0]] = serviceCallMethodName;
            System.CacheKeys.Keys[index[0]] = cacheKeyString;
        }
    }
    if (serviceCallMethodNames[serviceCallMethodName].EnableCaching) {

        var cacheKeyString = JSON.stringify(request);
        if (request.UserBasedServerCachingRequired) {
            isSpecificToUser = true;
        }
        cacheKeyString = modifyCacheKey(cacheKeyString, 0, isSpecificToUser);

        request = $.extend(request, {
            "ClientCacheKey": cacheKeyString
        });
        if (request.PagingCriteria && request.PagingCriteria.IsRefresh &&
            !request.IsRefresh) {
            request.IsRefresh = request.PagingCriteria.IsRefresh;
        }
        var isRefresh = isRequestForRefresh(request, 0, false);
        if (isRefresh === true) {
            clearOrUpdateCache([serviceCallMethodName]);
        }
        request = $
            .extend(
                request, {
                    "__BypassClientAndServerCache": isRefresh,
                    "__CacheTimeOut": serviceCallMethodNames[serviceCallMethodName].CacheTimeOut
                });
        System.CachingService.GetAllService.GetAll(request,
            serviceCallMethodName, successHandler, errorHandler,
            serviceType, returnDataType);

        if (!System.CacheKeyIndexor.MethodNames ||
            System.CacheKeyIndexor.MethodNames.length === 0) {
            var item = GetLocalStorage("__CacheKeyIndexor");
            if (item) {
                item = JSON.parse(item);
                System.CacheKeyIndexor.MethodNames = item.MethodNames;
                System.CacheKeyIndexor.Keys = item.Keys;
            }
        }
        var index = 0;
        if (System.CacheKeyIndexor.MethodNames &&
            System.CacheKeyIndexor.MethodNames.length > 0) {
            index = System.CacheKeyIndexor.MethodNames
                .find(serviceCallMethodName);
        }
        if (!index) {
            System.CacheKeyIndexor.MethodNames.push(serviceCallMethodName);
            System.CacheKeyIndexor.Keys.push([cacheKeyString]);
        } else if (index.length > 0 &&
            !System.CacheKeyIndexor.Keys[index[0]].find(cacheKeyString)) {
            System.CacheKeyIndexor.Keys[index[0]].push(cacheKeyString);
        }
        SetLocalStorage("__CacheKeyIndexor", JSON
            .stringify(System.CacheKeyIndexor));
        disableLoading();
    } else {
        var ajaxValue = {
            "type": serviceType,
            "url": url,
            // "contentType": "application/x-www-form-urlencoded;
            // charset=UTF-8",
            "dataType": returnDataType,
            "success": function(response) {
                disableLoading();
                if (serviceCallMethodNames[serviceCallMethodName].DependentMethods) {
                    clearOrUpdateCache(serviceCallMethodNames[serviceCallMethodName].DependentMethods);
                }
                successHandler(response);
            },
            "error": errorHandler,
            "cache": true,
            "complete": function() {
            }
        };
        if (serviceType === "POST") {
            ajaxValue.data = request;
        }
        if ($.ajax) {
            $.ajax(ajaxValue);
        } else {
            jQuery.ajax(ajaxValue);
        }
    }
};

Array.prototype.find = function(searchStr) {
    var returnArray = false;
    for (var i = 0; i < this.length; i++) {
        if (typeof(searchStr) === 'function') {
            if (searchStr.test && searchStr.test(this[i])) {
                if (!returnArray) {
                    returnArray = [];
                }
                returnArray.push(i);
            }
        } else {
            if (this[i] === searchStr) {
                if (!returnArray) {
                    returnArray = [];
                }
                returnArray.push(i);
            }
        }
    }
    return returnArray;
};
Array.prototype.remove = function() {
    var what, a = arguments,
        L = a.length,
        ax;
    while (L && this.length) {
        what = a[--L];
        while ((ax = this.indexOf(what)) !== -1) {
            this.splice(ax, 1);
        }
    }
    return this;
};

Array.prototype.contains = function(k) {
    for (var p in this)
        if (this[p] === k)
            return true;
    return false;
};

System.CloneObject = function(obj) {
    var newObj = (obj instanceof Array) ? [] : {};
    for (i in obj) {
        if (i == 'clone')
            continue;
        if (obj[i] && typeof obj[i] == "object") {
            newObj[i] = System.CloneObject(obj[i]);
        } else
            newObj[i] = obj[i];
    }
    return newObj;
};

var loadHTMLDoc = function(path, callBackFun) {
    callAjaxService(path, callBackFun, function() {
        return null;
    });
};

var getRandomColors = function(count) {
    function rainbow(numOfSteps, step) {
        // This function generates vibrant, "evenly spaced" colours (i.e. no
        // clustering). This is ideal for creating easily distinguishable
        // vibrant markers in Google Maps and other apps.
        // Adam Cole, 2011-Sept-14
        // HSV to RBG adapted from:
        // http://mjijackson.com/2008/02/rgb-to-hsl-and-rgb-to-hsv-color-model-conversion-algorithms-in-javascript
        var r, g, b;
        var h = step / numOfSteps;
        var i = ~~(h * 6);
        var f = h * 6 - i;
        var q = 1 - f;
        switch (i % 6) {
            case 0:
                r = 1, g = f, b = 0;
                break;
            case 1:
                r = q, g = 1, b = 0;
                break;
            case 2:
                r = 0, g = 1, b = f;
                break;
            case 3:
                r = 0, g = q, b = 1;
                break;
            case 4:
                r = f, g = 0, b = 1;
                break;
            case 5:
                r = 1, g = 0, b = q;
                break;
        }
        var c = "#" + ("00" + (~~(r * 255)).toString(16)).slice(-2) +
            ("00" + (~~(g * 255)).toString(16)).slice(-2) +
            ("00" + (~~(b * 255)).toString(16)).slice(-2);
        return (c);
    }
    var coloArray = [];
    for (var i = 0; i < count; i++) {
        coloArray.push(rainbow(count, i));
    }

    return coloArray;

};
var thirdLevelDrillDown = function(docType, vendorId, purchaseDoc, source) {
    // alert();

    $("#poDocView").empty();
    $('#myModalthredlevel #myModalLabel').empty();
    enableLoading("poDocView");
    var data = {
        "searchtype": docType,
        "vendorid": vendorId,
        "ordernumber": purchaseDoc,
        "source": source
    };
    $
        .ajax({
            url: directoryName +
                "/rest/material/getCanadaLevel3MaterialView/",
            type: "POST",
            data: data,
            success: function(response) {
                if (response && response.isException) {
                    showNotification("error", response.customMessage);
                    return;
                }
                if (response != null) {
                    var onHtmlLoadPO = function(HTMLresponse) {
                        if (HTMLresponse && HTMLresponse.isException) {
                            showNotification("error",
                                HTMLresponse.customMessage);
                            return;
                        }
                        $("#poDocView").html(HTMLresponse);
                        var po = response.level3PoDetails || null;

                        if (po != null) {
                            if (po.length > 0 && po[0] != null &&
                                po[0].ekpo_ebeln != null) {
                                $('#myModalthredlevel #myModalLabel')
                                    .html(
                                        'Purchase Order :<b> ' +
                                        po[0].ekpo_ebeln +
                                        '</b><span class="paddingleft100"> Purchase Order Type: <b>' +
                                        po[0].poType +
                                        '</b>');
                                $("#purDocNo").html(po[0].ekpo_ebeln);
                                var vendorInfo = "";
                                if (po[0].lifnr)
                                    vendorInfo += po[0].lifnr + "<br/>";
                                if (po[0].name1)
                                    vendorInfo += po[0].name1 + " ";
                                if (po[0].lname2)
                                    vendorInfo += po[0].lname2 + "<br/>";
                                if (po[0].lort01)
                                    vendorInfo += po[0].lort01 + "<br/>";
                                if (po[0].lort02)
                                    vendorInfo += po[0].lort02 + "<br/>";
                                if (po[0].lpstlz)
                                    vendorInfo += po[0].lpstlz + "<br/>";
                                if (po[0].regio)
                                    vendorInfo += po[0].regio + "<br/>";
                                if (po[0].lstras)
                                    vendorInfo += po[0].lstras + "<br/>";
                                if (po[0].adrnr)
                                    vendorInfo += po[0].adrnr;
                                $("#vendor").html(vendorInfo);

                                var customerInfo = "";
                                if (po[0].name2)
                                    customerInfo += po[0].name2 + "<br/>";
                                if (po[0].stras)
                                    customerInfo += po[0].stras + "<br/>";
                                if (po[0].pfach)
                                    customerInfo += po[0].pfach + "<br/>";
                                if (po[0].pstlz)
                                    customerInfo += po[0].pstlz + "<br/>";
                                if (po[0].ort01)
                                    customerInfo += po[0].ort01;
                                $("#shipTo,#billTo").html(customerInfo);
                                if (po[0].ekorg)
                                    $("#buyername").html(po[0].ekorg);
                                if (po[0].aedat)
                                    $("#createdon").html(po[0].aedat);
                                if (po[0].zterm)
                                    $("#paymentTerm").html(po[0].zterm);
                                if (po[0].ekpo_ebeln)
                                    $("#purDocNo").html(po[0].ekpo_ebeln);
                                var totalNetPrice = 0;
                                for (var i = 0; i < po.length; i++) {
                                    if (po[i] != null) {
                                        po[i].netwr = po[i].netwr || 0;
                                        po[i].netpr = po[i].netpr || 0;
                                        var ekpo_matnr = "";
                                        var maktx = "";
                                        var poType = "";
                                        var ekpo_menge = "";
                                        var lmein = "";
                                        var ebelp = "";
                                        if (po[i].ebelp)
                                            ebelp = po[i].ebelp;
                                        if (po[i].ekpo_matnr)
                                            ekpo_matnr = po[i].ekpo_matnr;
                                        if (po[i].maktx)
                                            maktx = po[i].maktx;
                                        if (po[i].poType)
                                            poType = po[i].poType;
                                        if (po[i].ekpo_menge)
                                            ekpo_menge = po[i].ekpo_menge;
                                        if (po[i].lmein)
                                            lmein = po[i].lmein;
                                        $("<tr/>")
                                            .append(
                                                $("<td/>").html(
                                                    ebelp))
                                            .append(
                                                $("<td/>").html(
                                                    ekpo_matnr))
                                            .append(
                                                $("<td/>").html(
                                                    maktx))
                                            .append(
                                                $("<td/>").html(
                                                    poType))
                                            .append(
                                                $("<td/>").html(
                                                    ekpo_menge))
                                            .append(
                                                $("<td/>").html(
                                                    lmein))
                                            .append(
                                                $("<td/>")
                                                .addClass(
                                                    "text-right")
                                                .html(
                                                    Math
                                                    .round(po[i].netpr * 100) / 100))
                                            .append($("<td/>").html(''))
                                            .append(
                                                $("<td/>")
                                                .addClass(
                                                    "text-right")
                                                .html(
                                                    Math
                                                    .round(po[i].netwr * 100) / 100))
                                            .append($("<td/>").html(""))
                                            .appendTo($("#tb3"));

                                        totalNetPrice += Math
                                            .round(po[i].netwr * 100) / 100;
                                    }
                                }
                                $("#tb3")
                                    .append(
                                        $("<tr/>")
                                        .append(
                                            $("<td/>")
                                            .attr({
                                                "colspan": "7"
                                            })
                                            .html(
                                                "<b>Total</b>"))
                                        .append(
                                            $("<td/>")
                                            .addClass(
                                                "text-right")
                                            .html(
                                                ""))

                                        .append(
                                            $("<td/>")
                                            .addClass(
                                                "text-right")
                                            .html(
                                                "<b>" +
                                                totalNetPrice +
                                                "<b/>")));
                                $("#totalNetPrice").html(totalNetPrice);
                                $(".js-rpd-feature-circle")
                                    .unbind('click')
                                    .bind(
                                        'click',
                                        function() {
                                            expandSection(
                                                this,
                                                po[0].ekpo_ebeln);
                                        });
                                if (po[0].poType == "EXTERNAL PROCESSING") {
                                    $("#linkedPO").removeClass("hide");
                                    $("#linkedPO .js-rpd-feature-circle")
                                        .unbind('click')
                                        .bind(
                                            'click',
                                            function() {
                                                expandLinkedPO(
                                                    this,
                                                    po[0].productionOrder);
                                            });
                                } else if (po[0].poType == "DROPSHIP") {
                                    $("#salesDoc").removeClass("hide");
                                    $("#salesDocNumber").html(
                                        "Sales Document : " +
                                        po[0].salesDoc);
                                    $("#salesDoc .js-rpd-feature-circle")
                                        .unbind('click')
                                        .bind(
                                            'click',
                                            function() {
                                                expandSalesDoc(
                                                    this,
                                                    po[0].salesDoc,
                                                    source);
                                            });
                                }
                            } else {
                                $("#poDocView")
                                    .html(
                                        '<div class="alert alert-danger fade in"><a class="close" data-dismiss="alert" href="#">x</a><h4 class="alert-heading"><i class="fa fa-exclamation-triangle"></i>&nbsp;OOPS!</h4><p class="text-align-left">No record found.</p></div>');
                            }
                            $("#btnPrintL3").unbind('click').bind('click',
                                function(e) {
                                    printElement("#printView");
                                });
                            $("#btnSaveL3").unbind('click').bind('click',
                                function(e) {
                                    savePDF("#printView");
                                });

                            /*
                             * var request = { "entitytype":jandj.kna1,
                             * "criteria":"kunnr:1582,kunnr:1582," };
                             * callAjaxService("getDatafromTable",request,
                             * callBackGetDatafromTable,callBackFailure,"POST");
                             */
                        }
                    };
                    callAjaxService("PODocView", onHtmlLoadPO, function() {
                        alert("error");
                    }, null, "GET", "html");
                }
            }
        });
};
var expandSection = function(obj, key) {
    var numberFormat2 = d3.format(",.2f");
    var type = $(obj).data("type");

    function callBackGetDatafromTable(response) {
        disableLoading("loading" + type);
        if (response && response.isException) {
            showNotification("error", response.customMessage);
            return;
        }
        var data = null;
        if (response)
            data = response["aggregatedGoodsHistoryList"];
        $("#tb" + type)
            .DataTable({
                // "sDom" :
                // "<'dt-top-row'Tlf>r<'dt-wrapper't><'dt-row
                // dt-bottom-row'<'row'<'col-sm-6'i><'col-sm-6
                // text-right'p>>",
                "bProcessing": true,
                "bLengthChange": true,
                // "bServerSide": true,
                "bInfo": true,
                "bFilter": false,
                "bJQueryUI": false,
                "aaData": data,
                "scrollX": true,
                "oTableTools": {
                    "aButtons": [{
                        "sExtends": "collection",
                        "sButtonText": 'Export <span class="caret" />',
                        "aButtons": ["csv", "pdf"]
                    }],
                    "sSwfPath": "js/plugin/datatables/media/swf/copy_csv_xls_pdf.swf"
                },
                "aoColumns": [{
                    "mDataProp": "materialDocument",
                    "sDefaultContent": ""
                }, {
                    "mDataProp": "poHistory",
                    "sDefaultContent": ""
                }, {
                    "mDataProp": "movementType",
                    "mRender": function(data, type, full) {
                        if (data == "101") {
                            return "Goods Received";
                        } else if (data == "543") {
                            return "Goods Issue";
                        } else {
                            return data;
                        }
                    },
                }, {
                    "mDataProp": "quantity",
                    "sDefaultContent": ""
                }, {
                    "mDataProp": "amount",
                    "sDefaultContent": "",
                    "mRender": function(data, type, full) {
                        return numberFormat2(data);
                    },
                    "sClass": "numbercolumn"
                }, {
                    "mDataProp": "currency",
                    "sDefaultContent": "",

                }, {
                    "mDataProp": "postingDate",
                    "sDefaultContent": "",
                    "mRender": function(data, type, full) {
                        return System.formatDate(data);
                    },
                    "sClass": "numbercolumn"
                }]
            });
    }
    enableLoading("loading" + type);
    if ($(obj).parent().next('div').hasClass('feature-folded')) {
        $(obj).html("-");

        $(obj).parent().next('div').removeClass('feature-folded').addClass(
            "minhieght200").find(".js-rpd-feature-set-innerbody").show(200);

        if (type && $(obj).data("loaded") != true) {
            $(obj).data("loaded", true);
            var request = {
                "type": type,
                "poNumber": key
            };
            callAjaxService("getGoodsHistroy", callBackGetDatafromTable,
                callBackFailure, request, "POST");
        }
    } else {
        $(obj).html("+");
        $(obj).parent().next('div').addClass('feature-folded').removeClass(
            "minhieght200").find(".js-rpd-feature-set-innerbody").hide(200);
    }
};

var expandSalesDoc = function(obj, key, source) {
    var type = $(obj).data("type");
    enableLoading("loading" + type);
    if ($(obj).parent().next('div').hasClass('feature-folded')) {
        $(obj).html("-");

        $(obj).parent().next('div').removeClass('feature-folded').addClass(
            "minhieght200").find(".js-rpd-feature-set-innerbody").show(200);

        if (type && $(obj).data("loaded") != true) {
            $(obj).data("loaded", true);
            thirdLevelDrillDownSO('', key, 'undefined', source, 'divSalesDoc');
        }
    } else {
        $(obj).html("+");
        $(obj).parent().next('div').addClass('feature-folded').removeClass(
            "minhieght200").find(".js-rpd-feature-set-innerbody").hide(200);
    }
};

var expandLinkedPO = function(obj, key) {
    var type = $(obj).data("type");

    function callBackGetDatafromTable(response) {
        disableLoading("loading" + type);
        if (response && response.isException) {
            showNotification("error", response.customMessage);
            return;
        }
        var data = null;
        if (response && response["aggregatedProductOrderList"] &&
            response["aggregatedProductOrderList"].length > 0)
            data = response["aggregatedProductOrderList"];
        $("#tb" + type)
            .DataTable({
                "bProcessing": true,
                "bLengthChange": true,
                "bInfo": true,
                "bFilter": false,
                "bJQueryUI": false,
                "aaData": data,
                "scrollX": true,
                "oTableTools": {
                    "aButtons": [{
                        "sExtends": "collection",
                        "sButtonText": 'Export <span class="caret" />',
                        "aButtons": ["csv", "pdf"]
                    }],
                    "sSwfPath": "js/plugin/datatables/media/swf/copy_csv_xls_pdf.swf"
                },
                "aoColumns": [{
                    "mDataProp": "material",
                    "sDefaultContent": ""
                }, {
                    "mDataProp": "batch",
                    "sDefaultContent": ""
                }, {
                    "mDataProp": "movmttype",
                    "sDefaultContent": ""
                }, {
                    "mDataProp": "qty",
                    "sDefaultContent": ""
                }, {
                    "mDataProp": "postingdate",
                    "sDefaultContent": ""
                }]
            });
    }
    enableLoading("loading" + type);
    if ($(obj).parent().next('div').hasClass('feature-folded')) {
        $(obj).html("-");

        $(obj).parent().next('div').removeClass('feature-folded').addClass(
            "minhieght200").find(".js-rpd-feature-set-innerbody").show(200);

        if (type && $(obj).data("loaded") != true) {
            $(obj).data("loaded", true);
            var request = {
                "type": "linktoproduction",
                "productionorder": key
            };
            callAjaxService("getLinkedPO", callBackGetDatafromTable,
                callBackFailure, request, "POST");
        }
    } else {
        $(obj).html("+");
        $(obj).parent().next('div').addClass('feature-folded').removeClass(
            "minhieght200").find(".js-rpd-feature-set-innerbody").hide(200);
    }
};

var thirdLevelDrillDownSO = function(vendorId, purchaseDoc, poNumber, source,
    divId) {
    if (!divId)
        $('#myModalthredlevel #myModalLabel').empty();
    var param = 'so' + ',' + vendorId + ',' + purchaseDoc + ',' + poNumber +
        ',' + source;
    var data = {
        "searchtype": "so",
        "vendorid": vendorId,
        "ordernumber": purchaseDoc,
        "salesorderdetail": poNumber,
        "source": source
    };
    var endpoint = directoryName +
        '/rest/material/getCanadaLevel3MaterialView/';
    divId = divId || "poDocView";
    $("#" + divId).empty();
    enableLoading(divId);
    var numberFormat2 = d3.format(",.2f");
    $
        .ajax({
            url: endpoint,
            data: data,
            type: "POST",
            success: function(response) {
                var htmlSO = function(html) {
                    $("#" + divId).html(html);
                    if (response && response.isException) {
                        showNotification("error", response.customMessage);
                        return;
                    }
                    if (response != null && response.level3SoDetails &&
                        response.level3SoDetails.length > 0 &&
                        response.level3SoDetails[0] != null) {
                        var resp = response.level3SoDetails[0];
                        if (divId == "poDocView")
                            $('#myModalthredlevel #myModalLabel')
                            .text('Sales Order Detailed View')
                            .html(
                                'Sales Order :<b> ' +
                                resp.salesdoc +
                                '</b><span class="paddingleft100"> Sales Order Type: <b>' +
                                resp.soType + '</b>');
                        var address = "";
                        if (resp.name1)
                            address += resp.name1 + '<br>';
                        if (resp.name2)
                            address += resp.name2 + '<br>';
                        if (resp.street)
                            address += resp.street + '<br>';
                        if (resp.city)
                            address += resp.city + ' ';
                        if (resp.regio)
                            address += resp.regio + '<br>';
                        if (resp.postalcode)
                            address += resp.postalcode + '<br>';
                        if (resp.telephone)
                            address += '<br>Telephone : ' + resp.telephone;
                        if (resp.fax)
                            address += '<br>Fax : ' + resp.fax;
                        /*
                         * if (resp.salesdoc)
                         * $('#saleOrdNo').html(resp.salesdoc);
                         */
                        $('#vendorSalesDoc').html(address);
                        if (resp.documentdate)
                            $('#numDateSalesDoc').html(resp.documentdate);
                        if (resp.reference)
                            $('#refDateSalesDoc').html(resp.reference);
                        var totalNetPrice = 0;
                        for (var i = 0; i < response.level3SoDetails.length; i++) {
                            if (response.level3SoDetails[i] != null) {
                                var r = response.level3SoDetails[i];
                                var materialno = "";
                                if (r.materialno)
                                    materialno = r.materialno;
                                var materialdescription = "";
                                if (r.materialdescription)
                                    materialdescription = r.materialdescription;
                                var orderquantity = "";
                                if (r.orderquantity)
                                    orderquantity = r.orderquantity;
                                var description = "";
                                if (r.description)
                                    description = r.description;
                                var netprice = 0;
                                if (r.netprice)
                                    netprice = numberFormat2(r.netprice);
                                var unitofmeasure = "";
                                if (r.unitofmeasure)
                                    unitofmeasure = r.unitofmeasure;
                                var netvalue = "";
                                if (r.netvalue)
                                    netvalue = numberFormat2(r.netvalue);

                                if (r.posnr)
                                    posnr = r.posnr;
                                $("<tr/>")
                                    .append($("<td/>").html(posnr))
                                    .append($("<td/>").html(materialno))
                                    .append(
                                        $("<td/>")
                                        .html(
                                            materialdescription))
                                    .append(
                                        $("<td/>")
                                        .addClass(
                                            'numbercolumn')
                                        .html(orderquantity))
                                    .append(
                                        $("<td/>")
                                        .html(description))
                                    .append(
                                        $("<td/>").addClass(
                                            'numbercolumn')
                                        .html(netprice))
                                    .append(
                                        $("<td/>").html(
                                            unitofmeasure))
                                    .append(
                                        $("<td/>").addClass(
                                            'numbercolumn')
                                        .html(netvalue))
                                    .appendTo($("#tb3SalesDoc"));
                                if (r.netvalue)
                                    totalNetPrice += Math.round(r.netvalue
                                        .replace(",", "") * 100) / 100;
                            }
                        }
                        // console.log(totalNetPrice);
                        $("<tr/>")
                            .append(
                                $("<td/>").html("<b>Total</b>")
                                .attr('colspan', '7'))
                            .append(
                                $("<td/>")
                                .addClass('numbercolumn')
                                .html(
                                    "<b>" +
                                    numberFormat2(totalNetPrice) +
                                    "</b>"))
                            .appendTo($("#tb3SalesDoc"));
                    } else {
                        $("#" + divId)
                            .html(
                                '<div class="alert alert-danger fade in"><a class="close" data-dismiss="alert" href="#">x</a><h4 class="alert-heading"><i class="fa fa-exclamation-triangle"></i>&nbsp;OOPS!</h4><p class="text-align-left">No record found.</p></div>');
                    }

                    $("#btnPrintL3").unbind('click').bind('click',
                        function(e) {
                            printElement("#printView");
                        });
                    $("#btnSaveL3").unbind('click').bind('click',
                        function(e) {
                            savePDF("#printView");
                        });
                };
                callAjaxService("SODocView", htmlSO, function() {
                    alert("error");
                }, null, "GET", "html");
            }

        });
    // callAjaxService("SODocView",onHtmlLoadPO,function(){alert("error")},null,"GET","html");
};

var enableLoading = function(id, top, left) {
    // // Loading spinner Code Start /////////
    id = id || 'loading';
    top = top || '150px';
    left = left || "50%";
    var opts = {
        lines: 13, // The number of lines to draw
        length: 25, // The length of each line
        width: 7, // The line thickness
        radius: 25, // The radius of the inner circle
        corners: 1, // Corner roundness (0..1)
        rotate: 0, // The rotation offset
        direction: 1, // 1: clockwise, -1: counterclockwise
        color: '#000', // #rgb or #rrggbb or array of colors
        speed: 1, // Rounds per second
        trail: 60, // Afterglow percentage
        shadow: false, // Whether to render a shadow
        hwaccel: false, // Whether to use hardware acceleration
        className: 'spinner', // The CSS class to assign to the spinner
        zIndex: 2e9, // The z-index (defaults to 2000000000)
        top: top, // Top position relative to parent in px
        left: left
            // Left position relative to parent in px
    };
    var target = document.getElementById(id);
    var spinner = new Spinner(opts).spin(target);
    $('#' + id).show();
};

var disableLoading = function(id) {
    id = id || 'loading';
    $('#' + id).hide();
};

var callBackFailure = function(response) {
    if (response.status && response.status && response.status == "901") {
        alert("Your Session has Expired ...");
        var loginURL = "login"
        if (lookUpTable.get("logoutURL")) {
            loginURL = lookUpTable.get("logoutURL");
        }
        window.location.href = loginURL;
    } else if (response.statusText != "abort") {
        showNotification("error",
            "Sorry, an error occurred while processing your request.");
    }
};

$.ajaxQ = (function() {
    var id = 0,
        Q = {};

    $(document).ajaxSend(function(e, jqx) {
        jqx._id = ++id;
        Q[jqx._id] = jqx;
    });
    $(document).ajaxComplete(function(e, jqx) {
        delete Q[jqx._id];
    });

    return {
        abortAll: function() {
            var r = [];
            $.each(Q, function(i, jqx) {
                r.push(jqx._id);
                jqx.abort();
            });
            return r;
        }
    };

})();

var GBTSearchValues = {};
var TokenAnalysisSearchValues = undefined;
var panZoomsvgObject;
var PanZoomsvg = function(id) {
    panZoomsvgObject = svgPanZoom('#' + id, {
        panEnabled: true,
        dragEnabled: false,
        controlIconsEnabled: false,
        zoomEnabled: true,
        zoomScaleSensitivity: 0.2,
        minZoom: 0.1,
        maxZoom: 10,
        fit: false,
        center: false,
        beforeZoom: function() {},
        onZoom: function() {
            $(".showAll").show().css({
                'visibility': 'visible'
            });
        },
        onPan: function() {
            $(".showAll").show().css({
                'visibility': 'visible'
            });
        },
        destroy: function(options) {
            for (var eventName in this.listeners) {
                options.svgElement.removeEventListener(eventName,
                    this.listeners[eventName]);
            }
        }
    });
    $("#" + id).mousedown(function() {
        $("#" + id).attr('class', 'grabbableactive');
    }).mouseup(function() {
        $("#" + id).attr('class', 'grabbable');
    }).attr('class', 'grabbable');
    return panZoomsvgObject;
};

var PanZoomsvgObj = function(id) {
    this.refID = svgPanZoom('#' + id, {
        panEnabled: true,
        dragEnabled: false,
        controlIconsEnabled: false,
        zoomEnabled: true,
        zoomScaleSensitivity: 0.2,
        minZoom: 0.1,
        maxZoom: 10,
        fit: false,
        center: false,
        beforeZoom: function() {},
        onZoom: function() {
            $($($("#" + id).closest(".jarviswidget")).find(".showAll")).show()
                .css({
                    'visibility': 'visible'
                });
        },
        onPan: function() {
            $($("#" + id).closest(".jarviswidget")).find(".showAll").show()
                .css({
                    'visibility': 'visible'
                });
        },
        destroy: function(options) {
            for (var eventName in this.listeners) {
                options.svgElement.removeEventListener(eventName,
                    this.listeners[eventName]);
            }
        }
    });
    $("#" + id).mousedown(function() {
        $("#" + id).attr('class', 'grabbableactive');
    }).mouseup(function() {
        $("#" + id).attr('class', 'grabbable');
    }).attr('class', 'grabbable');
    return panZoomsvgObject;
};

function addTableClickListenr(table_id, table, dbTableName, dbColNames, label) {
    $(table_id + ' tbody tr').each(
        function() {
            var nTds = $(this)[0];
            var rowData = table.fnGetData(nTds);
            var i = 0;
            var criteria = '';
            if (dbTableName && dbColNames) {
                entityType = System.dbName + "." + dbTableName;
                var sep = '';
                var end = dbColNames.length;
                dbColNames.forEach(function(col) {
                    sep = (i == end - 1) ? '' : ',';
                    var key;
                    if (rowData["key"]) {
                        key = rowData["key"].split("-");
                        if (key.length > 2)
                            key = key[2];
                    }
                    criteria += '' + col + ':' + key + sep;
                    i++;
                });
                var payload = "entitytype=" + System.dbName + "." +
                    dbTableName + "&criteria=" + criteria;
                $(this).attr({
                    'onClick': "javascript:searchL3DrillDown('" +
                        payload + "', '" + dbTableName +
                        "', '" + criteria + "','" +
                        dbTableName + "')",
                    'data-target': '#myModalthredlevel',
                    'data-toggle': 'modal'
                }).css({
                    'cursor': 'pointer'
                });
            }
        });
};

function searchL3DrillDown(payload, label, criteria, tableName) {
    $("#poDocView").empty();
    enableLoading("poDocView");
    $
        .ajax({
            url: directoryName + '/rest/level3/details/',
            data: payload,
            type: 'POST',
            success: function(l3DataObj) {
                if (l3DataObj && l3DataObj.isException) {
                    showNotification("error", l3DataObj.customMessage);
                    return;
                }
                var action = window.location.hash.replace(/^#/, ''),
                    fun;
                if (lookUpTable.get(action + '_' + label)) {
                    //eval(lookUpTable.get(action+'_'+label));
                    fun = window[lookUpTable.get(action + '_' + label)];
                    fun.apply(null, l3DataObj);
                    return;
                }
                $("#poDocView").empty();
                if (l3DataObj && l3DataObj[0]) {
                    l3Data = l3DataObj[0];
                    var keys = Object.keys(l3Data);
                    var onAjaxLoadSuccess = function(html) {
                        // console.log(html);
                        $('#myModalthredlevel #myModalLabel').text(
                            label.toUpperCase());
                        $("#btnPrintL3").unbind('click').bind('click',
                            function(e) {
                                printElement('#poDocView');
                            });
                        $("#btnSaveL3").unbind('click').bind('click',
                            function(e) {
                                savePDF('#poDocView');
                            });
                        $("#poDocView").html(html);
                        $("#entity").text(tableName.toUpperCase());
                        $("#criteria").text(criteria);
                        var i = 0,
                            idx = keys.length,
                            lvalue, lkey, rvalue, rkey;
                        while (i < idx) {
                            lvalue = l3Data[keys[i]] !== undefined &&
                                l3Data[keys[i]] !== null &&
                                l3Data[keys[i]].trim() != '' ? l3Data[keys[i]] :
                                '';
                            while (lvalue == '' && i < idx) {
                                i++;
                                lvalue = l3Data[keys[i]] !== undefined &&
                                    l3Data[keys[i]] !== null &&
                                    l3Data[keys[i]].trim() != '' ? l3Data[keys[i]] :
                                    '';
                            }
                            lkey = keys[i] !== undefined &&
                                keys[i] !== null &&
                                keys[i].trim() != '' ? keys[i] : '';
                            i++;
                            rvalue = l3Data[keys[i]] !== undefined &&
                                l3Data[keys[i]] !== null &&
                                l3Data[keys[i]].trim() != '' ? l3Data[keys[i]] :
                                '';
                            while (rvalue == '' && i < idx) {
                                i++;
                                rvalue = l3Data[keys[i]] !== undefined &&
                                    l3Data[keys[i]] !== null &&
                                    l3Data[keys[i]].trim() != '' ? l3Data[keys[i]] :
                                    '';
                            }
                            rkey = keys[i] !== undefined &&
                                keys[i] !== null &&
                                keys[i].trim() != '' ? keys[i] : '';
                            if (rvalue == '') {
                                rkey = '';
                            }
                            var colTitle = lookUpTable.get(lkey
                                .toUpperCase());
                            if (colTitle != null) {
                                lkey = colTitle;
                            }
                            colTitle = lookUpTable.get(rkey.toUpperCase());
                            if (colTitle != null) {
                                rkey = colTitle;
                            }
                            if (rvalue != '' || lvalue != '') {
                                $("#tbDynamic tbody")
                                    .append(
                                        $("<tr>\n")
                                        .append(
                                            "<th>" +
                                            lkey +
                                            "</th>\n")
                                        .append(
                                            "<td>" +
                                            lvalue +
                                            "</td>\n")
                                        .append(
                                            "<th>" +
                                            rkey +
                                            "</th>\n")
                                        .append(
                                            "<td>" +
                                            rvalue +
                                            "</td>\n"));
                            }

                            i++;
                        };
                    };
                    callAjaxService("SearchL3View", onAjaxLoadSuccess,
                        function() {
                            alert("error");
                        }, null, "GET", "html");
                } else {
                    $("#myModalLabel").empty();
                    $("#poDocView")
                        .html(
                            '<div class="alert alert-danger fade in"><a class="close" data-dismiss="alert" href="#">x</a><h4 class="alert-heading"><i class="fa fa-exclamation-triangle"></i>&nbsp;OOPS!</h4><p class="text-align-left">No record found.</p></div>');
                }
            }
        });
};

function detailDrillDown(payload, label, criteria, tableName) {
    $("#poDocView").empty();
    enableLoading("poDocView");
    $
        .ajax({
            url: directoryName + '/rest/dataAnalyser/getTableData/',
            data: payload + "&profilekey=" + window.location.hash.replace(/^#/, ''),
            type: 'POST',
            success: function(l3DataObj) {
                if (l3DataObj && l3DataObj.isException) {
                    showNotification("error", l3DataObj.customMessage);
                    return;
                }
                $("#poDocView").empty();
                if (l3DataObj && l3DataObj[0]) {
                    l3Data = l3DataObj[0];
                    var keys = Object.keys(l3Data);
                    var onAjaxLoadSuccess = function(html) {
                        // console.log(html);
                        $('#myModalDrilldown  #myModalLabel').text(label.toUpperCase());
                        $("#btnPrintL3").unbind('click').bind('click',
                            function(e) {
                                printElement('#poDocView');
                            });
                        $("#btnSaveL3").unbind('click').bind('click',
                            function(e) {
                                savePDF('#poDocView');
                            });
                        $("#poDocView").html(html);
                        $("#entity").text(tableName.toUpperCase());
                        $("#criteria").text(criteria);
                        var i = 0,
                            idx = keys.length,
                            lvalue, lkey, rvalue, rkey;
                        while (i < idx) {
                            lvalue = l3Data[keys[i]] !== undefined &&
                                l3Data[keys[i]] !== null &&
                                l3Data[keys[i]].trim() != '' ? l3Data[keys[i]] :
                                '';
                            while (lvalue == '' && i < idx) {
                                i++;
                                lvalue = l3Data[keys[i]] !== undefined &&
                                    l3Data[keys[i]] !== null &&
                                    l3Data[keys[i]].trim() != '' ? l3Data[keys[i]] :
                                    '';
                            }
                            lkey = keys[i] !== undefined &&
                                keys[i] !== null &&
                                keys[i].trim() != '' ? keys[i] : '';
                            i++;
                            rvalue = l3Data[keys[i]] !== undefined &&
                                l3Data[keys[i]] !== null &&
                                l3Data[keys[i]].trim() != '' ? l3Data[keys[i]] :
                                '';
                            while (rvalue == '' && i < idx) {
                                i++;
                                rvalue = l3Data[keys[i]] !== undefined &&
                                    l3Data[keys[i]] !== null &&
                                    l3Data[keys[i]].trim() != '' ? l3Data[keys[i]] :
                                    '';
                            }
                            rkey = keys[i] !== undefined &&
                                keys[i] !== null &&
                                keys[i].trim() != '' ? keys[i] : '';
                            if (rvalue == '') {
                                rkey = '';
                            }
                            var colTitle = lookUpTable.get(lkey
                                .toUpperCase());
                            if (colTitle != null) {
                                lkey = colTitle;
                            }
                            colTitle = lookUpTable.get(rkey.toUpperCase());
                            if (colTitle != null) {
                                rkey = colTitle;
                            }
                            if (rvalue != '' || lvalue != '') {
                                $("#tbDynamic tbody")
                                    .append(
                                        $("<tr>\n")
                                        .append(
                                            "<th>" +
                                            lkey +
                                            "</th>\n")
                                        .append(
                                            "<td>" +
                                            lvalue +
                                            "</td>\n")
                                        .append(
                                            "<th>" +
                                            rkey +
                                            "</th>\n")
                                        .append(
                                            "<td>" +
                                            rvalue +
                                            "</td>\n"));
                            }

                            i++;
                        };
                    };
                    callAjaxService("SearchL3View", onAjaxLoadSuccess,
                        function() {
                            alert("error");
                        }, null, "GET", "html");
                } else {
                    $("#myModalLabel").empty();
                    $("#poDocView")
                        .html(
                            '<div class="alert alert-danger fade in"><a class="close" data-dismiss="alert" href="#">x</a><h4 class="alert-heading"><i class="fa fa-exclamation-triangle"></i>&nbsp;OOPS!</h4><p class="text-align-left">No record found.</p></div>');
                }
            }
        });
};

function addTildInColumnName(columnName) {
    if (columnName && columnName.trim() && columnName.indexOf(" ") > -1 && columnName.indexOf("`") === -1) {
        columnName = "`" + columnName + "`";
    }
    return columnName;
}
var getFormattedDate = function(date, separator, isTime) {
    var year = date.getFullYear(),
        separator = separator || '/';
    var month = (1 + date.getMonth()).toString();
    month = month.length > 1 ? month : '0' + month;
    var day = date.getDate().toString();
    day = day.length > 1 ? day : '0' + day;
    if (isTime)
        return month + separator + day + separator + year + " " + date.getHours() + ":" + date.getMinutes() + ":" + date.getSeconds();
    else
        return month + separator + day + separator + year;
}
var getManipulatedDate = function(type, manipulation, date, dateformate) {

    var formattedDate = new Date(),
        month, day;
    type = type || 'day';
    dateformate = dateformate || "%m/%d/%Y";
    manipulation = manipulation || 0;
    date = new Date(date || Date.now());
    if (type == 'day') {
        formattedDate = new Date(date.getTime() + (manipulation * 1000 * 60 * 60 * 24));
    } else if (type == 'week') {
        formattedDate = new Date(date.getTime() + (manipulation * 1000 * 60 * 60 * 24 * 7));
    } else if (type == 'month') {
        date.setMonth(date.getMonth() + (manipulation * 1));
        formattedDate = date;
    } else if (type == 'quarter') {
        date.setMonth(date.getMonth() + (manipulation * 3));
        formattedDate = date;
    } else if (type == 'year') {
        date.setFullYear(date.getFullYear() + (manipulation * 1));
        formattedDate = date;
    }
    return d3.timeFormat(dateformate)(formattedDate);
};
var getTruncateDate = function(type, date, isLastDate, dateformate) {
    var formattedDate = new Date(),
        month, day;
    type = type || 'month';
    dateformate = dateformate || "%m/%d/%Y";
    date = new Date(date || Date.now());
    if (type == 'month') {
        formattedDate = new Date(date.getFullYear(), date.getMonth(), 1);
        if (isLastDate) {
            formattedDate = new Date(date.getFullYear(), date.getMonth() + 1, 0);
        }
    } else if (type == 'week') {
        var noOfdays = 6;
        if (date.getDay())
            noOfdays = date.getDay() - 1;
        formattedDate = new Date(date.getTime() + (noOfdays) * 1000 * 60 * 60 * 24 * -1);
        if (isLastDate) {
            noOfdays = 0;
            if (date.getDay())
                noOfdays = 7 - date.getDay();
            formattedDate = new Date(date.getTime() + (noOfdays) * 1000 * 60 * 60 * 24);
        }
    } else if (type == 'year') {
        formattedDate = new Date(date.getFullYear(), 0, 1);
        if (isLastDate) {
            formattedDate = new Date(date.getFullYear(), 11, 31);
        }
    } else if (type == 'fyear') {
        if (date.getMonth() > 2) {
            formattedDate = new Date(date.getFullYear(), 3, 1);
            if (isLastDate) {
                formattedDate = new Date(date.getFullYear() + 1, 2, 31);
            }
        } else {
            formattedDate = new Date(date.getFullYear() - 1, 3, 1);
            if (isLastDate) {
                formattedDate = new Date(date.getFullYear(), 2, 31);
            }
        }
    } else if (type == 'quarter') {
        var start = (Math.floor(date.getMonth() / 3) * 3);
        formattedDate = new Date(date.getFullYear(), start, 1);
        if (isLastDate) {
            formattedDate = new Date(date.getFullYear(), start + 3, 0);
        }
    }
    return d3.timeFormat(dateformate)(formattedDate);
};
var processRoles = function(roles, recentRole, secondaryRoles) {
    roles = JSON.parse(roles);
    System.roles = [];
    if (secondaryRoles)
        System.secondaryRoles = JSON.parse(secondaryRoles);
    var tempDiv = $("<div/>").addClass("row");
    for (var i = 0; i < roles.length; i++) {
        System.roles.push(roles[i].name);
        if (roles[i].displayOrder && roles[i].displayOrder > 0) {
            var logoDiv;
            if (roles[i].icon && roles[i].icon !== "")
                logoDiv = $("<i/>").addClass(roles[i].icon);
            else
                logoDiv = $("<img/>").attr("src", roles[i].logo);
            tempDiv.append($("<div/>").addClass("widget_tile " + roles[i].widgetClass + " " + roles[i].widgetColor).data("app", roles[i].name)
                .append($("<a/>").addClass("widget_content").attr({ "name": roles[i].name })
                    .append($("<div/>").addClass("main")
                        .append($("<span/>").addClass("image-helper"))
                        .append(logoDiv)
                        .append($("<span/>").html(roles[i].displayName)))
                ).attr({ "onclick": "updatePrimaryRole(this);", "name": roles[i].name,"title": roles[i].displayName})
            );
        }
        /*System.roles.push(roles[i].name);
        tempDiv
        	.append($("<div/>").addClass("col-sm-4").attr("onclick","javascript:updatePrimaryRole(this);").append($("<a/>").attr({"href":"javascript:void(0);","name":roles[i].name}).append($("<span/>").html(roles[i].displayName)).prepend($("<img/>").attr({"src":roles[i].favicon})))
        		.click(function(){
        			updatePrimaryRole(this);
        		})
        );*/
    }
    localStorage["lastusedapp"] = recentRole;
    $("#show-apps a").attr("data-content", $("<div/>").append(tempDiv.html()).html().replace(/"/g, "'"));
    var cassaRoles = lookUpTable.get("defaultrole");
    if(cassaRoles){
    	cassaRoles = cassaRoles.split(",");
    }else{
    	cassaRoles = ["IIT", "CR&D", "ROLE_CELGENE","Executive Role"];
    }
    var isCassaRoleOnly = true;
    for(i=0; i<System.roles.length; i++){
	    if(cassaRoles.indexOf(System.roles[i]) == -1){
	    	isCassaRoleOnly = false;
	    }
    }
    if(isCassaRoleOnly){
    	$("#show-apps").hide();
    }
};
var updatePrimaryRole = function(obj) {
    var request = {
        "primaryrole": $(obj).find("a").attr("name")
    };
    localStorage["lastusedapp"] = request.primaryrole;
    callAjaxService("updateUserPrimaryRole", function() { 
    	location.href = window.location.href.split('#')[0]; 
    	}, callBackFailure, request, "POST");
};
var getCelgeneSourceSystemData = function() {
	var request = {
			"source":"hive",
			"database" : "cassa_core",
			"table": "systems_status",
			"columns":"source,version,configs",
			"columnConfigurations":'[{"columnName":"source"},{"columnName":"version"},{"columnName":"configs"}]',
			"filter":"1=1",
			"order[0][column]":1,
			"order[0][dir]":'desc',
			"start":0,
			"length":10
		};
	$("#sourceSystemDiv").css({"width":"250px"});
	enableLoading();
    callAjaxService("getDataPagination", function(response){
    	disableLoading();
		if (response && response.isException) {
			showNotification("error", response.customMessage);
			$("#sourceSystemDiv").removeClass("show");
			return;
		}
		if(response && response.data){
			var data =  Jsonparse(response.data);
			$("#sourceSystem").css({"border-bottom":"none"}).empty();
			$("#sourceSystemDiv").addClass("show");
			var sourceObj ={
				"PRIME":{classname:"fa fa-2x fa-file-excel-o",color:"rgb(179, 226, 205)"},
				"ProPEL":{classname:"fa fa-2x fa-database",color:"rgb(203, 213, 232)"},
				"VeRa":{classname:"fa fa-2x fa-database",color:"rgb(253, 205, 172)"},
				"End Point - IRT":{classname:"fa fa-2x fa-book",color:"rgb(241, 226, 204)"}
			};
			for(var i=0;i<data.length;i++){
				var configs = Jsonparse(data[i].configs);
				/*var row = $("<tr/>").addClass("success").append($("<td/>").css({"background-color":sourceObj[data[i].source].color}).append($("<i/>").addClass("marginRight10 "+ sourceObj[data[i].source].classname)).append(data[i].source))
				.append($("<td/>").css({"background-color":sourceObj[data[i].source].color}).html(data[i].version));*/
				var anchor = $("<a/>").addClass("message-center").attr({"href":"javascript:void(0)"})
				.append($("<div/>").addClass("btn btn-default btn-circle-v1").css({"background-color":configs.color}).append($("<i/>").addClass(configs.classname)))
				.append($("<div/>").addClass("mail-contnet").append($("<h5/>").css({"margin":"5px 0 0"}).html(data[i].source)).append($("<span/>").addClass("mail-desc").html("Last Refresh on")).append($("<span/>").addClass("mail-desc").html(data[i].version)));
				//table.append(row);
				$("#sourceSystem").append(anchor);
			}
		}
    },callBackFailure, request, "POST",null,true);
};

//getFavMenuData

/*var favouriteData = function(){
	
	var user  = System.userName;
	var request ={
			 source:'postgreSQL',
			 database:'E2EMFPostGres',
			 //table:'user_favourite_menu',
			 query:"SELECT action FROM user_favourite_menu WHERE user='"+user+"' and enabled='1'",
			 columns:'action',
			 filters:"1 and 1 and user='"+user+"' and enabled='1'",
			 groupByEnabled:true,
			 cacheDisabled:true
	 	};
	
	 function callBackFavSuccess (response){
		 
		 
			//setTimeout(function(){
				
				localStorage.setItem("response", JSON.stringify(response));
				
			     response = JSON.parse(localStorage.getItem("response") || "[]");
			    return response;
				console.log("localStorage.getItem",localStorage.getItem("response"))
				console.log("myresponse",response);
			//},500)
			
		
		 
		 }
		 
		 function callBackFailure(response){
			 
		 }
	
	callAjaxService("getData", callBackFavSuccess, callBackFailure,
			 request, "POST",null,true);
}

*/


	
	
var constructMenu = function(resp) 
{
	
var responseData=resp;

	if(resp[resp.length-1].displayname === "Favourites")
	{
	
	var favouriteMenuChildren =[];
	
	
	var arrayResponse =!localStorage.getItem('response')?{data:[]}:JSON.parse(localStorage.getItem('response'));
	
	
    //console.log("arrayResponse",arrayResponse)
	 //alert(resp.message);
		for(var j =0;j<responseData.length;j++)
		{
			
			if(resp[j].childrens.length !==0){
				
				var responseChildren =resp[j].childrens;
				
				for(var h =0;h<responseChildren.length;h++)
					
					{
					
					//favouriteMenuChildren.push();
					
					for(var k =0;k<arrayResponse.data.length;k++)
					{
						
						if(arrayResponse.data[k].action === responseChildren[h].action){
							favouriteMenuChildren.push(responseChildren[h]);
							
					}
			
					}
						
					
					}
				console.log("fav",favouriteMenuChildren)
			}
}
		
	
		
		resp[resp.length-1].childrens =favouriteMenuChildren;
	}
	
	
	console.log("responseData",resp);
	

	
	var user=System.userName;
	
		
		var request ={
				 source:'postgreSQL',
				 database:'E2EMFPostGres',
				 //table:'user_favourite_menu',
				 query:"SELECT action FROM user_favourite_menu WHERE user='"+user+"' and enabled='1'",
				 columns:'action',
				 filters:"1 and 1 and user='"+user+"' and enabled='1'",
				 groupByEnabled:true,
				 cacheDisabled:true
		 	};
		
		
		
		
		callAjaxService("getData", callBackFavSuccess, callBackFailure,
				 request, "POST",null,true);
					 
			 
			
		
			 function callBackFavSuccess (response1){
				
				 localStorage.setItem("response", JSON.stringify(response1));
			 }
				 
	var respRECursiveFunction = function(resp){
		
		
		
		for(var x = 0; x< resp.length; x++){
			
			console.log("resp.length",resp.length)
			console.log("resp",resp)
			
			
			
			if(resp[x].action == "#" && resp[x].childrens.length == 0){
				console.log("resp[x].action",resp[x].action)
				resp.splice(x,1)
				
				
				respRECursiveFunction(resp);
				
				}
			
			
			
			}
		
		
		
		}
			 
	//var user=localStorage.getItem('searchUser');
	
	//getFavMenuData

		 //getresponse from LocalStorage
		 //push this response to Resp in resp format
	//var user= 'sokumar';
	
	

	
	

	respRECursiveFunction(resp);
			 
    if (resp && resp.length > 0) {
        var menuBar = $("#left-panel > ul");
        var constructMenuFunction = function(obj, response, isParent) {
        	console.log("response.length",response.length)
        	console.log("response",response)
        	
        	var responseData = response;
        	
        	
        
            for (var i = 0; i < response.length; i++) {
            	
            	
                if (response[i] && response[i].action != null &&
                	
                    response[i].action != "") {
                	console.log("response[i]",response[i])
                	
                	console.log("response-updtated",response)
                    userActions.push(response[i].action);
                    // var actionName = response[i].action;
                    if (response[i].source && response[i].source != null &&
                        response[i].source != "") {
                        userActionTabs[response[i].action] = response[i].source.split(",");
                    }
                    
                    var menuLink = $("<a/>").attr({ 
                        "href":response[i].action //||(response[i].action =="Fav_Menu")?"javascript:void(0)":response[i].action
                    
                    }).unbind('click').bind('click', function() {
                        if ($(this).attr("href") === location.hash.replace("#", ""))
                            location.reload();
                        
                    	
                    	
                        
                        
                    });
                    
                  
                    	
              
                    if (response[i].icon && response[i].icon != null &&
                        response[i].icon != null) {
                        menuLink.append($("<i/>").addClass(response[i].icon));
                    }
                    var display = {"display":"none"};
                    
                    if (isParent) {
                    	
                    	display = {"display":"none"};
                        menuLink.append($("<span/>").addClass("menu-item-parent").html(response[i].displayname));
                        console.log("response[i].displayname",response[i].displayname)
                    } else {
                    	display = {"display":"block"};
                        menuLink.append(response[i].displayname);
                    }
                    
                    
                    
                    var subMenu = $("<ul/>").addClass("collapsible collapsible-sub");
                    if (response[i].childrens && response[i].childrens != null &&
                        response[i].childrens.length > 0) {
                    	console.log("response[i].childrens.length",response[i].childrens.length)
                        constructMenuFunction(subMenu, response[i].childrens, 0);
                        menuLink.addClass("collapsible-header waves-effect");
                        obj.append($("<li/>").addClass("bold").append(menuLink).append($("<div/>").addClass('collapsible-body').append(subMenu)));
                    } else{
                    	if(response[i].sequence && response[i].sequence > 0){
                    		menuLink.addClass("collapsible-body waves-effect waves-cyan mr-5");
                        	obj.append($("<li/>").append(menuLink));   
                        	
                    	}
                    }
                        
                }
            }
            
        };
        
     
  


        constructMenuFunction(menuBar, resp, 1);
        
       
       
      

        var locationAddress = $("#left-panel > ul > li >a").attr("href");
        if (locationAddress == "#") {
            $($("#left-panel > ul > li > ul").parent())
                .addClass("active");
            locationAddress = $("#left-panel > ul > li > ul > li > a")
                .first().attr("href");
            if(!locationAddress){
            	locationAddress = $("#left-panel").find("ul").find("a[href!='#']").first().attr("href");
            }
            $($("#left-panel > ul > li.active > ul > li").first())
                .addClass("active");
            $("#left-panel").find("a.collapsible-body:first").parents("ul").show();
        }
        $("#breadcrumbHome").attr("href", "#" + locationAddress);
        if (!userActions.contains("Search"))
            $("#frm-header-search").addClass("hide");
        else
            $("#frm-header-search").removeClass("hide");

        if (!window.location.hash || window.location.hash == "#undefined")
            window.location.hash = "#" + locationAddress;
        else
            checkURL();

    } else {
        alert(resp.message);
    }
			 
			 		 
 function callBackFailure(response){
				 
			 }
			 
};
	
	

var storeLookupTable = function(response) {
    for (var i = 0; i < response.length; i++) {
        lookUpTable.put(response[i]["key"], response[i]["displayName"]);
    }
    if (lookUpTable.get('support_email') != null && lookUpTable.get('support_email') != undefined)
        $("#sendmail a").attr("href", encodeURI("mailto:" + lookUpTable.get('support_email')));
};

var showNotification = function(type, message) {
    disableLoading();
    noty({
        text: '<div><strong>' + type.toUpperCase() +
            ':</strong><div  class="pull-right">X</div> <br/>' + message +
            '!</div>',
        layout: 'topRight',
        type: type,
        theme: 'relax',
        timeout: 10000,
        closeWith: ['click'],
        animation: {
            open: 'animated bounceInRight',
            close: 'animated bounceOutRight'
        }
    });
};
var callGoogleAnalytics = function() {
    if (lookUpTable.get("isOverrideGA")) {
        return;
    }
    try {
        var dimension1Value = System.userDisplayName;
        var dimension2Value = $(".breadcrumb li:last-child").text();
        var dimension3Value = $("#searchconfigterm").val();
        var dimension4Value = System.businessRole || '';
        if (ga) {
            ga('set', 'dimension1', dimension1Value);
            ga('set', 'dimension2', dimension2Value);
            ga('set', 'dimension3', dimension3Value);
            ga('set', 'dimension4', dimension4Value);
            ga('send', 'pageview');
        }
    } catch (e) {

    }
};

var reportIssueObj = function() {
    var $registerForm = $("#frmSankeyFilter").validate({

        // Rules for form validation
        rules: {
            subject: {
                required: true
            }
        },

        // Messages for form validation
        messages: {
            subject: {
                key: 'Please enter subject'
            }
        },

        // Do not change code below
        errorPlacement: function(error, element) {
            error.insertAfter(element.parent());
        }
    });
    $("#frmSankeyFilter").submit(function(e) {
        e.preventDefault();
    });
    this.CreateView = function() {
        $("#smart-form-register input").val("");
        $("#smart-form-register textarea").val("");
        $("#btnCreate").html("Submit").unbind('click').bind('click',
            function() {
                reportIssue.Create();
            });
        $("#frmSankeyFilter em").remove();
        $(".state-error").removeClass("state-error");
    };
    this.Create = function() {
        if (!$("#frmSankeyFilter").valid())
            return;
        var temp = {
            "fields": {
                "project": {
                    "key": "JS"
                },
                "summary": $("[name=subject]").val(),
                "description": $("textarea[name=description]").val(),
                "reporter": {
                    "name": "Nagaraj"
                },
                "issuetype": {
                    "name": "Bug"
                },
                "fixVersions": [{
                    "id": "11000"
                }],
                "components": [{
                    "id": "10600"
                }],
                "customfield_10001": ["SAP"]
            }
        };

        var request = {
            "input": JSON.stringify(temp)
        };
        /*
         * var request = { "key" : $("[name=subject]").val(), "displayname" :
         * $("[name=description]").val() // $("[name=enabled]").val(), };
         */
        callAjaxService("CreateIssue", callbackSucessCreateLookUp,
            callBackFailure, request, "POST");
    };
    var callbackSucessCreateLookUp = function(response) {
        if (response && response.isException) {
            showNotification("error", response.customMessage);
            return;
        } else {
            $('#issueReport').modal('hide');
            showNotification("success", "Issue Report Sucessfully..");
        }
    };
};
var reportIssue = new reportIssueObj();

var preg_quote = function(str) {
    // http://kevin.vanzonneveld.net
    // + original by: booeyOH
    // + improved by: Ates Goral (http://magnetiq.com)
    // + improved by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
    // + bugfixed by: Onno Marsman
    // * example 1: preg_quote("$40");
    // * returns 1: '\$40'
    // * example 2: preg_quote("*RRRING* Hello?");
    // * returns 2: '\*RRRING\* Hello\?'
    // * example 3: preg_quote("\\.+*?[^]$(){}=!<>|:");
    // * returns 3: '\\\.\+\*\?\[\^\]\$\(\)\{\}\=\!\<\>\|\:'

    return (str + '').replace(/([\\\.\+\*\?\[\^\]\$\(\)\{\}\=\!\<\>\|\:])/g,
        "\\$1");
};
var highlight = function(data, search, highlightbackgound, type) {
    if (highlightbackgound == undefined)
        highlightbackgound = true;
    var searchSplit = [];
    // var searchSplit = search.replace(/\+/g," ").split(" ");
    if (type != undefined)
        searchSplit[0] = search;
    else
        searchSplit = search.replace(/\+/g, " ").split(" ");
    var tempData = '' + data;
    for (var i = 0; i < searchSplit.length; i++) {
        if (highlightbackgound)
            tempData = tempData.replace(new RegExp("(" +
                    preg_quote(searchSplit[i]) + ")", 'gi'),
                "<b style='background-color:#CCCCB2'>$1</b>");
        else
            tempData = tempData.replace(new RegExp("(" +
                preg_quote(searchSplit[i]) + ")", 'gi'), "<b>$1</b>");
    }
    tempData.replace("</b> <b>", "</b><b> </b><b>");
    return tempData;
};
var constructTabs = function(THIS) {
    var actionKey = location.hash.replace(/^#/, '');
    if (userActionTabs[actionKey] && userActionTabs[actionKey].length > 0) {
        for (var i = 0; i < userActionTabs[actionKey].length; i++) {
            var newtab = $("<li/>").attr({
                "data-source": userActionTabs[actionKey][i]
            });
            if (i == 0) {
                newtab.addClass("active");
            }
            var newAnchor = $("<a/>").attr({
                "data-toggle": "tab",
                "href": "#mainDivId"
            });
            if (userActionTabs[actionKey][i] == "SAP") {
                newAnchor.append($("<i/>").addClass("fa fa-gears"));
            } else if (userActionTabs[actionKey][i] == "JDE") {
                newAnchor.append($("<i/>").addClass("fa fa-gear"));
            } else {
                newAnchor.append($("<i/>").addClass("fa fa-plane"));
            }
            newtab.append(newAnchor.append($("<span/>").addClass(
                "hidden-mobile hidden-tablet").html(
                " " + userActionTabs[actionKey][i])));
            $("#myTab").append(newtab);
            var source = "'" + userActionTabs[actionKey][i] + "'";
            newtab.unbind('click').bind('click', function() {
                THIS.Load(this);
            });
            /*
             * $("#myTab").append($("<li/>").addCladd("active"))<li
             * class="active" data-source="SAP" ><a data-toggle="tab"
             * href="#mainDivId"><i class="fa fa-gears"></i> <span
             * class="hidden-mobile hidden-tablet">SAP</span></a></li>
             * $("#myTab li[data-source='SAP']")
             */
        }
    }
};
var removeHTMLTags = function(str) {
    str = "<div>" + str + "</div>";
    return $(str).text();
};

String.prototype.replaceAll = function(target, replacement) {
    return this.split(target).join(replacement);
};

function escapeHtml(string) {
    if (!string) {
        return '';
    }
    var entityMap = {
        '&': '&amp;',
        '<': '&lt;',
        '>': '&gt;',
        '"': '&quot;',
        "'": '&#39;',
        '/': '&#x2F;',
        '`': '&#x60;',
        '=': '&#x3D;'
    };
    return String(string).replace(/[&<>"'`=\/]/g, function(s) {
        return entityMap[s];
    });
}

Array.prototype.move = function(old_index, new_index) {
    if (new_index >= this.length) {
        var k = new_index - this.length;
        while ((k--) + 1) {
            this.push(undefined);
        }
    }
    this.splice(new_index, 0, this.splice(old_index, 1)[0]);
    return this; //If it wants to....
};

function addCommasToNumbers(totalRow) {
    if (!totalRow || parseInt(totalRow) < 1000) {
        return totalRow || 0;
    }
    var totalResult = totalRow + '',
        totalResultArr = totalResult.split('.'),
        ResultArr = totalResultArr[0],
        x2 = totalResultArr.length > 1 ? '.' + totalResultArr[1] : '',
        rgxp = /(\d+)(\d{3})/;

    while (rgxp.test(ResultArr)) {
        ResultArr = ResultArr.replace(rgxp, '$1' + ',' + '$2');
    }

    return ResultArr + x2;
}

function sortByProperty(property, exclude, desc) {
    'use strict';
    return function(a, b) {
        var sortStatus = 0;
        if (exclude && exclude.length > 0 && exclude.indexOf(a[property]) != -1) {
            if (desc)
                return -1;
            else
                return 1;
        }
        if (a[property] < b[property]) {
            sortStatus = -1;
        } else if (a[property] > b[property]) {
            sortStatus = 1;
        }

        return sortStatus;
    };
}

var materialData = new Array(); // Collected in Variance page while moving to PO
// Profile
var supplierData = new Array(); // Collected in Variance page while moving to PO
// Profile

var chartColorsConfig = function(item, type) {
    var rtnVal;
    type = type == undefined ? true : type;
    if (item == "Yes" || item == "Favourable" || item == "Issued" ||
        item == "Perfect" || item == "Exact" || item == "1") {
        rtnVal = type == true ? "#2ca02c" : "#d62728";
    } else if (item == "No" || item == "Unfavourable" || item == "Not Issued" ||
        item == "None") {
        rtnVal = type == true ? "#d62728" : "#2ca02c";
    } else if (item == "N/A") {
        rtnVal = "#00B2EE";
    }
    return rtnVal;
};

var varianceChartColorsConfig = function(item) {
    var rtnVal;
    if (item == "None") {
        rtnVal = "#2ca02c";
    } else if (item == "Unfavourable") {
        rtnVal = "#d62728";
    }
    return rtnVal;
};

var ordinalColorCodings = new Array("#ff7f0e", "#1f77b4", "#9467bd", "#8c564b",
    "#e377c2", "#7f7f7f");

jQuery.validator.addMethod("multiemail", function(value, element) {
        if (this.optional(element)) // return true on optional element
            return true;
        var emails = value.split(/[;,]+/); // split element by , and ;
        valid = true;
        for (var i = 0; i < emails.length; i++) {
            value = emails[i];
            valid = valid &&
                jQuery.validator.methods.email.call(this, $.trim(value),
                    element);
        }
        return valid;
    },

    jQuery.validator.messages.email);
jQuery.validator.addMethod("limitto", function(value, element, param) {
        var emails = value.split(/[;,]+/);
        var valid = true;
        if (emails != null && parseInt(emails.length) > parseInt(param))
            valid = false;

        return valid;
    },

    jQuery.validator.messages.email);

function hasRoleExist(roleName) {
    var isRoleExist = false;
    if (System.roles && System.roles.length > 0 && roleName) {
        for (var i = 0; i < System.roles.length; i++) {
            if (System.roles[i].trim() == roleName.trim()) {
                isRoleExist = true;
                break;
            }
        }
    }
    return isRoleExist;
}

function constructGrid(config, overrideFilter) {
    enableLoading();
    localStorage["datatablefilter"] = config.filter;
    $(config.html).html('');
    if (config.drilldowncallback &&
        config.drilldowncallback !== null &&
        config.drilldowncallback !== "") {
        loadScript("js/DataAnalyser/datatableDrilldown.js", null, config.html);
    }
    var columnData = [];
    for (var i = 0; i < config.columns.length; i++) {
        columnData.push({
            columnName: config.columns[i].data
        });
    }
    if (config.columninfo) {
        $(config.html).parent().find("[name='infodiv']").remove();
        $(config.html).parent().append($("<div/>").data("columns", config.columns).attr({ "name": "infodiv" }).addClass("demo infosidebar hide")
            .append($("<legend/>").addClass("no-padding margin-bottom-10")
                .append($("<span/>"))
                .append($("<a/>").addClass("pull-right").unbind('click').bind('click', function() {
                    $(this).parents(".demo").removeClass("activate").addClass("hide");
                }).append($("<i/>").addClass("fa fa-close txt-color-blueDark"))))
            .append($("<section/>")));
    }
    $(config.html)
        .DataTable({
            "dom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-3 col-xs-12 hidden-xs'l><'col-sm-3 col-xs-12 hidden-xs'C>r>" +
                "t" +
                "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
            "processing": true,
            "lengthChange": true,
            "serverSide": true,
            "sort": true,
            "info": true,
            "jQueryUI": false,
            "order": [
                [(config.data && config.data.orderby) ? config.data.orderby : 1, (config.data && config.data.ordertype) ? config.data.ordertype : "desc"]
            ],
            "scrollX": true,
            "responsive": true,
            "scrollY": '65vh',
            "scrollCollapse": true,
			"pageLength":config.pageLength || 10,
            "ajax": {
                "url": config.url,
                "type": "POST",
                "data": function(d) {
                    d.source = config.source,
                        d.database = config.database,
                        d.table = config.table,
                        d.query = config.query,
                        d.groupbyenabled = config.groupByEnabled,
                        d.columns = config.dataColumns,
                        d.columnConfigurations = JSON.stringify(columnData)
                    d.profilekey = location.hash.replace(/^#/, ''),
                        d.filter = overrideFilter || localStorage["datatablefilter"]
                },
                "dataSrc": function(json) {
                    if (json && json.isException) {
                        showNotification("error", json.message);
                        return [];
                    }
                    if (json !== undefined && json !== null) {
                        return JSON.parse(json.data);
                    }
                },
                error: function() {
                    alert('Error while Processing..');
                }
            },
            "initComplete": function() {
                disableLoading();
		if(config.callBack){
                	config.callBack();
                }
            },
            "autoWidth": false,
            "destroy": true,
            "columns": config.columns,
            "fnRowCallback": function(nRow, aData) {
                if (config.drilldowncallback &&
                    config.drilldowncallback !== null &&
                    config.drilldowncallback !== "") {
                    $('td', nRow)
                        .attr({
                            'onClick': "javascript:dataTableDrillDown." + config.drilldowncallback + "(this)"
                        });

                    
                }
		$(nRow).data("request",JSON.stringify(aData));
            },
            "fnHeaderCallback": function(nHead) {
                if ($(config.html + "_wrapper").parent().find("[name='infodiv']").length > 0 && $(nHead).find("i").length === 0) {
                    var data = $(config.html + "_wrapper").parent().find("[name='infodiv']").data();
                    $(nHead).find("th").each(function(index) {
                        var val = $(this).html();
                        var requestData = {
                            source: config.source,
                            database: config.database,
                            table: config.table,
                            filters: config.filter,
                            column: data.columns[index].data
                        };
                        $(this).html($("<div/>").css({ "display": "inline-flex" }).html(val)
                            .prepend($("<i/>").css({ "margin": "4px 5px 0px 0px" }).addClass("fa fa-info-circle txt-color-blue").unbind('click').bind('click', requestData, function(e) {
                                e.preventDefault();
                                e.stopPropagation();
                                enableLoading();
                                $(config.html + "_wrapper").parent().find("[name='infodiv']").find("span").html($(this).parent().text() + " Details");
                                callAjaxService("getcolumnInfo", function(response) { callBackGetColumnInfo(response, $(config.html + "_wrapper").parent().find("[name='infodiv']")) }, callBackFailure, requestData, "POST");
                            })));
                    });
                }
            }
        });
}

function callBackGetColumnInfo(response, div) {
    disableLoading();
    if (response && response.isException) {
        showNotification("error", response.customMessage);
        return;
    }
    if (response !== undefined && response !== null) {
        div.addClass("activate").removeClass("hide");
        //div.find("legend").find("span").html(columntitle+" Details");
        var chartDiv = $("<div/>").attr({ "id": "columnchartdiv" + div.prev().attr("id") });
        div.find("section").html('').append($("<div/>").addClass("panel-group acc-v1 margin-bottom-40")
            .append($("<label/>").addClass("").append("No Of Records : <b class='badge  bounceIn animated bg-color-greenLight'>" + response.noOfRecords + "</b>"))
            .append($("<label/>").addClass("")
                .append("Distinct : <b class='badge  bounceIn animated bg-color-greenLight'>" + response.distinct + "</b>"))
            .append($("<label/>").addClass("")
                .append("Min : <b class='badge  bounceIn animated bg-color-greenLight'>" + response.min + "</b>"))
            .append($("<label/>").addClass("")
                .append("Max : <b class='badge  bounceIn animated bg-color-greenLight'>" + response.max + "</b>"))
            .append($("<label/>").addClass("")
                .append("Average : <b class='badge  bounceIn animated bg-color-greenLight'>" + response.avg.toFixed(2) + "</b>"))
            .append($("<label/>").addClass("")
                .append("Median : <b class='badge  bounceIn animated bg-color-greenLight'>" + response.median + "</b>"))
            .append($("<label/>").addClass("")
                .append("Standard Deviation : <b class='badge  bounceIn animated bg-color-greenLight'>" + response.stdDeviation.toFixed(2) + "</b>"))
            .append($("<div/>").addClass("panel panel-default").append(chartDiv)));
    }
}

function onClickModelTarget(obj) {
    if ($(obj.currentTarget).attr('data-dest')) {
        obj.stopPropagation();
        $($(obj.currentTarget).attr('data-dest')).html($($(obj.currentTarget).attr('data-target')).find('.modal-body').html());
    }
}

function Jsonparse(str, type) {
    var obj = {};
    try {
        obj = JSON.parse(str);
    } catch (e) {
        if (type)
            console.log(type + " : Invalid json" + "\nEx :" + e);
        return obj;
    }
    return obj;
};

/*var callSpreadSheet = function(){	
	var callFAQSuccess=function(data){
		$('#faqSection').html('');
		$.each(data, function(index) {
			var answer;
			if(this.answer){
				answer = this.answer.replace(/""/g,'"').replace(/^"|"$/g,'');
			}
			var question;
			if(this.question){
				question = this.question.replace(/""/g,'"').replace(/^"|"$/g,'');
			}
			dl.append('<dt><span class="icon"></span>'+question+'</dt><dd>'+answer+'</dd>');

			$('<div/>').addClass('panel-faq panel-default-faq')
			.attr({'data-toggle':'collapse','data-parent':'#accordion','href':'#collapsAnswer'+index}).css({"cursor":"pointer"})
			.append($('<div/>').addClass('panel-heading-faq')
			.append($('<div/>').attr({'id':'questionSectionAllign'}).append($('<a/>').attr('href','javascript:void(0)')
					.append($('<i/>').addClass('fa fa-fw fa-plus pull-left'))
					.append($('<span/>').html(question)).addClass('accordion-toggle')
							))
			).append($('<div/>').append($('<h5/>').attr({'class':'collapse answerSectionAllign','id':'collapsAnswer'+index}).html(answer))).appendTo($('#faqSection'));
		});
		$('.panel-faq').click(function(){
			$(this).find("i").toggleClass("fa-plus");
			$(this).find("i").toggleClass("fa-minus");
		});
		
	};
	$( "#searching").click(function() {
		$("#searching").keyup(function(){
			var val = $(this).val().toLowerCase();
			$('.panel-heading-faq').hide();
			$('.panel-heading-faq').each(function(){
				var text=$(this).text().toLowerCase();
				if(text.indexOf(val)!=-1){
					$(this).show();
				}
			});
		});
		
	});
	var request = {
			"app":localStorage["lastusedapp"]
	}
	callAjaxService("FAQReportPost", callFAQSuccess, callBackFailure, request, "POST");
	
};*/


var FAQ = function() {
    var FAQReportPostSuccessCallback = function(data) {
        disableLoading();
        if (data && data.isException) {
            showNotification("error", data.customMessage);
            return;
        }
        if (data.length === 0) {
            showNotification("notification", "No help documentation available yet ");
            return;
        }
        if (data !== undefined &&
            data !== null
        ) {
            $("#accordion,#accordionFAQ").empty();
            if (data && data.length > 0) {
                if (window.location.hash === "#FAQ")
                    $("body").addClass("hidden-menu");
                else {
                    $(".demo").toggleClass("activate").attr("style", "");
                    $(".demo").draggable({ containment: "body" });
                }
                for (var i = 0; i < data.length; i++) {
                    var count = i + 1;
                    $("#accordion,#accordionFAQ").append($("<div/>").addClass("panel panel-default")
                        .append($("<div/>").addClass("panel-heading")
                            .append($("<h4/>").addClass("panel-title")
                                .append($("<a/>").addClass("accordion-toggle collapsed").attr({
                                    "data-toggle": "collapse",
                                    "data-parent": "#accordion",
                                    "href": "#collapse" + count
                                }).html(count + ". " + data[i].question))
                            ))
                        .append($("<div/>").addClass("panel-collapse collapse").attr("id", "collapse" + count)
                            .append($("<div/>").addClass("panel-body").html(data[i].answer))
                        ));
                }
                jQuery.expr[':'].contains = function(a, i, m) {
                    return jQuery(a).text().toUpperCase()
                        .indexOf(m[3].toUpperCase()) >= 0;
                };
                $('#search-faq').keyup(function() {
                    var s = $(this).val().trim().toLowerCase();
                    if (s === '') {
                        $('#accordion .panel').show();
                        return true;
                    }
                    $('#accordion .panel:not(:contains(' + s + '))').hide();
                    $('#accordion .panel:contains(' + s + ')').show();
                    return true;
                });
                $('#search-faq-popup').keyup(function() {
                    var s = $(this).val().trim().toLowerCase();
                    if (s === '') {
                        $('#accordionFAQ .panel').show();
                        return true;
                    }
                    $('#accordionFAQ .panel:not(:contains(' + s + '))').hide();
                    $('#accordionFAQ .panel:contains(' + s + ')').show();
                    return true;
                });
            }
        }

    };


    var init = function() {

        enableLoading();
        var request = {
            "app": localStorage["lastusedapp"]
        };
        callAjaxService("FAQReportPost", FAQReportPostSuccessCallback, callBackFailure,
            request, "POST");
    };
    
    init();
    
};

function liquidFillGaugeDefaultSettings() {
    return {
        minValue: 0, // The gauge minimum value.
        maxValue: 100, // The gauge maximum value.
        circleThickness: 0.05, // The outer circle thickness as a percentage of it's radius.
        circleFillGap: 0.05, // The size of the gap between the outer circle and wave circle as a percentage of the outer circles radius.
        circleColor: "#178BCA", // The color of the outer circle.
        waveHeight: 0.05, // The wave height as a percentage of the radius of the wave circle.
        waveCount: 1, // The number of full waves per width of the wave circle.
        waveRiseTime: 1000, // The amount of time in milliseconds for the wave to rise from 0 to it's final height.
        waveAnimateTime: 18000, // The amount of time in milliseconds for a full wave to enter the wave circle.
        waveRise: true, // Control if the wave should rise from 0 to it's full height, or start at it's full height.
        waveHeightScaling: true, // Controls wave size scaling at low and high fill percentages. When true, wave height reaches it's maximum at 50% fill, and minimum at 0% and 100% fill. This helps to prevent the wave from making the wave circle from appear totally full or empty when near it's minimum or maximum fill.
        waveAnimate: true, // Controls if the wave scrolls or is static.
        waveColor: "#178BCA", // The color of the fill wave.
        waveOffset: 0, // The amount to initially offset the wave. 0 = no offset. 1 = offset of one full wave.
        textVertPosition: .75, // The height at which to display the percentage text withing the wave circle. 0 = bottom, 1 = top.
        textSize: .75, // The relative height of the text to display in the wave circle. 1 = 50%
        valueCountUp: true, // If true, the displayed value counts up from 0 to it's final value upon loading. If false, the final value is displayed.
        displayPercent: true, // If true, a % symbol is displayed after the value.
        textColor: "#045681", // The color of the value text when the wave does not overlap it.
        waveTextColor: "#A4DBf8", // The color of the value text when the wave overlaps it.
        prefix: "",
        suffix: "",
        detailText: "",
        detailTextSize: 0.5,
        detailTextColor: "#045681",
        detailTextVertPosition: .50,
        descText: "",
        descTextSize: 0.35,
        descTextColor: "#FF7777",
        descTextVertPosition: .25,
        textFormat: ".4s",
        fieldType: "string",
        dateFormat: "%m/%d/%Y",
        widgetType: "fillgauge"
    };
}