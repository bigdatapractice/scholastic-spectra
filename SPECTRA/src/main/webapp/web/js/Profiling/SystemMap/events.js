
function node_onMouseOver(d,type) {
    if (type=="CAND") {
        if(d.depth < 2) return;
        toolTip.transition()
            .duration(200)
            .style("opacity", ".9");

        header1.text("ERP");
        header.text(d.receiver);
        header2.text("Doc Count: " + Number(d.Amount));
        toolTip.style("left", (d3.event.pageX-235) + "px")
            .style("top", (d3.event.pageY-200) + "px")
            .style("height","100px");

        highlightLinks(d,true);
    }
    else if (type=="CONTRIBUTION") {

        /*
        Highlight chord stroke
         */
        toolTip.transition()
            .duration(200)
            .style("opacity", ".9");
console.log( d);
        header1.text(pacsById[office + "_" + d.sender].sender);
        header.text(d.receiver);
        if (d.Month != undefined)
        	header2.text(formatCurrency(Number(d.value)) + " on " + d.Month + "/" + d.Day + "/" + d.Year);
        else 
        	header2.text("Doc Count:" + Number(d.value));
        toolTip.style("left", (d3.event.pageX-235) + "px")
            .style("top", (d3.event.pageY-200) + "px")
            .style("height","100px");
        highlightLink(d,true);
    }
    else if (type=="PAC") {
        /*
        highlight all contributions and all candidates
         */
        toolTip.transition()
            .duration(200)
            .style("opacity", ".9");

        header1.text("Sender/Receiver");
        header.text(pacsById[office + "_" + d.label].sender);
        header2.text("Doc Count: " + pacsById[office + "_" + d.label].Amount);
        toolTip.style("left", (d3.event.pageX-235) + "px")
            .style("top", (d3.event.pageY-200) + "px")
            .style("height","110px");
        highlightLinks(chordsById[d.label],true);
    }
}

function node_onMouseOut(d,type) {
    if (type=="CAND") {
        highlightLinks(d,false);
    }
    else if (type=="CONTRIBUTION") {
        highlightLink(d,false);
    }
    else if (type=="PAC") {
        highlightLinks(chordsById[d.label],false);
    }


    toolTip.transition()									// declare the transition properties to fade-out the div
        .duration(500)									// it shall take 500ms
        .style("opacity", "0");							// and go all the way to an opacity of nil

}

function highlightLink(g,on) {

    var opacity=((on==true) ? .6 : .1);

      // console.log("fadeHandler(" + opacity + ")");
      // highlightSvg.style("opacity",opacity);

       var link=d3.select(document.getElementById("l_" + g.Key));
        link.transition((on==true) ? 150:550)
            .style("fill-opacity",opacity)
            .style("stroke-opacity",opacity);

        var arc=d3.select(document.getElementById("a_" + g.Key));
        arc.transition().style("fill-opacity",(on==true) ? opacity :.2);

        var circ=d3.select(document.getElementById("c_" + g.receiver));
        circ.transition((on==true) ? 150:550)
        .style("opacity",((on==true) ?1 :0));

        var text=d3.select(document.getElementById("t_" + g.sender));
         text.transition((on==true) ? 0:550)
             .style("fill",(on==true) ? "#000" : "#777")
             .style("font-size",(on==true) ? "10px" : "8px")
             .style("stroke-width",((on==true) ? 2 : 0));


}

function highlightLinks(d,on) {

    d.relatedLinks.forEach(function (d) {
        highlightLink(d,on);
    })

}


senateButton.on("click",function (d) {

    senateButton.attr("class","selected");
    houseButton.attr("class",null);
    office="senate";
    linksSvg.selectAll("g.links").remove();
    clearInterval(intervalId);
    main();

});

houseButton.on("click",function (d) {
//    linkGroup.selectAll("g.links").remove();
    senateButton.attr("class",null);
    houseButton.attr("class","selected");
    office="house";
//    linksSvg.selectAll("g.links").remove();
    clearInterval(intervalId);
    main();
});