pageSetUp();
// # dc.js Getting Started and How-To Guide
'use strict';
var systemMapProfiling;
var systemMapProfilingInstance = function(){
	systemMapProfiling = new systemMapProfilingObject();
	systemMapProfiling.Load();
};

var systemMapProfilingObject = function() {
	var THIS = this;
	THIS.source = "SAP";
	var numberFormat = d3.format(",f");
	constructTabs(THIS);
	this.Load = function(obj){
		enableLoading();
		var source = $(obj).data("source");
		THIS.source = source || $("#myTab").find("li.active").data("source");
		var request = {
				"source" : THIS.source,
				"viewType" : "default"
			};
			callAjaxService("systemmap", callBackSuccessSystemMap, callBackFailure, request, "POST");
	};
	var dataSource = $("#myTab").find("li.active").data("source");
	var callBackSuccessSystemMap = function(data) {
		if (data && data.isException){
			showNotification("error",data.customMessage);
			return;
		}
		if (data != undefined && data != null && data["aggregatedSystemMapList"] != undefined && data["aggregatedSystemMapList"] != null)
		THIS.pieChartDirect = dc.pieChart("#pie-chart-direct"); // inbound/outbound
		THIS.pieChartStatus = dc.pieChart("#pie-chart-status"); // status
		THIS.pieChartMessage = dc.pieChart("#pie-chart-Message"); // message
		THIS.rhorzBarChart = dc.rowChart("#receiver-row-chart"); // receiver
		THIS.shorzBarChart = dc.rowChart("#sender-row-chart"); // sender
		THIS.timelineAreaChart = dc.lineChart("#timeline-area-chart");
		THIS.timelineChart = dc.barChart("#timeline-chart");
		data = data["aggregatedSystemMapList"];
		 /* Don't delete mapping
		  private String a; //createdOn
		 private String b; //messageType
		 private String c; //messageTypeName
		 private String d; //basicType
		 private String e; //idocStatus
		 private String f; //idocStatusName
		 private String g; //direction
		 private String h; //receiverPort
		 private String i; //receiverPartnType
		 private String j; //receiverPartnerNo
		 private String k; //receiver
		 private String l; //senderPort 
		 private String m; //senderPartnerType
		 private String n; //senderPartnerNo
		 private String o; //sender
		 private String p; //docNumberCount
*/		var dateFormat = d3.timeFormat("%m/%Y");
		$('#loading').hide();

		data.forEach(function(d) {
			d.dd = dateFormat.parse(d.a);
			d.month = d3.time.month(d.dd); // pre-calculate month for better
			d.p = +d.p;
		});
		var ndx = crossfilter(data);

		var timelineDimension = ndx.dimension(function(d) {
			return d.month;
		});
		// group by total volume within move, and scale down result
		var timelineGroup = timelineDimension.group().reduceSum(function(d) {
			return d.p;
		});

		var timelineAreaGroup = timelineDimension.group().reduce(
				function(p, v) {
					++p.count;
					p.total += v.p; // (v.open + v.close) / 2;
					return p;
				}, function(p, v) {
					--p.count;
					p.total -= v.p; // (v.open + v.close) / 2;
					return p;
				}, function() {
					return {
						count : 0,
						total : 0,
					};
				});

		// summerize volume by quarter

		// Direct
		var pieChartDirectGroupDimention = ndx.dimension(function(d) {
			if (d.g == 1)
				return "Outbound";
			else {
				return "Inbound";
			}

			return d.g;
		});

		// Status
		var pieChartStatusDimention = ndx.dimension(function(d) {
			if (d.f == undefined)
				d.f  = " ";
				return d.e + "-"+ d.f;

		});

		// Message type
		var pieChartMessageDimention = ndx.dimension(function(d) {
			if (d.c == undefined)
				d.c  = " ";
			return d.b + "-"+d.c;
		});

		// Direct group
		var pieDirectGroup = pieChartDirectGroupDimention.group().reduceSum(
				function(d) {
					return d.p;
				});

		// status group
		var pieStatusGroup = pieChartStatusDimention.group().reduceSum(
				function(d) {
					return d.p;
				});

		// message type group
		var pieMessageGroup = pieChartMessageDimention.group().reduceSum(
				function(d) {
					return d.p;
				});

		// counts per weekday
		var rhorzBarDimension = ndx.dimension(function(d) {
			if (d.i == "KU")
				return "Customers";
			else if(d.i == 'LI')
				return "Vendors";
			else
				return d.k;
		});

		var shorzBarDimension = ndx.dimension(function(d) {
			if (d.m == "KU")
				return "Customers";
			else if(d.m == 'LI')
				return "Vendors";
			else
				return d.o;
		});

		var rhorzBarGrp = rhorzBarDimension.group().reduceSum(
				function(d) {
					return d.p;
				});
		var shorzBarGrp = shorzBarDimension.group().reduceSum(
				function(d) {
					return d.p;
				});

		var colorScale = d3.scaleOrdinal().range([ '#000' ]);

		THIS.pieChartDirect.width(160)// Inboud/Outbound
		.height(210).radius(70).innerRadius(30).dimension(
				pieChartDirectGroupDimention).group(pieDirectGroup).on('filtered', function(chart) {
					THIS.refreshTable(pieChartDirectGroupDimention);
				}).title(function(d) {
					return d.key + ': ' + numberFormat(d.value);
				});

		// .label(function (d) {
		// if (pieChartDirect.hasFilter() && !pieChartDirect.hasFilter(d.key))
		// return d.key + "(0%)";
		// return d.key + "(" + Math.floor(d.value / all.value() * 100) + "%)";
		// });

		THIS.pieChartStatus.width(160)// status
		.height(210).radius(70).dimension(pieChartStatusDimention).group(
				pieStatusGroup).label(function(d) {
					var statusGroup = d.key.split("-");
					return statusGroup[0];
				}).on('filtered', function(chart) {
					THIS.refreshTable(pieChartStatusDimention);
				}).title(function(d) {
					return d.key + ': ' + numberFormat(d.value);
				});

		THIS.pieChartMessage.width(160)// Message type
		.height(210).radius(70).innerRadius(30).dimension(
				pieChartMessageDimention).group(pieMessageGroup).label(function(d) {
					var messageGroup = d.key.split("-");
					return messageGroup[0];
				}).on('filtered', function(chart) {
					THIS.refreshTable(pieChartMessageDimention);
				}).title(function(d) {
					return d.key + ': ' + numberFormat(d.value);
				});

		// #### sRow Chart
		THIS.rhorzBarChart.width(240).height(460).margins({
			top : 20,
			left : 2,
			right : 10,
			bottom : 20
		}).group(rhorzBarGrp).dimension(rhorzBarDimension)
		// assign colors to each value in the x scale domain
		.ordinalColors(
				[ '#3182bd', '#6baed6', '#9e5ae1', '#c64bef', '#da8aab' ])
				.label(function(d) {

					return d.key; // .split(".")[1];
				}).on('filtered', function(chart) {
					THIS.refreshTable(rhorzBarDimension);
				}).title(function(d) {
					return d.key + ': ' + numberFormat(d.value);
				});

		// #### rRow Chart
		THIS.shorzBarChart.width(200).height(460).margins({
			top : 20,
			left : 10,
			right : 10,
			bottom : 20
		}).group(shorzBarGrp).dimension(shorzBarDimension)
		// assign colors to each value in the x scale domain
		.ordinalColors(
				[ '#3182bd', '#6baed6', '#9e5ae1', '#c64bef', '#da8aab' ])
				.label(function(d) {

					return d.key; // .split(".")[1];
				}).on('filtered', function(chart) {
					THIS.refreshTable(shorzBarDimension);
				})

				// title sets the row text
				.title(function(d) {
					return d.key + ': ' + numberFormat(d.value); // d.value;
				}).elasticX(true).xAxis().ticks(4);

		THIS.timelineAreaChart.renderArea(true).width($('.getWidth').width()).height(130)
				.transitionDuration(1000).margins({
					top : 30,
					right : 70,
					bottom : 25,
					left : 80
				}).dimension(timelineDimension).mouseZoomable(true).on('filtered', function(chart) {
					THIS.refreshTable(timelineDimension);
				})
				// Specify a range chart to link the brush extent of the range
				// with the zoom focue of the current chart.
				.rangeChart(THIS.timelineChart).x(
						d3.time.scale()
								.domain(
										[ new Date(1999, 0, 1),
												new Date(2015, 11, 31) ]))
				.round(d3.time.month.round).xUnits(d3.time.months).elasticY(
						true).renderHorizontalGridLines(true)
				// .legend(dc.legend().x(800).y(10).itemHeight(13).gap(5))
				.brushOn(false)
				// Add the base layer of the stack with group. The second
				// parameter specifies a series name for use in the legend
				// The `.valueAccessor` will be used for the base layer
				.group(timelineAreaGroup).valueAccessor(function(d) {
					return d.value.total;
				})
				// stack additional layers with `.stack`. The first paramenter
				// is a new group.
				// The second parameter is the series name. The third is a value
				// accessor.
				//.stack(timelineSecondGroup, function(d) {
				//	return d.value;
				//})
				// title can be called by any stack layer.
				.title(function(d) {
					var value = d.value.total ? d.value.total : d.value;
					if (isNaN(value))
						value = 0;
					return dateFormat(d.key) + "\n" + numberFormat(value);
				});

		THIS.timelineChart.width($('.getWidth').width()).height(80).margins({
			top : 40,
			right : 70,
			bottom : 20,
			left : 80
		}).dimension(timelineDimension).group(timelineGroup).centerBar(true).gap(1).x(
						d3.time.scale()
								.domain(
										[ new Date(1999, 0, 1),
												new Date(2014, 11, 31) ]))
				.round(d3.time.month.round).alwaysUseRounding(true).xUnits(
						d3.time.months);
//		dc.dataCount(".dc-data-count").dimension(ndx).group(all);
		var dataTableData = data.splice(0, 100);
		THIS.data = data;
		THIS.dataTable = $(".dc-data-table")
				.DataTable(
						{
							"dom":"<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-3 col-xs-12 hidden-xs'l><'col-sm-3 col-xs-12 hidden-xs'C>r>"+
							"t"+
							"<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
							"processing" : true,
							"lengthChange" : true,
							"sort" : true,
							"info" : true,
							"jQueryUI" : false,
							"scrollX" : true,
							"data" : dataTableData,
							"tableTools" : {
								"aButtons" : [ {
									"sExtends" : "collection",
									"sButtonText" : 'Export <span class="caret" />',
									"aButtons" : [ "csv", "pdf" ]
								} ],
								"sSwfPath" : "js/plugin/datatables/media/swf/copy_csv_xls_pdf.swf"
							},
							"destroy" : true,
							"columns" : [
									{
										"data" : null,
										"defaultContent" : '<img src="./img/details_open.png">'
									},
									{
										"data" : "p",
										"defaultContent" : "",
									},
									{
										"data" : "b",
										"defaultContent" : "",
									},
									{
										"data" : "c",
										"defaultContent" : "",
									},
									{
										"defaultContent" : "",
										"data" : "e"
									},
									{
										"data" : "f",
										"defaultContent" : "",
									},
									{
										"data" : "g",
										"defaultContent" : "",
									},
									{
										"data" : "k",
										"defaultContent" : "",
									},
									{
										"data" : "j",
										"defaultContent" : "",
									},
									{
										"data" : "i",
										"defaultContent" : "",
									},
									{
										"data" : "o",
										"defaultContent" : "",
									},
									{
										"data" : "n",
										"defaultContent" : "",
									},
									{
										"data" : "m",
										"defaultContent" : "",
									},
									{
										"data" : "a",
										"defaultContent" : "",
										"class" : "numbercolumn"
									} ],
							"rowCallback" : function(nRow, aData,
									iDisplayIndex, iDisplayIndexFull) {
								var request = {
									"viewType" : "level2",
									"source" : THIS.source || "null",
									"credat" : aData.a || "null",
									"mestyp" : aData.b || "null",
									"status" : aData.e || "null",
									"direction" : aData.g || "null",
									"rcvprt" : aData.i || "null",
									"receiver" : aData.k || "null",
									"sender" : aData.o || "null",
									"receiverType" : aData.i || "null",
									"senderType" : aData.m || "null",
									"receiverPartner" : aData.j || "null",
									"senderPartner" : aData.n || "null",
								};
								/*
								 * private String soCount; a private String
								 * companyCode; b private String
								 * companyName; c private String salesOrg; d
								 * private String salesOrgName; e private
								 * String salesGroup; f private String
								 * salesGroupName; g private String
								 * division; h private String divisionName;
								 * i private String salesDocType; j private
								 * String plantGroup; k private String
								 * plantName; l private String netValue; m
								 * private String source; n private String
								 * region; o private String lastChanged; p
								 * private String materialType; q private
								 * String soType; r
								 */
								$(nRow).attr({
									"id" : "row" + iDisplayIndex
								}).unbind('click').bind('click', request,
										systemMapProfiling.callLevel2systemmap);
							}
						});

		dc.renderAll();
		/*
		 * // or you can render charts belong to a specific chart group
		 * dc.renderAll("group"); // once rendered you can call redrawAll to
		 * update charts incrementally when data // change without re-rendering
		 * everything dc.redrawAll(); // or you can choose to redraw only those
		 * charts associated with a specific chart group dc.redrawAll("group");
		 */

	};
	this.callLevel2systemmap = function(request) {
		var tr = $(this).closest('tr');
		
		THIS.childId = "child" + $(tr).attr("id");
		if ($(tr).find('td:eq(0)').hasClass("expandRow")) {
			$($(tr).find('td:eq(0)').find('img')).attr('src',
					'./img/details_open.png');
			$("#" + THIS.childId).hide(50);
		} else {

			$($(tr).find('td:eq(0)').find('img')).attr('src',
					'./img/details_close.png');
			if (!$(tr).next().hasClass("childRowtable")) {
				$(tr).after($("<tr/>").addClass("childRowtable").attr({
					"id" : THIS.childId
				}).append($("<td/>")).append($("<td/>").attr({
					"colspan" : "16",
					"id" : THIS.childId + "td"
				}).addClass("carousel")));

				enableLoading(THIS.childId + "td", "50%", "45%");
				request = request.data;
				request.source = THIS.source;
				callAjaxService("systemmap", function(response){callbackSucessLevel2systemmap(response,request);},
						callBackFailure, request, "POST");
			} else {
				$("#" + THIS.childId).show(50);
			}
		}
		$(tr).find('td:eq(0)').toggleClass("expandRow");
	};
	var callbackSucessLevel2systemmap = function(response,request) {
		disableLoading(THIS.childId + "td");
		if (response && response.isException){
			showNotification("error",response.customMessage);
			return;
		}
		var childTabledata = response["level2SystemMapList"];
		$("#" + THIS.childId + "td")
				.show()
				.html("")
				.append(
						$("<table/>")
								.addClass("table table-striped table-hover dc-data-table")
								.attr({
									"id" : THIS.childId + "Table"
								})
								.append(
										$("<thead/>")
												.append(
														$("<tr/>")
														.append(
																		$(
																				"<th/>")
																				.html(
																						"Doc Number"))
																.append(
																		$(
																				"<th/>")
																				.html(
																						"Source"))
																.append(
																		$(
																				"<th/>")
																				.html(
																						"Message Type"))
																.append(
																		$(
																				"<th/>")
																				.html(
																						"status"))
																.append(
																		$(
																				"<th/>")
																				.html(
																						"direction"))
																.append(
																		$(
																				"<th/>")
																				.html(
																						"Receiver Port"))
																						.append(
																		$(
																				"<th/>")
																				.html(
																						"Receiver"))
																						.append(
																		$(
																				"<th/>")
																				.html(
																						"Sender"))
																.append(
																		$(
																				"<th/>")
																				.html(
																						"Created On")))));

		$("#" + THIS.childId + "Table")
				.DataTable(
						{
							"dom":
								"t"+
								"<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
							"processing" : true,
							"lengthChange" : true,
							// "bServerSide": true,
							"info" : true,
							"filter" : false,
							"jQueryUI" : false,
							"data" : childTabledata,
							"scrollX" : true,
							"tableTools" : {
								"aButtons" : [ {
									"sExtends" : "collection",
									"sButtonText" : 'Export <span class="caret" />',
									"aButtons" : [ "csv", "pdf" ]
								} ],
								"sSwfPath" : "js/plugin/datatables/media/swf/copy_csv_xls_pdf.swf"
							},
							"columns" : [{
								"data" : "docnumber",
								"defaultContent" : ""
							}, {
								"data" : "source",
								"defaultContent" : ""
							}, {
								"data" : "mesType",
								"defaultContent" : ""
							}, {
								"data" : "status",
								"defaultContent" : ""
							}, {
								"data" : "direction",
								"defaultContent" : ""
							}, {
								"data" : "rcvprt",
								"defaultContent" : ""
							}, {
								"data" : "receiver",
								"defaultContent" : ""
							},
							 {
								"data" : "sender",
								"defaultContent" : ""
							},
							{
								"data" : "createddate",
								"defaultContent" : ""
							} ],
							"rowCallback" : function(nRow, aData,
									iDisplayIndex, iDisplayIndexFull) {
								$(nRow)
										.attr(
												{
													'onClick' : "javascript:systemMapProfiling.thirdLevelDrillDownsystemMapProfiling('e2e_"+THIS.source+"_l3_sm', '"
															+ aData.docnumber + "','100','"+dataSource+"')",
													'data-target' : '#myModalthredlevel',
													'data-toggle' : 'modal'
												});
							}
						});
	};
	this.thirdLevelDrillDownsystemMapProfiling = function(entitytype, criteria) {
		$("#poDocView").empty();
		enableLoading("poDocView");
		var CallbackSuccessLevel3Details = function(l3Data) {
			if (l3Data && l3Data.isException){
				showNotification("error",l3Data.customMessage);
				return;
			}
			if (l3Data && l3Data[0]) {
				l3Data = l3Data[0];
				var keys = Object.keys(l3Data);

				var onAjaxLoadSuccess = function(html) {
					if (html && html.isException){
						showNotification("error",html.customMessage);
						return;
					}
					// console.log(html);
					$('#myModalthredlevel #myModalLabel').text(entitytype);
					$("#poDocView").html(html);
					$("#entity").text(entitytype);
					$("#criteria").text(criteria);
					/*
					 * keys.forEach(function(key){ console.log(l3Data[key]);
					 * $("#tbDynamic tbody").append($("<tr>\n").append("<th>"+key+"</th>\n").append("<td>"+l3Data[key]+"</td>\n")); })
					 */
					// $("<tr/>").append($("<td/>").html(i+1)).append($("<td/>").html("po[i].ekpo_matnr"))
					var i = 0, idx = keys.length, lvalue, lkey, rvalue, rkey;
					while (i < idx) {
						lvalue = l3Data[keys[i]] !== undefined
								&& l3Data[keys[i]] !== null ? l3Data[keys[i]]
								: '';
						while (lvalue == '' && i < idx) {
							i++;
							lvalue = l3Data[keys[i]] !== undefined
									&& l3Data[keys[i]] !== null ? l3Data[keys[i]]
									: '';
						}
						lkey = keys[i] !== undefined && keys[i] !== null ? keys[i]
								: '';
						i++;
						rvalue = l3Data[keys[i]] !== undefined
								&& l3Data[keys[i]] !== null ? l3Data[keys[i]]
								: '';
						while (rvalue == '' && i < idx) {
							i++;
							rvalue = l3Data[keys[i]] !== undefined
									&& l3Data[keys[i]] !== null ? l3Data[keys[i]]
									: '';
						}
						rkey = keys[i] !== undefined && keys[i] !== null ? keys[i]
								: '';
						if (rvalue == '') {
							rkey = '';
						}
						if (rvalue != '' || lvalue != '') {
							$("#tbDynamic tbody")
									.append(
											$("<tr>\n")
													.append(
															"<th>" + lkey
																	+ "</th>\n")
													.append(
															"<td>" + lvalue
																	+ "</td>\n")
													.append(
															"<th>" + rkey
																	+ "</th>\n")
													.append(
															"<td>" + rvalue
																	+ "</td>\n"));
						}

						i++;
					}
					// for(var i=0, idx= keys.length; i<idx; i++){
					// var lkey = keys[i]!==undefined && keys[i] !==
					// null?keys[i]:'';
					// var lvalue = l3Data[keys[i]] !== undefined &&
					// l3Data[keys[i]] !== null? l3Data[keys[i]] :'';
					// var rkey = keys[i+1]!==undefined && keys[i+1] !==
					// null?keys[i+1]:'';
					// var rvalue = l3Data[keys[i+1]] !== undefined &&
					// l3Data[keys[i+1]] !== null? l3Data[keys[i+1]] :'';
					// if(lvalue !== '' || rvalue != ''){
					// $("#tbDynamic tbody").append($("<tr>\n")
					// .append("<th>"+lkey+"</th>\n")
					// .append("<td>"+lvalue+"</td>\n")
					// .append("<th>"+rkey+"</th>\n")
					// .append("<td>"+rvalue+"</td>\n"));
					// }
					// }
			        $("#btnPrintL3").click(function(e){
			        	printElement('#poDocView');
			        });
			        $("#btnSaveL3").click(function(e){
			            savePDF('#poDocView');
			        });
			        
				};
				callAjaxService("SearchL3View", onAjaxLoadSuccess, function() {
					alert("error");
				}, null, "GET", "html");
			} else {
				$("#poDocView").html('<div class="alert alert-danger fade in"><a class="close" data-dismiss="alert" href="#">×</a><h4 class="alert-heading"><i class="fa fa-exclamation-triangle"></i>&nbsp;OOPS!</h4><p class="text-align-left">No matching results found.</p></div>');;
			}
		};
		var request = {
			"entitytype" : System.dbName+"."+entitytype,
			"criteria" : "docnumber:" + criteria
		};
		callAjaxService("Level3Details", CallbackSuccessLevel3Details,
				callBackFailure, request, "POST");
	};
	this.refreshTable = function(dim) {

		if (THIS.dataTable !== null && dim !== null) {
			THIS.dataTable.clear();
			THIS.dataTable.rows.add(dim.top(1000));
			THIS.dataTable.columns.adjust().draw();
		} else
			console
					.log('[While Refreshing Data Table] This should never happen..');
	};

	// #### Version
	// Determine the current version of dc with `dc.version`
	d3.selectAll("#version").text(dc.version);
};

systemMapProfilingInstance();
