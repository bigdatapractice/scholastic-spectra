'use strict';
pageSetUp();
var poProfiling;
var poProfileInstance = function() {
	poProfiling = new poProfilingObject();
	poProfiling.Load();
};

var poProfilingObject = function() {
	var THIS = this;
	constructTabs(THIS);
	var dateFormat = d3.timeFormat("%m/%d/%Y");
	//var numberFormat = d3.format(".f");
	var numberFormat = d3.format(",f");
	THIS.type = "materialCount";
	this.Load = function(obj) {
		var source = $(obj).data("source");
		//console.log(source);
		source = source || $("#myTab").find("li.active").data("source");
		enableLoading();
		//source = source || "SAP";
		var request = {
			"source" : source,
			"viewType" : "default"
		};
		callAjaxService("poextractanalysis", callbackSuccessPOProfile, callBackFailure,
				request, "POST");
	};
	
	this.loadViewType = function(viewType){
		enableLoading();
		THIS.type = viewType;
		setTimeout(function() { drawChart();disableLoading(); }, 50);
	};

	var callbackSuccessPOProfile = function(data) {
		if (data && data.isException){
			showNotification("error",data.customMessage);
			return;
		}
		if (data != undefined && data != null
				&& data["aggregatedPOExtractionList"] != undefined
				&& data["aggregatedPOExtractionList"] != null && data["aggregatedPOExtractionList"].length > 0) {
	
			THIS.data = data["aggregatedPOExtractionList"];
			drawChart();
		}
		disableLoading();
		
	};
	var drawChart = function(){
		var data = THIS.data;
			$("[id^=pie-chart] h4").addClass("hide");
			/*
			 * private String poCount; a private String companyCode; b private
			 * String companyName; c private String purchaseOrg; d private
			 * String purchaseOrgName; e private String purchaseGroup; f private
			 * String purchaseGroupName; g private String purchasePlantGroup; h
			 * private String plantName; i private String orderQuantity; j
			 * private String netPrice; k private String grossOrderValue; l
			 * private String source; m private String lastChanged; n private
			 * String materialType; o private String vendorType; p private
			 * String poType; q
			 */
			data.forEach(function(d) {
				d.dd = dateFormat.parse(d.orderDate);

				d.month = d3.time.month(d.dd);
				
				//d.l = +d.l;
				d.zz = (THIS.type == "netvalue") ?  isNaN(d.netValue) ? 0 : d.netValue : isNaN(d.poCount) ? 0 : d.poCount;
				d.netValue = +d.netValue;
			});

			var ndx = crossfilter(data);
			var all = ndx.groupAll();

			THIS.pieChartMaterialType = dc.pieChart("#pie-chart");
			var pieMaterialTypeDimension = ndx.dimension(function(d) {
				return d.materialType || "Others";
			});

			var pieMaterialType = pieMaterialTypeDimension.group();
			if (pieMaterialType.size() > 1
					|| pieMaterialType.all()[0].key.split("-")[0] != "Others") {
				pieMaterialType = pieMaterialType.reduceSum(
						function(d) {
							return d.zz;
						});
			} else {
				$("#pie-chart h4").removeClass("hide");
				pieMaterialType = pieMaterialType
				.reduceSum(function() {
					return 0;
				});
			}
			
			THIS.pieChartVendor = dc.pieChart("#pie-chart-Vendor");
			var pieChartVendorGroupDimention = ndx.dimension(function(d) {
				return (d.relatedOrderType || "Others")+ "-" + (d.relatedOrderTypeDesc || "Not Available");
			});
			var pieVendorGroup = pieChartVendorGroupDimention.group();
			if (pieVendorGroup.size() > 1
					|| pieVendorGroup.all()[0].key.split("-")[0] != "Others") {
				pieVendorGroup = pieVendorGroup
					.reduceSum(function(d) {						
						return d.zz;
					});
			} else {
				$("#pie-chart-Vendor h4").removeClass("hide");
				pieVendorGroup = pieVendorGroup
				.reduceSum(function() {
					return 0;
				});
			}
			
			THIS.pieChartCompanyCode = dc.pieChart("#pie-chart-Companycode");
			var pieChartCompanyGroupDimention = ndx.dimension(function(d) {
				return (d.buyingCompany || "Others") + "-" + (d.buyingCompanyName || "Not Available");
			});
			var pieCompanyGroup = pieChartCompanyGroupDimention.group();
			if (pieCompanyGroup.size() > 1
					|| pieCompanyGroup.all()[0].key.split("-")[0] != "Others") {
				pieCompanyGroup = pieCompanyGroup
					.reduceSum(function(d) {
						return d.zz;
					});
			} else {
				$("#pie-chart-Companycode h4").removeClass("hide");
				pieCompanyGroup = pieCompanyGroup
				.reduceSum(function() {
					return 0;
				});
			}
			
			THIS.pieChartPurchaseGrp = dc.pieChart("#pie-chart-prchasegroup");
			var pieChartPurchaseGroupDimention = ndx.dimension(function(d) {
				return (d.orderType || "Others") + "-" + d.orderTypeDesc; 
			});
			var piePurchaseGroup = pieChartPurchaseGroupDimention.group();
			if (piePurchaseGroup.size() > 1
					|| piePurchaseGroup.all()[0].key.split("-")[0] != "Others") {
				piePurchaseGroup = piePurchaseGroup
						.reduceSum(function(d) {
							return d.zz;
						});
			} else {
				$("#pie-chart-prchasegroup h4").removeClass("hide");
				piePurchaseGroup = piePurchaseGroup
				.reduceSum(function() {
					return 0;
				});
			}
			
			THIS.pieChartPlant = dc.pieChart("#pie-chart-plant");
			var pieChartPlantDimention = ndx.dimension(function(d) {
				return (d.plant || "Others") + "-" + d.plantName;
			});
			var piePlantGroup = pieChartPlantDimention.group();
			if (piePlantGroup.size() > 1
					|| piePlantGroup.all()[0].key.split("-")[0] != "Others") {
				piePlantGroup = piePlantGroup.reduceSum(
					function(d) {
						return d.zz;
					});
			}else {
				$("#pie-chart-plant h4").removeClass("hide");
				piePlantGroup = piePlantGroup
				.reduceSum(function() {
					return 0;
				});
			}
			
			THIS.pieChartPurchaseorg = dc.pieChart("#pie-chart-purchaseorg");
			var pieChartPurchaseorgDimention = ndx.dimension(function(d) {
				return (d.orderingCompany || "Others") + "-" + (d.orderingCompanyName || "Not Available");
			});
			var piePurchaseOrgGroup = pieChartPurchaseorgDimention.group();
			if (piePurchaseOrgGroup.size() > 1
					|| piePurchaseOrgGroup.all()[0].key.split("-")[0] != "Others") {
				piePurchaseOrgGroup = piePurchaseOrgGroup
					.reduceSum(function(d) {
						return d.zz;
					});
			} else {
				$("#pie-chart-purchaseorg h4").removeClass("hide");
				piePurchaseOrgGroup = piePurchaseOrgGroup
				.reduceSum(function() {
					return 0;
				});
			}
			
			
			THIS.pieChartAddressType = dc.pieChart("#pie-chart-addresstype");
			var pieChartAddressTypeDimention = ndx.dimension(function(d) {
				return (d.addressType || "Others") + "-" + (d.addressTypeDesc || "Not Available");
			});
			var pieChartAddressTypeGroup = pieChartAddressTypeDimention.group();
			if (pieChartAddressTypeGroup.size() > 1
					|| pieChartAddressTypeGroup.all()[0].key.split("-")[0] != "Others") {
				pieChartAddressTypeGroup = pieChartAddressTypeGroup
					.reduceSum(function(d) {
						return d.zz;
					});
			} else {
				$("#pie-chart-addresstype h4").removeClass("hide");
				pieChartAddressTypeGroup = pieChartAddressTypeGroup
				.reduceSum(function() {
					return 0;
				});
			}
			

			THIS.horzBarChart = dc.rowChart("#row-chart");
			THIS.timelineAreaChart = dc.lineChart("#timeline-area-chart");
			THIS.timelineChart = dc.barChart("#timeline-chart");
			var timelineDimension = ndx.dimension(function(d) {
				return d.month;
			});
			var timelineGroup = timelineDimension.group().reduceSum(
					function(d) {
						return d.zz;
					});
			var timelineAreaGroup = timelineDimension.group().reduce(
					function(p, v) {
						++p.count;
						v.netValue = v.netValue || 0;
						p.total += v.netValue;// (v.open + v.close) / 2;
						// p.avg = Math.round(p.total / p.count);
						return p;
					}, function(p, v) {
						--p.count;
						v.netValue = v.netValue || 0;
						p.total -= v.netValue;// (v.open + v.close) / 2;
						// p.avg = p.count ? Math.round(p.total / p.count) : 0;
						return p;
					}, function() {
						return {
							count : 0,
							total : 0,
						};
					});

			// counts per weekday
			var horzBarDimension = ndx.dimension(function(d) {
				return d.poType || "Others";
			});
			var horzBarGrp = horzBarDimension.group().reduceSum(function(d) {
				return d.zz;
			});

			THIS.pieChartMaterialType.width(170).height(150).radius(60)
			// .innerRadius(30)
			.dimension(pieMaterialTypeDimension).group(pieMaterialType).on(
					'filtered', function(chart) {
						THIS.refreshTable(pieMaterialTypeDimension);
					}).title(function(d) {
						return THIS.type == "netvalue" ?
								 d.key + ': $' + numberFormat(d.value)
							:
								 d.key + ': ' + numberFormat(d.value);
					});

			THIS.pieChartVendor.width(170).height(150).radius(60).innerRadius(
					30).dimension(pieChartVendorGroupDimention).group(
					pieVendorGroup).label(function(d) {
						var b = d.key.split("-");
						return b[0];
					})
				.on('filtered', function(chart) {
				THIS.refreshTable(pieChartVendorGroupDimention);
			}).title(function(d) {
				return THIS.type == "netvalue" ?
					 d.key + ': $' + numberFormat(d.value)
				:
					 d.key + ': ' + numberFormat(d.value);
			});

			THIS.pieChartCompanyCode.width(250).height(200).radius(80)
			//.colors(colorScale)
			// .innerRadius(30)
			.dimension(pieChartCompanyGroupDimention).group(pieCompanyGroup)
					.label(function(d) {
						var b = d.key.split("-");
						return b[0];
					}).on('filtered', function(chart) {
						THIS.refreshTable(pieChartCompanyGroupDimention);
					}).title(function(d) {
						return THIS.type == "netvalue" ?
								 d.key + ': $' + numberFormat(d.value)
							:
								 d.key + ': ' + numberFormat(d.value);
					});

			THIS.pieChartPurchaseGrp.width(170).height(150).radius(60)
					.innerRadius(30).dimension(pieChartPurchaseGroupDimention)
					.group(piePurchaseGroup).label(function(d) {
						var purchaseGrp = d.key.split("-");
						return purchaseGrp[0];
					}).on('filtered', function(chart) {
						THIS.refreshTable(pieChartPurchaseGroupDimention);
					}).title(function(d) {
						return THIS.type == "netvalue" ?
								 d.key + ': $' + numberFormat(d.value)
							:
								 d.key + ': ' + numberFormat(d.value);
					});

			THIS.pieChartPlant.width(250).height(200).radius(80)
			// .innerRadius(30)
			.dimension(pieChartPlantDimention).group(piePlantGroup).label(
					function(d) {
						var h = d.key.split("-");
						return h[0];
					}).on('filtered', function(chart) {
				THIS.refreshTable(pieChartPlantDimention);
			}).title(function(d) {
				return THIS.type == "netvalue" ?
						 d.key + ': $' + numberFormat(d.value)
					:
						 d.key + ': ' + numberFormat(d.value);
			});

			THIS.pieChartPurchaseorg.width(250).height(200).radius(80)
					.innerRadius(40).dimension(pieChartPurchaseorgDimention)
					.group(piePurchaseOrgGroup).label(function(d) {
						var z = d.key.split("-");
						return z[0];
					}).on('filtered', function(chart) {
						THIS.refreshTable(pieChartPurchaseorgDimention);
					}).title(function(d) {
						return THIS.type == "netvalue" ?
								 d.key + ': $' + numberFormat(d.value)
							:
								 d.key + ': ' + numberFormat(d.value);
					});
			THIS.pieChartAddressType.width(170).height(150).radius(60)
			.innerRadius(30).dimension(pieChartAddressTypeDimention)
			.group(pieChartAddressTypeGroup).label(function(d) {
				var z = d.key.split("-");
				return z[0];
			}).on('filtered', function(chart) {
				THIS.refreshTable(pieChartAddressTypeDimention);
			}).title(function(d) {
				return THIS.type == "netvalue" ?
						 d.key + ': $' + numberFormat(d.value)
					:
						 d.key + ': ' + numberFormat(d.value);
			});
			// #### Row Chart
			THIS.horzBarChart.width(180).height(515).margins({
				top : 20,
				left : 10,
				right : 10,
				bottom : 20
			}).group(horzBarGrp).dimension(horzBarDimension).on('filtered',
					function(chart) {
						THIS.refreshTable(horzBarDimension);
					})
			// assign colors to each value in the x scale domain
			.ordinalColors(
					[ '#3182bd', '#6baed6', '#9e5ae1', '#c64bef', '#da8aab' ])
					.label(function(d) {

						return d.key;// .split(".")[1];
					})
					// title sets the row text
					.title(function(d) {
						if (isNaN(d.value))
							d.value = 0;
						return THIS.type == "netvalue" ?
								 d.key + ': $' + numberFormat(d.value)
							:
								 d.key + ': ' + numberFormat(d.value);
			}).elasticX(true).xAxis().ticks(4);

			THIS.timelineAreaChart.renderArea(true).width($('#content').width()).height(150)
					.transitionDuration(1000).margins({
						top : 30,
						right : 70,
						bottom : 25,
						left : 80
					}).dimension(timelineDimension).mouseZoomable(true).on(
							'filtered', function(chart) {
								THIS.refreshTable(timelineDimension);
							})
					// Specify a range chart to link the brush extent of the
					// range
					// with
					// the zoom focue of the current chart.
					.rangeChart(THIS.timelineChart).x(
							d3.time.scale().domain(
									[ new Date(2011, 0, 1),
											new Date(2015, 11, 31) ])).round(
							d3.time.month.round).xUnits(d3.time.months)
					.elasticY(true).renderHorizontalGridLines(true)
					// .legend(dc.legend().x(800).y(10).itemHeight(13).gap(5))
					.brushOn(false).group(timelineAreaGroup, "Net Value")
					.valueAccessor(function(d) {
						return d.value.total;
					}).renderHorizontalGridLines(true)
					// .legend(dc.legend().x(800).y(10).itemHeight(13).gap(5))
					.brushOn(false).group(timelineAreaGroup, "Net Value")
					.valueAccessor(function(d) {
						return d.value.total;
					})// .stack(timelineSecondGroup, "Material Count",
					// function(d) {
					// return d.value;
					// })
					// title can be called by any stack layer.
					.title(function(d) {
						var value = d.value.total;
						
						if (isNaN(value))
							value = 0;
						return dateFormat(d.key) + "\n$" + numberFormat(value);
					});

			THIS.timelineChart.width($('#content').width()).height(80).margins({
				top : 40,
				right : 70,
				bottom : 20,
				left : 80
			}).dimension(timelineDimension).group(timelineGroup).centerBar(true).gap(1).x(
					d3.time.scale().domain(
							[ new Date(2011, 0, 1), new Date(2015, 11, 31) ]))
					.round(d3.time.month.round).alwaysUseRounding(true).xUnits(
							d3.time.months).title(function(d) {
						var value = d.value;
						if (isNaN(value))
							value = 0;
						return dateFormat(d.key) + ": " + numberFormat(value);
					});

			dc.dataCount(".dc-data-count").dimension(ndx).group(all);

			var dataTableData = data.slice(0, 100);

			THIS.dataTable = $(".dc-data-table")
					.DataTable(
							{
								"dom":"<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-3 col-xs-12 hidden-xs'l><'col-sm-3 col-xs-12 hidden-xs'C>r>"+
								"t"+
								"<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
								"processing" : true,
								"lengthChange" : true,
								"bSort" : true,
								"info" : true,
								"jQueryUI" : false,
								"scrollX" : true,
								"data" : dataTableData,
								 "bAutoWidth": false,
								"tableTools" : {
									"aButtons" : [ {
										"sExtends" : "collection",
										"sButtonText" : 'Export <span class="caret" />',
										"aButtons" : [ "csv", "pdf" ]
									} ],
									"sSwfPath" : "js/plugin/datatables/media/swf/copy_csv_xls_pdf.swf"
								},
								"destroy" : true,
								"columns" : [
										{
											"data" : null,
											"defaultContent" : '<img src="./img/details_open.png">'
										},
										{
											"data" : "poCount",
											"defaultContent" : "0",											
											"sClass" : "numbercolumn"
										},
										{
											"data" : "buyingCompany",
											"defaultContent" : ""
										},
										{
											"data" : "orderingCompany",
											"defaultContent" : ""
										},
										{
											"data" : "netValue",
											"defaultContent" : ""
										},
										{
											"data" : "menge",
											"defaultContent" : ""
										},
										{
											"data" : "plant",
											"defaultContent" : ""
										},
										{
											"data" : "orderType",
											"defaultContent" : ""
										},
										{

											"data" : "relatedOrderType",
											"defaultContent" : ""
										},
										{

											"data" : "addressType",
											"defaultContent" : ""
										},
										{
											"data" : "poType",
											"defaultContent" : ""
										}, {
											"data" : "materialType",
											"defaultContent" : ""
										}, {
											"defaultContent" : "",
											"data" : "source"
										}, {
											"data" : "orderDate",
											"defaultContent" : ""											
										}, ],
								"fnRowCallback" : function(nRow, aData,
										iDisplayIndex, iDisplayIndexFull) {
									var request = {
											"viewType" : "level2",
											"source" : aData.source || "null",
											"orderType" : aData.orderType || "null",
											"relatedOrderType" : aData.relatedOrderType || "null",
											"addressType" : aData.addressType || "null",
											"plant" : aData.plant || "null",
											"poType" : aData.poType || "null",
											"materialType" : aData.materialType || "null",
											"orderDate" : aData.orderDate || "null"										
										};
										$(nRow).attr({
											"id" : "row" + iDisplayIndex
										}).unbind('click').bind('click', request,
												poProfiling.callLevel2PoProfile);
								}
							});
		dc.renderAll();
	};

	this.callLevel2PoProfile = function(request) {
		// console.log(request);
		var tr = $(this).closest('tr');
		THIS.childId = "child" + $(tr).attr("id");
		if ($(tr).find('td:eq(0)').hasClass("expandRow")) {
			$($(tr).find('td:eq(0)').find('img')).attr('src',
					'./img/details_open.png');
			$("#" + THIS.childId).hide(50);
		} else {

			$($(tr).find('td:eq(0)').find('img')).attr('src',
					'./img/details_close.png');
			if (!$(tr).next().hasClass("childRowtable")) {
				$(tr).after($("<tr/>").addClass("childRowtable").attr({
					"id" : THIS.childId
				}).append($("<td/>")).append($("<td/>").attr({
					"colspan" : "13",
					"id" : THIS.childId + "td"
				}).addClass("carousel")));

				enableLoading(THIS.childId + "td", "50%", "50%");
				request = request.data;
				callAjaxService(
						"poextractanalysis",
						function (response){callbackSucessLevel2POProfile(response, request);}, callBackFailure, request, "POST");
			} else {
				$("#" + THIS.childId).show(50);
			}
		}
		$(tr).find('td:eq(0)').toggleClass("expandRow");
	};

	var callbackSucessLevel2POProfile = function(response, request) {
		disableLoading(THIS.childId + "td");
		if (response && response.isException){
			showNotification("error",response.customMessage);
			return;
		}
		var childTabledata = null;
		if (response != null)
			childTabledata = response["level2POExtractionList"];
		
		$("#" + THIS.childId + "td")
				.show()
				.html("")
				.append(
						$("<table/>").addClass(
								"table table-striped table-hover dc-data-table").attr({
							"id" : THIS.childId + "Table"
						}).append(
								$("<thead/>").append(
										$("<tr/>").append(
												$("<th/>").html(
														"Purchase Order"))
														.append(
																$("<th/>").html(
																		"Purchase Order Item"))
														.append(
																$("<th/>").html(
																		"Material No"))
														.append(
																$("<th/>")
																		.html("Vendor No"))
														.append(
																$("<th/>").html(
																		"Vendor Name"))
														.append(
																$("<th/>").html(
																		"Vendor Type"))
														.append(
																$("<th/>").html(
																		"Net Value"))	
														.append(
																$("<th/>").html(
																		"Quantity"))	
														.append(
																$("<th/>").html(
																		"Unit of Measure"))		
														.append(
																$("<th/>").html(
																		"Inventory Posting"))					
										                .append(
																$("<th/>").html(
																		"Related OrderNo"))
										)));

				$("#" + THIS.childId + "Table")
						.DataTable(
								{
									"dom":
										"t"+
										"<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
									"processing" : true,
									"lengthChange" : true,
									// "bServerSide": true,
									"info" : true,
									"filter" : false,
									"jQueryUI" : false,
									"data" : childTabledata,
									"scrollX" : true,
									"tableTools" : {
										"aButtons" : [ {
											"sExtends" : "collection",
											"sButtonText" : 'Export <span class="caret" />',
											"aButtons" : [ "csv", "pdf" ]
										} ],
										"sSwfPath" : "js/plugin/datatables/media/swf/copy_csv_xls_pdf.swf"
									},
									"columns" : [ 
									               
									               // start
									               
								 {

									"data" : "poNumber",
									"defaultContent" : "",
										"render": function(data, type, full){
										var temp = $("<td/>")
										.html(
												$(
														"<a/>").addClass("cursor-pointer")
														.attr(
																{
																	
																	"onClick" : "javascript:thirdLevelDrillDown('po','', '"
																		+ data
																		+ "','"
																		+ request.source
																		+ "')",
																'data-target' : '#myModalthredlevel',
																'data-toggle' : 'modal'
																})
														.html(
																data));
										 return temp.html();
										}
								  },	               
									               
									               // end				               
									               
									               
									/*               
									{
										"data" : "poNumber",
										"defaultContent" : ""
									},*/ {
										"data" : "poLineItem",
										"defaultContent" : ""
									}, {
										"data" : "material",
										"defaultContent" : ""
									}, {
										"data" : "vendorno",
										"defaultContent" : ""
									}, {
										"data" : "vendorName",
										"defaultContent" : ""
									}, {
										"data" : "vendorType",
										"defaultContent" : "",
									}, {
										"data" : "netValue",
										"defaultContent" : "",
									}, {
										"data" : "netPrice",
										"defaultContent" : "",
									}, {
										"data" : "uom",
										"defaultContent" : "",
									}, {
										"data" : "inventoryPosting",
										"defaultContent" : "",
									}, {
										"data" : "relatedOrder",
										"defaultContent" : "",
										"render": function(data, type, full){
											var temp = $("<td/>")
											.html(
													$(
															"<a/>").addClass("cursor-pointer")
															.attr(
																	{
																		
																		"onClick" : "javascript:thirdLevelDrillDownSO('', '"
																			+ data
																			+ "','100','"
																			+ request.source
																			+ "')",
																	'data-target' : '#myModalthredlevel',
																	'data-toggle' : 'modal'
																	})
															.html(
																	data));
											 return temp.html();
											}
										
									}
								],							
									/*"fnRowCallback" : function(nRow, aData,
									iDisplayIndex, iDisplayIndexFull) {
								   $(nRow)
										.attr(
												{
													'onClick' : "javascript:thirdLevelDrillDown('po','', '"
															+ aData.poNumber
															+ "','"
															+ request.source
															+ "')",
													'data-target' : '#myModalthredlevel',
													'data-toggle' : 'modal'
												});
							}*/
						});
	};
	this.refreshTable = function(dim) {

		if (THIS.dataTable !== null && dim !== null) {
			THIS.dataTable.clear();
			THIS.dataTable.rows.add(dim.top(1000));
			THIS.dataTable.columns.adjust().draw();
		} else
			console
					.log('[While Refreshing Data Table] This should never happen..');
	};
	// #### Version
	// Determine the current version of dc with `dc.version`
	d3.selectAll("#version").text(dc.version);
	/*var config = {
			endpoint : 'poprofile',
			dataType: 'json',
			dataKey: 'aggregatedPurchaseOrderList',
			dataPreProcess: function(d) {
				var dateFormat = d3.timeFormat("%m/%d/%Y");
				d.dd = dateFormat.parse(d.n);

				d.month = d3.time.month(d.dd);
				d.a = +d.a;
				d.j = +d.j;
				d.k = +d.k;
				d.k = +d.k;
			},
			reqPayload:{
				"source" : 'SAP',
				"viewType" : "default"
			} ,
			mappings : [{
					chartType : 'pie',
					htmlElement: '#pie-chart-Companycode',
					field: 'a',
					resetHandler: '#resetCC'
				},{
					chartType : 'pie',
					htmlElement: '#pie-chart-plant',
					field: 'b',
					resetHandler: '#resetPlant'
				},{
					chartType : 'pie',
					htmlElement: '#pie-chart-purchaseorg',
					field: 'c',
					resetHandler: '#resetPurOrg'
				},{
					chartType : 'pie',
					htmlElement: '#pie-chart-prchasegroup',
					field: 'd',
					resetHandler: '#resetPurGr'
				},{
					chartType : 'pie',
					htmlElement: '#pie-chart',
					field: 'e',
					resetHandler: '#resetMT'
				},{
					chartType : 'pie',
					htmlElement: '#pie-chart-Vendor',
					field: 'f',
					resetHandler: '#resetVen'
				},{
					chartType : 'timeline',
					htmlElement: '#timeline-area-chart',
					subHtmlElement: '#timeline-chart',
					field: 'n',
					group: 'a',
					extractMonth: true,
					resetHandler:'#resetTAC',
					mapFunction : function(p, v) {
						++p.count;
						p.total += v.k;// (v.open + v.close) / 2;
						// p.avg = numberFormat(p.total / p.count);
						return p;
					} ,
					reduceFunction : function(p, v) {
						--p.count;
						p.total -= v.k;// (v.open + v.close) / 2;
						// p.avg = p.count ? numberFormat(p.total / p.count) : 0;
						return p;
					},
					countFunction : function() {
						return {
							count : 0,
							total : 0,
						};
					},
					titleFunction : function(d) {
						var dateFormat = d3.timeFormat("%m/%d/%Y");
        				var numberFormat = d3.format(".2f");
						var value = d.value.total;
						if (isNaN(value))
							value = 0;
						return dateFormat(d.key) + "\n" + numberFormat(value);
					}
				},{
					chartType : 'horbar',
					htmlElement: '#row-chart',
					field: 'q',
					// group: 'a',
					resetHandler:'#resetHor'
				},{
					chartType : 'table',
					htmlElement: '.dc-data-table',
					field: 'a',
					groupon: 'a',
					coloums: 'a, b, c, d, e, f, g, h, i, j, k',
					sortby: 'a',
					summarydiv : '.dc-data-count',
					renderlet: 'dc-table-group',
					hasChildren: true,
					childRequestLevel :'level2',
					childurl: directoryName + '/rest/profiling/poprofile',
					childDataKey: 'aggregatedPurchaseOrderList',
					childrenParams : 'companyCode, plantgroup, purchaseorg, purchasegroup, vendor_type, material_type, lastchangedon',
					//childrenColoums : 'companyCode, materialType,netPrice,netValue,plant,plantName,poNumber,poType,purchaseOrg,purchseGroup,source,unitOfMeasure,vendorId,vendorName,vendorType',
					childMappings : [{name: 'Company Code', mapping : 'a'},
									{name: 'Plant Group', mapping : 'b'}, 
									{name: 'Purchase Org', mapping : 'c'},
									{name: 'Purchase Group', mapping : 'd'}],
					childDrillDown: 'po'
						
			}]
		};*/



		//var ProfilingPricing = new Profiling(config);
		//console.log(ProfilingPricing);
//		ProfilingPricing.init(function(){
//			$('#loading').hide();
//		});
//	
	
	
};
poProfileInstance();