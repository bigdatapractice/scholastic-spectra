'use strict';
pageSetUp();
var pricingProfiling;
var pricingProfileInstance = function() {
	pricingProfiling = new pricingProfilingObject();
	pricingProfiling.Load();
};

var pricingProfilingObject = function() {
	var THIS = this;
	var numberFormat = d3.format(",f");
	constructTabs(THIS);
	this.Load = function(obj) {
		enableLoading();
		var source = $(obj).data("source");
		THIS.source = source || $("#myTab").find("li.active").data("source");
		//THIS.source = source || "SAP";
		THIS.type = "materialCount";
		var request = {
			"source" : THIS.source,
			"viewType" : "default"
		};
		callAjaxService("pricingProfile", callbackSuccessPricingProfile,
				callBackFailure, request, "POST");
	};
	
	this.loadViewType = function(viewType){
		enableLoading();
		THIS.type = viewType;
		setTimeout(function() { drawChart();disableLoading(); }, 50);
	};
	
	var callbackSuccessPricingProfile = function(data) {
		disableLoading();
		if (data && data.isException){
			showNotification("error",data.customMessage);
			return;
		}
		if (data != undefined && data != null
				&& data['aggregatedPricingList'] != null && data['aggregatedPricingList'].length > 0) {
			THIS.data = data['aggregatedPricingList'];
			drawChart();
		}
	};
	
	var drawChart = function (){
		var data = THIS.data;
		$("[id^=pie-chart] h4").addClass("hide");
		
			var dateFormat = d3.timeFormat("%m/%d/%Y");

			data.forEach(function(d) {
				d.dd = dateFormat.parse(d.lastchangedon);
				d.month = d3.time.month(d.dd);
				d.socount = +d.socount;
				d.netvalue = +d.netvalue;
				d.zz = (THIS.type == "netvalue") ?  isNaN(d.netvalue) ? 0 : d.netvalue : isNaN(d.socount) ? 0 : d.socount;
			});

			var ndx = crossfilter(data);
			var all = ndx.groupAll();
			
			THIS.pieChartCC = dc.pieChart("#pie-chart-Companycode");
			var dataCompanyCode = ndx.dimension(function(d) {
				return (d.companycode || "Others") + '-' + (d.companycodename || "Not Available") ;
			});
			var pieCCGroup = dataCompanyCode.group();
			if (pieCCGroup.size() > 1
					|| pieCCGroup.all()[0].key.split("-")[0] != "Others") {
				 pieCCGroup = pieCCGroup.reduceSum(function(d) {
						return d.zz;
					});
			} else {
				$("#pie-chart-Companycode h4").removeClass("hide");
				pieCCGroup = pieCCGroup
				.reduceSum(function() {
					return 0;
				});
			}
			THIS.pieChartCC.width(250).height(200).radius(80)
			// .innerRadius(30)
			.dimension(dataCompanyCode).group(pieCCGroup).label(function(d) {
				var name = d.key.split("-");
				return name[0];
			}).on('filtered', function(chart) {
				THIS.refreshTable(dataCompanyCode);
			}).title(function(d) {
				return THIS.type == "netvalue" ?
						 d.key + ': $' + numberFormat(d.value)
					:
						 d.key + ': ' + numberFormat(d.value);
			});

			THIS.pieChartPlant = dc.pieChart("#pie-chart-plant");
			var dataPlant = ndx.dimension(function(d) {
				return (d.plantgroup || "Others") + '-' + (d.plantname || "Not Available") ;
			});
			var piePlantGroup = dataPlant.group();
			if (piePlantGroup.size() > 1
					|| piePlantGroup.all()[0].key.split("-")[0] != "Others") {
				piePlantGroup = piePlantGroup.reduceSum(
						function(d) {
							return d.zz;
						});
			} else {
				$("#pie-chart-plant h4").removeClass("hide");
				piePlantGroup = piePlantGroup
				.reduceSum(function() {
					return 0;
				});
			}
			THIS.pieChartPlant.width(250).height(200).radius(80)
			// .innerRadius(30)
			.dimension(dataPlant).group(piePlantGroup).label(function(d) {
				var name = d.key.split("-");
				return name[0];
			}).on('filtered', function(chart) {
				THIS.refreshTable(dataPlant);
			}).title(function(d) {
				return THIS.type == "netvalue" ?
						 d.key + ': $' + numberFormat(d.value)
					:
						 d.key + ': ' + numberFormat(d.value);
			});

			THIS.pieChartSalesOrg = dc.pieChart("#pie-chart-salesorg");
			var dataSalesOrg = ndx.dimension(function(d) {
				return (d.salesorg || "Others") + '-' + (d.salesorgname || "Not Available") ;
			});
			var pieSalesOrgGroup = dataSalesOrg.group();
			if (pieSalesOrgGroup.size() > 1
					|| pieSalesOrgGroup.all()[0].key.split("-")[0] != "Others") {
				pieSalesOrgGroup = pieSalesOrgGroup.reduceSum(
						function(d) {
							return d.zz;
						});
			} else {
				$("#pie-chart-salesorg h4").removeClass("hide");
				pieSalesOrgGroup = pieSalesOrgGroup
				.reduceSum(function() {
					return 0;
				});
			}
			THIS.pieChartSalesOrg.width(250).height(200).radius(80)
			// .innerRadius(30)
			.dimension(dataSalesOrg).group(pieSalesOrgGroup).label(function(d) {
				var name = d.key.split("-");
				return name[0];
			}).on('filtered', function(chart) {
				THIS.refreshTable(dataSalesOrg);
			}).title(function(d) {
				return THIS.type == "netvalue" ?
						 d.key + ': $' + numberFormat(d.value)
					:
						 d.key + ': ' + numberFormat(d.value);
			});

			THIS.pieChartSalesGroup = dc.pieChart("#pie-chart-SalesGroup");
			var dataSalesGroup = ndx.dimension(function(d) {
				return d.salesgroup || "Others";
			});
			var pieSalesGroup = dataSalesGroup.group();
			if (pieSalesGroup.size() > 1
					|| pieSalesGroup.all()[0].key.split("-")[0] != "Others") {
				pieSalesGroup = pieSalesGroup.reduceSum(
						function(d) {
							return d.zz;
						});
			} else {
				$("#pie-chart-SalesGroup h4").removeClass("hide");
				pieSalesGroup = pieSalesGroup
				.reduceSum(function() {
					return 0;
				});
			}
			THIS.pieChartSalesGroup.width(250).height(200).radius(80)
			// .innerRadius(30)
			.dimension(dataSalesGroup).group(pieSalesGroup).on('filtered',
					function(chart) {
						THIS.refreshTable(dataSalesGroup);
					}).title(function(d) {
						return THIS.type == "netvalue" ?
								 d.key + ': $' + numberFormat(d.value)
							:
								 d.key + ': ' + numberFormat(d.value);
					});

			THIS.pieChartMT = dc.pieChart("#pie-chart-MaterialType");
			var dataMaterialType = ndx.dimension(function(d) {
				return d.materialtype || "Others";
			});
			var pieMTGroup = dataMaterialType.group();
			if (pieMTGroup.size() > 1
					|| pieMTGroup.all()[0].key.split("-")[0] != "Others") {
				pieMTGroup = pieMTGroup.reduceSum(
						function(d) {
							return d.zz;
						});
			} else {
				$("#pie-chart-MaterialType h4").removeClass("hide");
				pieMTGroup = pieMTGroup
				.reduceSum(function() {
					return 0;
				});
			}
			THIS.pieChartMT.width(250).height(200).radius(80)
			// .innerRadius(30)
			.dimension(dataMaterialType).group(pieMTGroup).on('filtered',
					function(chart) {
						THIS.refreshTable(dataMaterialType);
					}).title(function(d) {
						return THIS.type == "netvalue" ?
								 d.key + ': $' + numberFormat(d.value)
							:
								 d.key + ': ' + numberFormat(d.value);
					});

			THIS.pieChartSOType = dc.pieChart("#pie-chart-SOType");
			var dataSOType = ndx.dimension(function(d) {
				return d.sotype;
			});
			var pieSOType = dataSOType.group();
			if (pieSOType.size() > 1
					|| pieSOType.all()[0].key.split("-")[0] != "Others") {
				pieSOType = pieSOType.reduceSum(
						function(d) {
							return d.zz;
						});
			} else {
				$("#pie-chart-SOType h4").removeClass("hide");
				pieSOType = pieSOType
				.reduceSum(function() {
					return 0;
				});
			}
			THIS.pieChartSOType.width(250).height(200).radius(80)
			// .innerRadius(30)
			.dimension(dataSOType).group(pieSOType).on('filtered',
					function(chart) {
						THIS.refreshTable(dataSOType);
					}).title(function(d) {
						return THIS.type == "netvalue" ?
								 d.key + ': $' + numberFormat(d.value)
							:
								 d.key + ': ' + numberFormat(d.value);
					});

			THIS.pieChartPricCond = dc.pieChart("#pie-chart-PricingConditon");
			var dataPriceCond = ndx.dimension(function(d) {
				return (d.salesdoctype || "Others") +" - "+(d.salesdoctypedesc || "Not Available");
			});
			var piePCGroup = dataPriceCond.group();
			if (piePCGroup.size() > 1
					|| piePCGroup.all()[0].key.split("-")[0] != "Others") {
				piePCGroup = piePCGroup.reduceSum(
						function(d) {
							return d.zz;
						});
			} else {
				$("#pie-chart-PricingConditon h4").removeClass("hide");
				piePCGroup = piePCGroup
				.reduceSum(function() {
					return 0;
				});
			}
			THIS.pieChartPricCond.width(250).height(200).radius(80)
			// .innerRadius(30)
			.dimension(dataPriceCond).group(piePCGroup).on('filtered',
					function(chart) {
						THIS.refreshTable(dataPriceCond);
					}).label(function(d) {
					    var name = d.key.split("-");
					    return name[0];
				}).title(function(d) {
						return THIS.type == "netvalue" ?
								 d.key + ': $' + numberFormat(d.value)
							:
								 d.key + ': ' + numberFormat(d.value);
					});

			THIS.pieChartDistCha = dc.pieChart("#pie-chart-DistChannel");
			var dataDistCha = ndx.dimension(function(d) {
				return (d.distributionchannel || "Others") + '-' + (d.distributionchannelname || "Not Available");
			});
			var pieDCGroup = dataDistCha.group();
			if (pieDCGroup.size() > 1
					|| pieDCGroup.all()[0].key.split("-")[0] != "Others") {
				pieDCGroup = pieDCGroup.reduceSum(
						function(d) {
							return d.zz;
						});
			} else {
				$("#pie-chart-DistChannel h4").removeClass("hide");
				pieDCGroup = pieDCGroup
				.reduceSum(function() {
					return 0;
				});
			}
			THIS.pieChartDistCha.width(250).height(200).radius(80)
			// .innerRadius(30)
			.dimension(dataDistCha).group(pieDCGroup).label(function(d) {
				var name = d.key.split("-");
				return name[0];
			}).on('filtered', function(chart) {
				THIS.refreshTable(dataDistCha);
			}).title(function(d) {
				return THIS.type == "netvalue" ?
						 d.key + ': $' + numberFormat(d.value)
					:
						 d.key + ': ' + numberFormat(d.value);
			});

			THIS.pieChartDiv = dc.pieChart("#pie-chart-Division");
			var dataDivCha = ndx.dimension(function(d) {
				d.divisionname = (d.divisionname || "Others");
				return (d.division || "Others") + "-"+ (d.divisionname || "Not Available");
			});
			var pieDGroup = dataDivCha.group();
			if (pieDGroup.size() > 1
					|| pieDGroup.all()[0].key.split("-")[0] != "Others") {
				pieDGroup = pieDGroup.reduceSum(
						function(d) {
							return d.zz;
						});
			} else {
				$("#pie-chart-Division h4").removeClass("hide");
				pieDGroup = pieDGroup
				.reduceSum(function() {
					return 0;
				});
			}
			THIS.pieChartDiv.width(250).height(200).radius(80)
			// .innerRadius(30)
			.dimension(dataDivCha).group(pieDGroup).on('filtered',
					function(chart) {
						THIS.refreshTable(dataDivCha);
					}).title(function(d) {
						return THIS.type == "netvalue" ?
								 d.key + ': $' + numberFormat(d.value)
							:
								 d.key + ': ' + numberFormat(d.value);
					});

			THIS.timelineAreaChart = dc.lineChart("#timeline-area-chart");
			THIS.timelineChart = dc.barChart("#timeline-chart");
			THIS.horzBarChart = dc.rowChart("#row-chart");
			var horzBarDimension = ndx.dimension(function(d) {
				return (d.pricingcondition || "Others") + '_' + (d.pricingconditionname || "Not Available");
			});
			var horzBarGrp = horzBarDimension.group().reduceSum(function(d) {
				return d.zz;
			});

			THIS.horzBarChart.width(180).height(850).margins({
				top : 20,
				left : 10,
				right : 10,
				bottom : 20
			}).group(horzBarGrp).dimension(horzBarDimension).on('filtered',
					function(chart) {
						THIS.refreshTable(horzBarDimension);
					}).
				label(function(d) {
				    var name = d.key.split("_");
				    return name[0];
			})
			// assign colors to each value in the x scale domain
			.ordinalColors(
					[ '#3182bd', '#6baed6', '#9e5ae1', '#c64bef', '#da8aab' ])
					/*.label(function(d) {

						return d.key;// .split(".")[1];
					})*/
					// title sets the row text
					.title(function(d) {
						return THIS.type == "netvalue" ?
								 d.key + ': $' + numberFormat(d.value)
							:
								 d.key + ': ' + numberFormat(d.value);
					}).elasticX(true).xAxis().ticks(4);

			var timelineDimension = ndx.dimension(function(d) {
				return d.month;
			});
			// group by total volume within move, and scale down result
			var timelineGroup = timelineDimension.group().reduceSum(
					function(d) {
						return d.zz;
					});
			// group by total movement within month
			// var timelineSecondGroup = timelineDimension.group().reduceSum(
			// function(d) {
			// return d.grossOrderValue;
			// });
			var timelineAreaGroup = timelineDimension.group().reduce(
					function(p, v) {
						++p.count;
						v.netvalue = v.netvalue || 0;
						p.total += v.netvalue;// (v.open + v.close) / 2;
						// p.avg = numberFormat(p.total / p.count);
						return p;
					}, function(p, v) {
						--p.count;
						v.netvalue = v.netvalue || 0;
						p.total -= v.netvalue;// (v.open + v.close) / 2;
						// p.avg = p.count ? numberFormat(p.total / p.count) : 0;
						return p;
					}, function() {
						return {
							count : 0,
							total : 0,
						};
					});

			THIS.timelineAreaChart.renderArea(true)
					.width($('#content').width()).height(150)
					.transitionDuration(1000).margins({
						top : 30,
						right : 70,
						bottom : 25,
						left : 80
					}).dimension(timelineDimension).mouseZoomable(true)
					// Specify a range chart to link the brush extent of the
					// range
					// with
					// the zoom focue of the current chart.
					.rangeChart(THIS.timelineChart).x(
							d3.time.scale().domain(
									[ new Date(2011, 0, 1),
											new Date(2015, 11, 31) ])).round(
							d3.time.month.round).xUnits(d3.time.months)
					.elasticY(true).renderHorizontalGridLines(true)
					// .legend(dc.legend().x(800).y(10).itemHeight(13).gap(5))
					.brushOn(false).group(timelineAreaGroup, "Vendor Count")
					.valueAccessor(function(d) {
						return d.value.total;
					}).on('filtered', function(chart) {
						THIS.refreshTable(timelineDimension);
					})// .stack(timelineSecondGroup, "Material Count",
					// function(d) {
					// return d.value;
					// })
					// title can be called by any stack layer.
					.title(
							function(d) {
								var value = d.value.total;
								if (isNaN(value))
									value = 0;
								return dateFormat(d.key) + "\n $"
										+ numberFormat(value);
							});

			THIS.timelineChart.width($('#content').width()).height(80).margins(
					{
						top : 40,
						right : 70,
						bottom : 20,
						left : 80
					}).dimension(timelineDimension).group(timelineGroup).centerBar(true).gap(1).x(
					d3.time.scale().domain(
							[ new Date(2011, 0, 1), new Date(2015, 11, 31) ]))
					.round(d3.time.month.round).alwaysUseRounding(true).xUnits(
							d3.time.months).title(function(d) {
						var value = d.value.total;
						if (isNaN(value))
							value = 0;
						return dateFormat(d.key) + "\n" + numberFormat(value);
					});

			dc.dataCount(".dc-data-count").dimension(ndx).group(all);

			// dc.dataTable(".dc-data-table").dimension(dataTableDimension).group(
			// function(d) {
			//
			// var format = d3.format("02d");
			// return d.dd.getFullYear() + "/"
			// + format((d.dd.getMonth() + 1));
			// }).size(50) // (optional) max number of records to be shown,
			// // :default = 25
			// // dynamic columns creation using an array of closures
			// .columns([ function(d) {
			// return d.date;
			// }, function(d) {
			// return numberFormat3(d.companycode);
			// }, function(d) {
			// return d.plantgroup;
			// }, function(d) {
			// return d.salesorg;
			// }, function(d) {
			// return d.materialtype;
			// }, function(d) {
			// return d.salestype;
			// },, function(d) {
			// return d.salesdoctype;
			// }, function(d) {
			// return d.pricingcondition;
			// }, function(d) {
			// return d.distributionchannel;
			// }, function(d) {
			// return d.division;
			// }, function(d) {
			// return numberFormat(d.netvalue);
			// }, function(d) {
			// return d.lastchangedon;
			// } ])
			// // (optional) sort using the given field, :default =
			// // function(d){return d;}
			// .sortBy(function(d) {
			// return d.date;
			// })
			// // (optional) sort order, :default ascending
			// .order(d3.ascending)
			// // (optional) custom renderlet to post-process chart using D3
			// .renderlet(function(table) {
			// table.selectAll(".dc-table-group").classed("info", true);
			// });
			var dataTableData = data.slice(0, 100);
			THIS.dataTable = $(".dc-data-table")
					.DataTable(
							{
								"dom":"<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-3 col-xs-12 hidden-xs'l><'col-sm-3 col-xs-12 hidden-xs'C>r>"+
								"t"+
								"<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
								"processing" : true,
								"lengthChange" : true,
								"bSort" : true,
								"info" : true,
								"jQueryUI" : false,
								"scrollX" : true,
								"data" : dataTableData,
								"oTableTools" : {
									"aButtons" : [ {
										"sExtends" : "collection",
										"sButtonText" : 'Export <span class="caret" />',
										"aButtons" : [ "csv", "pdf" ]
									} ],
									"sSwfPath" : "js/plugin/datatables/media/swf/copy_csv_xls_pdf.swf"
								},
								"bDestroy" : true,
								"columns" : [
										{
											"data" : null,
											"defaultContent" : '<img src="./img/details_open.png">'
										},
										{
											"data" : "socount",
											"defaultContent" : "",
											"class" : "numbercolumn"
										},
										{
											"data" : "companycode",
											"defaultContent" : "",
										},
										{
											"data" : "companycodename",
											"defaultContent" : "",
										},
										{
											"data" : "salesorg",
											"defaultContent" : "",
										},
										{
											"data" : "salesorgname",
											"defaultContent" : "",
										},
										{
											"defaultContent" : "",
											"mData" : "salesgroup"
										},
										{
											"data" : "distributionchannel",
											"defaultContent" : "",
										},
										{
											"data" : "distributionchannelname",
											"defaultContent" : "",
										},
										{
											"data" : "division",
											"defaultContent" : "",
										},
										{
											"data" : "divisionname",
											"defaultContent" : "",
										},
										{
											"data" : "pricingcondition",
											"defaultContent" : "",
										},
										{
											"data" : "pricingconditionname",
											"defaultContent" : "",
										},
										{
											"data" : "materialtype",
											"defaultContent" : "",
										},
										{
											"data" : "plantgroup",
											"defaultContent" : "",
										},
										{
											"data" : "plantname",
											"defaultContent" : "",
										},
										/*
										 * { "data" : "division",
										 * "defaultContent" : "0", "mRender" :
										 * function(data, type, full) { if
										 * (isNaN(data)) data =0; return
										 * numberFormat(data); }, "class" :
										 * "numbercolumn" },
										 */
										{
											"data" : "salesdoctype",
											"defaultContent" : "",
										},
										{
											"data" : "sotype",
											"defaultContent" : "",
										},
										{
											"data" : "netvalue",
											"defaultContent" : "0",
											"mRender" : function(data, type,
													full) {
												return numberFormat(data);
											},
											"class" : "numbercolumn"
										}, {
											"data" : "source",
											"defaultContent" : "",
										}, {
											"data" : "region",
											"defaultContent" : "",
										}, {
											"data" : "lastchangedon",
											"defaultContent" : "",
										} ],
								"rowCallback" : function(nRow, aData,
										iDisplayIndex, iDisplayIndexFull) {
									var request = {
										"viewType" : "level2",
										"source" : aData.source || "null",
										"salesorg" : aData.salesorg || "null",
										"division" : aData.division || "null",
										"conditiontype" : aData.pricingcondition
												|| "null",
										"salestype" : aData.salesdoctype
												|| "null",
										"condpricingdate" : aData.lastchangedon
												|| "null",
										"plant" : aData.plantgroup || "null",
										"distchannel" : aData.distributionchannel
												|| "null",
										"region" : aData.region || "null",
										"companycode" : aData.companycode || "null", 
										"materialtype" : aData.materialtype || "null",
										"sotype" : aData.sotype || "null"
												
									// "pricingcondition" :
									// aData.pricingcondition || "null",
									// "socount" : aData.socount || "null"


									};
									/*
									 * private String soCount; a private String
									 * companyCode; b private String
									 * companyName; c private String salesOrg; d
									 * private String salesOrgName; e private
									 * String salesGroup; f private String
									 * salesGroupName; g private String
									 * division; h private String divisionName;
									 * i private String salesDocType; j private
									 * String plantGroup; k private String
									 * plantName; l private String netValue; m
									 * private String source; n private String
									 * region; o private String lastChanged; p
									 * private String materialType; q private
									 * String soType; r
									 */
									$(nRow)
											.attr({
												"id" : "row" + iDisplayIndex
											})
											.unbind('click')
											.bind(
													'click',
													request,
													pricingProfiling.callLevel2pricingProfile);
								}
							});

			dc.renderAll();
		};

	this.callLevel2pricingProfile = function(request) {
		var tr = $(this).closest('tr');
		THIS.childId = "child" + $(tr).attr("id");
		if ($(tr).find('td:eq(0)').hasClass("expandRow")) {
			$($(tr).find('td:eq(0)').find('img')).attr('src',
					'./img/details_open.png');
			$("#" + THIS.childId).hide(50);
		} else {

			$($(tr).find('td:eq(0)').find('img')).attr('src',
					'./img/details_close.png');
			if (!$(tr).next().hasClass("childRowtable")) {
				$(tr).after($("<tr/>").addClass("childRowtable").attr({
					"id" : THIS.childId
				}).append($("<td/>")).append($("<td/>").attr({
					"colspan" : "21",
					"id" : THIS.childId + "td"
				}).addClass("carousel")));

				enableLoading(THIS.childId + "td", "50%", "30%");
				request = request.data;
				callAjaxService("pricingProfile", function(response) {
					callbackSucessLevel2PricingProfile(response, request);
				}, callBackFailure, request, "POST");
			} else {
				$("#" + THIS.childId).show(50);
			}
		}
		$(tr).find('td:eq(0)').toggleClass("expandRow");
	};
	var callbackSucessLevel2PricingProfile = function(response, request) {
		disableLoading(THIS.childId + "td");
		if (response && response.isException){
			showNotification("error",response.customMessage);
			return;
		}
		var childTabledata = response["level2PricingList"];
		$("#" + THIS.childId + "td").show().html("").append(
				$("<table/>").addClass("table table-striped table-hover dc-data-table").attr(
						{
							"id" : THIS.childId + "Table"
						}).append(
						$("<thead/>").append(
								$("<tr/>").append(
										$("<th/>").html("Pricing Condition No"))

								.append($("<th/>").html("Condition Type"))

								.append($("<th/>").html("Net Value"))

								.append(
										$("<th/>").html(
												"Condition Pricing Date"))

						)));

		$("#" + THIS.childId + "Table")
				.DataTable(
						{
							dom:"t"+
							"<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
							"processing" : true,
							"lengthChange" : true,
							// "bServerSide": true,
							"info" : true,
							"filter" : false,
							"jQueryUI" : false,
							"data" : childTabledata,
							"scrollX" : true,
							"tableTools" : {
								"aButtons" : [ {
									"sExtends" : "collection",
									"sButtonText" : 'Export <span class="caret" />',
									"aButtons" : [ "csv", "pdf" ]
								} ],
								"sSwfPath" : "js/plugin/datatables/media/swf/copy_csv_xls_pdf.swf"
							},
							"columns" : [ {
								"data" : "docconditionno",
								"defaultContent" : ""
							}, {
								"data" : "conditiontype",
								"defaultContent" : ""
							}, {
								"data" : "netvalue",
								"defaultContent" : ""
							}, {
								"data" : "condpricingdate",
								"defaultContent" : ""
							} ],
							"rowCallback" : function(nRow, aData,
									iDisplayIndex, iDisplayIndexFull) {
								$(nRow)
										.attr(
												{
													'onClick' : "javascript:pricingProfiling.thirdLevelDrillDownPricingProfile('e2e_"+THIS.source+"_l3_pr','"+ aData.docconditionno +"')",
													'data-target' : '#myModalthredlevel',
													'data-toggle' : 'modal'
												});
							}
						});
	};
	this.thirdLevelDrillDownPricingProfile = function(entitytype, criteria) {
		$("#poDocView").empty();
		enableLoading("poDocView");
		var CallbackSuccessLevel3Details = function(l3Data) {
			if (l3Data && l3Data.isException){
				showNotification("error",l3Data.customMessage);
				return;
			}
			if (l3Data && l3Data[0]) {
				l3Data = l3Data[0];
				var keys = Object.keys(l3Data);
				var onAjaxLoadSuccess = function(html) {
					if (html && html.isException){
						showNotification("error",html.customMessage);
						return;
					}
					$('#myModalthredlevel #myModalLabel').text(entitytype);
					$("#poDocView").html(html);
					$("#entity").text(entitytype);
					$("#criteria").text(criteria);
					$("#btnPrintL3").unbind('click').bind('click', function(e) {
						printElement('#poDocView');
					});
					$("#btnSaveL3").unbind('click').bind('click',function(e) {
						savePDF('#poDocView');
					});
					/*
					 * keys.forEach(function(key){ console.log(l3Data[key]);
					 * $("#tbDynamic tbody").append($("<tr>\n").append("<th>"+key+"</th>\n").append("<td>"+l3Data[key]+"</td>\n")); })
					 */
					// $("<tr/>").append($("<td/>").html(i+1)).append($("<td/>").html("po[i].ekpo_matnr"))
					var i = 0, idx = keys.length, lvalue, lkey, rvalue, rkey;
					while (i < idx) {
						lvalue = l3Data[keys[i]] !== undefined
								&& l3Data[keys[i]] !== null ? l3Data[keys[i]]
								: '';
						while (lvalue == '' && i < idx) {
							i++;
							lvalue = l3Data[keys[i]] !== undefined
									&& l3Data[keys[i]] !== null ? l3Data[keys[i]]
									: '';
						}
						lkey = keys[i] !== undefined && keys[i] !== null ? keys[i]
								: '';
						i++;
						rvalue = l3Data[keys[i]] !== undefined
								&& l3Data[keys[i]] !== null ? l3Data[keys[i]]
								: '';
						while (rvalue == '' && i < idx) {
							i++;
							rvalue = l3Data[keys[i]] !== undefined
									&& l3Data[keys[i]] !== null ? l3Data[keys[i]]
									: '';
						}
						rkey = keys[i] !== undefined && keys[i] !== null ? keys[i]
								: '';
						if (rvalue == '') {
							rkey = '';
						}
						var colTitle = lookUpTable.get(lkey.toUpperCase());
						if (colTitle != null){
							lkey = colTitle;
						}
						 colTitle = lookUpTable.get(rkey.toUpperCase());
							if (colTitle != null){
								rkey = colTitle;
							}
						if (rvalue != '' || lvalue != '') {
							$("#tbDynamic tbody")
									.append(
											$("<tr>\n")
													.append(
															"<th>" + lkey
																	+ "</th>\n")
													.append(
															"<td>" + lvalue
																	+ "</td>\n")
													.append(
															"<th>" + rkey
																	+ "</th>\n")
													.append(
															"<td>" + rvalue
																	+ "</td>\n"));
						}

						i++;
					}
					// for(var i=0, idx= keys.length; i<idx; i++){
					// var lkey = keys[i]!==undefined && keys[i] !==
					// null?keys[i]:'';
					// var lvalue = l3Data[keys[i]] !== undefined &&
					// l3Data[keys[i]] !== null? l3Data[keys[i]] :'';
					// var rkey = keys[i+1]!==undefined && keys[i+1] !==
					// null?keys[i+1]:'';
					// var rvalue = l3Data[keys[i+1]] !== undefined &&
					// l3Data[keys[i+1]] !== null? l3Data[keys[i+1]] :'';
					// if(lvalue !== '' || rvalue != ''){
					// $("#tbDynamic tbody").append($("<tr>\n")
					// .append("<th>"+lkey+"</th>\n")
					// .append("<td>"+lvalue+"</td>\n")
					// .append("<th>"+rkey+"</th>\n")
					// .append("<td>"+rvalue+"</td>\n"));
					// }
					// }
			       
			        
				};
				callAjaxService("SearchL3View", onAjaxLoadSuccess, function() {
					alert("error");
				}, null, "GET", "html");
			} else {
				$("#poDocView").html('<div class="alert alert-danger fade in"><a class="close" data-dismiss="alert" href="#">×</a><h4 class="alert-heading"><i class="fa fa-exclamation-triangle"></i>&nbsp;OOPS!</h4><p class="text-align-left">No matching results found.</p></div>');;
			}
		};
		var request = {
			"entitytype" : System.dbName +"."+entitytype,
			"criteria" : "KNUMV:" + criteria
		};
		callAjaxService("Level3Details", CallbackSuccessLevel3Details,
				callBackFailure, request, "POST");
	};

	this.refreshTable = function(dim) {

		if (THIS.dataTable !== null && dim !== null) {
			THIS.dataTable.clear();
			THIS.dataTable.rows.add(dim.top(1000));
			THIS.dataTable.columns.adjust().draw();
		} else
			console
					.log('[While Refreshing Data Table] This should never happen..');
	};
	d3.selectAll("#version").text(dc.version);
};
pricingProfileInstance();