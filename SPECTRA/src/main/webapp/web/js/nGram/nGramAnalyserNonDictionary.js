'use strict';
pageSetUp();
var nGramAnalyser;
var nGramAnalyserInstance = function() {
	nGramAnalyser = new nGramAnalyserObject();
	nGramAnalyser.Load();
};
var panZoomForward;
var nGramAnalyserObject = function(){
	var THIS = this;
	$("#nGramform").submit(function(e) {
		//nGramAnalyser.Load();
	    e.preventDefault();
	});
	$("#submitButton").unbind("click").bind("click", function(){nGramAnalyser.Load();});
	$("#nGramSelect li").unbind("click").bind("click", function(){$("#nGrambutton").html($(this).text()+' <i class="fa fa-caret-down" ></i>');$("#nGrambutton").attr({'value':$(this).val()});THIS.Load();});
	$("#nGramform").validate({

		// Rules for form validation
		rules : {
			nGramInput : {
				required : true
			}
		},

		// Messages for form validation
		messages : {
			nGramInput : {
				required : 'Input String cannot be empty..'
			}
		},

		// Do not change code below
		errorPlacement : function(error, element) {
			error.insertAfter(element.parent());
		}
	});
	
	this.Load = function(){
		enableLoading();
		THIS.nGram = $("#nGrambutton").attr('value');
		if (THIS.nGram  == "0"){
			showNotification("error","Select Gram.");
			return;
		}
		var request = {
				"gram":THIS.nGram 
		};
		callAjaxService("nGramAnalyserNonDictionaryPost",nGramAnalyserCallBackSucess,callBackFailure);
	};
	var nGramAnalyserCallBackSucess = function(response){
		disableLoading();
		if (response && response.isException){
			showNotification("error",response.customMessage);
			return;
		}
		$("#fieldsetCloud").removeClass("hide");
		$("#cloud").empty();
		var width = 1000;
		var height = 600;
		var svg = d3.select('#cloud').append('svg')
		.attr('width', width)
		.attr('height', height)
		.attr('id', 'cloudSVG')
		.append('g')
		.attr('transform', 'translate('+width/2+', '+height/2+')');
		//function drawCloud(words) { d3.select('#cloud').append('svg') .attr('width', 600).attr('height', 400) .append('g') .selectAll('text') .data(words) .enter().append('text') .style('font-size', function(d) { return d.size + 'px'; }) .style('font-family', function(d) { return d.font; }) .style('fill', function(d, i) { return colors(i); }) .attr('text-anchor', 'middle') .attr('transform', function(d) { return 'translate(' + [d.x, d.y] + ')rotate(' + d.rotate + ')'; }) .text(function(d) { return d.text; }); }
		function drawCloud(words) {
			var vis = svg.selectAll('text').data(words);

			vis.enter().append('text')
				.style('font-size', function(d) { return d.size + 'px'; })
				.style('font-family', function(d) { return d.font; })
				.style('fill', function(d, i) { return colors(i); })
				.attr('text-anchor', 'middle')
				.attr('class', "icon-hover")
				.attr('onclick',function(d){return "nGramAnalyser.gramClick('"+d.text+"')";})
				.attr('transform', function(d) {
				  return 'translate(' + [d.x, d.y] + ')rotate(' + d.rotate + ')';
				})
				.text(function(d) { return d.text; });
		}
		var data = response["nGramAnalyser"];
		var totalsize = 1000;
		for (var i=0;i<data.length;i++){
			
			var temp =1250/(data[i].size*data[i].text.length);
			if (totalsize > temp)
				totalsize = temp;
		}
		
		var minFontSize =totalsize;
		
		console.log(totalsize);

		var typeFace = 'Gorditas';
		//var minFontSize = 0.5;
		var colors = d3.scale.category20b();
		/*data = data.sort(function(a, b){
		    return b.size - a.size;
		}).slice(0, 100);*/
		d3.layout.cloud()
		.size([width, height])
		.words(data)
		.rotate(function() { return ~~(Math.random()*2) * 0;}) // 0 or 90deg
		.font(typeFace)
		.fontSize(function(d) { return d.size * minFontSize; })
		.on('end', drawCloud)
		.start();
		
		if ($(".onoffswitch-checkbox").is(":checked")){
			panZoomForward = PanZoomsvg('treeviewidforward');
			if (panZoomForward && panZoomForward != null){
				panZoomForward.enableZoom();
				panZoomForward.enablePan();
			}
		}
		//$('svg g text').unbind("click").bind("click",function(){THIS.gramClick($($('svg g text')[0]).text());});
		//d3.layout.cloud() .size([600, 400]).words(data).rotate(function() { return ~~(Math.random()*2) * 90;}).fontSize(function(d) { return d.size; }) .on('end', drawCloud) .start();
		
		
		
		//d3.select('#cloud').append('svg') .attr('width', 600).attr('height', 400) .append('g') .selectAll('text') .data(words) .enter().append('text') .style('font-size', function(d) { return d.size + 'px'; }) .style('font-family', function(d) { return d.font; }) .style('fill', function(d, i) { return colors(i); }) .attr('text-anchor', 'middle') .attr('transform', function(d) { return 'translate(' + [d.x, d.y] + ')rotate(' + d.rotate + ')'; }) .text(function(d) { return d.text; });
	};
	this.enablePanZoom = function(){
		if ($(".onoffswitch-checkbox").is(":checked")){
			panZoomForward = PanZoomsvg('cloudSVG');
			if (panZoomForward && panZoomForward != null){
				panZoomForward.enableZoom();
				panZoomForward.enablePan();
			}
		} else{
			if (panZoomForward && panZoomForward != null){
				panZoomForward.disableZoom();
				panZoomForward.disablePan();
			}
		}
	};
	this.gramClick = function(text){
		searchKey = text;
		window.location.hash = "#catalogSearch";
	};
};
nGramAnalyserInstance();