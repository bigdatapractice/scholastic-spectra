'use strict';
pageSetUp();
var panZoomForward;
var nGramAnalyserObject = function(){
	var THIS = this,filterTemplate;
	THIS.id = 0;
	templates.getFilterConfig(null,function(){
		filterTemplate = templates.getFilterConfig.template(null);
		$("#filters").append(filterTemplate);
		
	});
	$("#nGramform,#frmModalWhere,#frmProfileConfig,#frmSaveProfile").submit(function(e) {
	    e.preventDefault();
	});
	$("#gramType").select2({
			placeholder : "Select Gram",
			data :  [{"id":"1","text":"Uni-grams"},{
					"id":"2","text":"Bi-grams"},{
					"id":"3","text":"Tri-grams"},{
					"id":"4","text":"4-grams"},{
					"id":"5","text":"5-grams"}]
	});
	$("#selectTable,#nGramInput").hide();
	$("#tableBack").unbind("click").bind("click", function() {
		$("#selectSource").show(300);
		$("#selectTable").hide(300);
	});
	$("#proceed").unbind("click").bind("click", function() {
		if (!$("#frmConf").valid())
			return;
		$("#selectTable").hide();
		$("#nGramInput").show();
		procesFilter(0);
		//$("#myModalWhere").modal("show");
	});
	$("#myProfilesBack").unbind("click").bind("click", function() {
		$("#selectSource").show(300);
		$("#myProfiles").hide(300);
	});
	$("#saveProfile").unbind("click").bind("click", function() {		
		if(THIS.id){
			saveProfile();
		}else{
			$("#myModalSave").modal("show");
		}
	});
	$("#btnSaveProfile").unbind("click").bind("click", function() {
		if (!$("#frmSaveProfile").valid())
			return;
		saveProfile();
	});
	$("#publish").unbind("click").bind("click", function() {
		publishProfile();
	});
	$("#previewProfile").unbind("click").bind("click", function() {
		if (!$("#frmProfileConfig").valid())
			return;		
		getNGramAnalyser();
	});
	function procesFilter(index,data){
		var element = $("#filters").find(".well:eq("+index+")");
		element.find("[name='field']").select2({
			placeholder : "Select Field",
			data :  THIS.fieldList
		});
		element.find("[name='addlink']").unbind("click").bind("click", function() {
			$("#filters").append(templates.getFilterConfig.template(null));
			procesFilter($("#filters").find(".well").length-1);
		});
		element.find("[name='removelink']").unbind("click").bind("click", function() {
			$(this).parents('.well').remove();
		});
		if(data){
			element.find("[name=field]").select2('val',data.fieldName);
			element.find("[name=displayname]").val(data.displayName);
			element.find("[name=fieldtype]").val(data.fieldType);
			if(data.isMandatory){
				element.find("[name=isMandatory]").attr("checked","checked");	
			}
			if(data.invisible){
				element.find("[name=invisible]").attr("checked","checked");	
			}
			if(data.innotin){
				element.find("[name=innotin]").attr("checked","checked");
			}
			element.find("[name=searchtype]").val(data.searchtype || "in");
			onChangeTypeInFieldConfig(element.find("[name=fieldtype]"),data);
			if (data.fieldType === "lookup" || data.fieldType === "select") {
				element.find("[name=lookup]").val(data.lookupquery);
				if(data.defaultvalue){
					if(data.isSingle){
						element.find("[name=selecttype]").val(data.isSingle);
					}
					element.find("[name=defaultvalue]").select2('data', data.defaultvalue, true);
				}
				
			} else if (data.fieldType === 'DATE' || data.fieldType === 'dynamicDate' || data.fieldType === 'NUM') {
				element.find("[name=from-defaultvalue]").val(data["from-defaultvalue"]);
				element.find("[name=to-defaultvalue]").val(data["to-defaultvalue"]);
			} else {
				element.find("[name=defaultvalue]").val(data.defaultvalue);
			}
		}		
		element.find("[name='fieldtype']").unbind("change").bind("change", function() {
			onChangeTypeInFieldConfig(this)
		});
	}
	var onChangeTypeInFieldConfig = function(obj, data) {
		var inputLabel = $(obj).parent().parent().find("[name='defaultvaluelabel']");
		//$(obj).parent().next().find("label:eq(1)");
		$(inputLabel).removeClass().empty();
		if ($(obj).val() === 'lookup' || $(obj).val() === 'select') {
			var mainDiv = $(obj).parent().parent();
			// $(inputLabel).addClass("input").append($("<input/>").attr({"type":"text","name":"defaultvalue"}));
			mainDiv.find("[name='lookupdiv']").show().find("select").unbind("change").bind("change",function(){
				constructDefaultValueSelect();
			});
			mainDiv.find("[name='lookupdiv']:eq(0)").find("input").unbind("blur").bind("blur",function(){
				constructDefaultValueSelect();
			});
			function formatResult(resp) {
				return resp.id + ' - ' + resp.name;
			}

			function formatSelection(resp) {
				return resp.id + ' - ' + resp.name;
			}

			function constructDefaultValueSelect(){
				var selectType = mainDiv.find("[name='lookupdiv']:eq(1)").find("select").val();
				if(data && data.isSingle){
					selectType = data.isSingle;
				}
				//$(obj).parent().parent().find("defaultvaluelabel]").select2('destroy'); 
				if($(obj).val() === 'select'){
					var selectdata =  mainDiv.find("[name='lookupdiv']:eq(0)").find('input').val();
					if(selectdata){
						selectdata = Jsonparse(selectdata);
						if(!selectdata || Object.keys(selectdata).length === 0){
							selectdata = [];
							var values =  mainDiv.find("[name='lookupdiv']:eq(0)").find('input').val().split(',');
							if(values && values.length>0){
								for(var i=0;i<values.length;i++){
									selectdata.push({id:values[i],text:values[i]});	
								}								
							}
						}
					}else if(data.defaultvalue){
						selectdata = data.defaultvalue;
					}
					$(obj).parent().parent().find("[name='lookupdiv']:eq(0)").find("strong").html("Enter Select value");
					$(inputLabel).find("[name='defaultvalue']").select2({
						placeholder : "Enter " + $(obj).parent().prev().find("input").val(),
						allowClear: selectType,
						multiple:!selectType,
						width: 'resolve',
						data : selectdata || []
					});
					if(data.defaultvalue){
						$(inputLabel).find("[name='defaultvalue']").select2("data",data.defaultvalue);
					}
				}else{
					mainDiv.find("[name='lookupdiv']:eq(0)").find("strong").html("Look Up Query");
					var selectedData;
					if(data && data.defaultvalue){
						selectedData = data.defaultvalue;
					}
					$(inputLabel).find("[name='defaultvalue']").select2({
									placeholder : "Enter " + $(obj).parent().prev().find("input").val(),
									minimumInputLength : 1,
									allowClear: selectType,
									width: 'resolve',
									multiple : !selectType,
									ajax : {
										url : serviceCallMethodNames["suggestionsLookUp"].url,
										dataType : 'json',
										type : 'GET',
										data : function(term) {
											return {
												q : term,
												source : THIS.sourceType,
												database : THIS.database,
												lookupQuery : mainDiv.find("[name='lookupdiv']:eq(0)").find("input").val()
											};
										},
										results : function(data) {
											return {
												results : data
											};
										}
									},
									initSelection : function(element, callback) {
										callback($.map(element.val().split(','),
												function(id) {
													id = id.split(":");
													return {
														id : id[0],
														name : id[1]
													};
												}));
									},
									formatResult : formatResult,
									formatSelection : formatSelection,
									dropdownCsclass : "bigdrop",
									escapeMarkup : function(m) {
										return m;
									}
								});
					
					if(selectedData && (selectedData.length>0 || Object.keys(selectedData).length>0)){
						if(selectType){
							selectedData = Array.isArray(selectedData)?selectedData[0]:selectedData;
						}else{
							selectedData = Array.isArray(selectedData)?selectedData:[selectedData];
						}
						$(obj).parent().parent().find("[name='defaultvalue']").select2('data',selectedData);
					}
				}
				
			}
			$(inputLabel).addClass("input").append($("<input/>").attr({
				"type" : "text",
				"name" : "defaultvalue"
			}).addClass("width-100per"));
			constructDefaultValueSelect();			
			
		} else if ($(obj).val() === 'DATE' || $(obj).val() === 'dynamicDate' || $(obj).val() === 'NUM') {
			$(inputLabel).addClass("row").append(
					$('<span/>').addClass('col-sm-2 text-right').html('From:'))
					.append($('<input/>').addClass('col-sm-3 input-xs').attr("name", "from-defaultvalue"))
					.append($('<span/>').addClass('col-sm-1 text-right').html('To:')).append(
							$('<input/>').addClass('col-sm-3 input-xs').attr("name", "to-defaultvalue"));
			if ($(obj).val() === 'DATE') {
				$(inputLabel).find('input:eq(0)').datepicker(
						{
							defaultDate : "+1w",
							changeMonth : true,
							onClose : function(selectedDate) {
								$(inputLabel).find('input:eq(1)').datepicker(
										"option", "minDate", selectedDate);
							}
						});
				$(inputLabel).find('input:eq(1)').datepicker(
						{
							defaultDate : "+1w",
							changeMonth : true,
							onClose : function(selectedDate) {
								$(inputLabel).find('input:eq(0)').datepicker(
										"option", "maxDate", selectedDate);
							}
						});				
			}
			//$(obj).parent().parent().find("[name='lookupdiv']").hide();
		} else {
			$(inputLabel).addClass("input").append($("<input/>").attr({
				"type" : "text",
				"name" : "defaultvalue"
			}));
			//$(obj).parent().parent().find("[name='lookupdiv']").hide();

		}
	};
	function getConfig(){
		var chartConfig = {};
		chartConfig.field = $("#field").select2("val");
		chartConfig.ngramType = $("#gramType").select2("val");
		return chartConfig;
	}
	var getFilters = function(){
		var filterConfig = [];
		$("#filters > .well")
				.each(
						function() {
							var currentObj = $(this);
							if (currentObj.find("[name=field]").select2('val')
									&& currentObj.find("[name=field]").select2(
											'val') !== "") {
								var tempConfig = {
									"fieldName" : currentObj.find("[name=field]").select2('val'),
									"displayName" : currentObj.find("[name=displayname]").val(),
									"fieldType" : currentObj.find("[name=fieldtype]").val()
								};
								if(currentObj.find("[name=isMandatory]").is(":checked")){
									tempConfig.isMandatory = true;
								}
								if(currentObj.find("[name=invisible]").is(":checked")){
									tempConfig.invisible = true;
								}
								if(currentObj.find("[name=innotin]").is(":checked")){
									tempConfig.innotin = true;
								}
								if(currentObj.find("[name=searchtype]")){
									tempConfig.searchtype = currentObj.find("[name=searchtype]").val();
								}
								if (tempConfig["fieldType"] === 'lookup' || tempConfig["fieldType"] === 'select') {
									tempConfig["lookupquery"] = currentObj.find("[name=lookup]").val();
									tempConfig["defaultvalue"] = currentObj.find("[name=defaultvalue]").select2('data');
									tempConfig.isSingle = currentObj.find("[name=lookupdiv]:eq(1)").find('select').val();
								} else if (tempConfig["fieldType"] === 'DATE' || tempConfig["fieldType"] === 'dynamicDate' || tempConfig["fieldType"] === 'NUM') {
									tempConfig["from-defaultvalue"] = currentObj.find("[name=from-defaultvalue]").val();
									tempConfig["to-defaultvalue"] = currentObj.find("[name=to-defaultvalue]").val();
								} else {
									tempConfig["defaultvalue"] = currentObj.find("[name=defaultvalue]").val();
								}
								filterConfig.push(tempConfig);
							}
						});
		return filterConfig;
	};
	function saveProfile(){
		var config = getConfig();
		var filters = getFilters();
		var request = {
				"source" : THIS.sourceType,
				"database" : THIS.database,
				"table" : THIS.table,
				//"fact" : JSON.stringify(fact),
				//"customfacts" : JSON.stringify(THIS.customFacts),
				"config" : JSON.stringify(config),
				"filters" : JSON.stringify(filters),
				//"summary" : JSON.stringify(summary),
				//"remarks" : remarks,
				"displayname" : THIS.ProfileName || $("#profilename").val(),
				"type" : "ngram",
				"id" :THIS.id
			};
			if ($(".notification-holder a") && $(".notification-holder a") !== null &&$(".notification-holder a").length > 0){
				request.notificationFilter = $(".notification-holder a").text();
			}
			enableLoading();
			callAjaxService("saveProfile", callBackSaveProfile,callBackFailure, request, "POST");
	}
	function callBackSaveProfile(response){
		if ((response && response.isException)|| (response.message && response.message ==="failure")) {
			showNotification("error", response.customMessage||response.message);
			return;
		}
		if (response !== undefined && response !== null) {
			disableLoading();
			$('#myModalSave').modal('hide');
			showNotification("success", "Profile saved successfully..");
			THIS.id = response;
		}
	}
	var myProfileSelect = function(obj) {
		var id = $(obj).data("refid");
		var request = {
			"id" : id,
			"viewId":-1
		};
		enableLoading();
		$("#cloneProfile").show();
		callAjaxService("getProfileConfig", callBackGetProfileConfig,
				callBackFailure, request, "POST");
	};
	function callBackGetProfileConfig(response){
		$("#myProfiles").hide();
		$("#nGramInput").show();
		disableLoading();
		var data = JSON.parse(response["config"]);
		THIS.sourceType = data[0]["source"];
		THIS.database = data[0]["database"];
		THIS.table = data[0]["configtable"];
		THIS.ProfileName = data[0]["displayname"];
		THIS.ProfileDesc = data[0]["remarks"];
		THIS.id = data[0]["id"];
		if(data[0]["config"] !==""){
			data[0]["config"] = data[0]["config"].replace(/@/g, "'");
			THIS.config = JSON.parse(data[0]["config"]);
		}	
		if (data[0]["filters"] && $.trim(data[0]["filters"]) !== "")
			THIS.filters = JSON.parse(data[0]["filters"].replace(/@/g, "'"));		
		tableSelect(THIS.database,true);
	}
	/*$("#goNGram").unbind("click").bind("click", function() {
	var nGramValues = [ {
		"id" : "1",
		"text" : "Unigram"
	}, {
		"id" : "2",
		"text" : "Bigram"
	}, {
		"id" : "3",
		"text" : "trigram"
	}, {
		"id" : "4",
		"text" : "4-gram"
	}, {
		"id" : "5",
		"text" : "5-gram"
	}];

	$('#ngram').select2({
		placeholder : "Choose nGram",
		data : nGramValues
	});
	$("#goNGram").unbind("click").bind("click", function() {
		enableLoading();
		var whereCondition =  $("#whereClause").val();
		THIS.nGram = $("#ngram").select2('val');
		
		var request = {};
		var config = {
				"gram":THIS.nGram,
				"source" : THIS.sourceType,
				"database" : THIS.database,
				"table" : THIS.table,
				"field": $("#field").select2("val"),
				"whereCondition":whereCondition
		};
		request.config = JSON.stringify(config);
		callAjaxService("nGramAnalyserPost",nGramAnalyserCallBackSucess,callBackFailure,request,"POST");
	});*/
	this.dynamicProfiling = function(config){
		
		disableLoading();
		THIS.config = JSON.parse(config.config);
		THIS.sourceType = config.source;
		THIS.database = config.database;
		THIS.table = config.configtable;
		if (config.filters && $.trim(config.filters) !== "" && config.filters.length>0 && config.filters!=="[]") {
			THIS.filters = JSON.parse(config.filters.replace(/@/g, "'"));
		}
		loadScript("js/plugin/d3.layout.cloud.js",function(){
			getNGramAnalyser(THIS.filters);
		});
		$("#nGramAnalyser").show();
		$("#nGrambutton").html($("#nGramSelect").find("li[value='"+THIS.config.ngramType+"']").text()+' <i class="fa fa-caret-down" ></i>');
		$("#nGrambutton").attr({'value':THIS.config.ngramType});		
		if(THIS.filters && THIS.filters.length>0){
			var invisibleFilterLength = THIS.filters.filter(function(a){return a.invisible;}).length;
			if(THIS.filters.length !== invisibleFilterLength){
				$("#nGramFilter").show();
				constructFilters(THIS.filters);
			}				
		}
			
	};
	var constructFilters = function(config,filterContentId) {
		filterContentId = filterContentId || "divnGramFilter";		
		var rowDiv, mainSection, rowIndex = 0,sectionClassName = "col col-4 col-sm-4",isInline;
		//if (THIS.additionalconfig && THIS.additionalconfig.isFilterInline) {
			sectionClassName = "col col-3 col-sm-3";
			isInline = true;
		//}
		if (config && config.length > 0) {
			$('#'+filterContentId).html('');
			rowDiv = $('<div/>').addClass('row smart-form'+(isInline?' col-sm-11':''));
			$('#'+filterContentId).append(rowDiv);
			$.each(config,function(index, obj) {
//								if (rowIndex % 2 === 0) {
//									rowDiv = $('<div/>').addClass('row');
//									$('#'+filterContentId).append(rowDiv);
//								}
								mainSection = $('<section/>').addClass(sectionClassName).attr({
									'key' : index
								}).data({
									"lookup-query" : obj.lookupquery,
									"field-name" : obj.fieldName,
									"display-name" : obj.displayName,
									"field-type" : obj.fieldType,
									"isSingle":obj.isSingle,
									"isMandatory":obj.isMandatory,
									"invisible":obj.invisible,
									"searchtype":obj.searchtype,
									"default-value" : obj.defaultvalue
								});
								if(obj.invisible){
									mainSection.hide();
								}
								rowDiv.append(mainSection.append($('<div/>').addClass('note')
																.append($('<strong/>').html(obj.displayName+ ' :')))
												.append($('<label/>').addClass('input width-100per')
																.append(obj.innotin?$("<select/>").attr({"name":"searchtype"}).css({"padding-left":"0px"}).addClass("form-control col col-3 ")
																		.append($("<option/>").html("In").attr({"value":"in"}))
																		.append($("<option/>").attr({"value":"not in"}).html("Not In"))
																		.append($("<option/>").html("Like").attr("value","like"))
																		.append($("<option/>").html("Not Like").attr("value","not like")):'')
																.append(obj.fieldType === 'TEXT' ? $('<input/>').val(obj.defaultvalue.replace(/'/g,""))
																				.attr({
																							'placeholder' : 'Enter '
																									+ obj.displayName
																						}).addClass('form-control'+(obj.innotin?' width-75per':''))
																				: (obj.fieldType === 'lookup' ||obj.fieldType === 'select')? $('<div/>').addClass((obj.innotin?'col-md-9':'col-md-12')).append($('<input/>')
																						.addClass('input'+isInline?' chart-bar ':' width-100per')): $('<div/>')
																								.addClass(obj.innotin?'row':'')
																								.append($('<div/>').addClass(obj.innotin?'col-sm-4':'col-sm-6')
//																												.append($('<div/>').addClass('note col-sm-3')
//																														.append($('<strong/>').html('From')))
																												.append($('<label/>').addClass('input')
																														.append($('<input/>').attr({
																																						"name" : "from-defaultvalue",
																																						"placeholder":"From"
																																					}).css({"width":"95%"})
																																				.addClass('form-control')
																																				.val(obj["from-defaultvalue"]))))
																								.append($('<div/>').addClass(obj.innotin?'col-sm-4':'col-sm-6')
																												.append($('<label/>').addClass('input')
																																.append($('<input/>').attr({
																																							"name" : "to-defaultvalue",
																																							"placeholder":"To"
																																						}).css({"width":"95%"})
																																				.addClass('form-control').val(obj["to-defaultvalue"])))))));
								if(obj.innotin){
									mainSection.find("[name='innotin']").val("notin");									
								}
								if(obj.searchtype){
									mainSection.find("[name='searchtype']").val(obj.searchtype);									
								}
								if (obj.fieldType === 'DATE' || obj.fieldType === 'dynamicDate') {
									if(obj.fieldType === 'dynamicDate'){
										mainSection.find('input:eq(0)').val(eval(obj["from-defaultvalue"]));
										mainSection.find('input:eq(1)').val(eval(obj["to-defaultvalue"]));
									}
									mainSection.find('input:eq(0)')
											.datepicker({
														defaultDate : "+1w",
														changeMonth : true,
														onClose : function(selectedDate) 
															{
																$('div[key="'+ index+ '"]').find('input:eq(1)').datepicker("option","minDate",selectedDate);
															}
													});
									mainSection.find('input:eq(1)')
											.datepicker({
														defaultDate : "+1w",
														changeMonth : true,
														onClose : function(selectedDate) 
															{
															$('div[key="'+ index+ '"]').find('input:eq(0)')
																	.datepicker("option","maxDate",selectedDate);
															}
													});
								} else if (obj.fieldType === 'lookup' || obj.fieldType === 'select') {
									function formatResult(resp) {
										return resp.id + (resp.name?' - ' + resp.name:'');
									}

									function formatSelection(resp) {
										return resp.id + (resp.name?' - ' + resp.name:'');
									}
									if(obj.fieldType === 'select'){
										var selectdata =  obj.lookupquery;
										if(selectdata){
											selectdata = Jsonparse(selectdata);
											if(!selectdata || Object.keys(selectdata).length === 0){
												selectdata = [];
												var values =  obj.lookupquery.split(',');
												if(values && values.length>0){
													for(var i=0;i<values.length;i++){
														selectdata.push({id:values[i],text:values[i]});	
													}								
												}
											}
										}
										mainSection.find("input").select2({
											allowClear: obj.isSingle,
											placeholder : "Enter " + obj.displayName,
											multiple:!obj.isSingle,
											data : selectdata || []
										});
									}else{
										mainSection.find('input')
										.select2({
													placeholder : "Enter "
															+ obj.displayName,
													minimumInputLength : 1,
													multiple : !obj.isSingle,
													allowClear: obj.isSingle,
													width: 'resolve',
													ajax : {
														url : serviceCallMethodNames["suggestionsLookUp"].url,
														dataType : 'json',
														type : 'GET',
														data : function(
																term) {
															return {
																q : term,
																source : THIS.sourceType,
																database : THIS.database,
																lookupQuery : obj.lookupquery
															};
														},
														results : function(
																data) {
															return {
																results : data
															};
														}
													},
													formatResult : formatResult,
													formatSelection : formatSelection,
													dropdownCsclass : "bigdrop",
													initSelection : function(
															element,
															callback) {
														callback($
																.map(element.val().split(','),
																		function(id) {id = id.split(":");
																			return {
																				id : id[0],
																				name : id[1]
																			};
																		}));
													},
													escapeMarkup : function(m) {
														return m;
													}
												});
									}
									
									if (obj.defaultvalue) {
										mainSection.find('input').select2('data', obj.defaultvalue, true);
									}
								}
								rowIndex++;
							});
		}
		$('[name="applynGramFilter"]').unbind('click').bind('click', function() {
			var filters = getFiltersFromProfile();
			if(filters && filters.length>0){
				enableLoading();
				getNGramAnalyser(filters);//callService(THIS.config, filterConfig, THIS.Summary);
				
			}
		});
	};
	var getFiltersFromProfile = function() {
		var filterContentId =  "divnGramFilter";
		var filterConfig = [];
		$('#'+filterContentId +' section').each(
				function() {
					var currentObj = $(this),isDefaultValue;
					var tempConfig = {
						"fieldName" : currentObj.data("field-name"),
						"displayName" : currentObj.data("display-name"),
						"fieldType" : currentObj.data("field-type")
					};
					if(currentObj.find("[name='searchtype']").length > 0){
						tempConfig.searchtype = currentObj.find("[name='searchtype']").val();
					}else{
						tempConfig.searchtype = " IN";
					}
					if (tempConfig["fieldType"] === 'lookup' || tempConfig["fieldType"] === 'select') {
						tempConfig["lookupquery"] = currentObj.data("lookup-query");
						tempConfig["defaultvalue"] = currentObj.find("input").select2('data');
						tempConfig.isSingle = currentObj.data("isSingle");
						if(tempConfig.defaultvalue && (tempConfig.defaultvalue.length > 0 || Object.keys(tempConfig.defaultvalue).length > 0)){
							isDefaultValue = true;
						}
					} else if (tempConfig["fieldType"] === 'DATE' || tempConfig["fieldType"] === 'dynamicDate' || tempConfig["fieldType"] === 'NUM') {
						tempConfig["from-defaultvalue"] = currentObj.find("[name^=from-defaultvalue]").val();
						tempConfig["to-defaultvalue"] = currentObj.find("[name^=to-defaultvalue]").val();
						if(tempConfig["from-defaultvalue"] && tempConfig["to-defaultvalue"]){
							isDefaultValue = true;
						}
					} else {
						tempConfig["defaultvalue"] = currentObj.find("input").val();
						if(tempConfig.defaultvalue){
							isDefaultValue = true;
						}
					}
					$(currentObj).find("em").remove();
					if(currentObj.data("isMandatory") && !isDefaultValue){
						$(currentObj).find("label:eq(1)").append($("<em/>").addClass("invalid").html(currentObj.data("display-name")+" can not be empty."));						
						filterConfig = false;
						return false;
					}
					filterConfig.push(tempConfig);
				});
		//setTimeout(function () {$("#"+filterContentId).find("em").remove()}, 2000);
		return filterConfig;
	};
	function getFilterQuery(filterConfig){
		if(!filterConfig){
			filterConfig = getFilters();
		}		
		var filterQuery = " where 1=1";
		if(filterConfig && filterConfig.length>0){
			for ( var i = 0; i < filterConfig.length; i++) {
				var searchtype = filterConfig[i].searchtype || " IN ",
				fromDateOperator=">=",toDateOperator="<=";
				if($.trim(searchtype.toLowerCase()) === "not in"){
					fromDateOperator="<=";
					toDateOperator=">=";
				}
				if (filterConfig[i]["fieldType"] === 'lookup' || filterConfig[i]["fieldType"] === 'select') {
					var lookupIds = "";
					var lookupData = filterConfig[i]["defaultvalue"];
					if (!lookupData || lookupData.length === 0)
						continue;
					if(lookupData && filterConfig[i].isSingle){
						if(lookupData.id !== '""' && lookupData.id !== 'null'){
							if($.trim(searchtype.toLowerCase()) === "in" || $.trim(searchtype.toLowerCase()) === "not in"){
								lookupIds = "'" + lookupData.id + "'";
								filterQuery += " AND (" + filterConfig[i]["fieldName"]
								+" "+ searchtype+ " (" + lookupIds + ")";
							}else{
								lookupIds = lookupData.id;
								filterQuery+= filterConfig[i]["fieldName"]+" "+searchtype + " '%" + lookupIds + "%'";
							}
						}
					} else if (lookupData && lookupData.length > 0) {
						if($.trim(searchtype.toLowerCase()) === "in" || $.trim(searchtype.toLowerCase()) === "not in"){
							for ( var x = 0; x < lookupData.length; x++) {
								if(lookupData[x].id !== '""' && lookupData[x].id !== 'null'){
									if (x === 0 )
										lookupIds = "'" + lookupData[x].id + "'";
									else
										lookupIds += ",'" + lookupData[x].id + "'";
								}
							}	
							if(lookupIds){
								filterQuery += " AND (" + filterConfig[i]["fieldName"]
								+" "+ searchtype+ " (" + lookupIds + ")";								
							}
						}else{
							filterQuery += " AND (";
							var operator = $.trim(searchtype.toLowerCase()) === 'like'?' OR ':' AND ';
							for(var x=0;x<lookupData.length;x++){
								if(x === 0)
									filterQuery+= filterConfig[i]["fieldName"]+" "+searchtype + " '%" + lookupData[x].id + "%'";
								else
									filterQuery+= operator +filterConfig[i]["fieldName"]+" "+searchtype + " '%" + lookupData[x].id + "%'";
							}
						}
					}
					
				if ((lookupIds === "" || lookupIds === "''" || lookupIds === '""' || lookupIds === 'null') && $.trim(searchtype.toLowerCase()) === "in")
					filterQuery += " AND (" + filterConfig[i]["fieldName"] + " is null OR " + filterConfig[i]["fieldName"] + " = '')";
				else if ((lookupIds === "" || lookupIds === "''" || lookupIds === '""' || lookupIds === 'null') && $.trim(searchtype.toLowerCase()) === "not in")
					filterQuery += " AND (" + filterConfig[i]["fieldName"] +" is not null OR " + filterConfig[i]["fieldName"] + " <> '')";
				else if ((lookupIds === "" || lookupIds === "''" || lookupIds === '""' || lookupIds === 'null') && $.trim(searchtype.toLowerCase()) === "like")
					filterQuery += " AND (" + filterConfig[i]["fieldName"] +" like '%null%' )";
				else if ((lookupIds === "" || lookupIds === "''" || lookupIds === '""' || lookupIds === 'null') && $.trim(searchtype.toLowerCase()) === "not like")
					filterQuery += " AND (" + filterConfig[i]["fieldName"] +" not like '%null% )";
				else
					filterQuery += " )";
				} else if (filterConfig[i]["fieldType"] === 'NUM') {
					if (filterConfig[i]["from-defaultvalue"]
							&& $.trim(filterConfig[i]["from-defaultvalue"]) !== "")
						filterQuery += " AND " + filterConfig[i]["fieldName"]
								+ " >= " + filterConfig[i]["from-defaultvalue"];
					if (filterConfig[i]["to-defaultvalue"]
							&& $.trim(filterConfig[i]["to-defaultvalue"]) !== "")
						filterQuery += " AND " + filterConfig[i]["fieldName"]
								+ " <= " + filterConfig[i]["to-defaultvalue"];
				} else if (filterConfig[i]["fieldType"] === 'DATE' || filterConfig[i]["fieldType"] === 'dynamicDate') {
					var fromDate = filterConfig[i]["from-defaultvalue"];
					var toDate = filterConfig[i]["to-defaultvalue"];
					if(filterConfig[i]["fieldType"] === 'dynamicDate' && isNaN(Date.parse(filterConfig[i]["from-defaultvalue"]))){
						fromDate = eval(filterConfig[i]["from-defaultvalue"]);
						toDate = eval(filterConfig[i]["to-defaultvalue"]);
					}
					if (THIS.sourceType ==="postgreSQL"){
						if (filterConfig[i]["from-defaultvalue"]
						&& $.trim(filterConfig[i]["from-defaultvalue"]) !== "")
							filterQuery += " AND "
									+ filterConfig[i]["fieldName"]
									+  fromDateOperator+" to_date('"
									+ fromDate
									+ "', 'MM/dd/yyyy') ";
						if (filterConfig[i]["to-defaultvalue"]
								&& $.trim(filterConfig[i]["to-defaultvalue"]) !== "")
							filterQuery += " AND "
									+ filterConfig[i]["fieldName"]
									+ toDateOperator+" to_date('"
									+ toDate
									+ "', 'MM/dd/yyyy')+1";
					}else{
						if (filterConfig[i]["from-defaultvalue"]
								&& $.trim(filterConfig[i]["from-defaultvalue"]) !== "")
							filterQuery += " AND unix_timestamp("
									+ filterConfig[i]["fieldName"]
									+ ", 'MM/dd/yyyy') "+fromDateOperator+" unix_timestamp('"
									+ fromDate
									+ "', 'MM/dd/yyyy') ";
						if (filterConfig[i]["to-defaultvalue"]
								&& $.trim(filterConfig[i]["to-defaultvalue"]) !== "")
							filterQuery += " AND unix_timestamp("
									+ filterConfig[i]["fieldName"]
									+ ", 'MM/dd/yyyy') "+toDateOperator+" unix_timestamp('"
									+ toDate
									+ "', 'MM/dd/yyyy')";
					}
				} else {
					if (filterConfig[i]["defaultvalue"] && $.trim(filterConfig[i]["defaultvalue"]) !== ""){
						if($.trim(searchtype.toLowerCase()) === "in" || $.trim(searchtype.toLowerCase()) === "not in"){
							filterQuery += " AND " + filterConfig[i]["fieldName"]
							+ " "+searchtype+ " ('" + filterConfig[i]["defaultvalue"]
							+ "')";
						}else{
							filterQuery += " AND " + filterConfig[i]["fieldName"]
							+ " "+searchtype+ " '%" + filterConfig[i]["defaultvalue"]
							+ "%'";
						}
						
					}
						
				}
			}
		}
		return filterQuery;
	}
	function getNGramAnalyser(filters){
		var request = {},whereCondition= getFilterQuery(filters);
		var field,ngramType;
		if($("#field").length>0){
			field = $("#field").select2("val");
		}else{
			field = THIS.config.field;
		}
		if($("#gramType").length>0){
			ngramType = $("#gramType").select2("val");
		}else if($("#nGrambutton").length >0){
			ngramType = $("#nGrambutton").val();
		}else{
			ngramType = THIS.config.ngramType;
		}
		var config = {
				"gram":ngramType,
				"source" : THIS.sourceType,
				"database" : THIS.database,
				"table" : THIS.table,
				"field": field,
				"whereCondition":whereCondition
		};
		request.config = JSON.stringify(config);
		enableLoading();
		callAjaxService("nGramAnalyserPost",nGramAnalyserCallBackSucess,callBackFailure,request,"POST");
	}
	$("#submitButton").unbind("click").bind("click", function(){nGramAnalyser.Load();});
	$("#nGramSelect li").unbind("click").bind("click", function(){
		$("#nGrambutton").html($(this).text()+' <i class="fa fa-caret-down" ></i>');$("#nGrambutton").attr({'value':$(this).val()});
		var filters = getFiltersFromProfile();
		getNGramAnalyser(filters);
	});
	$("#nGramform").validate({

		// Rules for form validation
		rules : {
			nGramInput : {
				required : true
			}
		},

		// Messages for form validation
		messages : {
			nGramInput : {
				required : 'Input String cannot be empty..'
			}
		},

		// Do not change code below
		errorPlacement : function(error, element) {
			error.insertAfter(element.parent());
		}
	});
	$("#frmSaveProfile").validate({
		rules : {
			profilename : {
				required : true
			}
		},
		messages : {
			profilename : {
				required : 'Profile name is required'
			}
		},
		errorPlacement : function(error, element) {
			error.insertAfter(element.parent());
		}
	});
	$("#frmProfileConfig").validate({
		rules : {
			field : {
				required : true
			},
			gramType : {
				required : true
			}
		},
		messages : {
			field : {
				required : 'Please select field'
			},
			gramType : {
				required : 'Please select gram type'
			}
		},
		errorPlacement : function(error, element) {
			error.appendTo(element.parent());
		}
	});
	$("#frmConf").validate({

		rules : {
			database : {
				required : true
			},
			table : {
				required : true
			},
			ngram : {
				required : true
			}
		},

		// Messages for form validation
		messages : {
			database : {
				required : 'Please select database'
			},
			table : {
				required : 'Please select table'
			},
			ngram : {
				required : 'Please select nGram'
			}
		},

		// Do not change code below
		errorPlacement : function(error, element) {
			error.appendTo(element.parent());
		}
	});
	var sourceSelect = function(obj) {
		THIS.sourceType = $(obj).data("source");
		var request;
		if (THIS.sourceType === "hive" || THIS.sourceType === "postgreSQL") {
			$("#selectSource").hide(300);
			$("#selectTable").show(300);
			request = {
				"source" : THIS.sourceType
			};
			enableLoading();
			callAjaxService("getDatabaseList", callBackGetDatabaseList,
					callBackFailure, request, "POST");
		} else if (THIS.sourceType === "myProfiles") {
			request = {
				"type" : "ngram"
			};
			enableLoading();
			callAjaxService("getMyProfiles", callBackGetMyProfiles,
					callBackFailure, request, "POST");
		}
	};
	var callBackGetMyProfiles = function(response) {
		disableLoading();
		
		$("#selectSource").hide(300);
		$("#myProfiles").show(300);
		if (response && response.isException) {
			showNotification("error", response.customMessage);
			return;
		}
		if (response !== undefined && response !== null) {
			var data = response;
			$("#myProfileList").empty();
			for ( var i = 0; i < data.length; i++) {
				var publishStatus = "Publish";
				var btnClass = "btn-outline";
				var actionId = 0;
				if (data[i].profilekey && data[i].profilekey !== null
						&& data[i].profilekey !== "") {
					publishStatus = "Published";
					btnClass = "btn-outline-success";
					actionId = data[i].actionid;
				}
				$("#myProfileList").append($("<li/>").addClass("list-group-item").data({"refid" : data[i].id,"actionid" : actionId})
										.append($("<a/>").append($("<span/>").addClass("text-left col-md-2 col-xs-2 col-sm-2 col-lg-2").html(data[i].displayname))
														.append($("<span/>").addClass("text-left col-md-2 col-xs-2 col-sm-2 col-lg-2").html(data[i].profilekey))
														.append($("<span/>").addClass("text-left col-md-1 col-xs-1 col-sm-1 col-lg-1").html(data[i].source))
														.append($("<span/>").addClass("text-left col-md-1 col-xs-1 col-sm-1 col-lg-1").html(data[i].database))
														.append($("<span/>").addClass("text-left col-md-2 col-xs-2 col-sm-2 col-lg-2").html(data[i].configtable))
														.append($("<span/>").addClass("badge text-center col-md-2 col-xs-2 col-sm-2 col-lg-2").html(data[i].createdon))
														.append($("<span/>").addClass("text-right col-md-1 col-xs-1 col-sm-1 col-lg-1")
																		.html($("<button/>").addClass(btnClass+ " btn")
																						.attr({
																									"href" : "void(0)",
																									'data-target' : '#profilePublishModal',
																									'data-toggle' : 'modal',
																									'data-refid' : data[i].id,
																									'data-displayname' : data[i].displayname,
																									'data-actionid' : actionId,
																									'data-action' : data[i].profilekey
																								})
																						.html(publishStatus)))
														.append($("<span/>").addClass("text-right col-md-1 col-xs-1 col-sm-1 col-lg-1")
																		.html($("<button/>").addClass("btn-outline-warning btn")
																						.attr({
																									'name':"history",
																									'data-refid' : data[i].id
																								}).html("History")))
										/*
										 * <button class="btn-outline btn
										 * margin-right-2"
										 * href="javascript:void(0);">spine</button>
										 */
										));
			}
			$("#myProfileList li").unbind("click").bind("click", function() {
				THIS.dataTableCounter = 0;
				THIS.facts = [];
				THIS.factsConfig = [];
				THIS.customFacts = [];
				THIS.dependentFacts = [];
				myProfileSelect(this);
			});
			$("[data-target^=#profilePublishModal]").unbind("click").bind("click",
					function(e) {
						e.stopPropagation();
						previewPublishProfile(this);
					});
			templates.getFilterConfig(null,function(){});			
		}
	};
	var callBackGetDatabaseList = function(response) {
		disableLoading();
		if (response && response.isException) {
			showNotification("error", response.customMessage);
			return;
		}
		if (response !== undefined && response !== null) {
			var databaseList = [];
			for ( var i = 0; i < response.length; i++) {
				databaseList.push({
					"id" : response[i],
					"text" : response[i]
				});
			}
			$("#database").select2({
				placeholder : "Choose Database",
				data : databaseList
			}).on("change", function(e) {
				tableSelect(e.val);
			});
			$("#table").select2({
				placeholder : "Choose Table",
				data : []
			});
			$("#field").select2({
				placeholder : "Choose Field",
				data : []
			});
		}
	};
	var tableSelect = function(database,isEditProfile) {
		THIS.database = database;
		var request = {
			"source" : THIS.sourceType,
			"database" : database
		};
		enableLoading();
		callAjaxService("getTableList", function(response){callBackGetTableList(response,isEditProfile);}, callBackFailure,
				request, "POST");
	};
	var callBackGetTableList = function(response,isEditProfile) {
		disableLoading();
		if (response && response.isException) {
			showNotification("error", response.customMessage);
			return;
		}
		if (response !== undefined && response !== null) {
			THIS.tableList = [];
			for ( var i = 0; i < response.length; i++) {
				THIS.tableList.push({
					"id" : response[i],
					"text" : response[i]
				});
			}
			$("#table").select2({
				placeholder : "Choose table",
				data : THIS.tableList
			}).on("change", function(e) {
				proceedColumn(e.val);
			}).select2("val", THIS.datatable);
		}
		if (isEditProfile)
			proceedColumn(THIS.table,isEditProfile);
	};
	var proceedColumn = function(table,isEditProfile) {
		THIS.table = table;
		var request = {
			"source" : THIS.sourceType,
			"database" : THIS.database,
			"table" : THIS.table
		};
		$("#sourceTableDesc").html(THIS.ProfileName+" : "+THIS.sourceType+" - "+THIS.database+" - "+THIS.table);
		enableLoading();
		callAjaxService("getColumnList", function(response){callBackGetColumnList(response,isEditProfile);},callBackFailure, request, "POST");
	};
	var callBackGetColumnList = function(response,isEditProfile) {
		disableLoading();
		if (response && response.isException) {
			showNotification("error", response.customMessage);
			return;
		}
		if (response !== undefined && response !== null) {
			THIS.fieldList = [];
			for ( var i = 0; i < response.length; i++) {
				THIS.fieldList.push({
					"id" : response[i].columnName,
					"text" : response[i].columnName
				});
			}
			$("#field").select2({
				placeholder : "Choose Field",
				data : THIS.fieldList
			});
		}
		if(isEditProfile){
			if(THIS.filters && THIS.filters.length>0){
				$("#filters").empty();
				for(var i=0;i<THIS.filters.length;i++){
					filterTemplate = templates.getFilterConfig.template(null);
					$("#filters").append(filterTemplate);
					procesFilter(i,THIS.filters[i]);
				}
			}else{
				procesFilter(0);
			}
			if(THIS.config){
				$("#field").select2("val",THIS.config.field);
				$("#gramType").select2("val",THIS.config.ngramType);
			}
			getNGramAnalyser();
		}
		
	};
	this.Load = function(){
		if (!$("#nGramform").valid())
			return;
		enableLoading();
		var nGramInput =  $("#nGramInput").val();
		THIS.nGram = $("#nGrambutton").attr('value');
		if (THIS.nGram  === "0"){
			showNotification("error","Select Gram.");
			return;
		}
		var request = {};
		var config = {
				"inputString":nGramInput,
				"gram":THIS.nGram 
		};
		request.config = JSON.stringify(config);
		callAjaxService("nGramAnalyserPost",nGramAnalyserCallBackSucess,callBackFailure,request,"POST");
	};
	
	var nGramAnalyserCallBackSucess = function(response){
		disableLoading();
		if (response && response.isException){
			showNotification("error",response.customMessage);
			return;
		}
		$("#myModalWhere").modal("hide");
		$("#nGramInput").show();
		$("#fieldsetCloud").removeClass("hide");
		$("#cloud").empty();
		var width = 1000;
		var height = 600;
		var svg = d3.select('#cloud').append('svg')
		.attr('width', width)
		.attr('height', height)
		.attr('id', 'cloudSVG')
		.append('g')
		.attr('transform', 'translate('+width/2+', '+height/2+')');
		function drawCloud(words) {
			var vis = svg.selectAll('text').data(words);

			vis.enter().append('text')
				.style('font-size', function(d) { return d.size + 'px'; })
				.style('font-family', function(d) { return d.font; })
				.style('fill', function(d, i) { return colors(i); })
				.attr('text-anchor', 'middle')
				.attr('class', "icon-hover")
				.attr('onclick',function(d){return "nGramAnalyser.gramClick('"+d.text+"')";})
				.attr('transform', function(d) {
				  return 'translate(' + [d.x, d.y] + ')rotate(' + d.rotate + ')';
				})
				.html(function(d) { return "<title>"+d.text+": "+Math.round(d.size/THIS.minFontSize)+"</title>"+ d.text; });
		}
		var data = response["nGramAnalyser"];
		var totalsize = 1000;
		for (var i=0;i<data.length;i++){
			
			var temp =1250/(data[i].size*data[i].text.length);
			if (totalsize > temp)
				totalsize = temp;
		}
		
		THIS.minFontSize =totalsize;
		var typeFace = 'Gorditas';
		var colors = d3.scale.category20b();
		d3.layout.cloud()
		.size([width, height])
		.words(data)
		.rotate(function() { return ~~(Math.random()*2) * 0;}) // 0 or 90deg
		.font(typeFace)
		.fontSize(function(d) { return d.size * THIS.minFontSize; })
		.on('end', drawCloud)
		.start();
		
		if ($(".onoffswitch-checkbox").is(":checked")){
			panZoomForward = PanZoomsvg('treeviewidforward');
			if (panZoomForward && panZoomForward != null){
				panZoomForward.enableZoom();
				panZoomForward.enablePan();
			}
		}
	};
	this.enablePanZoom = function(){
		if ($(".onoffswitch-checkbox").is(":checked")){
			panZoomForward = PanZoomsvg('cloudSVG');
			if (panZoomForward && panZoomForward != null){
				panZoomForward.enableZoom();
				panZoomForward.enablePan();
			}
		} else{
			if (panZoomForward && panZoomForward != null){
				panZoomForward.disableZoom();
				panZoomForward.disablePan();
			}
		}
	};
	this.gramClick = function(text){
		searchKey = text.replace(" ","+");
	};
	var previewPublishProfile = function(obj) {
		$("#userEnabled").removeAttr("checked");
		$("#smart-form-register input").removeAttr("readonly");
		if ($(obj).html() !== "Publish") {
			publishedActiondetails($(obj).attr("data-action"), $(obj).attr("data-actionid"), $(obj).data("refid"));
		}
		else {
			//$("#profilePublishModal").modal("show");
			var publishDataObj = {};
			publishDataObj.action = $(obj).data("displayname").replace(/ /g, '');
			publishDataObj.displayname = $(obj).data("displayname");
			publishDataObj.refid = $(obj).data("refid");
			publishDataObj.source = "";
			/*$("#frmPublishNgram").find("[name='action']").val($(obj).data("displayname").replace(/ /g, ''));
			$("#frmPublishNgram").find("[name='displayname']").val($(obj).data("displayname"));*/
			
			
			var callBackPublishProfile = function(data) {
				$(obj).html("Published").removeClass("btn-outline").addClass(
						"btn-outline-success");
				$(obj).parent().parent().find(".text-left.col-md-2 col-xs-2 col-sm-2 col-lg-2").text(
						data.action);
				$(obj).parent().parent().find(".text-right col-md-1 col-xs-1 col-sm-1 col-lg-1").attr(
						"data-action", data.action);
			};

			$("#smart-form-register input").val("");
			$("#frmSankeyFilter em").remove();
			$(".state-error").removeClass("state-error");

			templates.managePublishing(publishDataObj, "CREATE",callBackPublishProfile);
		}
	};

	var publishedActiondetails = function(action, actionId, refid) {
		enableLoading();

		var callbackSucessGetAction = function(response) {
			if (response && response.isException) {
				showNotification("error", response.customMessage);
				return;
			} else {
				if (response === null) {
					disableLoading();
					alert("Selected Profile is Published as Tab");
					return;
				}

				response.refid = refid;
				response.actionId = Number(actionId);
				templates.managePublishing(response, "VIEW", undefined);
			}
		};

		var request = {
			"action" : action
		};
		callAjaxService("getAction", function(response) {
			callbackSucessGetAction(response);
		}, callBackFailure, request, "POST");
	};
	$(".col-sm-3", "#selectSource").unbind("click").bind("click", function() {
		sourceSelect(this);
	});
};
var nGramAnalyser;
var nGramAnalyserInstance = function() {
	nGramAnalyser = new nGramAnalyserObject();
};
nGramAnalyserInstance();