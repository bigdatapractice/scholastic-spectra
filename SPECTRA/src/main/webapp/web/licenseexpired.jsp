<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html lang="en-us">
<head>
<meta charset="utf-8">
<!--<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">-->

<title>Login</title>


<!-- Basic Styles -->
<link rel="stylesheet" type="text/css" media="screen"
	href="css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" media="screen"
	href="css/font-awesome.min.css">
<link rel="stylesheet" type="text/css" media="screen"
	href="css/jquery-confirm.css">
<!-- SmartAdmin Styles : Please note (smartadmin-production.css) was created using LESS variables -->
<link rel="stylesheet" type="text/css" media="screen"
	href="css/smartadmin-production_unminified.css?v=${version}">
<link rel="stylesheet" type="text/css" media="screen"
	href="css/smartadmin-skins.css?v=${version}">

<!-- SmartAdmin RTL Support is under construction
			<link rel="stylesheet" type="text/css" media="screen" href="css/smartadmin-rtl.css"> -->

<!-- Demo purpose only: goes with demo.js, you can delete this css when designing your own WebApp
		<link rel="stylesheet" type="text/css" media="screen" href="css/demo.css"> -->

<!-- FAVICONS -->
<link rel="shortcut icon" href="img/favicon/sea_favicon.ico"
	type="image/x-icon">
<link id="loginfavicon" rel="icon" href="img/favicon/sea_favicon.ico"
	type="image/x-icon">

<!-- GOOGLE FONT -->
<link rel="stylesheet"
	href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,700italic,300,400,700">

</head>
<body >
<div class="container">
  <div class="jumbotron" style="margin-top: 100px; background-color: #585858;">
  	<div class="row">
	    <h2 class="text-center">
  			<i class="fa fa-fw fa-2x fa-exclamation-circle text-danger" aria-hidden="true"></i>
	     	<span style="color: #fff;">Your License is Invalid/expired. Please contact system administrator !!.</span>
	    </h2>
    </div>
  </div>  
</div>
</body>
</html>