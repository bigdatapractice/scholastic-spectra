package com.relevance.ppa.dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.relevance.prism.data.FlowData;
import com.relevance.prism.data.GoodsHistoryList;
import com.relevance.prism.data.MaterialReportList;
import com.relevance.prism.dao.BaseDao;
import com.relevance.prism.dao.E2emfDao;
import com.relevance.prism.data.E2emfBusinessObject;
import com.relevance.prism.data.Master;
import com.relevance.prism.data.MasterItem;
import com.relevance.prism.util.AppException;
import com.relevance.prism.util.E2emfConstants;
import com.relevance.prism.util.PrismDbUtil;
import com.relevance.prism.util.Log;
import com.relevance.prism.data.E2emfView;

public class EMEAMasterDao extends BaseDao implements E2emfDao{
	
	 //static Connection con = null;
	 //static Statement stmt = null;
	 Connection con = null;
	 Statement stmt = null;
	    
	    @Override
		public GoodsHistoryList getGoodsHistory(Object... obj){
			return null;
		}
	@Override
	public E2emfBusinessObject getBusinessObject() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public E2emfBusinessObject getBusinessObject(Object... obj) throws AppException{		
		
		String param = null;
		String searchParam = null;
		String masterSearchQuery = null;
		String source = null;
		
		ResultSet rs = null;
		List<MasterItem>  masterPojoList = new ArrayList<MasterItem>();
		Master master = new Master();
		
		
		if(obj.length >1){			
			param = (String) obj[0];
			searchParam = (String) obj[1];
			source = (String) obj[2];
			Log.Info("Getting Filter details for param " + param + " and search Str : " +searchParam);
		}else{
			Log.Info("No params specified for Slob Master Search ");
		}
		
		if( param != null && searchParam != null){
				masterSearchQuery = dbUtil.buildEMEAMasterSearchQuery(param, searchParam, source);
				
				try{
					//if (con == null)
					con = dbUtil.getHiveConnection(E2emfConstants.emeaDB);
					
					if(con != null){
					
						//if(stmt == null)
							stmt = con.createStatement();
						
						rs = stmt.executeQuery(masterSearchQuery); 
				        Log.Info("Fetched resultset with fetchsize " + rs.getFetchSize() + " for the emea searchQuery : \n " + masterSearchQuery );
					   	if(rs != null) {        	
				        	while(rs.next()){
				        		MasterItem masterPojo = new MasterItem();
				        		if(rs.getString("name") != null ){
				        			masterPojo.setName(rs.getString("name"));
				        		} else {
				        			masterPojo.setName(rs.getString("id"));
				        		}
				        		masterPojo.setId(rs.getString("id"));
				        		
				        		masterPojoList.add(masterPojo);
				        	}
				        	
				        	master.setMasterDataList(masterPojoList);
				        } 
				        /*else{
				        	Log.Info("ResultSet Empty for executed for query : \n " + masterSearchQuery);
				        } */
						
					} else {
						Log.Info("Connection object is null");
					}
					
				}catch(SQLException sql){
					Log.Error("SQLException while Fetching Master Details for Slob." + sql.getStackTrace());
					Log.Error(sql);
					throw new AppException(sql.getMessage(),"Error while processing the request.", 1 ,sql.getCause(), true);
				}catch(Exception e){
					Log.Error("Exception in Fetching Master Details for Slob." + e.getStackTrace());
					Log.Error(e);
					e.printStackTrace();
					if(e instanceof NullPointerException){
						throw new AppException("NullPointerException",e.getStackTrace(),"Error while processing the request.", 3 ,e.getCause(), true);
					}
					throw new AppException(e.getMessage(),"Error while processing the request.", 2 ,e.getCause(), true);
				}finally{
					if(con != null)
						try {
							con.close();
							//stmt.close();
							//con = null;
							//stmt = null;
						} catch (SQLException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
							throw new AppException(e.getMessage(),"Error while processing the request.", 1 ,e.getCause(), true);
						}
						
				}
					
		}else{
			Log.Info("Criteria not set to fetch Master Details..");
		}
		return master;
		
		}

	@Override
	public List<FlowData> getFlowDataList(Object... obj) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String executeQuery(String masterSearchQuery) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String executeQuery(Object... obj) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public E2emfView getLevel2ViewDetails(Object... obj) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public E2emfView getLevel3ViewDetails(Object... obj) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public E2emfView getDefaultViewDetails(Object... obj) {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public MaterialReportList getMaterialReport(Object... obj) {
		// TODO Auto-generated method stub
		return null;
	}
	/*@Override
	public E2emfMaterialFlowData getE2emfMaterialFlowData(String source)
			throws AppException {
		// TODO Auto-generated method stub
		return null;
	}*/
	@Override
	public E2emfBusinessObject getActions(Object[] obj) throws AppException {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public E2emfBusinessObject getRoles(Object[] obj) throws AppException {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public E2emfBusinessObject getPrimaryRoles(Object[] obj)
			throws AppException {
		// TODO Auto-generated method stub
		return null;
	}

}

