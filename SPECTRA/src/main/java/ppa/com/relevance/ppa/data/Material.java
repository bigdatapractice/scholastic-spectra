package com.relevance.ppa.data;

public class Material {
	String system;
	String plant;
	String plantDesc;
	String sapPlant;
	String specCode;
	String materialId;
	String materialName;
	String materialIdTemp;
	String materialNameTemp;
	String supplierId;
	String supplier;
	float price;
	String materialGroup;
	String finalCategory;
	String latest;
	int uniqueId;
	String contractStartDate;
	String contractEndDate;
	String currency;
	String incoterm;
	String minOrderQuantity;
	String leadTime;
	String subcategory;
	String purchaseHistory;
	String galaxyVendorNo;
	String galaxyVendorCity;
	String materialCreationDate;
	String galaxyUOM;
	String category;
	String active;
	
	public String getMaterialIdTemp() {
		return materialIdTemp;
	}
	public void setMaterialIdTemp(String materialIdTemp) {
		this.materialIdTemp = materialIdTemp;
	}
	
	public String getMaterialNameTemp() {
		return materialNameTemp;
	}
	public void setMaterialNameTemp(String materialNameTemp) {
		this.materialNameTemp = materialNameTemp;
	}
	public String getActive() {
		return active;
	}
	public void setActive(String active) {
		this.active = setDefaultvalue(active);
	}
	public String getSystem() {
		return setDefaultvalue(system);
	}
	public void setSystem(String system) {
		this.system = system;
	}
	public String getPlant() {
		return setDefaultvalue(plant);
	}
	public void setPlant(String plant) {
		this.plant = plant;
	}
	public String getPlantDesc() {
		return setDefaultvalue(plantDesc);
	}
	public void setPlantDesc(String plantDesc) {
		this.plantDesc = plantDesc;
	}
	public String getSapPlant() {
		return setDefaultvalue(sapPlant);
	}
	public void setSapPlant(String sapPlant) {
		this.sapPlant = sapPlant;
	}
	public String getSpecCode() {
		return setDefaultvalue(specCode);
	}
	public void setSpecCode(String specCode) {
		this.specCode = specCode;
	}
	public String getMaterialId() {
		return setDefaultvalue(materialId);
	}
	public void setMaterialId(String materialId) {
		this.materialId = materialId;
	}
	public String getMaterialName() {
		return setDefaultvalue(materialName);
	}
	public void setMaterialName(String materialName) {
		this.materialName = materialName;
	}
	public String getSupplierId() {
		return setDefaultvalue(supplierId);
	}
	public void setSupplierId(String supplierId) {
		this.supplierId = supplierId;
	}
	public String getSupplier() {
		return setDefaultvalue(supplier);
	}
	public void setSupplier(String supplier) {
		this.supplier = supplier;
	}
	public float getPrice() {
		return price;
	}
	public void setPrice(float price) {
		this.price = price;
	}
	public String getMaterialGroup() {
		return setDefaultvalue(materialGroup);
	}
	public void setMaterialGroup(String materialGroup) {
		this.materialGroup = materialGroup;
	}
	public String getFinalCategory() {
		return setDefaultvalue(finalCategory);
	}
	public void setFinalCategory(String finalCategory) {
		this.finalCategory = finalCategory;
	}
	public String getLatest() {
		return setDefaultvalue(latest);
	}
	public void setLatest(String latest) {
		this.latest = latest;
	}
	public int getUniqueId() {
		return uniqueId;
	}
	public void setUniqueId(int uniqueId) {
		this.uniqueId = uniqueId;
	}
	public String getContractStartDate() {
		return setDefaultvalue(contractStartDate);
	}
	public void setContractStartDate(String contractStartDate) {
		this.contractStartDate = contractStartDate;
	}
	public String getContractEndDate() {
		return setDefaultvalue(contractEndDate);
	}
	public void setContractEndDate(String contractEndDate) {
		this.contractEndDate = contractEndDate;
	}
	public String getCurrency() {
		return setDefaultvalue(currency);
	}
	public void setCurrency(String currency) {
		this.currency = currency;
	}
	public String getIncoterm() {
		return setDefaultvalue(incoterm);
	}
	public void setIncoterm(String incoterm) {
		if (incoterm == null || incoterm.equalsIgnoreCase("null"))
			incoterm = "Not available";
		this.incoterm = incoterm;
	}
	public String getMinOrderQuantity() {
		return setDefaultvalue(minOrderQuantity);
	}
	public void setMinOrderQuantity(String minOrderQuantity) {
		this.minOrderQuantity = minOrderQuantity;
	}
	public String getLeadTime() {
		return setDefaultvalue(leadTime);
	}
	public void setLeadTime(String leadTime) {
		this.leadTime = leadTime;
	}	
	public String getCategory() {
		return setDefaultvalue(category);
	}
	public void setCategory(String category) {
		this.category = category;
	}
	public String getSubcategory() {
		return setDefaultvalue(subcategory);
	}
	public void setSubcategory(String subcategory) {
		this.subcategory = subcategory;
	}
	public String getPurchaseHistory() {
		return setDefaultvalue(purchaseHistory);
	}
	public void setPurchaseHistory(String purchaseHistory) {
		this.purchaseHistory = purchaseHistory;
	}
	public String getGalaxyVendorNo() {
		return setDefaultvalue(galaxyVendorNo);
	}
	public void setGalaxyVendorNo(String galaxyVendorNo) {
		this.galaxyVendorNo = galaxyVendorNo;
	}
	public String getGalaxyVendorCity() {
		return setDefaultvalue(galaxyVendorCity);
	}
	public void setGalaxyVendorCity(String galaxyVendorCity) {
		this.galaxyVendorCity = galaxyVendorCity;
	}
	public String getMaterialCreationDate() {
		return setDefaultvalue(materialCreationDate);
	}
	public void setMaterialCreationDate(String materialCreationDate) {
		this.materialCreationDate = materialCreationDate;
	}
	public String getGalaxyUOM() {
		return setDefaultvalue(galaxyUOM);
	}
	public void setGalaxyUOM(String galaxyUOM) {
		this.galaxyUOM = galaxyUOM;
	}
	
	public String setDefaultvalue(String obj){
		if (obj == null || obj.equalsIgnoreCase("null"))
			obj =  "Not Available";
		return obj;
	}
}
