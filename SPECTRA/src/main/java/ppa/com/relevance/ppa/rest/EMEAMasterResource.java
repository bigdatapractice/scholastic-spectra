package com.relevance.ppa.rest;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import com.relevance.ppa.service.PPAServiceLocator;
import com.relevance.prism.service.Service;
import com.relevance.prism.util.AppException;
import com.relevance.prism.util.DataObfuscator;
import com.relevance.prism.util.E2emfAppUtil;
import com.relevance.prism.util.Log;

@Path("/emeaMasterData")
@Consumes(MediaType.APPLICATION_JSON)
public class EMEAMasterResource {

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/searchMasterData")
	public String getSlobMasterData(@QueryParam("type") String varX,
			@QueryParam("q") String searchParam, @QueryParam("source") String source){
		//added Comment to trigger build in Jenkins
		long startTime = System.currentTimeMillis();
		long endTime = 0L;
		
		Log.Info("Recieved Slob searchMasterData Request with param " + varX + " searchParam " + searchParam);
		 
		String masterDataJson = null;
		
		try{
			searchParam = DataObfuscator.deObfuscateToken(searchParam);
			Service service = PPAServiceLocator.getServiceInstance("emeamaster");
			masterDataJson = (String) service.getJsonObject(varX, searchParam, source);
			masterDataJson = DataObfuscator.obfuscate(masterDataJson);
			endTime = System.currentTimeMillis();
			
			 	
		}catch(AppException appe){
			Log.Error(appe);
			return appe.toString();
		}catch(Exception e){
			Log.Error("Exception getSlobMasterData() of the MasterData with queryParams " + e.getStackTrace());	
		}
			
		Log.Info("Returning Slob MasterData Json to presentation, total Time taken is " +  E2emfAppUtil.getTotalElapsedTime(startTime, endTime));
		
		return masterDataJson;
	}
}
