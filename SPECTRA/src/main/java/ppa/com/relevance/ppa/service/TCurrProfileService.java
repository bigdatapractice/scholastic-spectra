package com.relevance.ppa.service;

import java.sql.SQLException;
import java.util.ArrayList;

import com.relevance.ppa.dao.TcurrProfileDao;
import com.relevance.ppa.data.TCurrView;
import com.relevance.prism.util.AppException;
import com.relevance.prism.util.Log;

public class TCurrProfileService {

	public ArrayList<TCurrView> getResults() throws AppException, SQLException{
		try{
		TcurrProfileDao tcurfProfileDao = new TcurrProfileDao();
		ArrayList<TCurrView> tcurfViewList = tcurfProfileDao.getResults();
		return tcurfViewList;
		}catch(Exception e){
			Log.Error("Exception fetching the Details for EMEA TCURR Profile. \n " + e.getMessage());
			Log.Error(e);
			e.printStackTrace();
			if(e instanceof NullPointerException){
				throw new AppException("NullPointerException",e.getStackTrace(),"Error while processing the request.", 3 ,e.getCause(), true);
			}
			throw new AppException(e.getMessage(),"Error while processing the request.", 2 ,e.getCause(), true);
		}
	}
}
