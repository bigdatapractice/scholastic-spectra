package com.relevance.ppa.dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.relevance.ppa.data.Material;
import com.relevance.ppa.data.PPAMaterialMapping;
import com.relevance.prism.dao.BaseDao;
import com.relevance.prism.data.Master;
import com.relevance.prism.data.MasterItem;
import com.relevance.prism.util.AppException;
import com.relevance.prism.util.E2emfConstants;
import com.relevance.prism.util.Log;
import com.relevance.prism.util.PrismDbUtil;

public class PlantPurchaseAnalyticsDao extends BaseDao{
	

	// Pointed to  EDL	
	static String ppaSSEQuery = "SELECT leadtime,currency,incoterm,min_order_qty as minorderqty,award_start_date as contractstartdate,award_end_date as contractenddate,sse_env as plant,jj_plant_location as plantdesc, spec as speccode, plantcode as materialid, trade_name as materialname,material_group as materialgroup,final_category as finalcategory, supplier_name as supplier,  sse_price as price,contract_status as contractstatus,vendorno,vendorcity, sse_uom as galaxyuom  FROM EMEA_SSE_PRICE_LIST";
   static String ppaBPCSQuery = "SELECT env as plant, CASE WHEN env = 'ITM' THEN IGTEC ELSE IDRAW END AS speccode, matnr as materialid, txtlg  as materialname,"
   	+ " '' as supplier,ivend as supplierid, stprs as price,purchasehistory, plantdesc,imdcrt as matcreationdt,category  FROM ppa_bpcs_items";
	static String ppaPlantMappingQuery = "SELECT plant_code as plantcode,sap_plantcode as sapplantcode,bpcs_plant_code as bpcsplantcode FROM PLANT";
	static String ppaItemMatchQuery = "select itemscount, sourcesystem, sourceplant, sourcesapplant, sourceplantdesc, subcategory, category, contractstatus, targetsystem, targetplant, targetsapplant, type, typedesc, typecategory, direction, purchasehistory from EMEA_AG_SSEITEMS_PROFILE";
		
		
	// Pointed to Sandbox
	
/*	static String ppaSSEQuery = "SELECT leadtime,currency,incoterm,min_order_qty as minorderqty,award_start_date as contractstartdate,award_end_date as contractenddate,sse_env as plant,jj_plant_location as plantdesc, spec as speccode, plantcode as materialid, trade_name as materialname,material_group as materialgroup,final_category as finalcategory, supplier_name as supplier,  sse_price as price, '' as contractstatus,'' as  vendorno,'' as vendorcity FROM EMEA_SSE_PRICE_LIST";
	static String ppaBPCSQuery = "SELECT env as plant, CASE WHEN env = 'ITM' THEN IGTEC ELSE IDRAW END AS speccode, iprod as materialid, idesc as materialname, vndnam as supplier,vendor as supplierid, iacst as price,  '' as purchasehistory FROM EMEA_BPCS_ITEM_MASTER";	
	static String ppaPlantMappingQuery = "SELECT plant_code as plantcode,sap_plantcode as sapplantcode,bpcs_plant_code as bpcsplantcode FROM emea_plant";
	static String ppaItemMatchQuery = "select * from emea_sse_matched_items";*/
	
	PrismDbUtil dbUtil = null;
	// E2emfAppUtil appUtil = null;
	Connection con = null;
	Statement stmt = null;


	public ArrayList<PPAMaterialMapping> getMaterialMappingAlgorithm(Object... obj)
			throws AppException {
		ArrayList<PPAMaterialMapping> materialMappingList = new ArrayList<PPAMaterialMapping>();
		ArrayList<Material> sseMaterialNotFoundList = new ArrayList<Material>();
		ArrayList<Material> bpcsMaterialNotFoundList = new ArrayList<Material>();
		String plants = null;
		String source = null;
		//String target = null;
		StringBuilder queryToExecute = null;
		ResultSet rs = null;
		HashMap<String, Material> bpcsMaterialIDHashMap = new HashMap<String, Material>();
		HashMap<String, Material> sseMaterialIDHashMap = new HashMap<String, Material>();
		HashMap<String, Material> bpcsSpecCodeHashMap = new HashMap<String, Material>();
		HashMap<String, Material> sseSpecCodeHashMap = new HashMap<String, Material>();
		HashMap<String, Material> bpcsMaterialNameHashMap = new HashMap<String, Material>();
		HashMap<String, Material> sseMaterialNameHashMap = new HashMap<String, Material>();
		HashMap<String, List<Material>> bpcsPlantHashMap = new HashMap<String, List<Material>>();
		HashMap<String, List<Material>> ssePlantHashMap = new HashMap<String, List<Material>>();
		HashMap<String, String> plantLookUpHashMap = new HashMap<String, String>();
		HashMap<String, String> sseSAPPlantLookUpHashMap = new HashMap<String, String>();
		HashMap<String, String> bpcsSAPPlantLookUpHashMap = new HashMap<String, String>();
		HashMap<String, String> sseSynonymHashMap = new HashMap<String, String>();
		HashMap<String, String> bpcsSynonymHashMap = new HashMap<String, String>();
		HashMap<String, String> synonymWithOutNumbersHashMap = new HashMap<String, String>();
		String targetPlant = "";
		/*Integer lvDistance = 100;
		Integer tempLVDistance = 100;
		Integer tempLVDistanceSupplier =100;
		Integer tempLVDistanceSynonym = 100;
		float priceDiff = 100;
		boolean removeDuplicated = true;*/
		try {
			if (obj != null && obj.length >= 1) {
				plants = (String) obj[0];
				source = (String) obj[1];
				//target = (String) obj[2];
				Log.Info("Fetching the DefaultView for PPA Profile ... for Source " + source);
				String whereClause = "";

				con = dbUtil.getHiveConnection(E2emfConstants.emeaWRK);
				
				//con = dbUtil.getPostGresConnection();
				Log.Info("Using SLOB HIVE Connection...");
				
				queryToExecute = new StringBuilder(ppaPlantMappingQuery);
				if (plants != null && !plants.equals("")) {
					String[] plantArr=plants.split(",");
					String plantsStr="";
					for(int i=0;i<plantArr.length;i++)
						plantsStr += "'" + plantArr[i] + "',";
					if (!plantsStr.equals("")) plantsStr =plantsStr.substring(0,plantsStr.length()-1);
					whereClause = " where bpcs_plant_code in (" + plantsStr + ")";
					queryToExecute.append(whereClause);
				}
				Log.Debug("PPA Query to Execute :" + queryToExecute.toString());
				stmt = con.createStatement();
				rs = stmt.executeQuery(queryToExecute.toString());
				System.out.println("Loading Plant codes of SAP, BPCS & SSE");
				if (rs != null) {
					while (rs.next()) {
						plantLookUpHashMap.put(rs.getString("plantcode").toLowerCase().trim(),rs.getString("bpcsplantcode").toLowerCase().trim());
						sseSAPPlantLookUpHashMap.put(rs.getString("plantcode").toLowerCase().trim(),rs.getString("sapplantcode").toLowerCase().trim());
						bpcsSAPPlantLookUpHashMap.put(rs.getString("bpcsplantcode").toLowerCase().trim(),rs.getString("sapplantcode").toLowerCase().trim());
					}
				}
				queryToExecute = new StringBuilder(ppaBPCSQuery);
				if (plants != null && !plants.equals("")) {
					String[] plantArr=plants.split(",");
					String plantsStr="";
					for(int i=0;i<plantArr.length;i++)
						plantsStr += "'" + plantArr[i] + "',";
					if (!plantsStr.equals("")) plantsStr =plantsStr.substring(0,plantsStr.length()-1);
					whereClause = " where env in (" + plantsStr + ")";
					queryToExecute.append(whereClause);
				}
				Log.Debug("PPA Query to Execute :" + queryToExecute.toString());
				stmt = con.createStatement();
				rs = stmt.executeQuery(queryToExecute.toString());
				List<Material> bpcsMaterialList = new ArrayList<Material>();
				System.out.println("Loading BPCS materials..");
				if (rs != null) {
					while (rs.next()) {
						Material bpcsMaterial = new Material();
						List<Material> plantPPAMaterial = new ArrayList<Material>();
						bpcsMaterial.setPlant(rs.getString("plant"));
						bpcsMaterial.setMaterialId(rs.getString("materialid"));
						bpcsMaterial.setSpecCode(rs.getString("speccode"));
						//bpcsMaterial.setMaterialName(rs.getString("materialname"));
						//bpcsMaterial.setMaterialGroup(rs.getString("materialgroup"));
						bpcsMaterial.setMaterialName(rs.getString("materialname"));
						bpcsMaterial.setSupplier(rs.getString("supplier"));
						bpcsMaterial.setSupplierId(rs.getString("supplierid"));
						bpcsMaterial.setPrice(rs.getFloat("price"));
						bpcsMaterial.setCategory(rs.getString("category"));
						
						bpcsMaterial.setSystem("BPCS");
						bpcsMaterial.setPurchaseHistory(rs.getString("purchasehistory"));
						bpcsMaterial.setPlantDesc(rs.getString("plantdesc"));
                        bpcsMaterial.setMaterialCreationDate(rs.getString("matcreationdt"));
						
						bpcsMaterial.setSapPlant(bpcsSAPPlantLookUpHashMap.get(bpcsMaterial.getPlant().toLowerCase().trim()));
						if (!bpcsMaterial.getMaterialId().equalsIgnoreCase("NA")){
							bpcsMaterialIDHashMap.put(bpcsMaterial.getPlant().toLowerCase().trim() + "-" +bpcsMaterial.getMaterialId().toLowerCase().trim().replaceFirst("^0+(?!$)", ""), bpcsMaterial);
						}
						
						if (!bpcsMaterial.getMaterialName().trim().equalsIgnoreCase("") && !bpcsMaterial.getMaterialName().toLowerCase().trim().replaceAll("[^a-zA-Z0-9 ]", "").equalsIgnoreCase("") && !bpcsMaterial.getMaterialName().toLowerCase().trim().replaceAll("[^a-zA-Z0-9 ]", "").equalsIgnoreCase("na")){
							bpcsMaterialNameHashMap.put(bpcsMaterial.getPlant().toLowerCase().trim() + "-" +bpcsMaterial.getMaterialName().toLowerCase().trim().replaceAll("[^a-zA-Z0-9 ]", ""), bpcsMaterial);
						}
						
						if (!bpcsMaterial.getSpecCode().trim().equalsIgnoreCase("") ){
							bpcsSpecCodeHashMap.put(bpcsMaterial.getPlant().toLowerCase().trim() + "-" +bpcsMaterial.getSpecCode().toLowerCase().trim().replaceFirst("^0+(?!$)", ""), bpcsMaterial);
						}
						
						if (bpcsPlantHashMap.containsKey(bpcsMaterial.getPlant().toLowerCase().trim())){
							plantPPAMaterial = bpcsPlantHashMap.get(bpcsMaterial.getPlant().toLowerCase().trim());
						}
						
						plantPPAMaterial.add(bpcsMaterial);
						bpcsPlantHashMap.put(bpcsMaterial.getPlant().toLowerCase().trim(),plantPPAMaterial);
						bpcsMaterialList.add(bpcsMaterial);
					}
				}
				
				queryToExecute = new StringBuilder(ppaSSEQuery);
				if (plants != null && !plants.equals("")) {
					String[] plantArr=plants.split(",");
					String plantsStr="";
					for(int i=0;i<plantArr.length;i++)
						plantsStr += "'" + plantArr[i] + "',";
					if (!plantsStr.equals("")) plantsStr =plantsStr.substring(0,plantsStr.length()-1);
					whereClause = " where sse_env in (" + plantsStr + ")";
					queryToExecute.append(whereClause);
				}
				rs = stmt.executeQuery(queryToExecute.toString());
				List<Material> sseMaterialList = new ArrayList<Material>();
				Material sseMaterial = new Material();
				//Material bpcsMaterial = new Material();
				int uniqueId =0;
				System.out.println("Loading SSE Materials..");
				if (rs != null) {
					while (rs.next()) {
						uniqueId++;
						sseMaterial = new Material();
						sseMaterial.setSystem("SSE");
						List<Material> plantPPAMaterial = new ArrayList<Material>();
						sseMaterial.setPlant(rs.getString("plant"));
						sseMaterial.setPlantDesc(rs.getString("plantdesc"));
						sseMaterial.setMaterialId(rs.getString("materialid"));
						sseMaterial.setSpecCode(rs.getString("speccode"));
						sseMaterial.setGalaxyVendorNo(rs.getString("vendorno"));
						sseMaterial.setGalaxyVendorCity(rs.getString("vendorcity"));
						sseMaterial.setMaterialName(rs.getString("materialname")); 
						sseMaterial.setSupplier(rs.getString("supplier"));
//						sseMaterial.setVendorNo(rs.getString("vendorno"));
//						sseMaterial.setVendorCity(rs.getString("vendorcity"));
						sseMaterial.setPrice(rs.getFloat("price"));						//
						sseMaterial.setMaterialGroup(rs.getString("materialgroup"));
						sseMaterial.setFinalCategory(rs.getString("finalcategory"));
						sseMaterial.setContractStartDate(rs.getString("contractstartdate"));
						sseMaterial.setContractEndDate(rs.getString("contractenddate"));				
//						sseMaterial.setContractStatus(rs.getString("contractstatus"));						
						sseMaterial.setGalaxyUOM(rs.getString("galaxyuom"));
						sseMaterial.setSystem("SSE");
						sseMaterial.setCurrency(rs.getString("currency"));
						sseMaterial.setIncoterm(rs.getString("incoterm"));
						sseMaterial.setMinOrderQuantity(rs.getString("minorderqty"));
						sseMaterial.setLeadTime(rs.getString("leadtime"));
						sseMaterial.setUniqueId(uniqueId);
						sseMaterial.setSapPlant(sseSAPPlantLookUpHashMap.get(sseMaterial.getPlant().toLowerCase().trim()));
						targetPlant = plantLookUpHashMap.get(sseMaterial.getPlant().toLowerCase().trim());
					
						/*List<PPAMaterial> tempSSEMaterialList = new ArrayList<PPAMaterial>(); 
						if (ssePlantHashMap.containsKey(targetPlant.toLowerCase().trim()))
							tempSSEMaterialList = ssePlantHashMap.get(targetPlant.toLowerCase().trim());
						
						tempSSEMaterialList.add(sseMaterial);
						ssePlantHashMap.put(targetPlant.toLowerCase().trim(), tempSSEMaterialList);*/
						if (targetPlant  != null){
							if (sseMaterial.getMaterialId() != null && !sseMaterial.getMaterialId().equalsIgnoreCase("NA")){
								sseMaterialIDHashMap.put(targetPlant.toLowerCase().trim() + "-" +sseMaterial.getMaterialId().toLowerCase().trim().replaceFirst("^0+(?!$)", ""), sseMaterial);
							}
							
							if (sseMaterial.getMaterialName() != null && !sseMaterial.getMaterialName().trim().equalsIgnoreCase("") && !sseMaterial.getMaterialName().toLowerCase().trim().replaceAll("[^a-zA-Z0-9 ]", "").equalsIgnoreCase("") && !sseMaterial.getMaterialName().toLowerCase().trim().replaceAll("[^a-zA-Z0-9 ]", "").equalsIgnoreCase("na")){
								sseMaterialNameHashMap.put(targetPlant.toLowerCase().trim() + "-" +sseMaterial.getMaterialName().toLowerCase().trim().replaceAll("[^a-zA-Z0-9 ]", ""), sseMaterial);
							}
							
							if (sseMaterial.getSpecCode() != null && !sseMaterial.getSpecCode().trim().equalsIgnoreCase("")){
								sseSpecCodeHashMap.put(targetPlant.toLowerCase().trim() + "-" +sseMaterial.getSpecCode().toLowerCase().trim().replaceFirst("^0+(?!$)", ""), sseMaterial);
							}
							if (ssePlantHashMap.containsKey(targetPlant.toLowerCase().trim())){
							plantPPAMaterial = ssePlantHashMap.get(targetPlant.toLowerCase().trim());							
							}
							plantPPAMaterial.add(sseMaterial);
							ssePlantHashMap.put(targetPlant.toLowerCase().trim(),plantPPAMaterial);
						}			
										
						
						sseMaterialList.add(sseMaterial);
					}
					HashMap<String, Material> rbpcsMaterialIDHashMap =(HashMap<String, Material>) bpcsMaterialIDHashMap.clone();
					HashMap<String, Material> rsseMaterialIDHashMap = (HashMap<String, Material>) sseMaterialIDHashMap.clone();
					HashMap<String, Material> rbpcsSpecCodeHashMap =(HashMap<String, Material>) bpcsSpecCodeHashMap.clone();
					HashMap<String, Material> rsseSpecCodeHashMap =(HashMap<String, Material>) sseSpecCodeHashMap.clone();
					HashMap<String, Material> rbpcsMaterialNameHashMap =(HashMap<String, Material>) bpcsMaterialNameHashMap.clone();
					HashMap<String, Material> rsseMaterialNameHashMap = (HashMap<String, Material>)sseMaterialNameHashMap.clone();
					HashMap<String, List<Material>> rbpcsPlantHashMap = (HashMap<String, List<Material>>)bpcsPlantHashMap.clone();
					HashMap<String, List<Material>> rssePlantHashMap = (HashMap<String, List<Material>>)ssePlantHashMap.clone();
					HashMap<String, String> rplantLookUpHashMap = ( HashMap<String, String>)plantLookUpHashMap.clone();
					HashMap<String, String> rsseSAPPlantLookUpHashMap = ( HashMap<String, String>) sseSAPPlantLookUpHashMap.clone();
					HashMap<String, String> rbpcsSAPPlantLookUpHashMap = ( HashMap<String, String>) bpcsSAPPlantLookUpHashMap.clone();
					HashMap<String, String> rsseSynonymHashMap = (HashMap<String, String>) sseSynonymHashMap.clone();
					HashMap<String, String> rbpcsSynonymHashMap =( HashMap<String, String>)bpcsSynonymHashMap.clone(); 
					HashMap<String, String> rsynonymWithOutNumbersHashMap = (HashMap<String, String>)synonymWithOutNumbersHashMap.clone();
					//ArrayList<PPAMaterialMapping> rmaterialMappingList = ( ArrayList<PPAMaterialMapping>) materialMappingList.clone();
					ArrayList<Material> rsseMaterialNotFoundList = ( ArrayList<Material>)sseMaterialNotFoundList.clone();
					ArrayList<Material> rbpcsMaterialNotFoundList = ( ArrayList<Material>)bpcsMaterialNotFoundList.clone();
					List<Material> rsseMaterialList =( ArrayList<Material>) (( ArrayList<Material>)sseMaterialList).clone();
					materialMappingList = CompareSSEToBPCS(bpcsMaterialIDHashMap, sseMaterialIDHashMap, bpcsSpecCodeHashMap, sseSpecCodeHashMap, bpcsMaterialNameHashMap, sseMaterialNameHashMap, bpcsPlantHashMap, ssePlantHashMap, sseSynonymHashMap, bpcsSynonymHashMap, synonymWithOutNumbersHashMap, materialMappingList, sseMaterialNotFoundList,bpcsMaterialList);
				materialMappingList = ReversePlantPurchaseAnalyticsDao.compareBPCSToSSE(rbpcsMaterialIDHashMap, rsseMaterialIDHashMap, rbpcsSpecCodeHashMap, rsseSpecCodeHashMap, rbpcsMaterialNameHashMap, rsseMaterialNameHashMap, rbpcsPlantHashMap, rssePlantHashMap, rsseSynonymHashMap, rbpcsSynonymHashMap, rsynonymWithOutNumbersHashMap, materialMappingList, rbpcsMaterialNotFoundList, rsseMaterialList);
			}
		}
	} catch (SQLException sql) {
		sql.printStackTrace();
		Log.Error("SQLException while Fetching Master Details for PPA Material Matching."
				+ sql.getStackTrace());
		Log.Error(sql);
		throw new AppException(sql.getMessage(),
				"Error while processing the request.", 1, sql.getCause(),
				true);
	} catch (Exception e) {
		e.printStackTrace();
		Log.Error("Exception fetching the Details for PPA Material Matching."
				+ source + " \n " + e.getMessage());
		Log.Error(e);
		System.out.println("Error on Comparison " + e.getMessage());
		e.printStackTrace();
		if (e instanceof NullPointerException) {
			throw new AppException("NullPointerException",
					e.getStackTrace(),
					"Error while processing the request.", 3, e.getCause(),
					true);
		}
		throw new AppException(e.getMessage(),
				"Error while processing the request.", 2, e.getCause(),
				true);
	} finally {
		if (con != null)
			try {
				con.close();
				// stmt.close();
				// con = null;
				// stmt = null;
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				throw new AppException(e.getMessage(),
						"Error while processing the request.", 1,
						e.getCause(), true);
			}

	}
	return materialMappingList;
 
}
					
				
	public boolean compareAndsaveMaterialMappingInDB(Object... obj) throws AppException {
		ArrayList<PPAMaterialMapping> materialMappings = getMaterialMappingAlgorithm(obj);

		//String plants = null;
		String source = null;
		/*String target = null;
		StringBuilder queryToExecute = null;
		ResultSet rs = null;*/
		Connection conHive = null;
		try {		
			conHive = dbUtil.getHiveConnection(E2emfConstants.emeaDB);		
			//boolean isSchAvailinHive = dropAndCreateSchemaInHive(conHive);
			boolean isSchAvailinHive=true;		
			for (PPAMaterialMapping m : materialMappings) {
				if (isSchAvailinHive){
					try{
					String qry=prepareQueryForHive(m);
					System.out.println(qry);
					Statement hiveSt=conHive.createStatement();
					hiveSt.executeUpdate(qry);
					}catch(Exception ex){ ex.printStackTrace(); }
				}
			}

		}  catch (Exception e) {
			Log.Error("Exception fetching the Details for PPA Material Matching." + source + " \n "
					+ e.getMessage());
			Log.Error(e);
			e.printStackTrace();
			if (e instanceof NullPointerException) {
				/*
				 * throw new AppException("NullPointerException",
				 * e.getStackTrace(), "Error while processing the request.", 3,
				 * e.getCause(), true);
				 */
			}
			/*
			 * throw new AppException(e.getMessage(),
			 * "Error while processing the request.", 2, e.getCause(), true);
			 */
		} finally {
			if (con != null)
				try {			
					conHive.close();			
				} catch (SQLException e) {
					e.printStackTrace();
					/*
					 * throw new AppException(e.getMessage(),
					 * "Error while processing the request.", 1, e.getCause(),
					 * true);
					 */
				}

		}
		return true;

	}
	
	public ArrayList<PPAMaterialMapping> getMaterialMapping(Object... obj)
			throws AppException {
		ArrayList<PPAMaterialMapping> materialMappingList = new ArrayList<PPAMaterialMapping>();
		//String plants = null;
		String source = null;
		//String target = null;
		StringBuilder queryToExecute = null;
		ResultSet rs = null;
		try {
			if (obj != null && obj.length >= 1) {
				//plants = (String) obj[0];
				source = (String) obj[1];
				//target = (String) obj[2];
				Log.Info("Fetching the DefaultView for PPA Profile ... for Source "
						+ source);
				//String whereClause = "";

				con = dbUtil.getHiveConnection(E2emfConstants.emeaDB);
				Log.Info("Using SLOB HIVE Connection...");
				
				queryToExecute = new StringBuilder(ppaItemMatchQuery);
				Log.Debug("PPA Query to Execute :" + queryToExecute.toString());
				stmt = con.createStatement();
				rs = stmt.executeQuery(queryToExecute.toString());
				if (rs != null) {
					while (rs.next()) {
						PPAMaterialMapping ppaMaterialMapping = new PPAMaterialMapping();
						ppaMaterialMapping.setSourceSystem(rs.getString("sourcesystem"));
						ppaMaterialMapping.setSourcePlant(rs.getString("sourceplant"));
						ppaMaterialMapping.setSourcePlantDesc(rs.getString("sourceplantdesc"));
						ppaMaterialMapping.setContractStatus(rs.getString("contractstatus"));
						ppaMaterialMapping.setSourceSAPPlant(rs.getString("sourcesapplant"));
						ppaMaterialMapping.setTargetSAPPlant(rs.getString("targetsapplant"));
						ppaMaterialMapping.setTargetSystem(rs.getString("targetsystem"));
						ppaMaterialMapping.setTargetPlant(rs.getString("targetplant"));
						ppaMaterialMapping.setType(rs.getString("type"));
						ppaMaterialMapping.setTypeDesc(rs.getString("typedesc"));
						ppaMaterialMapping.setTypeCategory(rs.getString("typecategory"));
						ppaMaterialMapping.setCategory(rs.getString("category"));
						ppaMaterialMapping.setSubcategory(rs.getString("subcategory"));
						ppaMaterialMapping.setPurchaseHistory(rs.getString("purchasehistory"));
                      //  ppaMaterialMapping.setMaterialCreationDate(rs.getString("materialcreationdate"));
						ppaMaterialMapping.setDirection(rs.getString("direction"));
						ppaMaterialMapping.setItemsCount(rs.getString("itemscount"));						
//						ppaMaterialMapping.setGalaxyUom(rs.getString("galaxyuom"));
						 
						
//						ppaMaterialMapping.setSourceMaterialId(rs.getString("sourcematerialid"));
//						ppaMaterialMapping.setSourceMaterialName(rs.getString("sourcematerialname"));
//						ppaMaterialMapping.setTargetMaterialId(rs.getString("targetmaterialid"));
//						ppaMaterialMapping.setTargetMaterialName(rs.getString("targetmaterialname"));
//						ppaMaterialMapping.setSourceSpecCode(rs.getString("sourcespeccode"));
//						ppaMaterialMapping.setTargetSpecCode(rs.getString("targetspeccode"));
//						ppaMaterialMapping.setMaterialCreationDate(rs.getString("materialcreationdate"));
						
						//					ppaMaterialMapping.setSourceMaterialId(rs.getString("sourcematerialid"));
						//					ppaMaterialMapping.setSourceSpecCode(rs.getString("sourcespeccode"));
						//					ppaMaterialMapping.setSourceMaterialName(rs.getString("sourcematerialname"));
						//					ppaMaterialMapping.setSourceSupplier(rs.getString("sourcesupplier"));
						//					ppaMaterialMapping.setSourcePrice(rs.getFloat("sourceprice"));
//						ppaMaterialMapping.setMaterialGroup(rs.getString("materialgroup"));
//						ppaMaterialMapping.setSourceFinalCategory(rs.getString("sourcefinalcategory"));
//						ppaMaterialMapping.setSourceContractStartDate(rs.getString("sourcecontractstartdate"));
//						ppaMaterialMapping.setSourceContractEndDate(rs.getString("sourcecontractenddate"));
//						ppaMaterialMapping.setSourceCurrency(rs.getString("sourcecurrency"));
//						ppaMaterialMapping.setSourceIncoterm(rs.getString("sourceincoterm"));
						//ppaMaterialMapping.setSourceMinOrderQuantity(rs.getString("sourceminorderquantity"));
//						ppaMaterialMapping.setSourceLeadTime(rs.getString("sourceleadtime"));
//						ppaMaterialMapping.setTargetMaterialId(rs.getString("targetmaterialid"));
//						ppaMaterialMapping.setTargetSpecCode(rs.getString("targetspeccode"));
//						ppaMaterialMapping.setTargetMaterialName(rs.getString("targetmaterialname"));
//						ppaMaterialMapping.setTargetSupplier(rs.getString("targetsupplier"));
//						ppaMaterialMapping.setTargetSupplierId(rs.getString("targetsupplierid"));
//						ppaMaterialMapping.setTargetPrice(rs.getFloat("targetprice"));
//						ppaMaterialMapping.setWeightage(rs.getInt("weightage"));
						
						materialMappingList.add(ppaMaterialMapping);
					}
				}
			}
	} catch (SQLException sql) {
		Log.Error("SQLException while Fetching Master Details for PPA Material Matching."
				+ sql.getStackTrace());
		Log.Error(sql);
		throw new AppException(sql.getMessage(),
				"Error while processing the request.", 1, sql.getCause(),
				true);
	} catch (Exception e) {
		Log.Error("Exception fetching the Details for PPA Material Matching."
				+ source + " \n " + e.getMessage());
		Log.Error(e);
		e.printStackTrace();
		if (e instanceof NullPointerException) {
			throw new AppException("NullPointerException",
					e.getStackTrace(),
					"Error while processing the request.", 3, e.getCause(),
					true);
		}
		throw new AppException(e.getMessage(),
				"Error while processing the request.", 2, e.getCause(),
				true);
	} finally {
		if (con != null)
			try {
				con.close();
				// stmt.close();
				// con = null;
				// stmt = null;
			} catch (SQLException e) {
				e.printStackTrace();
				throw new AppException(e.getMessage(),
						"Error while processing the request.", 1,
						e.getCause(), true);
			}

	}
	return materialMappingList;
}

	
	public Master getSearchMaterialMapping(Object... obj)
			throws AppException {
		String param = null;
		String searchParam = null;
		String masterSearchQuery = null;
		String source = null;
		
		ResultSet rs = null;
		List<MasterItem> masterPojoList = new ArrayList<MasterItem>();
		Master master = new Master();
		
		if (obj.length > 1) {
			param = (String) obj[0];
			searchParam = (String) obj[1];
			source = (String) obj[2];
			Log.Info("Getting Filter details for param " + param
					+ " and search Str : " + searchParam);
		} else {
			Log.Info("No params specified for Material mapping Master Search ");
		}

		if (param != null && searchParam != null) {
			masterSearchQuery = buildMappingMaterialMasterSearchQuery(param, searchParam,
					source);
			try {
				// if (con == null)
				
				con = dbUtil.getHiveConnection(E2emfConstants.emeaDB);

				if (con != null) {

					stmt = con.createStatement();

					rs = stmt.executeQuery(masterSearchQuery);
					Log.Info("Fetched resultset with fetchsize "
							+ rs.getFetchSize()
							+ " for the Material mapping searchQuery : \n "
							+ masterSearchQuery);
					if (rs != null) {
						while (rs.next()) {
							MasterItem masterPojo = new MasterItem();

							if (param.equalsIgnoreCase("M")
									&& rs.getString("materialgroup") != null) {
								masterPojo.setName(rs.getString("materialgroup"));
								masterPojo.setId(rs.getString("materialgroup"));
							}
							if (param.equalsIgnoreCase("C")
									&& rs.getString("sourcefinalcategory") != null) {
								masterPojo.setName(rs.getString("sourcefinalcategory"));
								masterPojo.setId(rs.getString("sourcefinalcategory"));
							}
							

							masterPojoList.add(masterPojo);
						}

						master.setMasterDataList(masterPojoList);
					}
					/*
					 * else{
					 * Log.Info("ResultSet Empty for executed for query : \n " +
					 * masterSearchQuery); }
					 */

				} else {
					Log.Info("Connection object is null");
				}

			} catch (SQLException sql) {
				Log.Error("SQLException while Fetching Master Details for Slob."
						+ sql.getStackTrace());
				Log.Error(sql);
				throw new AppException(sql.getMessage(),
						"Error while processing the request.", 1,
						sql.getCause(), true);
			} catch (Exception e) {
				Log.Error("Exception in Fetching Master Details for Slob."
						+ e.getStackTrace());
				Log.Error(e);
				e.printStackTrace();
				if (e instanceof NullPointerException) {
					throw new AppException("NullPointerException",
							e.getStackTrace(),
							"Error while processing the request.", 3,
							e.getCause(), true);
				}
				throw new AppException(e.getMessage(),
						"Error while processing the request.", 2, e.getCause(),
						true);
			} finally {
				if (con != null)
					try {
						con.close();
						// stmt.close();
						// con = null;
						// stmt = null;
					} catch (SQLException e) {
						e.printStackTrace();
						throw new AppException(e.getMessage(),
								"Error while processing the request.", 1,
								e.getCause(), true);
					}

			}

		} else {
			Log.Info("Criteria not set to fetch Master Details..");
		}
		return master;

	}

	public String buildMappingMaterialMasterSearchQuery(String param,
			String searchStr, String source) {
		String masterSearchQuery = null;
		String tableName = "emea_sse_matched_items";
		if (param.equals("C")) {
			// tableName = "SLOB_" + source + "_FINAL";
			masterSearchQuery = "select distinct subcategory  from "
					+ tableName 
					+ " where (subcategory like '%"
					+ searchStr
					+ "%' or lcase(subcategory) like '%"
					+ searchStr
					+ "%' or ucase(subcategory) like '%"
					+ searchStr
					+ "%')" + "limit 30";

		} else if (param.equals("M")) {
			// tableName = "SLOB_" + source + "_FINAL";
			masterSearchQuery = "select distinct category  from " + tableName
					+ " where (category like '%"
					+ searchStr
					+ "%' or lcase(category) like '%"
					+ searchStr
					+ "%' or ucase(category) like '%"
					+ searchStr
					+ "%')" + "limit 30";

		} 
		Log.Info("Material Mapping Query built is \n" + masterSearchQuery);

		return masterSearchQuery;
	}

					
	public static int distance(String a, String b) {
		a = a.toLowerCase();
		b = b.toLowerCase();
		// i == 0
		int[] costs = new int[b.length() + 1];
		for (int j = 0; j < costs.length; j++)
			costs[j] = j;
		for (int i = 1; i <= a.length(); i++) {
			// j == 0; nw = lev(i - 1, j)
			costs[0] = i;
			int nw = i - 1;
			for (int j = 1; j <= b.length(); j++) {
				int cj = Math.min(1 + Math.min(costs[j], costs[j - 1]),
						a.charAt(i - 1) == b.charAt(j - 1) ? nw : nw + 1);
				nw = costs[j];
				costs[j] = cj;
			}
		}
		return costs[b.length()];
	}
	public static int min(int a, int b, int c) {
	     if (a <= b && a <= c) return a;
	     if (b <= a && b <= c) return b;
	     return c;
	}

	public boolean dropAndCreateSchemaInHive(Connection hiveCon) {
		//boolean isSchCreated = false;
		try {
			Statement hstmt = hiveCon.createStatement();
			hstmt.executeUpdate("drop table if exists emea_sse_matched_items");
			String qryCreatetblmatchedItms = "create table emea_sse_matched_items(sourcesystem	string,"
					+ "sourceplant	string,sourcesapplant	string,sourceplantdesc	string,sourcespeccode	string,"
					+ "sourcematerialid	string, sourcematerialname	string,subcategory	string,sourcecontractstartdate	string,"
					+ "sourcecontractenddate	string,category	string,sourcesupplier	string,contractstatus	string,"
					+ "sourceprice	string,sourcecurrency	string,sourceincoterm	string,sourceleadtime	string,sourceminorderquantity	string,"
					+ "targetsystem	string,targetplant	string,targetsapplant	string,targetspeccode	string,targetmaterialid	string,targetmaterialname	string,"
					+ "targetsupplierid	string,targetsupplier	string,targetprice	string,targetpricebyprice	string,type	string,typedesc	string,"
					+ "typecategory	string,lvdistnacedesc	string,lvdistnacesupplier	string,pricediff	string,pricediffbyprice	string,weightage	string,direction string)";
			hstmt.executeUpdate(qryCreatetblmatchedItms);
		} catch (Exception ex) {
			return false;
		}
		return true;
	}
	public String prepareQueryForHive(PPAMaterialMapping m){
		String query="";
		if (m.getTargetMaterialName()== null) m.setTargetMaterialName("");
		if(m.getTargetSupplier()== null) m.setTargetSupplier("");
		query = "insert into emea_sse_matched_items values('" 
						+  m.getSourceSystem() + "','"+  
						 m.getSourcePlant()  + "','"+  
						m.getSourceSAPPlant()  + "','"+  
						m.getSourcePlantDesc()  + "','"+  
						m.getSourceSpecCode()  + "','"+  
						m.getSourceMaterialId()  + "','"+  
						m.getSourceMaterialName().replace("\\","\\\\").replace("'","\\'")  + "','"+  // need to replace ' " for hive
						m.getSubcategory() + "','"+  
						m.getSourceContractStartDate()  + "','"+  
						m.getSourceContractEndDate()  + "','"+  
						m.getCategory().replace("'","\\'")  + "','"+  
						m.getSourceSupplier().replace("'","\\'")  + "','"+  
						m.getContractStatus()  + "','"+  
						Float.toString(m.getSourcePrice())  + "','"+  
						m.getSourceCurrency()  + "','"+  
						m.getSourceIncoterm()  + "','"+  
						m.getSourceLeadTime()  + "','"+  
						m.getSourceMinOrderQuantity()  + "','"+  
						m.getTargetSystem()  + "','"+  
						m.getTargetPlant()  + "','"+  
						m.getTargetSAPPlant()  + "','"+  
						m.getTargetSpecCode()  + "','"+  
						m.getTargetMaterialId()  + "','"+  
						m.getTargetMaterialName().replace("'","\\'")  + "','"+   // need to replace ' " for hive
						m.getTargetSupplierId()  + "','"+  
						m.getTargetSupplier().replace("'","\\'")  + "','"+  
						Float.toString(m.getTargetPrice())  + "','"+  
						Float.toString(m.getTargetPriceByPrice())  + "','"+  
						m.getType()  + "','"+  
						m.getTypeDesc()  + "','"+  
						m.getTypeCategory()  + "','"+  
						String.valueOf(m.getLvDistnaceDesc())  + "','"+  
						String.valueOf(m.getLvDistnaceSupplier())  + "','"+  
						Float.toString(m.getPriceDiff())  + "','"+  
						Float.toString(m.getPriceDiffByPrice())  + "','"+  
						String.valueOf(m.getWeightage())  + "','" +
						String.valueOf(m.getDirection()) + "')";  
		return  query;
	}
	public ArrayList<PPAMaterialMapping>  CompareSSEToBPCS(HashMap<String, Material> bpcsMaterialIDHashMap,
			HashMap<String, Material> sseMaterialIDHashMap,
			HashMap<String, Material> bpcsSpecCodeHashMap,
			HashMap<String, Material> sseSpecCodeHashMap,
			HashMap<String, Material> bpcsMaterialNameHashMap,
			HashMap<String, Material> sseMaterialNameHashMap,
			HashMap<String, List<Material>> bpcsPlantHashMap,
			HashMap<String, List<Material>> ssePlantHashMap,
			HashMap<String, String> sseSynonymHashMap,
			HashMap<String, String> bpcsSynonymHashMap, 
			HashMap<String, String> synonymWithOutNumbersHashMap, 
			ArrayList<PPAMaterialMapping> materialMappingList ,
			ArrayList<Material> sseMaterialNotFoundList ,
			List<Material> bpcsMaterialList
){
		Material sseMaterial = new Material();
		Material bpcsMaterial = new Material();
		String targetPlant = "";
		Integer lvDistance = 100;
		Integer tempLVDistance = 100;
		Integer tempLVDistanceSupplier =100;
		/*Integer tempLVDistanceSynonym = 100;
		float priceDiff = 100;*/
		boolean removeDuplicated = true;
		System.out.println("Starting SSE to BPCS Comparison");
		try{
		Iterator<Map.Entry<String, List<Material>>> itPlant =  ssePlantHashMap.entrySet().iterator();
		while (itPlant.hasNext()) {
			Map.Entry<String, List<Material>> pairs = (Map.Entry<String, List<Material>>) itPlant.next();
			List<Material> sseMaterials =  new ArrayList<Material>();
			List<Material> bpcsMaterials =  new ArrayList<Material>();
			sseMaterials = pairs.getValue();
			lvDistance = 100;
			//priceDiff = 100;
			targetPlant = pairs.getKey().toLowerCase().trim();
			bpcsMaterials = bpcsPlantHashMap.get(pairs.getKey());
			String out="Comparing " + targetPlant +  " with DM count : " ;
			out += (sseMaterials != null) ?  sseMaterials.size(): "0";
			out+= "\n The Plant count is ";
			out += (bpcsMaterial != null) ? bpcsMaterials.size() : "0";
			System.out.println(out);
			for (int j = sseMaterials.size(); j > 0 ; j--){
                            try{
				boolean matchFound = false;
				sseMaterial = sseMaterials.get(j-1);
				if (sseMaterial.getMaterialName() == null) sseMaterial.setMaterialName("");
				if (bpcsMaterial.getMaterialName() == null)bpcsMaterial.setMaterialName("");
				if (bpcsMaterialIDHashMap.containsKey(targetPlant+"-"+sseMaterial.getMaterialId().toLowerCase().trim().replaceFirst("^0+(?!$)", ""))){
					matchFound = true;
					bpcsMaterial = bpcsMaterialIDHashMap.get(targetPlant+"-"+sseMaterial.getMaterialId().toLowerCase().trim().replaceFirst("^0+(?!$)", ""));
					PPAMaterialMapping ppaMaterialMapping = new PPAMaterialMapping();
					ppaMaterialMapping.setSourceSystem("SSE");
					ppaMaterialMapping.setSourcePlant(sseMaterial.getPlant());
					ppaMaterialMapping.setSourcePlantDesc(sseMaterial.getPlantDesc());
					ppaMaterialMapping.setSourceMaterialId(sseMaterial.getMaterialId());
					ppaMaterialMapping.setSourceSpecCode(sseMaterial.getSpecCode());
					//ppaMaterialMapping.setSourceMaterialName(sseMaterial.getMaterialName());					
					ppaMaterialMapping.setSourceMaterialName(sseMaterial.getMaterialGroup());					
					ppaMaterialMapping.setSourceSupplier(sseMaterial.getSupplier());
					ppaMaterialMapping.setVendorNo(sseMaterial.getGalaxyVendorNo());
					ppaMaterialMapping.setVendorCity(sseMaterial.getGalaxyVendorCity());
					ppaMaterialMapping.setSourceSupplier(sseMaterial.getSupplier());
					ppaMaterialMapping.setSourcePrice(sseMaterial.getPrice());
//					ppaMaterialMapping.setContractStatus(sseMaterial.getContractStatus());
					ppaMaterialMapping.setGalaxyUom(sseMaterial.getGalaxyUOM());
					//ppaMaterialMapping.setCategory(sseMaterial.getMaterialGroup());
					/*if (sseMaterial.getMaterialGroup().equals("A") || sseMaterial.getMaterialGroup().equals("P"))
						ppaMaterialMapping.setCategory("Chemical");
					else
						ppaMaterialMapping.setCategory("Packaging");	*/		
					ppaMaterialMapping.setSubcategory(sseMaterial.getFinalCategory());
					ppaMaterialMapping.setTargetSystem(bpcsMaterial.getSystem());
					ppaMaterialMapping.setSourceContractStartDate(sseMaterial.getContractStartDate());
					ppaMaterialMapping.setSourceContractEndDate(sseMaterial.getContractEndDate());
					ppaMaterialMapping.setSourceCurrency(sseMaterial.getCurrency());
					ppaMaterialMapping.setSourceIncoterm(sseMaterial.getIncoterm());
					ppaMaterialMapping.setSourceMinOrderQuantity(sseMaterial.getMinOrderQuantity());
					ppaMaterialMapping.setSourceLeadTime(sseMaterial.getLeadTime());
					ppaMaterialMapping.setSourceSAPPlant(sseMaterial.getSapPlant());
					ppaMaterialMapping.setTargetSAPPlant(bpcsMaterial.getSapPlant());
					ppaMaterialMapping.setTargetMaterialId(bpcsMaterial.getMaterialId());
					ppaMaterialMapping.setTargetPlant(bpcsMaterial.getPlant());
					ppaMaterialMapping.setTargetSpecCode(bpcsMaterial.getSpecCode());
					ppaMaterialMapping.setTargetMaterialName(bpcsMaterial.getMaterialName());
					ppaMaterialMapping.setTargetSupplier(bpcsMaterial.getSupplier());
					ppaMaterialMapping.setPurchaseHistory(bpcsMaterial.getPurchaseHistory());
					ppaMaterialMapping.setTargetSupplierId(bpcsMaterial.getSupplierId());
					ppaMaterialMapping.setTargetPrice(bpcsMaterial.getPrice());
                     ppaMaterialMapping.setMaterialCreationDate(bpcsMaterial.getMaterialCreationDate());
                     ppaMaterialMapping.setCategory(bpcsMaterial.getCategory());
					ppaMaterialMapping.setDirection("dmtoplant");
					ppaMaterialMapping.setType("1");
					ppaMaterialMapping.setTypeDesc("Material Code");
					ppaMaterialMapping.setTypeCategory("Exact");
					ppaMaterialMapping.setWeightage(100);
				 //   System.out.println("SSSEgetMaterialName" + sseMaterial.getMaterialName() + "    BPCS : " + bpcsMaterial.getMaterialName());
					if (sseMaterial.getMaterialName() == null) sseMaterial.setMaterialName("");
					if (bpcsMaterial.getMaterialName() == null)bpcsMaterial.setMaterialName("");
					sseSynonymHashMap.put( sseMaterial.getMaterialName().toLowerCase().trim(), 
					bpcsMaterial.getMaterialName().toLowerCase().trim());
					bpcsSynonymHashMap.put(bpcsMaterial.getMaterialName().toLowerCase().trim(), sseMaterial.getMaterialName().toLowerCase().trim());
					synonymWithOutNumbersHashMap.put( sseMaterial.getMaterialName().toLowerCase().trim().replaceAll("[^a-zA-Z ]", ""), bpcsMaterial.getMaterialName().toLowerCase().trim().replaceAll("[^a-zA-Z ]", ""));
					
					materialMappingList.add(ppaMaterialMapping);
					if (removeDuplicated){
						bpcsPlantHashMap.get(bpcsMaterial.getPlant().trim().toLowerCase()).remove(bpcsMaterial);
					}
				} 
				if ((!matchFound || !removeDuplicated) && bpcsSpecCodeHashMap.containsKey(targetPlant+"-"+sseMaterial.getSpecCode().toLowerCase().trim().replaceFirst("^0+(?!$)", ""))){
					if (!sseMaterial.getSpecCode().toLowerCase().trim().replaceFirst("^0+(?!$)", "").equalsIgnoreCase("tbc")){
						matchFound = true;
						bpcsMaterial = bpcsSpecCodeHashMap.get(targetPlant+"-"+sseMaterial.getSpecCode().toLowerCase().trim().replaceFirst("^0+(?!$)", ""));
						PPAMaterialMapping ppaMaterialMapping = new PPAMaterialMapping();
						ppaMaterialMapping.setSourceSystem("SSE");
						ppaMaterialMapping.setSourcePlant(sseMaterial.getPlant());
						ppaMaterialMapping.setSourcePlantDesc(sseMaterial.getPlantDesc());
						ppaMaterialMapping.setSourceMaterialId(sseMaterial.getMaterialId());
						ppaMaterialMapping.setSourceSpecCode(sseMaterial.getSpecCode());
						//ppaMaterialMapping.setSourceMaterialName(sseMaterial.getMaterialName());
						ppaMaterialMapping.setSourceMaterialName(sseMaterial.getMaterialGroup());					
						ppaMaterialMapping.setSourceSupplier(sseMaterial.getSupplier());
						ppaMaterialMapping.setVendorNo(sseMaterial.getGalaxyVendorNo());
						ppaMaterialMapping.setVendorCity(sseMaterial.getGalaxyVendorCity());
						ppaMaterialMapping.setSourcePrice(sseMaterial.getPrice());
//						ppaMaterialMapping.setContractStatus(sseMaterial.getContractStatus());
						ppaMaterialMapping.setGalaxyUom(sseMaterial.getGalaxyUOM());
						ppaMaterialMapping.setCategory("Not found"); //sseMaterial.getMaterialGroup()
						ppaMaterialMapping.setSubcategory(sseMaterial.getFinalCategory());
						ppaMaterialMapping.setTargetSystem(bpcsMaterial.getSystem());
						ppaMaterialMapping.setSourceContractStartDate(sseMaterial.getContractStartDate());
						ppaMaterialMapping.setSourceContractEndDate(sseMaterial.getContractEndDate());
						ppaMaterialMapping.setSourceCurrency(sseMaterial.getCurrency());
						ppaMaterialMapping.setSourceIncoterm(sseMaterial.getIncoterm());
						ppaMaterialMapping.setSourceMinOrderQuantity(sseMaterial.getMinOrderQuantity());
						ppaMaterialMapping.setSourceLeadTime(sseMaterial.getLeadTime());
						ppaMaterialMapping.setSourceSAPPlant(sseMaterial.getSapPlant());
						ppaMaterialMapping.setTargetSAPPlant(bpcsMaterial.getSapPlant());
						ppaMaterialMapping.setTargetMaterialId(bpcsMaterial.getMaterialId());
						ppaMaterialMapping.setTargetPlant(bpcsMaterial.getPlant());
						ppaMaterialMapping.setTargetSpecCode(bpcsMaterial.getSpecCode());
						ppaMaterialMapping.setTargetMaterialName(bpcsMaterial.getMaterialName());
						ppaMaterialMapping.setTargetSupplier(bpcsMaterial.getSupplier());
						ppaMaterialMapping.setTargetSupplierId(bpcsMaterial.getSupplierId());
						ppaMaterialMapping.setPurchaseHistory(bpcsMaterial.getPurchaseHistory());
						ppaMaterialMapping.setTargetPrice(bpcsMaterial.getPrice());
                                                ppaMaterialMapping.setMaterialCreationDate(bpcsMaterial.getMaterialCreationDate());
						ppaMaterialMapping.setDirection("dmtoplant");
						ppaMaterialMapping.setType("2");
						ppaMaterialMapping.setTypeDesc("Spec Code");
						ppaMaterialMapping.setTypeCategory("Tentative");
						ppaMaterialMapping.setWeightage(90);						
						materialMappingList.add(ppaMaterialMapping);
						if (removeDuplicated){
							bpcsPlantHashMap.get(bpcsMaterial.getPlant().trim().toLowerCase()).remove(bpcsMaterial);
						}
					}
				} 
				if ((!matchFound || !removeDuplicated) && bpcsMaterialNameHashMap.containsKey(targetPlant+"-"+sseMaterial.getMaterialName().toLowerCase().trim().replaceAll("[^a-zA-Z0-9 ]", ""))){
					matchFound = true;
					bpcsMaterial = bpcsMaterialNameHashMap.get(targetPlant+"-"+sseMaterial.getMaterialName().toLowerCase().trim().replaceAll("[^a-zA-Z0-9 ]", ""));
					PPAMaterialMapping ppaMaterialMapping = new PPAMaterialMapping();
					ppaMaterialMapping.setSourceSystem("SSE");
					ppaMaterialMapping.setSourcePlant(sseMaterial.getPlant());
					ppaMaterialMapping.setSourcePlantDesc(sseMaterial.getPlantDesc());
					ppaMaterialMapping.setSourceMaterialId(sseMaterial.getMaterialId());
					ppaMaterialMapping.setSourceSpecCode(sseMaterial.getSpecCode());
					//ppaMaterialMapping.setSourceMaterialName(sseMaterial.getMaterialName());
					ppaMaterialMapping.setSourceMaterialName(sseMaterial.getMaterialGroup());					
					ppaMaterialMapping.setSourceSupplier(sseMaterial.getSupplier());
					ppaMaterialMapping.setVendorNo(sseMaterial.getGalaxyVendorNo());
					ppaMaterialMapping.setVendorCity(sseMaterial.getGalaxyVendorCity());
					ppaMaterialMapping.setSourcePrice(sseMaterial.getPrice());
//					ppaMaterialMapping.setContractStatus(sseMaterial.getContractStatus());
					ppaMaterialMapping.setGalaxyUom(sseMaterial.getGalaxyUOM());
					ppaMaterialMapping.setCategory("Not found"); //sseMaterial.getMaterialGroup()
					ppaMaterialMapping.setSubcategory(sseMaterial.getFinalCategory());
					ppaMaterialMapping.setSourceContractStartDate(sseMaterial.getContractStartDate());
					ppaMaterialMapping.setSourceContractEndDate(sseMaterial.getContractEndDate());
					ppaMaterialMapping.setSourceCurrency(sseMaterial.getCurrency());
					ppaMaterialMapping.setSourceIncoterm(sseMaterial.getIncoterm());
					ppaMaterialMapping.setSourceMinOrderQuantity(sseMaterial.getMinOrderQuantity());
					ppaMaterialMapping.setSourceLeadTime(sseMaterial.getLeadTime());
					ppaMaterialMapping.setSourceSAPPlant(sseMaterial.getSapPlant());
					ppaMaterialMapping.setTargetSAPPlant(bpcsMaterial.getSapPlant());
					ppaMaterialMapping.setTargetSystem(bpcsMaterial.getSystem());
					ppaMaterialMapping.setTargetMaterialId(bpcsMaterial.getMaterialId());
					ppaMaterialMapping.setTargetPlant(bpcsMaterial.getPlant());
					ppaMaterialMapping.setTargetSpecCode(bpcsMaterial.getSpecCode());
					ppaMaterialMapping.setTargetMaterialName(bpcsMaterial.getMaterialName());
					ppaMaterialMapping.setTargetSupplier(bpcsMaterial.getSupplier());
					ppaMaterialMapping.setTargetSupplierId(bpcsMaterial.getSupplierId());
					ppaMaterialMapping.setTargetPrice(bpcsMaterial.getPrice());
					ppaMaterialMapping.setPurchaseHistory(bpcsMaterial.getPurchaseHistory());
                                        ppaMaterialMapping.setMaterialCreationDate(bpcsMaterial.getMaterialCreationDate());
					ppaMaterialMapping.setDirection("dmtoplant");
					ppaMaterialMapping.setType("3");
					ppaMaterialMapping.setTypeDesc("Exact Material Desc");
					ppaMaterialMapping.setTypeCategory("Tentative");
					ppaMaterialMapping.setWeightage(70);						
					materialMappingList.add(ppaMaterialMapping);
				}
				/*if (!matchFound || !removeDuplicated){
					PPAMaterialMapping ppaMaterialMapping= new PPAMaterialMapping();
					ppaMaterialMapping.setSourceSystem("SSE");
					ppaMaterialMapping.setSourcePlant(sseMaterial.getPlant());
					ppaMaterialMapping.setSourcePlantDesc(sseMaterial.getPlantDesc());
					ppaMaterialMapping.setSourceMaterialId(sseMaterial.getMaterialId());
					ppaMaterialMapping.setSourceSpecCode(sseMaterial.getSpecCode());
					ppaMaterialMapping.setSourceMaterialName(sseMaterial.getMaterialName());
					ppaMaterialMapping.setSourceSupplier(sseMaterial.getSupplier());
					ppaMaterialMapping.setVendorNo(sseMaterial.getVendorNo());
					ppaMaterialMapping.setVendorCity(sseMaterial.getVendorCity());
					ppaMaterialMapping.setSourcePrice(sseMaterial.getPrice());
					ppaMaterialMapping.setContractStatus(sseMaterial.getContractStatus());
					ppaMaterialMapping.setCategory("Not found");
					ppaMaterialMapping.setSubcategory(sseMaterial.getFinalCategory());
					ppaMaterialMapping.setSourceContractStartDate(sseMaterial.getContractStartDate());
					ppaMaterialMapping.setSourceContractEndDate(sseMaterial.getContractEndDate());
					ppaMaterialMapping.setSourceCurrency(sseMaterial.getCurrency());
					ppaMaterialMapping.setSourceIncoterm(sseMaterial.getIncoterm());
					ppaMaterialMapping.setSourceMinOrderQuantity(sseMaterial.getMinOrderQuantity());
					ppaMaterialMapping.setSourceLeadTime(sseMaterial.getLeadTime());
					ppaMaterialMapping.setSourceSAPPlant(sseMaterial.getSapPlant());
					ppaMaterialMapping.setDirection("dmtoplant");
					String sourceMaterialName = ppaMaterialMapping.getSourceMaterialName().replaceAll("[^a-zA-Z0-9 ]", "").toLowerCase();
					String sourceSupplierName = ppaMaterialMapping.getSourceSupplier().replaceAll("[^a-zA-Z0-9 ]", "").toLowerCase();
					if (!sourceMaterialName.trim().equalsIgnoreCase("") && !sourceSupplierName.trim().equalsIgnoreCase("")  && !sourceMaterialName.trim().equalsIgnoreCase("na")
							&& bpcsMaterials != null){
						float sourcePrice = ppaMaterialMapping.getSourcePrice();
						for (int i = bpcsMaterials.size(); i > 0 ; i--){
							bpcsMaterial = bpcsMaterials.get(i-1);
							String targetMaterialNameString = bpcsMaterial.getMaterialName().replaceAll("[^a-zA-Z0-9 ]", "").toLowerCase();
							String targetSupplierString = bpcsMaterial.getSupplier().replaceAll("[^a-zA-Z0-9 ]", "").toLowerCase();
							if (!targetMaterialNameString.trim().equalsIgnoreCase("") && !targetMaterialNameString.trim().equalsIgnoreCase("na")){
								float targetPrice = bpcsMaterial.getPrice();
								tempLVDistance = distance(sourceMaterialName,targetMaterialNameString);
								tempLVDistanceSupplier = distance(sourceSupplierName,targetSupplierString);
								float tempPriceDiff = 0;
								if (targetPrice != 0 && sourcePrice != 0){
									tempPriceDiff = ((targetPrice-sourcePrice)*100)/sourcePrice;
									if (Float.isInfinite(tempPriceDiff) || Float.isNaN(tempPriceDiff) ){
										tempPriceDiff = 100;
									}
								}
								Boolean supplierMatch =false;
								if (tempLVDistanceSupplier < 6 || (targetSupplierString.length() > 1 && sourceSupplierName.length()>1 && (targetSupplierString.contains(sourceSupplierName) || sourceSupplierName.contains(targetSupplierString)))){
									supplierMatch = true;
								}
								if ((Math.abs(tempPriceDiff) < 20 && priceDiff > tempPriceDiff)  && supplierMatch && ((lvDistance > tempLVDistance && tempLVDistance < 7) || (targetMaterialNameString.length() > 1 && sourceMaterialName.length()>1 && (targetMaterialNameString.contains(sourceMaterialName) || sourceMaterialName.contains(targetMaterialNameString))))){
									matchFound = true;
									lvDistance = tempLVDistance;
									priceDiff = tempPriceDiff;
									ppaMaterialMapping.setTargetSystem(bpcsMaterial.getSystem());
									ppaMaterialMapping.setTargetMaterialId(bpcsMaterial.getMaterialId());
									ppaMaterialMapping.setTargetPlant(bpcsMaterial.getPlant());
									ppaMaterialMapping.setTargetSpecCode(bpcsMaterial.getSpecCode());
									ppaMaterialMapping.setTargetMaterialName(bpcsMaterial.getMaterialName());
									ppaMaterialMapping.setTargetSupplier(bpcsMaterial.getSupplier());
									ppaMaterialMapping.setTargetSupplierId(bpcsMaterial.getSupplierId());
									ppaMaterialMapping.setTargetPrice(bpcsMaterial.getPrice());
									ppaMaterialMapping.setTargetSAPPlant(bpcsMaterial.getSapPlant());
									ppaMaterialMapping.setPurchaseHistory(bpcsMaterial.getPurchaseHistory());
                                                                        ppaMaterialMapping.setMaterialCreationDate(bpcsMaterial.getMaterialCreationDate());
									ppaMaterialMapping.setLvDistnaceDesc(lvDistance);
									ppaMaterialMapping.setLvDistnaceSupplier(tempLVDistanceSupplier);
									ppaMaterialMapping.setPriceDiff(tempPriceDiff);
									ppaMaterialMapping.setPriceDiffByPrice(tempPriceDiff);
									ppaMaterialMapping.setDirection("dmtoplant");
									ppaMaterialMapping.setType("5");
									ppaMaterialMapping.setTypeDesc("Close Description and Supplier");
									ppaMaterialMapping.setTypeCategory("Tentative");
									ppaMaterialMapping.setWeightage(40);
								} 	
							}
						}
					}
					if (matchFound)
						materialMappingList.add(ppaMaterialMapping);
				
			}*/
			if (!matchFound || !removeDuplicated){
				lvDistance = 100;
				//priceDiff = 100;
				PPAMaterialMapping ppaMaterialMapping = new PPAMaterialMapping();
				ppaMaterialMapping.setSourceSystem("SSE");
				ppaMaterialMapping.setSourcePlant(sseMaterial.getPlant());
				ppaMaterialMapping.setSourcePlantDesc(sseMaterial.getPlantDesc());
				ppaMaterialMapping.setSourceMaterialId(sseMaterial.getMaterialId());
				ppaMaterialMapping.setSourceSpecCode(sseMaterial.getSpecCode());
				//ppaMaterialMapping.setSourceMaterialName(sseMaterial.getMaterialName());
				ppaMaterialMapping.setSourceMaterialName(sseMaterial.getMaterialGroup());					
				ppaMaterialMapping.setSourceSupplier(sseMaterial.getSupplier());
				ppaMaterialMapping.setVendorNo(sseMaterial.getGalaxyVendorNo());
				ppaMaterialMapping.setVendorCity(sseMaterial.getGalaxyVendorCity());
				ppaMaterialMapping.setSourcePrice(sseMaterial.getPrice());
				//ppaMaterialMapping.setContractStatus(sseMaterial.getContractStatus());
				ppaMaterialMapping.setGalaxyUom(sseMaterial.getGalaxyUOM());
				ppaMaterialMapping.setCategory("Not found");
				ppaMaterialMapping.setSubcategory(sseMaterial.getFinalCategory());
				ppaMaterialMapping.setSourceContractStartDate(sseMaterial.getContractStartDate());
				ppaMaterialMapping.setSourceContractEndDate(sseMaterial.getContractEndDate());
				ppaMaterialMapping.setSourceCurrency(sseMaterial.getCurrency());
				ppaMaterialMapping.setSourceIncoterm(sseMaterial.getIncoterm());
				ppaMaterialMapping.setSourceMinOrderQuantity(sseMaterial.getMinOrderQuantity());
				ppaMaterialMapping.setSourceLeadTime(sseMaterial.getLeadTime());
				ppaMaterialMapping.setSourceSAPPlant(sseMaterial.getSapPlant());
				String sourceMaterialName = ppaMaterialMapping.getSourceMaterialName().replaceAll("[^a-zA-Z0-9 ]", "").toLowerCase();
				String sourceSupplierName = ppaMaterialMapping.getSourceSupplier().replaceAll("[^a-zA-Z0-9 ]", "").toLowerCase();
				float sourcePrice = ppaMaterialMapping.getSourcePrice();
				if (!sourceMaterialName.trim().equalsIgnoreCase("")  && !sourceMaterialName.trim().equalsIgnoreCase("na")
						&& bpcsMaterials != null){
					for (int i = bpcsMaterials.size(); i > 0 ; i--){
						bpcsMaterial = bpcsMaterials.get(i-1);
						String targetMaterialNameString = bpcsMaterial.getMaterialName().replaceAll("[^a-zA-Z0-9 ]", "").toLowerCase();
						String targetSupplierString = bpcsMaterial.getSupplier().replaceAll("[^a-zA-Z0-9 ]", "").toLowerCase();
						if (!targetMaterialNameString.trim().equalsIgnoreCase("")  && !targetMaterialNameString.trim().equalsIgnoreCase("na")){
							float targetPrice = bpcsMaterial.getPrice();
							tempLVDistance = distance(sourceMaterialName,targetMaterialNameString);
							tempLVDistanceSupplier = distance(sourceSupplierName,targetSupplierString);
							float tempPriceDiff = 0;
							if (targetPrice != 0 && sourcePrice != 0){
								tempPriceDiff = ((targetPrice-sourcePrice)*100)/sourcePrice;
								if (Float.isInfinite(tempPriceDiff) || Float.isNaN(tempPriceDiff)){
									tempPriceDiff = 100;
								}
							}
							if ((lvDistance > tempLVDistance && tempLVDistance < 7) || (targetMaterialNameString.length() > 1 && sourceMaterialName.length()>1 && (targetMaterialNameString.contains(sourceMaterialName) || sourceMaterialName.contains(targetMaterialNameString)))){
								lvDistance = tempLVDistance;
								//priceDiff = tempPriceDiff;
								matchFound = true;
								ppaMaterialMapping.setTargetSystem(sseMaterial.getSystem());
								ppaMaterialMapping.setTargetMaterialId(bpcsMaterial.getMaterialId());
								ppaMaterialMapping.setTargetPlant(bpcsMaterial.getPlant());
								ppaMaterialMapping.setTargetSpecCode(bpcsMaterial.getSpecCode());
								ppaMaterialMapping.setTargetMaterialName(bpcsMaterial.getMaterialName());
								ppaMaterialMapping.setTargetSupplier(bpcsMaterial.getSupplier());
								ppaMaterialMapping.setTargetSupplierId(bpcsMaterial.getSupplierId());
								ppaMaterialMapping.setTargetPrice(bpcsMaterial.getPrice());
								ppaMaterialMapping.setTargetSAPPlant(bpcsMaterial.getSapPlant());
								ppaMaterialMapping.setPurchaseHistory(bpcsMaterial.getPurchaseHistory());
                                                                ppaMaterialMapping.setMaterialCreationDate(bpcsMaterial.getMaterialCreationDate());
								ppaMaterialMapping.setLvDistnaceDesc(lvDistance);
								ppaMaterialMapping.setLvDistnaceSupplier(tempLVDistanceSupplier);
								ppaMaterialMapping.setPriceDiff(tempPriceDiff);
								ppaMaterialMapping.setPriceDiffByPrice(tempPriceDiff);
								ppaMaterialMapping.setDirection("dmtoplant");
								ppaMaterialMapping.setType("6");
								ppaMaterialMapping.setTypeDesc("Close Description");
								ppaMaterialMapping.setTypeCategory("Tentative");
								ppaMaterialMapping.setWeightage(30);
							} 	
						}
					}
				}
				if (matchFound)
					materialMappingList.add(ppaMaterialMapping);
			}
			/*if (!matchFound || !removeDuplicated){
				PPAMaterialMapping ppaMaterialMapping = new PPAMaterialMapping();
				ppaMaterialMapping.setSourceSystem("SSE");
				ppaMaterialMapping.setSourcePlant(sseMaterial.getPlant());
				ppaMaterialMapping.setSourcePlantDesc(sseMaterial.getPlantDesc());
				ppaMaterialMapping.setSourceMaterialId(sseMaterial.getMaterialId());
				ppaMaterialMapping.setSourceSpecCode(sseMaterial.getSpecCode());
				ppaMaterialMapping.setSourceMaterialName(sseMaterial.getMaterialName());
				ppaMaterialMapping.setSourceSupplier(sseMaterial.getSupplier());
				ppaMaterialMapping.setVendorNo(sseMaterial.getVendorNo());
				ppaMaterialMapping.setVendorCity(sseMaterial.getVendorCity());
				ppaMaterialMapping.setSourcePrice(sseMaterial.getPrice());
				ppaMaterialMapping.setContractStatus(sseMaterial.getContractStatus());
				ppaMaterialMapping.setCategory("Not found");
				ppaMaterialMapping.setSubcategory(sseMaterial.getFinalCategory());
				ppaMaterialMapping.setSourceContractStartDate(sseMaterial.getContractStartDate());
				ppaMaterialMapping.setSourceContractEndDate(sseMaterial.getContractEndDate());
				ppaMaterialMapping.setSourceCurrency(sseMaterial.getCurrency());
				ppaMaterialMapping.setSourceIncoterm(sseMaterial.getIncoterm());
				ppaMaterialMapping.setSourceMinOrderQuantity(sseMaterial.getMinOrderQuantity());
				ppaMaterialMapping.setSourceLeadTime(sseMaterial.getLeadTime());
				ppaMaterialMapping.setSourceSAPPlant(sseMaterial.getSapPlant());				
				String sourceMaterialName = ppaMaterialMapping.getSourceMaterialName().replaceAll("[^a-zA-Z0-9 ]", "").toLowerCase();
				String sourceSupplierName = ppaMaterialMapping.getSourceSupplier().replaceAll("[^a-zA-Z0-9 ]", "").toLowerCase();
				if (!sourceMaterialName.trim().equalsIgnoreCase("") && !sourceSupplierName.trim().equalsIgnoreCase("") && !sourceMaterialName.trim().equalsIgnoreCase("na")){
					float sourcePrice = ppaMaterialMapping.getSourcePrice();
					for (int i = bpcsMaterials.size(); i > 0 ; i--){
						
						bpcsMaterial = bpcsMaterials.get(i-1);
						String targetMaterialNameString = bpcsMaterial.getMaterialName().replaceAll("[^a-zA-Z0-9 ]", "").toLowerCase();
						String targetSupplierString = bpcsMaterial.getSupplier().replaceAll("[^a-zA-Z0-9 ]", "").toLowerCase();
						if (!targetMaterialNameString.trim().equalsIgnoreCase("") && !targetMaterialNameString.trim().equalsIgnoreCase("") && !targetMaterialNameString.trim().equalsIgnoreCase("na")){
							float targetPrice = bpcsMaterial.getPrice();
							tempLVDistance = distance(sourceMaterialName,targetMaterialNameString);
							tempLVDistanceSupplier = distance(sourceSupplierName,targetSupplierString);
							float tempPriceDiff = 0;
							if (targetPrice != 0 && sourcePrice != 0){
								tempPriceDiff = ((targetPrice-sourcePrice)*100)/sourcePrice;
								if (Float.isInfinite(tempPriceDiff) || Float.isNaN(tempPriceDiff)){
									tempPriceDiff = 100;
								}
							}
							Boolean supplierMatch =false;
							if (tempLVDistanceSupplier < 6 || (targetSupplierString.length() > 1 && sourceSupplierName.length()>1 && (targetSupplierString.contains(sourceSupplierName) || sourceSupplierName.contains(targetSupplierString)))){
								supplierMatch = true;
							}
							if ((Math.abs(tempPriceDiff) < 20 && priceDiff > tempPriceDiff) && supplierMatch){
								matchFound = true; 
								lvDistance = tempLVDistance;
								priceDiff = tempPriceDiff;
								ppaMaterialMapping.setTargetSystem(sseMaterial.getSystem());
								ppaMaterialMapping.setTargetMaterialId(bpcsMaterial.getMaterialId());
								ppaMaterialMapping.setTargetPlant(bpcsMaterial.getPlant());
								ppaMaterialMapping.setTargetSpecCode(bpcsMaterial.getSpecCode());
								ppaMaterialMapping.setTargetMaterialName(bpcsMaterial.getMaterialName());
								ppaMaterialMapping.setTargetSupplier(bpcsMaterial.getSupplier());
								ppaMaterialMapping.setTargetSupplierId(bpcsMaterial.getSupplierId());
								ppaMaterialMapping.setPurchaseHistory(bpcsMaterial.getPurchaseHistory());
								ppaMaterialMapping.setTargetPrice(bpcsMaterial.getPrice());
								ppaMaterialMapping.setTargetSAPPlant(bpcsMaterial.getSapPlant());
								ppaMaterialMapping.setLvDistnaceDesc(lvDistance);
								ppaMaterialMapping.setLvDistnaceSupplier(tempLVDistanceSupplier);
								ppaMaterialMapping.setPriceDiff(tempPriceDiff);
								ppaMaterialMapping.setPriceDiffByPrice(tempPriceDiff);
								ppaMaterialMapping.setDirection("dmtoplant");
								ppaMaterialMapping.setType("7");
//								ppaMaterialMapping.setTypeDesc("Close Price");
								ppaMaterialMapping.setTypeCategory("Tentative");
								ppaMaterialMapping.setWeightage(20);
							} 	
						}
					}
				}
				if (matchFound)
					materialMappingList.add(ppaMaterialMapping);
			}*/
			if (!matchFound || !removeDuplicated){
				sseMaterialNotFoundList.add(sseMaterial);
			}
                        }catch(Exception ex) {System.out.println(ex.getMessage());ex.printStackTrace();}
		}
	}

	//List<Material> bpcsMaterials =  new ArrayList<Material>();
	boolean matchFound = false;
	for (int j= sseMaterialNotFoundList.size();j>0;j--){
            try{
		matchFound = false;
		lvDistance = 100;
		tempLVDistance = 100;
		tempLVDistanceSupplier =100;
		//tempLVDistanceSynonym = 100;
		sseMaterial = sseMaterialNotFoundList.get(j-1);
		PPAMaterialMapping ppaMaterialMapping = new PPAMaterialMapping();
		ppaMaterialMapping.setSourceSystem("SSE");
		ppaMaterialMapping.setSourcePlant(sseMaterial.getPlant());
		ppaMaterialMapping.setSourcePlantDesc(sseMaterial.getPlantDesc());
		ppaMaterialMapping.setSourceMaterialId(sseMaterial.getMaterialId());
		ppaMaterialMapping.setSourceSpecCode(sseMaterial.getSpecCode());
		//ppaMaterialMapping.setSourceMaterialName(sseMaterial.getMaterialName());
		ppaMaterialMapping.setSourceMaterialName(sseMaterial.getMaterialGroup());					
		ppaMaterialMapping.setSourceSupplier(sseMaterial.getSupplier());
		ppaMaterialMapping.setVendorNo(sseMaterial.getGalaxyVendorNo());
		ppaMaterialMapping.setVendorCity(sseMaterial.getGalaxyVendorCity());
		ppaMaterialMapping.setSourcePrice(sseMaterial.getPrice());
//		ppaMaterialMapping.setContractStatus(sseMaterial.getContractStatus());
		ppaMaterialMapping.setGalaxyUom(sseMaterial.getGalaxyUOM());
		ppaMaterialMapping.setCategory("Not found");
		ppaMaterialMapping.setSubcategory(sseMaterial.getFinalCategory());
		ppaMaterialMapping.setSourceContractStartDate(sseMaterial.getContractStartDate());
		ppaMaterialMapping.setSourceContractEndDate(sseMaterial.getContractEndDate());
		ppaMaterialMapping.setSourceCurrency(sseMaterial.getCurrency());
		ppaMaterialMapping.setSourceIncoterm(sseMaterial.getIncoterm());
		ppaMaterialMapping.setSourceMinOrderQuantity(sseMaterial.getMinOrderQuantity());
		ppaMaterialMapping.setSourceLeadTime(sseMaterial.getLeadTime());
		ppaMaterialMapping.setSourceSAPPlant(sseMaterial.getSapPlant());
		ppaMaterialMapping.setDirection("dmtoplant");
		String sourceMaterialName = ppaMaterialMapping.getSourceMaterialName().replaceAll("[^a-zA-Z0-9 ]", "").toLowerCase();
		String sourceMaterialNameSynonym = sseSynonymHashMap.get(ppaMaterialMapping.getSourceMaterialName().toLowerCase().trim());
		String sourceSupplierName = ppaMaterialMapping.getSourceSupplier().replaceAll("[^a-zA-Z0-9 ]", "").toLowerCase();
		if (!sourceMaterialName.trim().equalsIgnoreCase("") && !sourceSupplierName.trim().equalsIgnoreCase("")  && !sourceMaterialName.trim().equalsIgnoreCase("na")){
			float sourcePrice = ppaMaterialMapping.getSourcePrice();
			for (int i = bpcsMaterialList.size(); i > 0 ; i--){
				bpcsMaterial = bpcsMaterialList.get(i-1);
				String targetMaterialNameString = bpcsMaterial.getMaterialName().replaceAll("[^a-zA-Z0-9 ]", "").toLowerCase();
				String targetMaterialNameSynonym = bpcsSynonymHashMap.get(bpcsMaterial.getMaterialName().toLowerCase().trim());
				String targetSupplierString = bpcsMaterial.getSupplier().replaceAll("[^a-zA-Z0-9 ]", "").toLowerCase();
				if (!targetMaterialNameString.trim().equalsIgnoreCase("") && !targetMaterialNameString.trim().equalsIgnoreCase("na")){
					float targetPrice = bpcsMaterial.getPrice();
					tempLVDistance =100;
					tempLVDistanceSupplier = distance(sourceSupplierName,targetSupplierString);
					if (sourceMaterialNameSynonym != null && !sourceMaterialNameSynonym.equalsIgnoreCase("")){
						tempLVDistance = Math.min(tempLVDistance,distance(sourceMaterialNameSynonym,targetMaterialNameString));
						if (targetMaterialNameString.length() > 1 && sourceMaterialNameSynonym.length()>1 && (targetMaterialNameString.contains(sourceMaterialNameSynonym) || sourceMaterialNameSynonym.contains(targetMaterialNameString))){
							tempLVDistance =5;
						}
					}
					if (targetMaterialNameSynonym != null && !targetMaterialNameSynonym.equalsIgnoreCase("")){
						tempLVDistance = Math.min(tempLVDistance,distance(sourceMaterialName,targetMaterialNameSynonym));
						if (targetMaterialNameSynonym.length() > 1 && sourceMaterialName.length()>1 && (targetMaterialNameSynonym.contains(sourceMaterialName) || sourceMaterialName.contains(targetMaterialNameSynonym))){
							tempLVDistance =5;
						}
					}
					if (sourceMaterialNameSynonym != null && !sourceMaterialNameSynonym.equalsIgnoreCase("") && targetMaterialNameSynonym != null && !targetMaterialNameSynonym.equalsIgnoreCase("")){
						tempLVDistance = Math.min(tempLVDistance,distance(sourceMaterialNameSynonym,targetMaterialNameSynonym));
						if (targetMaterialNameSynonym.length() > 1 && sourceMaterialNameSynonym.length()>1 && (targetMaterialNameSynonym.contains(sourceMaterialNameSynonym) || sourceMaterialNameSynonym.contains(targetMaterialNameSynonym))){
							tempLVDistance =5;
						}
					}
					
				float tempPriceDiff = 0;
					if (targetPrice != 0 && sourcePrice != 0){
						tempPriceDiff = ((targetPrice-sourcePrice)*100)/sourcePrice;
						if (Float.isInfinite(tempPriceDiff) || Float.isNaN(tempPriceDiff) ){
							tempPriceDiff = 100;
						}
					}
					/*Boolean supplierMatch =false;
					if (tempLVDistanceSupplier < 6 || (targetSupplierString.length() > 1 && sourceSupplierName.length()>1 && (targetSupplierString.contains(sourceSupplierName) || sourceSupplierName.contains(targetSupplierString)))){
						supplierMatch = true;
					}*/
					if  (lvDistance > tempLVDistance && tempLVDistance < 7){
						matchFound = true;
						lvDistance = tempLVDistance;
						//priceDiff = tempPriceDiff;
						ppaMaterialMapping.setTargetSystem(bpcsMaterial.getSystem());
						ppaMaterialMapping.setTargetMaterialId(bpcsMaterial.getMaterialId());
						ppaMaterialMapping.setTargetPlant(bpcsMaterial.getPlant());
						ppaMaterialMapping.setTargetSpecCode(bpcsMaterial.getSpecCode());
						ppaMaterialMapping.setTargetMaterialName(bpcsMaterial.getMaterialName());
						ppaMaterialMapping.setTargetSupplier(bpcsMaterial.getSupplier());
						ppaMaterialMapping.setTargetSupplierId(bpcsMaterial.getSupplierId());
						ppaMaterialMapping.setTargetPrice(bpcsMaterial.getPrice());
						ppaMaterialMapping.setTargetSAPPlant(bpcsMaterial.getSapPlant());
						ppaMaterialMapping.setPurchaseHistory(bpcsMaterial.getPurchaseHistory());
                        ppaMaterialMapping.setMaterialCreationDate(bpcsMaterial.getMaterialCreationDate());
						ppaMaterialMapping.setLvDistnaceDesc(lvDistance);
						ppaMaterialMapping.setLvDistnaceSupplier(tempLVDistanceSupplier);
						ppaMaterialMapping.setPriceDiff(tempPriceDiff);
						ppaMaterialMapping.setPriceDiffByPrice(tempPriceDiff);
						ppaMaterialMapping.setType("8");
						ppaMaterialMapping.setTypeDesc("Synonym Close Description");
						ppaMaterialMapping.setTypeCategory("Tentative");
						ppaMaterialMapping.setWeightage(20);
					} 	
				}
			}
			if (!matchFound){
				ppaMaterialMapping.setTargetPlant(ppaMaterialMapping.getSourcePlant());
				ppaMaterialMapping.setTargetSystem("BPCS");
				ppaMaterialMapping.setType("4");
				ppaMaterialMapping.setTypeDesc("None");
				ppaMaterialMapping.setTypeCategory("None");
				ppaMaterialMapping.setWeightage(0);
			}
			materialMappingList.add(ppaMaterialMapping);
		}
               }catch(Exception ex) {System.out.println(ex.getMessage());ex.printStackTrace();} 
	}
	}catch(Exception ex ){
		ex.printStackTrace();
		Log.Error("Error on comparing SSE To BPCS. Error :" + ex.getMessage());
   }
	if(materialMappingList != null ){
		System.out.println("Finished comparing SSE To BPCS");	
	}
	return materialMappingList;
	}
	public ArrayList<PPAMaterialMapping>  compareAndDownloadMaterialMapping(Object... obj) throws AppException {
		ArrayList<PPAMaterialMapping> materialMappings = getMaterialMappingAlgorithm(obj);

		
		return materialMappings;

	}
	
	
	
	
}
