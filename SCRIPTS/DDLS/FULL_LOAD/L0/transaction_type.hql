DROP TABLE

IF EXISTS rlsf_prod.transaction_type;
	CREATE EXTERNAL TABLE rlsf_prod.TRANSACTION_TYPE (
		TRANSACTION_TYPE_ID string
		,CREATION_DATE string
		,LAST_UPDATE_DATE string
		,TRANSACTION_TYPE_NAME string
		,DESCRIPTION string
		) ROW FORMAT DELIMITED FIELDS TERMINATED BY '|' STORED AS TEXTFILE LOCATION 'hdfs://nameservice1/sch/l0/transaction_type';