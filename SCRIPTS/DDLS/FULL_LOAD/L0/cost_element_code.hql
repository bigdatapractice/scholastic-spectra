DROP TABLE

IF EXISTS rlsf_prod.cost_element_code;
	CREATE EXTERNAL TABLE rlsf_prod.cost_element_code (
		cost_element_id STRING
		,cost_element_code STRING
		) ROW FORMAT DELIMITED FIELDS TERMINATED BY '|' STORED AS TEXTFILE LOCATION 'hdfs://nameservice1/sch/l3/cost_element_code';