DROP TABLE

IF EXISTS rlsf_prod.cost_sub_inventory;
	CREATE EXTERNAL TABLE rlsf_prod.cost_sub_inventory (
		cst_inv_transaction_id STRING
		,external_system_ref_id STRING
		,inventory_org_id STRING
		,inventory_item_id STRING
		,subinventory_code STRING
		,code_combination_id STRING
		,transaction_qty STRING
		,rec_trxn_id STRING
		) ROW FORMAT DELIMITED FIELDS TERMINATED BY '|' STORED AS TEXTFILE LOCATION 'hdfs://nameservice1/sch/l0/cost_sub_inventory';