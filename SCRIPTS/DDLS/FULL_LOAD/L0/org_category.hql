DROP TABLE

IF EXISTS rlsf_prod.org_category;
	CREATE EXTERNAL TABLE rlsf_prod.org_category (
		org STRING
		,category STRING
		,loc STRING
		) ROW FORMAT DELIMITED FIELDS TERMINATED BY '|' STORED AS TEXTFILE LOCATION 'hdfs://nameservice1/sch/l0/org_category';