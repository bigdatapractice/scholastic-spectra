DROP TABLE IF EXISTS rlsf_prod.ohq_config_date;
CREATE EXTERNAL TABLE rlsf_prod.ohq_config_date (   run_date STRING,   
as_of_date STRING ) STORED AS PARQUET LOCATION '/sch/l1/ohq_config_date';
