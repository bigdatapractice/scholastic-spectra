DROP TABLE IF EXISTS rlsf_prod.po_cloud_gap;
CREATE EXTERNAL TABLE rlsf_prod.po_cloud_gap (
cost_center STRING,
po_num STRING,
item_num STRING,
oracle_id STRING,
isbn13 STRING,
title STRING,
transaction_date STRING,
qty_cloud decimal(38,6),
po_grp STRING
) STORED AS PARQUET LOCATION '/sch/l1/po_cloud_gap';