DROP TABLE IF EXISTS rlsf_prod.txn_cloud_gap;
CREATE EXTERNAL TABLE rlsf_prod.txn_cloud_gap (
transaction_id STRING,
item_num STRING,
oracle_id STRING,
isbn13 STRING,
title STRING,
adjustment_reason STRING,
transaction_date STRING,
qty_cloud decimal(38,6),
txn_grp STRING
) STORED AS PARQUET LOCATION '/sch/l1/txn_cloud_gap';