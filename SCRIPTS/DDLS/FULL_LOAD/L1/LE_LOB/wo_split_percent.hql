CREATE EXTERNAL TABLE rlsf_prod.wo_split_percent (
organization_id STRING,
work_order_number STRING,
inventory_item_id STRING,
creation_date STRING,
last_update_date STRING,
lob_forecast decimal(38,6),
major_channel STRING,
wo_qty_percent decimal(38,6)
) STORED AS PARQUET LOCATION '/sch/l1/wo_split_percent';