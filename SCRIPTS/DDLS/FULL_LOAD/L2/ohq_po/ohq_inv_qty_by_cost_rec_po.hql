DROP TABLE IF EXISTS rlsf_prod.ohq_inv_qty_by_cost_rec_po;
CREATE EXTERNAL TABLE rlsf_prod.ohq_inv_qty_by_cost_rec_po (
transactionseoinventoryitemid STRING,
costdistributionseolayerquantity DECIMAL(38,6),
transactionseoinventoryorgid STRING,
transactionseocreationdate STRING,
transactionseocstinvtransactionid STRING,
cstlayercostspeorectrxnid STRING,
txn_external_system_ref_id STRING,
cstlayercosttransid STRING,
po_number STRING,
transaction_type_name STRING
) STORED AS PARQUET LOCATION '/sch/l2/ohq_po/ohq_inv_qty_by_cost_rec_po';