DROP TABLE IF EXISTS rlsf_prod.le_lob_wms_detail;
CREATE EXTERNAL TABLE rlsf_prod.le_lob_wms_detail (
run_date STRING,
item STRING,
isbn13 STRING,
title STRING,
source STRING,
dr_legal_entity STRING,
dr_line_of_business STRING,
layer_quantity decimal(38,6),
split_percent decimal(38,6),
qty_percent decimal(38,6),
wms_qty decimal(38,6),
final_quantity decimal(38,6)
) STORED AS PARQUET LOCATION '/sch/l2/le_lob_wms_detail';