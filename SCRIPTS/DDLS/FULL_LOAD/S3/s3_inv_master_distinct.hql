DROP TABLE

IF EXISTS rlsf_prod.s3_inv_master_distinct;
	CREATE EXTERNAL TABLE rlsf_prod.s3_inv_master_distinct (
		oracle_item_id STRING
		,inventory_item_id STRING
		,DESCRIPTION string
		,ITEM_TYPE string
		) partitioned by (run_date string) STORED AS PARQUET LOCATION 's3a://schl-ams-nonprod-spectra/Data/inv_master_distinct';