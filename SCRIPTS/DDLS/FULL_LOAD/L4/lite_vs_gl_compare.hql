DROP TABLE

IF EXISTS rlsf_prod.lite_vs_gl_compare;
	CREATE EXTERNAL TABLE rlsf_prod.lite_vs_gl_compare (
		cost_center string
		,item string
		,subinventory_code string
		,inventory_item_flag string
		,inventory_asset_flag string
		,costing_enabled_flag string
		,external_sysref_id_ string
		,txn_id string
		,external_sys_ref_id_status string
		,lite_qty DOUBLE
		,organization_id string
		,item_id string
		,transaction_date string
		,imt_creation_date string
		,last_update_date string
		,source string
		,legacy_transaction_type string
		,legacy_reason string
		,source_line string
		,costed_flag string
		,reference string
		,gl_reference string
		,reference_status_ string
		,run_date string
		,days_diff INT
		) STORED AS PARQUET LOCATION '/sch/l4/lite_vs_gl_compare';