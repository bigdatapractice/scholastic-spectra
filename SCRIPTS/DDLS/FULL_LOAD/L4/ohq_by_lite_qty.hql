DROP TABLE

IF EXISTS rlsf_prod.ohq_by_lite_qty;
	CREATE EXTERNAL TABLE rlsf_prod.ohq_by_lite_qty (
		organization_code STRING
		,as_of_date STRING
		,isbn13 STRING
		,oracle_id STRING
		,title STRING
		,inventory_item_flag STRING
		,inventory_asset_flag STRING
		,costing_enabled_flag STRING
		,item_type STRING
		,item_category STRING
		,org_category STRING
		,subinventory_code STRING
		,lite_qty DECIMAL(38, 0)
		,adj_cloud_qty DECIMAL(38, 0)
		,avg_unit_cost DECIMAL(38, 4)
		,total_amount DECIMAL(38, 4)
		) STORED AS PARQUET LOCATION 'hdfs://nameservice1/sch/l4/ohq_by_lite_qty';