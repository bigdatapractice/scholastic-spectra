drop table if exists rlsf_prod.isbn_activity_agg;
create external table rlsf_prod.isbn_activity_agg (
ref_number string,
proforma_ref string,
isbn13 string,
title string,
cop_amt decimal(38,6),
cop_qty decimal(38,6),
legacy_reason string,
legacy_trans_type string,
period string,
transaction_date string,
isbn_Legal_Entity string,
isbn_Line_of_Business string,
isbn_Account string,
isbn_Cost_Center string,
isbn_Product_Line string,
isbn_Channel string,
isbn_Loc string,
isbn_Interco string
) stored as parquet location '/sch/l3/isbn_activity_agg';