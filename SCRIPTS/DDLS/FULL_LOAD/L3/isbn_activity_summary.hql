DROP TABLE IF EXISTS rlsf_prod.isbn_activity_summary;
CREATE EXTERNAL TABLE rlsf_prod.isbn_activity_summary (
organization_code STRING,
item STRING,
short_id STRING,
transaction_type STRING,
reference STRING,
legacy_transaction_type STRING,
legacy_reason STRING,
transaction_source STRING,
source STRING,
gl_batch_desc STRING,
jrnl_batch_name STRING,
dr_jrnl_line_num STRING,
cr_jrnl_line_num STRING,
dr_account_num STRING,
cr_account_num STRING,
1421000_flag STRING,
qty DECIMAL(38,6),
amount DECIMAL(38,6),
total_amount DECIMAL(38,6),
layer_quantity DECIMAL(38,6),
cost_unit DECIMAL(38,6),
entered_dr DECIMAL(38,6),
period STRING,
imt_creation_date STRING,
imt_creation_time STRING,
transaction_date STRING,
transaction_time STRING,
journal_creation_date STRING,
journal_creation_time STRING,
txn_transaction_id STRING,
rec_trxn_id STRING,
source_line STRING,
title STRING,
item_type STRING,
opus_item STRING,
isbn13 STRING,
dr_legal_entity STRING,
dr_line_of_business STRING,
dr_account STRING,
dr_cost_center STRING,
dr_product_line STRING,
dr_channel STRING,
dr_loc STRING,
dr_interco STRING,
cr_legal_entity STRING,
cr_line_of_business STRING,
cr_account STRING,
cr_cost_center STRING,
cr_product_line STRING,
cr_channel STRING,
cr_loc STRING,
cr_interco STRING,
txn_external_system_ref_id STRING,
po_wo STRING,
destination_org STRING,
item_category STRING,
category_description STRING,
journal STRING,
accounting_class_code STRING,
po_wo_transaction_type STRING,
po_wo_transaction_source STRING
) STORED AS PARQUET LOCATION 'hdfs://nameservice1/sch/l3/isbn_activity_summary';