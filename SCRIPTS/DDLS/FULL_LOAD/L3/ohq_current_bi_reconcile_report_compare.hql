DROP TABLE

IF EXISTS rlsf_prod.ohq_current_bi_reconcile_report_compare;
	CREATE EXTERNAL TABLE rlsf_prod.ohq_current_bi_reconcile_report_compare (
		hadoop_item_id String
		,hadoop_cost_center String
		,Hadoop_Layer String
		,isbn13 String
		,border_item String
		,description String
		,opus_item String
		,inv String
		,costed String
		,asset String
		,item_type String
		,Category_code String
		,Category_Name String
		,category_description String
		,Catalog_code String
		,cloud_item_id String
		,cloud_cost_center String
		,cloud_Layer String
		,hadoop_tot_qty DECIMAL(38, 2)
		,cloud_quantity DECIMAL(38, 2)
		,quantity_status String
		,diff_qty DECIMAL(38, 2)
		,abs_diff_qty DECIMAL(38, 2)
		,Hadoop_Unit_cost DECIMAL(38, 2)
		,cloud_unit_cost DECIMAL(38, 2)
		,Unit_cost_Status String
		,hadoop_tot_amount DECIMAL(38, 2)
		,cloud_amount DECIMAL(38, 2)
		,amount_status String
		,diff_amount DECIMAL(38, 2)
		,abs_diff_amount DECIMAL(38, 2)
		) ROW FORMAT DELIMITED FIELDS TERMINATED BY '|'
		WITH SERDEPROPERTIES('field.delim' = '|', 'serialization.format' = '|') STORED AS TEXTFILE LOCATION '/sch/l0/ohq_current_bi_reconcile_report_compare';