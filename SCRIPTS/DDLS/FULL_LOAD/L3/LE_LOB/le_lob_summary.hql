DROP TABLE IF EXISTS rlsf_prod.le_lob_summary;
CREATE EXTERNAL TABLE rlsf_prod.le_lob_summary (
item STRING,
isbn13 STRING,
title STRING,
legal_entity STRING,
le_description STRING,
line_of_business STRING,
lob_description STRING,
layer_quantity decimal(38,6),
run_date STRING
) STORED AS PARQUET LOCATION '/sch/l3/le_lob_summary';