DROP TABLE IF EXISTS rlsf_prod.ohq_inv_onhand_qty_cost_by_layer;
CREATE EXTERNAL TABLE rlsf_prod.ohq_inv_onhand_qty_cost_by_layer (   date_ STRING,
   org_id STRING,
   oracle_item STRING,
   isbn13 STRING,
   border_item STRING,
   opus_id STRING,
   lc_p_eo_rec_trans_id STRING,
   layer_qty_sum DECIMAL(38,6),   layer_cost_sum DECIMAL(38,6),   total_amount DECIMAL(38,6),   layer_date STRING,
   trans_eo_inventory_item_id STRING,
   cost_center STRING,
   run_date STRING,
   as_of_date STRING ) 
   STORED AS PARQUET LOCATION '/sch/l3/ohq_inv_onhand_qty_cost_by_layer' ;
