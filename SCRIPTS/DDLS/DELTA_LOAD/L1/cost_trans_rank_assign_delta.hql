DROP TABLE IF EXISTS rlsf_prod.cost_trans_rank_assign_delta;
CREATE EXTERNAL TABLE rlsf_prod.cost_trans_rank_assign_delta (   transactionseoinventoryitemid STRING,
   ranknumber BIGINT ) STORED AS PARQUET LOCATION '/sch/l1/cost_trans_rank_assign_delta';
