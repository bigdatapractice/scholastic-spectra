#!/bin/bash

export PYTHON_EGG_CACHE=./myeggs
LOG=onhandqtyexec-$(date +%s%N).log

connect_impala=$1   # ip-10-130-13-108.ec2.internal:21000 -- dev server
batchsize=$2		# batch size 
QUERY_FILE=$3 		# execute_ohq_scripts_l2.hql or execute_ohq_scripts_l3.hql
TRUNCATE_TABLE=$4 	# truncate_ohq_scripts_l2.hql or truncate_ohq_scripts_l3.hql
QUERYFOLDER=$5
RANKTABLE=$6

echo " batch size " $batchsize

startrange=1
loopvariablestart=`expr $startrange`  # initilizing the variable

CONFIGMAX="select max(ranknumber) from ${RANKTABLE}" 
echo "<INFO> Running Query to get prevous max date"
maxranknumber=$(impala-shell -i $connect_impala  -q "${CONFIGMAX}" | sed '4q;d' | sed 's/|//g' |awk '{$1=$1};1') >> ${LOG} 2>&1

maxrangenumber=`expr $maxranknumber` #final number of range
if [ $maxrangenumber -eq 0 ];
then
	echo " no inventories found. hence exiting the program. Please check the logs"
	hadoop fs -put ${LOG} /sch/logs
	exit 4
fi

outofbound=0 # if in the loop the range crosses the end range limit in while loop we are making sure it considers till the end range
reduce_no_from_range=0

if [ ! -f $TRUNCATE_TABLE ];
then
  ERROR="Unable to access [ query-file: $TRUNCATE_TABLE ], check existence and permissions"
  echo "$ERROR" >> ${LOG}
  hadoop fs -put ${LOG} /sch/logs
  exit 3
fi

while IFS= read -r line || [ -n "$line" ]
 do  
		echo "executing ${line} $(date +%T)"
		impala-shell -i $connect_impala -q "truncate ${line}" >>${LOG} 2>&1
		RC=$?
		echo "After  execution: $(date +%T)"
		if [ $RC -eq 0 ];
			then 
				echo "table truncated " $line
			else
				hadoop fs -put ${LOG} /sch/logs
				exit 4
		fi 
done <$TRUNCATE_TABLE 


while [ $loopvariablestart -le $maxrangenumber ]
do
	outofbound=`expr $loopvariableend + 1 + $batchsize`
	if [ $outofbound -gt $maxrangenumber ];
	then 
		reduce_no_from_range=`expr $outofbound - $maxrangenumber`
		echo " number will be reduced from end range in the final loop is " $reduce_no_from_range
	fi 
	
    	loopvariableend=`expr $loopvariablestart + $batchsize - $reduce_no_from_range`
	echo "loop ranges are : start --" $loopvariablestart
	echo "loop ranges are : end  -- " $loopvariableend
	echo "Start the script : $(date +%T)"
	
	if [ ! -f $QUERY_FILE ];
	then
		ERROR="Unable to access [ query-file: $QUERY_FILE ], check existence and permissions"
		echo "$ERROR" >> ${LOG}
		hadoop fs -put ${LOG} /sch/logs
		exit 3
	fi
	
	filecounter=0
	while IFS= read -r line || [ -n "$line" ]
	do 		
			
			echo "executing ${line} $(date +%T)"
			impala-shell -i $connect_impala  -f ${QUERYFOLDER}/$line --var fromranknumber=$loopvariablestart --var toranknumber=$loopvariableend   >>${LOG} 2>&1
			RC=$?
			echo "After  execution: $(date +%T)"
			if [ $RC -eq 0 ];
				then 
					filecounter=`expr $filecounter + 1`
					echo "table has ran successfully "  $line
				else
					echo " error in " $line
					echo " check the logs in hdfs and rerun from failed step "
					hadoop fs -put ${LOG} /sch/logs
					exit 5
			fi
			

	done <$QUERY_FILE 
	echo " for ranges from  "  $loopvariablestart
	echo " to " $loopvariableend
	echo "is completed"
	echo "no of files executed in the loop for a given range is " $filecounter
	loopvariablestart=`expr $loopvariableend + 1` #assigning loop end number to loop start and loop continous with next set of ranges
done 
echo " loop compelted for all ranges , please check the logs in case of any failures. log name is " ${LOG}
rm ${LOG}