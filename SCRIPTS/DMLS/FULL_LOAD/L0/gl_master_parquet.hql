INVALIDATE METADATA rlsf_prod.gl_master;

INVALIDATE METADATA rlsf_prod.gl_master;

INSERT INTO TABLE rlsf_prod.gl_master_parquet
SELECT *
	,cast(from_utc_timestamp(now(), 'America/New_York') AS string)
FROM rlsf_prod.gl_master;

TRUNCATE rlsf_prod.gl_master;