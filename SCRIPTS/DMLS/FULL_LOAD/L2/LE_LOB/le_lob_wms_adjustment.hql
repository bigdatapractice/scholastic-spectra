insert overwrite table rlsf_prod.le_lob_wms_adjustment

with split_qty as (
select item, isbn13, title, source, max(qty_percent) as qty_percent,substr(dr_legal_entity,1,1) as le from rlsf_prod.le_lob_wms_detail 
where qty_percent <>0 --and item='190858A'
group by item, isbn13, title,le,source)
,
le as (
select wms.item, wms.isbn13, wms.title, wms.source, max(wms.dr_legal_entity) as dr_legal_entity,substr(wms.dr_legal_entity,1,1) as le, wms.qty_percent 
from rlsf_prod.le_lob_wms_detail wms 
join split_qty on split_qty.item=wms.item and split_qty.isbn13=wms.isbn13 and split_qty.title=wms.title and split_qty.qty_percent=wms.qty_percent 
and substr(wms.dr_legal_entity,1,1)=split_qty.le and wms.source=split_qty.source
--where wms.item='190858A'
group by wms.item, wms.isbn13, wms.title, wms.qty_percent,le,wms.source)
,
lob as (
select wms.item, wms.isbn13, wms.title, wms.source, substr(wms.dr_legal_entity,1,1) as le, wms.dr_legal_entity, max(wms.dr_line_of_business) as dr_line_of_business, wms.qty_percent 
from rlsf_prod.le_lob_wms_detail wms join le on le.item=wms.item and le.isbn13=wms.isbn13 and le.title=wms.title and le.source=wms.source and 
le.qty_percent=wms.qty_percent and le.dr_legal_entity=wms.dr_legal_entity and substr(wms.dr_legal_entity,1,1)=le.le
--where wms.item='190858A' 
group by wms.item, wms.isbn13, wms.title, wms.qty_percent, wms.dr_legal_entity,le,wms.source)
,
summed as (
select item, isbn13, title, source, wms_qty, substr(dr_legal_entity,1,1) as le,
sum(layer_quantity) as layer_quantity,
sum(qty_percent) as tot_split_qty,
max(qty_percent) as max_split_qty,
max(qty_percent) + (wms_qty - sum(qty_percent)) as tuned_qty
from rlsf_prod.le_lob_wms_detail 
where qty_percent <>0 --and item='190858A' 
group by item, isbn13, title, wms_qty, le, source)

select substr(cast(now() as string),1,10) as run_date,
wms.item, wms.isbn13, wms.title, wms.source, wms.dr_legal_entity, wms.dr_line_of_business, wms.layer_quantity, wms.split_percent, 
summed.tuned_qty as split_quantity, wms.layer_quantity + summed.tuned_qty as final_quantity
from rlsf_prod.le_lob_wms_detail wms 
join lob on lob.item=wms.item and lob.isbn13=wms.isbn13 and lob.title=wms.title and lob.dr_legal_entity=wms.dr_legal_entity and
lob.dr_line_of_business=wms.dr_line_of_business and lob.le=substr(wms.dr_legal_entity,1,1) and wms.source=lob.source
join summed on summed.item=wms.item and summed.isbn13=wms.isbn13 and summed.title=wms.title and summed.le=substr(wms.dr_legal_entity,1,1) and
wms.source=summed.source
where wms.qty_percent <>0 --and wms.item='190858A'
;


insert into table rlsf_prod.le_lob_wms_adjustment

select
substr(cast(now() as string),1,10) as run_date,
wms.item,
wms.isbn13,
wms.title,
wms.source,
wms.dr_legal_entity,
wms.dr_line_of_business,
wms.layer_quantity,
wms.split_percent,
wms.qty_percent,
wms.final_quantity
from rlsf_prod.le_lob_wms_detail wms
left anti join rlsf_prod.le_lob_wms_adjustment adj on adj.item=wms.item and adj.isbn13=wms.isbn13 and adj.title=wms.title and
adj.legal_entity=wms.dr_legal_entity and adj.line_of_business=wms.dr_line_of_business and adj.layer_quantity=wms.layer_quantity
and adj.split_percent=wms.split_percent and adj.source=wms.source
;


insert into table rlsf_prod.le_lob_wms_adjustment

with split_agg as (

select sum(layer_quantity) as layer_quantity, dr_legal_entity, dr_line_of_business, item,
title, isbn13, source --substr(dr_legal_entity,1,1) as le
from rlsf_prod.le_lob
where concat(dr_legal_entity,dr_line_of_business) in ('520120','510120','114910','114000','114120') 
and concat(lelob_source_description,'-',event) in ('Base-Sales','PO_Split-Receipt','WO_Split-Collation')
group by dr_legal_entity, dr_line_of_business,item,
 title, item_type, isbn13, source --le
 )

select substr(cast(now() as string),1,10) as run_date,
agg.item,
agg.isbn13, 
agg.title,
agg.source,
agg.dr_legal_entity, 
agg.dr_line_of_business,
agg.layer_quantity as layer_quantity, 
0 as split_percent, 
0 as qty_percent,
agg.layer_quantity as final_quantity
from split_agg agg
--where layer_quantity > 0 
group by agg.dr_legal_entity, agg.dr_line_of_business,agg.item, agg.title,
agg.isbn13,agg.layer_quantity,agg.source
;

insert into table rlsf_prod.le_lob_wms_adjustment

with split_agg as (
select sum(layer_quantity) as layer_quantity,
case when lelob_source_description in ('WO_Split','PO_Split') and dr_legal_entity <>'' then dr_legal_entity
when lelob_source_description in ('WO_Split','PO_Split') and dr_legal_entity ='' then cr_legal_entity
else dr_legal_entity end as dr_legal_entity,
case when lelob_source_description in ('WO_Split','PO_Split') and dr_line_of_business <>'' then dr_line_of_business
when lelob_source_description in ('WO_Split','PO_Split') and dr_line_of_business ='' then cr_line_of_business
else dr_line_of_business end as dr_line_of_business,
item,
title, isbn13, source
from rlsf_prod.le_lob
where concat(dr_legal_entity,dr_line_of_business) not in ('101000','510000','520120','510120','114910','114000','114120') 
and concat(lelob_source_description,'-',event) in ('Base-Sales','PO_Split-Receipt','WO_Split-Collation')
group by dr_legal_entity, dr_line_of_business,item,
title, item_type,  isbn13, source
)

select 
substr(cast(now() as string),1,10) as run_date,
agg.item,
agg.isbn13, 
agg.title,
agg.source,
agg.dr_legal_entity,
agg.dr_line_of_business,
agg.layer_quantity as layer_quantity, 
0 as split_percent,
0 as qty_percent,
agg.layer_quantity as final_quantity
from split_agg agg
where layer_quantity <= 0
;