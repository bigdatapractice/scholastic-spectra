insert overwrite table rlsf_prod.le_lob_wms_merge

with dr as (
select distinct item, dr_legal_entity,dr_line_of_business
from rlsf_prod.le_lob
--where cr_account<>'1421000'
--and item='201428A'
)

SELECT
'' as organization_code,
wms.item,
'' as short_id,
'' as transaction_type,
'' as reference,
'' as legacy_transaction_type,
'' as legacy_reason,
'' as transaction_source,
'' as source,
'' as gl_batch_desc,
'' as jrnl_batch_name,
'' as dr_jrnl_line_num,
'' as cr_jrnl_line_num,
'' as dr_account_num,
'' as cr_account_num,
0 as total_amount,
wms.final_quantity as layer_quantity,
0 as cost_unit,
0 as entered_dr,
'' as period,
'' as imt_creation_date,
'' as imt_creation_time,
'' as transaction_date,
'' as transaction_time,
'' as journal_creation_date,
'' as journal_creation_time,
'' as txn_transaction_id,
'' as rec_trxn_id,
'' as source_line,
wms.title,
'' as item_type,
'' as opus_item,
wms.isbn13,
wms.legal_entity as dr_legal_entity,
wms.line_of_business as dr_line_of_business,
'' as dr_account,
'' as dr_cost_center,
'' as dr_product_line,
'' as dr_channel,
'' as dr_loc,
'' as dr_interco,
'' as cr_legal_entity,
'' as cr_line_of_business,
'' as cr_account,
'' as cr_cost_center,
'' as cr_product_line,
'' as cr_channel,
'' as cr_loc,
'' as cr_interco,
'' as txn_external_system_ref_id,
'' as po_wo,
'' as destination_org,
'' as item_category,
'' as category_description,
'' as journal,
'' as accounting_class_code,
'' as po_wo_transaction_type,
'' as po_wo_transaction_source,
'LE/LOB' as lelob_source,
'WMS_Split' as lelob_source_description,
'WMS Adjustment' as event
from rlsf_prod.le_lob_wms_adjustment wms
join dr on wms.item=dr.item and wms.legal_entity=dr.dr_legal_entity and
wms.line_of_business=dr.dr_line_of_business
where wms.split_quantity<>0 
--and dr.item='201428A'
;


insert into table rlsf_prod.le_lob_wms_merge

with dr as (
select distinct item, dr_legal_entity,dr_line_of_business,concat(dr_legal_entity,dr_line_of_business) as drlob
from rlsf_prod.le_lob
--where cr_account<>'1421000'
--and item='230433A'
)
,
cr as (
select distinct le.item, le.cr_legal_entity,le.cr_line_of_business
from rlsf_prod.le_lob le
left anti join dr on le.item=dr.item and concat(le.cr_legal_entity,le.cr_line_of_business)=dr.drlob
--where le.dr_account<>'1421000' and le.cr_account='1421000' 
--and le.item='230433A'
)

SELECT
'' as organization_code,
wms.item,
'' as short_id,
'' as transaction_type,
'' as reference,
'' as legacy_transaction_type,
'' as legacy_reason,
'' as transaction_source,
'' as source,
'' as gl_batch_desc,
'' as jrnl_batch_name,
'' as dr_jrnl_line_num,
'' as cr_jrnl_line_num,
'' as dr_account_num,
'' as cr_account_num,
0 as total_amount,
wms.final_quantity as layer_quantity,
0 as cost_unit,
0 as entered_dr,
'' as period,
'' as imt_creation_date,
'' as imt_creation_time,
'' as transaction_date,
'' as transaction_time,
'' as journal_creation_date,
'' as journal_creation_time,
'' as txn_transaction_id,
'' as rec_trxn_id,
'' as source_line,
wms.title,
'' as item_type,
'' as opus_item,
wms.isbn13,
'' as dr_legal_entity,
'' as dr_line_of_business,
'' as dr_account,
'' as dr_cost_center,
'' as dr_product_line,
'' as dr_channel,
'' as dr_loc,
'' as dr_interco,
wms.legal_entity as cr_legal_entity,
wms.line_of_business as cr_line_of_business,
'' as cr_account,
'' as cr_cost_center,
'' as cr_product_line,
'' as cr_channel,
'' as cr_loc,
'' as cr_interco,
'' as txn_external_system_ref_id,
'' as po_wo,
'' as destination_org,
'' as item_category,
'' as category_description,
'' as journal,
'' as accounting_class_code,
'' as po_wo_transaction_type,
'' as po_wo_transaction_source,
'LE/LOB' as lelob_source,
'WMS_Split' as lelob_source_description,
'WMS Adjustment' as event
from rlsf_prod.le_lob_wms_adjustment wms
join cr on wms.item=cr.item and wms.legal_entity=cr.cr_legal_entity and
wms.line_of_business=cr.cr_line_of_business
where wms.split_quantity<>0
--and cr.item='201428A'
;