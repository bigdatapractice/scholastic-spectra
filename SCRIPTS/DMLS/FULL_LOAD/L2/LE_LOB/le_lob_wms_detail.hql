insert overwrite table rlsf_prod.le_lob_wms_detail

with split_agg as (
select sum(layer_quantity) as layer_quantity,
case when lelob_source_description in ('WO_Split','PO_Split') and dr_legal_entity <>'' then substr(dr_legal_entity,1,1)
when lelob_source_description in ('WO_Split','PO_Split') and dr_legal_entity ='' then substr(cr_legal_entity,1,1)
else substr(dr_legal_entity,1,1) end as le,
case when lelob_source_description in ('WO_Split','PO_Split') and dr_legal_entity <>'' then dr_legal_entity
when lelob_source_description in ('WO_Split','PO_Split') and dr_legal_entity ='' then cr_legal_entity
else dr_legal_entity end as dr_legal_entity,
case when lelob_source_description in ('WO_Split','PO_Split') and dr_line_of_business <>'' then dr_line_of_business
when lelob_source_description in ('WO_Split','PO_Split') and dr_line_of_business ='' then cr_line_of_business
else dr_line_of_business end as dr_line_of_business,
item,
title, isbn13,source
from rlsf_prod.le_lob
where concat(dr_legal_entity,dr_line_of_business) not in ('101000','510000','520120','510120','114910','114000','114120') 
and concat(lelob_source_description,'-',event) in ('Base-Sales','PO_Split-Receipt','WO_Split-Collation')
group by le,dr_legal_entity, dr_line_of_business,item,
title, item_type, isbn13, source
)
,
split_sale as (
select sum(layer_quantity) as sale_qty,dr_legal_entity, dr_line_of_business, item,
title, isbn13,substr(dr_legal_entity,1,1) as le
from rlsf_prod.le_lob where trim(transaction_source)='WMS ADJUSTMENTS'
and concat(dr_legal_entity,dr_line_of_business) in ('101000','510000') 
and concat(lelob_source_description,'-',event) in ('Base-') 
group by dr_legal_entity, dr_line_of_business, item,
title, item_type,  isbn13,le
)

select substr(cast(now() as string),1,10) as run_date,agg.item,agg.isbn13, agg.title,agg.source,
agg.dr_legal_entity, agg.dr_line_of_business,
agg.layer_quantity as layer_quantity, 
case when sale.sale_qty is null then 0
else agg.layer_quantity/tot.tot_qty end as split_percent,
case when sale.sale_qty is null then 0
else cast(round(((agg.layer_quantity/tot_qty) * sale.sale_qty)) as decimal(38,6)) end as qty_percent,
case when sale.sale_qty is null then 0
else sale.sale_qty end as wms_qty,
case when sale.sale_qty is null then agg.layer_quantity
else agg.layer_quantity + cast(round(((agg.layer_quantity/tot_qty) * sale.sale_qty)) as decimal(38,6)) end as final_quantity
from split_agg agg
left join split_sale sale on sale.item=agg.item and sale.le=agg.le --and sale.organization_code=agg.organization_code
left join (select sum(layer_quantity) as tot_qty,item, le from split_agg where layer_quantity >0
group by item, le) as tot on agg.item=tot.item and tot.le=agg.le
where layer_quantity > 0 
group by agg.dr_legal_entity, agg.dr_line_of_business,agg.item, agg.title,agg.source,
agg.isbn13,agg.layer_quantity,sale.sale_qty,tot.tot_qty
;