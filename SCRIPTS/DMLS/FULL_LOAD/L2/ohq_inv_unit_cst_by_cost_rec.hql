SET decimal_v2 = false;

WITH only_receipt
AS (
	SELECT DISTINCT deb.transactionseoinventoryitemid
		,deb.transactionseoinventoryorgid
		,deb.cstlayercostspeorectrxnid
	FROM rlsf_prod.cost_trans_debit deb -- change table name accordingly
	WHERE cast(deb.COSTDISTRIBUTIONLINESEOENTEREDCURRENCYAMOUNT AS DECIMAL(38, 6)) <> 0
		AND deb.cstlayercostspeocosttransactiontype = 'RECEIPT'
	)
INSERT INTO TABLE rlsf_prod.ohq_inv_unit_cst_by_cost_rec
SELECT DISTINCT deb.transactionseoinventoryitemid
	,deb.transactionseoinventoryorgid
	,deb.cstlayercostspeorectrxnid
	,case when deb.costdistributionlineseocostelementid=dist_cost_element_id and cst.inventory_item_id=deb.transactionseoinventoryitemid
and cst.org_id=deb.transactionseoinventoryorgid and cst.rec_trxn_id=deb.cstlayercostspeorectrxnid
then 0.000000
else cast(deb.layer_unit_cost AS DECIMAL(38, 6)) end AS cost_unit
	,deb.transactionseocreationdate AS transactionseocreationdate
	,deb.costdistributionlineseocostelementid
	,deb.distributionlineid
	,deb.cstlayercostspeocosttransactiontype
FROM rlsf_prod.cost_trans_debit deb -- change table name accordingly
left join (select distinct dist_cost_element_id,inventory_item_id,org_id,rec_trxn_id from rlsf_prod.cost_trans_prod_allorgs_hist
where trim(val_unit_id) != '' and dist_cost_element_id = '300000073384740' and distribution_dr_cr_sign = 'DR'
and additional_processing_code='ACQUISITION') cst on 
cst.inventory_item_id=deb.transactionseoinventoryitemid
and cst.org_id=deb.transactionseoinventoryorgid and cst.rec_trxn_id=deb.cstlayercostspeorectrxnid
	left anti
JOIN only_receipt recp ON recp.transactionseoinventoryitemid = deb.transactionseoinventoryitemid
	AND recp.transactionseoinventoryorgid = deb.transactionseoinventoryorgid
	AND recp.cstlayercostspeorectrxnid = deb.cstlayercostspeorectrxnid
	JOIN rlsf_prod.cost_trans_rank_assign assign ON assign.transactionseoinventoryitemid = deb.transactionseoinventoryitemid
	AND assign.ranknumber >= ${var:fromranknumber}
	AND assign.ranknumber <= ${var:toranknumber}
WHERE cast(deb.COSTDISTRIBUTIONLINESEOENTEREDCURRENCYAMOUNT AS DECIMAL(38, 6)) > 0
	AND cast(costdistributionseolayerquantity AS DECIMAL(38, 6)) > 0
	AND deb.cstlayercostspeocosttransactiontype = 'ADJUST'
	AND deb.cstlayercostspeorectrxnid IN (
		SELECT DISTINCT cstlayercostspeorectrxnid
		FROM (
			SELECT cstlayercosttransid
				,cstlayercostspeorectrxnid
				,sum(cast(costdistributionseolayerquantity AS DECIMAL(38, 6))) AS layer_unit_qty
			FROM rlsf_prod.cost_trans_debit
			GROUP BY cstlayercosttransid
				,cstlayercostspeorectrxnid
			) aa
		WHERE cast(layer_unit_qty AS DECIMAL(38, 6)) > 0
		);

SET decimal_v2 = false;

INSERT INTO TABLE rlsf_prod.ohq_inv_unit_cst_by_cost_rec
SELECT DISTINCT deb.transactionseoinventoryitemid
	,deb.transactionseoinventoryorgid
	,deb.cstlayercostspeorectrxnid
	,case when deb.costdistributionlineseocostelementid=dist_cost_element_id and cst.inventory_item_id=deb.transactionseoinventoryitemid
and cst.org_id=deb.transactionseoinventoryorgid and cst.rec_trxn_id=deb.cstlayercostspeorectrxnid
then 0.000000
else cast(deb.layer_unit_cost AS DECIMAL(38, 6)) end AS cost_unit
	,deb.transactionseocreationdate AS transactionseocreationdate
	,deb.costdistributionlineseocostelementid
	,deb.distributionlineid
	,deb.cstlayercostspeocosttransactiontype
FROM rlsf_prod.cost_trans_debit deb -- change table name accordingly
left join (select distinct dist_cost_element_id,inventory_item_id,org_id,rec_trxn_id from rlsf_prod.cost_trans_prod_allorgs_hist
where trim(val_unit_id) != '' and dist_cost_element_id = '300000073384740' and distribution_dr_cr_sign = 'DR'
and additional_processing_code='ACQUISITION') cst on 
cst.inventory_item_id=deb.transactionseoinventoryitemid
and cst.org_id=deb.transactionseoinventoryorgid and cst.rec_trxn_id=deb.cstlayercostspeorectrxnid
	left anti
JOIN rlsf_prod.ohq_inv_unit_cst_by_cost_rec un_cst ON un_cst.transactionseoinventoryitemid = deb.transactionseoinventoryitemid
	AND un_cst.transactionseoinventoryorgid = deb.transactionseoinventoryorgid
	AND un_cst.cstlayercostspeorectrxnid = deb.cstlayercostspeorectrxnid
	JOIN rlsf_prod.cost_trans_rank_assign assign ON assign.transactionseoinventoryitemid = deb.transactionseoinventoryitemid
	AND assign.ranknumber >= ${var:fromranknumber}
	AND assign.ranknumber <= ${var:toranknumber}
WHERE cast(deb.COSTDISTRIBUTIONLINESEOENTEREDCURRENCYAMOUNT AS DECIMAL(38, 6)) <> 0
	AND cast(costdistributionseolayerquantity AS DECIMAL(38, 6)) > 0
	AND deb.cstlayercostspeocosttransactiontype IN (
		'ADJUST'
		,'RECEIPT'
		);
	