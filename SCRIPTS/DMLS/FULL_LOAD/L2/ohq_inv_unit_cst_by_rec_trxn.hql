INSERT overwrite TABLE rlsf_prod.ohq_inv_unit_cst_by_rec_trxn
SELECT cst.transactionseoinventoryitemid
	,itm.oracle_id
	,cst.transactionseoinventoryorgid
	,org.oicode
	,cst.cstlayercostspeorectrxnid
	,cost_unit
	,cst.transactionseocreationdate
	,cst.costdistributionlineseocostelementid
	,ele.cost_element_code
	,cst.distributionlineid
	,cst.cstlayercostspeocosttransactiontype
	,ohq.po_number
FROM rlsf_prod.ohq_inv_unit_cst_by_cost_rec cst
LEFT JOIN rlsf_prod.cost_element_code ele ON CST.costdistributionlineseocostelementid = ele.cost_element_id
LEFT JOIN rlsf_prod.oracle_items_parquet itm ON cst.transactionseoinventoryitemid = itm.inventory_item_id
LEFT JOIN rlsf_prod.org_master_parquet org ON cst.transactionseoinventoryorgid = org.oiorgid
LEFT JOIN rlsf_prod.ohq_po ohq ON cst.transactionseoinventoryitemid = ohq.inventory_item_id
	AND cst.cstlayercostspeorectrxnid = ohq.rec_trans_id;