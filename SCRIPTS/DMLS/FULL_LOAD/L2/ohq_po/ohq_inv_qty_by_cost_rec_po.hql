insert overwrite table rlsf_prod.ohq_inv_qty_by_cost_rec_po
select distinct 
deb.transactionseoinventoryitemid,
case when deb.cstlayercostspeorectrxnid=deb.cstlayercosttransid and deb.cstlayercostspeocosttransactiontype ="RECEIPT"
then cast(deb.costdistributionseolayerquantity as decimal(38,6)) 
when deb.cstlayercostspeorectrxnid=deb.cstlayercosttransid and deb.cstlayercostspeocosttransactiontype = "ISSUE"
then 0
else cast(deb.costdistributionseolayerquantity as decimal(38,6))
end as costdistributionseolayerquantity,
deb.transactionseoinventoryorgid,
deb.transactionseocreationdate as  transactionseocreationdate,
deb.transactionseocstinvtransactionid,
deb.cstlayercostspeorectrxnid,
deb.txn_external_system_ref_id,
deb.cstlayercosttransid,
deb.po_number,
deb.transaction_type_name
from  rlsf_prod.cost_trans_debit deb
join rlsf_prod.cost_trans_rank_assign assign on assign.transactionseoinventoryitemid = deb.transactionseoinventoryitemid and 
assign.ranknumber >=${var:fromranknumber} and assign.ranknumber <= ${var:toranknumber}
where deb.costdistributionlineseocostelementid='300000073384740' and deb.cstlayercostspeocosttransactiontype <> 'ADJUST';


insert into table rlsf_prod.ohq_inv_qty_by_cost_rec_po
select distinct 
deb.transactionseoinventoryitemid,
cast(deb.costdistributionseolayerquantity as decimal(38,6)) as costdistributionseolayerquantity,
deb.transactionseoinventoryorgid,
deb.transactionseocreationdate as  transactionseocreationdate,
deb.transactionseocstinvtransactionid,
deb.cstlayercostspeorectrxnid,
deb.txn_external_system_ref_id,
deb.cstlayercosttransid,
deb.po_number,
deb.transaction_type_name
from  rlsf_prod.cost_trans_debit deb
left anti join rlsf_prod.ohq_inv_qty_by_cost_rec_po rcpt on rcpt.cstlayercosttransid=deb.cstlayercosttransid and
rcpt.cstlayercostspeorectrxnid=deb.cstlayercostspeorectrxnid
join rlsf_prod.cost_trans_rank_assign assign on assign.transactionseoinventoryitemid = deb.transactionseoinventoryitemid and 
assign.ranknumber >=${var:fromranknumber} and assign.ranknumber <= ${var:toranknumber}
where deb.costdistributionlineseocostelementid='300000073384740' and deb.cstlayercostspeocosttransactiontype = 'ADJUST';