INSERT overwrite TABLE rlsf_prod.cost_trans_debit
SELECT CAST(dist_cost_element_id AS STRING) costdistributionlineseocostelementid
	,CAST(distribution_line_id AS STRING) distributionlineid
	,CAST(trans.inventory_item_id AS STRING) transactionseoinventoryitemid
	,CAST(layer_quantity AS STRING) costdistributionseolayerquantity
	,CAST(org_id AS STRING) transactionseoinventoryorgid
	,CAST(TXn_CREATION_DATE AS STRING) transactionseocreationdate
	,CAST(cst_inv_transaction_id AS STRING) transactionseocstinvtransactionid
	,CAST(trans.rec_trxn_id AS STRING) cstlayercostspeorectrxnid
	,CAST(dist_cost_txn_type AS STRING) cstlayercostspeocosttransactiontype
	,CAST(entered_currency_amount AS STRING) costdistributionlineseoenteredcurrencyamount
	,CAST(layer_unit_cost AS STRING) cstlayercostspeounitcost
	,CAST(dist_lines_last_update_date AS STRING) dist_lines_last_update_date
	,CAST(txn_external_system_ref_id AS STRING) txn_external_system_ref_id
	,CAST(layer_unit_cost AS DECIMAL(38, 6)) layer_unit_cost
	,CAST(txn_transaction_id AS STRING) cstlayercosttransid
	,'' AS po_number
	,'' transaction_type_name
	--,CAST(po_id.source_reference AS STRING) AS po_number
	--,CAST(po_id.inv_transaction_type AS STRING) AS transaction_type_name
FROM rlsf_prod.cost_trans_prod_allorgs_hist trans
--LEFT JOIN (
--	SELECT DISTINCT rec_trxn_id
--		,inventory_item_id
--		,distribution_id
--		,source_reference
--		,inv_transaction_type
--	FROM rlsf_prod.gl_master_parquet
--	WHERE distribution_dr_cr_sign = 'DR'
--	) po_id ON po_id.rec_trxn_id = trans.rec_trxn_id
--	AND po_id.distribution_id = trans.distribution_id
--	AND po_id.inventory_item_id = trans.inventory_item_id
WHERE distribution_dr_cr_sign = 'DR'
	AND trim(val_unit_id) != '';