insert overwrite table rlsf_prod.gmrd_isbn_details
select
source ,
ref_number ,
proforma_ref ,
isbn13 ,
title ,
rev_amt,
rev_qty,
cop_amt,
cop_qty,
case 
when rev_qty=0 and cop_qty=0 then 0 
else rev_qty - cop_qty end as  qty_var,
case 
when rev_amt = 0 or cop_amt =0 then 0
when abs(cop_amt/rev_amt) > 1 then 1 
else cast((cop_amt/rev_amt) as decimal(38,6))
end as cop_margin,
case 
when rev_qty = 0 or cop_amt = 0 or cop_qty = 0 then 0
else 
cast(((cop_amt/cop_qty) * rev_qty) as decimal(38,6))
end as profroma_cop_amt,
case 
when rev_amt = 0 or cop_amt =0 or cop_qty= 0 or rev_qty =0 then 0
when abs((((cop_amt/cop_qty) * rev_qty) / rev_amt)) > 1 then 1 
else cast((((cop_amt/cop_qty) * rev_qty) / rev_amt) as decimal(38,6))
end as proforma_margin,
case 
when rev_amt =0 or rev_qty = 0 then 0 
else cast((rev_amt/rev_qty) as decimal(38,6)) end as rev_per_unit,
case 
when cop_amt =0 or cop_qty = 0 then 0 
else cast((cop_amt/cop_qty) as decimal(38,6)) end as cop_per_unit,
period ,
transaction_date ,
status ,
legacy_reason ,
legacy_trans_type ,
if(isbn_Legal_Entity is null ,'000',isbn_Legal_Entity)  as isbn_Legal_Entity,
if(isbn_Line_of_Business is null ,'000',isbn_Line_of_Business) as isbn_Line_of_Business ,
if(isbn_Account is null ,'000',isbn_Account) as isbn_Account ,
if(isbn_Cost_Center is null ,'000', isbn_Cost_Center) as isbn_Cost_Center,
if(isbn_Product_Line is null ,'000', isbn_Product_Line) as isbn_Product_Line ,
if(isbn_Channel is null ,'000', isbn_Channel) as isbn_Channel ,
if(isbn_Loc is null ,'000',isbn_Loc) as isbn_Loc ,
if(isbn_Interco is null ,'000',isbn_Interco) as isbn_Interco ,
if(gmrd_Legal_Entity is null ,'000',gmrd_Legal_Entity) as gmrd_Legal_Entity ,
if(gmrd_Line_of_Business is null ,'000',gmrd_Line_of_Business) as gmrd_Line_of_Business ,
if(gmrd_Account is null ,'000', gmrd_Account)  as gmrd_Account,
if(gmrd_Cost_Center is null ,'000', gmrd_Cost_Center) as gmrd_Cost_Center,
if(gmrd_Product_Line is null ,'000', gmrd_Product_Line) as gmrd_Product_Line ,
if(gmrd_Channel is null ,'000',gmrd_Channel) as gmrd_Channel ,
if(gmrd_Loc is null ,'000',gmrd_Loc) as gmrd_Loc ,
if(gmrd_Interco is null ,'000',gmrd_Interco) as gmrd_Interco 
from 
rlsf_prod.gmrd_isbn_rev_ref_details; 