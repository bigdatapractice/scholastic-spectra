with compare_agg as (select sum(ohq) as ohq, sum(BI) as bi, sum(Matched) as Matched, sum(Not_Matched) as Not_Matched, 
sum(ohq_only) as ohq_only, sum(bi_only) as bi_only
from
(select 
if(diff_state <> 'NA-Hadoop', sum(total), 0) as ohq,
if(diff_state <> 'NA-Cloud', sum(total), 0) as BI,
if(diff_state = 'Matched', sum(total), 0) as Matched,
if(diff_state = 'Not Matched', sum(total), 0) as Not_Matched,
if(diff_state = 'NA-Cloud', sum(total), 0) as ohq_only,
if(diff_state = 'NA-Hadoop', sum(total), 0) as bi_only
from 
(select diff_state, count(diff_state) as total
from rlsf_prod.ohq_compare_bi_with_hadoop group by diff_state) cnt group by diff_state) as tot)

insert overwrite table rlsf_prod.ohq_compare_bi_with_hadoop_agg
select 'Total Records in OHQ' as description, cast(ohq as string) as total from compare_agg
union all
select 'Total Records in Cloud BI Extract' as description, cast(bi as string) as total from compare_agg
union all
select 'Matching Records' as description, cast(Matched as string) as total from compare_agg
union all
select 'Not Matching Records' as description, cast(Not_Matched as string) as total from compare_agg
union all
select 'Not Available in Records - Hadoop' as description, cast(bi_only as string) as total from compare_agg
union all
select 'Not Available in Records - Cloud' as description, cast(ohq_only as string) as total from compare_agg;