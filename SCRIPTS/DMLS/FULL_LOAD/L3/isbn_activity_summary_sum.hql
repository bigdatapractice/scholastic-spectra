set DECIMAL_V2=false;
insert into table rlsf_prod.isbn_activity_summary_sum

with ext_id as (
select distinct txn_external_system_ref_id from rlsf_prod.isbn_activity where dist_cost_element_id='300000073384740'
),
ext_ref_id as (
select distinct txn_external_system_ref_id from rlsf_prod.isbn_activity isbn
left anti join ext_id on ext_id.txn_external_system_ref_id=isbn.txn_external_system_ref_id
)

select
ORGANIZATION_CODE ,
ITEM ,
SHORT_ID ,
TRANSACTION_TYPE ,
REFERENCE ,
LEGACY_TRANSACTION_TYPE ,
LEGACY_REASON ,
TRANSACTION_SOURCE ,
SOURCE ,
GL_BATCH_DESC ,
isbn.JRNL_BATCH_NAME ,
DR_JRNL_LINE_NUM ,
CR_JRNL_LINE_NUM ,
dr_account_num ,
cr_account_num ,
case when dr_Account='1421000' then 'Y'
when dr_Account<>'1421000' and cr_Account='1421000' then 'Y'
else 'N' end as 1421000_Flag ,
case when ext_ref_id.txn_external_system_ref_id=isbn.txn_external_system_ref_id and dr_Account='1421000' and LAYER_QUANTITY < 0 then LAYER_QUANTITY *(-1)
when ext_ref_id.txn_external_system_ref_id=isbn.txn_external_system_ref_id and dr_Account<>'1421000' and cr_Account='1421000' and LAYER_QUANTITY > 0 then LAYER_QUANTITY *(-1)
when ext_ref_id.txn_external_system_ref_id=isbn.txn_external_system_ref_id and dr_Account='1421000' and LAYER_QUANTITY > 0 then LAYER_QUANTITY 
when ext_ref_id.txn_external_system_ref_id=isbn.txn_external_system_ref_id and dr_Account<>'1421000' and cr_Account='1421000' and LAYER_QUANTITY < 0 then LAYER_QUANTITY
when dist_cost_element_id='300000073384740' and dr_Account='1421000' and LAYER_QUANTITY < 0 then LAYER_QUANTITY *(-1)
when dist_cost_element_id='300000073384740' and dr_Account<>'1421000' and cr_Account='1421000' and LAYER_QUANTITY > 0 then LAYER_QUANTITY *(-1)
when dist_cost_element_id='300000073384740' and dr_Account='1421000' and LAYER_QUANTITY > 0 then LAYER_QUANTITY
when dist_cost_element_id='300000073384740' and dr_Account<>'1421000' and cr_Account='1421000' and LAYER_QUANTITY < 0 then LAYER_QUANTITY
else 0 end as QTY ,
case when dr_Account='1421000' and sum(total_amount) < 0 then sum(total_amount) *(-1)
when dr_Account<>'1421000' and cr_Account='1421000' and sum(total_amount) > 0 then sum(total_amount) *(-1)
when dr_Account<>'1421000' and cr_Account<>'1421000' then 0
else sum(total_amount) end as AMOUNT ,
sum(total_amount),
case when dist_cost_element_id='300000073384740' then LAYER_QUANTITY
when dist_cost_element_id<>'300000073384740' and ext_ref_id.txn_external_system_ref_id=isbn.txn_external_system_ref_id then LAYER_QUANTITY
else 0 end as LAYER_QUANTITY,
sum(cost_unit),
sum(entered_dr),
isbn.PERIOD ,
IMT_CREATION_DATE ,
IMT_CREATION_TIME ,
TRANSACTION_DATE ,
TRANSACTION_TIME ,
JOURNAL_CREATION_DATE ,
JOURNAL_CREATION_TIME ,
TXN_TRANSACTION_ID ,
REC_TRXN_ID ,
SOURCE_LINE ,
title ,
item_type ,
opus_item ,
isbn13 ,
dr_Legal_Entity ,
dr_Line_of_Business ,
dr_Account ,
dr_Cost_Center ,
dr_Product_Line ,
dr_Channel ,
dr_Loc ,
dr_Interco ,
cr_Legal_Entity ,
cr_Line_of_Business ,
cr_Account ,
cr_Cost_Center ,
cr_Product_Line ,
cr_Channel ,
cr_Loc ,
cr_Interco ,
isbn.txn_external_system_ref_id ,
PO_WO ,
Destination_org ,
Item_category ,
Category_description ,
Journal ,
case when dr_ACCOUNTING_CLASS_CODE = cr_ACCOUNTING_CLASS_CODE then dr_ACCOUNTING_CLASS_CODE
when dr_ACCOUNTING_CLASS_CODE = '' then cr_ACCOUNTING_CLASS_CODE
when cr_ACCOUNTING_CLASS_CODE = '' then dr_ACCOUNTING_CLASS_CODE
else concat(dr_ACCOUNTING_CLASS_CODE,'/',cr_ACCOUNTING_CLASS_CODE)
end as ACCOUNTING_CLASS_CODE ,
po_wo_transaction_type ,
po_wo_transaction_source
from rlsf_prod.isbn_activity isbn
left join ext_ref_id on ext_ref_id.txn_external_system_ref_id=isbn.txn_external_system_ref_id
join rlsf_prod.isbn_rank_assign assign on assign.PERIOD = isbn.PERIOD and 
assign.ranknumber >=${var:fromranknumber} and assign.ranknumber <= ${var:toranknumber}
where isbn.entered_dr <> 0
group by
ORGANIZATION_CODE ,
ITEM ,
SHORT_ID ,
TRANSACTION_TYPE ,
REFERENCE ,
LEGACY_TRANSACTION_TYPE ,
LEGACY_REASON ,
TRANSACTION_SOURCE ,
SOURCE ,
GL_BATCH_DESC ,
isbn.JRNL_BATCH_NAME ,
DR_JRNL_LINE_NUM ,
CR_JRNL_LINE_NUM ,
dr_account_num ,
cr_account_num ,
1421000_Flag ,
QTY ,
LAYER_QUANTITY ,
isbn.PERIOD ,
IMT_CREATION_DATE ,
IMT_CREATION_TIME ,
TRANSACTION_DATE ,
TRANSACTION_TIME ,
JOURNAL_CREATION_DATE ,
JOURNAL_CREATION_TIME ,
TXN_TRANSACTION_ID ,
REC_TRXN_ID ,
SOURCE_LINE ,
title ,
item_type ,
opus_item ,
isbn13 ,
dr_Legal_Entity ,
dr_Line_of_Business ,
dr_Account ,
dr_Cost_Center ,
dr_Product_Line ,
dr_Channel ,
dr_Loc ,
dr_Interco ,
cr_Legal_Entity ,
cr_Line_of_Business ,
cr_Account ,
cr_Cost_Center ,
cr_Product_Line ,
cr_Channel ,
cr_Loc ,
cr_Interco ,
isbn.txn_external_system_ref_id ,
PO_WO ,
Destination_org ,
Item_category ,
Category_description ,
Journal ,
ACCOUNTING_CLASS_CODE ,
po_wo_transaction_type ,
po_wo_transaction_source;