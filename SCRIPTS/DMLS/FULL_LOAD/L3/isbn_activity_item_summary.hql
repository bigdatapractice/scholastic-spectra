INSERT OVERWRITE TABLE rlsf_prod.isbn_activity_item_summary
select 
item ,
oiorgid ,
organization_code ,
gl_batch_desc ,
isbn13 ,
sum(qty) as qty ,
period
from rlsf_prod.isbn_activity_summary isbn
left join (select oicode,oiorgid from rlsf_prod.org_master_parquet) oi on oi.oicode=isbn.organization_code
group by
item ,
oiorgid ,
organization_code ,
gl_batch_desc ,
isbn13 ,
period
;