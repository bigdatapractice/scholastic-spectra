INSERT overwrite TABLE rlsf_prod.ohq_current_summary
SELECT oracle_item
	,cost_center
	,lc_p_eo_rec_trans_id AS rec_trans_id
	,unit_cost
	,sum(total_qty) AS tot_aty
	,sum(total_amount) AS tot_amount
FROM rlsf_prod.ohq_inv_as_of_date_with_layer_cost
GROUP BY oracle_item
	,cost_center
	,unit_cost
	,lc_p_eo_rec_trans_id