SET DECIMAL_V2 = false;

INSERT overwrite table rlsf_prod.ohq_po

WITH gl AS (
		SELECT DISTINCT inventory_org_id
			,rec_trxn_id
			,inventory_item_id
			,"Direct Organization Transfer" as source_reference
			,inv_transaction_type
			,inv_transaction_source
		FROM rlsf_prod.gl_master_parquet
		WHERE inv_transaction_type IN ('Direct Organization Transfer') and 
			rec_trxn_id not in (select distinct rec_trxn_id from rlsf_prod.gl_master_parquet where inv_transaction_source IN ('Purchase Order','Work Order'))
		union all
SELECT DISTINCT inventory_org_id
			,rec_trxn_id
			,inventory_item_id
			,source_reference
			,inv_transaction_type
			,inv_transaction_source
		FROM rlsf_prod.gl_master_parquet
		WHERE inv_transaction_source IN ('Purchase Order')	
		union all
SELECT DISTINCT inventory_org_id
			,rec_trxn_id
			,inventory_item_id
			,source_reference
			,inv_transaction_type
			,inv_transaction_source
		FROM rlsf_prod.gl_master_parquet
		WHERE inv_transaction_source IN ('Work Order') and cast(layer_quantity as BIGINT) >0				
		)
	,recp AS (
		SELECT *
		FROM gl
		WHERE inv_transaction_source IN ('Purchase Order') and inv_transaction_type='Purchase Order Receipt'	
		)
	,recp_adj AS (
		SELECT *
		FROM recp
		
		UNION
		
		SELECT *
		FROM gl WHERE gl.inv_transaction_source NOT IN ('Purchase Order') and gl.rec_trxn_id not in (select distinct rec_trxn_id from recp)
		),
unit_cst as 
(select org_code,transaction_inventory_item_id,cstlayer_cost_rec_trxnid,cost_element_code,sum(cost_unit) as cost_unit 
from rlsf_prod.ohq_inv_unit_cst_by_rec_trxn
where cost_element_code in ('Material', 'Manufacturing', 'Paper','Freight','Duty')
group by org_code,transaction_inventory_item_id,cstlayer_cost_rec_trxnid,cost_element_code
UNION ALL
select org_code,transaction_inventory_item_id,cstlayer_cost_rec_trxnid,'Others' as cost_element_code,sum(cost_unit) as cost_unit 
from rlsf_prod.ohq_inv_unit_cst_by_rec_trxn
where cost_element_code not in ('Material', 'Manufacturing', 'Paper','Freight','Duty')
group by org_code,transaction_inventory_item_id,cstlayer_cost_rec_trxnid
),

final as (SELECT distinct oracle_item
	,ohq.isbn13 
	,border_item
	,opus_id
	,cost_center
	,org_id
	,org.organization_name
	,lc_p_eo_rec_trans_id
	,ohq.inventory_item_id AS inventory_item_id
	,cast(total_qty as bigint) as total_qty
	,cast(unit_cost as decimal(38,2)) as unit_cost
	,cast(total_amount as decimal(38,2)) as total_amount
	,--case when recp_adj.inv_transaction_source='Work Order' then "Work Order"
	 recp_adj.source_reference as po_number
	,case when recp_adj.inv_transaction_source is null then 'Up Adjustment'
	else recp_adj.inv_transaction_source end as inv_transaction_source
	,layer_date
	,substr(layer_date, 1, 4) AS year_
	,CASE 
		WHEN substr(layer_date, 6, 2) = '01'
			THEN 'Jan'
		WHEN substr(layer_date, 6, 2) = '02'
			THEN 'Feb'
		WHEN substr(layer_date, 6, 2) = '03'
			THEN 'Mar'
		WHEN substr(layer_date, 6, 2) = '04'
			THEN 'Apr'
		WHEN substr(layer_date, 6, 2) = '05'
			THEN 'May'
		WHEN substr(layer_date, 6, 2) = '06'
			THEN 'Jun'
		WHEN substr(layer_date, 6, 2) = '07'
			THEN 'Jul'
		WHEN substr(layer_date, 6, 2) = '08'
			THEN 'Aug'
		WHEN substr(layer_date, 6, 2) = '09'
			THEN 'Sep'
		WHEN substr(layer_date, 6, 2) = '10'
			THEN 'Oct'
		WHEN substr(layer_date, 6, 2) = '11'
			THEN 'Nov'
		WHEN substr(layer_date, 6, 2) = '12'
			THEN 'Dec'
		ELSE ''
		END as month_
	,date_
	,substr(run_date,1,16) as run_date
	,as_of_date
	,og1.category as category
	,case when cs.rec_trxn_id=ohq.lc_p_eo_rec_trans_id then cs.subinventory_code
else 'Main' end as subinventory_code,

case WHEN unit_cst.transaction_inventory_item_id=ohq.inventory_item_id and unit_cst.cstlayer_cost_rec_trxnid=ohq.lc_p_eo_rec_trans_id and unit_cst.cost_element_code='Material' then round(unit_cst.cost_unit,2)
else 0 end Material,
case WHEN unit_cst.transaction_inventory_item_id=ohq.inventory_item_id and unit_cst.cstlayer_cost_rec_trxnid=ohq.lc_p_eo_rec_trans_id and unit_cst.cost_element_code='Manufacturing' then round(unit_cst.cost_unit,2)
else 0 end Manufacturing,
case WHEN unit_cst.transaction_inventory_item_id=ohq.inventory_item_id and unit_cst.cstlayer_cost_rec_trxnid=ohq.lc_p_eo_rec_trans_id and unit_cst.cost_element_code='Paper' then round(unit_cst.cost_unit,2)
else 0 end Paper,
case WHEN unit_cst.transaction_inventory_item_id=ohq.inventory_item_id and unit_cst.cstlayer_cost_rec_trxnid=ohq.lc_p_eo_rec_trans_id and unit_cst.cost_element_code='Freight' then round(unit_cst.cost_unit,2)
else 0 end Freight,
case WHEN unit_cst.transaction_inventory_item_id=ohq.inventory_item_id and unit_cst.cstlayer_cost_rec_trxnid=ohq.lc_p_eo_rec_trans_id and unit_cst.cost_element_code='Duty' then round(unit_cst.cost_unit,2)
else 0 end Duty,
case when unit_cst.transaction_inventory_item_id=ohq.inventory_item_id and unit_cst.cstlayer_cost_rec_trxnid=ohq.lc_p_eo_rec_trans_id and unit_cst.cost_element_code ='Others' then round(unit_cst.cost_unit,2) 
else 0 end Cost_Diff,
itm.title as title

FROM rlsf_prod.ohq_inv_as_of_date_with_layer_cost ohq
LEFT JOIN recp_adj ON ohq.org_id = recp_adj.inventory_org_id AND ohq.lc_p_eo_rec_trans_id = recp_adj.rec_trxn_id AND ohq.inventory_item_id = recp_adj.inventory_item_id
LEFT JOIN rlsf_prod.inventory_orgs_parquet org ON org.organization_code = ohq.cost_center
LEFT JOIN rlsf_prod.org_category og1 ON og1.org = ohq.cost_center
left join rlsf_prod.cost_sub_inventory cs on ohq.org_id=cs.INVENTORY_ORG_ID and cs.rec_trxn_id=ohq.lc_p_eo_rec_trans_id and cs.inventory_item_id=ohq.inventory_item_id
LEFT JOIN unit_cst on unit_cst.transaction_inventory_item_id=ohq.inventory_item_id and unit_cst.cstlayer_cost_rec_trxnid=ohq.lc_p_eo_rec_trans_id 
LEFT JOIN rlsf_prod.oracle_items_parquet itm ON itm.inventory_item_id=ohq.inventory_item_id

)

select oracle_item ,isbn13,border_item,opus_id,cost_center
,org_id,organization_name,lc_p_eo_rec_trans_id,inventory_item_id
,total_qty,unit_cost,total_amount,
sum(Material),sum(Manufacturing),sum(Paper), sum(Freight), sum(Duty), sum(Cost_Diff),
title,po_number,inv_transaction_source
,layer_date,year_,month_,date_,run_date,as_of_date
,category,subinventory_code
from final
group by oracle_item ,isbn13,border_item,opus_id,cost_center
,org_id,organization_name,lc_p_eo_rec_trans_id,inventory_item_id
,total_qty,unit_cost,total_amount,title,po_number,inv_transaction_source
,layer_date,year_,month_,date_,run_date,as_of_date
,category,subinventory_code
;