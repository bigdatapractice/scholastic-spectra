INSERT OVERWRITE TABLE rlsf_prod.ohq_by_lite_qty
WITH ohq AS (
		SELECT oracle_item
			,org_code AS cost_center
			,sum(cast(txn_qty AS DECIMAL(38, 0))) AS CLOUD_QTY
			,org_category
			,subinventory_code
		FROM rlsf_prod.material_transactions_dist
		GROUP BY oracle_item
			,cost_center
			,org_category
			,subinventory_code
		)
	,itm AS (
		SELECT DISTINCT oracle_id
			,isbn13
			,title
			,inventory_item_flag
			,inventory_asset_flag
			,costing_enabled_flag
			,item_type
			,category_code
		FROM rlsf_prod.oracle_items_parquet
		)
	,cost AS (
		SELECT DISTINCT oracle_item
			,isbn13
			,cost_center
			,as_of_date
			,avg_unit_cost
		FROM rlsf_prod.ohq_avg_unit_cost_snapshot
		WHERE oracle_item <> ''
			AND cost_center = 'JCD'
		)
SELECT ohq.cost_center AS ORGANIZATION_CODE
	,substr(cast(from_utc_timestamp(now(), 'America/New_York') AS string), 1, 16) AS as_of_date
	,itm.isbn13 AS ISBN13
	,ohq.oracle_item AS ORACLE_ID
	,itm.TITLE AS TITLE
	,itm.INVENTORY_ITEM_FLAG AS INVENTORY_ITEM_FLAG
	,itm.INVENTORY_ASSET_FLAG AS INVENTORY_ASSET_FLAG
	,itm.COSTING_ENABLED_FLAG AS COSTING_ENABLED_FLAG
	,itm.item_type AS ITEM_TYPE
	,itm.category_code AS ITEM_CATEGORY
	,ohq.org_category
	,ohq.subinventory_code
	,nvl(ohq.CLOUD_QTY, 0) AS OHQ_QTY
	,0 AS ADJ_CLOUD_QTY
	,nvl(cost.AVG_UNIT_COST, 0) AS AVG_UNIT_COST
	,abs(nvl(ohq.CLOUD_QTY, 0)) * nvl(cost.AVG_UNIT_COST, 0) AS TOTAL_AMOUNT
FROM ohq
LEFT JOIN itm ON ohq.oracle_item = itm.oracle_id
LEFT JOIN cost ON ohq.oracle_item = cost.oracle_item;