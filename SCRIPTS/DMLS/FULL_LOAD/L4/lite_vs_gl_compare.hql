refresh rlsf_prod.material_transactions_parquet;

WITH lite
AS (
	SELECT *
	FROM material_transactions_parquet
	WHERE TXN_ID <> 'TXN_ID'
	)
	,gl
AS (
	SELECT DISTINCT '' AS subinventory_code
		,inventory_item_id
		,inventory_org_id
		,txn_external_system_ref_id
		,reference
	FROM gl_master_parquet
	)
INSERT overwrite rlsf_prod.lite_vs_gl_compare
SELECT DISTINCT om.oicode
	,oi.oracle_id
	,gl.subinventory_code
	,CASE 
		WHEN oi.inventory_item_id = lite.item_id
			AND oi.org_id = lite.organization_id
			THEN oi.inventory_item_flag
		ELSE 'Y'
		END inventory_item_flag
	,CASE 
		WHEN oi.inventory_item_id = lite.item_id
			AND oi.org_id = lite.organization_id
			THEN oi.inventory_asset_flag
		ELSE 'Y'
		END inventory_asset_flag
	,CASE 
		WHEN oi.inventory_item_id = lite.item_id
			AND oi.org_id = lite.organization_id
			THEN oi.costing_enabled_flag
		ELSE 'Y'
		END costing_enabled_flag
	,gl.txn_external_system_ref_id
	,lite.txn_id
	,CASE 
		WHEN gl.txn_external_system_ref_id = lite.txn_id
			THEN 'Matched'
		ELSE 'Not Matched'
		END External_sys_ref_id_Status
	,lite.txn_qty
	,lite.organization_id
	,lite.item_id
	,lite.transaction_date
	,lite.imt_creation_date
	,lite.last_update_date
	,lite.source
	,lite.legacy_transaction_type
	,lite.legacy_reason
	,lite.source_line
	,lite.costed_flag
	,lite.reference
	,gl.reference
	,CASE 
		WHEN lite.reference = gl.reference
			THEN 'Matched'
		ELSE 'Not Matched'
		END ref_status
	,substr(cast(now() AS string), 1, 10) AS run_date
	,DATEDIFF(TO_DATE(substr(cast(now() AS string), 1, 10)), TO_DATE(lite.transaction_date)) AS days_diff
FROM lite
LEFT JOIN gl ON gl.txn_external_system_ref_id = lite.txn_id
	AND gl.inventory_item_id = lite.item_id
	AND gl.inventory_org_id = lite.organization_id
LEFT JOIN oracle_items_n oi ON oi.inventory_item_id = lite.item_id
	AND oi.org_id = lite.organization_id
LEFT JOIN org_master_parquet om ON om.oiorgid = lite.organization_id;