with isbn  as
(
select jrnl_batch_name, sum(cast(total_amount as decimal(38,6))) as total_amount
from rlsf_prod.isbn_activity
where period = 'Dec-19'
group by jrnl_batch_name
)
,
 ledger as
(
select batch_name, sum(cast(dr as decimal(38,6))) as dr
from rlsf_prod.jeff_extract_dec
where period = 'Dec-19'
group by batch_name
)

insert overwrite table rlsf_prod.isbn_compare_ledger_with_hadoop
select ledger.batch_name as Batch_Name,
--isbn.jrnl_name as jrnl_name,
isbn.total_amount as DR_Hadoop_Total_Amount,
ledger.dr as DR_Ledger_Total_Amount,
case 
    when isbn.jrnl_batch_name is null then 'NA'
    when cast(ledger.dr as int)= cast(isbn.total_amount as int) then 'Matched'
    else 'Not Matched'
    end as Diff_State,
case 
    when isbn.jrnl_batch_name is null then '0'
    when cast(ledger.dr as int) = cast(isbn.total_amount as int) then '0'
    else cast(round(ledger.dr) - round(isbn.total_amount) as string)
    end as Diff_Value    

from ledger
left join isbn on trim(ledger.batch_name) = trim(isbn.jrnl_batch_name)