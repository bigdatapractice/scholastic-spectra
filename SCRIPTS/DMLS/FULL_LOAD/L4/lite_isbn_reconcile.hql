INSERT OVERWRITE TABLE rlsf_prod.lite_isbn_reconcile

with isbn as (
select sum(qty) as qty, txn_external_system_ref_id from (
select distinct txn_external_system_ref_id,qty from rlsf_prod.isbn_activity_summary
) isbn group by txn_external_system_ref_id
),
lite as (
select * from rlsf_prod.material_transactions
)


select isbn.txn_external_system_ref_id as txn_id, 
'Matching' as txn_id_status ,
cast(lite.txn_qty as int) as lite_qty ,
cast(isbn.qty as int) as isbn_qty ,
case when cast(lite.txn_qty as int)=cast(isbn.qty as int) then 'Matching'
else 'Not Matching' end as qty_status 
from isbn
join lite on isbn.txn_external_system_ref_id=trim(lite.txn_id)

union all

select isbn.txn_external_system_ref_id as txn_id, 
'NA Lite' as txn_id_status ,
0 as lite_qty ,
cast(isbn.qty as int) as isbn_qty ,
'' as qty_status 
from isbn
left anti join lite on isbn.txn_external_system_ref_id=trim(lite.txn_id)

union all

select trim(lite.txn_id) as txn_id, 
'NA ISBN' as txn_id_status ,
cast(lite.txn_qty as int) as lite_qty ,
0 as isbn_qty ,
'' as qty_status 
from lite
left anti join isbn on isbn.txn_external_system_ref_id=trim(lite.txn_id)
;