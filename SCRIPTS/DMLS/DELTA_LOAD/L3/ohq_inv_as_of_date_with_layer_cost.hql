insert into table rlsf_prod.ohq_inv_as_of_date_with_layer_cost
select distinct  
ohq.oracle_item as oracle_item ,
ohq.isbn13 as isbn13 ,
ohq.border_item as border_item ,
ohq.opus_id as opus_id ,
ohq.cost_center as cost_center,
ohq.org_id,
ohq.lc_p_eo_rec_trans_id,
ohq.trans_eo_inventory_item_id as inventory_item_id ,
layer_qty_sum as total_qty,
layer_cost_sum as unit_cost,
total_amount as total_amount,
layer_date as layer_date,
ohq.date_,
ohq.run_date,
ohq.as_of_date  
from rlsf_prod.ohq_inv_onhand_qty_cost_by_layer ohq
join rlsf_prod.cost_trans_rank_assign_delta assign on assign.transactionseoinventoryitemid = ohq.trans_eo_inventory_item_id and 
assign.ranknumber >=${var:fromranknumber} and assign.ranknumber <= ${var:toranknumber}
join (select org_id,lc_p_eo_rec_trans_id,trans_eo_inventory_item_id,max(date_) as maxdate
from rlsf_prod.ohq_inv_onhand_qty_cost_by_layer
group by org_id,lc_p_eo_rec_trans_id,trans_eo_inventory_item_id) maxdate
on 
ohq.org_id=maxdate.org_id and ohq.lc_p_eo_rec_trans_id=maxdate.lc_p_eo_rec_trans_id and 
ohq.trans_eo_inventory_item_id = maxdate.trans_eo_inventory_item_id and ohq.date_=maxdate.maxdate
--where layer_qty_sum > 0
;

