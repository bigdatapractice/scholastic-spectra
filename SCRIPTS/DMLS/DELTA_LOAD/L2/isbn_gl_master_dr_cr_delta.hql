insert into table rlsf_prod.isbn_gl_master_dr_cr_delta

select  dr.* from rlsf_prod.isbn_gl_master_dr_delta dr
join rlsf_prod.isbn_rank_assign_delta assign on assign.JRNL_BATCH_NAME = dr.JRNL_BATCH_NAME and 
assign.ranknumber >=${var:fromranknumber} and assign.ranknumber <= ${var:toranknumber}

union all

select  cr.* from rlsf_prod.isbn_gl_master_cr_delta cr
join rlsf_prod.isbn_rank_assign_delta assign on assign.JRNL_BATCH_NAME = cr.JRNL_BATCH_NAME and 
assign.ranknumber >=${var:fromranknumber} and assign.ranknumber <= ${var:toranknumber}
;